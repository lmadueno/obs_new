<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);


?>
<br>

<div class="alert alert-primary alert-dismissible fade show" role="alert">
	<strong>Leyenda:</strong>
	<hr>
	<ul>
		<li>
			Se considera infectados COVID-19 el promedio móvil 7 desde inicio de pandemia.
		</li>
		<li>
			El 15% de los pacientes infectados requieren Oxigenoterapia.
		</li>
		<li>
			El 75% de pacientes que requieren Oxigenoterapia, necesitan 0.6 m3/hora.

		</li>
		<li>
			El 25% de pacientes que requieren Oxigenoterapia, necesitan 1.8 m3/hora.

		</li>
		<li>
			La necesidad de Oxigeno por quintiles (Rojo, Naranja, Amarillo, verde claro y verde oscuro)

		</li>

	</ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Analisis Casos Positivos COVID-19
			</div>
			<br>

			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">
									<div class="card-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="card card-outline-info">
													<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
														<div class="row">
															<div class="col-lg-12">
																<div class="panel panel-default">
																	<div class="panel-body">
																		<div class="row">
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Departamentos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_oxigeno" id="ddlDepartamentos_oxigeno" name="ddlDepartamentos_oxigeno" onchange="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Provincias</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_oxigeno" id="ddlProvincias_oxigeno" name="ddlProvincias_oxigeno" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Distritos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_minsa" id="ddlDistritos_oxigeno" name="ddlDistritos_oxigeno" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Accion</label>
																					<button type="button" class="form-control input-sm btn btn-primary filtrooxigeno" id="filtrooxigeno" name="filtrooxigeno" onclick="App.events(this); return false;">Filtrar</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
											DEMANDA DE OXIGENO POR DEPARTAMENTO

										</div>
										<table class="table table-sm table-detail" data-target="tabla2" width="100%"  >
											<thead>
												<tr>
													<th class="text-center bold" style="background:#ddebf8;"width="4%">#</th>
													<th class="text-center bold" style="background:#ddebf8;">Departamento</th>
													<th class="text-center bold" style="background:#ddebf8;">Positivos</th>
													<th class="text-center bold" style="background:#ddebf8;">Promedio 10 días</th>
													<th class="text-center bold" style="background:#ddebf8;">Pacientes</th>
													<th class="text-center bold" style="background:#ddebf8;">Demanda m3/h</th>
												</tr>
												<?php
												$i = 0;

												$total1 = 0;
												$total2 = 0;
												$total3 = 0;
												$pipxUbigeoDet = dropDownList((object) ['method' => 'tb_oxigeno_depa']);
												foreach ($pipxUbigeoDet as $item) {
													$i++;

													$total1 = $total1 + intval($item->sum);
													$total2 = $total2 + intval($item->pacientes); 
													$total3 = $total3 + intval($item->demanda); 
													?>
													<tr>
														<td class="text-center"><?php echo ($i) ?></td>
														<td class="text-center"><?php echo ($item->nomubigeo) ?></td>
														<td class="text-center"><?php echo number_format(round(($item->sum),0)) ?></td>
														<td class="text-center"><?php echo number_format(round(($item->avg),0)) ?></td>
														<td class="text-center"><?php echo number_format(round(($item->pacientes),0)) ?></td>
														<td class="text-center"><?php echo number_format(round(($item->demanda),0)) ?></td>
													</tr>
												<?php
												} ?>
												<tr>
													<td class="text-center"></td>
													<td class="text-center">Total</td>
													<td class="text-center"><?php echo number_format($total1) ?></td>
													<td class="text-center"></td>
													<td class="text-center"><?php echo number_format($total2) ?></td>
													<td class="text-center"><?php echo number_format($total3) ?></td>

												</tr>
											</thead>
										</table>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Demanda promedio Oxigeno m3/hora por Departamento (18 abril)
										</div>
										<div id='chartoxigeno_v2_1' style=" height: 800px; margin: 0 auto"></div>
									</div>
								</div>
							</div>
						</div>


					</div>


				</div>
				<br><br>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Incremento de la demanda de Oxígeno (m3/hora) en base a acumulado de infectados 10 días


						</div>
						<div id='chartoxigeno_v2_2'></div>
					</div>
					<!-- <div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Distribución de vacunados por grupo riesgo


						</div>
						<div id='chartvacuna_5'></div>
					</div>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Proceso de vacunación segun Genero

						</div>
						<div id='chartvacuna_6'></div>
					</div> -->
				</div>

			</div>

		</div>
	</div>
</div>
<br>

<br>

<!-- 
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Demanda de Oxígeno en base a casos positivos COVID-19
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Demanda de Oxígeno en Regiones, fallecidos y población por quintiles
						</div>
						<div id='chartoxigeno_1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Demanda de Oxígeno en Provincias, fallecidos y población por quintiles

						</div>
						<div id='chartoxigeno_2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Demanda de Oxígeno en Distritos, fallecidos y población por quintiles

						</div>
						<div id='chartoxigeno_3'></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="text-center col-lg-12">
					<a class="nav-link active vertabla" data-event="datos1" style="color: #1976D2; !important;" id="datos1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla de Demanda de Oxigeno por quintiles</b></a>
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Demanda de Oxígeno en Regiones, fallecidos y población por quintiles
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%" style="background:#ddebf8">#</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Quintil</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Población</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Fallecidos</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Casos(+) Semanal</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Total m3/h</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;
										$pTotal = 0;
										$ftotal = 0;
										$fftotal = 0;
										$dtotal = 0;
										$o = 0;
										$pdet = 0;
										$fdet = 0;
										$ffdet = 0;
										$ddet = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_regional']);
										// $categorias = ['0-5 años', '6-12 años', '13-17 años', '18-24 años', '25-39 años', '40-55 años', '56-65 años', '66-75 años', '76 + años'];

										foreach ($pipxUbigeoDet as $item) {
											$i++;
											$pTotal = $pTotal + intval($item->p);
											$ftotal = $ftotal + intval($item->f);
											$fftotal = $fftotal + intval($item->f10);
											$dtotal = $dtotal + intval($item->d);
										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center" width="4%">
													<a class="lnkAmpliar_salud" id="<?php echo ($item->quantil) ?>" data-event="lnk_<?php echo ($item->quantil) ?>" href="#" onclick="App.events(this); return false">
														<?php echo 'Q' . ($item->quantil) ?>
													</a>
												</td>
												<td scope="row" class="text-center"><?php echo number_format($item->p) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f10) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->d) ?></td>

											</tr>
											<tr data-target="lnk_<?php echo $item->quantil ?>" style="display: none;">
												<td colspan="8">
													<div class="card">
														<div class="card-header card-special">
															Quintiles Regionales
														</div>
														<div class="card-body">
															<div class="row">
																<div class="col-lg-12">
																	<table class="table table-sm table-detail datatable">
																		<tr>
																			<th class="text-center bold">#</th>
																			<th class="text-center bold">Entidades</th>
																			<th class="text-center bold">Población</th>
																			<th class="text-center bold">Fallecidos</th>
																			<th class="text-center bold">Casos(+) Semanal</th>
																			<th class="text-center bold">Total m3/h</th>
																		</tr>
																		<?php
																		$quintilesCovid = dropDownList((object) ['method' => 'oxigeno_regional_detalle', 'quantil' => $item->quantil]);

																		$suma41 = 0;
																		$suma51 = 0;
																		foreach ($quintilesCovid as $key1) {
																			$o++;
																			$pdet = $pdet + intval($key1->p);
																			$fdet = $fdet + intval($key1->f);
																			$ffdet = $ffdet + intval($key1->f10);
																			$ddet = $ddet + intval($key1->d);
																		?>
																			<tr>
																				<td class="text-center"><?php echo $o; ?></td>
																				<td class="text-left"><?php echo ($key1->departamento) ?></td>
																				<td class="text-center"><?php echo number_format($key1->p) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f10) ?></td>
																				<td class="text-center"><?php echo number_format($key1->d) ?></td>
																			</tr>
																		<?php } ?>
																		<tr>
																			<td class="text-center"></td>
																			<td class="text-center">Total</td>
																			<td class="text-center"><?php echo number_format($pdet);
																									$pdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($fdet);
																									$fdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ffdet);
																									$ffdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ddet);
																									$ddet = 0; ?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($pTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($fftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($dtotal) ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Demanda de Oxígeno en Provincias, fallecidos y población por quintiles
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%" style="background:#ddebf8">#</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Quintil</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Población</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Fallecidos</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Casos(+) Semanal</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Total m3/h</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;
										$pTotal = 0;
										$ftotal = 0;
										$fftotal = 0;
										$dtotal = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_provincial']);

										foreach ($pipxUbigeoDet as $item) {
											$i++;
											$pTotal = $pTotal + intval($item->p);
											$ftotal = $ftotal + intval($item->f);
											$fftotal = $fftotal + intval($item->f10);
											$dtotal = $dtotal + intval($item->d);
										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td><a class="lnkAmpliar_salud" id="<?php echo ($item->quantil) ?>" data-event="lnk__<?php echo ($item->quantil) ?>" href="#" onclick="App.events(this); return false">
														<?php echo 'Q' . ($item->quantil) ?>
													</a></td>
												<td scope="row" class="text-center"><?php echo number_format($item->p) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f10) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->d) ?></td>

											</tr>
											<tr data-target="lnk__<?php echo $item->quantil ?>" style="display: none;">
												<td colspan="8">
													<div class="card">
														<div class="card-header card-special">
															Quintiles Provinciales
														</div>
														<div class="card-body">
															<div class="row">
																<div class="col-lg-12">
																	<table class="table table-sm table-detail datatable">
																		<tr>
																			<th class="text-center bold">#</th>
																			<th class="text-center bold">Entidades</th>
																			<th class="text-center bold">Población</th>
																			<th class="text-center bold">Fallecidos</th>
																			<th class="text-center bold">Casos(+) Semanal</th>
																			<th class="text-center bold">Total m3/h</th>
																		</tr>
																		<?php
																		$quintilesCovid = dropDownList((object) ['method' => 'oxigeno_provincial_detalle', 'quantil' => $item->quantil]);

																		$suma41 = 0;
																		$suma51 = 0;
																		foreach ($quintilesCovid as $key1) {
																			$o++;
																			$pdet = $pdet + intval($key1->p);
																			$fdet = $fdet + intval($key1->f);
																			$ffdet = $ffdet + intval($key1->f10);
																			$ddet = $ddet + intval($key1->d);
																		?>
																			<tr>
																				<td class="text-center"><?php echo $o; ?></td>
																				<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																				<td class="text-center"><?php echo number_format($key1->p) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f10) ?></td>
																				<td class="text-center"><?php echo number_format($key1->d) ?></td>
																			</tr>
																		<?php } ?>
																		<tr>
																			<td class="text-center"></td>
																			<td class="text-center">Total</td>
																			<td class="text-center"><?php echo number_format($pdet);
																									$pdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($fdet);
																									$fdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ffdet);
																									$ffdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ddet);
																									$ddet = 0; ?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($pTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($fftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($dtotal) ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Demanda de Oxígeno en Distritos, fallecidos y población por quintiles
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%" style="background:#ddebf8">#</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Quintil</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Población</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Fallecidos</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Casos(+) semanal</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Total m3/h</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;
										$pTotal = 0;
										$ftotal = 0;
										$fftotal = 0;
										$dtotal = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_distrital']);

										foreach ($pipxUbigeoDet as $item) {
											$i++;
											$pTotal = $pTotal + intval($item->p);
											$ftotal = $ftotal + intval($item->f);
											$fftotal = $fftotal + intval($item->f10);
											$dtotal = $dtotal + intval($item->d);
										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td><a class="lnkAmpliar_salud" id="<?php echo ($item->quantil) ?>" data-event="lnk___<?php echo ($item->quantil) ?>" href="#" onclick="App.events(this); return false">
														<?php echo 'Q' . ($item->quantil) ?>
													</a></td>
												<td scope="row" class="text-center"><?php echo number_format($item->p) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->f10) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->d) ?></td>

											</tr>
											<tr data-target="lnk___<?php echo $item->quantil ?>" style="display: none;">
												<td colspan="8">
													<div class="card">
														<div class="card-header card-special">
															Quintiles Provinciales
														</div>
														<div class="card-body">
															<div class="row">
																<div class="col-lg-12">
																	<table class="table table-sm table-detail datatable">
																		<tr>
																			<th class="text-center bold">#</th>
																			<th class="text-center bold">Entidades</th>
																			<th class="text-center bold">Población</th>
																			<th class="text-center bold">Fallecidos</th>
																			<th class="text-center bold">Casos(+) Semanal</th>
																			<th class="text-center bold">Total m3/h</th>
																		</tr>
																		<?php
																		$quintilesCovid = dropDownList((object) ['method' => 'oxigeno_distrital_detalle', 'quantil' => $item->quantil]);

																		$suma41 = 0;
																		$suma51 = 0;
																		foreach ($quintilesCovid as $key1) {
																			$o++;
																			$pdet = $pdet + intval($key1->p);
																			$fdet = $fdet + intval($key1->f);
																			$ffdet = $ffdet + intval($key1->f10);
																			$ddet = $ddet + intval($key1->d);
																		?>
																			<tr>
																				<td class="text-center"><?php echo $o; ?></td>
																				<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																				<td class="text-center"><?php echo number_format($key1->p) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f) ?></td>
																				<td class="text-center"><?php echo number_format($key1->f10) ?></td>
																				<td class="text-center"><?php echo number_format($key1->d) ?></td>
																			</tr>
																		<?php } ?>
																		<tr>
																			<td class="text-center"></td>
																			<td class="text-center">Total</td>
																			<td class="text-center"><?php echo number_format($pdet);
																									$pdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($fdet);
																									$fdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ffdet);
																									$ffdet = 0; ?></td>
																			<td class="text-center"><?php echo number_format($ddet);
																									$ddet = 0; ?></td>
																		</tr>
																	</table>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($pTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($fftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($dtotal) ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">

			<div class="row">
				<div class="text-center col-lg-12">
					<div class="row">
						<div class="col-lg-12">
							<div class="card card-outline-info" data-target="datos12">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla general de demanda de Oxigeno por ubigeo
								</div>
								<br>
								<table class="table table-hover table-sm" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%" style="background:#ddebf8">#</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Departamento</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Población</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Fallecidos</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Casos(+) semanal</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Atencion medica</th>
								
											<th scope="col" class="text-center bold" style="background:#ddebf8">Total m3/h</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;
										$pTotal = 0;
										$ftotal = 0;
										$fftotal = 0;
										$atotal = 0;
										$stotal = 0;
										$m1total = 0;
										$ctotal = 0;
										$m2total = 0;
										$dtotal = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_tabla']);

										foreach ($pipxUbigeoDet as $item) {
											$i++;
											$pTotal = $pTotal + intVal($item->poblacion);
											$ftotal = $ftotal + intVal($item->diferencia);
											$fftotal = $fftotal + intVal($item->count);
											$atotal = $atotal + intVal($item->atencion_medica);
											$stotal = $stotal + intVal($item->severo);
											$m1total = $m1total + intVal($item->m3h1);
											$ctotal = $ctotal + intVal($item->critico);
											$m2total = $m2total + intVal($item->m3h2);
											$dtotal = $dtotal + intVal($item->demanda);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo $i ?></td>
												<td scope="row" class="text-center">
													<buttom class="btn btn-link lnkAmpliar_salud" id="<?php echo ($item->coddpto) ?>" data-event="lnk_<?php echo ($item->coddpto) ?>" href="#" onclick="App.events(this);">
														<?php echo ($item->departamento) ?>
													</buttom>
												</td>
												<td scope="row" class="text-center"><?php echo number_format($item->poblacion) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->diferencia) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->atencion_medica) ?></td>
												
												<td scope="row" class="text-center"><?php echo number_format($item->demanda) ?></td>

											</tr>
											<tr data-target="lnk_<?php echo $item->coddpto ?>" style="display: none;">
												<td colspan="11">
													<div class="card">
														<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
															PROVINCIAS
														</div>
														<div class="card-body">
															<table class="table table-hover table-sm" width="100%">
																<thead>
																	<tr>
																		<th scope="col" class="text-center bold" width="4%">#</th>
																		<th scope="col" class="text-center bold">Departamento</th>
																		<th scope="col" class="text-center bold">Provincia</th>
																		<th scope="col" class="text-center bold">Población</th>
																		<th scope="col" class="text-center bold">Fallecidos</th>
																		<th scope="col" class="text-center bold">Casos(+) Semanal</th>
																		<th scope="col" class="text-center bold">Atencion medica</th>
																		
																		<th scope="col" class="text-center bold">Total m3/h</th>
																	</tr>
																</thead>
																<tbody>
																	<?php
																	$p = 0;
																	$ppTotal = 0;
																	$ffftotal = 0;
																	$ff1total = 0;
																	$aatotal = 0;
																	$sstotal = 0;
																	$m1mtotal = 0;
																	$cctotal = 0;
																	$m2mtotal = 0;
																	$ddtotal = 0;
																	$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_tabla_provincia', 'departamento' => $item->coddpto]);

																	foreach ($pipxUbigeoDet as $items) {
																		$p++;
																		$ppTotal = $ppTotal + intVal($items->poblacion);
																		$ffftotal = $ffftotal + intVal($items->diferencia);
																		$ff1total = $ff1total + intVal($items->count);
																		$aatotal = $aatotal + intVal($items->atencion_medica);
																		$sstotal = $sstotal + intVal($items->severo);
																		$m1mtotal = $m1mtotal + intVal($items->m3h1);
																		$cctotal = $cctotal + intVal($items->critico);
																		$m2mtotal = $m2mtotal + intVal($items->m3h2);
																		$ddtotal = $ddtotal + intVal($items->demanda);
																	?>
																		<tr>
																			<td scope="row" class="text-center" width="4%"><?php echo ($p) ?></td>
																			<td scope="row" class="text-center" width="4%"><?php echo ($item->departamento) ?></td>

																			<td scope="row" class="text-center">
																				<buttom class="btn btn-link lnkAmpliar_salud" id="<?php echo ($items->ubigeo) ?>" data-event="lnk_<?php echo ($items->ubigeo) ?>" href="#" onclick="App.events(this);">


																					<?php echo ($items->nomubigeo) ?>
																				</buttom>
																			</td>
																			<td scope="row" class="text-center"><?php echo number_format($items->poblacion) ?></td>
																			<td scope="row" class="text-center"><?php echo number_format($items->diferencia) ?></td>
																			<td scope="row" class="text-center"><?php echo number_format($items->count) ?></td>
																			<td scope="row" class="text-center"><?php echo number_format($items->atencion_medica) ?></td>
																			
																			<td scope="row" class="text-center"><?php echo number_format($items->demanda) ?></td>

																		</tr>
																		<tr data-target="lnk_<?php echo $items->ubigeo ?>" style="display: none;">
																			<td colspan="13">
																				<div class="card">
																					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
																						DISTRITOS
																					</div>
																					<div class="card-body">
																						<table class="table table-hover table-sm" width="100%">
																							<thead>
																								<tr>
																									<th scope="col" class="text-center bold" width="4%">#</th>
																									<th scope="col" class="text-center bold">Departamento</th>
																									<th scope="col" class="text-center bold">Provincia</th>
																									<th scope="col" class="text-center bold">Distrito</th>
																									<th scope="col" class="text-center bold">Población</th>
																									<th scope="col" class="text-center bold">Fallecidos</th>
																									<th scope="col" class="text-center bold">Casos(+) Semanal</th>
																									<th scope="col" class="text-center bold">Atencion medica</th>
																								
																									<th scope="col" class="text-center bold">Total m3/h</th>
																								</tr>
																							</thead>
																							<tbody>
																								<?php
																								$d = 0;
																								$pp3Total = 0;
																								$fff3total = 0;
																								$ff13total = 0;
																								$aa3total = 0;
																								$ss3total = 0;
																								$m1m3total = 0;
																								$cc3total = 0;
																								$m2m3total = 0;
																								$dd3total = 0;
																								$pipxUbigeoDet = dropDownList((object) ['method' => 'oxigeno_tabla_distrito', 'provincia' => $items->ubigeo]);

																								foreach ($pipxUbigeoDet as $itemss) {
																									$d++;
																									$pp3Total = $pp3Total + intVal($itemss->poblacion);
																									$fff3total = $fff3total + intVal($itemss->diferencia);
																									$ff13total = $ff13total + intVal($itemss->count);
																									$aa3total = $aa3total + intVal($itemss->atencion_medica);
																									$ss3total = $ss3total + intVal($itemss->severo);
																									$m1m3total = $m1m3total + intVal($itemss->m3h1);
																									$cc3total = $cc3total + intVal($itemss->critico);
																									$m2m3total = $m2m3total + intVal($itemss->m3h2);
																									$dd3total = $dd3total + intVal($itemss->demanda);
																								?>
																									<tr>
																										<td scope="row" class="text-center" width="4%"><?php echo ($d) ?></td>
																										<td scope="row" class="text-center" width="4%"><?php echo ($item->departamento) ?></td>
																										<td scope="row" class="text-center" width="4%"><?php echo ($items->nomubigeo) ?></td>

																										<td scope="row" class="text-center">
																											<?php echo ($itemss->nomubigeo) ?>
																										</td>
																										<td scope="row" class="text-center"><?php echo number_format($itemss->poblacion) ?></td>
																										<td scope="row" class="text-center"><?php echo number_format($itemss->diferencia) ?></td>
																										<td scope="row" class="text-center"><?php echo number_format($itemss->count) ?></td>
																										<td scope="row" class="text-center"><?php echo number_format($itemss->atencion_medica) ?></td>
																									
																										<td scope="row" class="text-center"><?php echo number_format($itemss->demanda) ?></td>

																									</tr>
																								
																								<?php
																								} ?>
																								<tr>
																									<td scope="row" class="text-center" width="4%"></td>
																									<td scope="row" class="text-center"></td>
																									<td scope="row" class="text-center"></td>
																									<td scope="row" class="text-center">Total Acumulado</td>
																									<td scope="row" class="text-center"><?php echo number_format($pp3Total) ?></td>
																									<td scope="row" class="text-center"><?php echo number_format($fff3total) ?></td>
																									<td scope="row" class="text-center"><?php echo number_format($ff13total) ?></td>
																									<td scope="row" class="text-center"><?php echo number_format($aa3total) ?></td>
																									
																									<td scope="row" class="text-center"><?php echo number_format($dd3total) ?></td>
																								</tr>
																							</tbody>
																						</table>

																						
																					</div>
																				</div>

																			</td>
																		</tr>
																	<?php
																	} ?>
																	<tr>
																		<td scope="row" class="text-center" width="4%"></td>
																		<td scope="row" class="text-center"></td>
																		<td scope="row" class="text-center">Total Acumulado</td>
																		<td scope="row" class="text-center"><?php echo number_format($ppTotal) ?></td>
																		<td scope="row" class="text-center"><?php echo number_format($ffftotal) ?></td>
																		<td scope="row" class="text-center"><?php echo number_format($ff1total) ?></td>
																		<td scope="row" class="text-center"><?php echo number_format($aatotal) ?></td>
																		
																		<td scope="row" class="text-center"><?php echo number_format($ddtotal) ?></td>
																	</tr>
																</tbody>
															</table>

														</div>
													</div>

												</td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($pTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($fftotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($atotal) ?></td>
										
											<td scope="row" class="text-center"><?php echo number_format($dtotal) ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br> -->