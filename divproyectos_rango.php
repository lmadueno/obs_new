<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>



<!-- RANGO PROGRAMAS -->

<div id="accordion">
	<div class="card">
		<div class="card-header" id="headingOne" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:#000;font-weight: bold; ">

					Estadisticas Rango IOARR
				</button>
			</h5>
		</div>

		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos IOARR por Gobierno Nacional 2017-2021*

							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total IOARR de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)

										</div>
										<div id='chartrango_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR viables de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)

										</div>
										<div id='chartrango_2'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR con ejecución de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)

										</div>
										<div id='chartrango_3'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos" style="color: #1976D2; !important;" id="datos" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Rangos por tipo de formato por Gobierno Nacional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GN', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR viable por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GN', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR en ejecución por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GN', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Regional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total IOARR de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)

										</div>
										<div id='chartrango_12'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR viables de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chartrango_22'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR con ejecución de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)

										</div>
										<div id='chartrango_32'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos1" style="color: #1976D2; !important;" id="datos1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Rangos por tipo de formato por Gobierno Regional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GR', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR viables por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GR', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR en ejecución por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GR', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Local 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total IOARR de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chartrango_13'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR viables de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chartrango_23'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											IOARR con ejecución de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chartrango_33'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos2" style="color: #1976D2; !important;" id="datos2" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Rangos por tipo de formato por Gobierno Local 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GL', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR viable por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GL', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Rangos por tipo de formato IOARR en ejecucion por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => 'IOARR', 'nivel' => 'GR', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" id="headingTwo" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:#000;font-weight: bold;">
					Estadisticas Rango Proyectos de Inversión
				</button>
			</h5>
		</div>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
			<div class="card-body">
				<!-- RANGO PROYECTOS  -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb) </div>
										<div id='chart_rango_2'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_3'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_1" style="color: #1976D2; !important;" id="datos_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Rangos por tipo de formato por Gobierno Nacional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GN', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GN', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GN', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_12'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_22'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_32'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_2" style="color: #1976D2; !important;" id="datos_2" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla de datos de rangos por tipo de formato PI por Gobierno Regional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GR', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GR', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GR', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos Proyectos de Inversión por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_13'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_23'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_33'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_3" style="color: #1976D2; !important;" id="datos_3" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla de datos de rangos por tipo de formato PI por Gobierno Local 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GL', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GL', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROYECTO", 'nivel' => 'GL', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingThree" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color:#000;font-weight: bold;">
					Estadisticas Rango Programas de Inversión
				</button>
			</h5>
		</div>
		<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_1_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb) </div>
										<div id='chart_rango_2_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Nacional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_3_1'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_1" style="color: #1976D2; !important;" id="datos_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Rangos por tipo de formato por Gobierno Nacional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROGRAMA DE INVERSION", 'nivel' => 'GN', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GN', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROGRAMA DE INVERSION", 'nivel' => 'GN', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos por tipo de formato por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_12_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_22_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Regional, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_32_1'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_2" style="color: #1976D2; !important;" id="datos_2" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla de datos de rangos por tipo de formato PI por Gobierno Regional 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GR', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' => "PROGRAMA DE INVERSION','PROYECTO", 'nivel' => 'GR', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_2" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_2" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GR', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Rangos Proyectos de Inversión por Gobierno Nacional 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Total PI de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_13_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI viables de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_23_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											PI con ejecución de Gobiernos Local, por rangos de inversión y cantidad 2017 - 2021(19feb)
										</div>
										<div id='chart_rango_33_1'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_3" style="color: #1976D2; !important;" id="datos_3" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla de datos de rangos por tipo de formato PI por Gobierno Local 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GL', 'f15' => "0','1"]);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI viable por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GL', 'f15' => '0']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_3" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de datos de rangos por tipo de formato PI en ejecución por Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_3" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Rango</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Soles</th>
															<th scope="col" class="text-center bold">%Cantidad</th>
															<th scope="col" class="text-center bold">%soles</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$sTotal = 0;
														$stotal = 0;
														$total = 0;
														$total2 = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_mef_tabla', 'tipo' =>  "PROGRAMA DE INVERSION", 'nivel' => 'GL', 'f15' => '1']);
														// echo json_encode($pipxUbigeoDet);
														$rango = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
														foreach ($pipxUbigeoDet as $item) {
															$total = $total + intval($item->count);
															$total2 = $total2 + intval($item->sum);
														}
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$sTotal = $sTotal + intval($item->count);
															$stotal = $stotal + intval($item->sum);
															$totals = $total + intval($item->count);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-center"><?php echo ($rango[$i - 1]) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->count) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->sum) ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->count / $total), 2) * 100) . '%' ?></td>
																<td scope="row" class="text-center"><?php echo (round(($item->sum / $total2), 2) * 100) . '%' ?></td>

															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>
															<td scope="row" class="text-center"><?php echo "100 %"; ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>