<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>
<div class="card">
	<div class="card-header" style="background:#304e9c">
		<h4 class="m-b-0 text-white text-center">Exceso de fallecidos SINADEF 2020-2021 vs 2019 y costo social</h4>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="alert alert-primary alert-dismissible fade show" role="alert">
					<strong>Leyenda:</strong>
					<hr>
					<ul>
						<li>
							* Costo social MEF (1 fallecido = S/ 465,784.5)
						</li>
						<li>
							Fuente: Elaboración propia en bases a SINADEF y MEF
						</li>
					</ul>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Incremento de fallecidos SINADEF 2020 - 2021 vs 2019
					</div>
					<div id='pie1'></div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Costo social de fallecidos SINADEF 2020 - 2021 vs 2019
					</div>
					<div id='pie2'></div>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="tablap" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla Exceso de fallecidos SINADEF 2020-2021 vs 2019 y costo social</b></a>
					<!-- </div> -->
				</div>
			</div>
			<table class="table table-sm table-detail" data-target="tablap" style="display: none;" width="100%">
				<thead>
					<tr>
						<th class="text-center bold" style="background:#ddebf8">Presidente</th>
						<!-- <th class="text-center bold"style="background:#ddebf8">Rango de fechas</th> -->
						<th class="text-center bold"style="background:#ddebf8">Días</th>
						<th class="text-center bold"style="background:#ddebf8">Fallecidos</th>
						<th class="text-center bold"style="background:#ddebf8">%Fallecidos</th>
						<th class="text-center bold"style="background:#ddebf8">V.Social*</th>
					</tr>
					<?php
					$i = 0;

					$sTotal = 0;
					$stotal = 0;
					$data = array();
					$d1 = 0;
					$d2 = 0;
					$d3 = 0;
					$a=0;
					$aa= 465784.5;
					$pipxUbigeoDet = dropDownList((object) ['method' => 'pie_salud']);
					foreach ($pipxUbigeoDet as $item) {
						array_push($data, intval($item->count));
					}
					// echo json_encode($data);
					$d1 = $data[1] - $data[0];
					$d2 = $data[3] - $data[2];
					$d3 = ($data[7]+$data[6]) - ($data[5]+$data[4])+9308;
					$a=$d1+$d2+$d3;

					?>
					<tr>
						<td class="text-center">Martín Vizcarra (19mar2020-9nov2020, 236 días)</td>
						
						<td class="text-center">236</td>
						<td class="text-center"><?php echo number_format($d1) ?></td>
						<td class="text-center"><?php echo number_format(($d1/$a)*100) ."%" ?></td>
						<td class="text-center"><?php echo number_format($d1*$aa) ?></td>

					</tr>
					<tr>
						<td class="text-center">Manuel Merino (10nov2020-16nov2020, 7 días)</td>
					
						<td class="text-center">7</td>
						<td class="text-center"><?php echo number_format($d2) ?></td>
						<td class="text-center"><?php echo number_format(($d2/$a)*100) ."%" ?></td>
						<td class="text-center"><?php echo number_format($d2*$aa) ?></td>

					</tr>
					<tr>
						<td class="text-center">Francisco Sagasti (17nov2020-11abril2021, 146 días)</td>
					
						<td class="text-center">146</td>
						<td class="text-center"><?php echo number_format($d3) ?></td>
						<td class="text-center"><?php echo number_format(($d3/$a)*100 ) ."%" ?></td>
						<td class="text-center"><?php echo number_format($d3*$aa) ?></td>

					</tr>
					<tr>
						<!-- <td class="text-center"></td> -->
						<td class="text-center">Total</td>
						<td class="text-center">389</td>
						<td class="text-center"><?php echo number_format($a) ?></td>
						<td class="text-center"><?php echo "100%" ?></td>
						<td class="text-center"><?php echo number_format($a*$aa);?></td>

					</tr>
				</thead>
			</table>
			<br>

		</div>
	</div>
</div>