<?php
require 'php/app.php';
$data = json_decode($_GET['data']);
$titulo = '';
if ($data->titulo == 'MINSA') {
	$titulo = ' fallecidos COVID MINSA';
} else if ($data->titulo == 'SINADEF') {
	$titulo = ' fallecidos SINADEF MINSA';
} else if ($data->titulo == 'PROYECTADO') {
	$titulo = ' fallecidos COVID comparativo anual y proyectado';
} else {
	$titulo = $data->titulo;
}

?>
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header card-special text-center font-weight-bold">
				Información y estadísticas de <?php echo $titulo; ?>
			</div>
			<div class="card-body">
				<?php
				if ($data->titulo == 'MINSA') {
				?>

					<ul class="nav nav-tabs" id="myTab" role="tablist">

						<li class="nav-item">
							<a class="nav-link active tabSubEstadistica" style="color: #1976D2; !important;" id="resumen-tab" href="#" onclick="App.events(this)" title="Resumen"><b style="padding-left:0.5em;">Resumen</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link active tabSubEstadistica" style="color: black; !important;" id="fallecidos-tab" href="#" onclick="App.events(this)" title="Fallecidos"><b style="padding-left:0.5em;">Fallecidos</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link  tabSubEstadistica" id="fallecidospob-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Fallecidos/Poblacion por mil habitantes"><b style="padding-left:0.5em;">Fallecidos/pob</b></a>
						</li>
					</ul>

				<?php
				} else if ($data->titulo == 'SINADEF') {
				?>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active tabSubEstadisticaSina" style="color: #1976D2; !important;" id="resumen-tab" href="#" onclick="App.events(this)" title="Resumen"><b style="padding-left:0.5em;">Resumen</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link active tabSubEstadisticaSina" style="color: black; !important;" id="fallecidos-tab" href="#" onclick="App.events(this)" title="Fallecidos"><b style="padding-left:0.5em;">Fallecidos</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link  tabSubEstadisticaSina" id="fallecidospob-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Fallecidos/Poblacion por mil habitantes"><b style="padding-left:0.5em;">Fallecidos/pob</b></a>
						</li>
					</ul>

				<?php
				} else if ($data->titulo == 'CIPRL') {
				?>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active tabSubEstadisticaCIPRL" style="color: #1976D2; !important;" id="gobiernos-tab" href="#" onclick="App.events(this)" title="Analisis de Ciprl por años"><b style="padding-left:0.5em;">GR/GL</b></a>
						</li>
						<li class="nav-item">
							<a class="nav-link  tabSubEstadisticaCIPRL" id="universidades-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Analisis de Canon por años"><b style="padding-left:0.5em;">Universidades</b></a>
						</li>
					</ul>
				<?php
				}
				?>
				<?php
				if ($data->titulo == 'CIPRL' || $data->titulo == 'CANON') {
				?>
					<div class="row">
						<div class="col-lg-12">
							<div id="chart1" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
						</div>
						<div class="col-lg-12" id="mostarchar2" style="display:none;text-align:center">
							<div id="chart_2" data-target="lnkProvXrutas1" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>
							<a class="lnkAmpliar" data-event="lnkProvXrutas1" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar grafico de Provincias
							</a>
						</div>
						<div class="col-lg-12" id="mostarchar3" style="display:none;text-align:center">
							<div id="chart_3" data-target="lnkProvXrutas2" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>
							<a class="lnkAmpliar" data-event="lnkProvXrutas2" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar grafico de Distritos
							</a>
						</div>
					</div>
				<?php
				} else  if ($data->titulo == 'SINADEF' || $data->titulo == 'MINSA') {

				?>
					<div id="divresumeninformacion123"></div>
				<?php
				} else  if ($data->titulo == 'PROYECTADO') {
				?>

					<div class="row">
						<div class="col-lg 6">
							<div id="chart2" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
						</div>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-12">
									<table class="table table-sm table-detail" width="100%">
										<tr>
											<th class="text-center bold">Año</th>
											<th class="text-center bold">Fallecidos</th>
											<th class="text-center bold">Exceso</th>
											<th class="text-center bold">Total</th>
										</tr>
										<?php

										$item = dropDownList((object) ['method' => 'proyectados_fallecidos_anio']);
										?>

										<tr>
											<td class="text-center">Año 2018</td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2018) ?></td>
											<td class="text-center">0</td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2018) ?></td>
										</tr>
										<tr>
											<td class="text-center">Año 2019</td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2019) ?></td>
											<td class="text-center">0</td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2019) ?></td>
										</tr>
										<tr>
											<td class="text-center">Año 2020</td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2019) ?></td>
											<td class="text-center"><?php echo number_format($item{
																		0}->diferencia) ?></td>
											<td class="text-center"><?php echo number_format($item{
																		0}->a2019 + $item{
																		0}->diferencia) ?></td>
										</tr>
									</table>
								</div>
								<div class="col-lg-12" style="text-align:center;">
									Comparativo anual de fallecidos COVID durante el periodo pandemia 18 de Marzo al 4 de Octubre, del año 2017 al año 2020 considerandO la informacion oficial SINADEF. Se considera para el 2020 el promedio de los ultimos 3 años para el periodo indicado.
								</div>
							</div>



						</div>
						<div class="col-lg-12">
							<div id="chart1" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
						</div>
						<div class="col-lg-12" style="text-align:center;">
							COLABORACCION (28 septiembre): Comparativo de Fallecidos en PERU en pandemia COVID19, promedio diario de Fallecidos semanal y acumulado SINADEF (2020 menos 2019) vs COVID MINSA, proyectado de Setiembre a Diciembre 2020, con datos oficiales MINSA "actualizados" del 18 de marzo al 27 de septiembre (procesados por día).
						</div>
					</div>
				<?php
				} else  if ($data->titulo == 'EMPRESAS') {
				?>
					<div class="row">
						<div class="col-lg-12">
							<select id="ddlEstadisticaEmpresa" class="form-control form-control-sm ddlEstadisticaEmpresa" onclick="App.events(this); return false;">
								<option value="0"><b>Seleccione Una empresa</b></option>
								<option value="1"><b>IBT HEALTH SOCIEDAD ANONIMA CERRADA - IBT HEALTH S.A.C.</b></option>
								<option value="2"><b>IBT, LLC SUCURSAL DEL PERU</b></option>
								<option value="3"><b>VILLA MARIA DEL TRIUNFO SALUD S.A.C</b></option>
								<option value="4"><b>CALLAO SALUD S.A.C</b></option>
							</select>
						</div>
					</div>

				<?php
				}
				?>
			</div>
		</div>
	</div>
</div>
<?php
if ($data->titulo == 'CANON') {
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Consolidado anual de <?php echo $data->titulo; ?> por Región (millones de soles)
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col">
							<table class="table table-sm table-detail" width="100%">
								<thead>
									<tr>
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold" width="1%"></th>
										<th class="text-center bold" width="15%">Región</th>
										<th class="text-center bold" width="20%"> 2018</th>
										<th class="text-center bold" width="20%"> 2019</th>
										<th class="text-center bold" width="20%"> 2020</th>
										<th class="text-center bold" width="20%">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									$s2018 = 0;
									$s2019 = 0;
									$s2020 = 0;
									$stotal = 0;
									$pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacion', 'tipo' => $data->titulo]);
									foreach ($pipxUbigeoDet as $item) {
										$i++;
										$s2018 =   $s2018 + $item->s2018;
										$s2019 =   $s2019 + $item->s2019;
										$s2020 =   $s2020 + $item->s2020;
										$stotal =   $stotal + $item->total;
									?>
										<tr>
											<td class="text-center"><?php echo $i; ?></td>
											<td class="text-center" title="Ficha Entidad">
												<a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo; ?>" href="#" onclick="App.events(this); return false;">
													<i class="fas fa-file-contract"></i>
												</a>
											</td>
											<td class="text-left">
												<a class="lnkAmpliarInfoResumenP" id="<?php echo $item->ubigeo; ?>" data-event="lnkProvXrutas_<?php echo $i ?>" href="#" onclick="App.events(this); return false;">
													<?php echo $item->departamento; ?>
												</a>
											</td>
											<td class="text-right" title="<?php echo number_format($item->s2018) ?>"><?php echo number_format(round($item->s2018 / 1000000, 1), 2) ?></td>
											<td class="text-right" title="<?php echo number_format($item->s2019) ?>"><?php echo number_format(round($item->s2019 / 1000000, 1), 2) ?></td>
											<td class="text-right" title="<?php echo number_format($item->s2020) ?>"><?php echo number_format(round($item->s2020 / 1000000, 1), 2) ?></td>
											<td class="text-right" title="<?php echo number_format($item->total) ?>"><?php echo number_format(round(($item->total) / 1000000, 1), 2) ?></td>

										</tr>
										<tr data-target="lnkProvXrutas_<?php echo $i ?>" style="display: none;">
											<td colspan="8">
												<div id="divinformacionProvincia_<?php echo $item->ubigeo; ?>">
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td></td>
										<td></td>
										<td>Total</td>
										<td class="text-right"><?php echo number_format(round($s2018 / 1000000, 1), 2) ?></td>
										<td class="text-right"><?php echo number_format(round($s2019 / 1000000, 1), 2) ?></td>
										<td class="text-right"><?php echo number_format(round($s2020 / 1000000, 1)) ?></td>
										<td class="text-right"><?php echo number_format(round(($stotal) / 1000000, 1), 2) ?></td>

									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
} else if ($data->titulo == 'MINSA') {
?>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Consolidado anual de <?php echo $data->titulo; ?> por Región
				</div>
				<div class="card-body">
					<div id="getResumenSalud"></div>
				</div>
			</div>
		</div>
	</div>
<?php
} else if ($data->titulo == 'CIPRL') {
?>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Consolidado anual de <?php echo $data->titulo; ?> por Región (millones de soles)
				</div>
				<div class="card-body">
					<div id="getResumenSalud"></div>
				</div>
			</div>
		</div>
	</div>
<?php
} else if ($data->titulo == 'SINADEF') {
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Consolidado anual de <?php echo $data->titulo; ?> por Región
				</div>
				<div class="card-body">
					<div id="getResumenSalud"></div>
				</div>
			</div>
		</div>
	</div>
<?php
} else if ($data->titulo == 'PROYECTADO') {
?>
	<br>
	<!-- <div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Información avance, proyeccion de COVID y SINADEF
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-6">
									<table class="table table-sm table-detail" width="100%">
										<thead>
											<tr>
												<th class="text-center bold">#</th>
												<th class="text-center bold">Mes</th>
												<th class="text-center bold">Año 2020</th>
												<th class="text-center bold">Año 2021</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$i = 0;
											$s2020 = 0;
											$s2021 = 0;
											$stotal1 = 0;
											$stotal2 = 0;
											$proyectado_covid = dropDownList((object) ['method' => 'proyectado_covid_mensual']);
											foreach ($proyectado_covid as $item) {
												$i++;
												$s2020 =   intval($s2020) + intval($item->anio2020);
												$s2021 =   intval($s2021) + intval($item->anio2021);
												$stotal1 =  intval($stotal1) + intval($item->anio2020);
												$stotal2 =  intval($stotal2) + intval($item->anio2021);

											?>
												<td class="text-center"><?php echo $i; ?></td>
												<td class="text-center"><?php echo ($item->mes) ?></td>
												<td class="text-center"><?php echo $item->anio2020; ?></td>
												<td class="text-center"><?php echo $item->anio2021; ?></td>

												</tr>

											<?php } ?>
											<tr>
												<td></td>
												<td class="text-center">Total</td>
												<td class="text-center"><?php echo $stotal1 ?></td>
												<td class="text-center"><?php echo $stotal2 ?></td>


											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-lg-6">
									<div id="chartmensual" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
								</div>

							</div>
						</div>
						<div class="col-lg-6">
							<div class="row">
								<div class="col-lg-6">

								</div>
								<div class="col-lg-6">

								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div> -->
	<br>
	<div class="row">

		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Consolidado de fallecidos Proyectados
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col">
							<table class="table table-sm table-detail" width="100%">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Periodo</th>
										<th class="text-center bold">Prom. diario (semanal COVID)</th>
										<th class="text-center bold">Proyección semanal COVID</th>
										<th class="text-center bold">Prom. diario (semanal SINADEF)</th>
										<th class="text-center bold">Proyección semanal SINADEF</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$i = 0;
									$s2018 = 0;
									$s2019 = 0;
									$s2020 = 0;
									$stotal = 0;
									$pipxUbigeoDet = dropDownList((object) ['method' => 'proyectado_fallecidos']);
									foreach ($pipxUbigeoDet as $item) {
										$i++;
										$s2018 =   $s2018 + $item->covid_promedio;
										$s2019 =   $s2019 + $item->covid_proyectado;
										$s2020 =   $s2020 + $item->sinadef_promedio;
										$stotal =   $stotal + $item->sinadef_proyectado;
									?>
										<td class="text-center"><?php echo $i; ?></td>
										<td class="text-center"><?php echo ($item->fecha) ?></td>
										<td class="text-center"><?php echo number_format($item->covid_promedio) ?></td>
										<td class="text-center"><?php echo number_format($item->covid_proyectado) ?></td>
										<td class="text-center"><?php echo number_format($item->sinadef_promedio) ?></td>
										<td class="text-center"><?php echo number_format($item->sinadef_proyectado) ?></td>

										</tr>

									<?php } ?>
									<tr>
										<td></td>
										<td>Total</td>
										<td class="text-center"><?php echo number_format($s2018) ?></td>
										<td class="text-center"><?php echo number_format($s2019) ?></td>
										<td class="text-center"><?php echo number_format($s2020) ?></td>
										<td class="text-center"><?php echo number_format($stotal) ?></td>

									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
} else if ($data->titulo == 'EMPRESAS') {
?>
	<div id="getDivEmpresasEstadistica"></div>
<?php
} else if ($data->titulo == 'MACROINDICADORES') {
?>
	<div class="scrollable" style="overflow-y: auto;max-height: 450px;">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-special text-center font-weight-bold">
						Consolidado de Top 25 Empresas disponibilidad Oxi
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col">
								<table class="table table-sm table-detail" width="100%">
									<thead>
										<tr>
											<th class="text-center bold">#</th>
											<th class="text-center bold">RUC</th>
											<th class="text-center bold">Razón Social</th>
											<th class="text-center bold">Utilidad</th>
											<th class="text-center bold">Disponibilidad OxI</th>
											<th class="text-center bold">Participacion</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$s2018 =  0;
										$s2019 =  0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'macro_top']);
										foreach ($pipxUbigeoDet as $item) {

											$s2018 =   $s2018 + $item->utilidad;
											$s2019 =   $s2019 + $item->oxi;

										?>
											<tr>
												<td class="text-center"><?php echo $item->numeracion; ?></td>
												<td class="text-center"><?php echo $item->ruc; ?></td>
												<td class="text-left"><?php echo $item->razon_social; ?></td>
												<td class="text-center"><?php echo number_format($item->utilidad) ?></td>
												<td class="text-center"><?php echo number_format($item->oxi) ?></td>
												<td class="text-center"><?php echo $item->participa; ?></td>

											</tr>

										<?php } ?>
										<tr>

											<td></td>
											<td class="text-center">Total</td>
											<td class="text-center"></td>
											<td class="text-center"><?php echo number_format($s2018) ?></td>
											<td class="text-center"><?php echo number_format($s2019) ?></td>
											<td class="text-center"></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Inversiones Solicitadas por Región
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8">
							<div id="getDivEmpresasEstadistica"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Inversiones Solicitadas por Función
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-8">
							<div id="getDivEmpresasEstadistica1"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php
}
?>