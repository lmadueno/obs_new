<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<style>
	.highcharts-title {
    	fill: #434348;
    	font-weight: bold;
    	font-size: 12px !important;
	}
	.bold{
		font-weight: bold;
		background: #1976d226;
		color: #000;
	}
	.table thead th {
		vertical-align: bottom;
		border-bottom: 2px solid #fefeff;
	}
</style>
<input type="hidden" value="<?php echo  $data->id  ?>" id="txtLFid">
<div class="container"  width= "100%"!important  height="100%"!important>
    <div class="card card-outline-info">
        <div class="card-header">
		    <div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white"><?php echo $data->nombre?></h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOGlineaFerrea" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOGlineaFerrea" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card-special text-center font-weight-bold">
                                Analisis por Región de CANON, CIPRL y PIM del <?php echo ucwords(strtolower($data->nombre))?>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="chart_distXlineaFerrea"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>	
                                </div>
                                <div class="col-lg-12" id="chart_distXlineaFerrea12" style="display:none;text-align:center">
                                    <div id="chart_distXlineaFerrea1"  data-target="lnkProvXrutas1" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>	
                                    <a class="lnkAmpliar" data-event="lnkProvXrutas1" href="#" onclick="App.events(this); return false;">
                                    Mostrar/Ocultar grafico de distritos por tramo
                                    </a>
                                </div>
                            </div>														
                        </div>
                        <div class="card-footer text-center">
                            <a class="lnkAmpliar" data-event="lnkProvXrutas" href="#" onclick="App.events(this); return false;">
                                Mostrar/Ocultar tabla resumen
                            </a>
                        </div>
                        <div data-target="lnkProvXrutas" class="card-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-sm table-detail" width="100%">
                                        <thead>
                                            <tr>
                                                
                                                <th class="text-center bold" width="70%">Región</th>
                                                <th class="text-center bold" width="7%">Gobiernos locales</th>
												<th class="text-center bold" width="7%">COVID</th>
												<th class="text-center bold" width="7%">SINADEF</th>
                                                <th class="text-center bold" width="7%">Población</th>
                                                <th class="text-center bold" width="7%">Canon</th>
                                                <th class="text-center bold" width="7%">CIPRL</th>
                                                <th class="text-center bold" width="8%">PIM</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $poblacion  = 0;
                                                $distritos  = 0;
												$covid = 0;
												$sinadef = 0;
                                                $canon      = 0;
                                                $ciprl      = 0;
                                                $pim        = 0;
                                                $i 			= 0;
                                                $pipxUbigeoDet = dropDownList((object) ['method' => 'lineaFerreaTabla','nam'=>$data->nombre]);
                                                foreach ($pipxUbigeoDet as $item){
                                                    $i++;
                                                    $poblacion  = $poblacion + $item->valor;
                                                    $distritos  = $distritos + $item->cantidad;
													$covid = $covid + $item->covid;
													$sinadef = $sinadef + $item->sinadef;
													$canon      = $canon + $item->canon;
                                                    $canon      = $canon + $item->canon;
                                                    $ciprl      = $ciprl + $item->ciprl;
                                                    $pim        = $pim + $item->pim;												
                                            ?>	
                                                                                        
                                                <tr>
                                                    <td ><?php echo $item->nombdep;?></td>
                                                    <td class="text-right">
                                                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $i?>" href="#" onclick="App.events(this); return false;">
                                                            <?php echo $item->cantidad;?>
                                                        </a>													
                                                    </td>
													<td class="text-right" title="<?php echo number_format($item->covid)?>"><?php echo round($item->covid)?></td>	
													<td class="text-right" title="<?php echo number_format($item->sinadef)?>"><?php echo round($item->sinadef)?></td>	
                                                    <td class="text-right" title="<?php echo number_format($item->valor)?>"><?php echo round($item->valor/1000, 1)?>K</td>												
                                                    <td class="text-right" title="<?php echo number_format($item->canon)?>"><?php echo round($item->canon/1000000, 1)?>M</td>
                                                    <td class="text-right" title="<?php echo number_format($item->ciprl)?>"><?php echo round($item->ciprl/1000000, 1)?>M</td>
                                                    <td class="text-right" title="<?php echo number_format($item->pim)?>"><?php echo round($item->pim/1000000, 1)?>M</td>						
                                                </tr>
                                                
                                                <tr data-target="lnkProvXrutas_<?php echo $i?>" style="display: none;">
                                                    <td colspan="8">
                                                        <div class="card">
                                                            <div class="card-header card-special">
                                                                Distritos
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <table class="table table-sm table-detail">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="text-center bold" width="15%">Distrito</th>
                                                                                    <th class="text-center bold" width="35%">Partido Político</th>
																					<th class="text-center bold" width="7%">COVID</th>
																					<th class="text-center bold" width="7%">SINADEF</th>																					
                                                                                    <th class="text-center bold" width="10%">Población</th>
                                                                                    <th class="text-center bold" width="10%">Electores</th>
                                                                                    <th class="text-center bold" width="10%">Canon</th>
                                                                                    <th class="text-center bold" width="10%">CIPRL</th>
                                                                                    <th class="text-center bold" width="10%">PIA</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php																				
                                                                                    $distXoleoducto = dropDownList((object) ['method' => 'lineaFerreaTablaDistrito', 'region' => $item->nombdep,'nam'=>$data->nombre]);																				
																					$covidd     = 0;
																					$sinadefd   = 0;
                                                                                    $poblaciond = 0;
                                                                                    $electoresd = 0;
                                                                                    $canond     = 0;
                                                                                    $ciprld     = 0;
                                                                                    $piad       = 0;
                                                                                    
                                                                                    foreach ($distXoleoducto as $item){	
																						$covidd = $covidd + $item->covid;
																						$sinadefd = $sinadefd + $item->sinadef;																																						
                                                                                        $poblaciond  = $poblaciond + $item->valor;
                                                                                        $electoresd  = $electoresd + $item->electores;
                                                                                        $canond = $canond + $item->canon;
                                                                                        $ciprld = $ciprld + $item->ciprl;
                                                                                        $piad   = $piad + $item->pim;
                                                                                ?>
                                                                                <tr>
                                                                                                            
                                                                                    <td><a href="" class="lnkAmpliarEntidad" id="<?php echo $item->iddist?>" onclick="App.events(this); return false;"><?php echo ucwords(strtolower($item->distrito))?></a></td>
                                                                                    <td><input type="text" style="width: 100%" value="<?php echo ucwords(strtolower($item->partido))?>" readonly /></td>	
																					<td class="text-right" title="<?php echo number_format($item->covid)?>"><?php echo round($item->covid)?></td>	
																					<td class="text-right" title="<?php echo number_format($item->sinadef)?>"><?php echo round($item->sinadef)?></td>	
                                                                                    <td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->valor/1000, 1)?>K</td>
                                                                                    <td class="text-right" title="<?php echo number_format($item->electores)?>"><?php echo round($item->electores/1000, 1)?>K</td>
                                                                                    <td class="text-right" title="<?php echo number_format($item->canon)?>"><?php echo round($item->canon/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($item->ciprl)?>"><?php echo round($item->ciprl/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($item->pim)?>"><?php echo round($item->pim/1000000, 1)?>M</td>						
                                                                                </tr>
                                                                                <?php } ?>
                                                                                <tr>
                                                                                    
                                                                                    <td><b>Total</b></td>	
                                                                                    <td></td>	
																					<td class="text-right"><?php echo $covidd?></td>
																					<td class="text-right"><?php echo $sinadefd?></td>																					
                                                                                    <td class="text-right" title="<?php echo number_format($poblaciond)?>"><?php echo round($poblaciond/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($electoresd)?>"><?php echo round($electoresd/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($canond)?>"><?php echo round($canond/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($ciprld)?>"><?php echo round($ciprld/1000000, 1)?>M</td>
                                                                                    <td class="text-right" title="<?php echo number_format($piad)?>"><?php echo round($piad/1000000, 1)?>M</td>						
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>														
                                                            </div>
                                                        </div>													
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                            
                                                <td><b>Total</b></td>												
                                                <td class="text-right"><?php echo $distritos?></td>
												<td class="text-right"><?php echo $covid?></td>
												<td class="text-right"><?php echo $sinadef?></td>
                                                <td class="text-right" title="<?php echo number_format($poblacion)?>"><?php echo round($poblacion/1000000, 1)?>M</td>
                                                <td class="text-right" title="<?php echo number_format($canon)?>"><?php echo round($canon/1000000, 1)?>M</td>
                                                <td class="text-right" title="<?php echo number_format($ciprl)?>"><?php echo round($ciprl/1000000, 1)?>M</td>
                                                <td class="text-right" title="<?php echo number_format($pim)?>"><?php echo round($pim/1000000, 1)?>M</td>												
                                                                                
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>									
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">  
                    <div class="card">
                        <div class="card-header card-special text-center font-weight-bold">
                            Consolidado de Proyectos de Inversión según los niveles de Gobierno	
                        </div>
					    <div class="card-body">
						
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link ferreaFuncionTabla" id="tabuniversidades-funcionferrea"  href="#" onclick="App.events(this)" title="Municipalidades Distritales"><i class="fas fa-university"></i><b style="padding-left:0.5em;">Universidades</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active ferreaFuncionTabla" id="tabministerio-funcionferrea"  href="#" onclick="App.events(this)" title="Gobiernos Nacionales"><i class="fa fa-hospital"></i><b style="padding-left:0.5em;">G. Nacional</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link ferreaFuncionTabla" id="tabregiones-funcionferrea"  href="#" onclick="App.events(this)" title="Gobiernos Regionales"><i class="fa fa-building"></i><b style="padding-left:0.5em;">G. Regional</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link ferreaFuncionTabla" id="tabprovincias-funcionferrea"  href="#" onclick="App.events(this)" title="Municipalidades Provinciales"><i class="fa fa-place-of-worship"></i><b style="padding-left:0.5em;">M. Provincial</b></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link ferreaFuncionTabla" id="tabdistritos-funcionferrea"  href="#" onclick="App.events(this)" title="Municipalidades Distritales"><i class="fas fa-home"></i><b style="padding-left:0.5em;">M. Distrital</b></a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade " id="universidades-funcionferrea">									
                                    <div id="divpipUniversidadesferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>                                         
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="ministerio-funcionferrea">
                                    <div id="divpipMinisteriosferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="regiones-funcionferrea">
                                    <div id="divpipRegionesferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade " id="provincias-funcionferrea">
                                    <div id="divpipProvinciasferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade" id="distritos-funcionferrea">
                                    <div id="divpipDistritasferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>     
                                    </div>
                                </div>
                            </div>															
					    </div>															
				    </div>                                                                  
                </div>                                                                     
            </div>
            <div class="row">
                <div class="col-lg-12">   
                    <div class="card">     
                        <div class="card-header card-special text-center font-weight-bold">
                            Consolidado de Comisarías, Est. Salud y Ins. Educativas por Distrito
                        </div>
                        <div class="card-body">
                             <div class="row">
                                <div class="col-lg-12">    
                                    <div id="divinfraObjetosferrea">
                                        <div class="row justify-content-center h-100">
                                            <div class="col-sm-8 align-self-center text-center">
                                                <img src="assets/app/img/loading.gif" alt="">    
                                            </div>
                                        </div>                                         
                                    </div>                                              
                                </div>                                                     
                             </div>                                                           
                        </div>  
                        <div class="card-footer">
							<div style="text-align:center">
								Los datos de CIPRL, Población, Comisarias, Establecimientos de Salud y Centros Educativos mostrados en el tabla pertenecen solo a los distritos que recorre <?php echo $data->nombre?>		
							</div>			
					    </div>                                                        
                    </div>                                                             
                </div>                                                                     
            </div>
        </div>
    </div>
</div>
