<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold" >
											<div class="row">
												<div class="col-lg-8" style="padding-top:1em;">
													Función
												</div>
												<div class="col-lg-4">
													<div class="row">
														<div class="col-lg-12">
														   <a href="#" class="macroKeysFuncion" id="funcion_asc" onclick="App.events(this);" >
																<i class="fas fa-angle-up"></i>
															</a>
															
														</div>
														<div class="col-lg-12">
														   <a href="#" class="macroKeysFuncion" id="funcion_desc" onclick="App.events(this);" >
																<i class="fas fa-angle-down"></i>
															</a>
													        
														</div>
													</div>
												</div>
											</div>
										</th>
										<th class="text-center bold" >
											<div class="row">
												<div class="col-lg-8" style="padding-top:1em;">
													Cantidad
												</div>
												<div class="col-lg-4">
													<div class="row">
														<div class="col-lg-12">
															 <a href="#" class="macroKeysFuncion" id="cantidad_asc" onclick="App.events(this);" >
																<i class="fas fa-angle-up"></i>
															</a>
														</div>
														<div class="col-lg-12">
													        <a href="#" class="macroKeysFuncion" id="cantidad_desc" onclick="App.events(this);" >
																<i class="fas fa-angle-down"></i>
															</a>
														</div>
													</div>
												</div>
											</div>
										
										</th>
										<th class="text-center bold" >
										     <div class="row">
												<div class="col-lg-8" style="padding-top:1em;">
													Costo Total
												</div>
												<div class="col-lg-4">
													<div class="row">
														<div class="col-lg-12">
															 <a href="#" class="macroKeysFuncion" id="monto_asc" onclick="App.events(this);" >
																<i class="fas fa-angle-up"></i>
															</a>
														</div>
														<div class="col-lg-12">
													        <a href="#" class="macroKeysFuncion" id="monto_desc" onclick="App.events(this);" >
																<i class="fas fa-angle-down"></i>
															</a>
														</div>
													</div>
												</div>
											</div></th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Avance Ejecución</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Monto Pendiente</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'macro_mef_Funcion','tipo'=>$data->tipo,'forma'=>$data->forma]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->cantidad ;	
										$s2019=   $s2019+$item->total ;	
										$s2020=   $s2020+$item->diferencia ;	
										$stotal=   $stotal+$item->resto ;												
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" ><?php echo ($item->funcion_)?></td>												
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->total)?></td>		
										<td class="text-center" ><?php echo number_format($item->diferencia)?></td>		
										<td class="text-center" ><?php echo number_format($item->resto)?></td>	
                                    </tr>
									
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
                                        <td class="text-center" ><?php echo number_format($s2018)?></td>												
                                        <td class="text-center"><?php echo number_format($s2019)?></td>
                                        <td class="text-center"><?php echo number_format($s2020)?></td>
										<td class="text-center"><?php echo number_format($stotal)?></td>

                                    </tr>
								</tbody>
							</table>