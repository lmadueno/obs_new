<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-10">
                    <img src="assets/app/img/logo_lateral.png" alt="" width="185">
                </div>
                <div class="col-lg-2" style="text-align:right">
                    <a href="#" class="closeSidebar" onclick="App.events(this);" >
                        <img src="assets/app/img/left.jpg" alt="" width="30" height="30" style="margin-top:1em" class="float-right">
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h6 class="m-b-0 text-white">Banco de Información</h6>		
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-sm">
                                <tr>
                                    <td class="align-middle bold"> Información</td>
                                    <td class="align-middle bold"> Fecha</td>
                                    <td class="align-middle bold"> Fuente</td>
                                </tr>
                                <tr>
                                    <td> Proyectos</td>
                                    <td> 20-Abr-21</td>
                                    <td>
                                        <a href="https://www.mef.gob.pe/es/aplicativos-invierte-pe" target="_blank" >
                                        MEF
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> CIPRL</td>
                                    <td> 31-Dic-20</td>
                                    <td>
                                        <a href="https://www.obrasporimpuestos.pe/" target="_blank" >
                                        PROINVERSION
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> CANON</td>
                                    <td> 31-Dic-20</td>
                                    <td>
                                        <a href="https://www.mef.gob.pe/es/seguimiento-de-la-ejecucion-presupuestal-consulta-amigable" target="_blank" >
                                        MEF
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> OXI</td>
                                    <td> 31-Dic-20</td>
                                    <td>
                                        <a href="https://www.obrasporimpuestos.pe/" target="_blank" >
                                        PROINVERSION
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Población</td>
                                    <td> 31-Dic-17</td>
                                    <td>
                                        <a href="http://censos2017.inei.gob.pe/redatam/" target="_blank" >
                                        INEI Censo 2017
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Empresas</td>
                                    <td> 31-Ago-20</td>
                                    <td>
                                        <a href="http://www.toponlineapp.com/toponline/index.php?r=bases/completavip" target="_blank" >
                                            PERU TOP PUBLICATIONS
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> COVID-19</td>
                                    <td> <div id="fecha_covid"></div> </td>
                                    <td>
                                        <a href="https://www.datosabiertos.gob.pe/dataset/fallecidos-por-covid-19-ministerio-de-salud-minsa" target="_blank" >
                                        MINSA Datos Abiertos
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> SINADEF</td>
                                    <td> <div id="fecha_sinadef"></td>
                                    <td>
                                        <a href="https://cloud.minsa.gob.pe/s/NctBnHXDnocgWAg" target="_blank" >
                                            MINSA SINADEF
                                        </a>
                                    </td>
                                </tr> 
                                <tr>
                                    <td> Vacunas Covid</td>
                                    <td> <div id="fecha_vacunas"></div> </td>
                                    <td>
                                        <a href="https://www.datosabiertos.gob.pe/dataset/vacunaci%C3%B3n-contra-covid-19-ministerio-de-salud-minsa-0" target="_blank" >
                                        MINSA Datos Abiertos
                                        </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td> Demanda Oxigeno</td>
                                    <td> <div id="fecha_oxigeno"></td>
                                    <td>
                                        <a href="https://www.datosabiertos.gob.pe/dataset/casos-positivos-por-covid-19-ministerio-de-salud-minsa" target="_blank" >
                                        MINSA Datos Abiertos
                                        </a>
                                    </td>
                                </tr> 
                            </table>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>