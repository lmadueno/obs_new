<?php
	require 'php/app.php';         
?>
<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<div class="container"  width= "100%"!important  height="100%"!important>
	<div class="card card-outline-info" >
		<div class="card-header">
			<div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white">RESUMEN DE INFORMACION</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarResumen" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarResumen" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
			</div>
        </div>
		
		<div class="row">
			<div class="col-lg-12">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
			        <li class="nav-item">
					    <a class="nav-link active tabChangeOxi" id="empresas-tab" style="color: #1976D2; !important;"  href="#" onclick="App.events(this)" title="Empresas OxI"><i class="fa fa-city"></i><b style="padding-left:0.5em;">Empresas</b></a>
				    </li>
				    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="regiones-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Gobiernos Regionales OxI"><i class="fa fa-building"></i><b style="padding-left:0.5em;">Regiones</b></a>
                    </li>
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="provincias-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Municipalidades Provinciales OxI"><i class="fa fa-place-of-worship"></i><b style="padding-left:0.5em;">Provincias</b></a>
				    </li>
				    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="distritos-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Municipalidades Distritales OxI"><i class="fa fa-home"></i><b style="padding-left:0.5em;">Distritos</b></a>
                    </li>
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="ministerios-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Ministerios OxI"><i class="fa fa-hospital"></i><b style="padding-left:0.5em;">Ministerios</b></a>
                    </li>
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="universidades-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Universidades Nacionales OxI"><i class="fa fa-university"></i><b style="padding-left:0.5em;">Universidades</b></a>
                    </li>
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="politica-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Congresistas por Región"><i class="fa fa-balance-scale"></i><b style="padding-left:0.5em;">Politica</b></a>
                    </li>  
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="proyecto-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Consolidado de Proyectos"><i class="fa fa-gavel"></i><b style="padding-left:0.5em;">Proyecto</b></a>
                    </li>
                    <li class="nav-item">
					    <a class="nav-link tabChangeOxi" id="oportunidad-tab" style="color: black; !important;" href="#" onclick="App.events(this)" title="Oportunidad de proyectos viables"><i class="fa fa-podcast"></i><b style="padding-left:0.5em;">Oportunidades</b></a>
                    </li>    
                </ul>
				<div class="tab-content">
					<div class="tab-pane fade show active">
						<div id="divresumenoxi"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>