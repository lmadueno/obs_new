<?php
ini_set("memory_limit","16M");
// CREATE PHPSPREADSHEET OBJECT
require 'php/spreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$data   = json_decode($_GET['data']);
$sql_filter = '';			
$sql  = "SELECT 
pr.codproyecto,
pr.organizacion,
pr.pia,
pr.descripcion AS oportunidad,
fg.codfuncionproyecto,
fg.nomfuncionproyecto AS funcion,
po.codprogramaproyecto,
po.nomprogramaproyecto as programaproyecto,
pp.codsubprogramaproyecto,
pp.nomsubprograma AS programa,
pr.monto as monto,
pr.montoactualizado,
pr.codigosnip,
pr.codigounico,
CASE pr.codestadoproyecto
	WHEN 1 THEN 'ACTIVO'
	ELSE 'INACTIVO'
END AS estado,
pr.coddpto::integer as coddpto,
pr.codprov,
pr.coddist,
pr.coddpto || pr.codprov as codubigeo,
op.codorigen,
op.nomorigen AS origen,
dp.nomdpto AS region,
fp.codfase,
fp.nomfase AS fasegobierno,
ng.codnivelgobierno,
ng.nomnivelgobierno AS nivelgobierno,
pr.fecregistro,
to_char(to_date(fecregistro,'YYYY-MM-DD'), 'DD/MM/YYYY') as fecregistro2,
pr.ubicacion AS geometry,
CASE pr.codnivelgobierno
	WHEN 1 THEN 'fa-home'
	WHEN 2 THEN 'fa-building'
	WHEN 3 THEN 'fa-hospital'
	WHEN 4 THEN 'fa-university'
	WHEN 5 THEN 'fa-place-of-worship'
	ELSE NULL
END AS nivel,
CASE pr.codfase
	WHEN 1 THEN 'yellow'
	WHEN 2 THEN 'orange'
	WHEN 3 THEN 'red'
	WHEN 4 THEN 'green'
	WHEN 5 THEN ''
	WHEN 6 THEN 'cyan'
	WHEN 7 THEN 'white'
	WHEN 8 THEN 'purple'
	WHEN 9 THEN 'green'
	WHEN 10 THEN 'green-dark'
	WHEN 11 THEN 'blue'
	ELSE NULL
END AS fase,
pr.flag_seguimiento as flag_seguimiento,
pr.tope_cipril as tope_ciprl,
pr.estadooxi,
pr.poblacion
FROM obs.gen_proyecto pr
JOIN obs.gen_funcionproyecto fg ON fg.codfuncionproyecto = pr.codfuncionproyecto
JOIN obs.gen_programaproyecto po ON po.codprogramaproyecto = pr.codprogramaproyecto
JOIN obs.gen_origenproyecto op ON op.codorigen = pr.codorigen
JOIN obs.gen_departamento dp ON dp.coddpto = pr.coddpto
JOIN obs.gen_faseproyecto fp ON fp.codfase = pr.codfase
JOIN obs.gen_nivelgobierno ng ON ng.codnivelgobierno = pr.codnivelgobierno
JOIN obs.gen_subprogramaproyecto pp ON pp.codprogramaproyecto = pr.codprogramaproyecto AND pp.codsubprogramaproyecto = pr.codsubprogramaproyecto
JOIN distrito ds on ds.iddist = pr.coddpto || pr.codprov || pr.coddist
where

 ";
// -- pr.codfase not in(10) and
// -- pr.monto >= 0 and pr.tope_cipril >= 0  limit 1000";	

$sql_filter = $_GET['data'];    
$sql_filter = str_replace("coddpto", "pr.coddpto::integer", $sql_filter);
$sql_filter = str_replace("codfase", "pr.codfase", $sql_filter);
$sql_filter = str_replace("codnivelgobierno", "pr.codnivelgobierno", $sql_filter);
$sql_filter = str_replace("codfuncionproyecto", "pr.codfuncionproyecto", $sql_filter);
$sql_filter = str_replace("codsubprogramaproyecto", "pr.codsubprogramaproyecto", $sql_filter);
$sql_filter = str_replace("tope_ciprl", "pr.tope_cipril", $sql_filter);
$sql_filter = str_replace("codubigeo", "pr.coddpto || pr.codprov", $sql_filter);
$sql_filter = str_replace("oportunidad", "pr.descripcion", $sql_filter);
$sql_filter = str_replace("codprogramaproyecto", "pr.codprogramaproyecto", $sql_filter);
$sql_filter = str_replace("estadooxi", "pr.estadooxi", $sql_filter);

$connect = pg_connect("host=localhost port=5432 dbname=colaboraccion user=postgres password=geoserver");
$result = pg_query($connect, $sql . ' ' . $sql_filter);
var_dump($sql . '' . $sql_filter );
pg_close($connect);

?>
