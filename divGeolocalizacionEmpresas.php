<?php
 require 'php/app.php';

 $excelEmpresas = dropDownList((object) ['method' => 'empresasOXIExcelGeo']);
 
 ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            Georeferenciación de inversiones de proyectos OxI por Empresas
        </div>
    </div>
    <div class="col-lg-6">
        <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
            <table class="table table-sm table-detail">
                <tr>
                    <th class="text-center bold" width="5%"><input type="checkbox" id="cbox_" name="checkbox_GeoCoordenadasTotal" checked></th>
					<th class="text-center bold" width="70%">Empresas</th>
					<th class="text-center bold" width="20%">Cantidad Proyectos</th>
                </tr>
                <?php 
                    $sumtotal=0;
                    foreach ($excelEmpresas as $key){ 
                        $sumtotal= $sumtotal+   $key->cantidad ;
                ?>
                <tr>
                    <td class="text-center" ><input type="checkbox" id="cbox_<?php echo $key->empresa?>" name="checkbox_GeoCoordenadas" checked></td>
                    <td class="text-left" ><a href="" class="openModalDialogEmpresas" onclick="App.events(this); return false;" ><?php echo ucfirst($key->empresa)?></a></td>
					<td class="text-right" ><?php    echo ($key->cantidad)?></td>
                </tr>
                <?php }?>
                <tr>
                    <td class="text-center" ></td>
                    <td class="text-left" >Total</td>
					<td class="text-right" ><?php    echo ($sumtotal)?></td>
                </tr>
            </table>
        </div>       
    </div>  
    <div class="col-lg-6">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div id="map2" style="height:450px; width:100%;"></div>
                </div>
            </div>
        </div>
    </div>                                                                          
</div>
<br>
<div id="tablaTopEmpresas">
</div>