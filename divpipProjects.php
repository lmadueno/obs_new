
<?php
	require 'php/app.php';
    $data   = json_decode($_GET['data']);
    
?>
<div class="row">

	                                    <div class="col-lg-12">
											<table class="table table-sm table-detail">
												<thead>										
													<tr>
														<th class="text-center bold" rowspan="2" style="vertical-align:middle">Función</th>
														<th class="text-center bold" colspan="2" width="30%">Formulación</th>
														<th></th>
														<th class="text-center bold" colspan="2" width="30%">Evaluación</th>
														<th></th>
														<th class="text-center bold" colspan="2" width="30%">Perfil Viable</th>
														<th></th>
														<th class="text-center bold" colspan="2" width="30%">Total</th>
													</tr>	
													<tr>
														<th class="text-center bold" style="vertical-align:middle">Cant</th>
														<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
														<th></th>
														<th class="text-center bold" style="vertical-align:middle">Cant</th>
														<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
														<th></th>
														<th class="text-center bold" style="vertical-align:middle">Cant</th>
														<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
														<th></th>
														<th class="text-center bold" style="vertical-align:middle">Cant</th>
														<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
													</tr>	
												</thead>
												<?php $pipxUbigeo = dropDownList((object) ['method' => 'pipfuncionXUbigeoOtros','idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
												$tempPIP1=0;
												$tempPIP2=0;
												$tempPIP3=0;
												$tempPIP4=0;
												$tempPIP5=0;
												$tempPIP6=0;
												
												?>
												<?php foreach ($pipxUbigeo as $key){ ?>
													<tr>
														<td class="text-left">
                                                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->codfuncion?>" href="#" onclick="App.events(this); return false;">
																<?php echo ucfirst(strtolower($key->funcion))?>
													        </a>			                                                    
                                                        </td>														
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result1=0;
                                                                 $pipxUbigeo1 = dropDownList((object) ['method' => 'pipfuncionCount','funcion'=>$key->codfuncion,'codfase'=>2,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo1 as $key1){
                                                                    $result1=$key1->result;
                                                                 }
                                                                 $tempPIP1+= $result1;
                                                                 echo $result1;
                                                            ?>
                                                         </td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result2=0;
                                                                 $pipxUbigeo2 = dropDownList((object) ['method' => 'pipFuncionSuma','funcion'=>$key->codfuncion,'codfase'=>2,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo2 as $key1){
                                                                    $result2=$key1->result;
                                                                 }
                                                                 $tempPIP2+= $result2;
                                                                 if($result2==0) echo '-';else echo number_format($result2);
                                                            ?>
                                                         </td>
                                                         <td></td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result3=0;
                                                                 $pipxUbigeo3 = dropDownList((object) ['method' => 'pipfuncionCount','funcion'=>$key->codfuncion,'codfase'=>3,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo3 as $key1){
                                                                    $result3=$key1->result;
                                                                 }
                                                                 $tempPIP3+= $result3;
                                                                 echo $result3;
                                                            ?>
                                                         </td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result4=0;
                                                                 $pipxUbigeo4 = dropDownList((object) ['method' => 'pipFuncionSuma','funcion'=>$key->codfuncion,'codfase'=>3,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo4 as $key1){
                                                                    $result4=$key1->result;
                                                                 }
                                                                 $tempPIP4+= $result4;
                                                                 if($result4==0) echo '-';else echo number_format($result4);
                                                            ?>
                                                         </td>
                                                         <td></td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result5=0;
                                                                 $pipxUbigeo5 = dropDownList((object) ['method' => 'pipfuncionCount','funcion'=>$key->codfuncion,'codfase'=>4,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo5 as $key1){
                                                                    $result5=$key1->result;
                                                                 }
                                                                 $tempPIP5+= $result5;
                                                                 echo $result5;
                                                            ?>
                                                         </td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 $result6=0;
                                                                 $pipxUbigeo6 = dropDownList((object) ['method' => 'pipFuncionSuma','funcion'=>$key->codfuncion,'codfase'=>4,'idnivel'=>$data->idnivel,'idnivGob'=>$data->idnivGob,'codubigeo' => $data->codubigeo]);
                                                                 foreach ($pipxUbigeo6 as $key1){
                                                                    $result6=$key1->result;
                                                                 }
                                                                 $tempPIP6+= $result6;
                                                                 if($result6==0) echo '-';else echo number_format($result6);     
                                                            ?>
                                                         </td>
                                                         <td></td>
                                                         <td class="text-center">
                                                             <?php 
                                                                 echo $result1+$result3+$result5;
                                                            ?>
                                                         </td>
                                                         <td class="text-center">
                                                             <?php 
                                                                echo number_format($result2+$result4+$result6);     
                                                            ?>
                                                         </td>
													</tr>
													<tr data-target="lnkProvXrutas_<?php echo $key->codfuncion?>" style="display: none;">
                                                        <td colspan="12">
                                                            <div class="card">
                                                                <div class="card-body">
															        <div class="row">
																        <div class="col-lg-12">   
																			<table class="table table-sm table-detail">
																				<tr>
                                                                                    <th class="text-center bold" style="vertical-align:middle" width="40%">División Funcional</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Cantidad de Proyectos</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Total</th>
																				
																				</tr>
																					<?php $pipxUbigeoDet = dropDownList((object) ['method' => 'pipfuncionXUbigeoOtrosDet','idnivel'=>$data->idnivel, 'codubigeo' => $data->codubigeo,'codfuncion' => $key->codfuncion,'idnivGob'=>$data->idnivGob]);?>
																					<?php foreach ($pipxUbigeoDet as $key1){ ?>
																				<tr>
                                                                                    <td class="text-left">
                                                                                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key1->nomprogramaproyecto?>" href="#" onclick="App.events(this); return false;">
	    																				    <?php echo ucfirst(strtolower($key1->nomprogramaproyecto))?>
																				        </a>
                                                                                    </td>
                                                                                    <td class="text-center"><?php echo number_format($key1->cantidad)?></td>
                                                                                    <td class="text-center"><?php echo number_format($key1->total)?></td>
                                                                                </tr>
                                                                                        <tr data-target="lnkProvXrutas_<?php echo $key1->nomprogramaproyecto?>" style="display: none;">
                                                                                            <td colspan="3">
                                                                                                <div class="card">
                                                                                                    <div class="card-body">
                                                                                                        <div class="row">
                                                                                                            <div class="col-lg-12">
                                                                                                                <table class="table table-sm table-detail">
                                                                                                                    <tr>
                                                                                                                        <th class="text-center bold" style="vertical-align:middle" width="40%">Grupo Funcional</th>
                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Cantidad de Proyectos</th>
                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Total</th>
                                                                                                                    </tr>
                                                                                                                    <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'pipfuncionXUbigeoOtrosDet3','idnivel'=>$data->idnivel, 'codubigeo'=> $data->codubigeo,'codfuncion' => $key->codfuncion,'idnivGob'=>$data->idnivGob,'nomprogramaproyecto' => $key1->nomprogramaproyecto]);?>
                                                                                                                    <?php 
                                                                                                                    
                                                                                                                        foreach ($pipxUbigeoDet as $key2){ 
                                                                                                                        
                                                                                                                        ?>
                                                                                                                        <tr>
                                                                                                                            <td class="text-left">
                                                                                                                                <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key2->subprograma?>" href="#" onclick="App.events(this); return false;">	
                                                                                                                                    <?php echo ucfirst(strtolower($key2->subprograma))?>
                                                                                                                                </a>
                                                                                                                            </td>
                                                                                                                            <td class="text-center"><?php echo number_format($key2->cantidad)?></td>
                                                                                                                            <td class="text-center"><?php echo number_format($key2->total)?></td>
                                                                                                                        </tr>
                                                                                                                        <tr data-target="lnkProvXrutas_<?php echo $key2->subprograma?>" style="display: none;">
                                                                                                                            <td colspan="3">
                                                                                                                                <div class="card">
                                                                                                                                    <div class="card-body">
                                                                                                                                        <div class="row">
                                                                                                                                            <div class="col-lg-12">
                                                                                                                                                <table class="table table-sm table-detail">
                                                                                                                                                    <tr>
                                                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Codigo</th>
                                                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Fase</th>
                                                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Fecha</th>
                                                                                                                                                        <th class="text-center bold" style="vertical-align:middle">Monto</th>
                                                                                                                                                    </tr>
                                                                                                                                                    <?php $pipxUbigeoDet1 = dropDownList((object) ['method' => 'pipfuncionXUbigeoOtrosDet4','idnivel'=>$data->idnivel, 'codubigeo'=> $data->codubigeo,'codfuncion' => $key->codfuncion,'idnivGob'=>$data->idnivGob,'subprograma' => $key2->subprograma]);?>
                                                                                                                                                        <?php 
                                                                                                                                                        $temporalMonto=0;
                                                                                                                                                            foreach ($pipxUbigeoDet1 as $key3){ 
                                                                                                                                                            $temporalMonto+=$key3->monto;
                                                                                                                                                            ?>
                                                                                                                                                            <tr>
																																								<td class="text-center">
                                                                                                                                                                    <a target='_blank' class="form-control form-control-sm" style="border: 0;text-decoration: underline;" href='https://ofi5.mef.gob.pe/invierte/ejecucion/traeListaEjecucionSimplePublica/<?php  echo ucfirst(strtolower($key3->codunico));?>'><?php echo  ucfirst(strtolower($key3->codunico));?></a>
                                                                                                                                                                </td>
                                                                                                                                                                <td class="text-center"><?php echo ucfirst(strtolower($key3->funcion))?></td>
                                                                                                                                                                <td class="text-center"><?php echo ucfirst(strtolower($key3->programas))?></td>
                                                                                                                                                                <td class="text-center"><?php echo number_format($key3->monto)?></td>
                                                                                                                                                            </tr>
                                                                                                                                                        <?php } ?>

                                                                                                                                                </table>
                                                                                                                                            </div>
                                                                                                                                        </div>	
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                            </td>
                                                                                                                        
                                                                                                                        </tr>
                                                                                                                        
                                                                                                                        <?php } ?>
                                                                                                                        <!--<tr>
                                                                                                                                                        <td class="text-left">Total</td>
                                                                                                                                                        <td class="text-center"></td>
                                                                                                                                                        <td class="text-center"></td>
                                                                                                                                                        <td class="text-center"></td>
                                                                                                                                                        <td class="text-right"><?php echo number_format($temporalMonto)?></td>
                                                                                                                        
                                                                                                                        </tr>-->
                                                                                                                </table>
                                                                                                            </div>
                                                                                                        </div>	
                                                                                                    </div>
                                                                                                </div>
                                                                                            </td>
                                                                                        
                                                                                        </tr>
																					<?php }?>
																			</table>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
												<?php }?>	
													<tr>
														<td class="text-left">TOTAL :</td>
														<td class="text-center"><?php echo ($tempPIP1)?></td>
														<td class="text-right"><?php echo number_format($tempPIP2)?></td>
														<td></td>
														<td class="text-center"><?php echo ($tempPIP3)?></td>
														<td class="text-right"><?php echo number_format($tempPIP4)?></td>
														<td></td>
														<td class="text-center"><?php echo ($tempPIP5)?></td>
														<td class="text-right"><?php echo number_format($tempPIP6)?></td>
														<td></td>
														<td class="text-center"><?php echo ($tempPIP1+$tempPIP3+$tempPIP5)?></td>
														<td class="text-right"><?php echo number_format($tempPIP2+$tempPIP4+$tempPIP6)?></td>
													</tr>																		
											</table>
										</div>
									</div>