var providers = {};

providers['OpenStreetMap_Mapnik'] = {
    title: 'Por defecto',
    icon: 'icons/openstreetmap_mapnik.png',
    layer: L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        mapID: 'default',
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
};

providers['HERE_satelliteDay'] = {
    title: 'Satélite',
    icon: 'icons/here_satelliteday.png',
    layer: L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
        mapID: 'satellite',
        attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>'
    })
};

providers['OpenStreetMap_BlackAndWhite'] = {
    title: 'B/N',
    icon: 'icons/openstreetmap_blackandwhite.png',
    layer: L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        mapID: 'blackAndWhite',
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })
};


providers['CartoDB_Positron'] = {
    title: 'Positrón',
    icon: 'icons/cartodb_positron.png',
    layer: L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
		mapID: 'positron',
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
    })
};