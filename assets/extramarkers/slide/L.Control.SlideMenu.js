L.Control.SlideMenu = L.Control.extend({
    options: {
        position: 'topleft',
        menuposition: 'topleft', // topleft,topright,bottomleft,bottomright
        width: '300px',
        height: '100%',
        direction: 'horizontal', // vertical or horizontal
        changeperc: '10',
        delay: '10',
        icon: 'fa-bars',
        hidden: false
    },

    initialize: function(innerHTML, options){
        L.Util.setOptions(this, options);
        this._innerHTML = innerHTML;
        this._isLeftPosition = this.options.menuposition == 'topleft' ||
            this.options.menuposition == 'bottomleft' ? true : false;
        this._isTopPosition = this.options.menuposition == 'topleft' ||
            this.options.menuposition == 'topright' ? true : false;
        this._isHorizontal = this.options.direction == 'horizontal' ? true : false;
    },

    onAdd: function(map){
        this._container = L.DomUtil.create('div', 'leaflet-control');
		this._container.id  = "controlcontainer";
		
		var controlbox = L.DomUtil.create('div', '', this._container);		
        controlbox.id = 'controlbox';
		
		var logobox = L.DomUtil.create('div', 'logobox', controlbox);		
		logobox.innerHTML = '<img id="top-logo" src="assets/app/img/logo.png" width="150px"/>';        
		
		var boxcontainer = L.DomUtil.create('div', 'searchbox searchbox-shadow', controlbox);		
        boxcontainer.id = 'boxcontainer';
		
		var countersum = L.DomUtil.create('div', 'alert alert-info counterSum d-block d-sm-none', controlbox);
		countersum.style.marginTop = "5px";
		countersum.textContent = "-";
		
		var searchbox = L.DomUtil.create('div', 'searchbox-menu-container', boxcontainer);
		
		var link   = L.DomUtil.create('button', 'searchbox-menubutton', searchbox);		
        link.title = 'Menu';
		link.id    = 'searchbox-menubutton';
		
		var seeker   = L.DomUtil.create('div', 'row', boxcontainer);
        var seekerCol   = L.DomUtil.create('div', 'col-3', seeker);
		var boxselect = L.DomUtil.create('select', '', seekerCol);		
		boxselect.id  = 'ddlTipoBusq';		
		boxselect.style = 'display: initial;width: 80px;';
		L.DomUtil.addClass(boxselect, 'form-control form-control-sm');
		
		var boxoption_0 = document.createElement("option");
		boxoption_0.text = "Ubigeo";
		var boxoption_1 = document.createElement("option");
		boxoption_1.text = "Proyecto";
		var boxoption_2 = document.createElement("option");
		boxoption_2.text = "Snip";
		var boxoption_3 = document.createElement("option");
		boxoption_3.text = "Código Único";
		
		boxselect.add(boxoption_0);
		boxselect.add(boxoption_1);
		boxselect.add(boxoption_2);
		boxselect.add(boxoption_3);
		var seekerCol1   = L.DomUtil.create('div', 'col-9', seeker);
		var boxinput         = L.DomUtil.create('input', '', seekerCol1);
		boxinput.id          = 'txtProyecto';
		boxinput.setAttribute("data-key", "txtProyecto");
		boxinput.style       = 'margin-left:5px; height: 30px; width: 60%;';
        boxinput.placeholder = 'Ingrese nombre de proyecto ...';      
        var deletebutton = L.DomUtil.create('div', 'searchbox-searchbutton-container', boxcontainer);
        var buttonCerrar = L.DomUtil.create('button', 'searchbox-searchbutton1', deletebutton);
        buttonCerrar.id ='delete-all-selecction';
        buttonCerrar.style       = 'padding-left: 1.3em; padding-right: 4.7em;';
        var searchbutton = L.DomUtil.create('div', 'searchbox-searchbutton-container', boxcontainer);
		var button = L.DomUtil.create('button', 'searchbox-searchbutton', searchbutton);
		button.id    = 'searchbox-searchbutton';     
      
        this._menu = L.DomUtil.create('div', 'leaflet-menu', map._container);

        this._menu.style.width = this.options.width;
        this._menu.style.height = this.options.height;

        if(this._isHorizontal){
            var frominit = -(parseInt(this.options.width, 10));
            if(this._isLeftPosition){
                this._menu.style.left = '-' + this.options.width;
            }
            else{
                this._menu.style.right = '-' + this.options.width;
            }

            if(this._isTopPosition){
                this._menu.style.top = '0px';
            }
            else{
                this._menu.style.bottom = '0px';
            }
        }
        else{
            var frominit = -(parseInt(this.options.height, 10));
            if(this._isLeftPosition){
                this._menu.style.left = '0px';
            }
            else{
                this._menu.style.right = '0px';
            }

            if(this._isTopPosition){
                this._menu.style.top = '-' + this.options.height;
            }
            else{
                this._menu.style.bottom = '-' + this.options.height;
            }
        }
		
		var container = L.DomUtil.create('div', 'container', this._menu);
		var row       = L.DomUtil.create('div', 'row', container);
		var row_logo      = L.DomUtil.create('div', 'col-lg-10', row);
		var row_back      = L.DomUtil.create('div', 'col-lg-2 vertical-center', row);

		var logo   = L.DomUtil.create('img', 'logo', row_logo);		
		logo.src   = 'assets/app/img/logo_lateral.png';
		
        var closeButton = L.DomUtil.create('button', 'leaflet-menu-close-button fa', row_back);

        if(this._isHorizontal){
            if(this._isLeftPosition){
                closeButton.style.float = 'right';
                L.DomUtil.addClass(closeButton, 'fa-chevron-left');
            }
            else{
                closeButton.style.float = 'left';
                L.DomUtil.addClass(closeButton, 'fa-chevron-right');
            }
        }
        else{
            if(this._isTopPosition){
                closeButton.style.float = 'right';
                L.DomUtil.addClass(closeButton, 'fa-chevron-up');
            }
            else{
                closeButton.style.float = 'right';
                L.DomUtil.addClass(closeButton, 'fa-chevron-down');
            }
        }

        this._contents = L.DomUtil.create('div', 'leaflet-menu-contents', this._menu);
        this._contents.innerHTML = this._innerHTML;
        this._contents.style.clear = 'both';

        if(this._isHorizontal){
            var ispx = this.options.width.slice(-1) == 'x' ? true : false;
            var unit = parseInt(this.options.width, 10) * parseInt(this.options.changeperc, 10) / 100;
        }
        else{
            var ispx = this.options.height.slice(-1) == 'x' ? true : false;
            var unit = parseInt(this.options.height, 10) * parseInt(this.options.changeperc, 10) / 100;
        }

        L.DomEvent.disableClickPropagation(this._menu);
        L.DomEvent
            .on(link, 'click', L.DomEvent.stopPropagation)
            .on(link, 'click', function(){
                // Open
                this._animate(this._menu, frominit, 0, true, ispx, unit);
            }, this)
            .on(closeButton, 'click', L.DomEvent.stopPropagation)
            .on(closeButton, 'click', function(){
                // Close
                this._animate(this._menu, 0, frominit, false, ispx, unit);
            }, this);
        L.DomEvent.on(this._menu, 'mouseover', function(){
            map.scrollWheelZoom.disable();
        });
        L.DomEvent.on(this._menu, 'mouseout', function(){
            map.scrollWheelZoom.enable();
        });

        if(this.options.hidden){
            this.hide();
        }

        return this._container;
    },

    onRemove: function(map){
        //Remove sliding menu from DOM
        map._container.removeChild(this._menu);
        delete this._menu;
    },

    setContents: function(innerHTML){
        this._innerHTML = innerHTML;
        this._contents.innerHTML = this._innerHTML;
    },

    _animate: function(menu, from, to, isOpen, ispx, unit){
        if(this._isHorizontal){
            if(this._isLeftPosition){
                menu.style.left = from + (ispx ? 'px' : '%');
            }
            else{
                menu.style.right = from + (ispx ? 'px' : '%');
            }
        }
        else{
            if(this._isTopPosition){
                menu.style.top = from + (ispx ? 'px' : '%');
            }
            else{
                menu.style.bottom = from + (ispx ? 'px' : '%');
            }
        }

        if(from != to){
            setTimeout(function(slideMenu){
                var value = isOpen ? from + unit : from - unit;
                slideMenu._animate(slideMenu._menu, value, to, isOpen, ispx, unit);
            }, parseInt(this.options.delay), this);
        }
        else{
            return;
        }
    },

    hide: function () {
        this._container.style.display = 'none';
    },

    show: function () {
        this._container.style.display = 'inherit';
    }
});

L.control.slideMenu = function(innerHTML, options) {
    return new L.Control.SlideMenu(innerHTML, options);
};