function prueba() {
    var usu = document.getElementById('txtusu').value;
    var pwd = document.getElementById('txtpwd').value;
    if (validarVacios(usu, pwd) != 0) {
        const proxyurl = "https://cors-anywhere.herokuapp.com/";
        const url = "http://78.46.16.8:4000/users";
        axios.post(url, {
                usr: usu,
                pwd: pwd
            })
            .then(function(response) {
                if (response.data[0].rpta == 0) {
                    Swal.fire({
                        icon: 'error',
                        title: response.data[0].msj,
                        text: 'Las credenciales del Inicio de Session no se encuentran registradas.',
                    });
                } else {
                    Swal.fire({
                        icon: 'success',
                        title: 'Bienvenido al Sistema',
                        text: response.data[0].msj,
                        showConfirmButton: false,
                    });
                    var obj = {
                        method: 'crear',
                        sesion: { id: response.data[0].rpta, rol: response.data[0].rol }
                    };
                    setTimeout(function() {
                        $.get({
                            url: './php/session.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                window.location.href = "./index.php";
                                localStorage.setItem('logged-in', true);
                                sessionStorage.setItem('logged-in', true);
                            }
                        });
                    }, 100);
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }
}
var validarVacios = function(usu, pwd) {
    var rpta = 0;
    if (usu.length == 0) {
        toastr.warning('El Usuario no debe de Ir Vacio', 'Warning', { timeOut: 2000, closeButton: true, onclick: null, progressBar: true });
    } else if (pwd === '') {
        toastr.warning('La contraseña no debe de Ir Vacio', 'Warning', { timeOut: 2000, closeButton: true, onclick: null, progressBar: true });
    } else {
        rpta = 1;
    }
    return rpta;
}