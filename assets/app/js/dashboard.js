var dualChart = {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: [{
        categories: [],
        crosshair: true,
        gridLineWidth: 1
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    series: []
}
var combine = {
    title: {
        text: 'Combination chart'
    },
    xAxis: {
        categories: ['Apples', 'Oranges', 'Pears', 'Bananas', 'Plums']
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '50px',
                top: '18px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Jane',
        data: [90, 2, 1, 3, 4]
    }, {
        type: 'column',
        name: 'John',
        data: [50, 3, 5, 7, 6]
    }, {
        type: 'column',
        name: 'Joe',
        data: [70, 3, 3, 9, 0]
    }, {
        type: 'spline',
        name: 'Average',
        data: [50, 2.67, 3, 6.33, 3.33],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'spline',
        name: 'Average',
        data: [20, 2.67, 3, 6.33, 3.33],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }, {
        type: 'spline',
        name: 'Average',
        data: [80, 2.67, 3, 6.33, 3.33],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
}
var combinateChart = {
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        allowDecimals: false
    },
    yAxis: {
        title: {
            text: 'Soles'
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        valuePrefix: "S/. ",
        valueDecimals: 2,
        shared: true
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 0
        }
    },
    series: []
};
var direccionesTemporalruc;
var map111;
var multibar = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Millones de Soles (MS/)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {

                }
            }
        },
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: []
};
var stackedBarProyectado = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: []
}
var tile1;
var tile2;
var tile3;
var tile4;
var tile5;
var tile6;

var map3, map4, map5;
var owsrootUrl = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/ows';
var wmsrootUrl = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/wms';
var obj_indicador_empresa_2;
var App = function() {
    var init = function() {
        document.body.style.zoom = "80%";
        initComponents();
        window.addEventListener('storage', function(e) {
            location.reload();
        });
    }
    var lyrDraw = function(lyrs, cql_filter) {
        lyrs.forEach(function(lyr) {
            lyr.wmsParams.cql_filter = cql_filter;
            lyr.redraw();
        });
    }
    var initComponents = function() {
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil2"]', function() {
            var tmp = [];
            $("input[name=chk_quantil2]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile2], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil3"]', function() {
            var tmp = [];
            $("input[name=chk_quantil3]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile3], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil1"]', function() {
            var tmp = [];
            $("input[name=chk_quantil1]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile1], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });

    }
    var iniLoadAlert = function(title, html, time) {
        let timerInterval
        Swal.fire({
            title: title,
            html: html + ' <b></b> milisegundos.',
            imageUrl: '../assets/app/img/logo.png',
            imageWidth: 400,
            imageHeight: 50,
            timer: time,
            timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {

            if (result.dismiss === Swal.DismissReason.timer) {

            }
        })
    }
    var dashboarentidades_partidos = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.csn,
                obj.msn,
                obj.ccn,
                obj.mcn
            ]
        });
    }
    var dashBoardProyects_perfiles_grupoprograma = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                //(this.category);
                            }
                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'Monto',
                    data: obj.monto
                },
                {
                    type: 'spline',
                    name: 'Cantidad',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }
            ]
        });

    }
    var dashBoardProyects_perfiles_grupofuncion = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var tiutlo = this.category;

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_subprograma', programa: this.category, perfil: obj.perfil, funcion: obj.funcion })),
                                    success: function(response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function(i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomsubprograma);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por division funcional  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_division_programa'
                                        }
                                        dashBoardProyects_perfiles_grupoprograma(obj);
                                        $("#chart_dash_proyectos_funcion_division_programa").css("display", "");
                                    }
                                });
                            }
                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'Monto',
                    data: obj.monto
                },
                {
                    type: 'spline',
                    name: 'Cantidad',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }
            ]
        });

    }
    var dashBoardProyects_perfiles_funcion = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var tiutlo = this.category;
                                //(obj.obj_envion);
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_programa', funcion: this.category, perfil: obj.obj_envion })),
                                    success: function(response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function(i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomprogramaproyecto);
                                        });
                                        var obj1 = {
                                            titulo: 'Analisis por grupo funcional  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            perfil: obj.obj_envion,
                                            funcion: tiutlo,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_division'
                                        }
                                        dashBoardProyects_perfiles_grupofuncion(obj1);
                                        $("#chart_dash_proyectos_funcion_division").css("display", "");

                                    }
                                });
                            }
                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'Monto',
                    data: obj.monto
                },
                {
                    type: 'spline',
                    name: 'Cantidad',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }
            ]
        });

    }
    var dashBoardProyects_perfiles = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var tiutlo = this.category;

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_funcion', perfil: this.category })),
                                    success: function(response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function(i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomfuncionproyecto);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por funcion  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            obj_envion: tiutlo,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_'
                                        }

                                        dashBoardProyects_perfiles_funcion(obj);
                                        $("#chart_dash_proyectos_funcion_").css("display", "");

                                    }
                                });
                            }
                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'Monto',
                    data: obj.monto
                },
                {
                    type: 'spline',
                    name: 'Cantidad',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'white'
                    }
                }
            ]
        });

    }
    var dashBoardEmpresas_top = function(obj) {
        Highcharts.chart(obj.div, {
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: {
                labels: {
                    format: '{value:,.0f}'
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                alert(this.category)
                            }
                        }
                    }
                }

            },
            series: [{
                type: 'column',
                name: 'OxI',
                data: obj.oxi
            }, {
                type: 'spline',
                name: 'Utilidad',
                data: obj.utilidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'black'
                }
            }]
        });

    }
    var dashBoardEmpresas_top_tamaño_sector = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'OxI',
                    data: obj.oxi
                },
                {
                    type: 'column',
                    name: 'Utilidad',
                    data: obj.utilidad,
                    color: '#A7454F',
                }, {
                    type: 'spline',
                    name: 'Cantidad',
                    color: '#000000',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'black'
                    }
                }
            ]
        });

    }
    var dashBoardEmpresas_top_tamaño = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                var tamanio;
                                var titulo = this.category;
                                //(this.category);
                                if (this.category == 'Gran empresa') {
                                    tamanio = 2;
                                } else if (this.category == 'Mediana empresa') {
                                    tamanio = 3;
                                } else {
                                    tamanio = 4;
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2_sector', tamanio: tamanio })),
                                    success: function(response) {
                                        var oxi = [];
                                        var utilidad = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function(i, obj) {
                                            oxi.push(parseInt(obj.oxi));
                                            utilidad.push(parseInt(obj.utilidad));
                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomsector);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por sectores de ' + titulo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            oxi: oxi,
                                            utilidad: utilidad,
                                            cantidad: cantidad,
                                            div: 'chart_dash_indicador2_'
                                        }
                                        dashBoardEmpresas_top_tamaño_sector(obj);
                                    }
                                });
                                $("#chart_dash_indicador2_").css("display", "");
                            }
                        }
                    }
                }

            },
            series: [{
                    type: 'column',
                    name: 'OxI',
                    data: obj.oxi
                },
                {
                    type: 'column',
                    name: 'Utilidad',
                    color: '#A7454F',
                    data: obj.utilidad
                }, {
                    type: 'spline',
                    name: 'Cantidad',
                    color: '#000000',
                    yAxis: 1,
                    data: obj.cantidad,
                    marker: {
                        lineWidth: 2,
                        lineColor: Highcharts.getOptions().colors[3],
                        fillColor: 'black'
                    }
                }
            ]
        });

    }
    var dashboarProyectos_congreso = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                $("#chart_mef_empresas_1").css("display", "");

                                var obj = {
                                    method: 'macro_mef_sub',
                                    entidad: this.category
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        var cantidad = [];
                                        var resto = [];
                                        var total = [];
                                        var categories = [];
                                        $.each(response.data, function(i, obj) {
                                            cantidad.push(parseInt(obj.cantidad));
                                            resto.push(parseInt(obj.resto));
                                            total.push(parseInt(obj.total));
                                            categories.push(obj.nivel_g);
                                        });
                                        var obj_cantidad = {
                                            name: 'Cantidad',
                                            type: 'spline',
                                            color: '#000000',
                                            yAxis: 1,
                                            data: cantidad,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_monto = {
                                            name: 'Costo Total',
                                            type: 'column',
                                            data: total,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_resto = {
                                            name: 'Avance Ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            data: resto,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var titulo = response.data[0].region_r;
                                        var obj = {
                                            titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 por Región ' + titulo,
                                            subtitulo: '',
                                            categories: categories,
                                            cantidad: obj_cantidad,
                                            monto: obj_monto,
                                            monto_usado: obj_resto,
                                            div: 'chart_mef_empresas_1'
                                        }

                                        dashboarProyectos_congreso_sub(obj, response.data[0].region_r);
                                    }
                                });

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var dashboarProyectos_congreso_sub = function(obj, entidad) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                $("#chart_mef_empresas_2").css("display", "");
                                var obj = {
                                    method: 'macro_mef_sub_entidad',
                                    entidad: entidad,
                                    gobierno: this.category
                                }
                                var titulo = this.category;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        var cantidad = [];
                                        var resto = [];
                                        var total = [];
                                        var categories = [];
                                        $.each(response.data, function(i, obj) {
                                            cantidad.push(parseInt(obj.cantidad));
                                            resto.push(parseInt(obj.resto));
                                            total.push(parseInt(obj.total));
                                            categories.push(obj.entidad);
                                        });
                                        var obj_cantidad = {
                                            name: 'Cantidad',
                                            type: 'spline',
                                            color: '#000000',
                                            yAxis: 1,
                                            data: cantidad,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_monto = {
                                            name: 'Costo Total',
                                            type: 'column',
                                            data: total,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_resto = {
                                            name: 'Avance Ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            data: resto,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }

                                        var obj = {
                                            titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 en  ' + entidad + ' por ' + titulo,
                                            subtitulo: '',
                                            categories: categories,
                                            cantidad: obj_cantidad,
                                            monto: obj_monto,
                                            monto_usado: obj_resto,
                                            div: 'chart_mef_empresas_2'
                                        }

                                        dashboarProyectos_congreso_sub_2(obj);
                                    }
                                });

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var dashboarProyectos_congreso_sub_2 = function(obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var wordcloud_Function = function(obj1) {
        Highcharts.chart(obj1.div, {

            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                //(this.name);
                                var obj = {
                                    method: 'macro_mef_Funcion_programa',
                                    consulta: this.name,
                                }
                                var tmp = [];
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        $.each(response.data, function(i, obj) {
                                            tmp.push({ name: obj.nomprogramaproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_Function_2({ div: 'chart_mef_empresas_1_funcion', data: tmp });
                                    }
                                });
                                var obj = {
                                    method: 'macro_mef_Funcion_subprograma',
                                    consulta: this.name,
                                }
                                var tmp1 = [];
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        $.each(response.data, function(i, obj) {
                                            tmp1.push({ name: obj.nomsubprograma, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_Function_2({ div: 'chart_mef_empresas_2_funcion', data: tmp1 });
                                    }
                                });
                                $("#chart_mef_empresas_1_funcion").css("display", "");
                                $("#chart_mef_empresas_2_funcion").css("display", "");
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_Function_2 = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_tamnio_ind = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                //(this.name);
                                if (this.name == 'GRAN EMPRESA') {
                                    tamanio = 2;
                                } else if (this.name == 'MEDIANA EMPRESA') {
                                    tamanio = 3;
                                } else {
                                    tamanio = 4;
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2_sector', tamanio: tamanio })),
                                    success: function(response) {
                                        var tmp = [];
                                        $.each(response.data, function(i, obj) {
                                            tmp.push({ name: obj.nomsector, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_tamnio_ind_sector({ div: 'chart_dash_indicador2_', data: tmp });
                                    }
                                });
                                $("#chart_dash_indicador2_").css("display", "");
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_tamnio_ind_sector = function(obj1) {
            Highcharts.chart(obj1.div, {
                plotOptions: {
                    series: {
                        turboThreshold: 5000,
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function() {
                                    //(this.name);
                                }
                            }
                        }
                    }
                },
                series: [{
                    rotation: {
                        from: 0,
                        to: 0,
                        orientations: 5
                    },
                    type: 'wordcloud',
                    data: obj1.data,
                    name: 'wdsfg'
                }],
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormatter: function() {
                        var point = this;
                        return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                    }
                }
            });
        }
        //-----------------------------------------------
    var wordcloud_perfiles_ = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {

                                var tiutlo = this.name;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_funcion', perfil: this.name })),
                                    success: function(response) {
                                        var tmp = [];
                                        $.each(response.data, function(i, obj) {
                                            tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion({ div: 'chart_dash_proyectos_funcion_', data: tmp, obj_envion: tiutlo });
                                        $("#chart_dash_proyectos_funcion_").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                //(obj1);
                                var titulo = this.name;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_programa', funcion: this.name, perfil: obj1.obj_envion })),
                                    success: function(response) {
                                        var tmp = [];
                                        $.each(response.data, function(i, obj) {
                                            tmp.push({ name: obj.nomprogramaproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion_programa({
                                            div: 'chart_dash_proyectos_funcion_division',
                                            data: tmp,
                                            perfil: obj1.obj_envion,
                                            funcion: titulo
                                        });
                                        $("#chart_dash_proyectos_funcion_division").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion_programa = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function() {
                                //(obj1);

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_subprograma', programa: this.name, perfil: obj1.perfil, funcion: obj1.funcion })),
                                    success: function(response) {
                                        var tmp = [];
                                        $.each(response.data, function(i, obj) {
                                            tmp.push({ name: obj.nomsubprograma, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion_subprograma({ div: 'chart_dash_proyectos_funcion_division_programa', data: tmp });
                                        $("#chart_dash_proyectos_funcion_division_programa").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion_subprograma = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_mdl_partidos = function(obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }


    var changeTabPanelEstadistica = function(params) {
        $('#empresa').removeClass('active');
        $('#proyectos').removeClass('active');
        $('#entidades').removeClass('active');
        $('#politica').removeClass('active');
        $('#salud').removeClass('active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
    }

    function formatNumber(num) {
        if (!num || num == 'NaN') return '-';
        if (num == 'Infinity') return '&#x221e;';
        num = num.toString().replace(/\$|\,/g, '');
        if (isNaN(num))
            num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num * 100 + 0.50000000001);
        cents = num % 100;
        num = Math.floor(num / 100).toString();
        if (cents < 10)
            cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
            num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
        return (((sign) ? '' : '-') + num);
    }
    /*entidades start VCTM */
    var fs_chart_entidad_canon = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                datos = [];
                categoria = [];
                dat = [];

                dat.push({
                    'c0': '1',
                    'c1': 'Gobierno Regional',
                    'c2': 'S/.' + formatNumber(parseInt(response.data[0].ciprl_gobierno_regional)),
                    'c3': 'S/.' + formatNumber((parseInt(response.data[0].ciprl_gobierno_regional) / 3)),
                    'c4': 'S/.' + formatNumber((parseInt((response.data[0].ciprl_gobierno_regional / 3)) * 0.4)),
                    'c5': formatNumber(parseInt(response.data[0].poblacion_gobierno_regional)),
                    'c6': 'S/.' + formatNumber(parseInt(response.data[0].monto_anual_gobierno_regional))
                })
                dat.push({
                    'c0': '2',
                    'c1': 'Universidad nacional',
                    'c2': 'S/.' + formatNumber(parseInt(response.data[0].ciprl_universidad_nacional)),
                    'c3': 'S/.' + formatNumber((parseInt(response.data[0].ciprl_universidad_nacional) / 3)),
                    'c4': 'S/.' + formatNumber((parseInt((response.data[0].ciprl_universidad_nacional / 3)) * 0.4)),
                    'c5': formatNumber(parseInt(response.data[0].poblacion_gobierno_regional)),
                    'c6': 'S/.' + formatNumber(parseInt(response.data[0].monto_anual_universidad_nacional))
                })
                dat.push({
                    'c0': '3',
                    'c1': 'Gobierno Local',
                    'c2': 'S/.' + formatNumber(parseInt(response.data[0].ciprl_gobierno_local)),
                    'c3': 'S/.' + formatNumber((parseInt(response.data[0].ciprl_gobierno_local) / 3)),
                    'c4': 'S/.' + formatNumber((parseInt((response.data[0].ciprl_gobierno_local / 3)) * 0.4)),
                    'c5': formatNumber(parseInt(response.data[0].poblacion_gobierno_local)),
                    'c6': 'S/.' + formatNumber(parseInt(response.data[0].monto_anual_gobierno_local))
                })
                dat.push({
                        'c0': 'Estimación de monto anual a recibir de CANON por poblador  S/.',

                        'c6': 'S/.' + formatNumber((parseInt(response.data[0].monto_anual_gobierno_regional)) + (parseInt(response.data[0].monto_anual_universidad_nacional)) + (parseInt(response.data[0].monto_anual_gobierno_local)))
                    })
                    // console.log(dat);


                // Ahora dibujamos la tabla
                let $cuerpoTabla = document.querySelector('#tbl_dist_canon');
                // Recorrer todos los productos
                $("#tbl_dist_canon tr").remove();
                // var a = 0;
                dat.forEach((producto, indice) => {
                    // Crear un <tr>
                    const $tr = document.createElement("tr");
                    // Creamos el <td> de nombre y lo adjuntamos a tr
                    var c0 = document.createElement("td");
                    if (indice == 3) {
                        c0.setAttribute('colspan', '6');
                        c0.textContent = producto.c0; // el textContent del td es el nombre
                        $tr.appendChild(c0);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                    } else {

                        c0.textContent = producto.c0; // el textContent del td es el nombre
                        $tr.appendChild(c0);

                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                    }
                    // El td de precio
                    // Finalmente agregamos el <tr> al cuerpo de la tabla
                    $cuerpoTabla.appendChild($tr);
                    // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                });


                // $cuerpoTabla = null;
                // var chart3 = Highcharts.chart(obj.div, {
                //     title: {
                //         text: ''
                //     },
                //     subtitle: {
                //         text: ''
                //     },
                //     xAxis: {
                //         categories: categoria,
                //         gridLineWidth: 1
                //     },
                //     yAxis: [{ // Secondary yAxis
                //             labels: {
                //                 format: '{value}',
                //                 style: {
                //                     color: Highcharts.getOptions().colors[1]
                //                 }
                //             },
                //             title: {
                //                 text: 'Millones de soles',
                //                 style: {
                //                     color: Highcharts.getOptions().colors[1]
                //                 }
                //             }
                //         }, { // Secondary yAxis
                //             title: {
                //                 text: 'Cantidad Proyectos',
                //                 style: {
                //                     color: Highcharts.getOptions().colors[0]
                //                 }
                //             },
                //             labels: {
                //                 format: '{value:,.0f}',
                //                 style: {
                //                     color: Highcharts.getOptions().colors[0]
                //                 }
                //             },
                //             opposite: true
                //         }

                //     ],
                //     labels: {
                //         items: [{
                //             html: '',
                //             style: {
                //                 left: '50px',
                //                 top: '18px',
                //                 color: ( // theme
                //                     Highcharts.defaultOptions.title.style &&
                //                     Highcharts.defaultOptions.title.style.color
                //                 ) || 'black'
                //             }
                //         }]
                //     },
                //     series: [{
                //         type: 'column',
                //         name: 'Costo Actualizado',
                //         data: dat2,

                //         color: '#4472C4',
                //         lineWidth: 3

                //     }, {
                //         type: 'column',
                //         name: 'Devengado Acumulado',
                //         data: dat3,

                //         color: '#ED7D31',
                //         lineWidth: 3
                //     }, {
                //         type: 'column',
                //         name: 'Saldo ejecutar',
                //         data: dat4,

                //         color: '#BFBFBF',
                //         lineWidth: 3
                //     }, {
                //         type: 'column',
                //         name: 'Saldor por financiar',
                //         data: dat5,

                //         color: '#FF0000',
                //         lineWidth: 3
                //     }, {
                //         type: 'spline',
                //         name: 'Cantidad',
                //         yAxis: 1,
                //         data: dat1,
                //         color: '#C00000',
                //         lineWidth: 3

                //     }]
                // });

                // chart3.render();
            }
        });

    };
    /*entidades fin VCTM */

    var fs_fecha = function() {

    }

    var fs_pie_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                colores = [];
                if (obj.color == 'estado') {
                    colores = ColEstado = ['#5B9BD5', '#ED7D31', '#A5A5A5'];
                }
                data = [];
                dat = [];
                var simbolo = '';
                var d1 = 0;
                var d2 = 0;
                var d3 = 0;
                var mef = 465784.5;

                if (obj.fn == 'sum') {
                    simbolo = 'S/.'
                } else {
                    simbolo = ''
                }
                for (i = 0; i < response.data.length; i++) {
                    data.push(parseInt(response.data[i].count));
                }
                d1 = data[1] - data[0];
                d2 = data[3] - data[2];
                d3 = (data[7] + data[6]) - (data[5] + data[4]) + 9308;

                if (obj.div == 'pie1') {
                    dat.push({
                        'name': 'Martín Vizcarra (19mar2020-9nov2020, 236 días)',
                        'y': parseInt(d1),
                        'd': formatNumber(parseInt(d1)),
                        'color': colores[0]
                    });
                    dat.push({
                        'name': 'Manuel Merino (10nov2020-16nov2020, 7 días)',
                        'y': parseInt(d2),
                        'd': formatNumber(parseInt(d2)),
                        'color': colores[1]
                    });
                    dat.push({
                        'name': 'Francisco Sagasti (17nov2020-11abril2021, 146 días)',
                        'y': parseInt(d3),
                        'd': formatNumber(parseInt(d3)),
                        'color': colores[2]
                    });
                } else {
                    dat.push({
                        'name': 'Martín Vizcarra (19mar2020-9nov2020, 236 días)',
                        'y': parseInt(d1 * mef),
                        'd': formatNumber(parseInt(d1 * mef)),
                        'color': colores[0]
                    });
                    dat.push({
                        'name': 'Manuel Merino (10nov2020-16nov2020, 7 días)',
                        'y': parseInt(d2 * mef),
                        'd': formatNumber(parseInt(d2 * mef)),
                        'color': colores[1]
                    });
                    dat.push({
                        'name': 'Francisco Sagasti (17nov2020-11abril2021, 146 días)',
                        'y': parseInt(d3 * mef),
                        'd': formatNumber(parseInt(d3 * mef)),
                        'color': colores[2]
                    });
                }

                var chart1 = Highcharts.chart(obj.div, {
                    animationEnabled: true,
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                    },
                    title: {
                        text: obj.titulo
                    },

                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 220,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '12px',
                                    fontFamily: "Helvetica"
                                },
                                format: simbolo + '{point.d} : {point.percentage:.1f}% ',
                            },
                            showInLegend: true

                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: dat
                    }]
                });

                chart1.render();
            }
        });

    };

    var fs_chart1 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                // //(response);
                colores = [];
                if (obj.color == 'estado') {
                    colores = ColEstado = ['#00a2ff', '#9DC8F1'];
                } else if (obj.color == 'tipo') {
                    colores = ColTipoEjecucion = ['#55B1A5', '#f4a261'];
                } else if (obj.color == 'nivel') {
                    colores = ColNivel = ['#2561ba', '#0f6efc', '#4188f2'];
                }
                dat = [];
                var simbolo = '';
                if (obj.fn == 'sum') {
                    simbolo = 'S/.'
                } else {
                    simbolo = ''
                }

                for (i = 0; i < response.data.length; i++) {
                    dat.push({
                        'name': response.data[i].x,
                        'y': parseInt(response.data[i].sum),
                        'd': formatNumber(parseInt(response.data[i].sum)),
                        'color': colores[i]
                    });
                }
                //(dat);

                var chart1 = Highcharts.chart(obj.div, {
                    animationEnabled: true,
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                    },
                    title: {
                        text: ''
                    },

                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 180,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '12px',
                                    fontFamily: "Helvetica"
                                },
                                format: simbolo + '{point.d} : {point.percentage:.1f}% ',
                            },
                            showInLegend: true

                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: dat
                    }]
                });

                chart1.render();
            }
        });

    };
    var fs_chart2 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                colores = [];
                if (obj.color == 'estado') {
                    colores = ColEstado = ['#dc4340', '#0071bc'];
                } else if (obj.color == 'tipo') {
                    colores = ColTipoEjecucion = ['#008f39', '#f2cd00'];
                } else if (obj.color == 'nivel') {
                    colores = ColNivel = ['#03045e', '#023e8a', '#0077b6'];
                }
                data1 = [];
                data2 = [];
                var a = 0;
                var b = 0;
                if (obj.fn == 'sum') {
                    simbolo = 'Millones'
                } else {
                    simbolo = 'Cantidad'
                }

                for (i = 0; i < response.data.length; i++) {
                    a = a + parseInt(response.data[i].viable);
                    b = b + parseInt(response.data[i].ejecutable);

                    data1.push(a);
                    data2.push(b);
                }


                var chart1 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'area'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: ['2017', '2018', '2019', '2020', '2021'],
                        tickmarkPlacement: 'on',
                        title: {
                            enabled: false
                        }
                    },
                    yAxis: {
                        title: {
                            text: simbolo
                        },
                        labels: {
                            formatter: function() {
                                return this.value / 1000;
                            }
                        }
                    },
                    tooltip: {
                        split: true,
                        valueSuffix: ' ' + simbolo
                    },
                    plotOptions: {
                        area: {
                            stacking: 'normal',
                            lineColor: '#666666',
                            lineWidth: 1,
                            marker: {
                                lineWidth: 1,
                                lineColor: '#666666'
                            }
                        }
                    },
                    series: [{
                        name: 'Viable',
                        data: data1
                    }, {
                        name: 'Ejecutado',
                        data: data2
                    }, ]
                });

                chart1.render();
            }
        });

    };
    var fs_chart3 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                dat1 = [];
                dat2 = [];
                categoria = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
                for (i = 0; i < response.data.length; i++) {
                    // categoria.push(response.data[i].x);
                    dat1.push(parseInt(response.data[i].count));
                    dat2.push(parseInt(response.data[i].sum));
                }

                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Millones de Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            },
                            title: {
                                text: 'Millones de Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[2]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Monto',
                        data: dat2
                    }, {
                        type: 'spline',
                        name: 'Cantidad',
                        yAxis: 1,
                        data: dat1
                    }, {
                        type: 'spline',
                        name: 'Cantidad',

                        data: dat2
                    }]
                });
                chart3.render();
            }
        });
    };
    var fs_chart_oxigeno = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                dat1 = [];
                dat2 = [];
                dat3 = [];
                dat4 = [];
                dat5 = [];
                categoria = ['Q1', 'Q2', 'Q3', 'Q4', 'Q5'];
                for (i = 0; i < response.data.length; i++) {
                    // categoria.push(response.data[i].x);
                    dat2.push(parseInt(response.data[i].p));
                    dat3.push(parseInt(response.data[i].f));
                    dat4.push(parseInt(response.data[i].f10));
                    dat5.push(parseInt(response.data[i].d));
                }

                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Millares',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Población',
                        data: dat2,
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: 'Fallecidos',
                        yAxis: 1,
                        data: dat3,
                        color: 'red',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Casos(+) semanal',
                        yAxis: 1,
                        data: dat4,
                        color: '#ED7D31',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Total  m3/h',
                        yAxis: 1,
                        data: dat5,
                        color: '#00B050',
                        lineWidth: 3
                    }]
                });

                chart3.render();
            }
        });
    };
    var fs_chart_tipo_salud = function(obj) {

        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                //(response);
                // dat2 = [];
                var totalh = 0;
                var totalm = 0;
                var name1 = 'Hombres';
                var name2 = 'Mujeres';
                if (obj.name1 == 'sinadef-covid') {
                    name1 = 'SINADEF';
                    name2 = 'COVID-19';
                }

                var dath = []
                var datm = []

                categoria = ['72226 + años', '66-75 años', '56-65 años', '40-55 años', '25-39 años', '18-24 años', '13-17 años', '6-12 años', '0-5 años'];
                categorias = ['0-5 años', '6-12 años', '13-17 años', '18-24 años', '25-39 años', '40-55 años', '56-65 años', '66-75 años', '76 + años'];
                var negativo = 0;

                for (i = 0; i < response.data.length; i++) {
                    totalh = totalh + parseInt(response.data[i].sumh || ((response.data[i].h * 36.9) / 100) + response.data[i].h);
                    totalm = totalm + parseInt(response.data[i].summ || ((response.data[i].m * 36.9) / 100) + response.data[i].m);
                }
                for (i = 0; i < response.data.length; i++) {
                    negativo = response.data[i].sumh || response.data[i].h;
                    negativo = '-' + negativo;
                    dath.push(parseFloat((parseFloat(negativo / totalh) * 100).toFixed(2)));
                    datm.push(parseFloat(((parseFloat(response.data[i].summ || response.data[i].m) / totalm) * 100).toFixed(2)));
                }
                //(dath);
                //(datm);

                var chart9 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    accessibility: {
                        point: {
                            valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
                        }
                    },
                    xAxis: [{
                        categories: categorias,
                        reversed: false,
                        labels: {
                            step: 1
                        },
                        accessibility: {
                            description: 'Hombres'
                        }
                    }, { // mirror axis on right side
                        opposite: true,
                        reversed: false,
                        categories: categorias,
                        linkedTo: 0,
                        labels: {
                            step: 1
                        },
                        accessibility: {
                            description: 'Mujeres'
                        }
                    }],
                    yAxis: {
                        title: {
                            text: null
                        },
                        labels: {
                            formatter: function() {
                                return Math.abs(this.value) + '%';
                            }
                        },
                        accessibility: {
                            description: 'Percentage population',
                            rangeDescription: 'Range: 0 to 5%'
                        }
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },

                    tooltip: {
                        formatter: function() {
                            return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                                'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
                        }
                    },

                    series: [{
                        name: name1,
                        data: dath,
                        color: '#4E82BA'
                    }, {
                        name: name2,
                        data: datm,
                        color: '#BF514C'
                    }]
                });

                chart9.render();
            }
        });

    };
    var fs_chart_edad_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response2) {
                // console.log(response2);
                var categorias = [];
                var ratio = [];
                var obj2 = [];
                var obj3 = [];
                var obj4 = [];
                var obj5 = [];
                var obj6 = [];
                var obj7 = [];
                var obj8 = [];
                var obj9 = [];
                var obj10 = [];


                for (i = 0; i < response2.data.length; i++) {
                    // categorias.push(response2.data[i].semana);
                    // console.log(response2.data[i].tipo);
                    if (obj.tipo == "diario") {
                        categorias.push(response2.data[i].fallecidos);
                        obj2.push(parseInt(response2.data[i].c1));
                        obj3.push(parseInt(response2.data[i].c2));
                        obj4.push(parseInt(response2.data[i].c3));
                        obj5.push(parseInt(response2.data[i].c4));
                        obj6.push(parseInt(response2.data[i].c5));
                        obj7.push(parseInt(response2.data[i].c6));
                        obj8.push(parseInt(response2.data[i].c7));
                        obj9.push(parseInt(response2.data[i].c8));
                        obj10.push(parseInt(response2.data[i].c9));

                    } else if (obj.tipo == "semanal") {
                        categorias.push(response2.data[i].fallecidos);
                        obj2.push(parseInt(response2.data[i].c1));
                        obj3.push(parseInt(response2.data[i].c2));
                        obj4.push(parseInt(response2.data[i].c3));
                        obj5.push(parseInt(response2.data[i].c4));
                        obj6.push(parseInt(response2.data[i].c5));
                        obj7.push(parseInt(response2.data[i].c6));
                        obj8.push(parseInt(response2.data[i].c7));
                        obj9.push(parseInt(response2.data[i].c8));
                        obj10.push(parseInt(response2.data[i].c9));

                    } else if (obj.tipo == "mensual") {
                        categorias.push(response2.data[i].fallecidos);
                        obj2.push(parseInt(response2.data[i].c1));
                        obj3.push(parseInt(response2.data[i].c2));
                        obj4.push(parseInt(response2.data[i].c3));
                        obj5.push(parseInt(response2.data[i].c4));
                        obj6.push(parseInt(response2.data[i].c5));
                        obj7.push(parseInt(response2.data[i].c6));
                        obj8.push(parseInt(response2.data[i].c7));
                        obj9.push(parseInt(response2.data[i].c8));
                        obj10.push(parseInt(response2.data[i].c9));
                    }

                }

                // console.log(obj2);
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'spline',
                        scrollablePlotArea: {
                            minWidth: 1100,
                            scrollPositionX: 2
                        }
                    },
                    title: {
                        text: obj.titulo
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categorias,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: '0-5 años',
                        data: obj2,
                        color: '#5B9BD5',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: '6-12 años',
                        data: obj3,
                        color: '#ED7D31',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '13-17 años',
                        data: obj4,
                        color: '#A5A5A5',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '18-24 años',
                        data: obj5,
                        color: '#FFC000',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '25-39 años',
                        data: obj6,
                        color: '#4472C4',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '40-55 años',
                        data: obj7,
                        color: '#70AD47',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '56-65 años',
                        data: obj8,
                        color: '#255E91',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '66-75 años',
                        data: obj9,
                        color: '#9E480E',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: '>76 años',
                        data: obj10,
                        color: '#636363',
                        lineWidth: 3

                    }]
                });

                chart5.render();
            }
        });

    };


    var fs_chart3_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response1) {

                categorias = [];
                a = 0;
                obj5 = [];
                obj4 = [];

                for (i = 0; i < response1.data.length; i++) {
                    categorias.push(response1.data[i].mes);
                    if (response1.data[i].a2020 == null) {
                        response1.data[i].a2020 = 0;
                    }
                    a = a + parseInt(response1.data[i].a2020);
                    obj4.push(parseInt(response1.data[i].a2020));
                    obj5.push(a);
                }
                for (i = 0; i < 7; i++) {
                    categorias.push(response1.data[i].mes);

                    if (response1.data[i].a2021 == null) {
                        response1.data[i].a2021 = 0;
                    }
                    a = a + parseInt(response1.data[i].a2021);
                    if (i == 6) {
                        obj4.push((parseInt(response1.data[i - 1].a2021) * 30) / 100 + parseInt(response1.data[i - 1].a2021));
                    } else if (i < 6) {
                        obj4.push(parseInt(response1.data[i].a2021));

                    }
                    obj5.push(a);


                }
                // if (obj.flag == 'sinadef') {
                //     obj4.push(23406);
                //     obj5.push(150415);
                // } else {
                //     obj4.push(6404);
                //     obj5.push(53089);
                // }

                // categorias.push('Marzo');
                //(response1);
                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: 'Fallecidos COVID19 años 2020, 2021*, mensual y acumulado'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categorias,
                        gridLineWidth: 1

                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Cantidad',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'spline',
                        name: 'Fallecidos Mensuales',
                        data: obj4,
                        color: 'red',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Fallecidos Acumulados',
                        yAxis: 1,
                        data: obj5,
                        color: '#731d1d',
                        lineWidth: 3

                    }]
                });

                chart3.render();
            }
        });

    };
    var fs_chart3_salud_sinadef_logica = function(obj) {

        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response1) {
                categorias = [];
                a = 0;
                obj5 = [];
                obj4 = [];
                obj6 = [];
                x = 0;
                y = 0;
                z = 0;
                f = 0;
                for (i = 3; i < response1.data.length; i++) {
                    categorias.push(response1.data[i].mes);
                    x = parseInt(response1.data[i].a2020) - parseInt(response1.data[i].a2019);
                    if (x < 0) {
                        x == 0;
                    } else {
                        y = y + x;
                    }
                    obj4.push(x);
                    obj5.push(y);
                }
                for (i = 0; i < 7; i++) {
                    categorias.push(response1.data[i].mes);
                    if (i == 7) {
                        x = parseInt(((response1.data[i - 1].a2021 * 10) / 100) + parseInt(response1.data[i - 1].a2021)) - parseInt(response1.data[i].a2019);
                    } else {
                        x = parseInt((response1.data[i].a2021) - parseInt(response1.data[i].a2019));

                    }
                    if (x < 0) {
                        x == 0;
                    } else {
                        y = y + x;
                    }
                    obj4.push(x);
                    obj5.push(y);
                }


                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: 'Exceso de fallecidos 2020, 2021* vs línea base 2019, mensual y acumulado'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categorias,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Cantidad',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'spline',
                        name: 'Fallecidos Mensuales',
                        data: obj4,
                        color: 'red',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Fallecidos Acumulados',
                        yAxis: 1,
                        data: obj5,
                        color: '#731d1d',
                        lineWidth: 3

                    }]
                });

                chart3.render();
            }
        });

    };
    var fs_chart4_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response1) {
                //(response1);
                categorias = [];
                obj5 = [];
                obj4 = [];
                obj3 = [];
                var a = 0;
                var o = 0;
                var x = 0;
                for (i = 0; i < response1.data.length; i++) {
                    o++;
                    if (response1.data[i].fallecidos != null) {
                        if (i < 7) {
                            x = x + parseInt(response1.data[i].fallecidos);
                        } else {
                            x = (x = x + parseInt(response1.data[i].fallecidos)) - parseInt(response1.data[i - 7].fallecidos);
                        }
                        obj4.push(x);
                    }
                }

                for (i = 0; i < response1.data.length; i++) {
                    if (response1.data[i].fallecidos != null) {
                        categorias.push(response1.data[i].fecha);
                        obj3.push(parseInt(response1.data[i].fallecidos));
                        obj5.push(a = a + parseInt(response1.data[i].fallecidos));
                    }
                }
                // //(obj4);
                var chart5 = Highcharts.chart(obj.div, {
                    title: {
                        text: obj.texto
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categorias,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'spline',
                        name: 'Fallecidos diario 20-21',
                        data: obj3,
                        color: '#ED7D31',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Fallecidos semanal 20-21',
                        data: obj4,
                        color: '#ff0000',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: 'Acumulado fallecidos',
                        yAxis: 1,
                        data: obj5,
                        color: '#000',
                        lineWidth: 3

                    }]
                });

                chart5.render();
            }
        });

    };
    var fs_chart_proyecto_general = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                dat1 = [];
                dat2 = [];
                dat3 = [];
                dat4 = [];
                dat5 = [];
                categoria = [];
                datos = [];
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                c7 = 0;
                // categoria = ['Costo Actulizado', 'Devengado Acumulado', 'Saldo ejecutar', 'Saldor por financiar'];

                // categoria = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].nivel_gobierno || response.data[i].funcion);
                    dat1.push(parseInt(response.data[i].cantidad));
                    dat2.push(parseInt(response.data[i].costo));
                    dat3.push(parseInt(response.data[i].devengado));
                    dat4.push(parseInt(response.data[i].ejecutar));
                    dat5.push(parseInt(response.data[i].s_financiar));
                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].nivel_gobierno,
                        'c3': formatNumber(parseInt(response.data[i].cantidad)),
                        'c4': formatNumber(parseInt(response.data[i].costo)),
                        'c5': formatNumber(parseInt(response.data[i].devengado)),
                        'c6': formatNumber(parseInt(response.data[i].ejecutar)),
                        'c7': formatNumber(parseInt(response.data[i].s_financiar)),
                    });
                    c3 = c3 + parseInt(response.data[i].cantidad);
                    c4 = c4 + parseInt(response.data[i].costo);
                    c5 = c5 + parseInt(response.data[i].devengado);
                    c6 = c6 + parseInt(response.data[i].ejecutar);
                    c7 = c7 + parseInt(response.data[i].s_financiar);
                }
                datos.push({
                    'c1': ' ',
                    'c2': 'Total acumulado',
                    'c3': c3,
                    'c4': formatNumber(c4),
                    'c5': formatNumber(c5),
                    'c6': formatNumber(c6),
                    'c7': formatNumber(c7),
                });

                if (obj.flag == 'ok') {

                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#cuerpoTabla2');
                    // Recorrer todos los productos
                    $("#cuerpoTabla2 tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        let $c7 = document.createElement("td");
                        $c7.textContent = producto.c7; // el textContent del td es el nombre
                        $tr.appendChild($c7);

                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }
                // $cuerpoTabla = null;
                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Secondary yAxis
                            title: {
                                text: 'Cantidad Proyectos',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        }

                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Costo Actualizado',
                        data: dat2,

                        color: '#4472C4',
                        lineWidth: 3

                    }, {
                        type: 'column',
                        name: 'Devengado Acumulado',
                        data: dat3,

                        color: '#ED7D31',
                        lineWidth: 3
                    }, {
                        type: 'column',
                        name: 'Saldo ejecutar',
                        data: dat4,

                        color: '#BFBFBF',
                        lineWidth: 3
                    }, {
                        type: 'column',
                        name: 'Saldor por financiar',
                        data: dat5,

                        color: '#FF0000',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Cantidad',
                        yAxis: 1,
                        data: dat1,
                        color: '#C00000',
                        lineWidth: 3

                    }]
                });

                chart3.render();
            }
        });

    };
    var fs_chart_adjudicados = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                console.log(response);
                dat1 = [];
                dat2 = [];
                dat3 = [];
                dat4 = [];
                dat5 = [];
                categoria = [];
                datos = [];
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                c7 = 0;
                // categoria = ['Costo Actulizado', 'Devengado Acumulado', 'Saldo ejecutar', 'Saldor por financiar'];

                // categoria = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].empresa);
                    dat1.push(parseInt(response.data[i].monto2021));
                    dat2.push(parseInt(response.data[i].monto));
                    // dat3.push(parseInt(response.data[i].devengado));
                    // dat4.push(parseInt(response.data[i].ejecutar));
                    // dat5.push(parseInt(response.data[i].s_financiar));

                }


                // $cuerpoTabla = null;
                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Millones Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Secondary yAxis
                            title: {
                                text: 'Millones Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        }

                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Monto 2009-2021 (S/.)',
                        data: dat2,

                        color: '#4472C4',
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: 'Monto 2021 (S/.)',
                        yAxis: 1,
                        data: dat1,
                        color: '#C00000',
                        lineWidth: 3

                    }]
                });

                chart3.render();
            }
        });

    };
    var fs_chart4 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                dat1 = [];
                dat2 = [];
                dat3 = [];
                dat4 = [];
                dat5 = [];
                categoria = [];
                datos = [];
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                c7 = 0;
                // categoria = ['Costo Actulizado', 'Devengado Acumulado', 'Saldo ejecutar', 'Saldor por financiar'];

                // categoria = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].nivel_gobierno || response.data[i].funcion);
                    dat1.push(parseInt(response.data[i].cantidad));
                    dat2.push(parseInt(response.data[i].costo));
                    dat3.push(parseInt(response.data[i].devengado));
                    dat4.push(parseInt(response.data[i].ejecutar));
                    dat5.push(parseInt(response.data[i].s_financiar));
                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].nivel_gobierno,
                        'c3': formatNumber(parseInt(response.data[i].cantidad)),
                        'c4': formatNumber(parseInt(response.data[i].costo)),
                        'c5': formatNumber(parseInt(response.data[i].devengado)),
                        'c6': formatNumber(parseInt(response.data[i].ejecutar)),
                        'c7': formatNumber(parseInt(response.data[i].s_financiar)),
                    });
                    c3 = c3 + parseInt(response.data[i].cantidad);
                    c4 = c4 + parseInt(response.data[i].costo);
                    c5 = c5 + parseInt(response.data[i].devengado);
                    c6 = c6 + parseInt(response.data[i].ejecutar);
                    c7 = c7 + parseInt(response.data[i].s_financiar);
                }
                datos.push({
                    'c1': ' ',
                    'c2': 'Total acumulado',
                    'c3': c3,
                    'c4': formatNumber(c4),
                    'c5': formatNumber(c5),
                    'c6': formatNumber(c6),
                    'c7': formatNumber(c7),
                });

                if (obj.flag == 'ok') {

                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#' + obj.tabla);
                    // Recorrer todos los productos
                    $("#" + obj.tabla + " " + "tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        let $c7 = document.createElement("td");
                        $c7.textContent = producto.c7; // el textContent del td es el nombre
                        $tr.appendChild($c7);

                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }
                // $cuerpoTabla = null;
                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: 'Soles',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Secondary yAxis
                            title: {
                                text: 'Cantidad Proyectos',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        }

                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'column',
                        name: 'Costo Actualizado',
                        data: dat2,

                        color: '#4472C4',
                        lineWidth: 3

                    }, {
                        type: 'column',
                        name: 'Devengado Acumulado',
                        data: dat3,

                        color: '#ED7D31',
                        lineWidth: 3
                    }, {
                        type: 'column',
                        name: 'Saldo ejecutar',
                        data: dat4,

                        color: '#BFBFBF',
                        lineWidth: 3
                    }, {
                        type: 'column',
                        name: 'Saldor por financiar',
                        data: dat5,

                        color: '#FF0000',
                        lineWidth: 3
                    }, {
                        type: 'spline',
                        name: 'Cantidad',
                        yAxis: 1,
                        data: dat1,
                        color: '#C00000',
                        lineWidth: 3

                    }]
                });

                chart3.render();
            }
        });

    };
    var fs_tbl_empresa = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {


                c1 = 0;
                c2 = 0;
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                c7 = 0;
                c8 = 0;
                c9 = 0;
                c10 = 0;
                datos = [];
                // categoria = ['Costo Actulizado', 'Devengado Acumulado', 'Saldo ejecutar', 'Saldor por financiar'];

                // categoria = ['>= 5M', '>=2.5M a <5M', '>= 1M a < 2.5M', '>= 400K a < 1M', '>= 64.5K a < 400K', '>= 34.4K a < 64.5K', '< 34.4K'];
                for (i = 0; i < response.data.length; i++) {
                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].ruc,
                        'c3': response.data[i].razn_social,
                        'c4': response.data[i].ciiu,
                        'c5': response.data[i].sector_esp,
                        'c6': formatNumber(parseInt(response.data[i].abz)),
                        'c7': formatNumber(parseInt(response.data[i].facturado_2020_soles_maximo)),
                        'c8': formatNumber(parseInt(response.data[i].facturado_2019_soles_maximo)),
                        'c9': formatNumber(parseInt(response.data[i].facturado_2018_soles_maximo))
                    });
                }


                if (obj.flag == 'ok') {

                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#' + obj.tabla);
                    // Recorrer todos los productos
                    $("#" + obj.tabla + "tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        let $c7 = document.createElement("td");
                        $c7.textContent = producto.c7; // el textContent del td es el nombre
                        $tr.appendChild($c7);

                        let $c8 = document.createElement("td");
                        $c8.textContent = producto.c8; // el textContent del td es el nombre
                        $tr.appendChild($c8);

                        let $c9 = document.createElement("td");
                        $c9.textContent = producto.c9; // el textContent del td es el nombre
                        $tr.appendChild($c9);


                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }
                // $cuerpoTabla = null;

            }
        });

    };
    var fs_chart_oxigeno_acumuladom3h = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                (response);
                var ano = obj.ano;
                a2019 = [];
                a2020 = [];
                a2021 = [];
                bloque1 = [];
                categoria = [];
                data = [];
                for (i = 0; i < response.data.length - 1; i++) {
                    data.push(parseInt(response.data[i].m3h));
                    categoria.push(response.data[i].fecha_simulada);
                }

                var chart5 = Highcharts.chart(obj.div, {

                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        gridLineWidth: 1
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }, { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    series: [{
                        type: 'spline',
                        name: 'Demanda Oxigeno m3h',
                        data: data,
                        color: '#00B050',
                        lineWidth: 3
                    }]



                });
                chart5.render();
            }
        });
    }
    var fs_chart_calorv2 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                // console.log(response);
                var ano = obj.ano;
                a2019 = [];
                a2020 = [];
                a2021 = [];
                bloque1 = [];
                bloque2 = [];
                data = [];
                c = 0;
                x = 0;
                poblacion = [3211622, 3929211, 2642718, 3991527, 7758273, 6149050, 2478791, 1841828, 622866];
                for (i = 0; i < response.data.length; i++) {
                    if (response.data[i].ano == 2020) {
                        a2020.push(parseInt(response.data[i].sinadef));
                    } else if (response.data[i].ano == 2021) {
                        a2021.push(parseInt(response.data[i].sinadef));
                    }
                }
                for (i = 0; i < a2021.length; i++) {
                    a2020.push(a2021[i]);

                }
                // console.log(a2020);
                for (i = 0; i < a2020.length; i++) {

                    if (c == 9) {
                        c = 0;
                    }
                    x = (a2020[i] / poblacion[c]);
                    y = x * 100000;
                    bloque1.push(Math.round(y));
                    // bloque2.push(Math.round(a2021[i] / a2019[i]));
                    c++;
                }
                // console.log(bloque1);

                var x = 0;
                var y = 0;
                var z = 0;
                var v = 0;

                if (ano == '2020') {
                    for (i = 0; i < bloque1.length; i++) {
                        y++;

                        if ((i % 9) == 0) {
                            x++;
                            y = 0;
                        }
                        // //(bloque1[i]);
                        // //(y);
                        if (bloque1[i] < 0) {
                            var v = 0;
                            data.push([x - 1, y, v]);

                        } else {
                            data.push([x - 1, y, bloque1[i]]);
                        }

                    }
                } else if (ano == '2021') {
                    for (i = 0; i < bloque2.length; i++) {
                        y++;

                        if ((i % 9) == 0) {
                            x++;
                            y = 0;
                        }
                        // //(bloque2[i]);
                        // //(y);
                        if (bloque2[i] > 0) {
                            // //("asd");
                            var v = 0;
                            data.push([x - 1, y, bloque2[i]]);
                            // data.push([x - 1, y, v]);

                        } else {
                            data.push([x - 1, y, 0]);
                        }



                    }
                }

                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'heatmap',
                        marginTop: 40,
                        marginBottom: 80,
                        plotBorderWidth: 1
                    },
                    title: {
                        text: 'Tasa de fallecidos mensual por cada 100 mil habitantes, por rango de edad años 2020 y 2021* - Perú'
                    },
                    xAxis: {
                        categories: ['Ene-20', 'Feb-20', 'Mar-20', 'Abr-20', 'May-20', 'Jun-20', 'Jul-20', 'Ago-20', 'Sep-20', 'Oct-20', 'Nov-20', 'Dic-20', 'Ene-21', 'Feb-21', 'Mar-21', 'Abr-21', 'May-21', 'Jun-21']
                    },
                    yAxis: {
                        categories: ['0-5 años', '6-12 años', '13-17 años', '18-24 años', '25-39 años', '40-54 años', '55-64 años', '65-79 años', '80 + años'],
                        title: null,
                        reversed: true
                    },
                    accessibility: {
                        point: {
                            descriptionFormatter: function(point) {
                                var ix = point.index + 1,
                                    xName = getPointCategoryName(point, 'x'),
                                    yName = getPointCategoryName(point, 'y'),
                                    val = point.value;
                                return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                            }
                        }
                    },
                    colorAxis: {
                        stops: [
                            [0, '#faf5e6'],
                            [0.15, '#FFD966'],
                            [0.70, '#ffd23f'],
                            [0.85, '#ff5f59'],
                            [1, '#fc413a']
                        ],
                        min: Math.min.apply(Math, bloque1),
                        colormin: '#037E34',
                        colormax: '#FF0000',
                        max: Math.max.apply(Math, bloque1)
                    },
                    legend: {
                        align: 'right',
                        layout: 'vertical',
                        margin: 0,
                        verticalAlign: 'top',
                        y: 25,
                        symbolHeight: 280
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + getPointCategoryName(this.point, 'x') + '</b> sold <br><b>' +
                                this.point.value + '</b> items on <br><b>' + getPointCategoryName(this.point, 'y') + '</b>';
                        }
                    },
                    series: [{
                        name: 'Sales per employee',
                        borderWidth: 1,
                        data: data,
                        dataLabels: {
                            enabled: true,
                            color: '#000000'
                        }
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                yAxis: {
                                    labels: {
                                        formatter: function() {
                                            return this.value.charAt(0);
                                        }
                                    }
                                }
                            }
                        }]
                    }

                });
                chart5.render();
            }
        });
    }
    var fs_chart_calorv3 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                // console.log(response);
                var ano = obj.ano;
                // a2019 = [];
                a2020 = [];
                a2021 = [];
                bloque1 = [];
                categoriasss = [];
                // bloque2 = [];
                data = [];
                data2 = [];
                c = 0;
                x = 0;
                y = 0;
                x = 0;
                for (i = 0; i < response.data.length; i++) {


                    // if ((i % 25) == 0) {
                    //     x++;
                    //     y = 0;
                    // }

                    if (response.data[i].mes == "1") {
                        categoriasss.push(response.data[i].distrito || response.data[i].provincia || response.data[i].departamento);
                    }
                    if (i == 0) {
                        if (response.data[i].mes != response.data[1 - 1].mes) {
                            x++;
                            y = 0;
                            // flag = 1;

                            // categoriasss.push(response.data[i].departamentodomicilio);

                        }
                    } else {
                        if (response.data[i].mes != response.data[i - 1].mes) {
                            x++;
                            y = 0;

                            // categoriasss.push(response.data[i].departamentodomicilio);
                        }
                    }

                    // if (i > 0) {
                    //     if (response.data[i].mes != response.data[i - 1]) {
                    //         x++;
                    //         y = 0;
                    //     }
                    // }

                    if (response.data[i].e < 0) {
                        var v = 0;
                        data.push([x, y, v]);

                    } else {
                        bloque1.push(parseInt(response.data[i].e));
                        data.push([x, y, parseInt(response.data[i].e)]);
                        data2.push(parseInt(response.data[i].e));
                    }
                    y++;
                }


                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'heatmap',
                        marginTop: 40,
                        marginBottom: 80,
                        plotBorderWidth: 1
                    },
                    title: {
                        text: 'Tasa de fallecidos mensual por cada 100 mil habitantes, por departamento años 2020 y 2021* - Perú'
                    },
                    xAxis: {
                        categories: ['Ene-20', 'Feb-20', 'Mar-20', 'Abr-20', 'May-20', 'Jun-20', 'Jul-20', 'Ago-20', 'Sep-20', 'Oct-20', 'Nov-20', 'Dic-20', 'Ene-21', 'Feb-21', 'Mar-21', 'Abr-21', 'May-21', 'Jun-21']
                    },
                    yAxis: {
                        categories: categoriasss,
                        title: null,
                        reversed: true
                    },
                    accessibility: {
                        point: {
                            descriptionFormatter: function(point) {
                                var ix = point.index + 1,
                                    xName = getPointCategoryName(point, 'x'),
                                    yName = getPointCategoryName(point, 'y'),
                                    val = point.value;
                                return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                            }
                        }
                    },
                    colorAxis: {
                        stops: [
                            [0, '#faf5e6'],
                            [0.15, '#FFD966'],
                            [0.35, '#ffd23f'],
                            [0.75, '#ff5f59'],
                            [1, '#fc413a']
                        ],
                        min: Math.min.apply(Math, bloque1),
                        colormin: '#037E34',
                        colormax: '#FF0000',
                        max: Math.max.apply(Math, bloque1)
                    },
                    legend: {
                        align: 'right',
                        layout: 'vertical',
                        margin: 0,
                        verticalAlign: 'top',
                        y: 25,
                        symbolHeight: 280
                    },
                    tooltip: {
                        formatter: function() {
                            return '<b>' + getPointCategoryName(this.point, 'x') + '</b> sold <br><b>' +
                                this.point.value + '</b> items on <br><b>' + getPointCategoryName(this.point, 'y') + '</b>';
                        }
                    },
                    series: [{
                        name: 'Sales per employee',
                        borderWidth: 1,
                        data: data,
                        dataLabels: {
                            enabled: true,
                            color: '#000000'
                        }
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                yAxis: {
                                    labels: {
                                        formatter: function() {
                                            return this.value.charAt(0);
                                        }
                                    }
                                }
                            }
                        }]
                    }

                });
                chart5.render();
            }
        });
    }
    var fs_chart_sinadef_selector = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                // console.log(response);
                dat = [];
                categoria = [];
                for (i = 0; i < response.data.length; i++) {

                    obj.orden == 'pf' ? dat.push(parseInt(parseFloat(response.data[i].pf) * 100)) : dat.push(parseInt(parseFloat(response.data[i].ph)));
                    categoria.push(response.data[i].distrito || response.data[i].provincia || response.data[i].departamento)
                }

                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: obj.titulo
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria.reverse(),
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' %'
                    },
                    accessibility: {
                        point: {
                            descriptionFormatter: function(point) {
                                var ix = point.index + 1,
                                    xName = getPointCategoryName(point, 'x'),
                                    yName = getPointCategoryName(point, 'y'),
                                    val = point.value;
                                return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                            }
                        }
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    colorAxis: {
                        stops: [
                            [0, '#faf5e6'],
                            [0.15, '#FFD966'],
                            [0.35, '#ffd23f'],
                            [0.75, '#ff5f59'],
                            [1, '#fc413a']
                        ]
                    },
                    series: [{
                        name: '',
                        color: 'black',
                        data: dat.reverse()
                    }]

                });
                chart5.render();
            }
        });
    }

    var fs_chart_vacunas_selector = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                dat = [];
                datos = [];
                c2 = 0;
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                max = 0;
                len = response.data.length;
                categoria = [];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep);
                    dat.push(parseInt(response.data[i].dias));
                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep,
                        'c3': formatNumber(parseInt(response.data[i].poblacion_mayor)),
                        'c4': formatNumber(parseInt(response.data[i].pendientes)),
                        'c5': formatNumber(parseInt(response.data[i].velocidad)),
                        'c6': formatNumber(parseInt(response.data[i].dias))
                    });
                    c2 = c2 + parseInt(response.data[i].poblacion_mayor);
                    c3 = c3 + parseInt(response.data[i].pendientes);
                    c4 = c4 + parseInt(response.data[i].velocidad);
                }
                max = Math.max(...dat);
                datos.push({
                    'c1': ' ',
                    'c2': 'Total',
                    'c3': formatNumber(c2),
                    'c4': formatNumber(c3),
                    'c5': formatNumber(c4),
                    'c6': ' '

                });
                console.log(max);
                if (obj.flag == 'ok') {

                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#tablavacuna2');
                    // Recorrer todos los productos
                    $("#tablavacuna2 tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }

                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar',
                        scrollablePlotArea: {
                            Width: 400,
                            scrollPositionX: 1
                        }
                    },
                    title: {
                        text: obj.titulo
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        title: {
                            text: null
                        },
                        min: 0,
                        max: len - 8,
                        scrollbar: {
                            enabled: true
                        },
                        tickLength: 0
                    },
                    yAxis: {
                        min: 0,
                        max: max,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' Días'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    accessibility: {
                        point: {
                            descriptionFormatter: function(point) {
                                var ix = point.index + 1,
                                    xName = getPointCategoryName(point, 'x'),
                                    yName = getPointCategoryName(point, 'y'),
                                    val = point.value;
                                return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    colorAxis: {
                        stops: [
                            [0, '#FFE699'],
                            [0.25, '#FFC000'],
                            [0.50, '#ED7D31'],
                            [0.75, '#FF0000'],
                            [1, '#800000']
                        ]
                    },
                    series: [{
                        name: '',
                        color: 'black',
                        data: dat
                    }]

                });
                chart5.render();
            }
        });
    }
    var fs_chart_vacunas_selector2 = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                dat1 = [];
                dat2 = [];
                dat3 = [];
                datos = [];
                c2 = 0;
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                max = 0;
                len = response.data.length;
                categoria = [];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep);
                    dat1.push(parseFloat(response.data[i].machito));
                    dat2.push(parseFloat(response.data[i].hembrita));
                    dat3.push(parseFloat(response.data[i].tot));

                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep,
                        'c3': formatNumber(parseInt(response.data[i].poblacion_mayor)),
                        'c4': formatNumber(parseInt(response.data[i].macho)),
                        'c5': formatNumber(parseInt(response.data[i].hembra)),
                        'c6': formatNumber((parseInt(response.data[i].macho) + parseInt(response.data[i].hembra)))
                    });
                    c2 = c2 + parseInt(response.data[i].poblacion_mayor);
                    c3 = c3 + parseInt(response.data[i].macho);
                    c4 = c4 + parseInt(response.data[i].hembra);
                    c5 = c5 + (parseInt(response.data[i].macho) + parseInt(response.data[i].hembra))
                }
                max = Math.max(...dat3);
                datos.push({
                    'c1': ' ',
                    'c2': 'Total',
                    'c3': formatNumber(c2),
                    'c4': formatNumber(c3),
                    'c5': formatNumber(c4),
                    'c6': formatNumber(c5),

                });
                if (obj.flag == 'ok') {
                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#tablavacuna1');
                    // Recorrer todos los productos
                    $("#tablavacuna1 tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }

                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }
                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: obj.titulo
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        title: {
                            text: null
                        },
                        min: 0,
                        max: len - 8,
                        scrollbar: {
                            enabled: true
                        },
                        tickLength: 0
                    },
                    yAxis: {
                        min: 0,
                        max: max,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            formatter: function() {
                                return Math.abs(this.value) + '%';
                            }
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        bar: {

                            dataLabels: {
                                enabled: true,
                                format: '{y} %'
                            }
                        }
                    },
                    series: [{
                        name: '% Total x regiones',
                        color: '#4472C4',
                        data: dat3
                    }]

                });
                chart5.render();
            }
        });
    }
    var fs_chart_oxigeno_selector = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                console.log(response);
                dat = [];
                datos = [];
                c2 = 0;
                c3 = 0;
                c4 = 0;
                c5 = 0;
                c6 = 0;
                categoria = [];
                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep);
                    dat.push(parseInt(response.data[i].demanda));
                    datos.push({
                        'c1': i + 1,
                        'c2': response.data[i].nombdist || response.data[i].nombprov || response.data[i].nombdep,
                        'c3': parseInt(response.data[i].sum),
                        'c4': parseInt(response.data[i].avg),
                        'c5': parseInt(response.data[i].pacientes),
                        'c6': parseInt(response.data[i].demanda)
                    });
                    c2 = c2 + parseInt(response.data[i].sum);
                    c3 = c3 + parseInt(response.data[i].avg);
                    c4 = c4 + parseInt(response.data[i].pacientes);
                    c5 = c5 + parseInt(response.data[i].demanda);

                }
                datos.push({
                    'c1': ' ',
                    'c2': 'Total',
                    'c3': c2,
                    'c4': c3,
                    'c5': c4,
                    'c6': c5

                });
                if (obj.flag == 'ok') {

                    // Ahora dibujamos la tabla
                    let $cuerpoTabla = document.querySelector('#tabla_oxigeno');
                    // Recorrer todos los productos
                    $("#tabla_oxigeno tr").remove();
                    // var a = 0;
                    datos.forEach(producto => {
                        // Crear un <tr>
                        // a++;
                        const $tr = document.createElement("tr");
                        // Creamos el <td> de nombre y lo adjuntamos a tr
                        let $c1 = document.createElement("td");
                        $c1.textContent = producto.c1; // el textContent del td es el nombre
                        $tr.appendChild($c1);

                        let $c2 = document.createElement("td");
                        $c2.textContent = producto.c2; // el textContent del td es el nombre
                        $tr.appendChild($c2);

                        let $c3 = document.createElement("td");
                        $c3.textContent = producto.c3; // el textContent del td es el nombre
                        $tr.appendChild($c3);

                        let $c4 = document.createElement("td");
                        $c4.textContent = producto.c4; // el textContent del td es el nombre
                        $tr.appendChild($c4);

                        let $c5 = document.createElement("td");
                        $c5.textContent = producto.c5; // el textContent del td es el nombre
                        $tr.appendChild($c5);

                        let $c6 = document.createElement("td");
                        $c6.textContent = producto.c6; // el textContent del td es el nombre
                        $tr.appendChild($c6);

                        // El td de precio
                        // Finalmente agregamos el <tr> al cuerpo de la tabla
                        $cuerpoTabla.appendChild($tr);
                        // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                    });

                }

                function getPointCategoryName(point, dimension) {
                    var series = point.series,
                        isY = dimension === 'y',
                        axis = series[isY ? 'yAxis' : 'xAxis'];
                    return axis.categories[point[isY ? 'y' : 'x']];
                }

                var chart5 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: obj.titulo
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: ' m3/h'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },
                    accessibility: {
                        point: {
                            descriptionFormatter: function(point) {
                                var ix = point.index + 1,
                                    xName = getPointCategoryName(point, 'x'),
                                    yName = getPointCategoryName(point, 'y'),
                                    val = point.value;
                                return ix + '. ' + xName + ' sales ' + yName + ', ' + val + '.';
                            }
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    colorAxis: {
                        stops: [
                            [0, '#FFE699'],
                            [0.02, '#FFC000'],
                            [0.08, '#ED7D31'],
                            [0.10, '#FF0000'],
                            [1, '#800000']
                        ]
                    },
                    series: [{
                        name: '',
                        color: 'black',
                        data: dat
                    }]

                });
                chart5.render();
            }
        });
    }
    var fs_chart_vacuna_salud = function(obj) {

        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                //(response);
                // dat2 = [];
                // var totalh = 0;
                // var totalm = 0;
                // var name1 = 'Hombres';
                // var name2 = 'Mujeres';
                // if (obj.name1 == 'sinadef-covid') {
                //     name1 = 'SINADEF';
                //     name2 = 'COVID-19';
                // }

                // var dath = []
                // var datm = []

                // categoria = ['72226 + años', '66-75 años', '56-65 años', '40-55 años', '25-39 años', '18-24 años', '13-17 años', '6-12 años', '0-5 años'];
                categorias = ['18-24', '25-39', '40-54', '56-64', '65-79', '80 + '];
                // var negativo = 0;

                // for (i = 0; i < response.data.length; i++) {
                //     totalh = totalh + parseInt(response.data[i].sumh || ((response.data[i].h * 36.9) / 100) + response.data[i].h);
                //     totalm = totalm + parseInt(response.data[i].summ || ((response.data[i].m * 36.9) / 100) + response.data[i].m);
                // }
                // for (i = 0; i < response.data.length; i++) {
                //     negativo = response.data[i].sumh || response.data[i].h;
                //     negativo = '-' + negativo;
                //     dath.push(parseFloat((parseFloat(negativo / totalh) * 100).toFixed(2)));
                //     datm.push(parseFloat(((parseFloat(response.data[i].summ || response.data[i].m) / totalm) * 100).toFixed(2)));
                // }

                var datam = [53, 49, 60, 126, 1227, 2932]
                var chart9 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    accessibility: {
                        point: {
                            valueDescriptionFormat: ' {value}'
                        }
                    },
                    xAxis: [{
                        categories: categorias,
                        reversed: false,
                        labels: {
                            step: 1
                        },
                        accessibility: {
                            description: 'Hombres'
                        }
                    }],

                    yAxis: {
                        title: {
                            text: 'Dias',
                            align: 'high'
                        },
                        labels: {
                            formatter: function() {
                                return Math.abs(this.value) + '';
                            }
                        },
                        accessibility: {
                            description: 'Percentage population',
                            rangeDescription: 'Range: 0 to 5%'
                        }
                    },

                    plotOptions: {

                        bar: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    tooltip: {
                        formatter: function() {
                            return 'Estimado de días: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '';
                        }
                    },

                    series: [
                        //     name: name1,
                        //     data: dath,
                        //     color: '#4E82BA'
                        // },
                        {
                            name: 'Rango de edades',
                            data: datam.reverse(),
                            color: '#4472C4'
                        }
                    ]
                });

                chart9.render();
            }
        });

    };
    var fs_chart_vacuna_genero_salud = function(obj) {

        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                //(response);
                // dat2 = [];
                // var totalh = 0;
                // var totalm = 0;
                // var name1 = 'Hombres';
                // var name2 = 'Mujeres';
                // if (obj.name1 == 'sinadef-covid') {
                //     name1 = 'SINADEF';
                //     name2 = 'COVID-19';
                // }

                // var dath = []
                // var datm = []

                // categoria = ['72226 + años', '66-75 años', '56-65 años', '40-55 años', '25-39 años', '18-24 años', '13-17 años', '6-12 años', '0-5 años'];
                categorias = ['18-24 años', '25-39 años', '40-54 años', '55-64 años', '65-79 años', '80 + años'];
                // var negativo = 0;

                // for (i = 0; i < response.data.length; i++) {
                //     totalh = totalh + parseInt(response.data[i].sumh || ((response.data[i].h * 36.9) / 100) + response.data[i].h);
                //     totalm = totalm + parseInt(response.data[i].summ || ((response.data[i].m * 36.9) / 100) + response.data[i].m);
                // }
                // for (i = 0; i < response.data.length; i++) {
                //     negativo = response.data[i].sumh || response.data[i].h;
                //     negativo = '-' + negativo;
                //     dath.push(parseFloat((parseFloat(negativo / totalh) * 100).toFixed(2)));
                //     datm.push(parseFloat(((parseFloat(response.data[i].summ || response.data[i].m) / totalm) * 100).toFixed(2)));
                // }

                var datah = [47.53, 42.16, 40.18, 32.71, 7.83, 2.92];
                var datam = [31.91, 38.53, 36.95, 29.01, 6.37, 3.56];

                var chart9 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    accessibility: {
                        point: {
                            valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
                        }
                    },
                    xAxis: [{
                        categories: categorias,
                        reversed: false,
                        labels: {
                            step: 1
                        },
                        accessibility: {
                            description: 'Hombres'
                        }
                    }],
                    yAxis: {
                        title: {
                            text: null
                        },
                        labels: {
                            formatter: function() {
                                return Math.abs(this.value) + '%';
                            }
                        },
                        accessibility: {
                            description: 'Percentage population',
                            rangeDescription: 'Range: 0 to 5%'
                        }
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: true,
                                format: '{y} %'
                            }
                        }
                    },

                    tooltip: {
                        formatter: function() {
                            return 'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
                        }
                    },

                    series: [{
                            name: 'Hombres',
                            data: datam.reverse(),
                            color: '#4472C4'
                        },
                        {
                            name: 'Mujeres',
                            data: datah.reverse(),
                            color: '#FC7498'
                        }
                    ]
                });

                chart9.render();
            }
        });

    };
    var fs_chart_vacuna_genero_salud2 = function(obj) {

        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                //(response);
                // dat2 = [];
                // var totalh = 0;
                // var totalm = 0;
                // var name1 = 'Hombres';
                // var name2 = 'Mujeres';
                // if (obj.name1 == 'sinadef-covid') {
                //     name1 = 'SINADEF';
                //     name2 = 'COVID-19';
                // }

                // var dath = []
                // var datm = []

                // categoria = ['72226 + años', '66-75 años', '56-65 años', '40-55 años', '25-39 años', '18-24 años', '13-17 años', '6-12 años', '0-5 años'];
                categorias = ['18-24', '25-39', '40-54', '55-64', '65-79', '80 + '];
                // var negativo = 0;

                // for (i = 0; i < response.data.length; i++) {
                //     totalh = totalh + parseInt(response.data[i].sumh || ((response.data[i].h * 36.9) / 100) + response.data[i].h);
                //     totalm = totalm + parseInt(response.data[i].summ || ((response.data[i].m * 36.9) / 100) + response.data[i].m);
                // }
                // for (i = 0; i < response.data.length; i++) {
                //     negativo = response.data[i].sumh || response.data[i].h;
                //     negativo = '-' + negativo;
                //     dath.push(parseFloat((parseFloat(negativo / totalh) * 100).toFixed(2)));
                //     datm.push(parseFloat(((parseFloat(response.data[i].summ || response.data[i].m) / totalm) * 100).toFixed(2)));
                // }

                var datah = [79.44, 80.70, 77.13, 61.73, 14.20, 6.48];

                var chart9 = Highcharts.chart(obj.div, {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    accessibility: {
                        point: {
                            valueDescriptionFormat: '{value}.'
                        }
                    },
                    xAxis: [{
                        categories: categorias,
                        reversed: false,
                        labels: {
                            step: 1
                        },
                        accessibility: {
                            description: 'Hombres'
                        }
                    }],
                    yAxis: {
                        title: {
                            text: '% Avance',
                            align: 'high'
                        },
                        labels: {
                            formatter: function() {
                                return Math.abs(this.value) + '%';
                            }
                        },
                        accessibility: {
                            description: 'Percentage population',
                            rangeDescription: 'Range: 0 to 5%'
                        }
                    },

                    plotOptions: {

                        bar: {
                            dataLabels: {
                                enabled: true,
                                format: '{y} %'
                            }
                        }
                    },
                    tooltip: {
                        formatter: function() {
                            return 'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
                        }
                    },

                    series: [{
                        name: 'Rango de edades',
                        data: datah.reverse(),
                        color: '#4472C4'
                    }]
                });

                chart9.render();
            }
        });

    };
    var fs_chart_vacuna = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {

                dat1 = [];
                dat2 = [];
                // dat3 = [];
                // dat4 = [];
                // dat5 = [];
                var x = 0;
                categoria = ['07-Mar', '14-Mar', '21-Mar', '28-Mar', '4-Abr', '11Abr', '18-Abr', '25-Abr', '02-may', '09-May', '16-May', '23-May', '30-May', '06-Jun', '13-Jun', '20-Jun', '27-Jun', '04-Jul'];
                for (i = 0; i < response.data.length; i++) {
                    // categoria.push(response.data[i].x);
                    dat1.push(parseInt(response.data[i].sum));
                    x = x + parseInt(response.data[i].sum)
                    dat2.push(x);
                }

                var chart3 = Highcharts.chart(obj.div, {
                    title: {
                        text: ''
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        categories: categoria
                    },
                    yAxis: [{ // Secondary yAxis
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[0]
                                }
                            },
                            opposite: true
                        },
                        { // Primary yAxis
                            labels: {
                                format: '{value:,.0f}',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            },
                            title: {
                                text: '',
                                style: {
                                    color: Highcharts.getOptions().colors[1]
                                }
                            }
                        }
                    ],
                    labels: {
                        items: [{
                            html: '',
                            style: {
                                left: '50px',
                                top: '18px',
                                color: ( // theme
                                    Highcharts.defaultOptions.title.style &&
                                    Highcharts.defaultOptions.title.style.color
                                ) || 'black'
                            }
                        }]
                    },
                    plotOptions: {
                        column: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{y:,.0f}'
                            }
                        }
                    },
                    series: [{
                        type: 'column',
                        name: 'Vacunados Semanal',
                        data: dat1,
                        lineWidth: 3

                    }, {
                        type: 'spline',
                        name: 'Acumulado ',
                        yAxis: 1,
                        data: dat2,
                        color: '#00B050',
                        lineWidth: 3
                    }]
                });

                chart3.render();
            }
        });
    };
    var fs_pie_vacuna_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                colores = [];
                if (obj.color == 'estado') {
                    colores = ColEstado = ['#4472C4', '#ED7D31', '#A5A5A5', '#FFC000', '#5B9BD5', '#70AD47'];
                }
                data = [];
                dat = [];
                var simbolo = '';
                var d1 = 0;
                var d2 = 0;
                var d3 = 0;
                var mef = 465784.5;

                if (obj.fn == 'sum') {
                    simbolo = 'S/.'
                } else {
                    simbolo = ''
                }
                for (i = 0; i < response.data.length; i++) {
                    data.push(parseInt(response.data[i].count));
                }
                d1 = data[1] - data[0];
                d2 = data[3] - data[2];
                d3 = (data[7] + data[6]) - (data[5] + data[4]) + 9308;

                if (obj.div == 'chartvacuna_4') {
                    dat.push({
                        'name': '18-24 años',
                        'y': parseInt(240256),
                        'd': formatNumber(parseInt(240256)),
                        'color': colores[0]
                    });
                    dat.push({
                        'name': '25-39 años',
                        'y': parseInt(1099256),
                        'd': formatNumber(parseInt(1099256)),
                        'color': colores[1]
                    });
                    dat.push({
                        'name': '40-54 años',
                        'y': parseInt(3668475),
                        'd': formatNumber(parseInt(3668475)),
                        'color': colores[2]
                    });
                    dat.push({
                        'name': '55-64 años',
                        'y': parseInt(2103121),
                        'd': formatNumber(parseInt(2103121)),
                        'color': colores[3]
                    });
                    dat.push({
                        'name': '65-79 años',
                        'y': parseInt(1842894),
                        'd': formatNumber(parseInt(1842894)),
                        'color': colores[4]
                    });
                    dat.push({
                        'name': '80+ años',
                        'y': parseInt(514248),
                        'd': formatNumber(parseInt(514248)),
                        'color': colores[5]
                    });

                }

                var chart1 = Highcharts.chart(obj.div, {
                    animationEnabled: true,
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                    },
                    title: {
                        text: obj.titulo
                    },

                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 220,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '12px',
                                    fontFamily: "Helvetica"
                                },
                                format: simbolo + '{point.d} : {point.percentage:.1f}% ',
                            },
                            showInLegend: true

                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: dat
                    }]
                });

                chart1.render();
            }
        });

    };

    var sort = function(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable");
        switching = true;
        //Set the sorting direction to ascending:
        dir = "asc";
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /*Loop through all table rows (except the
            first, which contains table headers):*/
            for (i = 1; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /*check if the two rows should switch place,
                based on the direction, asc or desc:*/
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                //Each time a switch is done, increase this count by 1:
                switchcount++;
            } else {
                /*If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again.*/
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }

    var fs_pie2_vacuna_salud = function(obj) {
        $.getJSON({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                colores = [];
                if (obj.color == 'estado') {
                    colores = ColEstado = ['#4472C4', '#ED7D31', '#A5A5A5', '#FFC000', '#5B9BD5', '#70AD47', '#264478', '#9E480E', '#636363'];
                }
                data = [];
                dat = [];
                var simbolo = '';
                var d1 = 0;
                var d2 = 0;
                var d3 = 0;

                for (i = 0; i < response.data.length; i++) {
                    categoria.push(response.data[i].grupo_riesgo);
                    dat.push({
                        'name': response.data[i].grupo_riesgo,
                        'y': parseInt(response.data[i].cantidad),
                        'd': formatNumber(parseInt(response.data[i].cantidad)),
                        'color': colores[i]
                    });


                }



                var chart1 = Highcharts.chart(obj.div, {
                    animationEnabled: true,
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie',
                    },
                    title: {
                        text: obj.titulo
                    },

                    tooltip: {
                        pointFormat: '<b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            size: 220,
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    fontSize: '12px',
                                    fontFamily: "Helvetica"
                                },
                                format: simbolo + '{point.d} : {point.percentage:.1f}% ',
                            },
                            showInLegend: true

                        }
                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: dat
                    }]
                });

                chart1.render();
            }
        });

    };
    var events = function(params) {
        switch (params.id) {
            case 'principal':
                var htmTable1 = `
               
        <div class="container" style="padding-left: 200px;padding-right : 200px;margin-top: 80px;">
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Empresas</h4>
                    </div>
                    <div class="card-body" style="text-align:center">
                        <div class="row">
                            <div class="col-lg-12">
                                <a id="empresa_dash" href="#" onclick="App.events(this);"> <img src="../assets/app/img/iconos-Empresas.png" alt="" loading="lazy"></a>
                            </div>
                            <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                <p style="text-align:center">
                                    Indicadores de Empresas
                                    privadas con y sin
                                    participación el mecanismo
                                    OxI, registrados en
                                    PROINVERSIÓN y Ranking de
                                    empresas en el Perú.
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Proyectos</h4>
                    </div>
                    <div class="card-body" style="text-align:center">
                        <div class="row">
                            <div class="col-lg-12">
                                <a id="proyectos_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Proyectos.png" alt="" loading="lazy"></a>
                            </div>
                            <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                <p style="text-align:center">
                                    Indicadores de Perfiles de
                                    proyectos de inversión por
                                    función, división funciona y
                                    grupo funcional, sector, área
                                    geográfica de intervención y
                                    proyectos priorizados,
                                    registrados en INVIERTE PE.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Entidades</h4>
                    </div>
                    <div class="card-body" style="text-align:center">
                        <div class="row">
                            <div class="col-lg-12">
                                <a id="entidades_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Entidades.png" width="165" alt="" loading="lazy"></i></a>
                            </div>
                            <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                <p style="text-align:center">
                                    Indicadores de Entidades por
                                    niveles de gobierno con
                                    proyectos de inversión con
                                    perfiles viables con y sin
                                    ejecución, registrados en el
                                    MEF.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <br>
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Salud</h4>
                    </div>
                    <div class="card-body" style="text-align:center">
                        <div class="row">
                            <div class="col-lg-12">
                                <a id="salud_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Salud.png" width="125" alt="" loading="lazy"></a>
                            </div>
                            <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                <p style="text-align:center">
                                    Indicadores del sector
                                    Salud, impacto del
                                    COVID19 por niveles de
                                    gobierno, según registro
                                    MINSA y SINADEF.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Política</h4>
                    </div>
                    <div class="card-body" style="text-align:center">
                        <div class="row">
                            <div class="col-lg-12">
                                <a id="politica_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/Icono_Politica.png" width="106" alt="" loading="lazy"></i></a>
                            </div>
                            <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                <p style="text-align:center">
                                    Indicadores de Políticas Públicas por niveles de gobierno, información para oportunidades que pueden ser aprovechadas en la gestión pública
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>`;
                $('#content_dashboard').html(htmTable1);
                break;
            case 'empresa_dash':
                changeTabPanelEstadistica("empresa");
                $.post({
                    url: 'empresas_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;

                        var objChart10 = {
                            div: 'chartbrecha_10',
                            method: 'empresas_listado',
                            flag: 'ok',
                            tabla: 'tabla_empresas'

                        };
                        fs_tbl_empresa(objChart10);

                        var obj = {
                            method: 'macro_top'
                        }
                        var oxi = [];
                        var utilidad = [];
                        var categories = [];
                        var categories1 = [];
                        var oxi1 = [];
                        var utilidad1 = [];

                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                var htmTable1 = `<table class="table table-sm table-detail">
												<tr>
													<th style="background:#ddebf8;text-align:center"><b>#</b></th>
													<th style="background:#ddebf8;text-align:center"><b>RUC</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Razon Social</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Fuente</b></th>
                                                </tr>`;
                                var htmTable2 = `<table class="table table-sm table-detail">
												<tr>
                                                    <th style="background:#ddebf8;text-align:center"><b>#</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>RUC</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Razon Social</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Fuente</b></th>
                                                </tr>`;
                                var cont = 0;
                                var cont1 = 0;
                                var sumOxi = 0;
                                var sumUtilidad = 0;
                                var sumOxi1 = 0;
                                var sumUtilidad1 = 0;
                                $.each(response.data, function(i, obj) {
                                    if (obj.participa == 'Participa' && oxi.length < 25) {
                                        cont++;
                                        oxi.push(parseInt(obj.oxi));
                                        utilidad.push(parseInt(obj.utilidad));
                                        categories.push(obj.razon_social);
                                        sumOxi += parseInt(obj.oxi);
                                        sumUtilidad += parseInt(obj.utilidad);
                                        htmTable1 += '<tr>';
                                        htmTable1 += '<td style="text-align:center">' + cont + '</td>';
                                        htmTable1 += '<td style="text-align:center"><button class="btn btn-link lnkModalEmpresas" id="' + obj.ruc + '" onclick="App.events(this);" data-toggle="modal" data-target="#exampleModal">' + obj.ruc + '</button></td>';
                                        htmTable1 += '<td style="text-align:left">' + obj.razon_social + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                        htmTable1 += '<td style="text-align:left">' + obj.fuente + '</td>';
                                        htmTable1 += '</tr>';

                                    } else if (obj.participa == 'No Participa' && oxi1.length < 25) {
                                        cont1++;
                                        oxi1.push(parseInt(obj.oxi));
                                        utilidad1.push(parseInt(obj.utilidad));
                                        categories1.push(obj.razon_social);
                                        sumOxi1 += parseInt(obj.oxi);
                                        sumUtilidad1 += parseInt(obj.utilidad);
                                        htmTable2 += '<tr>';
                                        htmTable2 += '<td style="text-align:center">' + cont1 + '</td>';
                                        htmTable2 += '<td style="text-align:center"><button class="btn btn-link lnkModalEmpresas" id="' + obj.ruc + '" onclick="App.events(this);" data-toggle="modal" data-target="#exampleModal">' + obj.ruc + '</button></td>';
                                        htmTable2 += '<td style="text-align:left">' + obj.razon_social + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                        htmTable2 += '<td style="text-align:left">' + obj.fuente + '</td>';
                                        htmTable2 += '</tr>';
                                    }
                                });
                                htmTable1 += '<tr>';
                                htmTable1 += '<td style="text-align:center">Total</td>';
                                htmTable1 += '<td style="text-align:center"></td>';
                                htmTable1 += '<td style="text-align:left"></td>';
                                htmTable1 += '<td style="text-align:center">' + sumOxi.toLocaleString('es-MX') + '</td>';
                                htmTable1 += '<td style="text-align:center">' + sumUtilidad.toLocaleString('es-MX') + '</td>';
                                htmTable1 += '<td style="text-align:left"></td>';
                                htmTable1 += '</tr>';
                                htmTable1 += `
                                            <tr>
                                                <td colspan="6"> 
                                                    <table class="table table-sm table-detail">
                                                    <tr>
                                                        <th style="background:#ddebf8;text-align:center">*</th>
                                                        <th style="text-align:left">Data calculada a partir de los ingresos (*0.3 dividido entre 2) <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="background:#ddebf8;text-align:center">**</th>
                                                        <th style="text-align:left">Data mostrada por el <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>`;
                                $('#html_empresas_top_25').html(htmTable1);
                                htmTable2 += '<tr>';
                                htmTable2 += '<td style="text-align:center">Total</td>';
                                htmTable2 += '<td style="text-align:center"></td>';
                                htmTable2 += '<td style="text-align:left"></td>';
                                htmTable2 += '<td style="text-align:center">' + sumOxi1.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:center">' + sumUtilidad1.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:left"></td>';
                                htmTable2 += '</tr>';
                                htmTable2 += `
                                <tr>
                                    <td colspan="6"> 
                                        <table class="table table-sm ">
                                        <tr>
                                            <th style="background:#ddebf8;text-align:center">*</th>
                                            <th style="text-align:left">Data calculada a partir de los ingresos (*0.3 dividido entre 2) <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                        </tr>
                                        <tr>
                                            <th style="background:#ddebf8;text-align:center">**</th>
                                            <th style="text-align:left">Data mostrada por el <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>`;
                                $('#html_empresas_top_25_no').html(htmTable2);
                                var obj = {
                                    titulo: 'Top empresas con mayor utilidad al 2019',
                                    subtitulo: 'Con participacion en Oxi',
                                    categories: categories,
                                    oxi: oxi,
                                    utilidad: utilidad,
                                    div: 'empresas_top_25'
                                }

                                dashBoardEmpresas_top(obj);
                                var obj = {
                                    titulo: 'Top empresas con mayor utilidad al 2019',
                                    subtitulo: 'Sin participacion en Oxi',
                                    categories: categories1,
                                    oxi: oxi1,
                                    utilidad: utilidad1,
                                    div: 'empresas_top_25_no'
                                }

                                dashBoardEmpresas_top(obj);

                            }
                        });

                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2' })),
                            success: function(response) {
                                var cont = 0;
                                var sumOxi = 0;
                                var sumUtilidad = 0;
                                var sumcantidad = 0;
                                var oxi = [];
                                var utilidad = [];
                                var cantidad = [];
                                var categories = [];
                                var htmTable2 = `<table class="table table-sm table-detail">
                                <tr>
                                    <th style="background:#ddebf8;text-align:center"><b>#</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Tamaño</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Cantidad</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                </tr>`;
                                $.each(response.data, function(i, obj) {
                                    cont++;
                                    oxi.push(parseInt(obj.oxi));
                                    utilidad.push(parseInt(obj.utilidad));
                                    categories.push(obj.nomtamanio);
                                    cantidad.push(parseInt(obj.cantidad));
                                    sumOxi += parseInt(obj.oxi);
                                    sumUtilidad += parseInt(obj.utilidad);
                                    sumcantidad += parseInt(obj.cantidad);
                                    htmTable2 += '<tr>';
                                    htmTable2 += '<td style="text-align:center">' + cont + '</td>';
                                    htmTable2 += '<td style="text-align:center"><button class="btn btn-link lnlAmpliarTamanio" id="' + obj.nomtamanio + '" data-event="lnkProvXrutas_' + obj.nomtamanio + '" onclick="App.events(this);">' + obj.nomtamanio + '</button></td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.cantidad).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '</tr>';
                                    htmTable2 += '<tr  data-target="lnkProvXrutas_' + obj.nomtamanio + '" style="display: none;">';
                                    htmTable2 += `<td colspan="8">
                                        <div class="card">
                                            <div class="card-header">
                                                Sectores
                                            </div>
                                            <div class="card-body">`;
                                    htmTable2 += '<div id="div_' + obj.nomtamanio + '"></div>';
                                    htmTable2 += `</div>
                                        </div>
                                    </td>
                                    </tr>`;

                                });
                                htmTable2 += '<tr>';
                                htmTable2 += '<td style="text-align:center">Total</td>';
                                htmTable2 += '<td style="text-align:center"></td>';
                                htmTable2 += '<td style="text-align:center">' + sumcantidad.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:center">' + sumUtilidad.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:center">' + sumOxi.toLocaleString('es-MX') + '</td>';

                                htmTable2 += '</tr>';
                                htmTable2 += `</table>`;
                                //(sumcantidad);
                                $('#html_dash_indicador2').html(htmTable2);

                                var obj = {
                                    titulo: 'Disponibilidad Oxi y cantidad por tamaño de empresa',
                                    subtitulo: '',
                                    categories: categories,
                                    oxi: oxi,
                                    cantidad: cantidad,
                                    utilidad: utilidad,
                                    div: 'chart_dash_indicador2'
                                }

                                dashBoardEmpresas_top_tamaño(obj);
                            }
                        });

                    }
                });
                break;
            case 'proyectos_dash':
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_monto.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                //filaprincipal1
                                var objChartpie1 = {
                                    div: 'chart1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_general',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie3);
                                //filaprincipal2
                                var objChartpie_1 = {
                                    div: 'chart_1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_1);
                                var objChartpie_2 = {
                                    div: 'chart_2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_2);
                                var objChartpie_3 = {
                                    div: 'chart_3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'PROGRAMA DE INVERSION',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_3);
                                var objChartpie_4 = {
                                    div: 'chart_4',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_4);

                                ////segunda parte1
                                var objChartpie11 = {
                                    div: 'chart11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie11);
                                var objChartpie22 = {
                                    div: 'chart22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie22);
                                var objChartpie33 = {
                                    div: 'chart33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie33);
                                ////segundaparte2
                                var objChartpie111 = {
                                    div: 'chart111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie111);
                                var objChartpie222 = {
                                    div: 'chart222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie222);
                                var objChartpie333 = {
                                    div: 'chart333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie333);

                                //fila2sub

                                ////segunda parte1
                                var objChartpie_11 = {
                                    div: 'chart_11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_11);
                                var objChartpie_22 = {
                                    div: 'chart_22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_22);
                                var objChartpie_33 = {
                                    div: 'chart_33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_33);
                                ////segundaparte2
                                var objChartpie_111 = {
                                    div: 'chart_111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_111);
                                var objChartpie_222 = {
                                    div: 'chart_222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_222);
                                var objChartpie_333 = {
                                    div: 'chart_333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_333);
                                /////////////////////////////////////////////evolucion
                                //primera parte
                                var objChartpie1 = {
                                    div: 'chart__1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart__2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart__3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie3);
                                //segunda parte GN
                                var objChartpiegn1 = {
                                    div: 'chartgn1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn1);
                                var objChartpiegn2 = {
                                    div: 'chartgn2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn2);
                                var objChartpiegn3 = {
                                    div: 'chartgn3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn3);

                                //tercera fila GR
                                var objChartpiegr1 = {
                                    div: 'chartgr1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr1);
                                var objChartpiegr2 = {
                                    div: 'chartgr2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr2);
                                var objChartpiegr3 = {
                                    div: 'chartgr3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr3);

                                //tercera fila GL
                                var objChartpiegl1 = {
                                    div: 'chartgl1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl1);
                                var objChartpiegl2 = {
                                    div: 'chartgl2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl2);
                                var objChartpiegl3 = {
                                    div: 'chartgl3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl3);

                            }
                        });
                    }
                });
                break;
            case 'entidades_dash':
                changeTabPanelEstadistica('entidades');
                $.post({
                    url: 'entidades_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;

                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../diventidades_proyectos_niveles.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('entidad_dash').innerHTML = response;
                                var obj = {
                                    method: 'pi_partidospoliticos'
                                }
                                $.getJSON({
                                    url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        var categories = [];
                                        var csn = [];
                                        var msn = [];
                                        var ccn = [];
                                        var mcn = [];
                                        $.each(response.data, function(i, obj) {
                                            if (obj.nomnivelgobierno != 'GOBIERNO PROVINCIAL') {
                                                csn.push(parseInt(obj.cantidad_pro_s));
                                                msn.push(parseInt(obj.monto_pro_s));
                                                ccn.push(parseInt(obj.cantidad_pro));
                                                mcn.push(parseInt(obj.monto_pro));
                                                categories.push(obj.nomnivelgobierno);
                                            }
                                        });
                                        var obj_csn = {
                                            name: 'Cantidad PI viables sin ejecucion',
                                            type: 'spline',
                                            color: '#0070c0',
                                            yAxis: 1,
                                            data: csn,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_msn = {
                                            name: 'Monto PI viables sin ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            yAxis: 0,
                                            data: msn,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_ccn = {
                                            name: 'Cantidad PI viables con ejecucion',
                                            type: 'spline',
                                            color: '#92d050',
                                            yAxis: 1,
                                            data: ccn,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_mcn = {
                                            name: 'Monto PI viables con ejecucion',
                                            type: 'column',
                                            yAxis: 0,
                                            color: '#50497a',
                                            data: mcn,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj = {
                                            titulo: 'Grafico de Proyectos de Inversion(PI) cantidad y monto',
                                            subtitulo: '',
                                            categories: categories,
                                            csn: obj_csn,
                                            msn: obj_msn,
                                            ccn: obj_ccn,
                                            mcn: obj_mcn,
                                            div: 'chart_mef_entidades'
                                        }
                                        dashboarentidades_partidos(obj)

                                    }
                                });
                            }
                        });

                    }
                });
                break;
            case 'politica_dash':
                changeTabPanelEstadistica('entidades');
                $.post({
                    url: 'politica_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = {
                            method: 'pi_partidospoliticos'
                        }
                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                var categories = [];
                                var csn = [];
                                var msn = [];
                                var ccn = [];
                                var mcn = [];
                                $.each(response.data, function(i, obj) {
                                    csn.push(parseInt(obj.cantidad_pro_s));
                                    msn.push(parseInt(obj.monto_pro_s));
                                    ccn.push(parseInt(obj.cantidad_pro));
                                    mcn.push(parseInt(obj.monto_pro));
                                    categories.push(obj.nomnivelgobierno);
                                });
                                var obj_csn = {
                                    name: 'Cantidad PI viables sin ejecucion',
                                    type: 'spline',
                                    color: '#0070c0',
                                    yAxis: 1,
                                    data: csn,
                                    tooltip: {
                                        valueSuffix: ''
                                    },

                                }
                                var obj_msn = {
                                    name: 'Monto PI viables sin ejecucion',
                                    type: 'column',
                                    color: '#A7454F',
                                    yAxis: 0,
                                    data: msn,
                                    tooltip: {
                                        valueSuffix: ''
                                    },

                                }
                                var obj_ccn = {
                                    name: 'Cantidad PI viables con ejecucion',
                                    type: 'spline',
                                    color: '#92d050',
                                    yAxis: 1,
                                    data: ccn,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj_mcn = {
                                    name: 'Monto PI viables con ejecucion',
                                    type: 'column',
                                    yAxis: 0,
                                    color: '#50497a',
                                    data: mcn,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj = {
                                    titulo: 'Grafico de Proyectos de Inversion(PI) cantidad y monto',
                                    subtitulo: '',
                                    categories: categories,
                                    csn: obj_csn,
                                    msn: obj_msn,
                                    ccn: obj_ccn,
                                    mcn: obj_mcn,
                                    div: 'chart_mef_entidades'
                                }
                                dashboarentidades_partidos(obj)

                            }
                        });
                    }
                });
                break;
            case 'salud_dash':
                $.post({
                    url: 'salud_dashboard.php',
                    success: function(response) {
                        // mete salud_dashboard.php al apartado
                        document.getElementById('content_dashboard').innerHTML = response;
                        var params = { method: 'departamento' };
                        dropDownList('ddlDepartamentos', params);
                        /////////////////////////////edad//////////////////
                        var obj = {
                            titulo: 'EDAD',
                            tab: 'datos'
                        }
                        $.post({
                            url: '../divsaludproyectado_edad.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('div_salud_edad').innerHTML = response;
                                var departamento = "AMAZONAS','ANCASH','APURIMAC','AREQUIPA','AYACUCHO','CAJAMARCA','CALLAO','CUSCO','HUANCAVELICA','HUANUCO','ICA','JUNIN','LA LIBERTAD','LAMBAYEQUE','LIMA','LORETO','MADRE DE DIOS','MOQUEGUA','PASCO','PIURA','PUNO','SAN MARTIN','TACNA','TUMBES','UCAYALI";
                                var params = { method: 'departamento' };
                                dropDownList('ddlDepartamentos', params);
                                var obj11 = {
                                    method: 'filtro_edad',
                                    div: 'chartedad_11',
                                    dpto: departamento,
                                    sexo1: 'h',
                                    sexo2: 'm'
                                }
                                fs_chart_tipo_salud(obj11);
                                var obj22 = {
                                    method: 'filtro_sinadef',
                                    div: 'chartedad_22',
                                    dpto: departamento,
                                    sexo1: 'MASCULINO',
                                    sexo2: 'FEMENINO'
                                }
                                fs_chart_tipo_salud(obj22);
                                var obj33 = {
                                    method: 'filtro_covid',
                                    div: 'chartedad_33',
                                    dpto: departamento,
                                    sexo1: 'MASCULINO',
                                    sexo2: 'FEMENINO'
                                }
                                fs_chart_tipo_salud(obj33);
                                /*calor1*/
                                // var obj55 = {
                                //     method: 'chartcalor',
                                //     div: 'chartedad_calor2020',
                                //     ano: '2020'
                                // }
                                // fs_chart_calor(obj55)

                                // var obj56 = {
                                //     method: 'chartcalor',
                                //     div: 'chartedad_calor2021',
                                //     ano: '2021'

                                // }
                                // fs_chart_calor(obj56)
                                /** end calor1*/

                                /**calor2 */
                                var obj55 = {
                                    method: 'chartcalorv2',
                                    div: 'chartedad_calor20_21',
                                    ano: '2020'
                                }
                                fs_chart_calorv2(obj55);

                                var obj56 = {
                                    method: 'chartcalorv3',
                                    div: 'chartedad_calor_regiones',
                                    ano: '2021'
                                }
                                fs_chart_calorv3(obj56);
                                /**end calor 2 */

                                var obj57 = {
                                    method: 'sinadef_mensual_v2',
                                    titulo: 'Fallecidos SINADEF mensual, por rango de Edad proyectado a Mayo 2021',
                                    div: 'chart_sinadef_edad_mensual',
                                    tipo: 'mensual'
                                }
                                fs_chart_edad_salud(obj57);
                                var obj58 = {
                                    method: 'sinadef_semanal_v2',
                                    titulo: 'Fallecidos SINADEF semanal por rango de Edad proyectado al 11 de Mayo 2021',
                                    div: 'chart_sinadef_edad_semanal',
                                    tipo: 'semanal'
                                }
                                fs_chart_edad_salud(obj58);
                                var obj59 = {
                                    method: 'sinadef_diario_v2',
                                    titulo: 'Fallecidos SINADEF COVID-19 promedio diario semanal por rango de Edad proyectado al 11 Mayo 2021',
                                    div: 'chart_sinadef_edad_diario',
                                    tipo: 'diario'
                                }
                                fs_chart_edad_salud(obj59);

                            }
                        });
                        //salud_dash//



                        //////////////////////////////// /sinadef//////////////////////////////////////////






                        ////////////////////////////oxigeno/////////////////

                    }
                });

                break;
        }
        /****entidades start */
        if (params.classList.contains('proyectos_niveles')) {
            var id = params.getAttribute('id');
            if (id == 'proyectos_niveles') {
                $.post({
                    url: 'entidades_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;

                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../diventidades_proyectos_niveles.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('entidad_dash').innerHTML = response;
                                var obj = {
                                    method: 'pi_partidospoliticos'
                                }
                                $.getJSON({
                                    url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        var categories = [];
                                        var csn = [];
                                        var msn = [];
                                        var ccn = [];
                                        var mcn = [];
                                        $.each(response.data, function(i, obj) {
                                            if (obj.nomnivelgobierno != 'GOBIERNO PROVINCIAL') {
                                                csn.push(parseInt(obj.cantidad_pro_s));
                                                msn.push(parseInt(obj.monto_pro_s));
                                                ccn.push(parseInt(obj.cantidad_pro));
                                                mcn.push(parseInt(obj.monto_pro));
                                                categories.push(obj.nomnivelgobierno);
                                            }
                                        });
                                        var obj_csn = {
                                            name: 'Cantidad PI viables sin ejecucion',
                                            type: 'spline',
                                            color: '#0070c0',
                                            yAxis: 1,
                                            data: csn,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_msn = {
                                            name: 'Monto PI viables sin ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            yAxis: 0,
                                            data: msn,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_ccn = {
                                            name: 'Cantidad PI viables con ejecucion',
                                            type: 'spline',
                                            color: '#92d050',
                                            yAxis: 1,
                                            data: ccn,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_mcn = {
                                            name: 'Monto PI viables con ejecucion',
                                            type: 'column',
                                            yAxis: 0,
                                            color: '#50497a',
                                            data: mcn,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj = {
                                            titulo: 'Grafico de Proyectos de Inversion(PI) cantidad y monto',
                                            subtitulo: '',
                                            categories: categories,
                                            csn: obj_csn,
                                            msn: obj_msn,
                                            ccn: obj_ccn,
                                            mcn: obj_mcn,
                                            div: 'chart_mef_entidades'
                                        }
                                        dashboarentidades_partidos(obj)

                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
        if (params.classList.contains('canon')) {
            var id = params.getAttribute('id');
            if (id == 'canon') {
                $.post({
                    url: 'entidades_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../diventidades_canon.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('entidad_dash').innerHTML = response;
                                var params = { method: 'departamento' };
                                dropDownList('ddlDepartamentos_canon', params);

                                var obj = {
                                    method: 'dist_anual_canon',
                                    div: 'chart_mef_entidades',
                                    tbl: 'tbl_dist_canon',
                                    flag: 'ok'
                                }
                                fs_chart_entidad_canon(obj)
                            }
                        });
                    }
                });
            }
        }
        /**entidades end */

        if (params.classList.contains('brecha')) {
            var id = params.getAttribute('id');
            if (id == 'brecha') {
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_brecha.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                /////////////////////////////////////////////brecha
                                //primera parte

                                var objChart10 = {
                                    div: 'chartbrecha_10',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    flag: 'ok',
                                    tabla: 'cuerpoTabla2'

                                };
                                fs_chart4(objChart10);
                                //segunda parte
                                var objChart11 = {
                                    div: 'chartbrecha_11',
                                    method: 'rango_brecha',
                                    tipo: 'IOARR'
                                };
                                fs_chart4(objChart11);
                                var objChart22 = {
                                    div: 'chartbrecha_22',
                                    method: 'rango_brecha',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart4(objChart22);
                                var objChart33 = {
                                    div: 'chartbrecha_33',
                                    method: 'rango_brecha',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart4(objChart33);
                                //IOARR
                                var objChart_1 = {
                                    div: 'chartbrecha_1',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'IOARR',
                                    nivel: 'GN'
                                };
                                fs_chart4(objChart_1);
                                var objChart_2 = {
                                    div: 'chartbrecha_2',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'IOARR',
                                    nivel: 'GR'
                                };
                                fs_chart4(objChart_2);
                                var objChart_3 = {
                                    div: 'chartbrecha_3',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'IOARR',
                                    nivel: 'GL'
                                };
                                fs_chart4(objChart_3);

                                //PROYECTOS
                                var obj_Chart_1 = {
                                    div: 'chart_brecha_1',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROYECTO',
                                    nivel: 'GN'
                                };
                                fs_chart4(obj_Chart_1);
                                var obj_Chart_2 = {
                                    div: 'chart_brecha_2',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROYECTO',
                                    nivel: 'GR'
                                };
                                fs_chart4(obj_Chart_2);
                                var obj_Chart_3 = {
                                    div: 'chart_brecha_3',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROYECTO',
                                    nivel: 'GL'
                                };
                                fs_chart4(obj_Chart_3);

                                //PROGRAMA
                                var objChart_1_1 = {
                                    div: 'chart_brecha_1_1',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROGRAMA DE INVERSION',
                                    nivel: 'GN'
                                };
                                fs_chart4(objChart_1_1);
                                var objChart_2_2 = {
                                    div: 'chart_brecha_2_1',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROGRAMA DE INVERSION',
                                    nivel: 'GR'
                                };
                                fs_chart4(objChart_2_2);
                                var objChart_3_3 = {
                                    div: 'chart_brecha_3_1',
                                    method: 'rango_brecha_colapsada',
                                    tipo: 'PROGRAMA DE INVERSION',
                                    nivel: 'GL'
                                };
                                fs_chart4(objChart_3_3);



                            }
                        });

                    }
                });
            }

        }
        if (params.classList.contains('adjudicados')) {

            // document.getElementById('html_dash_proyectos_funcion').innerHTML = '<proadjudicados-obs></proadjudicados-obs>';
            var obj = {
                tipo: 'adjudicados'
            };
            $.post({
                url: '../divproyectos_adjudicados_dash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                    var objChart10 = {
                        div: 'chart_adjudicados',
                        method: 'chart_proyectos_adjudicados',
                        tabla: 'cuerpoTabla2',
                        flag: 'ok'
                    };
                    fs_chart_adjudicados(objChart10);
                }
            });
        }
        if (params.classList.contains('general')) {
            var id = params.getAttribute('id');
            if (id == 'general') {
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_datamef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                /////////////////////////////////////////////brecha
                                //primera parte
                                // $('#dfuncion').selectpicker();  
                                $('.selectpicker').selectpicker({});
                                var params = { method: 'funcion' };
                                // dropDownListss('dfuncion', params);
                                var objChart10 = {
                                    div: 'chartgeneral_10',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    tabla: 'cuerpoTabla2',
                                    flag: 'ok'
                                };
                                fs_chart4(objChart10);

                                var objChart10 = {
                                    div: 'chartgeneral_11',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    flag: 'ok',
                                    funcion: 'SALUD',
                                    tabla: 'fsalud'
                                };
                                fs_chart4(objChart10);
                                var objChart10 = {
                                    div: 'chartgeneral_22',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    flag: 'ok',
                                    funcion: 'EDUCACIÓN',
                                    tabla: 'feducacion'
                                };
                                fs_chart4(objChart10);
                                var objChart10 = {
                                    div: 'chartgeneral_33',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    flag: 'ok',
                                    funcion: 'ORDEN PÚBLICO Y SEGURIDAD',
                                    tabla: 'fseguridad'
                                };
                                fs_chart4(objChart10);
                                var objChart10 = {
                                    div: 'chartgeneral_44',
                                    method: 'rango_brecha',
                                    tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    flag: 'ok',
                                    funcion: 'TRANSPORTE',
                                    tabla: 'ftransporte'
                                };
                                fs_chart4(objChart10);
                            }
                        });

                    }
                });
            }

        }
        if (params.classList.contains('rango')) {
            var id = params.getAttribute('id');
            if (id == 'rango') {
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_rango.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                /////////////////////////////////////////////rango
                                //primera parte

                                var objChart1 = {
                                    div: 'chartrango_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GN',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart1);
                                var objChart2 = {
                                    div: 'chartrango_2',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GN',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart2);
                                var objChart3 = {
                                    div: 'chartrango_3',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GN',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart3);
                                //segunda parte

                                var objChart12 = {
                                    div: 'chartrango_12',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GR',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart12);
                                var objChart22 = {
                                    div: 'chartrango_22',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GR',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart22);
                                var objChart32 = {
                                    div: 'chartrango_32',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GR',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart32);

                                //tercera parte
                                var objChart13 = {
                                    div: 'chartrango_13',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GL',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart13);
                                var objChart23 = {
                                    div: 'chartrango_23',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GL',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart23);
                                var objChart33 = {
                                    div: 'chartrango_33',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GL',
                                    tipo: 'IOARR'
                                };
                                fs_chart3(objChart33);
                                /////////////////////////////////////////////rango proyecto_perfil
                                //primera parte

                                var objChart_1 = {
                                    div: 'chart_rango_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GN',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_1);
                                var objChart_2 = {
                                    div: 'chart_rango_2',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GN',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_2);
                                var objChart_3 = {
                                    div: 'chart_rango_3',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GN',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_3);
                                //segunda parte

                                var objChart_12 = {
                                    div: 'chart_rango_12',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GR',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_12);
                                var objChart_22 = {
                                    div: 'chart_rango_22',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GR',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_22);
                                var objChart_32 = {
                                    div: 'chart_rango_32',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GR',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_32);

                                //tercera parte
                                var objChart_13 = {
                                    div: 'chart_rango_13',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GL',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_13);
                                var objChart_23 = {
                                    div: 'chart_rango_23',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GL',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_23);
                                var objChart_33 = {
                                    div: 'chart_rango_33',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GL',
                                    tipo: 'PROYECTO'
                                };
                                fs_chart3(objChart_33);
                                //////////////////////////////////////// PROGRAMAS DE INVERSION
                                var objChart_1_1 = {
                                    div: 'chart_rango_1_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GN',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_1_1);
                                var objChart_2_1 = {
                                    div: 'chart_rango_2_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GN',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_2_1);
                                var objChart_3_1 = {
                                    div: 'chart_rango_3_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GN',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_3_1);
                                //segunda parte

                                var objChart_12_1 = {
                                    div: 'chart_rango_12_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GR',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_12_1);
                                var objChart_22_1 = {
                                    div: 'chart_rango_22_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GR',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_22_1);
                                var objChart_32_1 = {
                                    div: 'chart_rango_32_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GR',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_32_1);

                                //tercera parte
                                var objChart_13_1 = {
                                    div: 'chart_rango_13_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: "0','1",
                                    nivel: 'GL',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_13_1);
                                var objChart_23_1 = {
                                    div: 'chart_rango_23_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '0',
                                    nivel: 'GL',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_23_1);
                                var objChart_33_1 = {
                                    div: 'chart_rango_33_1',
                                    method: 'rango_mef',
                                    color: 'estado',
                                    f15: '1',
                                    nivel: 'GL',
                                    tipo: 'PROGRAMA DE INVERSION'
                                };
                                fs_chart3(objChart_33_1);

                            }
                        });

                    }
                });
            }

        }
        if (params.classList.contains('evolucion')) {
            var id = params.getAttribute('id');
            if (id == 'evolucion') {
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_evolucion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                //filaprincipal1
                                var objChartpie1 = {
                                    div: 'chart1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_general',
                                    fn: 'count',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento',
                                    fn: 'count',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie3);
                                //filaprincipal2
                                var objChartpie_1 = {
                                    div: 'chart_1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'IOARR',
                                    fn: 'count',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_1);
                                var objChartpie_2 = {
                                    div: 'chart_2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: 'IOARR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_2);
                                var objChartpie_3 = {
                                    div: 'chart_3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'PROGRAMA DE INVERSION',
                                    fn: 'count',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_3);
                                var objChartpie_4 = {
                                    div: 'chart_4',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_4);

                                ////segunda parte1
                                var objChartpie11 = {
                                    div: 'chart11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GN',
                                    fn: 'count',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie11);
                                var objChartpie22 = {
                                    div: 'chart22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GR',
                                    fn: 'count',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie22);
                                var objChartpie33 = {
                                    div: 'chart33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GL',
                                    fn: 'count',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie33);
                                ////segundaparte2
                                var objChartpie111 = {
                                    div: 'chart111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GN',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie111);
                                var objChartpie222 = {
                                    div: 'chart222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie222);
                                var objChartpie333 = {
                                    div: 'chart333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GL',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie333);

                                //fila2sub

                                ////segunda parte1
                                var objChartpie_11 = {
                                    div: 'chart_11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: 'IOARR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_11);
                                var objChartpie_22 = {
                                    div: 'chart_22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: 'IOARR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_22);
                                var objChartpie_33 = {
                                    div: 'chart_33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: 'IOARR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_33);
                                ////segundaparte2
                                var objChartpie_111 = {
                                    div: 'chart_111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_111);
                                var objChartpie_222 = {
                                    div: 'chart_222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_222);
                                var objChartpie_333 = {
                                    div: 'chart_333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_333);
                                /////////////////////////////////////////////evolucion
                                //primera parte
                                var objChartpie1 = {
                                    div: 'chart__1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart__2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: 'IOARR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart__3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie3);
                                //segunda parte GN
                                var objChartpiegn1 = {
                                    div: 'chartgn1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn1);
                                var objChartpiegn2 = {
                                    div: 'chartgn2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GN',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn2);
                                var objChartpiegn3 = {
                                    div: 'chartgn3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn3);

                                //tercera fila GR
                                var objChartpiegr1 = {
                                    div: 'chartgr1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr1);
                                var objChartpiegr2 = {
                                    div: 'chartgr2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr2);
                                var objChartpiegr3 = {
                                    div: 'chartgr3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr3);

                                //tercera fila GL
                                var objChartpiegl1 = {
                                    div: 'chartgl1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl1);
                                var objChartpiegl2 = {
                                    div: 'chartgl2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GL',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl2);
                                var objChartpiegl3 = {
                                    div: 'chartgl3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'count',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl3);

                            }
                        });
                    }
                });
            }
            if (id == 'monto') {
                $.post({
                    url: 'proyectos_dashboard.php',
                    success: function(response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = { tipo: 'perfil' }
                        $.post({
                            url: '../divproyectos_monto.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('html_dash_proyectos_funcion').innerHTML = response;
                                //filaprincipal1
                                var objChartpie1 = {
                                    div: 'chart1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_general',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie3);
                                //filaprincipal2
                                var objChartpie_1 = {
                                    div: 'chart_1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_1);
                                var objChartpie_2 = {
                                    div: 'chart_2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_2);
                                var objChartpie_3 = {
                                    div: 'chart_3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto',
                                    formato: 'PROGRAMA DE INVERSION',
                                    fn: 'sum',
                                    color: 'nivel'
                                };
                                fs_chart1(objChartpie_3);
                                var objChartpie_4 = {
                                    div: 'chart_4',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e',
                                    f15: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_4);

                                ////segunda parte1
                                var objChartpie11 = {
                                    div: 'chart11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie11);
                                var objChartpie22 = {
                                    div: 'chart22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie22);
                                var objChartpie33 = {
                                    div: 'chart33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_tipo_documento_nivel',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'tipo'
                                };
                                fs_chart1(objChartpie33);
                                ////segundaparte2
                                var objChartpie111 = {
                                    div: 'chart111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie111);
                                var objChartpie222 = {
                                    div: 'chart222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie222);
                                var objChartpie333 = {
                                    div: 'chart333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_proyecto_estado_ejecucion_nivel',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie333);

                                //fila2sub

                                ////segunda parte1
                                var objChartpie_11 = {
                                    div: 'chart_11',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_11);
                                var objChartpie_22 = {
                                    div: 'chart_22',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_22);
                                var objChartpie_33 = {
                                    div: 'chart_33',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_33);
                                ////segundaparte2
                                var objChartpie_111 = {
                                    div: 'chart_111',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GN',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_111);
                                var objChartpie_222 = {
                                    div: 'chart_222',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GR',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_222);
                                var objChartpie_333 = {
                                    div: 'chart_333',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_ioar_proyecto_v_e_nivel',
                                    nivel: 'GL',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart1(objChartpie_333);
                                /////////////////////////////////////////////evolucion
                                //primera parte
                                var objChartpie1 = {
                                    div: 'chart__1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie1);
                                var objChartpie2 = {
                                    div: 'chart__2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: 'IOARR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie2);
                                var objChartpie3 = {
                                    div: 'chart__3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpie3);
                                //segunda parte GN
                                var objChartpiegn1 = {
                                    div: 'chartgn1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn1);
                                var objChartpiegn2 = {
                                    div: 'chartgn2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn2);
                                var objChartpiegn3 = {
                                    div: 'chartgn3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GN',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegn3);

                                //tercera fila GR
                                var objChartpiegr1 = {
                                    div: 'chartgr1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr1);
                                var objChartpiegr2 = {
                                    div: 'chartgr2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr2);
                                var objChartpiegr3 = {
                                    div: 'chartgr3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GR',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegr3);

                                //tercera fila GL
                                var objChartpiegl1 = {
                                    div: 'chartgl1',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl1);
                                var objChartpiegl2 = {
                                    div: 'chartgl2',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: 'IOARR',
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl2);
                                var objChartpiegl3 = {
                                    div: 'chartgl3',
                                    titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                    method: 'monto_evolucion_tipo_formato_nivel',
                                    formato: "PROYECTO','PROGRAMA DE INVERSION",
                                    nivel: 'GL',
                                    fn: 'sum',
                                    color: 'estado'
                                };
                                fs_chart2(objChartpiegl3);

                            }
                        });
                    }
                });
            }
        }


        if (params.classList.contains('edad_salud')) {
            var obj = {
                titulo: 'EDAD',
                tab: 'datos'
            }
            $.post({
                url: '../divsaludproyectado_edad.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('div_salud_edad').innerHTML = response;
                    var departamento = "AMAZONAS','ANCASH','APURIMAC','AREQUIPA','AYACUCHO','CAJAMARCA','CALLAO','CUSCO','HUANCAVELICA','HUANUCO','ICA','JUNIN','LA LIBERTAD','LAMBAYEQUE','LIMA','LORETO','MADRE DE DIOS','MOQUEGUA','PASCO','PIURA','PUNO','SAN MARTIN','TACNA','TUMBES','UCAYALI";
                    var params = { method: 'departamento' };
                    dropDownList('ddlDepartamentos', params);
                    var obj11 = {
                        method: 'filtro_edad',
                        div: 'chartedad_11',
                        dpto: departamento,
                        sexo1: 'h',
                        sexo2: 'm'
                    }
                    fs_chart_tipo_salud(obj11);
                    var obj22 = {
                        method: 'filtro_sinadef',
                        div: 'chartedad_22',
                        dpto: departamento,
                        sexo1: 'MASCULINO',
                        sexo2: 'FEMENINO'
                    }
                    fs_chart_tipo_salud(obj22);
                    var obj33 = {
                        method: 'filtro_covid',
                        div: 'chartedad_33',
                        dpto: departamento,
                        sexo1: 'MASCULINO',
                        sexo2: 'FEMENINO'
                    }
                    fs_chart_tipo_salud(obj33);
                    /*calor1*/
                    // var obj55 = {
                    //     method: 'chartcalor',
                    //     div: 'chartedad_calor2020',
                    //     ano: '2020'
                    // }
                    // fs_chart_calor(obj55)

                    // var obj56 = {
                    //     method: 'chartcalor',
                    //     div: 'chartedad_calor2021',
                    //     ano: '2021'

                    // }
                    // fs_chart_calor(obj56)
                    /** end calor1*/

                    /**calor2 */
                    var obj55 = {
                        method: 'chartcalorv2',
                        div: 'chartedad_calor20_21',
                        ano: '2020'
                    }
                    fs_chart_calorv2(obj55);

                    var obj56 = {
                        method: 'chartcalorv3',
                        div: 'chartedad_calor_regiones',
                        ano: '2021'
                    }
                    fs_chart_calorv3(obj56);
                    /**end calor 2 */

                    var obj57 = {
                        method: 'sinadef_mensual_v2',
                        titulo: 'Fallecidos SINADEF mensual, por rango de Edad proyectado a Mayo 2021',
                        div: 'chart_sinadef_edad_mensual',
                        tipo: 'mensual'
                    }
                    fs_chart_edad_salud(obj57);
                    var obj58 = {
                        method: 'sinadef_semanal_v2',
                        titulo: 'Fallecidos SINADEF semanal por rango de Edad proyectado al 11 de Mayo 2021',
                        div: 'chart_sinadef_edad_semanal',
                        tipo: 'semanal'
                    }
                    fs_chart_edad_salud(obj58);
                    var obj59 = {
                        method: 'sinadef_diario_v2',
                        titulo: 'Fallecidos SINADEF COVID-19 promedio diario semanal por rango de Edad proyectado al 11 Mayo 2021',
                        div: 'chart_sinadef_edad_diario',
                        tipo: 'diario'
                    }
                    fs_chart_edad_salud(obj59);

                }
            });
        }

        //secambiamesmes
        if (params.classList.contains('covid_sinadef_salud')) {
            //COVID
            // var obj = {
            //     titulo: 'COVID-19',
            //     tab: 'datos'
            // }
            // $.post({
            //     url: '../divsaludproyectadocosto_salud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            //     success: function(response) {
            //         document.getElementById('div_costo_salud').innerHTML = response;
            //         var obj_pie = {
            //             div: 'pie1',
            //             titulo: 'Distribuidos por gestión presidencial',
            //             method: 'pie_salud',
            //             color: 'estado'
            //         }
            //         fs_pie_salud(obj_pie);
            //         var obj_pie = {
            //             div: 'pie2',
            //             titulo: 'Distribuidos por gestión presidencial',
            //             method: 'pie_salud',
            //             color: 'estado'
            //         }
            //         fs_pie_salud(obj_pie);

            //     }
            // });

            var obj = {
                titulo: 'COVID-19',
                tab: 'datos'
            }

            $.post({
                url: '../divsaludproyectadominsa.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('div_salud_edad').innerHTML = response;
                    // var depa = $("#ddlDepartamento").find(":selected").attr("value");
                    var params = { method: 'departamento' };
                    dropDownList('ddlDepartamentos_minsa', params);
                    var params = { method: 'departamento' };
                    dropDownList('ddlDepartamentos_sinadef', params);
                    //sinadef selector
                    var objsel = {
                        method: 'fs_chart_sinadef_selector',
                        div: 'chart_self',
                        orden: 'pf',
                        titulo: 'Exceso fallecidos % sobre el promedio histórico por Departamentos'
                    }
                    fs_chart_sinadef_selector(objsel);


                    var objsel = {
                        method: 'fs_chart_sinadef_selector',
                        div: 'chart_selh',
                        titulo: 'Exceso fallecidos por 100k hab. sobre el promedio por Departamento',
                        orden: 'ph'
                    }
                    fs_chart_sinadef_selector(objsel);

                    ////g2
                    var obj = {
                        method: 'total_minsa_c_resumen',
                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response2) {
                            //(response2);
                            var a = 0;
                            var b = 0;

                            for (i = 0; i < response2.data.length; i++) {

                                if (response2.data[i].a2020 != null) {
                                    a = a + parseInt(response2.data[i].a2020);

                                }
                                if (response2.data[i].a2021 != null) {
                                    if (i == 8) {
                                        b = b + (parseInt(response2.data[i - 1].a2021) * 30) / 100 + parseInt(response2.data[i - 1].a2021);
                                    } else {
                                        b = b + parseInt(response2.data[i].a2021);

                                    }
                                }
                            }
                            /* promedio*/
                            p = a / 10;
                            p2 = (b) / 8;
                            /* promedio*/
                            var ob1 = {
                                name: '2020',
                                color: 'red',
                                data: [a, 0]
                            }
                            var ob2 = {
                                name: '2021*',
                                color: '#731d1d',
                                data: [0, b]
                            }
                            var p1 = {
                                name: '2020',
                                color: 'red',
                                data: [p, 0]
                            }
                            var p2 = {
                                name: '2021*',
                                color: '#731d1d',
                                data: [0, p2]
                            }

                            /** prmedio semanal*/
                            var ps1 = {
                                name: '2020',
                                color: 'red',
                                data: [915, 0]
                            }
                            var ps2 = {
                                    name: '2021*',
                                    color: '#731d1d',
                                    data: [0, 1144]
                                }
                                /**promedio diario */
                            var pd1 = {
                                name: '2020',
                                color: 'red',
                                data: [131, 0]
                            }
                            var pd2 = {
                                name: '2021*',
                                color: '#731d1d',
                                data: [0, 171]
                            }
                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2020', '2021'];
                            Chart.title.text = 'Total de fallecidos COVID19, 2020 - 2021*';
                            Chart.series = [ob2, ob1];
                            Highcharts.chart('chart6', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2020', '2021'];
                            Chart.title.text = 'Promedio mensual de fallecidos COVID19, 2020 - 2021*';
                            Chart.series = [p2, p1];
                            Highcharts.chart('chart7', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2020', '2021'];
                            Chart.title.text = 'Promedio semanal de fallecidos COVID19, 2020 - 2021*';
                            Chart.series = [ps2, ps1];
                            Highcharts.chart('chart9', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2020', '2021'];
                            Chart.title.text = 'Promedio diario de fallecidos COVID19, 2020 - 2021*';
                            Chart.series = [pd2, pd1];
                            Highcharts.chart('chart101', Chart);

                        }
                    });

                    var obj = {
                        method: 'total_minsa_c_resumen',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            // console.log(response1);
                            var categorias = [];

                            var obj4 = {
                                name: '2020',
                                type: 'spline',

                                color: 'red',
                                data: []
                            };
                            var obj5 = {
                                name: '2021*',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };
                            // obj6.data.push(0, 0);
                            for (i = 0; i < response1.data.length; i++) {
                                categorias.push(response1.data[i].mes);
                                obj4.data.push(parseInt(response1.data[i].a2020));
                                if (response1.data[i].a2021 != 0) {
                                    // obj5.data.push(parseInt(response1.data[i].a2021));
                                    if (i == 8) {
                                        obj6.data.push(((parseInt(response1.data[i - 1].a2021) * 30) / 100 + parseInt(response1.data[i - 1].a2021)));
                                    } else if (i < 8) {
                                        obj5.data.push(parseInt(response1.data[i].a2021));
                                        obj6.data.push(parseInt(response1.data[i].a2021));
                                    }
                                }
                            }
                            /*quiii falta proyeccion*/
                            // obj6.data.push(9034);
                            var total = [obj4, obj6, obj5];
                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Fallecidos Covid a nivel nacional meses por año 2020-2021*';
                            Chart1.series = total;
                            Highcharts.chart('chart1', Chart1);
                        }
                    });
                    var obj = {
                        method: 'total_minsa_c_resumen'
                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            //(response1);
                            var categorias = [];

                            var obj4 = {
                                name: '2020',
                                type: 'column',
                                color: 'red',
                                data: []
                            };
                            var obj5 = {
                                name: '2021*',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                name: '2021*',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            for (i = 0; i < response1.data.length; i++) {
                                categorias.push(response1.data[i].mes);
                                obj4.data.push(parseInt(response1.data[i].a2020));
                                if (response1.data[i].a2021 != 0) {
                                    // obj5.data.push(parseInt(response1.data[i].a2021));
                                    if (i == 8) {
                                        obj6.data.push((parseInt(response1.data[i - 1].a2021) * 30) / 100 + parseInt(response1.data[i - 1].a2021));
                                    } else if (i < 8) {
                                        obj5.data.push(parseInt(response1.data[i].a2021));
                                        obj6.data.push(parseInt(response1.data[i].a2021));
                                    }
                                }
                            }
                            // obj5.data[2] = 6404;
                            var Chart = dualChart;
                            Chart.xAxis[0].categories = categorias;
                            Chart.title.text = 'Total de fallecidos mensual COVID19, 2020- 2021*';
                            Chart.series = [obj4, obj6];
                            Highcharts.chart('chart8', Chart);
                        }
                    });
                    var obj_salud = {
                        method: 'total_minsa_c_resumen',
                        div: 'chart102',

                    }
                    fs_chart3_salud(obj_salud);

                    var obj_d_s_a = {
                        method: 'covid_d_s_a',
                        div: 'chart103',
                        texto: 'fallecidos COVID19 2020/2021 vs 2019, diario, semanal y acumulado',

                    }
                    fs_chart4_salud(obj_d_s_a);
                    var obj = {
                        method: 'covid_pdiario_asemanal',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            //(response1);
                            var categorias = [];
                            var ratio = [];

                            var obj4 = {
                                name: '2020',
                                type: 'spline',
                                color: 'red',
                                data: []
                            };
                            var obj5 = {
                                name: '2021*',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                name: '2020',
                                type: 'spline',
                                color: 'red',
                                data: []
                            };
                            var obj7 = {
                                name: '2021*',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj9 = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };
                            var obj10 = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };
                            // obj9.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                            obj4.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                            categorias.push('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
                            obj6.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                            // categorias.push('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
                            ratio.push(12, 11, 9, 8, 5);
                            for (i = 0; i < response1.data.length - 2; i++) {
                                categorias.push(response1.data[i].semana);
                                if (response1.data[i].ano == '2020') {
                                    obj4.data.push(parseInt(response1.data[i].s));
                                    obj6.data.push(parseInt(response1.data[i].d));
                                } else {
                                    obj5.data.push(parseInt(response1.data[i].s));
                                    obj7.data.push(parseInt(response1.data[i].d));
                                    obj9.data.push(parseInt(response1.data[i].s));
                                    obj10.data.push(parseInt(response1.data[i].d));

                                }
                            }
                            var proyec = obj5.data[obj5.data.length - 1];
                            var proyec2 = obj7.data[obj7.data.length - 1];
                            for (i = 0; i < 9; i++) {
                                if (i == 0) {
                                    proyec = obj5.data[obj5.data.length - 1];
                                    proyec2 = obj7.data[obj7.data.length - 1];
                                } else {
                                    proyec = obj9.data[obj9.data.length - 1];
                                    proyec2 = obj10.data[obj10.data.length - 1];
                                }
                                obj9.data.push(((parseInt(proyec) * ratio[i]) / 100) + parseInt(proyec));
                                obj10.data.push(((parseInt(proyec2) * ratio[i]) / 100) + parseInt(proyec2));
                            }


                            var total1 = [obj4, obj9, obj5];
                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Fallecidos COVID-19 MINSA acumulado semanal, 2020-2021*';
                            Chart1.series = total1;
                            Highcharts.chart('chart2', Chart1);
                            // //(total1);
                            var total2 = [obj6, obj10, obj7];
                            var Chartd = dualChart;
                            Chartd.xAxis[0].categories = categorias;
                            Chartd.title.text = 'Fallecidos COVID-19 MINSA promedio diario semanal, 2020-2021*';
                            Chartd.series = total2;
                            Highcharts.chart('chartd', Chartd);
                            // //(total2);
                        }
                    });

                    var obj = {
                        method: 'edadgeneralminsa',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response2) {
                            //(response2);
                            var categoriass = [];


                            var obj5 = {
                                name: '2020',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            var obj8 = {
                                name: '2021',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                type: 'spline',
                                name: '2020',
                                data: [],
                                color: 'red',

                            };
                            var obj7 = {
                                type: 'spline',
                                name: '2021*',
                                data: [],
                                color: '#731d1d',

                            };

                            for (i = 0; i < response2.data.length; i++) {
                                categoriass.push(i);
                                obj5.data.push(parseInt(response2.data[i].a2020));
                                obj8.data.push(parseInt(response2.data[i].a2021));
                                obj6.data.push(parseInt(response2.data[i].a2020));
                                obj7.data.push(parseInt(response2.data[i].a2021));
                            }

                            var total = [obj6, obj7];
                            var Chart1 = combine;
                            Chart1.xAxis.categories = categoriass;
                            Chart1.title.text = 'Comparativo anual de fallecidos COVID-19 por Edad, 2020 - 2021*';
                            // Chart1.subtitle.text = '(2018-2021)';
                            Chart1.series = total;
                            Highcharts.chart('chart5', Chart1);
                            //(total);

                        }
                    });

                    //////sinadef////

                    ///sinadefmesmes
                    var obj = {
                        method: 'total_sinadef_c_resumen',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response2) {
                            //(response2);
                            var a = 0;
                            var b = 0;
                            var c = 0;
                            var d = 0;
                            for (i = 0; i < response2.data.length; i++) {
                                if (response2.data[i].a2018 != null) {
                                    a = a + parseInt(response2.data[i].a2018);
                                }
                                if (response2.data[i].a2019 != null) {
                                    b = b + parseInt(response2.data[i].a2019);
                                }
                                if (response2.data[i].a2020 != null) {
                                    c = c + parseInt(response2.data[i].a2020);
                                }
                                if (response2.data[i].a2021 != null) {
                                    if (i == 8) {
                                        d = d + (parseInt(response2.data[i - 1].a2021) * 8) / 100 + parseInt(response2.data[i - 1].a2021);
                                    } else {
                                        d = d + parseInt(response2.data[i].a2021);

                                    }
                                }
                            }
                            /* promedio*/
                            pa = a / 12;
                            pb = b / 12;
                            pc = c / 12;
                            pd = (d) / 8;
                            /* promedio*/

                            var p1 = {
                                name: '2018',
                                color: 'red',
                                data: [pa, 0, 0, 0]
                            }
                            var p2 = {
                                name: '2019',
                                color: '#731d1d',
                                data: [0, pb, 0, 0]
                            }
                            var p3 = {
                                name: '2020',
                                color: '#731d1d',
                                data: [0, 0, pc, 0]
                            }
                            var p4 = {
                                name: '2021* ',
                                color: '#731d1d',
                                data: [0, 0, 0, pd]
                            }

                            /** prmedio semanal*/
                            var ps1 = {
                                name: '2018',
                                color: 'red',
                                data: [2164, 0, 0, 0]
                            }
                            var ps2 = {
                                name: '2019',
                                color: 'red',
                                data: [0, 2203, 0, 0]
                            }
                            var ps3 = {
                                name: '2020',
                                color: 'red',
                                data: [0, 0, 4059, 0]
                            }
                            var ps4 = {
                                    name: '2021*',
                                    color: '#731d1d',
                                    data: [0, 0, 0, 6276]
                                }
                                /**promedio diario */
                            var pd1 = {
                                name: '2018',
                                color: 'red',
                                data: [309, 0, 0, 0]
                            }
                            var pd2 = {
                                name: '2019',
                                color: 'red',
                                data: [0, 315, 0, 0]
                            }
                            var pd3 = {
                                name: '2020',
                                color: 'red',
                                data: [0, 0, 580, 0]
                            }
                            var pd4 = {
                                name: '2021* ',
                                color: '#731d1d',
                                data: [0, 0, 0, 920]
                            }

                            var ob1 = {
                                name: '2018',
                                color: 'red',
                                data: [a, 0, 0, 0]
                            }
                            var ob2 = {
                                name: '2019',
                                color: 'red',
                                data: [0, b, 0, 0]
                            }

                            var ob3 = {
                                name: '2020',
                                color: 'red',
                                data: [0, 0, c, 0]
                            }
                            var ob4 = {
                                name: '2021* ',
                                color: '#731d1d',
                                data: [0, 0, 0, d]
                            }

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2018', '2019', '2020', '2021*'];
                            Chart.title.text = 'Total de fallecidos SINADEF 2018-2021* ';
                            Chart.series = [ob4, ob3, ob2, ob1];
                            Highcharts.chart('chart_6', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                            Chart.title.text = 'Promedio mensual de fallecidos SINADEF, 2018 - 2021*';
                            Chart.series = [p4, p3, p2, p1];
                            Highcharts.chart('chart_7', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                            Chart.title.text = 'Promedio semanal de fallecidos SINADEF, 2018 - 2021*';
                            Chart.series = [ps4, ps3, ps2, ps1];
                            Highcharts.chart('chart_9', Chart);

                            var Chart = stackedBarProyectado;
                            Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                            Chart.title.text = 'Promedio diario de fallecidos SINADEF, 2018 - 2021*';
                            Chart.series = [pd4, pd3, pd2, pd1];
                            Highcharts.chart('chart_101', Chart);

                        }
                    });

                    var obj = {
                        method: 'total_sinadef_c_resumen',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            //(response1);
                            var categorias = [];

                            var obj2 = {
                                name: '2018',
                                type: 'spline',
                                color: '#4F81BD',
                                data: []
                            };
                            var obj3 = {
                                name: '2019',
                                type: 'spline',
                                color: '#F28F43',
                                data: []
                            };
                            var obj4 = {
                                name: '2020',
                                type: 'spline',
                                color: 'red',
                                data: []
                            };
                            var obj5 = {
                                name: '2021*  ',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };

                            for (i = 0; i < response1.data.length; i++) {

                                categorias.push(response1.data[i].mes);
                                obj2.data.push(parseInt(response1.data[i].a2018));
                                obj3.data.push(parseInt(response1.data[i].a2019));
                                obj4.data.push(parseInt(response1.data[i].a2020));

                                if (response1.data[i].a2021 != 0 && i < 9) {
                                    if (i == 8) {
                                        obj6.data.push((parseInt(response1.data[i - 1].a2021) * 8) / 100 + parseInt(response1.data[i - 1].a2021));

                                    } else if (i < 8) {
                                        obj5.data.push(parseInt(response1.data[i].a2021));
                                        obj6.data.push(parseInt(response1.data[i].a2021));

                                    }
                                }
                            }
                            // obj6.data.push(22225);
                            // obj6.data.push(29277);
                            // obj6.data.push(31342);

                            var total = [obj2, obj3, obj4, obj6, obj5];
                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Total de Fallecidos mensual SINADEF a nivel nacional 2018-2021*';
                            Chart1.series = total;
                            Highcharts.chart('chart10', Chart1);
                            //(total);
                        }
                    });
                    var objch8 = {
                        method: 'total_sinadef_c_resumen',
                        tabla: 'tablasinadef1',
                        flag: 'ok'
                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(objch8)),
                        success: function(response1) {
                            //(response1);
                            var categorias = [];

                            var obj2 = {
                                name: '2018',
                                type: 'column',
                                color: '#4F81BD',
                                data: []
                            };
                            var obj3 = {
                                name: '2019',
                                type: 'column',
                                color: '#F28F43',
                                data: []
                            };
                            var obj4 = {
                                name: '2020',
                                type: 'column',
                                color: 'red',
                                data: []
                            };
                            var obj5 = {
                                name: '2021*',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            datos = [];
                            for (i = 0; i < response1.data.length; i++) {

                                categorias.push(response1.data[i].mes);
                                obj2.data.push(parseInt(response1.data[i].a2018));
                                obj3.data.push(parseInt(response1.data[i].a2019));
                                obj4.data.push(parseInt(response1.data[i].a2020));

                                datos.push({
                                    'c1': i + 1,
                                    'c2': response1.data[i].mes,
                                    'c3': parseInt(response1.data[i].a2018),
                                    'c4': parseInt(response1.data[i].a2019),
                                    'c5': parseInt(response1.data[i].a2020),
                                    'c6': response1.data[i].a2021 == null ? '-' : i == 6 ? Math.round((parseInt(response1.data[i - 1].a2021) * 8) / 100 + parseInt(response1.data[i - 1].a2021)) : parseInt(response1.data[i].a2021)
                                });
                                if (response1.data[i].a2021 != 0 && i < 9) {
                                    if (i == 8) {
                                        obj5.data.push((parseInt(response1.data[i - 1].a2021) * 8) / 100 + parseInt(response1.data[i - 1].a2021));
                                    } else if (i < 8) {
                                        obj5.data.push(parseInt(response1.data[i].a2021));
                                    }

                                }
                            }


                            // Ahora dibujamos la tabla
                            let $cuerpoTabla = document.querySelector('#' + objch8.tabla);
                            // Recorrer todos los productos
                            $("#" + objch8.tabla + " " + "tr").remove();
                            // var a = 0;
                            datos.forEach(producto => {
                                // Crear un <tr>
                                // a++;
                                const $tr = document.createElement("tr");
                                // Creamos el <td> de nombre y lo adjuntamos a tr
                                let $c1 = document.createElement("td");
                                $c1.textContent = producto.c1; // el textContent del td es el nombre
                                $c1.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c1);

                                let $c2 = document.createElement("td");
                                $c2.textContent = producto.c2; // el textContent del td es el nombre
                                $c2.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c2);

                                let $c3 = document.createElement("td");
                                $c3.textContent = producto.c3; // el textContent del td es el nombre
                                $c3.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c3);

                                let $c4 = document.createElement("td");
                                $c4.textContent = producto.c4; // el textContent del td es el nombre
                                $c4.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c4);

                                let $c5 = document.createElement("td");
                                $c5.textContent = producto.c5; // el textContent del td es el nombre
                                $c5.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c5);

                                let $c6 = document.createElement("td");
                                $c6.textContent = producto.c6; // el textContent del td es el nombre
                                $c6.setAttribute('class', 'text-center bold');
                                $tr.appendChild($c6);

                                // El td de precio
                                // Finalmente agregamos el <tr> al cuerpo de la tabla
                                $cuerpoTabla.appendChild($tr);
                                // Y el ciclo se repite hasta que se termina de recorrer todo el arreglo
                            });


                            // obj5.data.push(31342);
                            var total = [obj2, obj3, obj4, obj5];
                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Fallecidos SINADEF a nivel nacional por meses por 2018-2021*';
                            Chart1.series = total;
                            Highcharts.chart('chart_8', Chart1);
                            //(total);
                        }
                    });

                    var obj = {
                        method: 'sinadef_pdiario_asemanal',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response2) {
                            //(response2);
                            var categorias = [];
                            var ratio = [];

                            var obj2s = {
                                name: '2018',
                                type: 'spline',
                                color: '#4F81BD',
                                data: []
                            };
                            var obj3s = {
                                name: '2019',
                                type: 'spline',
                                color: '#F28F43',
                                data: []
                            };
                            var obj4s = {
                                name: '2020',
                                type: 'spline',
                                color: 'red',
                                data: []
                            };
                            var obj5s = {
                                name: '2021* ',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj3d = {
                                name: '2018',
                                type: 'spline',
                                color: '#4F81BD',
                                data: []
                            };
                            var obj4d = {
                                name: '2019',
                                type: 'spline',
                                color: '#F28F43',
                                data: []
                            };
                            var obj5d = {
                                name: '2020',
                                type: 'spline',
                                color: 'red',
                                data: []
                            };
                            var obj6d = {
                                name: '2021* ',
                                type: 'spline',
                                color: '#731d1d',
                                data: []
                            };
                            var obj7s = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };
                            var obj8d = {
                                name: 'proyectado',
                                type: 'spline',
                                color: 'gray',
                                data: []
                            };

                            ratio.push(6, 7, 4, 2, 4);
                            for (i = 0; i < response2.data.length - 2; i++) {
                                categorias.push(response2.data[i].semana);
                                if (response2.data[i].ano == 2018) {
                                    obj2s.data.push(parseInt(response2.data[i].s));
                                    obj3d.data.push(parseInt(response2.data[i].d));
                                } else if (response2.data[i].ano == 2019) {
                                    obj3s.data.push(parseInt(response2.data[i].s));
                                    obj4d.data.push(parseInt(response2.data[i].d));
                                } else if (response2.data[i].ano == 2020) {
                                    obj4s.data.push(parseInt(response2.data[i].s));
                                    obj5d.data.push(parseInt(response2.data[i].d));

                                } else if (response2.data[i].ano == 2021) {
                                    obj5s.data.push(parseInt(response2.data[i].s));
                                    obj6d.data.push(parseInt(response2.data[i].d));
                                    obj7s.data.push(parseInt(response2.data[i].s));
                                    obj8d.data.push(parseInt(response2.data[i].d));
                                }

                            }

                            var proyec = obj5s.data[obj5s.data.length - 1];
                            var proyec2 = obj6d.data[obj6d.data.length - 1];
                            for (i = 0; i < 9; i++) {
                                if (i == 0) {
                                    proyec = obj5s.data[obj5s.data.length - 1];
                                    proyec2 = obj6d.data[obj6d.data.length - 1];
                                } else {
                                    proyec = obj7s.data[obj7s.data.length - 1];
                                    proyec2 = obj8d.data[obj8d.data.length - 1];
                                }
                                obj7s.data.push(((parseInt(proyec) * ratio[i]) / 100) + parseInt(proyec));
                                obj8d.data.push(((parseInt(proyec2) * ratio[i]) / 100) + parseInt(proyec2));
                            }

                            var total1 = [obj2s, obj3s, obj4s, obj7s, obj5s];
                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Fallecidos SINADEF acumulado semanal 2018-2021*';
                            Chart1.series = total1;
                            Highcharts.chart('chart20', Chart1);

                            var total2 = [obj3d, obj4d, obj5d, obj8d, obj6d];
                            var Chart2 = dualChart;
                            Chart2.xAxis[0].categories = categorias;
                            Chart2.title.text = 'Fallecidos SINADEF acumulado diario semanal 2018-2021*';
                            Chart2.series = total2;
                            Highcharts.chart('chartdd', Chart2);

                        }
                    });
                    var sinadef_d_s_a = {
                        method: 'sinadef_d_s_a',
                        div: 'chart60',
                        texto: 'Exceso de fallecidos SINADEF 2020, 2021* vs 2019, promedio diario, semanal y acumulado'
                    }
                    fs_chart4_salud(sinadef_d_s_a);
                    var obj_salud = {
                        method: 'total_sinadef_c_resumen',
                        div: 'chart70',
                        flag: 'sinadef',

                    }
                    fs_chart3_salud_sinadef_logica(obj_salud);

                    var obj = {
                        method: 'edadgeneral',

                    }
                    $.getJSON({
                        url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response2) {
                            //(response2);
                            var categoriass = [];

                            var obj2 = {
                                name: '2017',
                                type: 'column',
                                color: 'red',
                                data: []
                            };
                            var obj3 = {
                                name: '2018',
                                type: 'column',
                                color: '#4472C4',
                                data: []
                            };
                            var obj4 = {
                                name: '2019',
                                type: 'column',
                                color: '#ED7D31',
                                data: []
                            };
                            var obj5 = {
                                name: '2020',
                                type: 'column',
                                color: 'red',
                                data: []
                            };
                            var obj8 = {
                                name: '2021* ',
                                type: 'column',
                                color: '#731d1d',
                                data: []
                            };
                            var obj6 = {
                                type: 'spline',
                                name: '2020',
                                data: [],
                                color: 'red'

                            };
                            var obj7 = {
                                type: 'spline',
                                name: '2021* ',
                                data: [],
                                color: '#731d1d'

                            };

                            for (i = 0; i < response2.data.length; i++) {

                                categoriass.push(i);
                                obj2.data.push(parseInt(response2.data[i].a2017));
                                obj3.data.push(parseInt(response2.data[i].a2018));
                                obj4.data.push(parseInt(response2.data[i].a2019));
                                obj5.data.push(parseInt(response2.data[i].a2020));
                                obj8.data.push(parseInt(response2.data[i].a2021));
                                obj6.data.push(parseInt(response2.data[i].a2020));
                                obj7.data.push(parseInt(response2.data[i].a2021));


                            }

                            var total = [obj3, obj4, obj6, obj7];
                            var Chart1 = combine;
                            Chart1.xAxis.categories = categoriass;
                            Chart1.title.text = 'Comparativo anual de fallecidos SINADEF por Edad, 2017 - 2021*';
                            // Chart1.subtitle.text = '(2018-2021)';
                            Chart1.series = total;
                            Highcharts.chart('chart50', Chart1);
                            //(total);
                        }
                    });
                }
            });



        }
        if (params.classList.contains('modal_empresa')) {

            var obj = {
                titulo: 'ruc_empresa'
            }
            $.post({
                url: '../popupCompanies.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    // var url = "https://api.apis.net.pe/v1/ruc?numero=20131312955";
                    // axios.post(url)
                    //     .then(function(response) {
                    //         console.log(response);
                    //     });
                    var settings = {
                        "url": "https://api.apis.net.pe/v1/ruc?numero=20131312955",
                        "method": "GET",
                        "timeout": 0,
                    };

                    $.ajax(settings).done(function(response) {
                        console.log(response);
                    });

                }
            });
        }

        if (params.classList.contains('oxigeno_salud')) {
            var obj = {
                titulo: 'OXIGENO',
                tab: 'datos'
            }
            $.post({
                url: '../divsaludproyectado_oxigeno.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('div_salud_edad').innerHTML = response;
                    var params = { method: 'departamento' };
                    dropDownList('ddlDepartamentos_oxigeno', params);
                    // var objoxigeno = {
                    //     div: 'chartoxigeno_1',
                    //     method: 'oxigeno_regional',
                    //     color: 'estado',
                    //     f15: "0','1",
                    //     nivel: 'GN',
                    //     tipo: 'IOARR'
                    // };
                    // fs_chart_oxigeno(objoxigeno);
                    // var objoxigeno = {
                    //     div: 'chartoxigeno_2',
                    //     method: 'oxigeno_provincial',
                    //     color: 'estado',
                    //     f15: "0','1",
                    //     nivel: 'GN',
                    //     tipo: 'IOARR'
                    // };
                    // fs_chart_oxigeno(objoxigeno);
                    // var objoxigeno = {
                    //     div: 'chartoxigeno_3',
                    //     method: 'oxigeno_distrital',
                    //     color: 'estado',
                    //     f15: "0','1",
                    //     nivel: 'GN',
                    //     tipo: 'IOARR'
                    // };
                    // fs_chart_oxigeno(objoxigeno);

                    var objoxigeno = {
                        div: 'chartoxigeno_v2_2',
                        method: 'oxigeno_acumuladom3h',
                        color: 'estado'
                    };
                    fs_chart_oxigeno_acumuladom3h(objoxigeno);

                    var objsel = {
                        method: 'fs_chart_oxigeno_selector2',
                        div: 'chartoxigeno_v2_1',
                        flag: 'ok'
                    }
                    fs_chart_oxigeno_selector(objsel);

                }
            });
        }
        /**datarev */
        if (params.classList.contains('vacunas_salud')) {
            var obj = {
                titulo: 'VACUNAS',
                tab: 'datos'
            }
            $.post({
                url: '../divsaludproyectado_vacunas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('div_salud_edad').innerHTML = response;
                    var params = { method: 'departamento' };
                    dropDownList('ddlDepartamentos_vacunas', params);

                    var departamento = "AMAZONAS','ANCASH','APURIMAC','AREQUIPA','AYACUCHO','CAJAMARCA','CALLAO','CUSCO','HUANCAVELICA','HUANUCO','ICA','JUNIN','LA LIBERTAD','LAMBAYEQUE','LIMA','LORETO','MADRE DE DIOS','MOQUEGUA','PASCO','PIURA','PUNO','SAN MARTIN','TACNA','TUMBES','UCAYALI";
                    //cambiar todos
                    var obj11 = {
                        method: 'filtro_edad',
                        div: 'chartvacuna_1',
                        dpto: departamento,
                        sexo1: 'h',
                        sexo2: 'm'
                    }
                    fs_chart_vacuna_salud(obj11);
                    var objoxigeno = {
                        div: 'chartvacuna_2',
                        method: 'fs_vacuna_semanal',
                    };
                    fs_chart_vacuna(objoxigeno);

                    var objsel = {
                        method: 'fs_chart_vacunas_selector2',
                        div: 'chartvacuna_3',
                        orden: 'pf',
                        titulo: 'Días para completar vacunación a población mayor a 18 años por Ciudad',
                        dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                        prov: idprov == '[Seleccione]' ? '' : idprov,
                        dist: iddist == '[Seleccione]' ? '' : iddist,
                        tabla: 'tablavacuna2',
                        flag: 'ok'
                    }
                    fs_chart_vacunas_selector(objsel);

                    var objsel = {
                        method: 'fs_chart_vacunas_selector3',
                        div: 'chartvacuna_3b',
                        orden: 'pf',
                        titulo: '% Avance de vacunación (2 dósis) a población mayor a 18 años por Ciudad',
                        dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                        prov: idprov == '[Seleccione]' ? '' : idprov,
                        dist: iddist == '[Seleccione]' ? '' : iddist,
                        tabla: 'tablavacuna1',
                        flag: 'ok'
                    }
                    fs_chart_vacunas_selector2(objsel);


                    var obj_pie = {
                        div: 'chartvacuna_4',
                        titulo: '',
                        method: 'pie_salud',
                        color: 'estado'
                    }
                    fs_pie_vacuna_salud(obj_pie);
                    var obj_pie = {
                        div: 'chartvacuna_5',
                        titulo: '',
                        method: 'pie_salud2',
                        color: 'estado'
                    }
                    fs_pie2_vacuna_salud(obj_pie);
                    var obj11 = {
                        method: 'filtro_edad',
                        div: 'chartvacuna_6',
                        dpto: departamento,
                        sexo1: 'h',
                        sexo2: 'm'
                    }
                    fs_chart_vacuna_genero_salud(obj11);

                    var obj11 = {
                        method: 'filtro_edad',
                        div: 'chartvacuna_7',
                        dpto: departamento,
                        sexo1: 'h',
                        sexo2: 'm'
                    }
                    fs_chart_vacuna_genero_salud2(obj11);
                }
            });
        }


        if (params.classList.contains('ddldepa')) {

            if ($("#ddldepa").find(":selected").attr("value")) {
                // dashBoardEmpresas_top_tamaño(obj);
                var depa = $("#ddldepa").find(":selected").attr("value");
                var departamento = ['AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI', "AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI"];
                var obj11 = {
                    method: 'filtro_edad',
                    div: 'chartedad_11',
                    dpto: departamento[depa],
                    sexo1: 'h',
                    sexo2: 'm'
                }
                fs_chart_tipo_salud(obj11);

                var obj22 = {
                    method: 'filtro_sinadef',
                    div: 'chartedad_22',
                    dpto: departamento[depa],
                    sexo1: 'MASCULINO',
                    sexo2: 'FEMENINO'
                }
                fs_chart_tipo_salud(obj22);
                var obj33 = {
                    method: 'filtro_covid',
                    div: 'chartedad_33',
                    dpto: departamento[depa],
                    sexo1: 'MASCULINO',
                    sexo2: 'FEMENINO'
                }
                fs_chart_tipo_salud(obj33);
                $.post({
                    url: '../divsaludproyectado_edad.php?data2=' + encodeURIComponent(JSON.stringify({ dpto: depa })),
                    success: function(response) {}
                })
            }
        }
        /**filtro proyectos general inicio */

        if (params.classList.contains('dfuncion')) {
            var idfuncion = $("#dfuncion").find(":selected").text();
            var params = { method: 'programa', id: idfuncion };
            // console.log(params);
            dropDownListss('programa', params);
        }

        if (params.classList.contains('programa')) {
            var idprograma = $("#programa").find(":selected").text();
            var params = { method: 'subprograma', id: idprograma };
            dropDownList('sprograma', params);
        }

        /**filtro proyectos general end */
        if (params.classList.contains('ddlDepartamentos')) {
            var iddepa = $("#ddlDepartamentos").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias', params);
            console.log(params);
        }
        if (params.classList.contains('ddlProvincias')) {
            var idprov = $("#ddlProvincias").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos', params);
        }
        /*canon filtro*/
        if (params.classList.contains('ddlDepartamentos_canon')) {
            var iddepa = $("#ddlDepartamentos_canon").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias_canon', params);
        }
        if (params.classList.contains('ddlProvincias_canon')) {
            var idprov = $("#ddlProvincias_canon").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos_canon', params);
        }
        /**canon filtro */
        if (params.classList.contains('ddlDepartamentos_oxigeno')) {
            var iddepa = $("#ddlDepartamentos_oxigeno").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias_oxigeno', params);
        }
        if (params.classList.contains('ddlProvincias_oxigeno')) {
            var idprov = $("#ddlProvincias_oxigeno").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos_oxigeno', params);
        }

        if (params.classList.contains('ddlDepartamentos_minsa')) {
            var iddepa = $("#ddlDepartamentos_minsa").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias_minsa', params);
        }
        if (params.classList.contains('ddlProvincias_minsa')) {
            var idprov = $("#ddlProvincias_minsa").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos_minsa', params);
        }

        if (params.classList.contains('ddlDepartamentos_vacunas')) {
            var iddepa = $("#ddlDepartamentos_vacunas").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias_vacunas', params);
        }
        if (params.classList.contains('ddlProvincias_vacunas')) {
            var idprov = $("#ddlProvincias_vacunas").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos_vacunas', params);
        }

        if (params.classList.contains('ddlDepartamentos_sinadef')) {
            var iddepa = $("#ddlDepartamentos_sinadef").find(":selected").attr("value");
            var params = { method: 'provincias', id: iddepa };
            dropDownList('ddlProvincias_sinadef', params);
        }
        if (params.classList.contains('ddlProvincias_sinadef')) {
            var idprov = $("#ddlProvincias_sinadef").find(":selected").attr("value");
            var params = { method: 'distritos', id: idprov };
            dropDownList('ddlDistritos_sinadef', params);
        }
        //filtro proyectos
        if (params.classList.contains('filtroproyecto')) {
            var c15 = $("#brecha1").find(":selected").val();
            var ipmi = $("#brecha2").find(":selected").val();
            var iddpto = $("#ddlDepartamentos").find(":selected").text();
            var idprov = $("#ddlProvincias").find(":selected").text();
            var iddist = $("#ddlDistritos").find(":selected").text();

            var objChart10 = {
                div: 'chartbrecha_10',
                method: 'rango_brecha',
                tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                c15: c15 == '[Seleccione]' ? '' : c15,
                ipmi: ipmi == '[Seleccione]' ? '' : ipmi,
                flag: 'ok',
                tabla: 'cuerpoTabla2',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            };
            fs_chart4(objChart10);
        }
        //filtro
        //filtro proyectos general
        if (params.classList.contains('filtroproyecto_general')) {
            var c15 = $("#estado").find(":selected").val();
            var tipo = $("#tipo").find(":selected").val();
            var funcion = $("#funcion").find(":selected").text();
            var programa = $("#programa").find(":selected").text();
            var sprograma = $("#sprograma").find(":selected").text();
            var entidad = $("#entidad").find(":selected").text();

            var objChart10 = {
                div: 'chartgeneral_10',
                method: 'proyecto_general',
                // tipo: "IOARR','PROYECTO','PROGRAMA DE INVERSION",
                c15: c15 == '[Seleccione]' ? '' : c15,
                ipmi: ipmi == '[Seleccione]' ? '' : ipmi,
                flag: 'ok',
                vi_tipo: tipo == '[Seleccione]' ? '' : tipo,
                vi_funcion: funcion == '[Seleccione]' ? '' : funcion,
                vi_programa: programa == '[Seleccione]' ? '' : programa,
                vi_sprograma: sprograma == '[Seleccione]' ? '' : sprograma,
                vi_entidad: entidad == '[Seleccione]' ? '' : entidad
            };
            fs_chart_proyecto_general(objChart10);
        }
        //filtro 
        //filtro canon
        if (params.classList.contains('filtrocanon')) {
            var iddpto = $("#ddlDepartamentos_canon").find(":selected").text();
            var idprov = $("#ddlProvincias_canon").find(":selected").text();
            var iddist = $("#ddlDistritos_canon").find(":selected").text();

            var obj = {
                method: 'dist_anual_canon',
                tbl: 'tbl_dist_canon',
                flag: 'ok',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist

            }
            fs_chart_entidad_canon(obj)
        }
        //filtro canon
        if (params.classList.contains('filtroedad')) {
            var iddpto = $("#ddlDepartamentos").find(":selected").text();
            var idprov = $("#ddlProvincias").find(":selected").text();
            var iddist = $("#ddlDistritos").find(":selected").text();
            var obj55 = {
                method: 'chartcalorv2',
                div: 'chartedad_calor20_21',
                ano: '2020',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart_calorv2(obj55);
            var obj56 = {
                method: 'chartcalorv3',
                div: 'chartedad_calor_regiones',
                ano: '2021',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart_calorv3(obj56);
        }


        if (params.classList.contains('filtrominsa')) {
            var iddpto = $("#ddlDepartamentos_minsa").find(":selected").text();
            var idprov = $("#ddlProvincias_minsa").find(":selected").text();
            var iddist = $("#ddlDistritos_minsa").find(":selected").text();
            var obj = {
                method: 'total_minsa_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response2) {
                    //(response2);
                    var a = 0;
                    var b = 0;

                    for (i = 0; i < response2.data.length; i++) {

                        if (response2.data[i].a2020 != null) {
                            a = a + parseInt(response2.data[i].a2020);

                        }
                        if (response2.data[i].a2021 != null) {
                            if (i == 8) {
                                b = b + (parseInt(response2.data[i].a2021) * 30) / 100 + parseInt(response2.data[i].a2021);
                            } else {
                                b = b + parseInt(response2.data[i].a2021);

                            }
                        }
                    }
                    /* promedio*/
                    p = a / 10;
                    p2 = (b) / 8;
                    /* promedio*/
                    var ob1 = {
                        name: '2020',
                        color: 'red',
                        data: [a, 0]
                    }
                    var ob2 = {
                        name: '2021*',
                        color: '#731d1d',
                        data: [0, b]
                    }
                    var p1 = {
                        name: '2020',
                        color: 'red',
                        data: [p, 0]
                    }
                    var p2 = {
                        name: '2021*',
                        color: '#731d1d',
                        data: [0, p2]
                    }

                    /** prmedio semanal*/
                    var ps1 = {
                        name: '2020',
                        color: 'red',
                        data: [915, 0]
                    }
                    var ps2 = {
                            name: '2021*',
                            color: '#731d1d',
                            data: [0, 1144]
                        }
                        /**promedio diario */
                    var pd1 = {
                        name: '2020',
                        color: 'red',
                        data: [131, 0]
                    }
                    var pd2 = {
                        name: '2021*',
                        color: '#731d1d',
                        data: [0, 171]
                    }
                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2020', '2021'];
                    Chart.title.text = 'Total de fallecidos COVID19, 2020 - 2021*';
                    Chart.series = [ob2, ob1];
                    Highcharts.chart('chart6', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2020', '2021'];
                    Chart.title.text = 'Promedio mensual de fallecidos COVID19, 2020 - 2021*';
                    Chart.series = [p2, p1];
                    Highcharts.chart('chart7', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2020', '2021'];
                    Chart.title.text = 'Promedio semanal de fallecidos COVID19, 2020 - 2021*';
                    Chart.series = [ps2, ps1];
                    Highcharts.chart('chart9', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2020', '2021'];
                    Chart.title.text = 'Promedio diario de fallecidos COVID19, 2020 - 2021*';
                    Chart.series = [pd2, pd1];
                    Highcharts.chart('chart101', Chart);

                }
            });
            var obj = {
                method: 'total_minsa_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response1) {
                    //(response1);
                    var categorias = [];

                    var obj4 = {
                        name: '2020',
                        type: 'spline',

                        color: 'red',
                        data: []
                    };
                    var obj5 = {
                        name: '2021*',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };
                    // obj6.data.push(0, 0);
                    for (i = 0; i < response1.data.length; i++) {
                        categorias.push(response1.data[i].mes);
                        obj4.data.push(parseInt(response1.data[i].a2020));
                        if (response1.data[i].a2021 != 0) {
                            // obj5.data.push(parseInt(response1.data[i].a2021));
                            if (i == 8) {
                                obj6.data.push((parseInt(response1.data[i].a2021) * 30) / 100 + parseInt(response1.data[i].a2021));
                            } else if (i < 8) {
                                obj5.data.push(parseInt(response1.data[i].a2021));
                                obj6.data.push(parseInt(response1.data[i].a2021));
                            }
                        }
                    }
                    /*quiii falta proyeccion*/
                    // obj6.data.push(9034);
                    var total = [obj4, obj6, obj5];
                    var Chart1 = dualChart;
                    Chart1.xAxis.categories = categorias;
                    Chart1.title.text = 'Fallecidos Covid a nivel nacional meses por año 2020-2021*';
                    Chart1.series = total;
                    Highcharts.chart('chart1', Chart1);
                }
            });
            var obj = {
                method: 'total_minsa_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response1) {
                    //(response1);
                    var categorias = [];

                    var obj4 = {
                        name: '2020',
                        type: 'column',
                        color: 'red',
                        data: []
                    };
                    var obj5 = {
                        name: '2021*',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        name: '2021*',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };
                    for (i = 0; i < response1.data.length; i++) {
                        categorias.push(response1.data[i].meses);
                        obj4.data.push(parseInt(response1.data[i].a2020));
                        if (response1.data[i].a2021 != 0) {
                            // obj5.data.push(parseInt(response1.data[i].a2021));
                            if (i == 8) {
                                obj6.data.push((parseInt(response1.data[i].a2021) * 30) / 100 + parseInt(response1.data[i].a2021));
                            } else if (i < 8) {
                                obj5.data.push(parseInt(response1.data[i].a2021));
                                obj6.data.push(parseInt(response1.data[i].a2021));
                            }
                        }
                    }
                    // obj5.data[2] = 6404;
                    var Chart = dualChart;
                    Chart.xAxis.categories = categorias;
                    Chart.title.text = 'Total de fallecidos mensual COVID19, 2020- 2021*';
                    Chart.series = [obj4, obj6];
                    Highcharts.chart('chart8', Chart);
                }
            });
            var obj_salud = {
                method: 'total_minsa_c_resumen',
                div: 'chart102',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart3_salud(obj_salud);
            var obj_d_s_a = {
                method: 'covid_d_s_a',
                div: 'chart103',
                texto: 'fallecidos COVID19 2020/2021 vs 2019, diario, semanal y acumulado',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart4_salud(obj_d_s_a);
            var obj = {
                method: 'covid_pdiario_asemanal',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response1) {
                    //(response1);
                    var categorias = [];
                    var ratio = [];

                    var obj4 = {
                        name: '2020',
                        type: 'spline',
                        color: 'red',
                        data: []
                    };
                    var obj5 = {
                        name: '2021*',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        name: '2020',
                        type: 'spline',
                        color: 'red',
                        data: []
                    };
                    var obj7 = {
                        name: '2021*',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj9 = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };
                    var obj10 = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };
                    // obj9.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    obj4.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    categorias.push('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
                    obj6.data.push(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                    // categorias.push('1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12');
                    ratio.push(19, 17, 20, 21, 23);
                    for (i = 0; i < response1.data.length - 1; i++) {
                        categorias.push(response1.data[i].semana);
                        if (response1.data[i].ano == '2020') {
                            obj4.data.push(parseInt(response1.data[i].s));
                            obj6.data.push(parseInt(response1.data[i].d));
                        } else {
                            obj5.data.push(parseInt(response1.data[i].s));
                            obj7.data.push(parseInt(response1.data[i].d));
                            obj9.data.push(parseInt(response1.data[i].s));
                            obj10.data.push(parseInt(response1.data[i].d));

                        }
                    }
                    var proyec = obj5.data[obj5.data.length - 1];
                    var proyec2 = obj7.data[obj7.data.length - 1];
                    for (i = 0; i < 8; i++) {
                        if (i == 0) {
                            proyec = obj5.data[obj5.data.length - 1];
                            proyec2 = obj7.data[obj7.data.length - 1];
                        } else {
                            proyec = obj9.data[obj9.data.length - 1];
                            proyec2 = obj10.data[obj10.data.length - 1];
                        }
                        obj9.data.push(((parseInt(proyec) * ratio[i]) / 100) + parseInt(proyec));
                        obj10.data.push(((parseInt(proyec2) * ratio[i]) / 100) + parseInt(proyec2));
                    }


                    var total1 = [obj4, obj9, obj5];
                    var Chart1 = dualChart;
                    Chart1.xAxis[0].categories = categorias;
                    Chart1.title.text = 'Fallecidos COVID-19 MINSA acumulado semanal, 2020-2021*';
                    Chart1.series = total1;
                    Highcharts.chart('chart2', Chart1);
                    // //(total1);
                    var total2 = [obj6, obj10, obj7];
                    var Chartd = dualChart;
                    Chartd.xAxis[0].categories = categorias;
                    Chartd.title.text = 'Fallecidos COVID-19 MINSA promedio diario semanal, 2020-2021*';
                    Chartd.series = total2;
                    Highcharts.chart('chartd', Chartd);
                    // //(total2);
                }
            });
            var obj = {
                method: 'edadgeneralminsa',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response2) {
                    //(response2);
                    var categoriass = [];


                    var obj5 = {
                        name: '2020',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };
                    var obj8 = {
                        name: '2021',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        type: 'spline',
                        name: '2020',
                        data: [],
                        color: 'red',

                    };
                    var obj7 = {
                        type: 'spline',
                        name: '2021*',
                        data: [],
                        color: '#731d1d',

                    };

                    for (i = 0; i < response2.data.length; i++) {
                        categoriass.push(i);
                        obj5.data.push(parseInt(response2.data[i].a2020));
                        obj8.data.push(parseInt(response2.data[i].a2021));
                        obj6.data.push(parseInt(response2.data[i].a2020));
                        obj7.data.push(parseInt(response2.data[i].a2021));
                    }

                    var total = [obj6, obj7];
                    var Chart1 = combine;
                    Chart1.xAxis.categories = categoriass;
                    Chart1.title.text = 'Comparativo anual de fallecidos COVID-19 por Edad, 2020 - 2021*';
                    // Chart1.subtitle.text = '(2018-2021)';
                    Chart1.series = total;
                    Highcharts.chart('chart5', Chart1);
                    //(total);

                }
            });
        }

        if (params.classList.contains('filtrooxigeno')) {
            var iddpto = $("#ddlDepartamentos_oxigeno").find(":selected").text();
            var idprov = $("#ddlProvincias_oxigeno").find(":selected").text();
            var iddist = $("#ddlDistritos_oxigeno").find(":selected").text();

            var objsel = {
                method: 'fs_chart_oxigeno_selector2',
                div: 'chartoxigeno_v2_1',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist,
                flag: 'ok'
            }
            fs_chart_oxigeno_selector(objsel);

        }
        if (params.classList.contains('filtrovacunas')) {
            var iddpto = $("#ddlDepartamentos_vacunas").find(":selected").text();
            var idprov = $("#ddlProvincias_vacunas").find(":selected").text();
            var iddist = $("#ddlDistritos_vacunas").find(":selected").text();
            var objsel = {
                method: 'fs_chart_vacunas_selector2',
                div: 'chartvacuna_3',
                orden: 'pf',
                titulo: 'Días para completar vacunación a población mayor a 18 años por Ciudad',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist,
                flag: 'ok',
                tabla: 'tablavacuna2'
            }
            fs_chart_vacunas_selector(objsel);
            var objsel = {
                method: 'fs_chart_vacunas_selector3',
                div: 'chartvacuna_3b',
                orden: 'pf',
                titulo: '% Avance de vacunación (2 dósis) a población mayor a 18 años por Ciudad',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist,
                tabla: 'tablavacuna1',
                flag: 'ok'
            }
            fs_chart_vacunas_selector2(objsel);
        }

        //filtro2 sinadef
        if (params.classList.contains('filtrominsa_sinadef')) {
            var iddpto = $("#ddlDepartamentos_sinadef").find(":selected").text();
            var idprov = $("#ddlProvincias_sinadef").find(":selected").text();
            var iddist = $("#ddlDistritos_sinadef").find(":selected").text();
            //(iddpto);
            //(idprov);
            //(iddist);

            //gs1245
            var objsel = {
                method: 'fs_chart_sinadef_selector',
                div: 'chart_self',
                orden: 'pf',
                titulo: 'Exceso fallecidos % sobre el promedio histórico por Departamentos',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart_sinadef_selector(objsel);


            var objsel = {
                method: 'fs_chart_sinadef_selector',
                div: 'chart_selh',
                orden: 'ph',
                titulo: 'Exceso fallecidos por 100k hab. sobre el promedio por Departamento',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart_sinadef_selector(objsel);

            var obj = {
                method: 'total_sinadef_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response2) {
                    //(response2);
                    var a = 0;
                    var b = 0;
                    var c = 0;
                    var d = 0;
                    for (i = 0; i < response2.data.length; i++) {
                        if (response2.data[i].a2018 != null) {
                            a = a + parseInt(response2.data[i].a2018);
                        }
                        if (response2.data[i].a2019 != null) {
                            b = b + parseInt(response2.data[i].a2019);
                        }
                        if (response2.data[i].a2020 != null) {
                            c = c + parseInt(response2.data[i].a2020);
                        }
                        if (response2.data[i].a2021 != null) {
                            if (i == 8) {
                                d = d + (parseInt(response2.data[i].a2021) * 8) / 100 + parseInt(response2.data[i].a2021);
                            } else {
                                d = d + parseInt(response2.data[i].a2021);

                            }
                        }
                    }
                    /* promedio*/
                    pa = a / 12;
                    pb = b / 12;
                    pc = c / 12;
                    pd = (d) / 8;
                    /* promedio*/

                    var p1 = {
                        name: '2018',
                        color: 'red',
                        data: [pa, 0, 0, 0]
                    }
                    var p2 = {
                        name: '2019',
                        color: '#731d1d',
                        data: [0, pb, 0, 0]
                    }
                    var p3 = {
                        name: '2020',
                        color: '#731d1d',
                        data: [0, 0, pc, 0]
                    }
                    var p4 = {
                        name: '2021* ',
                        color: '#731d1d',
                        data: [0, 0, 0, pd]
                    }

                    /** prmedio semanal*/
                    var ps1 = {
                        name: '2018',
                        color: 'red',
                        data: [2164, 0, 0, 0]
                    }
                    var ps2 = {
                        name: '2019',
                        color: 'red',
                        data: [0, 2203, 0, 0]
                    }
                    var ps3 = {
                        name: '2020',
                        color: 'red',
                        data: [0, 0, 4059, 0]
                    }
                    var ps4 = {
                            name: '2021*',
                            color: '#731d1d',
                            data: [0, 0, 0, 6276]
                        }
                        /**promedio diario */
                    var pd1 = {
                        name: '2018',
                        color: 'red',
                        data: [309, 0, 0, 0]
                    }
                    var pd2 = {
                        name: '2019',
                        color: 'red',
                        data: [0, 315, 0, 0]
                    }
                    var pd3 = {
                        name: '2020',
                        color: 'red',
                        data: [0, 0, 580, 0]
                    }
                    var pd4 = {
                        name: '2021* ',
                        color: '#731d1d',
                        data: [0, 0, 0, 920]
                    }

                    var ob1 = {
                        name: '2018',
                        color: 'red',
                        data: [a, 0, 0, 0]
                    }
                    var ob2 = {
                        name: '2019',
                        color: 'red',
                        data: [0, b, 0, 0]
                    }

                    var ob3 = {
                        name: '2020',
                        color: 'red',
                        data: [0, 0, c, 0]
                    }
                    var ob4 = {
                        name: '2021* ',
                        color: '#731d1d',
                        data: [0, 0, 0, d]
                    }

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2018', '2019', '2020', '2021*'];
                    Chart.title.text = 'Total de fallecidos SINADEF 2018-2021* ';
                    Chart.series = [ob4, ob3, ob2, ob1];
                    Highcharts.chart('chart_6', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                    Chart.title.text = 'Promedio mensual de fallecidos SINADEF, 2018 - 2021*';
                    Chart.series = [p4, p3, p2, p1];
                    Highcharts.chart('chart_7', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                    Chart.title.text = 'Promedio semanal de fallecidos SINADEF, 2018 - 2021*';
                    Chart.series = [ps4, ps3, ps2, ps1];
                    Highcharts.chart('chart_9', Chart);

                    var Chart = stackedBarProyectado;
                    Chart.xAxis.categories = ['2018', '2019', '2020', '2021'];
                    Chart.title.text = 'Promedio diario de fallecidos SINADEF, 2018 - 2021*';
                    Chart.series = [pd4, pd3, pd2, pd1];
                    Highcharts.chart('chart_101', Chart);

                }
            });

            var obj = {
                method: 'total_sinadef_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response1) {
                    //(response1);
                    var categorias = [];

                    var obj2 = {
                        name: '2018',
                        type: 'spline',
                        color: '#4F81BD',
                        data: []
                    };
                    var obj3 = {
                        name: '2019',
                        type: 'spline',
                        color: '#F28F43',
                        data: []
                    };
                    var obj4 = {
                        name: '2020',
                        type: 'spline',
                        color: 'red',
                        data: []
                    };
                    var obj5 = {
                        name: '2021*  ',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };

                    for (i = 0; i < response1.data.length; i++) {

                        categorias.push(response1.data[i].mes);
                        obj2.data.push(parseInt(response1.data[i].a2018));
                        obj3.data.push(parseInt(response1.data[i].a2019));
                        obj4.data.push(parseInt(response1.data[i].a2020));

                        if (response1.data[i].a2021 != 0 && i < 9) {
                            if (i == 8) {
                                obj6.data.push((parseInt(response1.data[i].a2021) * 8) / 100 + parseInt(response1.data[i].a2021));

                            } else if (i < 8) {
                                obj5.data.push(parseInt(response1.data[i].a2021));
                                obj6.data.push(parseInt(response1.data[i].a2021));

                            }
                        }
                    }
                    // obj6.data.push(22225);
                    // obj6.data.push(29277);
                    // obj6.data.push(31342);

                    var total = [obj2, obj3, obj4, obj6, obj5];
                    var Chart1 = dualChart;
                    Chart1.xAxis[0].categories = categorias;
                    Chart1.title.text = 'Total de Fallecidos mensual SINADEF a nivel nacional 2018-2021*';
                    Chart1.series = total;
                    Highcharts.chart('chart10', Chart1);
                    //(total);
                }
            });
            var obj = {
                method: 'total_sinadef_c_resumen',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response1) {
                    //(response1);
                    var categorias = [];

                    var obj2 = {
                        name: '2018',
                        type: 'column',
                        color: '#4F81BD',
                        data: []
                    };
                    var obj3 = {
                        name: '2019',
                        type: 'column',
                        color: '#F28F43',
                        data: []
                    };
                    var obj4 = {
                        name: '2020',
                        type: 'column',
                        color: 'red',
                        data: []
                    };
                    var obj5 = {
                        name: '2021*',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };


                    for (i = 0; i < response1.data.length; i++) {

                        categorias.push(response1.data[i].mes);
                        obj2.data.push(parseInt(response1.data[i].a2018));
                        obj3.data.push(parseInt(response1.data[i].a2019));
                        obj4.data.push(parseInt(response1.data[i].a2020));

                        if (response1.data[i].a2021 != 0 && i < 9) {
                            if (i == 8) {
                                obj5.data.push((parseInt(response1.data[i].a2021) * 8) / 100 + parseInt(response1.data[i].a2021));
                            } else if (i < 8) {
                                obj5.data.push(parseInt(response1.data[i].a2021));
                            }

                        }
                    }

                    // obj5.data.push(31342);
                    var total = [obj2, obj3, obj4, obj5];
                    var Chart1 = dualChart;
                    Chart1.xAxis[0].categories = categorias;
                    Chart1.title.text = 'Fallecidos SINADEF a nivel nacional por meses por 2018-2021*';
                    Chart1.series = total;
                    Highcharts.chart('chart_8', Chart1);
                    //(total);
                }
            });

            var obj = {
                method: 'sinadef_pdiario_asemanal',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response2) {
                    //(response2);
                    var categorias = [];
                    var ratio = [];

                    var obj2s = {
                        name: '2018',
                        type: 'spline',
                        color: '#4F81BD',
                        data: []
                    };
                    var obj3s = {
                        name: '2019',
                        type: 'spline',
                        color: '#F28F43',
                        data: []
                    };
                    var obj4s = {
                        name: '2020',
                        type: 'spline',
                        color: 'red',
                        data: []
                    };
                    var obj5s = {
                        name: '2021* ',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj3d = {
                        name: '2018',
                        type: 'spline',
                        color: '#4F81BD',
                        data: []
                    };
                    var obj4d = {
                        name: '2019',
                        type: 'spline',
                        color: '#F28F43',
                        data: []
                    };
                    var obj5d = {
                        name: '2020',
                        type: 'spline',
                        color: 'red',
                        data: []
                    };
                    var obj6d = {
                        name: '2021* ',
                        type: 'spline',
                        color: '#731d1d',
                        data: []
                    };
                    var obj7s = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };
                    var obj8d = {
                        name: 'proyectado',
                        type: 'spline',
                        color: 'gray',
                        data: []
                    };

                    ratio.push(19, 17, 20, 21, 23);
                    for (i = 0; i < response2.data.length - 2; i++) {
                        categorias.push(response2.data[i].semana);
                        if (response2.data[i].ano == 2018) {
                            obj2s.data.push(parseInt(response2.data[i].s));
                            obj3d.data.push(parseInt(response2.data[i].d));
                        } else if (response2.data[i].ano == 2019) {
                            obj3s.data.push(parseInt(response2.data[i].s));
                            obj4d.data.push(parseInt(response2.data[i].d));
                        } else if (response2.data[i].ano == 2020) {
                            obj4s.data.push(parseInt(response2.data[i].s));
                            obj5d.data.push(parseInt(response2.data[i].d));

                        } else if (response2.data[i].ano == 2021) {
                            obj5s.data.push(parseInt(response2.data[i].s));
                            obj6d.data.push(parseInt(response2.data[i].d));
                            obj7s.data.push(parseInt(response2.data[i].s));
                            obj8d.data.push(parseInt(response2.data[i].d));
                        }

                    }

                    var proyec = obj5s.data[obj5s.data.length - 1];
                    var proyec2 = obj6d.data[obj6d.data.length - 1];
                    for (i = 0; i < 8; i++) {
                        if (i == 0) {
                            proyec = obj5s.data[obj5s.data.length - 1];
                            proyec2 = obj6d.data[obj6d.data.length - 1];
                        } else {
                            proyec = obj7s.data[obj7s.data.length - 1];
                            proyec2 = obj8d.data[obj8d.data.length - 1];
                        }
                        obj7s.data.push(((parseInt(proyec) * ratio[i]) / 100) + parseInt(proyec));
                        obj8d.data.push(((parseInt(proyec2) * ratio[i]) / 100) + parseInt(proyec2));
                    }

                    var total1 = [obj2s, obj3s, obj4s, obj7s, obj5s];
                    var Chart1 = dualChart;
                    Chart1.xAxis[0].categories = categorias;
                    Chart1.title.text = 'Fallecidos SINADEF acumulado semanal 2018-2021*';
                    Chart1.series = total1;
                    Highcharts.chart('chart20', Chart1);

                    var total2 = [obj3d, obj4d, obj5d, obj8d, obj6d];
                    var Chart2 = dualChart;
                    Chart2.xAxis[0].categories = categorias;
                    Chart2.title.text = 'Fallecidos SINADEF acumulado diario semanal 2018-2021*';
                    Chart2.series = total2;
                    Highcharts.chart('chartdd', Chart2);

                }
            });
            var sinadef_d_s_a = {
                method: 'sinadef_d_s_a',
                div: 'chart60',
                texto: 'fallecidos SINADEF 2020/2021 vs 2019, diario, semanal y acumulado',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist

            }
            fs_chart4_salud(sinadef_d_s_a);
            var obj_salud = {
                method: 'total_sinadef_c_resumen',
                div: 'chart70',
                flag: 'sinadef',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            fs_chart3_salud_sinadef_logica(obj_salud);

            var obj = {
                method: 'edadgeneral',
                dpto: iddpto == '[Seleccione]' ? '' : iddpto,
                prov: idprov == '[Seleccione]' ? '' : idprov,
                dist: iddist == '[Seleccione]' ? '' : iddist
            }
            $.getJSON({
                url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response2) {
                    //(response2);
                    var categoriass = [];

                    var obj2 = {
                        name: '2017',
                        type: 'column',
                        color: 'red',
                        data: []
                    };
                    var obj3 = {
                        name: '2018',
                        type: 'column',
                        color: '#4472C4',
                        data: []
                    };
                    var obj4 = {
                        name: '2019',
                        type: 'column',
                        color: '#ED7D31',
                        data: []
                    };
                    var obj5 = {
                        name: '2020',
                        type: 'column',
                        color: 'red',
                        data: []
                    };
                    var obj8 = {
                        name: '2021* ',
                        type: 'column',
                        color: '#731d1d',
                        data: []
                    };
                    var obj6 = {
                        type: 'spline',
                        name: '2020',
                        data: [],
                        color: 'red'

                    };
                    var obj7 = {
                        type: 'spline',
                        name: '2021* ',
                        data: [],
                        color: '#731d1d'

                    };

                    for (i = 0; i < response2.data.length; i++) {

                        categoriass.push(i);
                        obj2.data.push(parseInt(response2.data[i].a2017));
                        obj3.data.push(parseInt(response2.data[i].a2018));
                        obj4.data.push(parseInt(response2.data[i].a2019));
                        obj5.data.push(parseInt(response2.data[i].a2020));
                        obj8.data.push(parseInt(response2.data[i].a2021));
                        obj6.data.push(parseInt(response2.data[i].a2020));
                        obj7.data.push(parseInt(response2.data[i].a2021));


                    }

                    var total = [obj3, obj4, obj6, obj7];
                    var Chart1 = combine;
                    Chart1.xAxis.categories = categoriass;
                    Chart1.title.text = 'Comparativo anual de fallecidos SINADEF por Edad, 2017 - 2021*';
                    // Chart1.subtitle.text = '(2018-2021)';
                    Chart1.series = total;
                    Highcharts.chart('chart50', Chart1);
                    //(total);
                }
            });

        }

        if (params.classList.contains('ddlMapas')) {
            if ($("#ddlMapas").find(":selected").attr("value") == 1) {
                iniLoadAlert('Cargando Mapas!', 'Cambiando Mapa SINADEF', 1000);
                setTimeout(function() {
                    var obj = {
                        tipo: 'sinadef',
                    }
                    $.post({
                        url: '../divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divmapa').innerHTML = response;
                            map3 = L.map('map3').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map3);
                            tile1 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_sinadef_departamentos',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map3);
                            map3.invalidateSize();
                            map4 = L.map('map4').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map4);
                            tile2 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_sinadef_provincias',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map4);
                            map4.invalidateSize();
                            map5 = L.map('map5').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                            tile3 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_sinadef_distritos',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map5);
                            map5.invalidateSize();
                        }
                    });
                }, 1000);
            } else {
                iniLoadAlert('Cargando Mapas!', 'Cambiando Mapa COVID-19', 1000);
                setTimeout(function() {
                    var obj = {
                        tipo: 'covid',
                    }
                    $.post({
                        url: '../divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divmapa').innerHTML = response;
                            map3 = L.map('map3').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map3);
                            tile4 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_minsa_departamentos',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map3);
                            map3.invalidateSize();
                            map4 = L.map('map4').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map4);
                            tile5 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_minsa_provincias',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map4);
                            map4.invalidateSize();
                            map5 = L.map('map5').setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                            tile6 = L.tileLayer.betterWms(wmsrootUrl, {
                                layers: 'colaboraccion_2020:gen_fallecidos_minsa_distritos',
                                tiled: true,
                                format: 'image/png',
                                minZoom: 0,
                                continuousWorld: true,
                                transparent: true
                            }).addTo(map5);
                            map5.invalidateSize();
                        }
                    });
                }, 1000);

            }
        }
        if (params.classList.contains('lnkModalEntidades')) {
            var id = params.getAttribute('id');
            var obj = {
                method: 'fichaEntidadAmpliar',
                tipo: id.length,
                ubigeo: id
            }
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    var envio = response.data[0];
                    $.get({
                        url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                        success: function(response) {
                            envio.method = 'ciprlXregion';
                            $('#modal__').html(response);
                            $(".estilodashboard").css("background", "#1976d2");
                            $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                            if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                    success: function(response) {
                                        //(response.data);
                                        if (Object.keys(response).length > 0) {
                                            var chart = combinateChart;
                                            chart.series = response.data.series;
                                            chart.plotOptions.series.pointStart = response.data.pointStart;
                                            //(chart);
                                            Highcharts.chart('chart_' + envio.codubigeo, chart);
                                        }
                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_sinadef',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos SINADEF por región';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadefEntidad', Chart1);

                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_covid',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos COVID-19';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartCovidEntidad', Chart1);

                                    }
                                });
                            }
                        }
                    });
                }
            });

        }
        if (params.classList.contains('lnkModalEmpresas')) {
            var id = params.getAttribute('id');

            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ ruc: id, method: 'modal_Companies' })),
                success: function(response) {
                    var obj = response.data[0];
                    var empresa = response.data[0].empresa;
                    $.get({
                        url: '../popupCompanies.php?obj=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            $('#modal__').html(response);
                            $(".estilodashboard").css("background", "#1976d2");
                            $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                            obj.method = 'empconceptos';
                            $.getJSON({
                                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    //(response);
                                    if (Object.keys(response).length > 0) {
                                        chartCompanies(obj.ruc, response.data);

                                    }
                                }
                            });
                            var obj1 = {

                                empresa: empresa
                            };
                            $.post({
                                url: '../popupCompaniesTabla.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                success: function(response) {
                                    //(response);
                                    document.getElementById('divTablaproyectosComnpanies').innerHTML = response;
                                }
                            });
                            var url = "http://5.9.118.78:4000/getRucSunat";
                            axios.post(url, { nruc: id })
                                .then(function(response) {
                                    //(response);
                                    $("#contribuyente_condicion").val(response.data.result.condicion);
                                    $("#actividad_exterior").val(response.data.result.actividad_exterior);
                                    $("#emision_electronica").val(response.data.result.emision_electronica);
                                    $("#estado").val(response.data.result.estado);
                                    $("#fecha_inscripcion").val(response.data.result.fecha_inscripcion);
                                    $("#inicio_actividades").val(response.data.result.inicio_actividades);
                                    $("#nombre_comercial").val(response.data.result.nombre_comercial);
                                    $("#razon_social").val(response.data.result.razon_social);
                                    $("#sistema_contabilidad").val(response.data.result.sistema_contabilidad);
                                    $("#sistema_emision").val(response.data.result.sistema_emision);
                                    $("#tipo").val(response.data.result.tipo);
                                    $("#direccion").val(response.data.result.direccion);
                                    var htmTable = `<table class="table table-sm table-detail">
												<tr>
													<th style="background:#ddebf8"><b>CIU</b></th>
													<th style="background:#ddebf8"><b>descripcion</b></th>
												   
												   
												</tr>`;
                                    $.each(response.data.result.actividad_economica, function(i, obj) {
                                        htmTable += '<tr>';
                                        htmTable += '<td style="text-align:center;text-align:center">' + obj.ciiu + '</td>';
                                        htmTable += '<td style="text-align:center;text-align:center">' + obj.descripcion + '</td>';

                                        htmTable += '</tr>';
                                    });
                                    htmTable += `</table>`;
                                    $('#txtactividadEconomica').html(htmTable);
                                    var htmTable1 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8"><b>Nombre</b></th>
											<th style="background:#ddebf8"><b>Cargo</b></th>
											<th style="background:#ddebf8"><b>Desde</b></th>
										</tr>`;
                                    $.each(response.data.result.representantes_legales, function(i, obj) {

                                        htmTable1 += '<tr>';
                                        htmTable1 += '<td style="text-align:center">' + obj.nombre + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + obj.cargo + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + obj.desde + '</td>';
                                        htmTable1 += '</tr>';
                                    });
                                    htmTable1 += `</table>`;
                                    $('#txtrepresentantes').html(htmTable1);
                                    var htmTable2 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Periodo</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Año</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Mes</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Total de Trabajadores</b></th>
										</tr>`;
                                    $.each(response.data.result.cantidad_trabajadores, function(i, obj) {
                                        //(obj);
                                        htmTable2 += '<tr>';
                                        htmTable2 += '<td style="text-align:center">' + obj.periodo + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + obj.anio + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + getMes(parseInt(obj.mes)) + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + obj.total_trabajadores + '</td>';
                                        htmTable2 += '</tr>';
                                    });
                                    htmTable2 += `</table>`;
                                    $('#txtTrabajadores').html(htmTable2);
                                    var htmTable3 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
										</tr>`;
                                    $.each(response.data.result.comprobante_electronico, function(i, obj) {

                                        htmTable3 += '<tr>';
                                        htmTable3 += '<td style="text-align:center">' + obj + '</td>';
                                        htmTable3 += '</tr>';
                                    });
                                    htmTable3 += `</table>`;
                                    $('#txtcomprobante_electronico').html(htmTable3);
                                })
                                .catch(function(error) {
                                    //(error);
                                });
                            direccionesTemporalruc = id;
                            map111 = L.map('map111', { zoomControl: false }).setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map111);
                            new L.Control.Zoom({ position: 'topright' }).addTo(map111);
                            loadCompaniesPrincipal("ruc='" + id + "'");
                            var obj2 = {
                                method: 'empresas_expo_impo',
                                ruc: id
                            };

                            $.getJSON({
                                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj2)),
                                success: function(response) {

                                    //(response.data[0]);
                                    var htmTable3 = `<table class="table table-sm table-detail">
                                    <tr>
                                        <th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2019</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2018</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2017</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2016</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2015</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2014</b></th>
                                    </tr>`;
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Importaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Exportaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += `</table>`;
                                    $('#txtexportacionesTable').html(htmTable3);
                                    Highcharts.chart('txtexportaciones', {

                                        title: {
                                            text: 'Exportaciones e Importaciones 2014-2019'
                                        },

                                        subtitle: {
                                            text: '(millones de soles)'
                                        },

                                        yAxis: {
                                            title: {
                                                text: 'S/.'
                                            }
                                        },

                                        xAxis: {
                                            accessibility: {
                                                rangeDescription: 'Range: 2014-2019'
                                            }
                                        },

                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'middle'
                                        },

                                        plotOptions: {
                                            series: {
                                                label: {
                                                    connectorAllowed: false
                                                },
                                                pointStart: 2010
                                            }
                                        },

                                        series: [{
                                                name: 'Importacion',
                                                data: [parseInt(response.data[0].usdimpo2019), parseInt(response.data[0].usdimpo2018), parseInt(response.data[0].usdimpo2017), parseInt(response.data[0].usdimpo2016), parseInt(response.data[0].usdimpo2015), parseInt(response.data[0].usdimpo2014)]
                                            },
                                            {
                                                name: 'Exportacion',
                                                data: [parseInt(response.data[0].usdexpo2019), parseInt(response.data[0].usdexpo2018), parseInt(response.data[0].usdexpo2017), parseInt(response.data[0].usdexpo2016), parseInt(response.data[0].usdexpo2015), parseInt(response.data[0].usdexpo2014)]
                                            }
                                        ],

                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                },
                                                chartOptions: {
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                    });
                                }
                            });
                        }
                    });



                }
            });
        }
        if (params.classList.contains('openModalProyectos')) {
            var id = params.getAttribute('id');
            //(id);
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ codigo: id, method: 'openModalProyectos' })),
                success: function(response) {
                    var obj = response.data[0];
                    $.get({
                        url: './popupProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            $('#modal__').html(response);
                            $(".estilodashboard").css("background", "#1976d2");
                            $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');

                        }
                    });
                }
            });
        }
        if (params.classList.contains('macroKeys')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: '../divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('html_mef_empresas').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('macroKeysFuncion')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: '../divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('html_mef_empresas_2').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('generar_Mef')) {
            $("#chart_mef_empresas_1").css("display", "none");
            $("#chart_mef_empresas_2").css("display", "none");
            var obj = {
                method: 'macro_mef',
                tipo: $("#ddl_empresas_mef_tipo").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma").find(":selected").attr("value")
            }
            var cantidad = [];
            var resto = [];
            var total = [];
            var categories = [];
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    $.each(response.data, function(i, obj) {
                        cantidad.push(parseInt(obj.cantidad));
                        resto.push(parseInt(obj.resto));
                        total.push(parseInt(obj.total));
                        categories.push(obj.region);
                    });
                    var obj_cantidad = {
                        name: 'Cantidad',
                        type: 'spline',
                        color: '#000000',
                        yAxis: 1,
                        data: cantidad,
                        tooltip: {
                            valueSuffix: ''
                        },

                    }
                    var obj_monto = {
                        name: 'Costo Total',
                        type: 'column',
                        data: total,
                        tooltip: {
                            valueSuffix: ''
                        }

                    }
                    var obj_resto = {
                        name: 'Avance Ejecucion',
                        type: 'column',
                        color: '#A7454F',
                        data: resto,
                        tooltip: {
                            valueSuffix: ''
                        }

                    }
                    var obj = {
                        titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 por Región',
                        subtitulo: '',
                        categories: categories,
                        cantidad: obj_cantidad,
                        monto: obj_monto,
                        monto_usado: obj_resto,
                        div: 'chart_mef_empresas'
                    }

                    dashboarProyectos_congreso(obj);
                }
            });
            var obj = {
                tipo: $("#ddl_empresas_mef_tipo").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma").find(":selected").attr("value")
            }
            $.post({
                url: '../divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('html_mef_empresas').innerHTML = response;
                }
            });

        }
        if (params.classList.contains('generar_Mef_funcion')) {
            $("#chart_mef_empresas_1_funcion").css("display", "none");
            $("#chart_mef_empresas_2_funcion").css("display", "none");
            var obj = {
                method: 'macro_mef_Funcion',
                tipo: $("#ddl_empresas_mef_tipo_funcion").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma_funcion").find(":selected").attr("value")
            }
            var tmp = [];
            var cantidad = [];
            var categories = [];
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {

                    $.each(response.data, function(i, obj) {
                        cantidad.push(parseInt(obj.cantidad));
                        categories.push(obj.funcion_);
                        tmp.push({ name: obj.funcion_, weight: parseInt(obj.cantidad) });
                    });
                    wordcloud_Function({ div: 'chart_mef_empresas_funcion', data: tmp });
                }
            });
            var obj = {
                tipo: $("#ddl_empresas_mef_tipo_funcion").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma_funcion").find(":selected").attr("value")
            }
            $.post({
                url: '../divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('html_mef_empresas_2').innerHTML = response;
                }
            });

        }

        if (params.classList.contains('vertabla')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('vernivel')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('ver_nivel')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_salud')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliars')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_empresa')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');


            var target = $("[data-target='" + id + "']");
            var envio = {
                region: id1
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_nivelGobierno.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_nivel')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                region: array[0],
                gobierno: array[1]
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_entidades.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_cui')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                region: array[0],
                gobierno: array[1],
                entidad: array[2]
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_cui.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('closemodal')) {
            $('#exampleModal').modal('hide')
        }
        if (params.classList.contains('ddlGraficos_tamano')) {
            $("#chart_dash_indicador2_").css("display", "none");
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2' })),
                success: function(response) {
                    var cont = 0;
                    var sumOxi = 0;
                    var sumUtilidad = 0;
                    var oxi = [];
                    var utilidad = [];
                    var cantidad = [];
                    var categories = [];
                    var tmp = [];
                    $.each(response.data, function(i, obj) {
                        cont++;
                        oxi.push(parseInt(obj.oxi));
                        utilidad.push(parseInt(obj.utilidad));
                        categories.push(obj.nomtamanio);
                        cantidad.push(parseInt(obj.cantidad));
                        sumOxi += parseInt(obj.oxi);
                        sumUtilidad += parseInt(obj.utilidad);
                        tmp.push({ name: obj.nomtamanio, weight: parseInt(obj.cantidad) });
                    });
                    var obj = {
                        titulo: 'Titulo del Grafico',
                        subtitulo: 'millones de soles',
                        categories: categories,
                        oxi: oxi,
                        cantidad: cantidad,
                        utilidad: utilidad,
                        div: 'chart_dash_indicador2'
                    }


                    if ($("#ddlGraficos_tamano").find(":selected").attr("value") == 1) {
                        dashBoardEmpresas_top_tamaño(obj);
                    } else {
                        wordcloud_tamnio_ind({ div: 'chart_dash_indicador2', data: tmp });
                    }
                }
            });

        }
        if (params.classList.contains('lnlAmpliarTamanio')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var tamnio;
            if (id1 == 'Gran empresa') {
                tamnio = 2;
            } else if (id1 == 'Mediana empresa') {
                tamnio = 3;
            } else if (id1 == 'Pequeña empresa') {
                tamnio = 4;
            } else {
                tamnio = 1;
            }
            var target = $("[data-target='" + id + "']");
            var envio = {
                    tamanio: tamnio
                }
                //(envio);
            $.get({
                url: '../divresrmenyestadisticasEmpresasTamnio.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_sector')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                tamanio: array[1],
                sector: array[0]
            }
            $.get({
                url: '../divresrmenyestadisticasEmpresasSector.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('ddlGraficos_funcion_dashP')) {
            $("#chart_dash_proyectos_funcion_").css("display", "none");
            $("#chart_dash_proyectos_funcion_division").css("display", "none");
            $("#chart_dash_proyectos_funcion_division_programa").css("display", "none");
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind' })),
                success: function(response) {
                    var monto = [];
                    var cantidad = [];
                    var categories = [];
                    var tmp = [];
                    $.each(response.data, function(i, obj) {
                        monto.push(parseInt(obj.monto));
                        categories.push(obj.nomfase);
                        cantidad.push(parseInt(obj.cantidad));
                        tmp.push({ name: obj.nomfase, weight: parseInt(obj.cantidad) });
                    });
                    var obj = {
                        titulo: 'Grafico de Analisis de perfiles',
                        subtitulo: '',
                        categories: categories,
                        monto: monto,
                        cantidad: cantidad,
                        div: 'chart_dash_proyectos_funcion'
                    }


                    if ($("#ddlGraficos_funcion_dashP").find(":selected").attr("value") == 1) {
                        dashBoardProyects_perfiles(obj);
                    } else {
                        wordcloud_perfiles_({ div: 'chart_dash_proyectos_funcion', data: tmp });
                    }
                }
            });

        }
        if (params.classList.contains('lnkNivelGobiernoPartido')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var envio = 0;
            //(id1);
            if (id1 == 'GOBIERNO REGIONAL') {
                envio = 1;
            } else if (id1 == 'GOBIERNO PROVINCIAL') {
                envio = 2;
            } else if (id1 == 'GOBIERNO DISTRITAL') {
                envio = 3;
            }

            var target = $("[data-target='" + id + "']");
            var envio = {
                envio: envio,
                texto: id1.charAt(0).toUpperCase() + id1.slice(1).toLowerCase()
            }

            $.get({
                url: '../divresrmenyestadisticasMacroDashboardEntidades.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnlEntidadPartido_sub')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var target = $("[data-target='" + id + "']");
            var envio = {
                nivel: params.getAttribute('id').split('-')[1],
                partido: params.getAttribute('id').split('-')[0]
            }
            $.get({
                url: '../divresrmenyestadisticasMacroDashboardEntidades_sub.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpluarProyectosFase')) {
            var id = params.getAttribute('id').split('-');

            $.get({
                url: 'modalentidades.php',
                success: function(response) {
                    $('#modal__').html(response);
                    $(".estilodashboard").css("background", "#1976d2");
                    $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                    var obj = {
                            method: 'modal_funcion_entidades',
                            ubigeo: id[0],
                            nivel: id[1]
                        }
                        //(obj)
                    $.getJSON({
                        url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var tmp = [];
                            $.each(response.data, function(i, obj) {
                                tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                            });
                            wordcloud_mdl_partidos({ div: 'ml_char1', data: tmp });
                        }
                    });

                    var envio = {
                        ubigeo: id[0],
                        nivel: id[1],
                        tipo: 'funcion'
                    }
                    $.get({
                        url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                        success: function(response) {
                            document.getElementById('ml_html1').innerHTML = response;
                        }
                    });
                }
            });
        }
        if (params.classList.contains('lnkprogramaProyectosModal')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                    funcion: array[0],
                    ubigeo: array[2],
                    nivel: array[1],
                    tipo: 'programa'
                }
                //(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnksubprogramaProyectosModal')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                    funcion: array[1],
                    programa: array[0],
                    ubigeo: array[3],
                    nivel: array[2],
                    tipo: 'subprograma'
                }
                //(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkcui_entidades_perfil')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                    funcion: array[2],
                    programa: array[1],
                    ubigeo: array[4],
                    nivel: array[3],
                    subprograma: array[0],
                    tipo: 'proyectos'
                }
                //(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpluarProyectosperfil')) {
            var id = params.getAttribute('id').split('-');
            $.get({
                url: 'modalentidades.php',
                success: function(response) {
                    $('#modal__').html(response);
                    $(".estilodashboard").css("background", "#1976d2");
                    $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                    var obj = {
                        method: 'modal_funcion_entidades_perfil',
                        ubigeo: id[0],
                        nivel: id[1]
                    }
                    $.getJSON({
                        url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var tmp = [];
                            $.each(response.data, function(i, obj) {
                                tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                            });
                            wordcloud_mdl_partidos({ div: 'ml_char1', data: tmp });
                        }
                    });
                }
            });
        }
        if (params.classList.contains('lnkAmpliar_totales')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');

            var target = $("[data-target='" + id + "']");
            var envio = {
                partido: id1
            }
            $.get({
                url: '../entidades_totales_partidos.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_depa')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');

            var target = $("[data-target='" + id + "']");
            var envio = {
                departamento: id1
            }
            $.get({
                url: '../oxigeno_provincias.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_prov')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');

            var array = id1.split('$');
            var target = $("[data-target='" + id + "']");
            var envio = {
                provincia: id1,
            }
            $.get({
                url: '../oxigeno_distritos.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_GenralFuncion')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');


            var target = $("[data-target='" + id + "']");
            var envio = {
                tipo: 'funcion',
                perfil: id1
            }
            $.get({
                url: './proyectos_general.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_Genralprograma')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            //(id1);

            var target = $("[data-target='" + id + "']");
            var envio = {
                tipo: 'programa',
                perfil: id1.split('$')[0],
                funcion: id1.split('$')[1]
            }
            $.get({
                url: './proyectos_general.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_GenralSubprograma')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            //(id1);

            var target = $("[data-target='" + id + "']");
            var envio = {
                tipo: 'subprograma',
                perfil: id1.split('$')[0],
                funcion: id1.split('$')[1],
                programa: id1.split('$')[2]
            }
            $.get({
                url: './proyectos_general.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_GenralCUI')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            //(id1);

            var target = $("[data-target='" + id + "']");
            var envio = {
                tipo: 'cui',
                perfil: id1.split('$')[0],
                funcion: id1.split('$')[1],
                programa: id1.split('$')[2],
                subprograma: id1.split('$')[3]
            }
            $.get({
                url: './proyectos_general.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
    }
    var dropDownList = function(selector, params) {
        $('#' + selector).empty();

        $.ajax({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(params)),
            type: 'get',
            dataType: 'json',
            success: function(response) {
                $('#' + selector).append($("<option></option>").attr("value", '0').text('[Seleccione]'));
                for (i = 0; i < response.data.length; i++) {
                    $('#' + selector).append($("<option></option>").attr("value", response.data[i].id).text(response.data[i].etiqueta));
                }
            }
        });
    }
    var dropDownListado = function(selector, params) {
        $('#' + selector).empty();

        $.ajax({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(params)),
            type: 'get',
            dataType: 'json',
            success: function(response) {
                $('#' + selector + 'option[value="' + response.data[0].etiqueta + '"]').text();
                for (i = 0; i < response.data.length; i++) {
                    $('#' + selector + 'option[value="' + response.data[i].etiqueta + '"]').text();
                }
            }
        });
        // $('#dfuncion').selectpicker('refresh');
    }
    var dropDownListss = function(selector, params) {
        // $('#' + selector).empty();

        $.ajax({
            url: '../php/appdash.php?data=' + encodeURIComponent(JSON.stringify(params)),
            type: 'get',
            dataType: 'json',
            success: function(response) {

            }
        });
        // $('#dfuncion').selectpicker('refresh');
    }
    var chartCompanies = function(codempresa, result) {
        Highcharts.chart('chart_' + codempresa, {
            title: {
                text: 'Evoluci�n de Ingresos y Utilidades'
            },

            yAxis: {
                title: {
                    text: 'Soles'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: result.pointStart
                }
            },

            series: [{
                name: 'Ingresos',
                data: result.series.ingresos
            }, {
                name: 'Utilidades',
                data: result.series.utilidades
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    }
    var getMes = function(mes) {
        var mm;
        switch (mes) {
            case 1:
                mm = 'Enero';
                break;
            case 2:
                mm = 'Febrero';
                break;
            case 3:
                mm = 'Marzo';
                break;
            case 4:
                mm = 'Abril';
                break;
            case 5:
                mm = 'Mayo';
                break;
            case 6:
                mm = 'Junio';
                break;
            case 7:
                mm = 'Julio';
                break;
            case 8:
                mm = 'Agosto';
                break;
            case 9:
                mm = 'Setiembre';
                break;
            case 10:
                mm = 'Octubre';
                break;
            case 11:
                mm = 'Noviembre';
                break;
            case 12:
                mm = 'Diciembre';
                break;
        }
        return mm;
    }
    var defaultParameters = function(obj) {
        var params = {
            service: 'WFS',
            version: '1.0.0',
            request: 'GetFeature',
            typeName: 'colaboraccion_20:' + obj.layerName,
            outputFormat: 'application/json'
        };
        if (obj.hasOwnProperty('sql')) {
            if (obj.sql.length > 0) {
                params.cql_filter = obj.sql;
            }
        }
        return params;
    }
    var loadCompaniesPrincipal = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_empresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {

                var markers = L.geoJson(data, {

                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-briefcase',
                                markerColor: 'black',
                                shape: 'square',
                                prefix: 'fas'
                            })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(map111);
                    }
                });

            }
        });


    }
    return {
        init: function() {
            return init();
        },

        events: function(params) {
            return events(params);
        },
        sort: function(params) {
            return sort(params);
        }
    }
}();