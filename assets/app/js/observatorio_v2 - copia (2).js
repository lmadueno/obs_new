var AppObsV2 = function() {

    const NIVEL_DE_GOBIERNO_REGIONAL = 1;
    const NIVEL_DE_GOBIERNO_PROVINCIAL = 2;
    const NIVEL_DE_GOBIERNO_DISTRITAL = 3;
    const API_REST_GEOSERVER = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=colaboraccion_2020';

    /*
    const layers = ['region', 'provincia', 'distrito', 'region_minsa2', 'provincia_minsa2', 'distrito_minsa2',
                'region_sinadef2', 'provincia_sinadef2', 'distrito_sinadef2', 'region_oxigeno', 'provincia_oxigeno', 'distrito_oxigeno',
                'region_vacunas', 'provincia_vacunas', 'distrito_vacunas'];
            const layersVarName = ['lyrRegion', 'lyrProvincia', 'lyrDistrito', 'lyrRegionMinsa2', 'lyrProvinciaMinsa2', 'lyrDistritoMinsa2',
                'lyrRegionSinadef2', 'lyrProvinciaSinadef2', 'lyrDistritoSinadef2', 'lyrRegionOxigeno', 'lyrProvinciaOxigeno', 'lyrDistritoOxigeno',
                'lyrRegionVacunas', 'lyrProvinciaVacunas', 'lyrDistritoVacunas'];
    */
    const layers = [{
            dataId: 'region',
            name: 'lyrRegion',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia',
            name: 'lyrProvincia',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito',
            name: 'lyrDistrito',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_minsa2',
            name: 'lyrRegionMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_minsa2',
            name: 'lyrProvinciaMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_minsa2',
            name: 'lyrDistritoMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_sinadef2',
            name: 'lyrRegionSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_sinadef2',
            name: 'lyrProvinciaSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_sinadef2',
            name: 'lyrDistritoSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_oxigeno',
            name: 'lyrRegionOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_oxigeno',
            name: 'lyrProvinciaOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_oxigeno',
            name: 'lyrDistritoOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_vacunas',
            name: 'lyrRegionVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_vacunas',
            name: 'lyrProvinciaVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_vacunas',
            name: 'lyrDistritoVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        }
    ];

    let groupLayerCiprl = new L.layerGroup();
    let groupLayerHealth = new L.layerGroup();
    let groupLayerHealthOxygen = new L.layerGroup();
    let groupLayerHealthVaccine = new L.layerGroup();
    let elementId = '';
    let infoRegion, infoProvince, infoDistrict, legendDates = [];

    let angieNET = (nivel_gobierno = 0) => {
        const filter_obs = document.querySelector('filter-obs');
        const infolayer = document.querySelector('infolayer-obs');

        infolayer.departamentosIds = filter_obs.departamentosElegidos;
        infolayer.provinciasIds = filter_obs.provinciasElegidas;
        infolayer.distritosIds = filter_obs.distritosElegidos;
        infolayer.montoCiprlIni = filter_obs.montoCiprlIni;
        infolayer.montoCiprlFin = filter_obs.montoCiprlFin;
        infolayer.poblacionIni = filter_obs.poblacionIni;
        infolayer.poblacionFin = filter_obs.poblacionFin;
        infolayer.corredorMinero = filter_obs.capasElegidas.includes("cruza_corredor");
        infolayer.oleoducto = filter_obs.capasElegidas.includes("cruza_oleoducto");
        infolayer.corredorPrincipales = filter_obs.capasElegidas.includes("cruza_via");
        infolayer.lineasFerreas = filter_obs.capasElegidas.includes("cruza_lineaferrea");
        return infolayer.getInfoUbigeos(nivel_gobierno);
    }

    let init = () => {
        setDateLegend();
        getInfoUbigeoRegion();
        getInfoUbigeoProvince();
        getInfoUbigeoDistrict();

        document.addEventListener('click', function(e) {
            if (e.target && (e.target.hasAttribute("data-id") || e.target.hasAttribute("id"))) {
                const dataId = e.target.hasAttribute("data-id") ? e.target.getAttribute("data-id") : e.target.getAttribute("id");
                if (layers.some(x => x.dataId == dataId)) elementId = dataId;
                const isActive = e.target.checked;


                if (layers.reduce((acc, { dataId }) => (acc.push(dataId), acc), []).indexOf(elementId) != -1) {
                    localStorage.setItem('update-selectpicker', JSON.stringify({
                        'key': Math.random()
                    }));
                    $('.legendGroupLayers').show();
                }

                switch (dataId) {

                    case 'apagarciprl':
                        groupLayerCiprl.clearLayers();
                        break;
                    case 'infoCIPRL':
                        addGroupLayerToMap(isActive, groupLayerCiprl);
                        break;
                    case 'region':
                        addInfoLayerToMap(groupLayerCiprl, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case 'provincia':
                        addInfoLayerToMap(groupLayerCiprl, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case 'distrito':
                        addInfoLayerToMap(groupLayerCiprl, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;


                    case 'apagarsalud':
                        groupLayerHealth.clearLayers();
                        break;
                    case 'infoMinsa':
                        addGroupLayerToMap(isActive, groupLayerHealth);
                        break;
                    case 'region_minsa2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case 'provincia_minsa2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case 'distrito_minsa2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;
                    case 'region_sinadef2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case 'provincia_sinadef2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case 'distrito_sinadef2':
                        addInfoLayerToMap(groupLayerHealth, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;


                    case 'apagaroxigeno':
                        removeLegendFromMap('legendOx');
                        groupLayerHealthOxygen.clearLayers();
                        break;
                    case 'infoOxigeno':
                        addGroupLayerToMap(isActive, groupLayerHealthOxygen);
                        break;
                    case 'region_oxigeno':
                        addInfoLayerToMap(groupLayerHealthOxygen, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case 'provincia_oxigeno':
                        addInfoLayerToMap(groupLayerHealthOxygen, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case 'distrito_oxigeno':
                        addInfoLayerToMap(groupLayerHealthOxygen, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;


                    case 'apagarvacunas':
                        removeLegendFromMap('legendVac');
                        groupLayerHealthVaccine.clearLayers();
                        break;
                    case 'infoVacunas':
                        addGroupLayerToMap(isActive, groupLayerHealthVaccine);
                        break;
                    case 'region_vacunas':
                        addInfoLayerToMap(groupLayerHealthVaccine, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case 'provincia_vacunas':
                        addInfoLayerToMap(groupLayerHealthVaccine, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case 'distrito_vacunas':
                        addInfoLayerToMap(groupLayerHealthVaccine, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;
                }

                switch (dataId) {
                    case 'region_oxigeno':
                    case 'provincia_oxigeno':
                    case 'distrito_oxigeno':
                        addLegendToMap('legendOx', 'Mapa de Oxigeno requerido(m3/hora)', `Fecha de actualización ${findUpdateDate('oxigeno')}`);
                        break;
                    case 'region_vacunas':
                    case 'provincia_vacunas':
                    case 'distrito_vacunas':
                        addLegendToMap('legendVac', 'Mapa de Vacunas(Dosis 1 y 2/población)', `Fecha de actualización ${findUpdateDate('vacunas')}`);
                        break;
                }
            }
        });
    }

    let filterObs = (e) => {
        const capasOpcionales = [lyrCorredorMinero, lyrOleoducto, lyrCorrPrincipales, lyrFerreas];
        const query = e.detail.query;

        /* _______________________REFACTORIZAR_______________________ */
        switch (elementId) {
            case 'region':
                addInfoLayerToMap(groupLayerCiprl, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                break;
            case 'provincia':
                addInfoLayerToMap(groupLayerCiprl, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                break;
            case 'distrito':
                addInfoLayerToMap(groupLayerCiprl, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                break;


            case 'region_minsa2':
                addInfoLayerToMap(groupLayerHealth, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                break;
            case 'provincia_minsa2':
                addInfoLayerToMap(groupLayerHealth, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                break;
            case 'distrito_minsa2':
                addInfoLayerToMap(groupLayerHealth, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                break;
            case 'region_sinadef2':
                addInfoLayerToMap(groupLayerHealth, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                break;
            case 'provincia_sinadef2':
                addInfoLayerToMap(groupLayerHealth, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                break;
            case 'distrito_sinadef2':
                addInfoLayerToMap(groupLayerHealth, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                break;


            case 'region_oxigeno':
                addInfoLayerToMap(groupLayerHealthOxygen, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                break;
            case 'provincia_oxigeno':
                addInfoLayerToMap(groupLayerHealthOxygen, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                break;
            case 'distrito_oxigeno':
                addInfoLayerToMap(groupLayerHealthOxygen, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                break;

<<<<<<< HEAD
=======

>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901
            case 'region_vacunas':
                addInfoLayerToMap(groupLayerHealthVaccine, angieNET(1), handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                break;
            case 'provincia_vacunas':
                addInfoLayerToMap(groupLayerHealthVaccine, angieNET(2), handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                break;
            case 'distrito_vacunas':
                addInfoLayerToMap(groupLayerHealthVaccine, angieNET(3), handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                break;
        }

        /* _______________________REFACTORIZAR_______________________ */

        if (query.length > 0) {
            const departamento = query.filter(x => !['provincia', 'distrito'].includes(x.key));
            const provincia = query.filter(x => !['distrito'].includes(x.key));
            const distrito = query;

            layers.filter(x => x.nivelGobierno == NIVEL_DE_GOBIERNO_REGIONAL).forEach(layer => filterLayer(layer, getQuery(departamento).join(' and ')));
            layers.filter(x => x.nivelGobierno == NIVEL_DE_GOBIERNO_PROVINCIAL).forEach(layer => filterLayer(layer, getQuery(provincia).join(' and ')));
            layers.filter(x => x.nivelGobierno == NIVEL_DE_GOBIERNO_DISTRITAL).forEach(layer => filterLayer(layer, getQuery(distrito).join(' and ')));

<<<<<<< HEAD
            console.log(getQuery(departamento).join(' and '));
            console.log(getQuery(provincia).join(' and '));
            console.log(getQuery(distrito).join(' and '));

            const existsKeyCapas = (x) => x.key == 'capas';

            if (query.some(existsKeyCapas)) {
                ['cruza_corredor', 'cruza_oleoducto', 'cruza_via', 'cruza_lineaferrea'].forEach(function(layer, index) {
=======
            const existsKeyCapas = (x) => x.key == 'capas';

            if (query.some(existsKeyCapas)) {
                ['cruza_corredor', 'cruza_oleoducto', 'cruza_via', 'cruza_lineaferrea'].forEach(function (layer, index) {
>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901

                    if (query.find(existsKeyCapas).items.includes(layer) && !map.hasLayer(capasOpcionales[index])) {
                        capasOpcionales[index].addTo(map);
                    } else {
                        map.removeLayer(capasOpcionales[index]);
                    }
                });
            }
        } else {
            layers.forEach(layer => filterLayer(layer, '1=1'));

<<<<<<< HEAD
            capasOpcionales.forEach(function(capaOpcional) {
=======
            capasOpcionales.forEach(function (capaOpcional) {
>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901
                if (map.hasLayer(capaOpcional)) map.removeLayer(capaOpcional);
            });
        }
    }

    let getQuery = (query) => {
        return query.reduce((acc, { query }) => (acc.push(query), acc), []);
    }

    let addGroupLayerToMap = (isActive = false, groupLayer = null) => {
        if (isActive) {
            groupLayer.addTo(map);
        } else {
            groupLayer.removeLayer(map);
            map.removeLayer(groupLayer);
        }
    }

    let addLegendToMap = (legendClassName = '', title = '', subtitle = '') => {
        if (document.querySelector(`.${legendClassName}`) != null) document.querySelector(`.${legendClassName}`).remove();

        var legend = L.control({ position: "bottomleft" });

        legend.onAdd = function(map) {
            var div = L.DomUtil.create("div", legendClassName);
            div.innerHTML = setLegend(title, subtitle);
            return div;
        };

        legend.addTo(map);
    }

    let removeLegendFromMap = (legendClassName = '') => {
        document.querySelector(`.${legendClassName}`).remove();
    }

    let addInfoLayerToMap = (containerLayer, contentLayer, callBackClick, nivelGobierno) => {
        containerLayer.clearLayers();

        var markers = L.geoJson(contentLayer, {
            pointToLayer: function(feature, latlng) {
                var marker = L.marker(latlng, {
                    icon: L.divIcon({ html: '<img src="assets/app/img/info.png" title="' + feature.properties.nomubigeo + '" border="0"/>', className: 'info' })
                });
                return marker;
            }
        }).addTo(containerLayer);

        markers.on('click', function(e) {
            var data = e.layer.feature.properties;
            callBackClick(nivelGobierno, data);
        });
    }

    let handleClickLayerRegion = (nivelGobierno, data) => {
        console.log(data);
        App.mostrarModalEntidad(data);

    }

<<<<<<< HEAD
    let getInfoUbigeoRegion = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_dpto_geojson_info&outputFormat=application%2Fjson`);
=======
    let getInfoUbigeoRegion = async () => {
        const response = await fetch(`${API_REST_GEOSERVER}:info_region&outputFormat=application%2Fjson`);
>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901
        const infoUbigeoRegion = await response.json();
        infoRegion = infoUbigeoRegion.features;
    }

<<<<<<< HEAD
    let getInfoUbigeoProvince = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_provincia_geojson_info&outputFormat=application%2Fjson`);
=======
    let getInfoUbigeoProvince = async () => {
        const response = await fetch(`${API_REST_GEOSERVER}:info_provincia&outputFormat=application%2Fjson`);
>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901
        const infoUbigeoProvince = await response.json();
        infoProvince = infoUbigeoProvince.features;
    }

<<<<<<< HEAD
    let getInfoUbigeoDistrict = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_distrito_geojson_info&outputFormat=application%2Fjson`);
=======
    let getInfoUbigeoDistrict = async () => {
        const response = await fetch(`${API_REST_GEOSERVER}:info_distrito&outputFormat=application%2Fjson`);
>>>>>>> b7b0f40ab6b47162ddfce25dc0c84df983fef901
        const infoUbigeoDistrict = await response.json();
        infoDistrict = infoUbigeoDistrict.features;
    }

    let filterHealthLayers = () => {
        const regionId = filterParams.find(x => x.key == 'ddlRegion').selected;
        const provinceId = filterParams.find(x => x.key == 'ddlProvincia').selected;
        const layerProvinceNames = layers.filter(x => x.nivelGobierno == NIVEL_DE_GOBIERNO_PROVINCIAL || x.nivelGobierno == NIVEL_DE_GOBIERNO_DISTRITAL);
        const queryFilterRegion = "coddpto IN (" + regionId.join(',') + ")";
        const queryFilterProvince = "codprov IN (" + provinceId.join(',') + ")";

        if (regionId.length > 0) layers.forEach(layer => filterLayer(layer, queryFilterRegion));
        //if(provinceId.length > 0) layers.forEach(layer => filterLayer(layer, queryFilterProvince));
    }

    let filterLayer = ({ name }, queryFilter = '1 = 1') => {
        if (window[name]) {
            window[name].wmsParams.cql_filter = queryFilter;
            window[name].redraw();
        }
    }

    let setLegend = (title = '', subtitle = '') => {
        let html = '';
        const colors = ["037E34", "A2E07A", "ECFC25", "FC8A25", "E73E3E"];
        const labels = ["Muy Bajo", "Bajo", "Medio", "Alto", "Muy Alto"];

        html += `<h4>${title}</h4>`;
        html += labels.map((label, i) => `<i style="background: #${colors[i]}"></i><span>${label}</span><br>`).join('');
        html += `<h4 style="text-align: left">${subtitle}</h4>`;
        return html;
    }

    let setDateLegend = async() => {
        legendDates = await getDateLegend();
    }

    let getDateLegend = async() => {
        const objCoordenadas = { method: 'fecha_actualizacion_data' };
        const response = await fetch(`http://observatorio.colaboraccion.pe/php/app.php?data=${encodeURIComponent(JSON.stringify(objCoordenadas))}`);
        //const response = await fetch(`http://localhost/obs_new-master/php/app.php?data=${encodeURIComponent(JSON.stringify(objCoordenadas))}`);
        const data = await response.json();
        return data;
    }

    let findUpdateDate = (type) => {
        const exists = legendDates.find(x => x.tipo == type);
        if (exists) {
            return exists.fecha;
        }
    }

    let nvl = (elementId, replacementValue = '') => {
        const element = document.getElementById(elementId);
        if (element) {
            return document.getElementById(elementId).value.length == 0 ? replacementValue : document.getElementById(elementId).value;
        }
        return replacementValue;
    }

    return {
        init: function() {
            return init();
        },
        filterHealthLayers: function() {
            return filterHealthLayers();
        },
        findUpdateDate: function(type) {
            return findUpdateDate(type);
        },
        nvl: function(elementId, replacementValue) {
            return nvl(elementId, replacementValue);
        },
        filterObs: function(e) {
            return filterObs(e);
        }
    }
}();