var si;
var map;
var map2;
var map3;
var map33;
var map4;
var map5;
var map10;
var map111;
var layerCongreso;
var direccionesTemporal = [];
var direccionesTemporalruc;
var direccionesTemporal_ = [];
var direccionesLatLng_ = [];
var circleEmpresa;
var ubigeoTmp;
var circleDrawProyects;
var tile1;
var layerGroup;
var tile2;
var tile3;
var tile4;
var tile5;
var LayerGorupTemporal = [];
var tile6;
var drawnItems = new L.FeatureGroup();
var objCircle;
var objRectangle;
var objDrawLayerGroup;
var tmpDrawFilter = { region: [], provincia: [], distrito: [] };
var drawCodproyect = [];
var openNewDialog;
var tmpLineas = [];
var tmpVias = [];
var tmpOleoducto = [];
var tmpAccidentes = [];
var tmpHidrografias = [];
var tmpMineros = [];
var topoPoryects;
var layerCongreso;
var layerGroupCirculo;
var sidebar;
var lyrInfo;
var layerLatLngCoordenadas;
var direcciones;
var nav;
var cdn = 'assets/app/img/';
//var urlApi = 'http://192.168.99.248:8000/api/';
var owsrootUrl = 'http://78.46.16.8:8080/geoserver/colaboraccion_2020/ows';
var wmsrootUrl = 'http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms';
var modal_lg = $('#modal_lg');
var ddlFase2 = $('#ddlFases2').clone();
var ddlNivel2 = $('#ddlNivel2').clone();
var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

var filterParams = [
    { key: 'ddlRegion', lyr: 'projects', column: 'coddpto', type: 'Number', data: [], selected: [] },
    { key: 'ddlProvincia', lyr: 'projects', column: 'codubigeo', type: 'String', data: [], selected: [] },
    { key: 'ddlFases', lyr: 'projects', column: 'codfase', type: 'Number', data: [], selected: [] },
    { key: 'txtEjecucion', lyr: 'projects', column: 'en_ejecucion', type: 'String', selected: { txtEjecucion: '' } },
    { key: 'txtExpedienteTecnico', lyr: 'projects', column: 'expediente_tecnico', type: 'String', selected: { txtExpedienteTecnico: '' } },
    { key: 'ddlNivel', lyr: 'projects', column: 'codnivelgobierno', type: 'Number', data: [], selected: [] },
    { key: 'txtFecRegistro', lyr: 'projects', column: 'fecregistro', type: 'Date', data: [], selected: { txtFecIni: '', txtFecFin: '' } },
    { key: 'ddlFuncion', lyr: 'projects', column: 'codfuncionproyecto', type: 'Number', data: [], selected: [] },
    { key: 'ddlPrograma', lyr: 'projects', column: 'codprogramaproyecto', type: 'Number', data: [], selected: [] },
    { key: 'ddlSubPrograma', lyr: 'projects', column: 'codsubprogramaproyecto', type: 'Number', data: [], selected: [] },
    { key: 'txtMontoAsignado', lyr: 'projects', column: 'monto', type: 'Number', selected: { txtMontoIni: '', txtMontoFin: '' } },
    { key: 'txtMontoCIPRL', lyr: 'projects', column: 'tope_ciprl', type: 'Number', selected: { txtMontoCIPRLIni: '', txtMontoCIPRLFin: '' } },
    { key: 'txtPoblacion', lyr: 'projects', column: 'poblacion', type: 'Number', selected: { txtPoblacionIni: '', txtPoblacionFin: '' } },
    { key: 'ddlOrigen', lyr: 'projects', column: 'codorigen', type: 'Number', data: [], selected: [] },
    { key: 'ddlEstadoOxi', lyr: 'projects', column: 'estadooxi', type: 'Number', data: [], selected: [] },

    { key: 'ddlRegion2', lyr: 'companies', column: 'coddpto', type: 'Number', data: [], selected: [] },
    { key: 'ddlTamano2', lyr: 'companies', column: 'codcodtamanio', type: 'Number', data: [], selected: [] },
    { key: 'ddlSector2', lyr: 'companies', column: 'codsector', type: 'Number', data: [], selected: [] },
    { key: 'ddlGrupo2', lyr: 'companies', column: 'codgroup', type: 'Number', data: [], selected: [] },
    { key: 'txtRuc', lyr: 'companies', column: 'ruc', type: 'String', selected: { txtRuc: '' } },
    { key: 'txtRazonSocial', lyr: 'companies', column: 'empresa', type: 'String', selected: { txtRazonSocial: '' } },
    { key: 'txtNroTrab', lyr: 'companies', column: 'nro_trabajadores', type: 'Number', selected: { txtNroTrabIni: '', txtNroTrabFin: '' } },
    { key: 'txtIngresos', lyr: 'companies', column: 'ingresos', type: 'Number', selected: { txtIngresosIni: '', txtIngresosFin: '' } },
    { key: 'txtUtil', lyr: 'companies', column: 'utilidad', type: 'Number', selected: { txtUtilIni: '', txtUtilFin: '' } },
    { key: 'txtPatrimonio', lyr: 'companies', column: 'patrimonio', type: 'Number', selected: { txtPatrimonioIni: '', txtPatrimonioFin: '' } },
    { key: 'txtOxi', lyr: 'companies', column: 'oxi', type: 'Number', selected: { txtOxiIni: '', txtOxiFin: '' } },
    { key: 'txtradioBuena1', lyr: 'companies', column: 'listado', type: 'String', selected: { txtradioBuena1: '' } },
    { key: 'txtrasContribuyente', lyr: 'companies', column: 'resoluciongrupo', type: 'String', selected: { txtrasContribuyente: '' } },
    { key: 'txtProyecto', lyr: 'individual', column: 'oportunidad', operator: 'like', type: 'String', selected: { txtProyecto: '' } },
    { key: 'txtradioOxI', lyr: 'companies', column: 'participaoxi	', type: 'String', selected: { txtradioOxI: '' } },

    { key: 'ddlRegion3', lyr: 'politica', column: 'coddpto', type: 'Number', data: [], selected: [] },
    { key: 'ddlProvincia2', lyr: 'politica', column: 'codubigeo', type: 'String', data: [], selected: [] },
    { key: 'ddlPartidosPoliticos', lyr: 'politica', column: 'partido_politico', type: 'String', data: [], selected: [] },
];

var clusters = {
    'projects': {
        1: App.createCluster('idea'),
        2: App.createCluster('formulacion'),
        3: App.createCluster('evaluacion'),
        4: App.createCluster('perfil'),
        5: App.createCluster('no-viable'),
        6: App.createCluster('expediente'),
        7: App.createCluster('licitacion'),
        8: App.createCluster('adjudicado'),
        11: App.createCluster('ejecucion')
    },
    'companies': {
        1: App.createCluster('companies')
    },
    'universidades': {
        1: App.createCluster('college')
    },
    'routes': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'oleoducto': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'corredor': {
        lyrGroup: L.layerGroup(),
        items: []
    },

    'inversion': {
        lyrGroup: L.layerGroup(),
        items: []
    },

    'poblacion': {
        lyrGroup: L.layerGroup(),
        items: []
    },

    'poblacion_calor': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'pobreza': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'pobreza_calor': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'Mapas_de_Calor': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'congreso': {
        lyrGroup: L.layerGroup(),
        items: []
    },
    'general_region': {
        lyrGroup: L.layerGroup(),
        items: []
    } //este es el cluster (agrupacion q cargara y sefiltrara),

};
var layers = [{
        title: 'Por defecto',
        icon: 'assets/iconLayers/icons/openstreetmap_mapnik.png',
        layer: L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            mapID: 'default',
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        })
    },
    {
        title: 'Satélite',
        icon: 'assets/iconLayers/icons/here_satelliteday.png',
        layer: L.tileLayer('http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
            mapID: 'satellite',
            attribution: 'Map &copy; 1987-2014 <a href="http://developer.here.com">HERE</a>'
        })
    },
    {
        title: 'B/N',
        icon: 'assets/iconLayers/icons/openstreetmap_blackandwhite.png',
        layer: L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
            mapID: 'blackAndWhite',
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        })
    },
    {
        title: 'Positrón',
        icon: 'assets/iconLayers/icons/cartodb_positron.png',
        layer: L.tileLayer('http://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png', {
            mapID: 'positron',
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>'
        })
    }
];

var overLayers = [{
        key: 'apagar',
        group: "____________",
        layers: [{
            key: 'apagar',
            name: 'Cerrar',
            icon: '<i class="fas fa-align-justify"></i>',
            layer: L.layerGroup(),
            //active: true,
        }]
    },

    {
        key: 'Limite',
		collapsed: true,
        group: "Limit",
        layers: [{
            key: 'limit',
            name: 'Limite Frontera',
            icon: '<i class="fas fa-fill"></i>',
            layer: L.layerGroup(),
            active: false,
        }]
    },
    {
        key: 'baseLayer',
        group: "Capas base",
        layers: [

            {
                key: 'proyectos',
                name: 'Proyectos',
                icon: '<i class="fas fa-map-marker"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'Mapas_de_Calor',
                name: 'Termoinfografia',
                icon: '<i class="fas fa-atlas"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'empresas',
                name: 'Empresas',
                icon: '<i class="fas fa-briefcase"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'universidades',
                name: 'Universidades',
                icon: '<i class="fas fa-university"></i>',
                layer: L.layerGroup(),
                active: false
            }
        ]
    },
    {
        key: 'baseUbigeo',
        group: "CIPRL",
		collapsed: true,
        layers: [
		{
            key: 'apagarciprl',
            name: 'Apagar',
            icon: '<i class="fas fa-power-off"></i>',
            layer: L.layerGroup(),
            type: 'wms',
            control: 'radio'
        },{
                key: 'infoCIPRL',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegion: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrProv: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrDist: L.layerGroup(),
                /*{
                					lyrGroup: L.layerGroup(),
                					items: []
                				},*/
                type: 'wms'
            },

            {
                key: 'region',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    },
    {
    key: 'baseMinsa',
    group: "SALUD",
    collapsed: true,
    layers: [{
        key: 'apagarsalud',
        name: 'Apagar',
        icon: '<i class="fas fa-power-off"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },
    {
        key: 'infoMinsa',
        name: 'Información',
        icon: '<i class="fas fa-info-circle"></i>',
        layer: L.layerGroup(),
        lyrRegion: L.layerGroup(),
        lyrProv: L.layerGroup(),
        lyrDist: L.layerGroup(),
        type: 'wms'
    },
    {
        key: 'region_minsa2',
        name: 'GR COVID/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },
    {
        key: 'provincia_minsa2',
        name: 'MP COVID/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },
    {
        key: 'distrito_minsa2',
        name: 'MD COVID/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },

    {
        key: 'region_sinadef2',
        name: 'GR SINADEF/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },
    {
        key: 'provincia_sinadef2',
        name: 'MP SINADEF/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    },
    {
        key: 'distrito_sinadef2',
        name: 'MD SINADEF/P',
        icon: '<i class="fas fa-globe-americas"></i>',
        layer: L.layerGroup(),
        type: 'wms',
        control: 'radio'
    }
    ]
	},
	{
        key: 'baseOxigeno',
        group: "Salud Oxigeno",
        collapsed: true,
        layers: [{
                key: 'apagaroxigeno',
                name: 'Apagar',
                icon: '<i class="fas fa-power-off"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'infoOxigeno',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',
                layer: L.layerGroup(),
                lyrRegion: L.layerGroup(),
                lyrProv: L.layerGroup(),
                lyrDist: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'region_oxigeno',
                name: 'GR Oxigeno',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia_oxigeno',
                name: 'MP Oxigeno',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito_oxigeno',
                name: 'MD Oxigeno',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
        ]
    },
	{
        key: 'baseVacunas',
        group: "Salud Vacunas",
        collapsed: true,
        layers: [{
                key: 'apagarvacunas',
                name: 'Apagar',
                icon: '<i class="fas fa-power-off"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'infoVacunas',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',
                layer: L.layerGroup(),
                lyrRegion: L.layerGroup(),
                lyrProv: L.layerGroup(),
                lyrDist: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'region_vacunas',
                name: 'GR Vacunas',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia_vacunas',
                name: 'MP Vacunas',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito_vacunas',
                name: 'MD Vacunas',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
        ]
    },
    {
        key: 'baseSinadef',
        group: "SINADEF",
        layers: [{
                key: 'infoSinadef',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegion: L.layerGroup(),
                lyrProv: L.layerGroup(),
                lyrDist: L.layerGroup(),
                type: 'wms'
            },

            {
                key: 'region_sinadef',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia_sinadef',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito_sinadef',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    },

    /****************************************************************************************************************************************
    																VCTM
    *****************************************************************************************************************************************/

    {
        key: 'baseMinsa2',
        group: "COVID-19/Pob",
        layers: [{
                key: 'infoMinsa2',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegion: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrProv: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrDist: L.layerGroup(),
                /*{
                					lyrGroup: L.layerGroup(),
                					items: []
                				},*/
                type: 'wms'
            },

            {
                key: 'region_minsa2',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia_minsa2',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito_minsa2',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    },
    {
        key: 'baseSinadef2',
        group: "SINADEF/Pob",
        layers: [{
                key: 'infoSinadef2',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegion: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrProv: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrDist: L.layerGroup(),
                /*{
                					lyrGroup: L.layerGroup(),
                					items: []
                				},*/
                type: 'wms'
            },

            {
                key: 'region_sinadef2',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provincia_sinadef2',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distrito_sinadef2',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    },

    /****************************************************************************************************************************************
    																FIN VCTM
    *****************************************************************************************************************************************/

    {
        key: 'basePolitica',
        group: "Presiden.",
        collapsed: true,
        layers: [{
                key: 'infoPol',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegionPolitica: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrProvPolitica: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrDistPolitica: L.layerGroup(),
                /*{
                					lyrGroup: L.layerGroup(),
                					items: []
                				},*/
                type: 'wms'
            },

            {
                key: 'regionPolitica',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provinciaPolitica',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distritoPolitica',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    }, {
        key: 'basePolitica|',
        group: "Municipal",
        collapsed: true,
        layers: [{
                key: 'infoPol1',
                name: 'Información',
                icon: '<i class="fas fa-info-circle"></i>',

                layer: L.layerGroup(),

                lyrRegionPolitica: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrProvPolitica: L.layerGroup()
                    /*{
                    					lyrGroup: L.layerGroup(),
                    					items: []
                    				}*/
                    ,
                lyrDistPolitica: L.layerGroup(),
                /*{
                					lyrGroup: L.layerGroup(),
                					items: []
                				},*/
                type: 'wms'
            },

            {
                key: 'regionPolitica',
                name: 'Región',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'provinciaPolitica',
                name: 'Provincia',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            },
            {
                key: 'distritoPolitica',
                name: 'Distrito',
                icon: '<i class="fas fa-globe-americas"></i>',
                layer: L.layerGroup(),
                type: 'wms',
                control: 'radio'
            }
        ]
    },
    {
        key: 'baseMineria',
        group: "Energia y Minas",
        collapsed: true,
        layers: [

            {
                key: 'corredor',
                name: 'C. Minero',
                icon: '<i class="fas fa-industry"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'inversion',
                name: 'Inv. Minera',
                icon: '<i class="fas fa-burn"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'oleoducto',
                name: 'Oleoducto',
                icon: '<i class="fas fa-filter"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'areaminera',
                name: 'A.Operacion',
                icon: '<i class="fas fa-filter"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'proyectosmineros',
                name: 'P. Mineros',
                icon: '<i class="fas fa-map-marker"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'carteraproyectos',
                name: 'Cartera Proy.',
                icon: '<i class="fas fa-filter"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'reinfo',
                name: 'Re Info',
                icon: '<i class="fas fa-filter"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'pasivoambiental',
                name: 'Pasivo Amb.',
                icon: '<i class="fas fa-filter"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            }
        ]
    },
	{
		key: 'basePrincipal',
        group: "C. principales",
        collapsed: true,
        layers: [
		]
	},
    {
        key: 'baseExtras',
        group: "C. complemento",
        collapsed: true,
        layers: [{
                key: 'vias',
                name: 'Vías',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'corrprincipales',
                name: 'C. Principales',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'corrsecundarios',
                name: 'C. Secundarios',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'accidentes',
                name: 'Accidentes',
                icon: '<i class="fas fa-ambulance"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'aeropuertos',
                name: 'Aeropuertos',
                icon: '<i class="fas fa-plane"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'puertos',
                name: 'Puertos',
                icon: '<i class="fas fa-anchor"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'ferreas',
                name: 'Lineas ferreas',
                icon: '<i class="fas fa-train"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },

            {
                key: 'corrhidrovias',
                name: 'Hidrovias',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'conflictos',
                name: 'Conflictos',
                icon: '<i class="fas fa-ban"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'poblacion',
                name: 'Población',
                icon: '<i class="fas fa-atlas"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
            {
                key: 'pobreza',
                name: 'Pobreza',
                icon: '<i class="fas fa-band-aid"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            }, {
                key: 'comisarias',
                name: 'Comisarias',
                icon: '<i class="fas fa-school"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            }, 
			{
                key: 'hospitales',
                name: 'Hospitales',
                icon: '<i class="fas fa-hospital"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'redes_regionales',
                name: 'R. regionales',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            }
        ]
    },
    {
        key: 'baseExtras',
        group: "REDNACE",
        collapsed: true,
        layers: [
			{
                key: 'sbitel',
                name: 'On air Bitel',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'nbitel',
                name: 'Nodos Bitel',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'ntelefonica',
                name: 'Nodos Telefónica',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'ninternexa',
                name: 'Nodos InterNexa',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'foptical',
                name: 'F. óptica',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'foptmicro',
                name: 'Microondas',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },			
			{
                key: 'conexion',
                name: 'Conexión',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'core',
                name: 'Core',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            },
			{
                key: 'distribucion',
                name: 'Distribución',
                icon: '<i class="fas fa-route"></i>',
                layer: L.layerGroup(),
                type: 'wms'
            }
        ]
    },
    /*{
        key: 'baseTelecom',
        group: "Telecom",
        collapsed: true,
        layers: [
			{
                key: 'srutas',
                name: 'Rutas',
                icon: '<i class="fas fa-route"></i>',
                layer: {
					type: "tileLayer.wms",
					args: ["http://78.46.16.8:8080/geoserver/colaboraccion_2020/wms", {
						layers: 'colaboraccion_2020:telecom_rutas_nodos',
						format: 'image/png',
						transparent: true,
						opacity: 0.6
					}]
				},
                type: 'wms'
            }
		]
	}*/
];

function featureToMarker(feature, latlng) {
	return L.marker(latlng, {
		title: `${feature.properties.codlocal}`,
        icon: L.ExtraMarkers.icon({
            icon: 'fa-school',
            //markerColor: feature.properties.color,
            shape: 'circle',
            prefix: 'fas'
        })
    });
}

var overLayersBasico = [{
        key: 'apagar',
        group: "____________",
        layers: [{
            key: 'apagar',
            name: 'Cerrar',
            icon: '<i class="fas fa-align-justify"></i>',
            layer: L.layerGroup(),
            //active: true,
        }]
    },
    {
        key: 'baseLayer',
        group: "Capas base",
        layers: [

            {
                key: 'proyectos',
                name: 'Proyectos',
                icon: '<i class="fas fa-map-marker"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'Mapas_de_Calor',
                name: 'Termoinfografia',
                icon: '<i class="fas fa-atlas"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'empresas',
                name: 'Empresas',
                icon: '<i class="fas fa-briefcase"></i>',
                layer: L.layerGroup(),
                active: false
            },
            {
                key: 'universidades',
                name: 'Universidades',
                icon: '<i class="fas fa-university"></i>',
                layer: L.layerGroup(),
                active: false
            }
        ]
    }


];
var pieChart = {
    chart: {
        type: 'pie',
        margin: [0, 0, 0, 0]
    },
    credits: {
        enabled: false
    },
    title: {
        text: '',
        align: 'center',
        style: {
            fontSize: '11px',
            fontWeight: 'bold'
        }
    },
    tooltip: {
        pointFormat: '{series.name} : <b>{point.y}</b> ({point.percentage:.1f}%)'
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: []
};
var charPie2 = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            },

        }
    },
    series: []
};
var lineChart = {
    title: {
        text: ''
    },
    tooltip: {
        valuePrefix: "S/. ",
        valueDecimals: 2,
        shared: true
    },
    yAxis: {
        title: {
            text: 'Soles'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 0
        }
    },
    series: [],
    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }
};

var combinateChart = {
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        allowDecimals: false
    },
    yAxis: {
        title: {
            text: 'Soles'
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        valuePrefix: "S/. ",
        valueDecimals: 2,
        shared: true
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 0
        }
    },
    series: []
};

var barChart = {
    /*chart: {
    	type: 'column'
    },*/
    title: {
        text: ''
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: [{
        labels: {
            format: '{value} MM',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: 'Millones de soles',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, {
        title: {
            text: 'Población',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value} K',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    /*tooltip: {
		shared: true,
		useHTML: true,
        formatter: function() {
            var points = '<span style="font-size:10px">' + this.x + '</span><table class="tip">';
            $.each(this.points,function(i,point){
                points+='<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>' + '<td style="text-align: right">' + App.formatNumber(point.y) + '</td></tr>'
            });
            points += '</table>';
            return points;
        }
    },*/
    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {

                }
            }
        },
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: []
};
var multibar = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: [],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Millones de Soles (MS/)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        series: {
            cursor: 'pointer',
            point: {
                events: {

                }
            }
        },
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: []
};
var lineChart = {
    title: {
        text: ''
    },

    subtitle: {
        text: ''
    },

    yAxis: {
        title: {
            text: ''
        }
    },

    xAxis: {
        accessibility: {
            rangeDescription: ''
        }
    },

    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
    },

    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            }
        }
    },

    series: [],

    responsive: {
        rules: [{
            condition: {
                maxWidth: 500
            },
            chartOptions: {
                legend: {
                    layout: 'horizontal',
                    align: 'center',
                    verticalAlign: 'bottom'
                }
            }
        }]
    }

}
var dualChart = {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: [{
        categories: [],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    series: []
}

var stackedBar = {
    chart: {
        type: 'bar'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Millones de soles'
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: []
}
var stackedBarProyectado = {
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: []
    },
    yAxis: {
        min: 0,
        title: {
            text: ''
        }
    },
    legend: {
        reversed: true
    },
    plotOptions: {
        series: {
            stacking: 'normal'
        }
    },
    series: []
}
