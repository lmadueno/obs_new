var App = function() {

    var init = function() {
        iniLoadAlert('Cargando Mapas!', 'El Observatorio de Inversion Publica iniciara en', 3500);
        setTimeout(function() {
            document.body.style.zoom = "80%";
            topoInit();
            initMap();
            counterSum("codfuncionproyecto=160");
            for (var x = 1; x <= 26; x++) {
                loadCompanies("coddpto = '" + padWithZeroes(x, 2) + "'");
            }
            legendMapCovid();
            legendMapSaludSinadef();
            legendPresidenciales();
            $(".legendSalud").css("display", "none");
            $('input[id="75"]').attr('disabled', 'disabled');
            $('input[id="76"]').attr('disabled', 'disabled');
            $('input[id="77"]').attr('disabled', 'disabled');
            $('input[id="78"]').attr('disabled', 'disabled');
            $(".legendPresidencial").css("display", "none");
            $(".legendSaludSinadef").css("display", "none");

            loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
            loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
            loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');

            loadSaludXubigeo('vw_fallecidos_sinadef_departamentos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrRegion, "");
            loadSaludXubigeo('vw_fallecidos_sinadef_provincias_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrProv, '');
            loadSaludXubigeo('vw_fallecidos_sinadef_distritos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrDist, '');

            loadSaludXubigeo('vw_fallecidos_minsa_departamentos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrRegion, "");
            loadSaludXubigeo('vw_fallecidos_minsa_provincias', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrProv, '');
            loadSaludXubigeo('vw_fallecidos_minsa_distritos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrDist, '');
            loadColleges();
            loadConflicts();
            initComponents();
            loadOleoducto();
            loadLineaFerrea();
            loadCorredorMinero();
            loadCorredorLogistico();
            loadHidrografia();
            loadAccidentes();
            loadComisarias("");
        }, 1000);
    }
    var iniLoadAlert = function(title, html, time) {
        let timerInterval
        Swal.fire({
            title: title,
            html: html + ' <b></b> milisegundos.',
            imageUrl: 'assets/app/img/logo.png',
            imageWidth: 400,
            imageHeight: 50,
            timer: time,
            timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {

            if (result.dismiss === Swal.DismissReason.timer) {

            }
        })
    }
    var topoInit = function() {
        L.TopoJSON = L.GeoJSON.extend({
            addData: function(jsonData) {
                if (jsonData.type === "Topology") {
                    for (key in jsonData.objects) {
                        geojson = topojson.feature(jsonData,
                            jsonData.objects[key]);
                        L.GeoJSON.prototype.addData.call(this, geojson);

                    }
                } else {
                    L.GeoJSON.prototype.addData.call(this, jsonData);

                }
            }
        });
    }
    var initMap = function() {
        map = L.map('map', {
                center: [-9.44, -70.74],
                zoom: 6,
                zoomControl: false,
                condensedAttributionControl: false
            },
            topoLayerVias = new L.TopoJSON(),
            topoLayerHidro = new L.TopoJSON(),
            topoLayer1 = new L.TopoJSON(),
            topoLayer = new L.TopoJSON()
        );
        filterMap();
        layersMap();
        counterMap();
        dialogContent();
        zoomContol();
        logoMap();
        switchMap();
        sideBarMenu();
        btnFlotantes();
        logoMapCabecera();
        //drawControlMap();
        //searchBox();
    }
    var searchBox = function() {
        var searchboxControl = createSearchboxControl();
        var control = new searchboxControl({});
        map.addControl(control);
        var html = `<div class="row">
                <div class="col-lg-4">
            
                    <select class="form-select form-control-sm" aria-label="Default select example" id="ddlTipoBusq">
                        <option value="1">Ubigeos</option>
                        <option value="2">Proyectos</option>
                        <option value="3">Empresas</option>
                        <option value="4">Partidos Politicos</option>
                        <option value="5">Direcciones</option>
            
                    </select>
                </div>
                <div class="col-lg-8">
                    <div id="inputSeacrh">  <input type="text" class="form-control form-control-sm" id="txtseacrhubi" placeholder="Busqueda por Región, Provincia o Distrito">   </div>
                </div>
            </div>`;

        $(".searchbox-menu-container").remove();
        $('#selectSearch').html(html);
    }
    var logoMapCabecera = function() {
        var logo = L.control({ position: 'topleft' });
        logo.onAdd = function(map) {
            var img = L.DomUtil.create('img');
            img.id = 'bottom-logo';
            img.src = cdn + 'logo_lateral.png';
            img.style.width = '145px';
            return img;
        };
        logo.addTo(map);
    }

    var drawControlMap = function() {


        map.addLayer(drawnItems);
        var drawControl = window.drawControl = new L.Control.Draw({
            position: 'topleft',
            draw: {

                circle: false,
                circlemarker: false,
                marker: false,
                polyline: false,
                rectangle: true,
                polygon: false,

            },

            edit: {
                featureGroup: drawnItems,
                remove: true,
                edit: false
            }
        });

        map.addControl(drawControl);
        var type;
        var polygon = null;
        map.on('draw:created', function(e) {
            openNewDialog = false;
            clearLayers(clusters.companies);
            clearLayers(clusters.projects);
            document.getElementById("txtTipoFiltro").value = 'drawObjeto';

            if (polygon != null) map.removeLayer(polygon);
            var obj = {};
            type = e.layerType;
            if (e.layerType == 'circle') {
                obj.method = 'draw_circle_ubigeos';
                obj.lat = e.layer._latlng.lat;
                obj.lng = e.layer._latlng.lng;
                obj.radio = e.layer._mRadius;

            } else if (e.layerType == 'rectangle') {
                obj.method = 'draw_rectangle_ubigeos';
                var poligono = ("POLYGON((" + e.layer._latlngs[0][0].lng + e.layer._latlngs[0][0].lat + "," +
                    e.layer._latlngs[0][1].lng + e.layer._latlngs[0][1].lat + "," +
                    e.layer._latlngs[0][2].lng + e.layer._latlngs[0][2].lat + "," +
                    e.layer._latlngs[0][3].lng + e.layer._latlngs[0][3].lat + "," +
                    e.layer._latlngs[0][0].lng + e.layer._latlngs[0][0].lat + "))");
                obj.poligono = poligono;
            } else if (e.layerType == 'polygon') {
                obj.method = 'draw_rectangle_ubigeos';
                var tmp = [];
                e.layer._latlngs[0].forEach(function(row) {
                    tmp.push(row.lng + ' ' + row.lat);
                });

                var poligono = "POLYGON((" + tmp.join(',') + "," + e.layer._latlngs[0][0].lng + e.layer._latlngs[0][0].lat + "))";
                obj.poligono = poligono;

            }
            console.log(obj);
            $.getJSON({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {

                    var dpto = [];
                    var dpto2 = [];
                    var prov = [];
                    var dist = [];
                    for (i = 0; i < response.data.length; i++) {
                        if (response.data[i].ubigeo.length == 2) {
                            dpto.push("'" + response.data[i].ubigeo + "'");
                            dpto2.push(parseInt(response.data[i].ubigeo));
                        } else if (response.data[i].ubigeo.length == 4) {
                            prov.push("'" + response.data[i].ubigeo + "'");
                        } else if (response.data[i].ubigeo.length == 6) {
                            dist.push("'" + response.data[i].ubigeo + "'");
                        }
                    }
                    tmpDrawFilter.region = dpto;
                    tmpDrawFilter.provincia = prov;
                    tmpDrawFilter.distrito = dist;
                    lyrDraw([lyrRegion], "codubigeo in (" + dpto.join(',') + ")");
                    loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo in (" + dpto.join(',') + ")");
                    lyrDraw([lyrProvincia], "codubigeo in (" + prov.join(',') + ")");
                    loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, "codubigeo in (" + prov.join(',') + ")");
                    lyrDraw([lyrDistrito], "codubigeo in (" + dist.join(',') + ")");
                    loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, "codubigeo in (" + dist.join(',') + ")");

                    lyrDraw([lyrRegionMinsa], "ubigeo in (" + dpto.join(',') + ")");
                    lyrDraw([lyrProvinciaMinsa], "ubigeo in (" + prov.join(',') + ")");
                    lyrDraw([lyrDistritoMinsa], "ubigeo in (" + dist.join(',') + ")");
                    lyrDraw([lyrRegionSinadef], "ubigeo in (" + dpto.join(',') + ")");
                    lyrDraw([lyrProvinciaSinadef], "ubigeo in (" + prov.join(',') + ")");
                    lyrDraw([lyrDistritoSinadef], "ubigeo in (" + dist.join(',') + ")");
                    loadSaludXubigeo('vw_fallecidos_sinadef_departamentos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrRegion, "ubigeo in (" + dpto.join(',') + ")");
                    loadSaludXubigeo('vw_fallecidos_sinadef_provincias_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrProv, "ubigeo in (" + prov.join(',') + ")");
                    loadSaludXubigeo('vw_fallecidos_sinadef_distritos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrDist, "ubigeo in (" + dist.join(',') + ")");

                    loadSaludXubigeo('vw_fallecidos_minsa_departamentos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrRegion, "ubigeo in (" + dpto.join(',') + ")");
                    loadSaludXubigeo('vw_fallecidos_minsa_provincias', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrProv, "ubigeo in (" + prov.join(',') + ")");
                    loadSaludXubigeo('vw_fallecidos_minsa_distritos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrDist, "ubigeo in (" + dist.join(',') + ")");
                    obj.method = 'draw_rectangle_projects';
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {

                            var tmp = [];
                            for (i = 0; i < response.data.length; i++) {
                                tmp.push(parseInt(response.data[i].codproyecto));
                            }
                            loadProjects('codproyecto in (' + tmp.join(',') + ') ');
                            drawCodproyect = tmp;
                            counterSum('codproyecto in (' + tmp.join(',') + ')');
                            console.log('codproyecto in (' + tmp.join(',') + ') ');
                        }
                    });
                    filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                    filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                    obj.method = 'draw_area_influencia_comisarias1';
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var tmp = [];
                            for (i = 0; i < response.data.length; i++) {
                                tmp.push(response.data[i].cod_pnp);
                            }
                            loadComisarias("cod_pnp in (" + tmp.join(',') + ")");
                        }
                    });
                    obj.method = 'draw_area_influencia_comisarias2';
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            console.log(response);
                            var tmp = [];
                            for (i = 0; i < response.data.length; i++) {
                                tmp.push(response.data[i].rucempresa);
                            }

                            loadCompanies('ruc in (' + tmp.join(',') + ') ');
                        }
                    });
                    obj.method = 'draw_area_influencia_comisarias3';
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            console.log(response);
                            var tmp = [];
                            for (i = 0; i < response.data.length; i++) {
                                tmp.push(response.data[i].cu);
                            }
                            $('input[data-id="hospitales"]').removeAttr('disabled');
                            loadHospitales('cu in (' + tmp.join(',') + ') ');
                            console.log('cu in (' + tmp.join(',') + ') ');
                        }
                    });




                }
            });

            polygon = e.layer;
            drawnItems.addLayer(polygon);
        });

        drawnItems.on('click', function(e) {
            var consulta = openNewDialog == true ? filter('projects') : '';

            if (type == "circle") {
                var obj = {
                    method: 'draw_circle',
                    lat: e.layer._latlng.lat,
                    lng: e.layer._latlng.lng,
                    radio: e.layer._mRadius,
                    consulta: consulta
                }
            } else {
                var tmp = [];
                e.layer._latlngs[0].forEach(function(row) {
                    tmp.push(row.lng + ' ' + row.lat);
                });

                var obj = {
                    method: 'draw_rectangle',
                    poligono: "POLYGON((" + tmp.join(',') + "," + e.layer._latlngs[0][0].lng + e.layer._latlngs[0][0].lat + "))",
                    consulta: consulta
                }

            }
            $.post({
                url: 'popupDrawUbigeos.php?obj=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {

                    dialog13.setContent(response);
                    dialog13.setLocation([95, 50]);
                    map10 = L.map('map10').setView([-9.44, -70.74], 7);
                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                    }).addTo(map10);

                    loadProjectsDraw('iddist in (' + tmpDrawFilter.distrito.join(',') + ') and ' + filter('projects'));
                    if (type == "circle") {
                        L.circle(L.latLng(obj.lat, obj.lng, 5), obj.radio, {
                            opacity: 0.5,
                            weight: 1,
                            fillOpacity: 0.4
                        }).addTo(map10);
                        obj.method = openNewDialog == true ? 'draw_circle_wordCloud_new' : 'draw_circle_wordCloud';

                        var objTmp = {
                            lat: obj.lat,
                            lng: obj.lng,
                            radio: obj.radio
                        }
                        objCircle = objTmp;
                    } else {
                        var tmp = [];
                        var tmpLat = [];
                        var tmpLng = [];
                        e.layer._latlngs[0].forEach(function(row) {
                            tmp.push([row.lat, row.lng]);
                            tmpLat.push(row.lat);
                            tmpLng.push(row.lng);
                        });
                        L.polygon(tmp).addTo(map10);

                        obj.method = openNewDialog == true ? 'draw_rectangle_wordCloud_new' : 'draw_rectangle_wordCloud';
                        var objTmp = {
                            lats: tmpLat.join(','),
                            lngs: tmpLng.join(',')
                        }
                        objRectangle = objTmp;
                    }
                    console.log(obj);
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            var tmp = [];
                            for (i = 0; i < response1.data.length; i++) {

                                tmp.push({ name: response1.data[i].funcion, weight: parseInt(response1.data[i].can) });
                            }
                            var obj1 = {
                                div: 'chartDrawUbigeos',
                                data: tmp
                            }
                            var titulo = openNewDialog == true ? 'Division Funcional' : 'Función';

                            Highcharts.chart(obj1.div, {
                                plotOptions: {
                                    series: {
                                        turboThreshold: 5000 // Comment out this code to display error
                                    }
                                },
                                series: [{
                                    rotation: {
                                        from: 0,
                                        to: 0,
                                        orientations: 5
                                    },
                                    type: 'wordcloud',
                                    data: obj1.data,
                                    name: 'wdsfg'
                                }],
                                title: {
                                    text: 'Listado de Proyectos de Inversion por ' + titulo
                                },
                                tooltip: {
                                    pointFormatter: function() {
                                        var point = this;
                                        return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                                    }
                                }
                            });
                        }
                    });
                }
            });



        });
    }
    var layersMap = function() {
        var lyrPoblacion = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupPoblacion',
            layers: 'colaboraccion_2020:vw_poblacion_censo130920191',
            tiled: true,
            format: 'image/png',
            opacity: 0.7,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'poblacion').layer.addLayer(lyrPoblacion);
        var lyrAeropuertos = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupAeropuertos',
            layers: 'colaboraccion_2020:gen_aeropuertos',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'aeropuertos').layer.addLayer(lyrAeropuertos);
        var lyrPuertos = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupPuertos',
            layers: 'colaboraccion_2020:gen_puertosmaritimos',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'puertos').layer.addLayer(lyrPuertos);
        var lyrPobreza = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupPobreza',
            layers: 'colaboraccion_2020:vw_ubigeo_nbi',
            tiled: true,
            format: 'image/png',
            opacity: 0.7,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'pobreza').layer.addLayer(lyrPobreza);
        lyrLimite = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_img_limitedistrital',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
        });
        filterGroup('overLayers', 'Limite', 'limit').layer.addLayer(lyrLimite);
        lyrLimiteDep = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_img_limite_dep',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'Limite', 'limit').layer.addLayer(lyrLimiteDep);
        lyrreinfo = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_reinfo',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseMineria', 'reinfo').layer.addLayer(lyrreinfo);
        lyrpasivoambiental = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_pasivoambiental',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseMineria', 'pasivoambiental').layer.addLayer(lyrpasivoambiental);
        lyrproyectosoperacionminera = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_proyectosoperacionminera',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseMineria', 'proyectosmineros').layer.addLayer(lyrproyectosoperacionminera);
        lyrcartera = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_carteradeproyectos',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseMineria', 'carteraproyectos').layer.addLayer(lyrcartera);
        lyrareaminera = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_operacionminera',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseMineria', 'areaminera').layer.addLayer(lyrareaminera);
        lyrRegionPolitica = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_politica_region_2sd',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'basePolitica', 'regionPolitica').layer.addLayer(lyrRegionPolitica);
        lyrProvPolitica = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_politica_provincia_2sd',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'basePolitica', 'provinciaPolitica').layer.addLayer(lyrProvPolitica);
        lyrDistPolitica = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_politica_distrito_2sd',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'basePolitica', 'distritoPolitica').layer.addLayer(lyrDistPolitica);

        lyrRegionMinsa = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_minsa_departamentos',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseMinsa', 'region_minsa').layer.addLayer(lyrRegionMinsa);
        lyrProvinciaMinsa = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_minsa_provincias',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseMinsa', 'provincia_minsa').layer.addLayer(lyrProvinciaMinsa);
        lyrDistritoMinsa = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_minsa_distritos',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseMinsa', 'distrito_minsa').layer.addLayer(lyrDistritoMinsa);
        lyrRegionSinadef = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_departamentos',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseSinadef', 'region_sinadef').layer.addLayer(lyrRegionSinadef);
        lyrProvinciaSinadef = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_provincias',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseSinadef', 'provincia_sinadef').layer.addLayer(lyrProvinciaSinadef);
        lyrDistritoSinadef = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_distritos',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseSinadef', 'distrito_sinadef').layer.addLayer(lyrDistritoSinadef);
        lyrRegion = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_dpto_geojson',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseUbigeo', 'region').layer.addLayer(lyrRegion);
        lyrProvincia = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:vw_provincia_geojson',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseUbigeo', 'provincia').layer.addLayer(lyrProvincia);
        lyrDistrito = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupFicha',
            layers: 'colaboraccion_2020:vw_distrito_geojson',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseUbigeo', 'distrito').layer.addLayer(lyrDistrito);
        var lyrAccidentes = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_tramosaccidentes',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'accidentes').layer.addLayer(lyrAccidentes);
        lyrHidrovias = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:prueba_capas',
            tiled: true,
            format: 'image/png',
            minZoom: 0,
            continuousWorld: true,
            transparent: true,
            opacity: 0.85
        });
        filterGroup('overLayers', 'baseExtras', 'corrhidrovias').layer.addLayer(lyrHidrovias);

        var lyrVias = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupVias',
            layers: 'colaboraccion_2020:gen_vias',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'vias').layer.addLayer(lyrVias);
        var lyrFerreas = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_lineaferrea',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'ferreas').layer.addLayer(lyrFerreas);
        var lyrOleoducto = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_oleoducto',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseMineria', 'oleoducto').layer.addLayer(lyrOleoducto);
        var lyrCorrPrincipales = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_corredoresprincipales',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'corrprincipales').layer.addLayer(lyrCorrPrincipales);
        var lyrCorrSecundarios = L.tileLayer.betterWms(wmsrootUrl, {
            layers: 'colaboraccion_2020:gen_corredoresalimentadores',
            tiled: true,
            format: 'image/png',
            opacity: 1,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseExtras', 'corrsecundarios').layer.addLayer(lyrCorrSecundarios);
        var lyrCorredorMinero = L.tileLayer.betterWms(wmsrootUrl, {
            id: 'popupPobreza',
            layers: 'colaboraccion_2020:gen_corredor',
            tiled: true,
            format: 'image/png',
            opacity: 0.7,
            minZoom: 0,
            continuousWorld: true,
            transparent: true
        });
        filterGroup('overLayers', 'baseMineria', 'corredor').layer.addLayer(lyrCorredorMinero);

    }
    var counterMap = function() {
        var logo = L.control({ position: 'topcenter' });
        logo.onAdd = function(map) {
            var div = L.DomUtil.create('div');
            div.innerHTML = '<div class="counterSum" role="alert" style="padding: 0px;width: 500px;">-</div>';
            return div;
        };
        logo.addTo(map);
    }
    var zoomContol = function() {
        var zomm = L.control.zoom({
            position: 'bottomright'
        });
        zomm.addTo(map)
    }
    var logoMap = function() {
        var logo = L.control({ position: 'bottomcenter' });
        logo.onAdd = function(map) {
            var img = L.DomUtil.create('img');
            img.id = 'bottom-logo';
            img.src = cdn + 'logo.png';
            img.style.width = '125px';
            return img;
        };
        logo.addTo(map);
    }
    var btnFlotantes = function() {
        L.easyButton({
            id: 'btnMain',
            position: 'topright',
            leafletClasses: true,
            states: [{

                onClick: function() {
                    const $el = $('.leaflet-panel-layers-list');
                    const $el1 = $('.leaflet-control-layers-scrollbar');
                    $el1.removeClass("mostrar1");
                    $el.removeClass("mostrar1");
                    $el.addClass('mostrar');
                    $el1.addClass('mostrar');

                    $('input[ID="36"]').prop("checked", true);
                    var zoom = $('.leaflet-bottom.leaflet-right');
                    zoom.removeClass("zoomstyleremove");
                    zoom.addClass("zoomstyle");

                },
                title: 'Menú informativo',
                icon: '<span><i class="fa fa-align-justify" style="color: black"></i></span>'
            }]
        }).addTo(map);
    }

    function addTopoData1(topoData) {
        topoLayer1.addData(topoData);
        topoLayer1.addTo(clusters.pobreza.lyrGroup);
        topoLayer1.eachLayer(handleLayerPobreza);
    }

    function handleLayerPobreza(layer) {

        layer.bindPopup(layer.feature.properties.popupContent, { closeButton: false, minWidth: 20, minWidth: 300 });
        layer.setStyle({
            fillColor: '#00000000',
            fillOpacity: 0.65,
            color: '#00000000',
            weight: 0.25,
            opacity: 0.9
        });
        layer.on('click', function(e) {
            var objCoordenadas = {
                method: 'popUpLatitudLongitud',
                lng: e.latlng.lng,
                lat: e.latlng.lat
            };

            $.get({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objCoordenadas)),
                success: function(response) {
                    var result = JSON.parse(response);
                    var id = result.data[0].codubigeo;
                    var obj = {
                        method: 'infociprlDistrito',
                        ubigeo: id,
                        nivel: 'distrito'
                    }
                    $.get({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var result = JSON.parse(response);
                            var envio = result.data[0];
                            $.get({
                                url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                success: function(response) {
                                    envio.method = 'ciprlXregion';
                                    dialog.setContent(response);
                                    setTimeout(function() {
                                        $('.datatable').dataTable({
                                            "scrollY": "100px",
                                            "scrollCollapse": true,
                                            searching: false,
                                            paging: false,
                                            info: false,
                                            ordering: false,
                                            destroy: true
                                        });
                                    }, 500);

                                    if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                            success: function(response) {
                                                if (Object.keys(response).length > 0) {
                                                    var chart = window['combinateChart'];
                                                    chart.series = response.data.series;
                                                    chart.plotOptions.series.pointStart = response.data.pointStart;

                                                    Highcharts.chart('chart_' + envio.codubigeo, chart);
                                                }
                                            }
                                        });
                                    }
                                    chartPie({ 'container': 'pieDistrito', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 4, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    chartPie({ 'container': 'pieProvincia', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 6, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    chartPie({ 'container': 'pieRegion', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 2, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    var obj1 = {
                                        idnivel: document.getElementById('codnivelPIP').value,
                                        codubigeo: document.getElementById('codUbigeoPIP').value,
                                        idnivGob: 03
                                    };
                                    $.post({
                                        url: 'divpipProjects.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            document.getElementById('divpipRegiones').innerHTML = response;
                                        }
                                    });
                                }
                            });

                        }
                    });
                }
            });
        });
        layer.on('mouseover', function(e) {
            var objCoordenadas = {
                method: 'popUpLatitudLongitud',
                lng: e.latlng.lng,
                lat: e.latlng.lat
            };
            $.get({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objCoordenadas)),
                success: function(response) {
                    var result = JSON.parse(response);
                    var obj = {
                        ubigeo: result.data[0].iddist
                    }
                    $.get({
                        url: 'popupPobreza.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            layer._popup.setContent(response);
                        }
                    });

                }
            });

            layer.openPopup();
        });
        layer.on('mouseout', function() { layer.closePopup(); });
        layer.on({
            mouseover: enterLayer,
            mouseout: leaveLayer
        });
    }

    function enterLayer() {
        this.bringToFront();
        this.setStyle({
            weight: 3,
            color: '#4040ff',
            dashArray: '',
            fillOpacity: 0.4
        });

    }

    function leaveLayer() {
        this.bringToBack();
        this.setStyle({
            fillOpacity: 0.65,
            color: '#990000',
            weight: 0.25,
            opacity: 0.4
        });
    }

    function addTopoData(topoData) {

        topoLayer.addData(topoData);
        topoLayer.addTo(clusters.poblacion.lyrGroup);
        topoLayer.eachLayer(handleLayer);

    }

    function handleLayer(layer) {
        layer.bindPopup(layer.feature.properties.popupContent, { closeButton: false, minWidth: 20, minWidth: 200 });
        layer.setStyle({
            fillColor: '#00000000',
            fillOpacity: 0.65,
            color: '#00000000',
            weight: 0.25,
            opacity: 0.9
        });
        layer.on('click', function(e) {

            var objCoordenadas = {
                method: 'popUpLatitudLongitud',
                lng: e.latlng.lng,
                lat: e.latlng.lat
            };

            $.get({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objCoordenadas)),
                success: function(response) {
                    var result = JSON.parse(response);
                    var id = result.data[0].iddist;
                    var obj = {
                        method: 'infociprlDistrito',
                        ubigeo: id,
                        nivel: 'distrito'
                    }
                    $.get({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var result = JSON.parse(response);
                            var envio = result.data[0];
                            $.get({
                                url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                success: function(response) {
                                    envio.method = 'ciprlXregion';
                                    dialog.setContent(response);
                                    setTimeout(function() {
                                        $('.datatable').dataTable({
                                            "scrollY": "100px",
                                            "scrollCollapse": true,
                                            searching: false,
                                            paging: false,
                                            info: false,
                                            ordering: false,
                                            destroy: true
                                        });
                                    }, 500);

                                    if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                            success: function(response) {
                                                if (Object.keys(response).length > 0) {
                                                    var chart = window['combinateChart'];
                                                    chart.series = response.data.series;
                                                    chart.plotOptions.series.pointStart = response.data.pointStart;

                                                    Highcharts.chart('chart_' + envio.codubigeo, chart);
                                                }
                                            }
                                        });
                                    }
                                    chartPie({ 'container': 'pieDistrito', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 4, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    chartPie({ 'container': 'pieProvincia', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 6, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    chartPie({ 'container': 'pieRegion', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 2, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    var obj1 = {
                                        idnivel: document.getElementById('codnivelPIP').value,
                                        codubigeo: document.getElementById('codUbigeoPIP').value,
                                        idnivGob: 03
                                    };
                                    $.post({
                                        url: 'divpipProjects.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            document.getElementById('divpipRegiones').innerHTML = response;
                                        }
                                    });
                                }
                            });

                        }
                    });
                }
            });
        });
        layer.on('mouseover', function(e) {
            var objCoordenadas = {
                method: 'popUpLatitudLongitud',
                lng: e.latlng.lng,
                lat: e.latlng.lat
            };
            $.get({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objCoordenadas)),
                success: function(response) {
                    var result = JSON.parse(response);
                    var obj = {
                        ubigeo: result.data[0].iddist

                    }
                    $.get({
                        url: 'popupPoblacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            layer._popup.setContent(response);
                        }
                    });

                }
            });

            layer.openPopup();
        });
        layer.on('mouseout', function() { layer.closePopup(); });
        layer.on({
            mouseover: enterLayer,
            mouseout: leaveLayer
        });
    }

    function addTopoData(topoData) {

        topoLayer.addData(topoData);
        topoLayer.addTo(clusters.poblacion.lyrGroup);
        topoLayer.eachLayer(handleLayer);

    }

    var filterMap = function() {
        var layer;
        var session = document.getElementById("txtidRol").value;
        if (session == 1 || session == 3) {
            layer = overLayers;
        } else {
            layer = overLayersBasico;
        }
        var panelLayers = new L.Control.PanelLayers({}, layer, {
            collapsibleGroups: true,
            collapsed: false
        });
        panelLayers.on('panel:selected', function(feature) {
            switch (feature.key) {
                case 'poblacion':
                    clusters.poblacion.lyrGroup.addTo(map);
                    $.getJSON('assets/app/json/distritoJH.json').done(addTopoData);
                    break;
                case 'pobreza':
                    clusters.pobreza.lyrGroup.addTo(map);
                    $.getJSON('assets/app/json/vw_ciprl_x_distrito_newPolygon.json').done(addTopoData1);
                    break;
                case 'region':
                    var oleoductoChk = $('input[id="42"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Region', 1300);
                    }

                    setTimeout(function() {
                        map.removeLayer(overLayers[2].layers[0].lyrProv);
                        map.removeLayer(overLayers[2].layers[0].lyrDist);
                    }, 2000);
                    getLyrDrawUbigeos();
                    break;
                case 'provincia':
                    var oleoductoChk = $('input[id="42"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Provincia', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[2].layers[0].lyrRegion);
                        map.removeLayer(overLayers[2].layers[0].lyrDist);
                    }, 2000);
                    getLyrDrawUbigeos();
                    break;
                case 'distrito':
                    var oleoductoChk = $('input[id="42"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Distrito', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[2].layers[0].lyrRegion);
                        map.removeLayer(overLayers[2].layers[0].lyrProv);
                    }, 2000);
                    getLyrDrawUbigeos();
                    break;
                case 'region_minsa':
                    var oleoductoChk = $('input[id="55"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Region', 1300);
                    }
                    // getProyectsCapaCiprl(1);
                    setTimeout(function() {
                        map.removeLayer(overLayers[5].layers[0].lyrProv);
                        map.removeLayer(overLayers[5].layers[0].lyrDist);
                    }, 2000);

                    break;
                case 'provincia_minsa':
                    var oleoductoChk = $('input[id="55"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Provincia', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[5].layers[0].lyrRegion);
                        map.removeLayer(overLayers[5].layers[0].lyrDist);
                    }, 2000);
                    //getProyectsCapaCiprl(2);
                    break;
                case 'distrito_minsa':
                    var oleoductoChk = $('input[id="55"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Distrito', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[5].layers[0].lyrRegion);
                        map.removeLayer(overLayers[5].layers[0].lyrProv);
                    }, 2000);

                    break;
                case 'region_sinadef':
                    var oleoductoChk = $('input[id="59"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Region', 1300);
                    }
                    // getProyectsCapaCiprl(1);
                    setTimeout(function() {
                        map.removeLayer(overLayers[6].layers[0].lyrProv);
                        map.removeLayer(overLayers[6].layers[0].lyrDist);
                    }, 2000);

                    break;
                case 'provincia_sinadef':
                    var oleoductoChk = $('input[id="59"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Provincia', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[6].layers[0].lyrRegion);
                        map.removeLayer(overLayers[6].layers[0].lyrDist);
                    }, 2000);
                    //getProyectsCapaCiprl(2);
                    break;
                case 'distrito_sinadef':
                    var oleoductoChk = $('input[id="59"]:checked').length;
                    if (oleoductoChk > 0) {
                        iniLoadAlert('Cargando Informacion!', 'Cambiando Capa Distrito', 1300);
                    }
                    setTimeout(function() {
                        map.removeLayer(overLayers[6].layers[0].lyrRegion);
                        map.removeLayer(overLayers[6].layers[0].lyrProv);
                    }, 2000);
                    //getProyectsCapaCiprl(3);
                    break;
                case 'Mapas_de_Calor':
                    if (document.getElementById("txttermoInfografia").value == 2) {
                        toastr.success("Cargando Mapa de Calor", "COVID-19", { timeOut: 9000, closeButton: true, progressBar: true });
                        map.removeLayer(overLayers[5].layers[document.getElementById("txttermoInfoUbigeo").value].layer);
                        mapaCalorSalud("gen_fallecidos_minsa_info");
                    } else if (document.getElementById("txttermoInfografia").value == 3) {
                        map.removeLayer(overLayers[6].layers[document.getElementById("txttermoInfoUbigeo").value].layer);
                        toastr.success("Cargando Mapa de Calor", "SINADEF", { timeOut: 9000, closeButton: true, progressBar: true });
                        mapaCalorSalud("gen_fallecidos_sinadef_info");
                    }
                    clusters.Mapas_de_Calor.lyrGroup.addTo(map);
                    break;
            }
        });

        panelLayers.on('panel:unselected', function(feature) {
            switch (feature.key) {
                case 'poblacion':
                    map.removeLayer(clusters.poblacion.lyrGroup);
                    break;
                case 'pobreza':
                    map.removeLayer(clusters.pobreza.lyrGroup);
                    break;
                case 'apagar':
                    const $el = $('.leaflet-panel-layers-list');
                    const $el1 = $('.leaflet-control-layers-scrollbar');
                    $el1.removeClass("mostrar");
                    $el.removeClass("mostrar");
                    $el.addClass('mostrar1');
                    $el1.addClass('mostrar1');
                    var zoom = $('.leaflet-bottom.leaflet-right');
                    zoom.removeClass("zoomstyle");
                    zoom.addClass("zoomstyleremove");
                    break;
                case 'Mapas_de_Calor':
                    if (document.getElementById("txttermoInfografia").value == 2) {
                        map.addLayer(overLayers[5].layers[document.getElementById("txttermoInfoUbigeo").value].layer);
                        $('#COVID-19').prop("checked", true);
                    } else if (document.getElementById("txttermoInfografia").value == 3) {
                        map.addLayer(overLayers[6].layers[document.getElementById("txttermoInfoUbigeo").value].layer);
                        $('#SINADEF').prop("checked", true);
                    }
                    map.removeLayer(clusters.Mapas_de_Calor.lyrGroup);
                    break;
                case 'oleoducto':
                    tmpOleoducto = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {

                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");

                    }

                    break;
                case 'ferreas':
                    tmpLineas = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {

                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");
                    }

                    break;
                case 'corrprincipales':
                    tmpVias = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {
                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");
                    }

                    break;
                case 'accidentes':
                    tmpAccidentes = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {
                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");
                    }

                    break;
                case 'corrhidrovias':
                    tmpHidrografias = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {
                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");
                    }

                    break;
                case 'corredor':
                    tmpMineros = [];
                    if (tmpMineros.length != 0 || tmpOleoducto.length != 0 || tmpAccidentes.length != 0 || tmpHidrografias.length != 0 || tmpVias.length != 0 || tmpLineas.length != 0) {
                        getLyrDrawUbigeos();
                    } else {

                        lyrDraw([lyrRegion], "1=1");
                        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo <> 26");
                        lyrDraw([lyrProvincia], "1=1");
                        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, '');
                        lyrDraw([lyrDistrito], "1=1");
                        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, '');
                        counterSum("codfuncionproyecto=160");
                        clearLayers(clusters.projects);
                        loadProjects("codfuncionproyecto=160");
                    }

                    break;

            }
        });

        map.addControl(panelLayers);
    }
    var mapaCalorSalud = function(layer) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: layer,
                        sql: ''
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var tmp = [];
                var cont = 0;
                $.each(data.features, function(i, item) {
                    tmp.push([Number(item.geometry.coordinates[1]), Number(item.geometry.coordinates[0]), item.properties.quantil]);
                    cont++;
                });

                L.heatLayer(tmp, {
                    minOpacity: 0.8,
                    /*,
                                        gradient: {
                                            1: "rgb(3,126,52)",
                                            0.6: "rgb(236,252,37)",
                                            0.8: "rgb(162,224,122)", 
                                            0.4: "rgb(252,138,37)", 
                                            0.2: "rgb(231,62,62)"
                                        }*/
                }).addTo(clusters.Mapas_de_Calor.lyrGroup);
            }
        });
    }
    var switchMap = function() {
        var control = L.control.iconLayers(layers).addTo(map);

        control.on('activelayerchange', function(e) {

            if (e.layer.options.mapID == 'satellite') {
                document.getElementById("bottom-logo").src = cdn + "logo_bn.png";
                si = "muestra";
            } else {
                document.getElementById("bottom-logo").src = cdn + "logo.png";
                si = "";

            }

            if (si == "muestra") {
                map.addLayer(overLayers[1].layers[0].layer);
            } else {
                map.removeLayer(overLayers[1].layers[0].layer);
            }
        });

    }
    var dialogContent = function() {
        if (screen.width < 1224) {
            dialog = L.control.dialog({
                size: [modal_lg.height() * 0.80, screen.height * 0.82],
                location: [10, 10],
                anchor: [10, screen.width * 0.55],
                maxSize: [screen.width, screen.height],
                initOpen: false
            }).addTo(map);
        } else {
            dialog = L.control.dialog({
                size: [modal_lg.height() * 0.80, 550],
                maxSize: [screen.width, screen.height],
                location: [10, 10],
                initOpen: false,
            }).addTo(map);
        }
        dialog.hideResize();
        if (screen.width < 1224) {
            dialog2 = L.control.dialog({
                size: [modal_lg.height() * 0.80, screen.height * 0.82],
                location: [10, 10],
                anchor: [10, screen.width * 0.55],
                initOpen: false
            }).addTo(map);
        } else {
            dialog2 = L.control.dialog({
                size: [500, 480],
                location: [10, 10],
                maxSize: [screen.width, screen.height],
                initOpen: false,
            }).addTo(map);
        }
        dialog2.hideResize();
        if (screen.width < 1224) {
            dialog3 = L.control.dialog({
                size: [modal_lg.height() * 0.85, screen.height * 0.82],
                anchor: [20, screen.width * 0.55],
                maxSize: [screen.width, screen.height],
                initOpen: false
            }).addTo(map);
        } else {
            dialog3 = L.control.dialog({
                size: [modal_lg.height() * 0.80, 750],
                maxSize: [screen.width, screen.height],
                location: [10, 10],
                initOpen: false,
            }).addTo(map);
        }
        dialog3.hideResize();
        dialog4 = L.control.dialog({
            size: [modal_lg.height() * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [screen.width, screen.height],
            initOpen: false
        }).addTo(map);

        dialog4.hideResize();
        dialog5 = L.control.dialog({
            size: [modal_lg.height() * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [screen.width, screen.height],
            initOpen: false
        }).addTo(map);

        dialog5.hideResize();
        dialog6 = L.control.dialog({
            size: [modal_lg.height() * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [screen.width, screen.height],
            initOpen: false
        }).addTo(map);

        dialog6.hideResize();
        if (screen.width < 1224) {
            dialog7 = L.control.dialog({
                size: [720, 550],
                location: [10, 10],
                anchor: [10, screen.width * 0.55],
                maxSize: [screen.width, screen.height],
                initOpen: false
            }).addTo(map);
        } else {
            dialog7 = L.control.dialog({
                size: [screen.width * 0.85, screen.height * 0.82],
                maxSize: [screen.width, screen.height],
                location: [10, 10],
                initOpen: false,
            }).addTo(map);
        }
        dialog7.hideResize();
        dialog8 = L.control.dialog({
            size: [screen.width * 0.85, screen.height * 0.82],
            anchor: [4000, 4000],
            maxSize: [4000, 4000],
            initOpen: false
        }).addTo(map);

        dialog8.hideResize();
        dialog9 = L.control.dialog({
            size: [screen.width * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [4000, 4000],
            initOpen: false
        }).addTo(map);

        dialog9.hideResize();
        dialog10 = L.control.dialog({
            size: [600, 580],
            location: [10, 10],
            maxSize: [screen.width, screen.height],
            initOpen: false,
        }).addTo(map);
        dialog10.hideResize();
        dialog11 = L.control.dialog({
            size: [screen.width * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [4000, 4000],
            initOpen: false
        }).addTo(map);

        dialog11.hideResize();
        dialog12 = L.control.dialog({
            size: [screen.width * 0.85, screen.height * 0.82],
            anchor: [20, screen.width * 0.55],
            maxSize: [4000, 4000],
            initOpen: false
        }).addTo(map);

        dialog12.hideResize();
        if (screen.width < 1224) {
            dialog13 = L.control.dialog({
                size: [720, 550],
                location: [10, 10],
                anchor: [10, screen.width * 0.55],
                maxSize: [screen.width, screen.height],
                initOpen: false
            }).addTo(map);
        } else {
            dialog13 = L.control.dialog({
                size: [screen.width * 0.85, screen.height * 0.82],
                maxSize: [screen.width, screen.height],
                location: [10, 10],
                initOpen: false,
            }).addTo(map);
        }
        dialog13.hideResize();
    }
    var legendMap1 = function() {
        var legend = L.control({ position: "bottomleft" });
        legend.onAdd = function(map) {
            var div = L.DomUtil.create("div", "legend");
            div.innerHTML += '<div class="card" style="width: 9.4rem;"><div class="row"><div class="col-2"><button class="btn-hove" style="padding-top:0.3rem;padding-left:0.2rem;" data-toggle="collapse" data-target="#collapseExample1" aria-expanded="false" aria-controls="collapseExample1"><i class="fa fa-align-justify"></i></button></div><div class="col"><p style="padding-left:1.6rem !important;padding-top:0.1rem !important">Leyenda</p></div></div></div>';
            div.innerHTML += '<div class="row" style="padding-left:0.5rem"><div class="col-sm-2"><div style="background-color:#ffc107; width: 15px; height: 15px;"></div></div><div class="col-8"><span style="font-size:12px">P. Adjudicados</span></div></div><div class="row" style="padding-left:0.5rem"><div class="col-2"><div style="background-color: #007bff; width: 15px; height: 15px;"></div></div><div class="col-8"><span style="font-size:12px">P. Concluidos</span></div></div>';
            return div;
        };
        legend.addTo(map2);

    }
    var legendMapCovid = function() {
        var legend = L.control({ position: "bottomleft" });
        legend.onAdd = function(map) {
            var div = L.DomUtil.create("div", "legendSalud");
            div.innerHTML += "<h4>Leyenda COVID-19</h4>";
            div.innerHTML += '<i style="background: #037E34"></i><span>Muy Bajo</span><br>';
            div.innerHTML += '<i style="background: #A2E07A"></i><span>Bajo</span><br>';
            div.innerHTML += '<i style="background: #ECFC25"></i><span>Medio</span><br>';
            div.innerHTML += '<i style="background: #FC8A25"></i><span>Alto</span><br>';
            div.innerHTML += '<i style="background: #E73E3E"></i><span>Muy Alto</span><br>';
            div.innerHTML += '<span  style="padding-left: 1.5em;">Fallecidos 2020</span><br>';
            return div;
        };
        legend.addTo(map);

    }
    var legendPresidenciales = function() {
        var legend = L.control({ position: "bottomleft" });
        legend.onAdd = function(map) {
            var div = L.DomUtil.create("div", "legendPresidencial");
            div.innerHTML += "<h4>Leyenda Presidenciales</h4>";
            div.innerHTML += '<i style="background: #e66b00"></i><span>Fuerza Popular</span><br>';
            div.innerHTML += '<i style="background: #f81f80"></i><span>Peruanos por el kambio</span><br>';
            div.innerHTML += '<i style="background: #cd0101"></i><span>Frente Amplio</span><br>';
            div.innerHTML += '<i style="background: #13100e"></i><span>Accion Popular</span><br>';
            div.innerHTML += '<i style="background: #e63135"></i><span>Alianza Popular</span><br>';
            return div;
        };
        legend.addTo(map);

    }
    var legendMapSaludSinadef = function() {
        var legend = L.control({ position: "bottomleft" });
        legend.onAdd = function(map) {
            var div = L.DomUtil.create("div", "legendSaludSinadef");
            div.innerHTML += "<h4>Leyenda SINADEF</h4>";
            div.innerHTML += '<i style="background: #037E34"></i><span>Muy Bajo</span><br>';
            div.innerHTML += '<i style="background: #A2E07A"></i><span>Bajo</span><br>';
            div.innerHTML += '<i style="background: #ECFC25"></i><span>Medio</span><br>';
            div.innerHTML += '<i style="background: #FC8A25"></i><span>Alto</span><br>';
            div.innerHTML += '<i style="background: #E73E3E"></i><span>Muy Alto</span><br>';
            div.innerHTML += '<span>Diferencia fallecidos (2020-2019)</span><br>';

            return div;
        };
        legend.addTo(map);

    }
    var initComponentsProyects = function() {
        $('.selectpicker').selectpicker({});
        $('#ddlRegion3,#ddlProvincia2,#ddlPartidosPoliticos').on('changed.bs.select', function(e, idx, isSelected, previousValue) {
            if (idx != undefined) {
                var selected = false;
                var key = $(this).attr('data-key');
                var arr = filterArr('filterParams', 'key', key);
                var val = format(arr.type, this.options[idx].value);
                var idxArr = 0;
                if (isSelected) {
                    arr.selected.push(val);
                } else {
                    idxArr = arr.selected.indexOf(val);
                    filterArr('filterParams', 'key', key)['selected'].splice(idxArr, 1);
                }
                if (key == 'ddlRegion3') ddlProvincia1();
            }
        });
        $("#ddlProvincia,#ddlPrograma,#ddlSubPrograma").prop("disabled", true);
        $('#ddlRegion, #ddlProvincia, #ddlFases, #ddlNivel,#ddlFuncion,#ddlEstadoOxi,#ddlPrograma,#ddlSubPrograma, #ddlTamano2, #ddlGrupo2, #ddlSector2, #ddlRegion2').on('changed.bs.select', function(e, idx, isSelected, previousValue) {
            if (idx != undefined) {
                var selected = false;
                var key = $(this).attr('data-key');
                var arr = filterArr('filterParams', 'key', key);
                var val = format(arr.type, this.options[idx].value);
                var idxArr = 0;
                if (isSelected) {
                    arr.selected.push(val);
                } else {
                    idxArr = arr.selected.indexOf(val);
                    filterArr('filterParams', 'key', key)['selected'].splice(idxArr, 1);
                }
                if (key == 'ddlRegion') {
                    $("#ddlProvincia").prop("disabled", false);
                    ddlProvincia();
                }
                if (key == 'ddlFuncion') {
                    $("#ddlPrograma").prop("disabled", false);
                    ddlPrograma();
                }
                if (key == 'ddlPrograma') {
                    $("#ddlSubPrograma").prop("disabled", false);
                    ddlSubPrograma();
                }
                $('.selectpicker').selectpicker('refresh');
            }
        });
        $('#txtRazonSocial').autocomplete({
            minLength: 3,
            source: "php/app.php",
            html: true,
            open: function(event, u) {
                $('.ui-autocomplete').css('z-index', 10000);
                $('.ui-autocomplete').css('font', '10px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif');
                $('.ui-autocomplete').css('max-height', '200px');
                $('.ui-autocomplete').css('overflow-y', 'auto');
                $('.ui-autocomplete').css('overflow-x', 'hidden');
                $('.ui-autocomplete').css('padding-right', '10px');
            }
        });
        $(".bs-select-all").on('click', function() {
            var key = $(this).closest('.dropdown').find('select').attr('data-key');

            var obj = filterArr('filterParams', 'key', key);
            if (obj.data.length == 0) {
                obj.data = $(this).closest('.dropdown').find('option').map(function() { return format(obj.type, this.value); }).get();
            }
            obj.selected = obj.data;
            if (key == 'ddlRegion') {
                $("#ddlProvincia").prop("disabled", false);
            }
            if (key == 'ddlFuncion') {
                $("#ddlPrograma").prop("disabled", false);
            }
            if (key == 'ddlPrograma') {
                $("#ddlSubPrograma").prop("disabled", false);
            }
            $('.selectpicker').selectpicker('refresh');
        });
        $(".bs-deselect-all").on('click', function() {
            var key = $(this).closest('.dropdown').find('select').attr('data-key');
            var obj = filterArr('filterParams', 'key', key);
            obj.selected = [];
            if (key == 'ddlRegion') {
                $("#ddlProvincia").prop("disabled", true);
            }
            if (key == 'ddlFuncion') {
                $("#ddlPrograma").prop("disabled", true);
            }
            if (key == 'ddlPrograma') {
                $("#ddlSubPrograma").prop("disabled", true);
            }
            $('.selectpicker').selectpicker('refresh');

        });
        $('[data-key="txtMontoAsignado"], [data-key="txtMontoCIPRL"], [data-key="txtPoblacion"]').change(function(e) {
            var key = $(this).attr('data-key');
            var val = format('Number', e.target.value);

            if (key == 'txtMontoAsignado') {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val * 1000000;
            } else if (key == 'txtPolacion') {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val * 1000;
            } else {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val;
            }
        });
        $('#ddlFases').selectpicker('selectAll');
        dropdownlist([
            { 'field': 'ddlFases', 'selected': true }
        ]);
        $('#txtradioInfl,#txtRuc').keyup(function() {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
        $('[data-key="txtNroTrab"],[data-key="txtUtil1"],[data-key="txtMontoAsignado1"], [data-key="txtRanking"], [data-key="txtIngresos"], [data-key="txtUtil"], [data-key="txtPatrimonio"], [data-key="txtOxi"], [data-key="txtMontoAsignado"], [data-key="txtMontoCIPRL"], [data-key="txtPoblacion"], [data-key="txtMontoCIPRL1"], [data-key="txtPoblacion1"]').change(function(e) {
            var key = $(this).attr('data-key');
            var val = format('Number', e.target.value);

            if (key == 'txtMontoAsignado' || key == 'txtOxi' || key == 'txtRanking' || key == 'txtUtil' || key == 'txtUtil1' || key == 'txtMontoAsignado1') {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val * 1000000;
            } else if (key == 'txtPolacion' || key == 'txtPolacion1') {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val * 1000;
            } else {
                filterArr('filterParams', 'key', key)['selected'][this.id] = val;
            }

        });
        $('[data-key="txtRazonSocial"],[data-key="txtRuc"]').change(function(e) {

            var key = $(this).attr('data-key');
            var val = format('String', e.target.value);
            filterArr('filterParams', 'key', key)['selected'][this.id] = val;
        });
    }
    var loadCongreso = function(cql) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'gen_politicos_marker',
                        sql: cql
                    })
                )),
            dataType: 'json',
            success: function(data) {
                layerCongreso = L.layerGroup().addTo(map33);
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/' + feature.properties.imagen + '.png" title="' + feature.properties.bancada + '" border="0"/>', className: 'info' })
                        });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(layerCongreso);
                    }
                });


            }
        });
    }
    var initComponents = function() {

        $('#ddlTipoBusq').change(function() {
            var ddlTipoBusq = $('#ddlTipoBusq option:selected').index();
            switch (ddlTipoBusq) {
                case 0:
                    var html = `<input type="text" class="form-control form-control-sm" id="txtseacrhubi" placeholder="Busqueda por Región, Provincia o Distrito">`;
                    $('#inputSeacrh').html(html);
                    $('#txtseacrhubi').autocomplete({
                        minLength: 3,
                        source: "php/searchubigeos.php",
                        html: true,
                        open: function(event, u) {
                            $('.ui-autocomplete').css('z-index', 10000);
                            $('.ui-autocomplete').css('font', '10px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif');
                            $('.ui-autocomplete').css('max-height', '200px');
                            $('.ui-autocomplete').css('overflow-y', 'auto');
                            $('.ui-autocomplete').css('overflow-x', 'hidden');
                            $('.ui-autocomplete').css('padding-right', '10px');
                        }
                    });
                    break;
                case 1:
                    var html = `<input type="text" class="form-control form-control-sm" id="txtseacrhpro" placeholder="Busqueda por código unico">`;
                    $('#inputSeacrh').html(html);
                    break;
                case 2:
                    var html = `<input type="text" class="form-control form-control-sm" id="txtseacrhemp" placeholder="Busqueda por RUC o Razón Social">`;
                    $('#inputSeacrh').html(html);
                    $('#txtseacrhemp').autocomplete({
                        minLength: 3,
                        source: "php/searchempresas.php",
                        html: true,
                        open: function(event, u) {
                            $('.ui-autocomplete').css('z-index', 10000);
                            $('.ui-autocomplete').css('font', '10px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif');
                            $('.ui-autocomplete').css('max-height', '200px');
                            $('.ui-autocomplete').css('overflow-y', 'auto');
                            $('.ui-autocomplete').css('overflow-x', 'hidden');
                            $('.ui-autocomplete').css('padding-right', '10px');
                        }
                    });
                    break;
                case 3:
                    var html = `<input type="text" class="form-control form-control-sm" id="txtseacrhparti" placeholder="Busqueda por partidos politicos">`;
                    $('#inputSeacrh').html(html);
                    $('#txtseacrhparti').autocomplete({
                        minLength: 3,
                        source: "php/searchpartidos.php",
                        html: true,
                        open: function(event, u) {
                            $('.ui-autocomplete').css('z-index', 10000);
                            $('.ui-autocomplete').css('font', '10px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif');
                            $('.ui-autocomplete').css('max-height', '200px');
                            $('.ui-autocomplete').css('overflow-y', 'auto');
                            $('.ui-autocomplete').css('overflow-x', 'hidden');
                            $('.ui-autocomplete').css('padding-right', '10px');
                        }
                    });
                    break;
                case 4:
                    var html = `<input type="text" class="form-control form-control-sm" id="txtseacrhdire" placeholder="Busqueda Direcciones">`;
                    $('#inputSeacrh').html(html);
                    break;
            }
        });
        $('#txtseacrhubi').autocomplete({
            minLength: 3,
            source: "php/searchubigeos.php",
            html: true,
            open: function(event, u) {
                $('.ui-autocomplete').css('z-index', 10000);
                $('.ui-autocomplete').css('font', '10px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif');
                $('.ui-autocomplete').css('max-height', '200px');
                $('.ui-autocomplete').css('overflow-y', 'auto');
                $('.ui-autocomplete').css('overflow-x', 'hidden');
                $('.ui-autocomplete').css('padding-right', '10px');
            }
        });

        $('.leaflet-control-dialog-grabber').draggable();
        $(document).on('change', 'input[type="radio"][name="rbtnopor"]', function() {

            switch ($(this).val()) {
                case 'rbtnFunProy':
                    var objOport = {
                        tipo1: 'funcion',
                        checked1: 3

                    }
                    $.post({
                        url: 'divConsolidadoProyectosOportunidades.php?data=' + encodeURIComponent(JSON.stringify(objOport)),
                        success: function(response) {
                            document.getElementById('idconsolidadoOportunidades').innerHTML = response;
                        }
                    });

                    break;
                case 'rbtnDivProy':
                    var objOport = {
                        tipo1: 'division',
                        checked1: 4

                    }
                    $.post({
                        url: 'divConsolidadoProyectosOportunidades.php?data=' + encodeURIComponent(JSON.stringify(objOport)),
                        success: function(response) {
                            document.getElementById('idconsolidadoOportunidades').innerHTML = response;
                        }
                    });
                    break;
                case 'rbtnGruProy':
                    var objOport = {
                        tipo1: 'grupo',
                        checked1: 5

                    }
                    $.post({
                        url: 'divConsolidadoProyectosOportunidades.php?data=' + encodeURIComponent(JSON.stringify(objOport)),
                        success: function(response) {
                            document.getElementById('idconsolidadoOportunidades').innerHTML = response;
                        }
                    });
                    break;
            }
        });
        $(document).on('change', 'input[type="checkbox"][name="checkbox_PartidoTotal"]', function() {
            if (layerCongreso != undefined) {
                layerCongreso.clearLayers();
            }
            var checkBox = document.getElementById("cbox_1");
            if (checkBox.checked == true) {
                $('input[name="checkbox_Partido"]').prop("checked", true);
                loadCongreso('1=1');
            } else if (checkBox.checked == false) {
                if (layerCongreso != undefined) {
                    $('input[name="checkbox_Partido"]').prop("checked", false);
                    layerCongreso.clearLayers();
                }
            }
        });
        $(document).on('change', 'input[type="checkbox"][name="checkbox_Partido"]', function() {
            var temp = [];
            $('input[type=checkbox][name="checkbox_Partido"]:checked').each(function() {
                temp.push("'" + $(this).prop("id").substring(6) + "'");
            });
            sqlCount = 'bancada in (' + temp.join(',') + ')';

            if (layerCongreso != undefined) {
                layerCongreso.clearLayers();
                loadCongreso(sqlCount);
            }

        });

        $(document).on('change', 'input[type="radio"][name="rbtncprpd"]', function() {

            switch ($(this).val()) {
                case 'rbtnCantProy':
                    var objCRPD = {
                        tipo: 'cantidad',
                        checked: 1,
                        nivel: clickTablaId.substring(14)
                    }
                    $.post({
                        url: 'divConsolidadoProyectosDet.php?data=' + encodeURIComponent(JSON.stringify(objCRPD)),
                        success: function(response) {
                            document.getElementById('idconsolidadoProyectos').innerHTML = response;
                        }
                    });
                    break;
                case 'rbtnMontoProy':
                    var objCRPD = {
                        tipo: 'monto',
                        checked: 2,
                        nivel: clickTablaId.substring(14)

                    }
                    $.post({
                        url: 'divConsolidadoProyectosDet.php?data=' + encodeURIComponent(JSON.stringify(objCRPD)),
                        success: function(response) {
                            document.getElementById('idconsolidadoProyectos').innerHTML = response;
                        }
                    });
                    break;
            }
        });
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil1"]', function() {
            var tmp = [];
            $("input[name=chk_quantil1]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile1], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });
        $(document).on('change', 'input[type="checkbox"][name="checkbox_GeoCoordenadasTotal"]', function() {
            var checkBox = document.getElementById("cbox_");
            if (checkBox.checked == true) {
                $('input[name="checkbox_GeoCoordenadas"]').prop("checked", true);
                loadGeoEmpresas('');
            } else if (checkBox.checked == false) {
                $('input[name="checkbox_GeoCoordenadas"]').prop("checked", false);
                deleteGeo();
            }
        });
        $(document).on('change', 'input[type="checkbox"][name="checkbox_GeoCoordenadas"]', function() {

            var id = this.getAttribute('id');
            var temp = [];

            $('input[type=checkbox][name="checkbox_GeoCoordenadas"]:checked').each(function() {
                temp.push("'" + $(this).prop("id").substring(5) + "'");
            });
            console.log("empresa IN (" + temp.join(',') + ")");
            if (this.checked == true) {
                loadGeoEmpresas("empresa IN (" + temp.join(',') + ")");
            } else {
                deleteGeo();
                loadGeoEmpresas("empresa IN (" + temp.join(',') + ")");
            }
        });
        $(document).on('change', 'input[type="checkbox"][name="mapsCoordenadas1"]', function() {
            console.log(direccionesTemporal);
            var checkboxes = document.getElementsByName('mapsCoordenadas');
            for (var checkbox of checkboxes) {

                if (checkbox.checked == true) {
                    layerLatLngCoordenadas.clearLayers();

                } else {

                    loadCompaniesAnexos("ruc ='" + direccionesTemporalruc + "'")
                }
                checkbox.checked = this.checked;
            }


        });
        $(document).on('change', 'input[type="checkbox"][name="mapsCoordenadas"]', function() {


            if (layerLatLngCoordenadas != undefined) {
                map111.removeLayer(layerLatLngCoordenadas);
            }

            //layerLatLngCoordenadas.clearLayers();
            var tmp = [];
            $("input[name=mapsCoordenadas]").each(function(index) {

                if ($(this).is(':checked')) {
                    tmp.push(index + 1);

                }
            });
            if (tmp.length == 0) {
                loadCompaniesAnexos("ruc ='12345'");
            } else {
                loadCompaniesAnexos("ruc ='" + direccionesTemporalruc + "' and identificador in(" + tmp.join(',') + ")");
            }

            console.log(tmp.length);


        });
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil2"]', function() {
            var tmp = [];
            $("input[name=chk_quantil2]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile2], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });
        $(document).on('change', 'input[type="checkbox"][name="chk_quantil3"]', function() {
            var tmp = [];
            $("input[name=chk_quantil3]").each(function(index) {
                if ($(this).is(':checked')) {
                    tmp.push(index + 1);
                    lyrDraw([tile3], "quantil in (" + tmp.join(',') + ")");
                }
            });
        });


        $(document).on('change', 'input[type="radio"][name="radioEmpresas"]', function() {
            var divStakedBar1 = document.getElementById('stkd_charempresa1');
            switch ($(this).val()) {
                case 'rbtn1':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 400px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 400px; width: 100%;';
                    var obj = {
                        limitFin: 10,
                        limitInicio: 0,
                        titulo: 'Top 10 Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 1
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 10,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);

                    break;
                case 'rbtn1Resto':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 2400px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 2400px; width: 100%;';
                    var obj = {
                        limitFin: 109,
                        limitInicio: 10,
                        titulo: '93 Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 2
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 10
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'rbtn1Total':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 3500px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 3500px; width: 100%;';
                    var obj = {
                        limitFin: 109,
                        limitInicio: 0,
                        titulo: 'Total Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 3
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'rbtn2':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 400px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 400px; width: 100%;';
                    var obj = {
                        limitFin: 10,
                        limitInicio: 0,
                        titulo: 'Top 10 Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 1,
                        rbtnTabla: 1
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 10,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'rbtn2Resto':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 2400px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 2400px; width: 100%;';

                    var obj = {
                        limitFin: 109,
                        limitInicio: 10,
                        titulo: 'Resto de Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 1,
                        rbtnTabla: 2
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 10
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'rbtn2Total':
                    var divStakedBar = document.getElementById('stkd_charempresa');
                    divStakedBar.style.cssText = 'height: 3500px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 3500px; width: 100%;';
                    var obj = {
                        limitFin: 109,
                        limitInicio: 0,
                        titulo: 'Total Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 1,
                        rbtnTabla: 3
                    };
                    tablaTopEmpresasOxi(obj);
                    stackedBarTopEmpresasOxi(obj);
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
            }
        });

        $('input[data-id="infoMinsa"]').attr('disabled', 'disabled');
        $(document).on('click', 'input[type="radio"][name="COVID-19"]', function() {
            toastr.success("Cargando Capas ", "Usted Selecciono capa COVID-19", { timeOut: 2000, closeButton: true, progressBar: true });
            document.getElementById("txttermoInfografia").value = 2;
            $(".legendSalud").css("display", "");
            $('#COVID-19').prop("checked", true);
            $('input[name="SINADEF"]').attr('disabled', 'disabled');
            $('input[id="SINADEF"]').attr('disabled', 'disabled');
            var tmp = '';
            var id = this.getAttribute('data-id');
            switch (id) {
                case 'region_minsa':
                    document.getElementById("txttermoInfoUbigeo").value = 1;
                    break;
                case 'provincia_minsa':
                    document.getElementById("txttermoInfoUbigeo").value = 2;
                    break;
                case 'distrito_minsa':
                    document.getElementById("txttermoInfoUbigeo").value = 3;
                    break;
            }
            var info = $('input[data-id="infoMinsa"]:checked').length > 0;
            filterArr('overLayers', 'key', 'baseMinsa').layers.filter(p => p.control == 'radio' && p.key != id).forEach(function(row) {
                map.removeLayer(row.layer);
            });
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseMinsa', 'infoMinsa');
                if (info) {
                    map.removeLayer(lyr.layer);
                    switch (id) {
                        case 'region_minsa':
                            tmp = lyr['lyrRegion'];
                            document.getElementById("txttermoInfoUbigeo").value = 1;
                            break;
                        case 'provincia_minsa':
                            tmp = lyr['lyrProv'];
                            document.getElementById("txttermoInfoUbigeo").value = 2;
                            break;
                        case 'distrito_minsa':
                            tmp = lyr['lyrDist'];
                            document.getElementById("txttermoInfoUbigeo").value = 3;
                            break;
                    }
                    lyr.layer = tmp;
                    map.addLayer(lyr.layer);
                }
            }
            $('#COVID-19').prop("checked", true);
            $('input[data-id="infoMinsa"]').removeAttr('disabled');
        });

        $(document).on('click', 'input[data-id="infoMinsa"]', function() {
            var tmp = '';
            var item = $(this);
            var id = $('input[type="radio"][name="COVID-19"]:checked').attr('data-id');
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseMinsa', 'infoMinsa');
                switch (id) {
                    case 'region_minsa':
                        tmp = lyr['lyrRegion'];
                        break;
                    case 'provincia_minsa':
                        tmp = lyr['lyrProv'];
                        break;
                    case 'distrito_minsa':
                        tmp = lyr['lyrDist'];
                        break;
                }

                lyr.layer = tmp;
                if (item.is(':checked')) {
                    map.addLayer(lyr.layer);
                    iniLoadAlert('Informacion COVID-19!', 'Cargando capa ' + id, 1300);
                } else {
                    map.removeLayer(lyr.layer);
                }
            }

        });
        $('input[data-id="infoSinadef"]').attr('disabled', 'disabled');
        $(document).on('click', 'input[type="radio"][name="SINADEF"]', function() {
            toastr.success("Cargando Capas ", "Usted Selecciono capa SINADEF", { timeOut: 2000, closeButton: true, progressBar: true });
            document.getElementById("txttermoInfografia").value = 3;
            $(".legendSaludSinadef").css("display", "");
            $('#SINADEF').prop("checked", true);
            $('input[name="COVID-19"]').attr('disabled', 'disabled');
            $('input[id="COVID-19"]').attr('disabled', 'disabled');
            // 
            var tmp = '';
            var id = this.getAttribute('data-id');
            switch (id) {
                case 'region_sinadef':
                    document.getElementById("txttermoInfoUbigeo").value = 1;
                    break;
                case 'provincia_sinadef':
                    document.getElementById("txttermoInfoUbigeo").value = 2;
                    break;
                case 'distrito_sinadef':
                    document.getElementById("txttermoInfoUbigeo").value = 3;
                    break;
            }
            var info = $('input[data-id="infoSinadef"]:checked').length > 0;
            filterArr('overLayers', 'key', 'baseSinadef').layers.filter(p => p.control == 'radio' && p.key != id).forEach(function(row) {
                map.removeLayer(row.layer);
            });
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseSinadef', 'infoSinadef');
                if (info) {
                    map.removeLayer(lyr.layer);
                    switch (id) {
                        case 'region_sinadef':
                            tmp = lyr['lyrRegion'];
                            document.getElementById("txttermoInfoUbigeo").value = 1;
                            break;
                        case 'provincia_sinadef':
                            tmp = lyr['lyrProv'];
                            document.getElementById("txttermoInfoUbigeo").value = 2;
                            break;
                        case 'distrito_sinadef':
                            tmp = lyr['lyrDist'];
                            document.getElementById("txttermoInfoUbigeo").value = 3;
                            break;
                    }
                    lyr.layer = tmp;
                    map.addLayer(lyr.layer);
                }
            }
            $('#SINADEF').prop("checked", true);
            $('input[data-id="infoSinadef"]').removeAttr('disabled');
        });

        $(document).on('click', 'input[data-id="infoSinadef"]', function() {
            var tmp = '';
            var item = $(this);
            var id = $('input[type="radio"][name="SINADEF"]:checked').attr('data-id');
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseSinadef', 'infoSinadef');
                switch (id) {
                    case 'region_sinadef':
                        tmp = lyr['lyrRegion'];
                        break;
                    case 'provincia_sinadef':
                        tmp = lyr['lyrProv'];
                        break;
                    case 'distrito_sinadef':
                        tmp = lyr['lyrDist'];
                        break;
                }

                lyr.layer = tmp;
                if (item.is(':checked')) {
                    map.addLayer(lyr.layer);
                    iniLoadAlert('Informacion SINADEF!', 'Cargando capa ' + id, 1300);
                } else {
                    map.removeLayer(lyr.layer);
                }
            }

        });








        $(document).on('change', 'input[type="radio"][name="radioDivPIPSTotal"]', function() {
            switch ($(this).val()) {
                case 'rbtnPipsTotal':
                    var objWord = {
                        method: 'wordRbtnPipsTotal',
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        codnivel: document.getElementById('codnivelPIP').value,
                        tipoChange: 'ofq.nomfuncionproyecto'
                    };
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objWord)),
                        success: function(response) {
                            var word = JSON.parse(response.data[0].subprograma);
                            var objEnvioWord = {
                                data: word,
                                div: 'divword2'
                            };

                            wordCAloud1(objEnvioWord);
                        }
                    });
                    break;
                case 'rbtnPipsTotal1':
                    var objWord = {
                        method: 'wordRbtnPipsTotal',
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        codnivel: document.getElementById('codnivelPIP').value,
                        tipoChange: 'opp.nomprogramaproyecto'
                    };
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objWord)),
                        success: function(response) {
                            var word = JSON.parse(response.data[0].subprograma);
                            var objEnvioWord = {
                                data: word,
                                div: 'divword2'
                            };
                            wordCAloud1(objEnvioWord);
                        }
                    });
                    break;
                case 'rbtnPipsTotal2':
                    var objWord = {
                        method: 'wordRbtnPipsTotal',
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        codnivel: document.getElementById('codnivelPIP').value,
                        tipoChange: 'ogp.nomsubprograma'
                    };
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objWord)),
                        success: function(response) {
                            var word = JSON.parse(response.data[0].subprograma);
                            var objEnvioWord = {
                                data: word,
                                div: 'divword2'
                            };
                            wordCAloud1(objEnvioWord);
                        }
                    });
                    break;
            }
        });
        $(document).on('change', 'input[type="radio"][name="rbtncpe"]', function() {

            switch ($(this).val()) {
                case 'rbtnCIPRL':
                    var objCPE = {
                        tipo: 'ciprl',
                        checked: 1,
                        region: document.getElementById('coddptoPF').value
                    }
                    $.post({
                        url: 'divCiprlPoblacionElectoresQuintil.php?data=' + encodeURIComponent(JSON.stringify(objCPE)),
                        success: function(response) {
                            document.getElementById('ciprlpoelec').innerHTML = response;
                        }
                    });
                    break;
                case 'rbtnPoblacion':
                    var objCPE = {
                        tipo: 'poblacion',
                        checked: 2,
                        region: document.getElementById('coddptoPF').value
                    }
                    $.post({
                        url: 'divCiprlPoblacionElectoresQuintil.php?data=' + encodeURIComponent(JSON.stringify(objCPE)),
                        success: function(response) {
                            document.getElementById('ciprlpoelec').innerHTML = response;
                        }
                    });
                    break;
                case 'rbtnEectores':
                    var objCPE = {
                        tipo: 'electores',
                        checked: 3,
                        region: document.getElementById('coddptoPF').value
                    }
                    $.post({
                        url: 'divCiprlPoblacionElectoresQuintil.php?data=' + encodeURIComponent(JSON.stringify(objCPE)),
                        success: function(response) {
                            document.getElementById('ciprlpoelec').innerHTML = response;
                        }
                    });
                    break;
            }
        });
        $('input[data-id="infoCIPRL"]').attr('disabled', 'disabled');
        $(document).on('click', 'input[type="radio"][name="CIPRL"]', function() {
            var tmp = '';
            var id = this.getAttribute('data-id');
            var info = $('input[data-id="infoCIPRL"]:checked').length > 0;
            filterArr('overLayers', 'key', 'baseUbigeo').layers.filter(p => p.control == 'radio' && p.key != id).forEach(function(row) {
                map.removeLayer(row.layer);
            });
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL');
                if (info) {
                    map.removeLayer(lyr.layer);
                    switch (id) {
                        case 'region':
                            tmp = lyr['lyrRegion'];
                            break;
                        case 'provincia':
                            tmp = lyr['lyrProv'];
                            break;
                        case 'distrito':
                            tmp = lyr['lyrDist'];
                            break;
                    }
                    lyr.layer = tmp;
                    map.addLayer(lyr.layer);
                }
            }

            $('#CIPRL').prop("checked", true);
            $('input[data-id="infoCIPRL"]').removeAttr('disabled');
        });



        $(document).on('click', 'input[data-id="infoCIPRL"]', function() {
            var tmp = '';
            var item = $(this);
            var id = $('input[type="radio"][name="CIPRL"]:checked').attr('data-id');
            if (typeof id != "undefined") {
                lyr = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL');
                switch (id) {
                    case 'region':
                        tmp = lyr['lyrRegion'];
                        break;
                    case 'provincia':
                        tmp = lyr['lyrProv'];
                        break;
                    case 'distrito':
                        tmp = lyr['lyrDist'];
                        break;
                }

                lyr.layer = tmp;
                if (item.is(':checked')) {
                    map.addLayer(lyr.layer);
                    iniLoadAlert('Informacion Ciprl!', 'Cargando capa ' + id, 1300);
                } else {
                    map.removeLayer(lyr.layer);
                }
            }

        });

        $('input[data-id="hospitales"]').attr('disabled', 'disabled');
    }
    var initComponentsHome = function() {
        $('#txtProyecto').tokenInput(
            "php/app.php", {
                hintText: "Ingrese Distrito",
                allowCustomEntry: true,
                allowFreeTagging: true,
                searchingText: "Buscando...",
                minChars: 1,
                tokenLimit: 5,
                tokenValue: "id",
                preventDuplicates: false
            });
        $('#ddlTipoBusq').change(function() {
            var ddlTipoBusq = $('#ddlTipoBusq option:selected').index();
            console.log(ddlTipoBusq);
            switch (ddlTipoBusq) {
                case 0:
                    $('#txtProyecto').tokenInput(
                        "php/app.php", {
                            hintText: "Ingrese Distrito",
                            allowCustomEntry: true,
                            allowFreeTagging: true,
                            noResultsText: "No se Encontraron resultados",
                            searchingText: "Buscando...",
                            minChars: 1,
                            tokenLimit: 5,
                            tokenValue: "id",
                            preventDuplicates: false
                        });
                    break;
                case 1:
                    $(".token-input-list").remove();
                    $("#txtProyecto").show();
                    break;
                case 2:
                    $(".token-input-list").remove();
                    $("#txtProyecto").show();
                    break;
                case 3:
                    $(".token-input-list").remove();
                    $("#txtProyecto").show();
                    break;
            }
        });
        $("#searchbox").click(function() {
            var tmp = [],
                tmp1 = [],
                tmp2 = [];
            var sqlCount = '1 = 1';
            var infoCIPRL = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL');
            var ddlTipoBusq = $('#ddlTipoBusq option:selected').index();
            var nameProject = $('#txtProyecto').val().toUpperCase();
            var arrToken = $('#txtProyecto').tokenInput("get");

            if (nameProject === '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Ocurrio un Error!!!',
                    text: 'Debe Ingresar un criterio de Busqueda',
                });
            } else {
                clearLayers(clusters.projects);
                clearLayers(clusters.Mapas_de_Calor);
                switch (ddlTipoBusq) {
                    case 0:
                        if (arrToken.length > 0) {
                            $.each(arrToken, function(i, obj) {
                                tmp.push("'" + obj.id + "'");
                                tmp1.push("'" + obj.id.substring(0, 4) + "'");
                                tmp2.push("'" + obj.id.substring(0, 2) + "'");
                            });
                            sqlCount = 'iddist in (' + tmp.join(',') + ')';
                            sqlCount1 = 'codubigeo in (' + tmp.join(',') + ')';
                            loadCiprlXubigeo('vw_distrito_geojson_info', infoCIPRL.lyrDist, "codubigeo IN (" + tmp.join(',') + ")");
                            loadCiprlXubigeo('vw_provincia_geojson_info', infoCIPRL.lyrProv, "codubigeo IN (" + tmp1.join(',') + ")");
                            loadCiprlXubigeo('vw_dpto_geojson_info', infoCIPRL.lyrRegion, "codubigeo IN (" + tmp2.join(',') + ")");

                            lyrDraw([lyrDistrito], "codubigeo IN (" + tmp.join(',') + ")");
                            lyrDraw([lyrProvincia], "codubigeo IN (" + tmp1.join(',') + ")");
                            lyrDraw([lyrRegion], "codubigeo IN (" + tmp2.join(',') + ")");

                            lyrDraw([lyrDistritoMinsa], "ubigeo IN (" + tmp.join(',') + ")");
                            lyrDraw([lyrProvinciaMinsa], "ubigeo IN (" + tmp1.join(',') + ")");
                            lyrDraw([lyrRegionMinsa], "ubigeo IN (" + tmp2.join(',') + ")");

                            lyrDraw([lyrDistritoSinadef], "ubigeo IN (" + tmp.join(',') + ")");
                            lyrDraw([lyrProvinciaSinadef], "ubigeo IN (" + tmp1.join(',') + ")");
                            lyrDraw([lyrRegionSinadef], "ubigeo IN (" + tmp2.join(',') + ")");


                            loadProjects(sqlCount);
                            mapaCalorPrueba(sqlCount);
                            $('#CIPRL').prop("checked", true);
                            $('input[data-id="infoCIPRL"]').prop("checked", true);
                            map.removeLayer(filterGroup('overLayers', 'baseLayer', 'proyectos').layer);
                            var obj = {
                                consulta_region: "co.codubigeo in (" + tmp2.join(',') + ")",
                                consulta_provincia: "geo.codubigeo in (" + tmp1.join(',') + ")",
                                consulta_distrito: "geo.codubigeo in (" + tmp.join(',') + ")",
                            };
                            $.post({
                                url: 'navTableBqdResult.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    document.getElementById('divTablebqdProycts').innerHTML = response;
                                }
                            });

                        } else {
                            loadCiprlXubigeo('vw_distrito_geojson_info', infoCIPRL.lyrDist, '');
                            lyrDraw([lyrDistrito], '1 = 1');
                        }
                        if (!map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'distrito').layer)) {
                            map.addLayer(filterGroup('overLayers', 'baseUbigeo', 'distrito').layer);
                        }
                        break;
                    case 2:
                        if (nameProject.length > 0) {
                            nameProject = nameProject.split(',');
                            for (var i = 0; i < nameProject.length; i++) {
                                tmp.push("oportunidad like '%" + nameProject[i].trim() + "%'");
                            }
                            sqlCount = tmp.join(' AND ');
                            loadProjects(sqlCount);
                            mapaCalorPrueba(sqlCount);
                            document.getElementById('divTablebqdProycts').innerHTML = "";
                        } else {
                            loadProjects('1 = 1');
                            mapaCalorPrueba('1 = 1');
                        }
                        break;
                    case 1:
                        if (nameProject.length > 0) {
                            nameProject = nameProject.split(',');
                            for (var i = 0; i < nameProject.length; i++) {
                                tmp.push("'" + nameProject[i].trim() + "'");
                            }
                            sqlCount = "codigounico IN (" + tmp.join(',') + ")";
                            console.log(sqlCount);
                            loadProjects(sqlCount);
                            mapaCalorPrueba(sqlCount);
                            document.getElementById('divTablebqdProycts').innerHTML = "";
                        }
                        break;
                }
                if (sqlCount == '1 = 1') {
                    loadProjects(sqlCount);
                    mapaCalorPrueba(sqlCount);
                }

                counterSum(sqlCount);
            }

        });
    }
    var sideBarMenu = function() {
        var id = document.getElementById("txtidRol").value;
        var loading = '<div class="container" style="text-align:center !important;padding-top:20em;">';
        loading += '<img src="assets/app/img/loading.gif" alt="">';
        loading += '</div>';
        sidebar = L.control.sidebar('sidebar', {
            closeButton: true,
            position: 'left',
            autoPan: false
        });
        map.addControl(sidebar);
        sidebar.addPanel({
            id: 'home',
            tab: '<i class="fa fa-search"></i>',
            title: 'Busqueda Rapida de Proyectos, entidades y empresas',
            button: function(event) {
                sidebar.open('home');
                $('.leaflet-sidebar-content').html(loading);
                setTimeout(function() {
                    $.post({
                        url: 'navproyects.php',
                        success: function(response) {
                            $('.leaflet-sidebar-content').html(response);
                            initComponentsHome();

                        }
                    });
                }, 100);
            }
        });
        sidebar.addPanel({
            id: 'busqueda',
            tab: '<i class="fa fa-search-location"></i>',
            title: 'Filtro de Proyectos de Inversion Pública',
            button: function(event) {
                sidebar.open('busqueda');
                $('.leaflet-sidebar-content').html(loading);
                setTimeout(function() {
                    $.post({
                        url: 'navproyectsfiltro.php',
                        success: function(response) {
                            $('.leaflet-sidebar-content').html(response);
                            initComponentsProyects();
                        }
                    });
                }, 100);
            }
        });
        sidebar.addPanel({
            id: 'user',
            tab: '<i class="fa fa-user"></i>',
            position: 'bottom',
            title: 'Información de Usuario',
            button: function(event) {
                var obj = {
                    id: document.getElementById("txtiduser").value,
                    method: 'usercredentials'
                }
                $.post({
                    url: 'popupusersettings.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        dialog2.setContent(response);
                    }
                });
            }
        });
        sidebar.addPanel({
            id: 'draw',
            tab: '<i class="fa fa-search"></i>',
            title: 'Mis Objetos Guardados',
            button: function(event) {
                sidebar.open('draw');
                $('.leaflet-sidebar-content').html(loading);
                setTimeout(function() {
                    $.post({
                        url: 'navDraw.php',
                        success: function(response) {
                            $('.leaflet-sidebar-content').html(response);
                        }
                    });
                }, 100);
            }
        });
        if (id == 1) {
            sidebar.addPanel({
                id: 'adminuser',
                position: 'bottom',
                title: 'Configuración de Usuarios',
                tab: '<i class="fa fa-cog"></i>',
                button: function(event) {
                    $.get({
                        url: 'popupcreateuser.php',
                        success: function(response) {
                            dialog.setContent(response);
                            $.post({
                                url: 'divtablauser.php',
                                success: function(response) {
                                    document.getElementById('divtablauser').innerHTML = response;
                                }
                            });
                        }
                    });

                }
            });
        }
        sidebar.addPanel({
            id: 'off',
            tab: '<i class="fa fa-power-off"></i>',
            title: 'Salir del Observatorio',
            position: 'bottom',
            button: function(event) {
                Swal.fire({
                    title: 'Cerrar Session',
                    text: 'Seguro que desea Salir',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        var obj = {
                            method: 'destruir'
                        }
                        $.get({
                            url: './php/session.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                window.location.href = "./index.php";
                            }
                        });
                    }
                })
            }
        });


        sidebar.addPanel({
            id: 'estadisticas',
            tab: '<i class="fa fa-chart-bar"></i>',
            position: 'top',
            title: 'Mostrar Dashboard',
            button: function(event) {
                window.open('http://observatorio.colaboraccion.pe/dashboard/dashboard.php', '_blank');

            }
        });
        sidebar.addPanel({
            id: 'resumen',
            tab: '<i class="fa fa-city"></i>',
            title: 'Resumen de Información',
            button: function(event) {
                $.post({
                    url: 'popupresumenInformacion.php',
                    success: function(response) {
                        dialog8.setLocation([90, 90]);
                        dialog8.setContent(response);
                        var obj = {
                            mostrar: 'empresas-tab',
                        }
                        $.post({
                            url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('divresumenoxi').innerHTML = response;
                                var obj = {
                                    method: 'chartPieEmpresaOxi'
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        var data = [
                                            { 'name': 'Top 10 Empresas', 'y': parseInt(response.data[0].top10), 'selected': true },
                                            { 'name': 'Resto de Empresas', 'y': parseInt(response.data[0].total) }
                                        ];
                                        var series = [{
                                            name: 'Empresas',
                                            colorByPoint: true,
                                            data: data
                                        }];
                                        var charpieobj = charPie2;
                                        charpieobj.series = series;
                                        Highcharts.chart('chartContainerEmpresas', charpieobj);
                                        $.post({
                                            url: 'divGeolocalizacionEmpresas.php',
                                            success: function(response) {
                                                document.getElementById('idGeoempresas').innerHTML = response;
                                                map2 = L.map('map2').setView([-9.33, -74.44], 5);
                                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                                }).addTo(map2);
                                                map2.invalidateSize();
                                                loadGeoEmpresas('');
                                                legendMap1();
                                                var obj = {
                                                    titulo: 'Top 10 empresas Millones de soles',
                                                    rbtnTabla: 1,
                                                    desdeLimite: 2,
                                                    limitFin: 10,
                                                    limitInicio: 0
                                                }
                                                tablaTopEmpresasOxi(obj);
                                                stackedBarTopEmpresasOxi(obj);
                                                var obj1AdjCon = {
                                                    method: 'chartStakedEmpresas',
                                                    limitFin: 10,
                                                    limitInicio: 0
                                                };
                                                empresasTableChangeAdjudicados(obj1AdjCon);

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });

            }
        });
        sidebar.addPanel({
            id: 'temporal',
            tab: '<i class="fa fa-text-width"></i>',
            position: 'top',
            title: 'Mostrar Indicadores',
            button: function(event) {
                //window.open('http://observatorio.colaboraccion.pe/obs_new/dashboard/dashboard.php', '_blank');
                var obj = {
                    tipo: 'MACROINDICADORES'
                }
                $.post({
                    url: 'popupindicadoresycredenciales.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        dialog7.setLocation([90, 90]);
                        dialog7.setContent(response);
                        var obj = {
                            titulo: document.getElementById("txttipoCIPRLcANONM").value
                        }
                        $.post({
                            url: 'divresrmenyestadisticas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('divresumeninformacion').innerHTML = response;
                                var obj = {
                                    tipo: 'region',
                                    forma: 'asc'
                                }
                                $.post({
                                    url: 'divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('getDivEmpresasEstadistica').innerHTML = response;


                                    }
                                });
                                var obj = {
                                    tipo: 'funcion',
                                    forma: 'asc'
                                }
                                $.post({
                                    url: 'divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('getDivEmpresasEstadistica1').innerHTML = response;


                                    }
                                });

                            }
                        });

                    }
                });

            }
        });

        sidebar.addPanel({
            id: 'leyenda',
            tab: '<i class="fa fa-book"></i>',
            title: 'Mostrar Información',
            button: function(event) {
                sidebar.open('leyenda');
                $('.leaflet-sidebar-content').html(loading);
                setTimeout(function() {
                    $.post({
                        url: 'navleyenda.php',
                        success: function(response) {
                            $('.leaflet-sidebar-content').html(response);
                            initComponentsHome();

                        }
                    });
                }, 100);
            }
        });

    }
    var tablaTopEmpresasOxi = function(obj) {
        $.post({
            url: 'tableTopEmpresasOxi.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                document.getElementById('tablaTopEmpresas').innerHTML = response;
            }
        });
    }
    var stackedBarTopEmpresasOxi = function(obj) {
        obj.method = 'empresasOXIExcel';
        $.getJSON({
            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                var categorias = [];
                var obj2009 = {
                    name: '2009',
                    data: []
                }
                var obj2010 = {
                    name: '2010',
                    data: []
                }
                var obj2011 = {
                    name: '2011',
                    data: []
                }
                var obj2012 = {
                    name: '2012',
                    data: []
                }
                var obj2013 = {
                    name: '2013',
                    data: []
                }
                var obj2014 = {
                    name: '2014',
                    data: []
                }
                var obj2015 = {
                    name: '2015',
                    data: []
                }
                var obj2016 = {
                    name: '2016',
                    data: []
                }
                var obj2017 = {
                    name: '2017',
                    data: []
                }
                var obj2018 = {
                    name: '2018',
                    data: []
                }
                var obj2019 = {
                    name: '2019',
                    data: []
                }
                var obj2020 = {
                    name: '2020',
                    data: []
                }
                for (i = 0; i < response.data.length; i++) {
                    categorias.push(response.data[i].nombre);
                    obj2015.data.push(parseInt(response.data[i].ano2015));
                    obj2016.data.push(parseInt(response.data[i].ano2016));
                    obj2017.data.push(parseInt(response.data[i].ano2017));
                    obj2018.data.push(parseInt(response.data[i].ano2018));
                    obj2019.data.push(parseInt(response.data[i].ano2019));
                    obj2020.data.push(parseInt(response.data[i].ano2020));
                    if (obj.desdeLimite == 1) {
                        obj2009.data.push(parseInt(response.data[i].ano209));
                        obj2010.data.push(parseInt(response.data[i].ano2010));
                        obj2011.data.push(parseInt(response.data[i].ano2011));
                        obj2012.data.push(parseInt(response.data[i].ano2012));
                        obj2013.data.push(parseInt(response.data[i].ano2013));
                        obj2014.data.push(parseInt(response.data[i].ano2014));
                    }
                }
                if (obj.desdeLimite == 1) {
                    var total = [obj2009, obj2010, obj2011, obj2012, obj2013, obj2014, obj2015, obj2016, obj2017, obj2018, obj2019, obj2020];
                } else {
                    var total = [obj2015, obj2016, obj2017, obj2018, obj2019, obj2020];
                }

                var stck = stackedBar;
                stck.xAxis.categories = categorias;
                stck.title.text = obj.titulo;
                stck.series = total;
                Highcharts.chart('stkd_charempresa', stck);

            }
        });
    }
    var empresasTableChangeAdjudicados = function(obj) {
        $.get({
            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                var result = JSON.parse(response);
                var objChart = {
                    div: 'stkd_charempresa1',
                    titulo: 'Ranking de Empresas por Monto de Inversión en Obras por Impuestos Adjudicados y Concluidos',
                    intervalo: 25,
                    datatitulo: result.data.datatitulo,
                    data: result.data.sAdjudicado,
                    data1: result.data.sConcluido
                }
                chartStakedBaraDJUDICADOScONCLUIDOS(objChart);
            }
        });
    }
    var chartStakedBaraDJUDICADOScONCLUIDOS = function(obj) {
        var chart1 = Highcharts.chart(obj.div, {
            animationEnabled: true,
            chart: {
                type: 'bar'
            },
            plotOptions: {
                series: {
                    stacking: 'normal',
                    maxPointWidth: 10,
                },

            },
            title: {
                text: obj.titulo,

                fontSize: 15,
                fontFamily: "Helvetica",
                fontWeight: "bold",


            },
            xAxis: {
                categories: obj.datatitulo,

                fontSize: 15,
                fontFamily: "Helvetica",
                fontWeight: "bold",

            },
            legend: {
                verticalAlign: "bottom",
                fontSize: 11,
                fontFamily: "Helvetica",
                Margin: 8
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Millones de Soles (M S/.)',

                    fontSize: 15,
                    fontFamily: "Helvetica",
                    color: "gray"

                }
            },
            series: [

                {
                    name: 'Concluido',
                    data: obj.data1,
                    color: '#007bff',
                    legendIndex: 1

                }, {
                    name: 'Adjudicado',
                    data: obj.data,
                    color: '#ffc107',
                    legendIndex: 0

                }
            ]
        });
        chart1.render();
    }
    var validarSoloNumeros = function(texto) {
        var comparar = '1234567890';
        for (var i = 0; i < texto.length; i++) {
            if (comparar.indexOf(texto.substring(i, i + 1)) == -1) {
                return 0;
            }
        }
        return 1;
    }
    var validarSololetras = function(texto) {
        var comparar = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZáéíóúÁÉÍÓÚ ';
        for (var i = 0; i < texto.length; i++) {
            if (comparar.indexOf(texto.substring(i, i + 1)) == -1) {
                return 0;
            }
        }
        return 1;
    }
    var validarVacioUser = function() {
        var rpta = 0;
        if (document.getElementById("txtapepat").value.length == 0) {
            toastr.warning("El apellido Paterno no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtapemat").value.length == 0) {
            toastr.warning("El apellido Materno no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtnombres").value.length == 0) {
            toastr.warning("El Nombre no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtcorreo").value.length == 0) {
            toastr.warning("El Correo Electronico no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtempresa").value.length == 0) {
            toastr.warning("La empresa no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtnro").value.length == 0) {
            toastr.warning("El numero de Documento no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if ($("#ddlrol").find(":selected").attr("value") == 0) {
            toastr.warning("Debe seleccionar un rol", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSoloNumeros(document.getElementById("txtnro").value) == 0) {
            toastr.warning("El documento solo puede contener numeros", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSololetras(document.getElementById("txtapepat").value) == 0) {
            toastr.warning("El apellido Paterno solo debe de contener letras", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSololetras(document.getElementById("txtnombres").value) == 0) {
            toastr.warning("El apellido Materno solo puede contener letras", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else {
            rpta = 1;
        }
        return rpta;

    }
    var validarVacioUserModify = function() {
        var rpta = 0;
        if (document.getElementById("txtapepatM").value.length == 0) {
            toastr.warning("El apellido Paterno no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtapematM").value.length == 0) {
            toastr.warning("El apellido Materno no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtnombresM").value.length == 0) {
            toastr.warning("El Nombre no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtcorreoM").value.length == 0) {
            toastr.warning("El Correo Electronico no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtnroM").value.length == 0) {
            toastr.warning("El numero de Documento no puede ir vacio", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSoloNumeros(document.getElementById("txtnroM").value) == 0) {
            toastr.warning("El documento solo puede contener numeros", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSololetras(document.getElementById("txtapepatM").value) == 0) {
            toastr.warning("El apellido Paterno solo debe de contener letras", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (validarSololetras(document.getElementById("txtnombresM").value) == 0) {
            toastr.warning("El apellido Materno solo puede contener letras", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else if (document.getElementById("txtclaveM").value.length == 0) {
            toastr.warning("La Contraseña no puede ir vacia", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
        } else {
            rpta = 1;
        }
        return rpta;

    }
    var defaultFilters1 = function() {
        if (filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiIni.length == 0) {
            filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiIni = $('#txtOxiIni').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiIni = $('#txtOxiIni').val() * 1000000;
        }
        if (filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiFin.length == 0) {
            filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiFin = $('#txtOxiFin').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtOxi').selected.txtOxiFin = $('#txtOxiFin').val() * 1000000;
        }

        if (filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosIni.length == 0) {
            filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosIni = $('#txtIngresosIni').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosIni = $('#txtIngresosIni').val() * 1000000;
        }
        if (filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosFin.length == 0) {
            filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosFin = $('#txtIngresosFin').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtIngresos').selected.txtIngresosFin = $('#txtIngresosFin').val() * 1000000;
        }

        if (filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilIni.length == 0) {
            filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilIni = $('#txtUtilIni').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilIni = $('#txtUtilIni').val() * 1000000;
        }
        if (filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilFin.length == 0) {
            filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilFin = $('#txtUtilFin').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtUtil').selected.txtUtilFin = $('#txtUtilFin').val() * 1000000;
        }


    }
    var defaultFiltersProyects = function() {
        if (filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoIni.length == 0) {
            filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoIni = $('#txtMontoIni').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoIni = $('#txtMontoIni').val() * 1000000;
        }
        if (filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoFin.length == 0) {
            filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoFin = $('#txtMontoFin').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtMontoAsignado').selected.txtMontoFin = $('#txtMontoFin').val() * 1000000;
        }


        if (filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLIni.length == 0) {
            filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLIni = $('#txtMontoCIPRLIni').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLIni = $('#txtMontoCIPRLIni').val() * 1000000;
        }
        if (filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLFin.length == 0) {
            filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLFin = $('#txtMontoCIPRLFin').attr('data-value') * 1000000;
        } else {
            filterArr('filterParams', 'key', 'txtMontoCIPRL').selected.txtMontoCIPRLFin = $('#txtMontoCIPRLFin').val() * 1000000;
        }



        if (filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionIni.length == 0) {
            filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionIni = $('#txtPoblacionIni').attr('data-value') * 1000;
        } else {
            filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionIni = $('#txtPoblacionIni').val() * 1000;
        }
        if (filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionFin.length == 0) {
            filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionFin = $('#txtPoblacionFin').attr('data-value') * 1000;
        } else {
            filterArr('filterParams', 'key', 'txtPoblacion').selected.txtPoblacionFin = $('#txtPoblacionFin').val() * 1000;
        }

    }

    var getObjetosUbigeo = function() {
        var url = "http://95.217.44.43:4000/getubigeosGeojson";
        axios.post(url, { tipo: document.getElementById("txtTipoFiltro").value, corredor: document.getElementById('txtCorredor').value })
            .then(function(response) {
                defaultFiltersProyects();
                var ddlNivel = filterArr('filterParams', 'key', 'ddlNivel').selected;
                var ddlFuncion = filterArr('filterParams', 'key', 'ddlFuncion').selected;
                if (ddlFuncion.length != 0 && ddlNivel.length != 0) {
                    setTimeout(function() {
                        loadProjects("iddist in (" + response.data.dist.join(',') + ") and " + filter('projects', 'ddlRegion'));
                        counterSum("iddist::integer in (" + response.data.dist.join(',') + ") and " + filter('projects', 'ddlRegion'));
                        mapaCalorPrueba("iddist::integer in (" + response.data.dist.join(',') + ") and " + filter('projects', 'ddlRegion'));
                    }, 100);
                    toastr.success("Capa de Proyectos y CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                } else {
                    toastr.success("Capa CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                }

                filterLayerGeojson(response.data.dpto, response.data.provincia, response.data.dist);

            })
            .catch(function(error) {
                console.log(error);
            });
    }
    var filterLayer2 = function() {
        var idCheckBox = $('input[type="radio"][name="txtradioPuesto"]:checked').attr('data-key');
        var txtCondition1 = '';
        switch (idCheckBox) {
            case 'txtradioPrimerLugar':
                txtCondition1 = ' and posicion=1';
                break;
            case 'txtradioSegundoLugar':
                txtCondition1 = ' and posicion=2';
                break;
            case 'txtradioTercerLugar':
                txtCondition1 = ' and posicion=3';
                break;
        }
        if (document.getElementById("txtLimaMML").checked == true && document.getElementById("txtLimaProvincia").checked == false) {
            lyrDraw([lyrRegionPolitica], "cod_ubigeo in (26)" + txtCondition1);
            lyrDraw([lyrProvPolitica], "codubigeo in ('1501')" + txtCondition1);
            lyrDraw([lyrDistPolitica], "cod_prov in ('1501')" + txtCondition1);
            loadPoliticaUbigeoInfo('vw_etiqueta_dpto', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrRegionPolitica, "codubigeo IN (26)");
            loadPoliticaUbigeoInfo('vw_etiqueta_prov', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrProvPolitica, "gid=113");
            loadPoliticaUbigeoInfo('vw_etiqueta_dist_20190411_1', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrDistPolitica, "codprov IN (1501)");
            map.setView([-12.0809386, -76.9273332], 10);
        }
        if (document.getElementById("txtLimaProvincia").checked == true && document.getElementById("txtLimaMML").checked == false) {
            lyrDraw([lyrRegionPolitica], "cod_ubigeo in (40)" + txtCondition1);
            lyrDraw([lyrProvPolitica], "codubigeo in ('1502','1503','1504','1505','1506','1507','1508','1509','1510')" + txtCondition1);
            lyrDraw([lyrDistPolitica], "cod_prov in ('1502','1503','1504','1505','1506','1507','1508','1509','1510')" + txtCondition1);
            loadPoliticaUbigeoInfo('vw_etiqueta_dpto', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrRegionPolitica, "codubigeo IN (40)");
            loadPoliticaUbigeoInfo('vw_etiqueta_prov', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrProvPolitica, "codubigeo IN ('1502','1503','1504','1505','1506','1507','1508','1509','1510')");
            loadPoliticaUbigeoInfo('vw_etiqueta_dist_20190411_1', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrDistPolitica, "codprov IN ('1502','1503','1504','1505','1506','1507','1508','1509','1510')");
            map.setView([-11.9770042, -76.8873656], 8);
        }
        if (document.getElementById("txtLimaProvincia").checked == true && document.getElementById("txtLimaMML").checked == true) {
            lyrDraw([lyrRegionPolitica], "cod_ubigeo in ('15')" + txtCondition1);
            lyrDraw([lyrProvPolitica], "cod_ubigeo in ('15')" + txtCondition1);
            lyrDraw([lyrDistPolitica], "cod_ubigeo in ('15')" + txtCondition1);
            loadPoliticaUbigeoInfo('vw_etiqueta_dpto', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrRegionPolitica, "codubigeo IN (15)");
            loadPoliticaUbigeoInfo('vw_etiqueta_prov', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrProvPolitica, "coddpto IN (15)");
            loadPoliticaUbigeoInfo('vw_etiqueta_dist_20190411_1', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrDistPolitica, "coddpto IN (15)");
            map.setView([-11.9770042, -76.8873656], 8);
        } else {
            var addTo = false;
            var ddlRegion = filterArr('filterParams', 'key', 'ddlRegion3').selected;
            var ddlProvincia = filterArr('filterParams', 'key', 'ddlProvincia2').selected;
            var ddlPartidosPoliticos = filterArr('filterParams', 'key', 'ddlPartidosPoliticos').selected;
            var infoPol = filterGroup('overLayers', 'basePolitica', 'infoPol');

            var txtCondition = '';
            if (ddlPartidosPoliticos.toString().length > 0) {
                txtCondition = " and partido_politico in (" + ddlPartidosPoliticos.join(',') + ")";
            }
            if (ddlRegion.length > 0 && ddlProvincia.length == 0) {
                lyrDraw([lyrRegionPolitica, lyrProvPolitica, lyrDistPolitica], "cod_ubigeo in (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_dpto', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrRegionPolitica, "codubigeo IN (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_prov', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrProvPolitica, "coddpto IN (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_dist_20190411_1', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrDistPolitica, "coddpto IN (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
            } else if (ddlRegion.length > 0 && ddlProvincia.length > 0) {
                lyrDraw([lyrRegionPolitica], "cod_ubigeo in (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
                lyrDraw([lyrProvPolitica], "codubigeo in (" + ddlProvincia.join(',') + ")" + txtCondition + txtCondition1);
                lyrDraw([lyrDistPolitica], "cod_prov in (" + ddlProvincia.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_dpto', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrRegionPolitica, "codubigeo IN (" + ddlRegion.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_prov', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrProvPolitica, "codprov IN (" + ddlProvincia.join(',') + ")" + txtCondition + txtCondition1);
                loadPoliticaUbigeoInfo('vw_etiqueta_dist_20190411_1', filterGroup('overLayers', 'basePolitica', 'infoPol').lyrDistPolitica, "codprov IN (" + ddlProvincia.join(',') + ")" + txtCondition + txtCondition1);
            }
        }
        if (map.hasLayer(infoPol.layer)) {
            addTo = true;
            map.removeLayer(infoPol.layer);
        }
        if (map.hasLayer(filterGroup('overLayers', 'basePolitica', 'regionPolitica').layer)) {
            infoPol.layer = infoPol.lyrRegionPolitica;
        } else if (map.hasLayer(filterGroup('overLayers', 'basePolitica', 'provinciaPolitica').layer)) {
            infoPol.layer = infoPol.lyrProvPolitica;
        } else if (map.hasLayer(filterGroup('overLayers', 'basePolitica', 'distritoPolitica').layer)) {
            infoPol.layer = infoPol.lyrDistPolitica;
        }

        if (addTo) infoPol.layer.addTo(map);
    }
    var loadPoliticaUbigeoInfo = function(layerName, group, cql_filter) {
        group.clearLayers();
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: layerName,
                        sql: cql_filter.length == 0 ? '' : cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {

                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/infog12.png" title="' + feature.properties.nomubigeo + '" border="0"/>', className: 'info' })
                        });
                        marker.bindPopup("", { closeButton: true, minWidth: 600 });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(group);
                    }
                });
                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;

                    var ubigeo = '';
                    var consulta = '';
                    if (obj.idnivel === 1) {
                        ubigeo = obj.coddpto;
                        consulta = 'ec.ubigeo_departamento and id_prov is null';
                    } else if (obj.idnivel === 2) {
                        ubigeo = obj.codubigeo;
                        consulta = 'ec.ubigeo_provincia and id_dist is null';
                    } else if (obj.idnivel === 3) {
                        ubigeo = obj.codubigeo;
                        consulta = 'ec.ubigeo_distrito and id_dist is not null';
                    }

                    var obj1 = {
                        method: 'chartpopUpTablaPolitica',
                        ubigeo: ubigeo,
                        consulta: consulta
                    };

                    $.get({
                        url: 'popupPoliticaVotos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            console.log(response);
                            e.layer._popup.setContent(response);

                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                success: function(response1) {
                                    var result = JSON.parse(response1);

                                    var tmp = [];
                                    var tmp1 = [];
                                    var porcentaje = JSON.parse(result.data[0].porcentaje);
                                    var resultado = JSON.parse(result.data[0].resultado);
                                    for (i = 0; i < porcentaje.length; i++) {
                                        tmp.push(parseFloat(porcentaje[i]));
                                    }
                                    for (x = 0; x < resultado.length; x++) {
                                        tmp1.push(parseInt(resultado[x]));
                                    }
                                    var objRegion = {
                                        div: 'chartpopupTabla',
                                        titulo: JSON.parse(result.data[0].partidos),
                                        data: tmp,
                                        data1: tmp1
                                    };
                                    chartDualAxesCongresos1(objRegion);
                                }
                            });
                        }
                    });


                });


            }

        });
    }
    var events = function(params) {
        switch (params.id) {
            case 'Presiden.':
                if (!params.checked) {
                    var id = $('input[type="radio"][name="Presiden."]:checked').attr('data-id');
                    var lyr = filterGroup('overLayers', 'basePolitica', id).layer;
                    var lyrBase = filterGroup('overLayers', 'basePolitica', 'infoPol').layer;
                    if (map.hasLayer(lyr)) {
                        map.removeLayer(lyr);
                        map.removeLayer(lyrBase);
                    }
                    if (map.hasLayer(lyrBase)) {
                        map.removeLayer(lyrBase);
                    }
                    $('input[name="Presiden."]').prop("checked", false);
                    $('input[data-id="infoPol"]').attr('disabled', 'disabled');
                    $(".legendPresidencial").css("display", "none");
                } else if (params.checked) {
                    $(".legendPresidencial").css("display", "");

                }
                break;
            case 'btnFiltrar3':
                $('input[data-id="regionPolitica"]').removeAttr('disabled');
                $('input[data-id="provinciaPolitica"]').removeAttr('disabled');
                $('input[data-id="distritoPolitica"]').removeAttr('disabled');
                $('input[id="Politica"]').removeAttr('disabled');
                filterLayer2();
                break;
            case 'btnLimpiar3':
                $('input[id="75"]').attr('disabled', 'disabled');
                $('input[id="76"]').attr('disabled', 'disabled');
                $('input[id="77"]').attr('disabled', 'disabled');
                $('input[id="78"]').attr('disabled', 'disabled');
                $(".legendPresidencial").css("display", "none");
                break;
            case 'changePerfil':
                var id = params.getAttribute('data-id');
                console.log(id);
                Swal.fire({
                    title: '¿Seguro que desea cambiar el Perfil del Usuario?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        var url = "http://95.217.44.43:4000/getChangePerfil";
                        axios.post(url, {
                                rol: $("#ddlrolChange_" + id).find(":selected").attr("value"),
                                id: id,
                            })
                            .then(function(response) {

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Correcto',
                                    text: response.data[0].msj,
                                });
                                $.post({
                                    url: 'divtablauser.php',
                                    success: function(response) {
                                        document.getElementById('divtablauser').innerHTML = response;
                                    }
                                });
                            })
                            .catch(function(error) {
                                console.log(error);
                            });

                    }
                });
                break;
            case 'saveDrawObjetos':
                Swal.fire({
                    title: 'Guardar Objeto',
                    text: '¿Seguro que desea guardar el siguiente Objeto?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire({
                            title: 'Ingrese Descripcion al Objeto',
                            input: 'text',
                            inputAttributes: {
                                autocapitalize: 'off'
                            },
                            showCancelButton: true,
                            confirmButtonText: 'Grabar',
                            showLoaderOnConfirm: true,

                        }).then((result) => {

                            if (result.isConfirmed) {
                                var obj = {};

                                if (objCircle != undefined) {

                                    objCircle.method = 'gen_objetos_circle';
                                    obj = objCircle;
                                }
                                if (objRectangle != undefined) {
                                    objRectangle.method = 'gen_objetos_rectangle';
                                    obj = objRectangle;
                                }
                                obj.descripcion = result.value;

                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        toastr.success(response.data[0].msj, "Correcto", { timeOut: 6000, closeButton: true, progressBar: true });
                                    }
                                });
                            }
                        })

                    }
                })
                break;
            case 'btnGenerateTwit':
                console.log("estoy aqui");
                var envio = $('#txtTextoTwit').val() + ' since:' + $('#txtDesdeTwit').val()
                axios.post('http://5.9.118.78:4000/getTwitter', { envio: envio })
                    .then(function(response) {
                        console.log(response);
                        var htmTable = `<table class="table table-detail">
                                        <tr class="table-dark">
                                            <th class="text-dark"><b>Twit</b></th>
                                            <th class="text-dark"><b>Usuario</b></th>
                                           
                                           
                                        </tr>`;
                        $.each(response.data.statuses, function(i, obj) {

                            htmTable += '<tr>';
                            htmTable += '<td style="text-align:center">' + obj.text + '</td>';
                            htmTable += '<td style="text-align:center">' + obj.user.name + '</td>';

                            htmTable += '</tr>';
                        });
                        htmTable += `</table>`;
                        $('#divTwit').html(htmTable);
                    }).catch(function(error) {
                        console.log(error);
                    });

                break;
            case 'btnFiltrar':
                clearLayers(clusters.projects);
                clearLayers(clusters.Mapas_de_Calor);
                filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                $('input[data-id="hospitales"]').removeAttr('disabled');
                if (document.getElementById("txtTipoFiltro").value == 'proyecto') {
                    var ddlRegion = filterArr('filterParams', 'key', 'ddlRegion').selected;
                    var ddlNivel = filterArr('filterParams', 'key', 'ddlNivel').selected;
                    var ddlFuncion = filterArr('filterParams', 'key', 'ddlFuncion').selected;
                    if (ddlRegion.length != 0) {
                        defaultFiltersProyects();
                        if (ddlNivel.length != 0 && ddlFuncion.length != 0) {
                            setTimeout(function() {
                                ddlRegion.forEach(function(row) {

                                    loadProjects("coddpto = " + row + " and " + filter('projects', 'ddlRegion'));
                                    mapaCalorPrueba("coddpto = " + row + " and " + filter('projects', 'ddlRegion'));
                                });
                                counterSum(filter('projects'));
                            }, 100);

                            toastr.success("Capa de Proyectos y CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                        } else {
                            toastr.success("Capa CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                        }
                        filterLayer();
                    } else {
                        toastr.error("Debe seleccionar como minimo una Región", "Filtros Incompletos", { timeOut: 2000, closeButton: true, progressBar: true });
                    }

                } else if (document.getElementById("txtTipoFiltro").value == 'drawObjeto') {

                    defaultFiltersProyects();
                    var ddlNivel = filterArr('filterParams', 'key', 'ddlNivel').selected;
                    var ddlFuncion = filterArr('filterParams', 'key', 'ddlFuncion').selected;
                    if (ddlFuncion.length != 0 && ddlNivel.length != 0) {
                        setTimeout(function() {

                            loadProjects('codproyecto in (' + drawCodproyect.join(',') + ') and ' + filter('projects'));

                            counterSum('codproyecto in (' + drawCodproyect.join(',') + ') and ' + filter('projects'));

                        }, 100);
                        toastr.success("Capa de Proyectos y CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                    } else {
                        toastr.success("Capa CIPRL ", "Aplicando Filtros", { timeOut: 6000, closeButton: true, progressBar: true });
                    }
                    filterLayerGeojson(tmpDrawFilter.region, tmpDrawFilter.provincia, tmpDrawFilter.distrito);
                    openNewDialog = true;


                } else {
                    getObjetosUbigeo();
                }
                break;
            case 'btnFiltrarProyectsCompanies':
                var id = params.getAttribute('data-event');
                var tmp = [];
                var query = "";
                var obj = {
                    method: 'getSnipForEmpresa',
                    empresa: id
                };

                $.getJSON({
                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        for (i = 0; i < response.data.length; i++) {

                            tmp.push("'" + response.data[i].snips + "'");
                        }
                        query = "(codigosnip in(" + tmp.join(',') + ") or codigounico in (" + tmp.join(',') + "))";
                        clearLayers(clusters.projects);
                        loadProjects(query);
                    }
                });
                break;
            case 'lnkCerrar':
                map.closePopup();
                break;

            case 'btnFiltrar2':

                layerGroupCirculo.clearLayers();

                filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                clearLayers(clusters.companies);

                defaultFilters1();
                var data = filterArr('filterParams', 'key', 'ddlRegion2').selected;
                if (filterArr('filterParams', 'key', 'txtRuc').selected.txtRuc.toString().length > 0) {
                    sqlempresas = "ruc = '" + filterArr('filterParams', 'key', 'txtRuc').selected.txtRuc + "'";
                } else if (filterArr('filterParams', 'key', 'txtRazonSocial').selected.txtRazonSocial.length > 0) {
                    sqlempresas = "empresa = " + filterArr('filterParams', 'key', 'txtRazonSocial').selected.txtRazonSocial;
                }
                filterArr('filterParams', 'key', 'ddlRegion2').selected = [];
                /*if (sqlempresas != undefined) {
                    loadCorredor1(sqlempresas);
                }
                else {
                    loadCorredor1("coddpto =2828");
                }*/
                circleEmpresa = {};
                console.log(filterArr('filterParams', 'key', 'txtRazonSocial').selected.txtRazonSocial.toString().length);
                if (data.length == 0) {
                    if (filterArr('filterParams', 'key', 'txtRazonSocial').selected.txtRazonSocial.toString().length != 0) {
                        loadCompanies(filter('companies'));
                        counterEmpresa(filter('companies'));
                        console.log(filter('companies'));
                    } else if (filterArr('filterParams', 'key', 'txtRuc').selected.txtRuc.toString().length != 0) {
                        loadCompanies(filter('companies'));
                        counterEmpresa(filter('companies'));
                        console.log(filter('companies'));
                    } else {
                        toastr.error("Debe seleccionar como minimo una Región o digitar un numero de RUC o ingresar una Razon Social", "Filtros Incompletos", { timeOut: 2000, closeButton: true, progressBar: true });
                    }



                } else {
                    data.forEach(function(row) {
                        loadCompanies("coddpto = " + row + " and " + filter('companies'));


                    });
                    counterEmpresa(filter('companies'));
                }





                if (!map.hasLayer(filterGroup('overLayers', 'baseLayer', 'empresas').layer)) {
                    map.addLayer(filterGroup('overLayers', 'baseLayer', 'empresas').layer);
                }
                filterArr('filterParams', 'key', 'ddlRegion2').selected = data;


                break;

            case 'btnLimpiar':
                document.getElementById("txtTipoFiltro").value = "proyecto";
                document.getElementById('txtCorredor').value = "";
                clearLayers(clusters.projects);
                clearLayers(clusters.Mapas_de_Calor);
                filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                counterSum("codfuncionproyecto=160");
                $('input[data-id="hospitales"]').attr('disabled', 'disabled');
                $('input[data-id="hospitales"]').prop("checked", false);
                break;
            case 'btnLimpiar2':
                $('#counterEmpresa').html("");
                if (layerGroupCirculo != undefined) {
                    layerGroupCirculo.clearLayers();
                }
                filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                clearLayers(clusters.companies);
                break;
            case 'agregarUsuario':
                var rpta = validarVacioUser();
                if (rpta != 0) {
                    const url = "http://95.217.44.43:4000/createuser";
                    axios.post(url, {
                            ape_pat: document.getElementById("txtapepat").value,
                            ape_mat: document.getElementById("txtapemat").value,
                            nombres: document.getElementById("txtnombres").value,
                            correo: document.getElementById("txtcorreo").value,
                            empresa: document.getElementById("txtempresa").value,
                            nro: document.getElementById("txtnro").value,
                            rol: $("#ddlrol").find(":selected").attr("value")
                        })
                        .then(function(response) {
                            if (response.data[0].rpta == 0) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error Al guardar',
                                    text: response.data[0].msj,
                                });
                            } else {
                                document.getElementById("txtapepat").value = "";
                                document.getElementById("txtapemat").value = "";
                                document.getElementById("txtnombres").value = "";
                                document.getElementById("txtcorreo").value = "";
                                document.getElementById("txtempresa").value = "";
                                document.getElementById("txtnro").value = "";
                                Swal.fire({
                                    title: 'Correcto',
                                    text: response.data[0].msj,
                                    icon: 'success',
                                });
                                $.post({
                                    url: 'divtablauser.php',
                                    success: function(response) {
                                        document.getElementById('divtablauser').innerHTML = response;
                                    }
                                });
                            }
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                }
                break;
            case 'enviarCorreoUser':
                var id = params.getAttribute('data-id');
                var array = id.split('$');
                Swal.fire({
                    title: 'Enviar Clave',
                    text: 'Seguro que desea reenviar la clave a' + array[0],
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        var url = "http://95.217.44.43:4000/usersPwd";
                        axios.post(url, {
                                correo: array[0],
                                id: array[1],
                            })
                            .then(function(response) {
                                Swal.fire({
                                    title: 'Correcto',
                                    text: "contraseña enviada por favor verifique en el siguiente correo: " + array[0],
                                    icon: 'success',
                                });
                            })
                            .catch(function(error) {
                                console.log(error);
                            });
                    }
                })
                break;
            case 'showpwd':
                if (document.getElementById("txtclaveM").type === 'password') {
                    document.getElementById("txtclaveM").setAttribute('type', 'text');
                } else {
                    document.getElementById("txtclaveM").setAttribute('type', 'password');
                }
                break;
            case 'modificarUsuario':
                var rpta = validarVacioUserModify();
                if (rpta != 0) {
                    Swal.fire({
                        title: 'Modificar Usuario',
                        text: 'Seguro que desea Modificar el Usuario',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'Aceptar'
                    }).then((result) => {
                        console.log("enviar a bd");
                        if (result.value) {
                            var url = "http://95.217.44.43:4000/userModify";
                            axios.post(url, {
                                    ape_pat: document.getElementById("txtapepatM").value,
                                    ape_mat: document.getElementById("txtapematM").value,
                                    nombres: document.getElementById("txtnombresM").value,
                                    correo: document.getElementById("txtcorreoM").value,
                                    clave: document.getElementById("txtclaveM").value,
                                    nro: document.getElementById("txtnroM").value,
                                    id: document.getElementById("txtiduser").value
                                })
                                .then(function(response) {
                                    if (response.data[0].rpta == 0) {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Error Al guardar',
                                            text: response.data[0].msj,
                                        });
                                    } else {
                                        Swal.fire({
                                            title: 'Correcto',
                                            text: response.data[0].msj,
                                            icon: 'success',
                                        });
                                        if ($("#txtapepat").val() != undefined) {
                                            $.post({
                                                url: 'divtablauser.php',
                                                success: function(response) {
                                                    document.getElementById('divtablauser').innerHTML = response;
                                                }
                                            });
                                        }
                                    }

                                })
                                .catch(function(error) {
                                    console.log(error);
                                });
                        }
                    })
                }
                break;
            case 'changeestadouser':
                var id = params.getAttribute('data-id');
                var array = id.split('$');
                Swal.fire({
                    title: array[2] == 1 ? "Desactivar" + ' cuenta' : "Activar" + ' cuenta',
                    text: array[2] == 1 ? 'Seguro que desea desactivar cuenta de ' + array[0] : 'Seguro que desea activar cuenta de ' + array[0],
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Aceptar'
                }).then((result) => {
                    if (result.value) {
                        var url = "http://95.217.44.43:4000/userActive";
                        axios.post(url, {
                                correo: array[0],
                                id: array[1],
                                estado: array[2]
                            })
                            .then(function(response) {
                                Swal.fire({
                                    icon: 'success',
                                    title: response.data[0].rpta == 1 ? 'Usuario Inactivo' : 'Usuario Activo',
                                    text: response.data[0].msj,
                                });
                                $.post({
                                    url: 'divtablauser.php',
                                    success: function(response) {
                                        document.getElementById('divtablauser').innerHTML = response;
                                    }
                                });
                            })
                            .catch(function(error) {
                                console.log(error);
                            });

                    } else {

                        $.post({
                            url: 'divtablauser.php',
                            success: function(response) {
                                document.getElementById('divtablauser').innerHTML = response;
                            }
                        });
                    }
                });
                break;
            case 'CIPRL':
                if (!params.checked) {
                    var id = $('input[type="radio"][name="CIPRL"]:checked').attr('data-id');

                    var lyr = filterGroup('overLayers', 'baseUbigeo', id).layer;
                    var lyrBase = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').layer;

                    if (map.hasLayer(lyr)) {
                        map.removeLayer(lyr);
                        map.removeLayer(lyrBase);
                    }

                    if (map.hasLayer(lyrBase)) {
                        map.removeLayer(lyrBase);
                    }

                    $('input[name="CIPRL"]').prop("checked", false);
                    $('input[data-id="infoCIPRL"]').attr('disabled', 'disabled');
                }
                break;
            case 'COVID-19':
                if (!params.checked) {
                    var id = $('input[type="radio"][name="COVID-19"]:checked').attr('data-id');
                    var lyr = filterGroup('overLayers', 'baseMinsa', id).layer;
                    var lyrBase = filterGroup('overLayers', 'baseMinsa', 'infoMinsa').layer;
                    if (map.hasLayer(lyr)) {
                        map.removeLayer(lyr);
                        map.removeLayer(lyrBase);
                    }

                    if (map.hasLayer(lyrBase)) {
                        map.removeLayer(lyrBase);
                    }
                    $('input[name="COVID-19"]').prop("checked", false);
                    $('input[data-id="infoMinsa"]').attr('disabled', 'disabled');
                    $(".legendSalud").css("display", "none");
                } else if (params.checked) {
                    $(".legendSalud").css("display", "");

                }
                break;
            case 'SINADEF':
                if (!params.checked) {
                    var id = $('input[type="radio"][name="SINADEF"]:checked').attr('data-id');
                    var lyrBase = filterGroup('overLayers', 'baseSinadef', 'infoSinadef').layer;
                    var lyr = filterGroup('overLayers', 'baseSinadef', id).layer;

                    if (map.hasLayer(lyr)) {
                        map.removeLayer(lyr);
                        map.removeLayer(lyrBase);
                    }

                    if (map.hasLayer(lyrBase)) {
                        map.removeLayer(lyrBase);
                    }

                    $(".legendSaludSinadef").css("display", "none");
                    $('input[name="SINADEF"]').prop("checked", false);
                    $('input[data-id="infoSinadef"]').attr('disabled', 'disabled');
                } else if (params.checked) {
                    $(".legendSaludSinadef").css("display", "");

                }
                break;
        }
        var changeTabPanelEstadistica = function(params) {
            $('#macro-tab').removeClass('active');
            $('#ciprl-tab').removeClass('active');
            $('#cannon-tab').removeClass('active');
            $('#minsa-tab').removeClass('active');
            $('#sinadef-tab').removeClass('active');
            $('#proyectado-tab').removeClass('active');
            $('#empresas-tab').removeClass('active');
            document.getElementById('macro-tab').style.color = "black";
            document.getElementById('ciprl-tab').style.color = "black";
            document.getElementById('cannon-tab').style.color = "black";
            document.getElementById('minsa-tab').style.color = "black";
            document.getElementById('sinadef-tab').style.color = "black";
            document.getElementById('proyectado-tab').style.color = "black";
            document.getElementById('empresas-tab').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
        }
        var changeTabPanelSuibEstadistica = function(params) {
            $('#fallecidos-tab').removeClass('active');
            $('#resumen-tab').removeClass('active');
            $('#fallecidospob-tab').removeClass('active');
            document.getElementById('fallecidos-tab').style.color = "black";
            document.getElementById('resumen-tab').style.color = "black";
            document.getElementById('fallecidospob-tab').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
        }
        var changeTabPanelOxi = function(params) {
            $('#empresas-tab').removeClass('active');
            $('#regiones-tab').removeClass('active');
            $('#provincias-tab').removeClass('active');
            $('#distritos-tab').removeClass('active');
            $('#ministerios-tab').removeClass('active');
            $('#universidades-tab').removeClass('active');
            $('#politica-tab').removeClass('active');
            $('#proyecto-tab').removeClass('active');
            $('#oportunidad-tab').removeClass('active');
            document.getElementById('empresas-tab').style.color = "black";
            document.getElementById('regiones-tab').style.color = "black";
            document.getElementById('provincias-tab').style.color = "black";
            document.getElementById('distritos-tab').style.color = "black";
            document.getElementById('ministerios-tab').style.color = "black";
            document.getElementById('universidades-tab').style.color = "black";
            document.getElementById('politica-tab').style.color = "black";
            document.getElementById('oportunidad-tab').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
        }

        if (params.classList.contains('macroKeys')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: 'divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('getDivEmpresasEstadistica').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('macroKeysFuncion')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: 'divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('getDivEmpresasEstadistica1').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('lnkAmpliar5')) {

            clickTablaId = params.getAttribute('data-event');
            var div = document.getElementById("idconsolidadoProyectos").style.display = "";

            var obj = {
                nivel: clickTablaId.substring(14),

                tipo: 'cantidad',
                checked: 1
            };




            if (clickTablaId === 'lnkProvXrutas_1') {
                $.post({
                    url: 'divConsolidadoProyectosDet.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('idconsolidadoProyectos').innerHTML = response;
                    }
                });

            } else if (clickTablaId === 'lnkProvXrutas_5') {
                $.post({
                    url: 'divConsolidadoProyectosDet.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('idconsolidadoProyectos').innerHTML = response;
                    }
                });

            } else if (clickTablaId === 'lnkProvXrutas_2') {
                $.post({
                    url: 'divConsolidadoProyectosDet.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('idconsolidadoProyectos').innerHTML = response;
                    }
                });

            }

        }
        //resumen de información
        if (params.classList.contains('tabChangeOxi')) {
            var id = params.getAttribute('id');
            changeTabPanelOxi(id);
            var obj = {
                mostrar: id,
            };
            switch (id) {
                case 'empresas-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            var obj = {
                                method: 'chartPieEmpresaOxi'
                            }

                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    var data = [
                                        { 'name': 'Top 10 Empresas', 'y': parseInt(response.data[0].top10), 'selected': true },
                                        { 'name': 'Resto de Empresas', 'y': parseInt(response.data[0].total) }
                                    ];
                                    var series = [{
                                        name: 'Empresas',
                                        colorByPoint: true,
                                        data: data
                                    }];
                                    var charpieobj = charPie2;
                                    charpieobj.series = series;
                                    Highcharts.chart('chartContainerEmpresas', charpieobj);
                                    $.post({
                                        url: 'divGeolocalizacionEmpresas.php',
                                        success: function(response) {
                                            document.getElementById('idGeoempresas').innerHTML = response;
                                            map2 = L.map('map2').setView([-9.33, -74.44], 5);
                                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                            }).addTo(map2);
                                            map2.invalidateSize();
                                            loadGeoEmpresas('');
                                            legendMap1();
                                            var obj = {
                                                titulo: 'Top 10 empresas Millones de soles',
                                                rbtnTabla: 1,
                                                desdeLimite: 2,
                                                limitFin: 10,
                                                limitInicio: 0
                                            }
                                            tablaTopEmpresasOxi(obj);
                                            stackedBarTopEmpresasOxi(obj);
                                            var obj1AdjCon = {
                                                method: 'chartStakedEmpresas',
                                                limitFin: 10,
                                                limitInicio: 0
                                            };
                                            empresasTableChangeAdjudicados(obj1AdjCon);

                                        }
                                    });
                                }
                            });
                        }
                    });

                    break;
                    //region ri
                case 'regiones-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            ////Ranking de Gobiernos Regionales (GR) por monto de inversión en obras por impuestos 2009 - 2020 torta
                            var obj = {
                                method: 'cirplxMontoOxi'
                            }
                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    var categorias = [];
                                    var objCiprl = {
                                        name: 'CIPRL',
                                        data: []
                                    }
                                    var objOxi = {
                                        name: 'OXI',
                                        data: []
                                    }
                                    for (i = 0; i < response.data.length; i++) {
                                        categorias.push(response.data[i].ano);
                                        objCiprl.data.push(Number((parseInt(response.data[i].ciprl)).toFixed(2)));
                                        objOxi.data.push(Number((parseInt(response.data[i].oxi)).toFixed(2)));
                                    }
                                    var total = [objCiprl, objOxi];
                                    var multibar1 = lineChart;
                                    multibar1.xAxis.categories = categorias;
                                    multibar1.title.text = 'Evolución CIPRL y monto utilizado en Gobiernos Regionales';
                                    multibar1.series = total;
                                    multibar1.subtitle.text = '(millones de soles)';
                                    /////grafico
                                    Highcharts.chart('regionOxiCiprl', multibar1);
                                    var data = [
                                        { 'name': 'G.R. con OXI', 'y': 70.71, 'selected': true },
                                        { 'name': 'G.R. sin OXI', 'y': 29.29 }
                                    ];
                                    var series = [{
                                        name: 'Oxi',
                                        colorByPoint: true,
                                        data: data
                                    }];
                                    var charpieobj = charPie2;
                                    charpieobj.series = series;
                                    Highcharts.chart('chartContainerGobiernosRegionales1', charpieobj);
                                    var data = [
                                        { 'name': 'G.R. con OXI', 'y': 32.43, 'selected': true },
                                        { 'name': 'G.R. sin OXI', 'y': 67.57 }
                                    ];
                                    var series = [{
                                        name: 'Empresas',
                                        colorByPoint: true,
                                        data: data
                                    }];
                                    var charpieobj1 = charPie2;
                                    charpieobj1.series = series;
                                    Highcharts.chart('chartContainerGobiernosRegionales2', charpieobj1);
                                }
                            });
                            var objCiprl = {
                                tipo: 2
                            };
                            $.post({
                                url: 'divCiprlGraficoEntidades.php?data=' + encodeURIComponent(JSON.stringify(objCiprl)),
                                success: function(response) {
                                    document.getElementById('idTablaciprlGraficoRegion').innerHTML = response;
                                    var objChart = {
                                        div: 'idTablaciprlGraficoProvinciasGraf',
                                        data: [19.82, 19.31, 20.55, 20.05, 20.28],
                                        data2: [2.15, 3.76, 6.99, 12.37, 74.73]
                                    };
                                    chartDivCIPRL(objChart);
                                }
                            });
                            var objTabla = {
                                estado: 'Adjudicado'
                            };
                            $.post({
                                url: 'divtablaGobiernosRegionales.php?data=' + encodeURIComponent(JSON.stringify(objTabla)),
                                success: function(response) {
                                    document.getElementById('idtablaRegionesAdjudicadas').innerHTML = response;
                                }
                            });
                            var objStakedAdjudicado = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Adjudicado',
                                tabla: 'obs.gen_regiones_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedAdjudicado)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerGobiernosRegionales4',
                                        titulo: 'Monto de Proyecto de Inversión OxI adjudicados por Gobiernos Regionales',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                            var objTabla = {
                                estado: 'Concluido'
                            };
                            $.post({
                                url: 'divtablaGobiernosRegionales.php?data=' + encodeURIComponent(JSON.stringify(objTabla)),
                                success: function(response) {
                                    document.getElementById('idtablaRegionesConcluidas').innerHTML = response;
                                }
                            });

                            var objStakedConcluido = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Concluido',
                                tabla: 'obs.gen_regiones_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedConcluido)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerGobiernosRegionales3',
                                        titulo: 'Monto de Proyecto de Inversión OxI concluida por Gobiernos Regionales',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });

                        }
                    });
                    break;
                    //provincia ri
                case 'provincias-tab':
                    console.log(obj);
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            var objCiprl = {
                                tipo: 2
                            };
                            $.post({
                                url: 'divCiprlGraficoEntidades.php?data=' + encodeURIComponent(JSON.stringify(objCiprl)),
                                success: function(response) {
                                    document.getElementById('idTablaciprlGraficoProvincias').innerHTML = response;
                                    var objChart = {
                                        div: 'idTablaciprlGraficoProvinciasGraf',
                                        data: [19.82, 19.31, 20.55, 20.05, 20.28],
                                        data2: [2.15, 3.76, 6.99, 12.37, 74.73]
                                    };
                                    chartDivCIPRL(objChart);
                                }
                            });

                            //debemodificarse
                            chartCIPRLbarTemporal1();
                            var objChartpieConcluido = {
                                div: 'chartContainerProvinciasConcluidas',
                                name1: 'Con OxI 28 MP - S/. 587M',
                                name2: 'Sin OxI 168 MP - S/. 0M',
                                y1: 14.28,
                                y2: 85.72
                            };
                            var objChartpieAdjudicado = {
                                div: 'chartContainerProvinciasAdjudicadas',
                                name1: 'Con OxI 22 MP - S/. 266M',
                                name2: 'Sin OxI 174 MP S/. 0M',
                                y1: 11.22,
                                y2: 88.78
                            };
                            chartPieExcel(objChartpieConcluido);
                            chartPieExcel(objChartpieAdjudicado);
                            var objTabla = {
                                estado: 'Adjudicado'
                            };
                            $.post({
                                url: 'divtablaMunicipalidadesProvinciales.php?data=' + encodeURIComponent(JSON.stringify(objTabla)),
                                success: function(response) {
                                    document.getElementById('idtablaProvinciasAdjudicadas').innerHTML = response;
                                }
                            });
                            var objStakedAdjudicado = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Adjudicado',
                                tabla: 'obs.gen_provincias_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedAdjudicado)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerProvincialesStakedAdjudicado',
                                        titulo: 'Monto de Proyecto de Inversión OxI adjudicados por Municipalidades Provinciales',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                            var objTablaConcluida = {
                                estado: 'Concluido'
                            };
                            $.post({
                                url: 'divtablaMunicipalidadesProvinciales.php?data=' + encodeURIComponent(JSON.stringify(objTablaConcluida)),
                                success: function(response) {
                                    document.getElementById('idtablaProvinciasConcluidas').innerHTML = response;
                                }
                            });
                            var objStakedConcluido = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Concluido',
                                tabla: 'obs.gen_provincias_oxi_excel',

                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedConcluido)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerProvincialesStakedConcluido',
                                        titulo: 'Monto de Proyecto de Inversión OxI concluida por Municipalidad Provincial',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                        }
                    });



                    break;
                case 'distritos-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            var objCiprl = {
                                tipo: 3
                            };
                            $.post({
                                url: 'divCiprlGraficoDistrito.php?data=' + encodeURIComponent(JSON.stringify(objCiprl)),
                                success: function(response) {
                                    document.getElementById('idTablaciprlGraficoDistritos').innerHTML = response;
                                    var objChart = {
                                        div: 'idTablaciprlGraficoDistritosGraf',
                                        data: [19.92, 19.91, 20.15, 19.99, 20.02],
                                        data2: [0.60, 2.19, 5.31, 11.65, 80.25]
                                    };
                                    chartDivCIPRL(objChart);
                                }
                            });
                            chartCIPRLbarTemporal2();
                            var objChartpieConcluido = {
                                div: 'chartContainerDistritosConcluidas',
                                name1: 'Con OxI 96 MD - S/. 994M',
                                name2: 'Sin OxI 1778 MD - S/. 0M',
                                y1: 1.76,
                                y2: 98.24
                            };
                            var objChartpieAdjudicado = {
                                div: 'chartContainerDistritalesAdjudicadas',
                                name1: 'Con OxI 33 MD - S/. 444M',
                                name2: 'Sin OxI 1841 MP S/. 0M',
                                y1: 1.76,
                                y2: 98.24
                            };
                            chartPieExcel(objChartpieConcluido);
                            chartPieExcel(objChartpieAdjudicado);
                            var objTabla = {
                                estado: 'Adjudicado'
                            };
                            $.post({
                                url: 'divtablaMunicipalidadDistrital.php?data=' + encodeURIComponent(JSON.stringify(objTabla)),
                                success: function(response) {
                                    document.getElementById('idtablaDistritosAdjudicadas').innerHTML = response;
                                }
                            });
                            var objStakedAdjudicado = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Adjudicado',
                                tabla: 'obs.gen_distritos_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedAdjudicado)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerDistritalesStakedAdjudicado',
                                        titulo: 'Monto de Proyecto de Inversión OxI adjudicados por Municipalidades Distritales',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                            var objTablaConcluida = {
                                estado: 'Concluido'
                            };
                            $.post({
                                url: 'divtablaMunicipalidadDistrital.php?data=' + encodeURIComponent(JSON.stringify(objTablaConcluida)),
                                success: function(response) {
                                    document.getElementById('idtablaDistritosConcluidas').innerHTML = response;
                                }
                            });
                            var objStakedConcluido = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Concluido',
                                tabla: 'obs.gen_distritos_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedConcluido)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerDistritalesStakedConcluido',
                                        titulo: 'Monto de Proyecto de Inversión OxI concluida por Municipalidad Distrital',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });

                        }
                    });
                    break;
                case 'ministerios-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            var objChartpie = {
                                div: 'chartContainerMinisterios1',
                                titulo: 'Ministerios con estado Concluido por Monto de Inversion',
                                name1: 'Con OxI 2 MIN - S/. 9.4M',
                                name2: 'Sin OxI 17 MIN - S/. 0M',
                                y1: 10.52,
                                y2: 89.48
                            };
                            var objChartpie1 = {
                                div: 'chartContainerMinisterios',
                                titulo: 'Ministerios con estado Ajudicado por Monto de Inversion',
                                name1: 'Con Oxi 7 MIN - S/. 640M',
                                name2: 'Sin Oxi 12 MIN S/. 0M',
                                y1: 36.84,
                                y2: 63.16
                            };
                            chartPieExcel(objChartpie);
                            chartPieExcel(objChartpie1);
                            var objStakedAdjudicado = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Adjudicados',
                                tabla: 'obs.gen_ministerios_oxi_excel',
                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedAdjudicado)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerMinisterios2',
                                        titulo: 'Monto de Proyecto de Inversión OxI adjudicados por Ministerios',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                            var objStakedConcluido = {
                                method: 'chartStakedBar',
                                tipo: 'Gobiernos',
                                estado: 'Concluido',
                                tabla: 'obs.gen_ministerios_oxi_excel',

                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedConcluido)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerMinisterios3',
                                        titulo: 'Monto de Proyecto de Inversión OxI concluidas por Ministerios',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                        }
                    });
                    break;
                case 'universidades-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            chartCIPRLbarTemporal3();
                            var objStakedConcluido = {
                                method: 'chartStakedBar',
                                tipo: 'Universidades',

                            };
                            $.get({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objStakedConcluido)),
                                success: function(response) {
                                    var result = JSON.parse(response);

                                    var objChart = {
                                        div: 'chartContainerUniversidades1',
                                        titulo: 'Monto de Proyecto de Inversión OxI concluidas por Universidades',
                                        intervalo: 25,
                                        datatitulo: result.data.datatitulo,
                                        data2009: result.data.data2009,
                                        data2010: result.data.data2010,
                                        data2011: result.data.data2011,
                                        data2012: result.data.data2012,
                                        data2013: result.data.data2013,
                                        data2014: result.data.data2014,
                                        data2015: result.data.data2015,
                                        data2016: result.data.data2016,
                                        data2017: result.data.data2017,
                                        data2018: result.data.data2018,
                                        data2019: result.data.data2019
                                    }
                                    chartStakedBar(objChart);
                                }
                            });
                            var objChartpie1 = {
                                div: 'chartContainerUniversidades',
                                titulo: 'Universidades con estado Ajudicado por Monto de Inversion',
                                name1: 'Con Oxi 3 UN - S/. 30.81M',
                                name2: 'Sin Oxi 48 UN S/. 0M',
                                y1: 5.89,
                                y2: 94.11
                            };
                            chartPieExcel(objChartpie1);
                        }
                    });
                    break;
                case 'politica-tab':
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            map33 = L.map('map3', { zoomControl: false, condensedAttributionControl: false }).setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map33);
                            map33.invalidateSize();
                            loadCongreso('1=1');
                            var objCon = {
                                limit: 20
                            };
                            $.post({
                                url: 'divCongresistas.php?data=' + encodeURIComponent(JSON.stringify(objCon)),
                                success: function(response) {
                                    document.getElementById('divCongresistas').innerHTML = response;
                                    var objphp = {
                                        method: 'chartMultipleAxisCongreso'
                                    };
                                    $.get({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                        success: function(response1) {

                                            var result = JSON.parse(response1);
                                            var tmp = [];
                                            var tmp1 = [];
                                            var tmp2 = [];
                                            var tmp3 = [];
                                            var partido = JSON.parse(result.data[0].partido);
                                            var electores = JSON.parse(result.data[0].electores);
                                            var congresistas = JSON.parse(result.data[0].congresistas);
                                            var region = JSON.parse(result.data[0].region);
                                            for (i = 0; i < partido.length; i++) {
                                                tmp.push((partido[i]));
                                            }
                                            for (i = 0; i < electores.length; i++) {
                                                tmp1.push(parseInt(electores[i]));
                                            }
                                            for (x = 0; x < congresistas.length; x++) {
                                                tmp2.push(parseInt(congresistas[x]));
                                            }
                                            for (x = 0; x < region.length; x++) {
                                                tmp3.push(parseInt(region[x]));
                                            }
                                            var objRegion = {
                                                titulo: 'Tabla de Congresistas, Regiones y Electores por Partido Político',
                                                partido: tmp,
                                                electores: tmp1,
                                                congresistas: tmp2,
                                                region: tmp3
                                            };
                                            chartMultipleAxisCongreso(objRegion);
                                        }
                                    });
                                }
                            });

                        }
                    });
                    break;
                case 'proyecto-tab':
                    var objCRPD = {
                        tipo: 'cantidad',
                        checked: 1
                    }
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            $.post({
                                url: 'divConsolidadoProyectos.php?data=' + encodeURIComponent(JSON.stringify(objCRPD)),
                                success: function(response) {
                                    document.getElementById('proyectosrpd').innerHTML = response;
                                    var objCRPD2 = {
                                        method: 'chartTablaproyectos'

                                    };

                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objCRPD2)),
                                        success: function(response) {
                                            console.log(response);
                                            var tmp1 = [];
                                            var tmp2 = [];
                                            var tmp3 = [];
                                            var entidades = JSON.parse(response.data[0].entidad);
                                            var proyectos = JSON.parse(response.data[0].proyectos);
                                            var montos = JSON.parse(response.data[0].monto);

                                            for (i = 0; i < entidades.length; i++) {
                                                tmp1.push(entidades[i].substring(9, 20));
                                            }
                                            for (i = 0; i < proyectos.length; i++) {
                                                tmp2.push(parseInt(proyectos[i]));
                                            }
                                            for (i = 0; i < montos.length; i++) {
                                                tmp3.push(parseInt(montos[i]) / 1000000);
                                            }
                                            Highcharts.chart('chartTablaproyectos', {
                                                chart: {
                                                    zoomType: 'xy'
                                                },
                                                title: {
                                                    text: 'Consolidado general según niveles de gobierno'
                                                },
                                                subtitle: {
                                                    text: ''
                                                },
                                                xAxis: [{
                                                    categories: tmp1,
                                                    crosshair: true
                                                }],
                                                yAxis: [{ // Primary yAxis
                                                    labels: {
                                                        format: '{value}',
                                                        style: {
                                                            color: Highcharts.getOptions().colors[1]
                                                        }
                                                    },
                                                    title: {
                                                        text: 'N° PROYECTOS',
                                                        style: {
                                                            color: Highcharts.getOptions().colors[1]
                                                        }
                                                    }
                                                }, { // Secondary yAxis
                                                    title: {
                                                        text: 'MONTO S/. (millones)',
                                                        style: {
                                                            color: Highcharts.getOptions().colors[0]
                                                        }
                                                    },
                                                    labels: {
                                                        format: '{value}',
                                                        style: {
                                                            color: Highcharts.getOptions().colors[0]
                                                        }
                                                    },
                                                    opposite: true
                                                }],
                                                tooltip: {
                                                    shared: true
                                                },
                                                legend: {
                                                    layout: 'vertical',
                                                    align: 'left',
                                                    x: 120,
                                                    verticalAlign: 'top',
                                                    y: 100,
                                                    floating: true,
                                                    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
                                                        'rgba(255,255,255,0.25)'
                                                },
                                                series: [{
                                                    name: 'Monto',
                                                    type: 'column',
                                                    yAxis: 1,
                                                    data: tmp3,
                                                    tooltip: {
                                                        valueSuffix: ''
                                                    }

                                                }, {
                                                    name: 'Proyectos',
                                                    type: 'spline',
                                                    data: tmp2,
                                                    tooltip: {
                                                        valueSuffix: ''
                                                    }
                                                }]
                                            });


                                        }

                                    });
                                }
                            });

                        }
                    });
                    break;
                case 'oportunidad-tab':
                    var objCRPD = {
                        tipo: 'cantidad',
                        checked: 1
                    }
                    $.post({
                        url: 'divresumenInformacion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divresumenoxi').innerHTML = response;
                            var objOport = {
                                tipo1: 'funcion',
                                checked1: 3

                            }
                            $.post({
                                url: 'divConsolidadoProyectosOpo.php?data=' + encodeURIComponent(JSON.stringify(objOport)),
                                success: function(response) {
                                    document.getElementById('proyectosopo').innerHTML = response;
                                    var objphp2 = {
                                        method: "consolidadototalOportunidades"
                                    };
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp2)),
                                        success: function(response) {

                                            console.log(response.data[0]);
                                            var tmp = [];
                                            var quintil = JSON.parse(response.data[0].proyecto);
                                            var tmp1 = [];
                                            var partidos = JSON.parse(response.data[0].monto);
                                            var tmp2 = [];
                                            var votantes = JSON.parse(response.data[0].montof15);
                                            var tmp3 = [];
                                            var autoridades = JSON.parse(response.data[0].proyectyof15);

                                            for (i = 0; i < quintil.length; i++) {
                                                tmp.push((quintil[i]));
                                            }
                                            for (i = 0; i < partidos.length; i++) {
                                                tmp1.push(parseInt(partidos[i]));
                                            }
                                            for (x = 0; x < votantes.length; x++) {
                                                tmp2.push(parseInt(votantes[x]));
                                            }
                                            for (x = 0; x < autoridades.length; x++) {
                                                tmp3.push(parseInt(autoridades[x]));
                                            }
                                            var objMuni = {
                                                titulo: 'Tabla Oportunidades con perfil viable y expediente tecnico',
                                                entidades: JSON.parse(response.data[0].nom),
                                                cantidad: tmp,
                                                cantidad1: tmp3,
                                                monto: tmp2,
                                                monto1: tmp1
                                            };
                                            chartMultipleAxis(objMuni);
                                        }
                                    });





                                    $.post({
                                        url: 'divConsolidadoProyectosOportunidades.php?data=' + encodeURIComponent(JSON.stringify(objOport)),
                                        success: function(response) {
                                            document.getElementById('idconsolidadoOportunidades').innerHTML = response;
                                        }
                                    });
                                }

                            });

                        }
                    });
                    break;

            }

        }
        var chartMultipleAxis = function(obj) {
            Highcharts.chart('idconsolidadoOportunidadesGrafico', {
                chart: {
                    zoomType: 'xy'
                },
                title: {
                    text: obj.titulo,
                    align: 'center'
                },
                subtitle: {
                    text: '',
                    align: 'left'
                },
                xAxis: [{
                    categories: obj.entidades,
                    crosshair: true
                }],
                yAxis: [{ // Primary yAxis
                    labels: {
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Monto de Perfil Viable',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true

                }, { // Secondary yAxis
                    gridLineWidth: 2,
                    title: {
                        text: 'Cantidad de Proyectos',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {

                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }

                }, { // Tertiary yAxis
                    gridLineWidth: 2,
                    title: {
                        text: 'Monto de Expediente Tecnico',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {

                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    x: 100,
                    verticalAlign: 'top',
                    y: 85,
                    floating: true,
                    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
                        'rgba(255,255,255,0.25)'
                },

                series: [{
                        name: 'Cantidad (P.V.)',
                        type: 'column',
                        yAxis: 1,
                        data: obj.cantidad

                    },
                    {
                        name: 'Cantidad (E.T.)',
                        type: 'column',
                        yAxis: 1,
                        data: obj.cantidad1

                    }, {
                        name: 'Monto (E.T.)',
                        type: 'spline',
                        yAxis: 2,
                        data: obj.monto,
                        marker: {
                            enabled: true
                        },
                        dashStyle: 'shortdot',
                    },
                    {
                        name: 'Monto (P.V.)',
                        type: 'spline',
                        data: obj.monto1,
                        color: '#C0504D'
                    }
                ],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                floating: false,
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 0
                            },
                            yAxis: [{
                                labels: {
                                    align: 'right',
                                    x: 0,
                                    y: -6
                                },
                                showLastLabel: false
                            }, {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -6
                                },
                                showLastLabel: false
                            }, {
                                visible: false
                            }]
                        }
                    }]
                }
            });

        }
        var chartMultipleAxisCongreso = function(obj) {
            Highcharts.chart('chartCongreso', {
                chart: {
                    zoomType: 'xy'
                },
                title: {
                    text: obj.titulo,
                    align: 'center'
                },
                subtitle: {
                    text: '',
                    align: 'left'
                },
                xAxis: [{
                    categories: obj.partido,
                    crosshair: true
                }],
                yAxis: [{ // Primary yAxis
                    labels: {
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Región',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true

                }, { // Secondary yAxis
                    gridLineWidth: 2,
                    title: {
                        text: 'Partidos Politicos',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {

                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    }

                }, { // Tertiary yAxis
                    gridLineWidth: 2,
                    title: {
                        text: 'Congresistas',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    labels: {

                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    shared: true
                },
                legend: {
                    layout: 'vertical',
                    align: 'center',
                    x: 100,
                    verticalAlign: 'top',
                    y: 85,
                    floating: true,
                    backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || // theme
                        'rgba(255,255,255,0.25)'
                },
                plotOptions: {
                    series: {
                        cursor: 'pointer',
                        point: {
                            events: {
                                click: function() {
                                    //alert('Category: ' + this.category + ', value: ' + this.y);
                                    var intro = document.getElementById('ocultarLink');
                                    intro.style.display = '';
                                    var det = document.getElementById('chartCongresoRegionDet');
                                    det.style.display = '';
                                    var objX = {
                                        method: 'chartMultipleAxisCongresoDet',
                                        partido: this.category
                                    };
                                    var par = this.category;
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objX)),
                                        success: function(response1) {
                                            var tmp = [];
                                            var tmp1 = [];
                                            var electores = JSON.parse(response1.data[0].electores);
                                            var congresistas = JSON.parse(response1.data[0].congresistas);
                                            for (i = 0; i < electores.length; i++) {
                                                tmp.push(parseInt(electores[i]));
                                            }
                                            for (x = 0; x < congresistas.length; x++) {
                                                tmp1.push(parseInt(congresistas[x]));
                                            }
                                            var objRegion = {
                                                div: 'chartCongresoRegionDet',
                                                text: 'Congresistas y Electores de ' + par + ' por Región ',
                                                titulo: JSON.parse(response1.data[0].ubigeo),
                                                data: tmp,
                                                data1: tmp1
                                            };
                                            chartDualAxesCongresos(objRegion);


                                        }
                                    });
                                }
                            }
                        }
                    }
                },
                series: [{
                        name: 'Electores',
                        type: 'column',
                        yAxis: 1,
                        data: obj.electores

                    }, {
                        name: 'Congresistas',
                        type: 'spline',
                        yAxis: 2,
                        data: obj.congresistas,
                        marker: {
                            enabled: false
                        },
                        dashStyle: 'shortdot',
                    },
                    {
                        name: 'Región',
                        type: 'spline',
                        data: obj.region,
                        color: '#C0504D'
                    }
                ],
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                floating: false,
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom',
                                x: 0,
                                y: 0
                            },
                            yAxis: [{
                                labels: {
                                    align: 'right',
                                    x: 0,
                                    y: -6
                                },
                                showLastLabel: false
                            }, {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -6
                                },
                                showLastLabel: false
                            }, {
                                visible: false
                            }]
                        }
                    }]
                }
            });

        }
        var loadCongreso = function(cql) {
            $.ajax({
                url: owsrootUrl + L.Util.getParamString(
                    L.Util.extend(
                        defaultParameters({
                            layerName: 'gen_politicos_marker',
                            sql: cql
                        })
                    )),
                dataType: 'json',
                success: function(data) {
                    layerCongreso = L.layerGroup().addTo(map33);
                    var markers = L.geoJson(data, {
                        pointToLayer: function(feature, latlng) {
                            var marker = L.marker(latlng, {
                                icon: L.divIcon({ html: '<img src="assets/app/img/' + feature.properties.imagen + '.png" title="' + feature.properties.bancada + '" border="0"/>', className: 'info' })
                            });
                            return marker;
                        },
                        onEachFeature: function(feature, layer) {
                            layer.addTo(layerCongreso);
                        }
                    });


                }
            });
        }
        var chartCIPRLbarTemporal3 = function() {
            var chart1 = Highcharts.chart('container3', {
                title: {
                    text: ''
                },

                xAxis: {
                    categories: [

                        '2014',
                        '2015',
                        '2016',
                        '2017',
                        '2018',
                        '2019'

                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Millones de Soles (MM)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>S/{point.y:.2f} MM</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        type: 'spline',
                        name: 'Tope CIPRL',
                        data: [1066.53, 1107.33, 975.88, 686.47, 736.9, 956.4],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    },
                    {
                        type: 'spline',
                        name: 'Monto Utilizado',
                        data: [0, 0, 0, 0, 12.38, 18.43],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    }
                ]
            });
            chart1.render();
        }
        var chartStakedBar = function(obj) {
            var chart1 = Highcharts.chart(obj.div, {
                animationEnabled: true,
                chart: {
                    type: 'bar'
                },
                plotOptions: {
                    series: {
                        stacking: 'normal',
                        maxPointWidth: 10,
                    },

                },
                title: {
                    text: obj.titulo,

                    fontSize: 15,
                    fontFamily: "Helvetica",
                    fontWeight: "bold",


                },
                xAxis: {
                    categories: obj.datatitulo,

                    fontSize: 15,
                    fontFamily: "Helvetica",
                    fontWeight: "bold",

                },
                legend: {
                    verticalAlign: "bottom",
                    fontSize: 11,
                    fontFamily: "Helvetica",
                    Margin: 8
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Millones de Soles (M S/.)',

                        fontSize: 15,
                        fontFamily: "Helvetica",
                        color: "gray"

                    }
                },
                series: [{
                        name: '2019',
                        data: obj.data2019,
                        color: '#C0504D',
                        legendIndex: 10

                    },
                    {
                        name: '2018',
                        data: obj.data2018,
                        color: '#FF9633',
                        legendIndex: 9

                    },
                    {
                        name: '2017',
                        data: obj.data2017,
                        color: '#FFCE33',
                        legendIndex: 8
                    },
                    {
                        name: '2016',
                        data: obj.data2016,
                        color: '#E0FF33',
                        legendIndex: 7
                    },
                    {
                        name: '2015',
                        data: obj.data2015,
                        color: '#86FF33',
                        legendIndex: 6

                    },
                    {
                        name: '2014',
                        data: obj.data2014,
                        color: 'gray',
                        legendIndex: 5
                    },
                    {
                        name: '2013',
                        data: obj.data2013,
                        color: '#33FFE3',
                        legendIndex: 4
                    },
                    {
                        name: '2012',
                        data: obj.data2012,
                        color: '#33BBFF',
                        legendIndex: 3
                    },
                    {
                        name: '2011',
                        data: obj.data2011,
                        color: '#4F81BD',
                        legendIndex: 2
                    },
                    {
                        name: '2010',
                        data: obj.data2010,
                        color: '#9C33FF',
                        legendIndex: 1

                    },
                    {
                        name: '2009',
                        data: obj.data2009,
                        color: '#EC33FF',
                        legendIndex: 0
                    }

                ]
            });
            chart1.render();
        }
        var chartCIPRLbarTemporal1 = function() {
            var chart1 = Highcharts.chart('container1', {
                title: {
                    text: ''
                },

                xAxis: {
                    categories: [
                        '2011',
                        '2012',
                        '2013',
                        '2014',
                        '2015',
                        '2016',
                        '2017',
                        '2018',
                        '2019'

                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Millones de Soles (MM)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>S/{point.y:.2f} MM</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        type: 'spline',
                        name: 'Tope CIPRL',
                        data: [4836.87, 4543.83, 4298.36, 4633.20, 4008.13, 2918.37, 2691.04, 1623.31, 1585.51],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    },
                    {
                        type: 'spline',
                        name: 'Monto Utilizado',

                        data: [5.62, 50.90, 182.90, 151.00, 80.02, 106.37, 139.44, 128.61, 0.00],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    }
                ]
            });
            chart1.render();
            var obj = {
                method: 'cirplxMontoOxiProvincia'
            }
            $.getJSON({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    var categorias = [];
                    var objCiprl = {
                        name: 'CIPRL',
                        data: []
                    }
                    var objOxi = {
                        name: 'OXI',
                        data: []
                    }
                    for (i = 0; i < response.data.length; i++) {
                        categorias.push(response.data[i].ano);
                        objCiprl.data.push(Number((parseInt(response.data[i].ciprl)).toFixed(2)));
                        objOxi.data.push(Number((parseInt(response.data[i].oxi)).toFixed(2)));
                    }
                    var total = [objCiprl, objOxi];
                    var multibar1 = lineChart;
                    multibar1.xAxis.categories = categorias;
                    multibar1.title.text = 'Evolución CIPRL y monto utilizado en Gobiernos Regionales';
                    multibar1.series = total;
                    multibar1.subtitle.text = '(millones de soles)';
                    /////grafico
                    Highcharts.chart('regionOxiCiprl', multibar1);
                    var data = [
                        { 'name': 'G.R. con OXI', 'y': 70.71, 'selected': true },
                        { 'name': 'G.R. sin OXI', 'y': 29.29 }
                    ];
                    var series = [{
                        name: 'Oxi',
                        colorByPoint: true,
                        data: data
                    }];
                    var charpieobj = charPie2;
                    charpieobj.series = series;
                    Highcharts.chart('chartContainerGobiernosRegionales1', charpieobj);
                    var data = [
                        { 'name': 'G.R. con OXI', 'y': 32.43, 'selected': true },
                        { 'name': 'G.R. sin OXI', 'y': 67.57 }
                    ];
                    var series = [{
                        name: 'Empresas',
                        colorByPoint: true,
                        data: data
                    }];
                    var charpieobj1 = charPie2;
                    charpieobj1.series = series;
                    Highcharts.chart('chartContainerGobiernosRegionales2', charpieobj1);
                }
            });
        }
        var chartCIPRLbarTemporal2 = function() {
            var chart1 = Highcharts.chart('container2', {
                title: {
                    text: ''
                },

                xAxis: {
                    categories: [
                        '2011',
                        '2012',
                        '2013',
                        '2014',
                        '2015',
                        '2016',
                        '2017',
                        '2018',
                        '2019'

                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Millones de Soles (MM)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>S/{point.y:.2f} MM</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                        type: 'spline',
                        name: 'Tope CIPRL',
                        data: [18956.35, 18593.46, 17548.33, 18195.35, 15480.23, 11953.96, 10639.81, 7476.87, 9036.28],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    },
                    {
                        type: 'spline',
                        name: 'Monto Utilizado',
                        data: [5.34, 142.66, 105.57, 268.66, 114.47, 259.78, 298.10, 236.13, 0.00],
                        marker: {
                            lineWidth: 5,
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    }
                ]
            });
            chart1.render();
        }
        var chartDivCIPRL = function(obj) {
            var chart1 = Highcharts.chart(obj.div, {
                title: {
                    text: ''
                },

                xAxis: {
                    categories: [
                        'Q1',
                        'Q2',
                        'Q3',
                        'Q4',
                        'Q5'
                    ],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Porcentaje (%)'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0">{point.y:.2f}%</td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                legend: {
                    align: 'center',
                    verticalAlign: 'middle',

                },

                series: [{
                        color: '#C0504D',
                        type: 'spline',
                        name: 'CIPRL :',
                        data: obj.data,

                    },
                    {
                        color: '#3377ff',
                        type: 'column',
                        name: 'Entidades :',
                        data: obj.data2,
                    }
                ]
            });
            chart1.render();
        }
        var chartPieExcel = function(obj) {
            var chart1 = Highcharts.chart(obj.div, {
                animationEnabled: true,
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie',
                },
                title: {
                    text: ''
                },

                tooltip: {
                    pointFormat: '<b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        size: 100,
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            style: {
                                fontWeight: 'bold',
                                fontSize: '9px',
                                fontFamily: "Helvetica"
                            },
                            format: '{point.name}: {point.y}% ',
                        },
                        showInLegend: true

                    }
                },
                series: [{
                    name: 'Porcentaje',
                    colorByPoint: true,
                    data: [{
                            name: obj.name1,
                            y: obj.y1,
                            color: '#4F81BD',
                        },
                        {
                            name: obj.name2,
                            y: obj.y2,
                            color: '#C0504D'
                        },
                    ]
                }]
            });
            chart1.render();
        }
        var changeTabPanelEntidadProyects = function(params, params2) {
            $('#universidades-tab-funcion').removeClass('active');
            $('#ministerio-tab-funcion').removeClass('active');
            $('#regiones-tab-funcion').removeClass('active');
            $('#provincias-tab-funcion').removeClass('active');
            $('#distritos-tab-funcion').removeClass('active');
            $('#ministerio-funcion').removeClass('show active');
            $('#regiones-funcion').removeClass('show active');
            $('#provincias-funcion').removeClass('show active');
            $('#distritos-funcion').removeClass('show active');
            $('#universidades-funcion').removeClass('show active');
            document.getElementById('universidades-tab-funcion').style.color = "black";
            document.getElementById('ministerio-tab-funcion').style.color = "black";
            document.getElementById('regiones-tab-funcion').style.color = "black";
            document.getElementById('provincias-tab-funcion').style.color = "black";
            document.getElementById('distritos-tab-funcion').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
            var elemento1 = document.getElementById(params2);
            elemento1.className += " show active";
        }
        var changeTabPanelSubCiprla = function(params) {
            $('#gobiernos-tab').removeClass('active');
            $('#universidades-tab').removeClass('active');
            document.getElementById('gobiernos-tab').style.color = "black";
            document.getElementById('universidades-tab').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
        }
        if (params.classList.contains('changeTablaOxi')) {
            var id = params.getAttribute('id');
            var divStakedBar = document.getElementById('stkd_charempresa');
            var divStakedBar1 = document.getElementById('stkd_charempresa1');
            switch (id) {
                case 'tableTotal':
                    var msj = "Usted Selecciono Total de empresas";
                    var objTabla = {
                        limitFin: 109,
                        limitInicio: 0,
                        titulo: 'Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 3
                    };

                    divStakedBar.style.cssText = 'height: 3500px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 3500px; width: 100%;';
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'tabletop10':
                    var msj = "Usted Selecciono Top 10 de empresas";
                    var objTabla = {
                        limitFin: 10,
                        limitInicio: 0,
                        titulo: 'Top 10 Empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 1
                    };

                    divStakedBar.style.cssText = 'height: 400px; width: 100%;';
                    divStakedBar.style.cssText = 'height: 400px; width: 100%;';
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 10,
                        limitInicio: 0
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    break;
                case 'tableFiferencia':
                    var msj = "Usted Selecciono resto de empresas";
                    var objTabla = {
                        limitFin: 109,
                        limitInicio: 10,
                        titulo: 'Resto de empresas con OxI 2009 - 2019 (Millones de Soles)',
                        desdeLimite: 2,
                        rbtnTabla: 2
                    };
                    var obj1AdjCon = {
                        method: 'chartStakedEmpresas',
                        limitFin: 109,
                        limitInicio: 10
                    };
                    empresasTableChangeAdjudicados(obj1AdjCon);
                    divStakedBar.style.cssText = 'height: 2400px; width: 100%;';
                    divStakedBar1.style.cssText = 'height: 2400px; width: 100%;';
                    break;

            }
            toastr.success(msj, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });

            tablaTopEmpresasOxi(objTabla);
            stackedBarTopEmpresasOxi(objTabla)
        }
        //ciprel indicadores clic dentro
        if (params.classList.contains('tabSubEstadisticaCIPRL')) {
            var id = params.getAttribute('id');
            console.log("entro ciprl");
            changeTabPanelSubCiprla(id);
            if (id == 'gobiernos-tab') {
                var obj = {
                    method: 'chartciprliformacion'
                }
                $.getJSON({
                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        console.log(response);
                        var categorias = [];
                        var obj2018 = {
                            name: '2018',
                            data: []
                        }
                        var obj2019 = {
                            name: '2019',
                            data: []
                        }
                        var obj2020 = {
                            name: '2020',
                            data: []
                        }
                        for (i = 0; i < response.data.length; i++) {
                            categorias.push(response.data[i].departamento);
                            if (id == 'gobiernos-tab') {
                                obj2018.data.push(Number((parseInt(response.data[i].ciprl2018) / 1000000).toFixed(3)));
                                obj2019.data.push(Number((parseInt(response.data[i].ciprl2019) / 1000000).toFixed(3)));
                                obj2020.data.push(Number((parseInt(response.data[i].ciprl2020) / 1000000).toFixed(3)));

                            } else {
                                obj2018.data.push(Number((parseInt(response.data[i].canon2018) / 1000000).toFixed(3)));
                                obj2019.data.push(Number((parseInt(response.data[i].canon2019) / 1000000).toFixed(3)));
                                obj2020.data.push(Number((parseInt(response.data[i].canon2020) / 1000000).toFixed(3)));
                            }

                        }
                        var total = [obj2018, obj2019, obj2020];
                        var multibar1 = multibar;
                        multibar.xAxis.categories = categorias;
                        multibar.title.text = 'Distribucion de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por Regiones';
                        multibar.series = total;
                        multibar.subtitle.text = '(millones de soles)'
                        console.log(obj2018);
                        multibar.plotOptions.series.point.events.click = function(event) {
                            document.getElementById("mostarchar2").style.display = "";

                            var obj = {
                                method: 'chartciprliformacionProvincia',
                                departamento: event.point.category
                            }
                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response1) {
                                    var categorias = [];
                                    var obj2018 = {
                                        name: '2018',
                                        data: []
                                    }
                                    var obj2019 = {
                                        name: '2019',
                                        data: []
                                    }
                                    var obj2020 = {
                                        name: '2020',
                                        data: []
                                    }
                                    for (i = 0; i < response1.data.length; i++) {
                                        categorias.push(response1.data[i].nombprov);
                                        if (id == 'ciprl-tab') {
                                            obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                            obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                            obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                        } else {
                                            obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                            obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                            obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                        }
                                    }
                                    var total = [obj2018, obj2019, obj2020];
                                    multibar1.xAxis.categories = categorias;
                                    multibar1.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por provincias, región ' + event.point.category;
                                    multibar1.series = total;
                                    multibar1.subtitle.text = '(millones de soles)'
                                    multibar1.plotOptions.series.point.events.click = function(event) {
                                        document.getElementById("mostarchar3").style.display = "";
                                        var obj1 = {
                                            method: 'chartciprliformacionDistrito',
                                            provincia: event.point.category,
                                            region: obj.departamento
                                        }
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                            success: function(response1) {
                                                var categorias = [];
                                                var obj2018 = {
                                                    name: '2018',
                                                    data: []
                                                }
                                                var obj2019 = {
                                                    name: '2019',
                                                    data: []
                                                }
                                                var obj2020 = {
                                                    name: '2020',
                                                    data: []
                                                }
                                                for (i = 0; i < response1.data.length; i++) {
                                                    categorias.push(response1.data[i].distrito);
                                                    if (id == 'ciprl-tab') {
                                                        obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                                        obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                                        obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                                    } else {
                                                        obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                                        obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                                        obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                                    }
                                                }
                                                var total = [obj2018, obj2019, obj2020];
                                                var multibar2 = multibar;
                                                multibar2.xAxis.categories = categorias;
                                                multibar2.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por distritos, provincia ' + event.point.category + ', región ' + obj.departamento;
                                                multibar2.series = total;
                                                multibar2.subtitle.text = '(millones de soles)';
                                                multibar2.plotOptions.series.point.events.click = function(event) {
                                                    console.log('erfgh');
                                                };
                                                Highcharts.chart('chart_3', multibar2);
                                            }
                                        });
                                    }
                                    Highcharts.chart('chart_2', multibar1);

                                }
                            });
                        }
                        Highcharts.chart('chart1', multibar);
                    }
                });

                var obj = {
                    titulo: 'Ciprl',
                    tabla: 'gobiernos'
                }

                $.post({
                    url: 'divresrmenyestadisticasSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('getResumenSalud').innerHTML = response;
                    }
                });
            } else {
                var obj = {
                    method: 'getCiprlUniversidades'
                }
                $.getJSON({
                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        var categorias = [];
                        var obj2018 = {
                            name: '2018',
                            data: []
                        }
                        var obj2019 = {
                            name: '2019',
                            data: []
                        }
                        var obj2020 = {
                            name: '2020',
                            data: []
                        }
                        for (i = 0; i < response.data.length; i++) {
                            categorias.push(response.data[i].universidad);

                            obj2018.data.push(Number((parseInt(response.data[i].s2018) / 1000000).toFixed(2)));
                            obj2019.data.push(Number((parseInt(response.data[i].s2019) / 1000000).toFixed(2)));
                            obj2020.data.push(Number((parseInt(response.data[i].s2020) / 1000000).toFixed(2)));
                        }
                        var total = [obj2018, obj2019, obj2020];
                        var multibar1 = multibar;
                        multibar1.xAxis.categories = categorias;
                        multibar1.yAxis.title.text = 'millones de soles';
                        multibar1.title.text = 'Distribucion de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por Universidades';
                        multibar1.series = total;
                        multibar1.subtitle.text = '(millones de soles)'
                        Highcharts.chart('chart1', multibar1);
                    }
                });
                var obj = {
                    titulo: 'Ciprl',
                    tabla: 'universidades'
                }

                $.post({
                    url: 'divresrmenyestadisticasSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('getResumenSalud').innerHTML = response;
                    }
                });
            }


        }
        if (params.classList.contains('lnkAmpliar2')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');


            var target = $("[data-target='" + id + "']");
            var envio = {
                quintil: id1,
                region: document.getElementById('coddptoPF').value,
                tipo: document.getElementById('hiddenfieldtipoCPRL').value
            }
            $.get({
                url: 'divCiprlPoblacionElectoresQuintildet.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('tablapoblacioelectoresquintiles_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('tabEntidadProyects')) {
            var id = params.getAttribute('id');

            switch (id) {
                case 'ministerio-tab-funcion':
                    changeTabPanelEntidadProyects(id, 'ministerio-funcion');
                    var obj = {

                        idnivel: document.getElementById('codnivelPIP').value,
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        idnivGob: 03
                    };
                    var div = document.getElementById('divpipMinisterios');
                    break;
                case 'regiones-tab-funcion':
                    changeTabPanelEntidadProyects(id, 'regiones-funcion');
                    var obj = {

                        idnivel: document.getElementById('codnivelPIP').value,
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        idnivGob: 02
                    };
                    var div = document.getElementById('divpipRegiones');
                    break;
                case 'provincias-tab-funcion':
                    changeTabPanelEntidadProyects(id, 'provincias-funcion');
                    var obj = {

                        idnivel: document.getElementById('codnivelPIP').value,
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        idnivGob: 05
                    };
                    var div = document.getElementById('divpipProvincias');
                    break;
                case 'distritos-tab-funcion':
                    changeTabPanelEntidadProyects(id, 'distritos-funcion');
                    var obj = {

                        idnivel: document.getElementById('codnivelPIP').value,
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        idnivGob: 01
                    };
                    var div = document.getElementById('divpipDistritas');
                    break;
                case 'universidades-tab-funcion':
                    changeTabPanelEntidadProyects(id, 'universidades-funcion');
                    var obj = {

                        idnivel: document.getElementById('codnivelPIP').value,
                        codubigeo: document.getElementById('codUbigeoPIP').value,
                        idnivGob: 04
                    };
                    var div = document.getElementById('divpipUniversidades');
                    break;
            }
            $.post({
                url: 'divpipProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    div.innerHTML = response;
                }
            });
        }
        var changeTabPanelSuibEstadistica1 = function(params) {
            $('#fallecidos-tab').removeClass('active');
            $('#resumen-tab').removeClass('active');
            $('#fallecidospob-tab').removeClass('active');
            document.getElementById('fallecidos-tab').style.color = "black";
            document.getElementById('resumen-tab').style.color = "black";
            document.getElementById('fallecidospob-tab').style.color = "black";
            var elemento = document.getElementById(params);
            elemento.className += " active";
            elemento.style.color = "#1976D2";
        }
        if (params.classList.contains('tabSubEstadistica')) {
            var id = params.getAttribute('id');
            changeTabPanelSuibEstadistica1(id);
            if (id == 'resumen-tab') {
                var obj = {
                    tab: 'resumen-tab',
                    tipo: 'COVID'
                }

                $.post({
                    url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('divresumeninformacion123').innerHTML = response;
                        // var obj = {
                        //     method: 'quintiles_mes_anual_covid',
                        // }
                        // $.getJSON({
                        //     url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        //     success: function(response1) {
                        //         console.log(response1);
                        //         var categorias = [];
                        //         var obj1 = {
                        //             name: '2020',
                        //             type: 'column',
                        //             yAxis: 0,
                        //             color: '#2f7ed8',
                        //             data: []
                        //         };

                        //         for (i = 0; i < response1.data.length; i++) {

                        //             categorias.push(response1.data[i].meses);
                        //             obj1.data.push(parseInt(response1.data[i].fallecidos));
                        //         }
                        //         var total = [obj1];

                        //         var Chart1 = dualChart;
                        //         Chart1.xAxis[0].categories = categorias;
                        //         Chart1.title.text = 'Fallecidos COVID a nivel nacional';
                        //         Chart1.subtitle.text = '(2020)';
                        //         Chart1.series = total;
                        //         Highcharts.chart('chartSinadef4', Chart1);

                        //     }
                        // });
                        var obj = {
                            method: 'quintiles_mes_anual_covid',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                console.log(response1);
                                var categorias = [];

                                var obj4 = {
                                    name: '2020',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#F28F43',
                                    data: []
                                };
                                var obj5 = {
                                    name: '2021',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#000',
                                    data: []
                                };
                                var obj6 = {
                                    name: 'proyectado',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: 'red',
                                    data: []
                                };
                                for (i = 0; i < response1.data.length; i++) {
                                    categorias.push(response1.data[i].meses);
                                    obj4.data.push(parseInt(response1.data[i].s2020));
                                    if (response1.data[i].s2021 != 0) {
                                        obj5.data.push(parseInt(response1.data[i].s2021));
                                    }

                                }
                                obj6.data.push(3457);
                                obj6.data.push(6178);
                                obj6.data.push(9961);
                                obj6.data.push(11998);

                                var total = [obj4, obj6, obj5];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos Covid a nivel nacional';
                                Chart1.subtitle.text = '(2020-2021)';
                                Chart1.series = total;
                                Highcharts.chart('chartSinadef4', Chart1);
                            }
                        });
                        var obj = {
                            method: 'quintil_semanal_covidgrafico',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                console.log(response1);
                                var categorias = [];

                                var obj4 = {
                                    name: '2020',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#F28F43',
                                    data: []
                                };
                                var obj5 = {
                                    name: '2021',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#000',
                                    data: []
                                };
                                var obj6 = {
                                    name: 'proyectado',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: 'red',
                                    data: []
                                };
                                for (i = 0; i < response1.data.length; i++) {
                                    categorias.push(response1.data[i].semana);

                                    obj4.data.push(parseInt(response1.data[i].anio2020));
                                    if (response1.data[i].s2021 != 0 && i <= 6) {
                                        obj5.data.push(parseInt(response1.data[i].anio2021));
                                    }
                                    if (i <= 16) {
                                        obj6.data.push(parseInt(response1.data[i].anio2021))
                                    }
                                }
                                var total = [obj4, obj6, obj5];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos Covid a nivel nacional';
                                Chart1.subtitle.text = '(2020-2021)';
                                Chart1.series = total;
                                Highcharts.chart('chartSinadef5', Chart1);
                                console.log(total);

                            }
                        });

                        var obj = {
                            method: 'quintiles_minsa_tab',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                console.log(response1);
                                var categorias = [];
                                var categorias1 = [];
                                var categorias2 = [];
                                var obj1 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj1_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };
                                var obj2 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj2_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };
                                var obj3 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj3_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };

                                for (i = 0; i < response1.data.length; i++) {
                                    if (response1.data[i].tipo == 1) {
                                        categorias.push(response1.data[i].texto);
                                        obj1.data.push(parseInt(response1.data[i].entidades));
                                        obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    } else if (response1.data[i].tipo == 2) {
                                        categorias1.push(response1.data[i].texto);
                                        obj2.data.push(parseInt(response1.data[i].entidades));
                                        obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    } else if (response1.data[i].tipo == 3) {
                                        categorias2.push(response1.data[i].texto);
                                        obj3.data.push(parseInt(response1.data[i].entidades));
                                        obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    }

                                }
                                var total = [obj3, obj3_Pob];
                                var Chart3 = dualChart;
                                Chart3.xAxis[0].categories = categorias1;
                                Chart3.title.text = 'Fallecidos COVID-19 Distrito';
                                Chart3.series = total;
                                Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef3', Chart3);
                                var total = [obj2, obj2_Pob];
                                var Chart2 = dualChart;
                                Chart2.xAxis[0].categories = categorias1;
                                Chart2.title.text = 'Fallecidos COVID-19 Provincia';
                                Chart2.series = total;
                                Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef2', Chart2);
                                var total = [obj1, obj1_Pob];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos COVID-19 Region';
                                Chart1.series = total;
                                Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef1', Chart1);
                            }
                        });
                        var obj = {
                            tipo: 'covid',
                        }
                        $.post({
                            url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getResumenSalud').innerHTML = response;
                                map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                }).addTo(map3);
                                tile4 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_minsa_departamentos',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map3);
                                map3.invalidateSize();
                                map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                }).addTo(map4);
                                tile5 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_minsa_provincias',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map4);
                                map4.invalidateSize();
                                map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                tile6 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_minsa_distritos',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map5);
                                map5.invalidateSize();
                            }
                        });

                    }
                });
            } else {
                var obj = {
                    tab: id
                }
                $.post({
                    url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        console.log(id);
                        console.log("fallecidos minsa");

                        document.getElementById('divresumeninformacion123').innerHTML = response;
                        var obj = {
                            method: 'chartciprliformacionMinsa',
                            tipo: 'fallecidos'
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                console.log(response);
                                var categorias = [];
                                var obj2020 = {
                                    color: '#F28F43',
                                    name: '2020',
                                    data: [],
                                    visible: false
                                }
                                var obj2021 = {
                                    name: '2021',
                                    color: '#000',
                                    data: [],
                                    visible: false
                                }
                                var total = {
                                    color: '#2f7ed8',
                                    name: 'total',
                                    data: []
                                }
                                for (i = 0; i < response.data.length; i++) {
                                    categorias.push(response.data[i].departamento);
                                    if (id == 'fallecidos-tab') {
                                        obj2020.data.push(parseInt(response.data[i].anio2020));
                                        obj2021.data.push(parseInt(response.data[i].anio2021));
                                        total.data.push(parseInt(response.data[i].total));
                                    } else {
                                        obj2020.data.push(parseInt(response.data[i].fallecidos_2020_1000));
                                        obj2021.data.push(parseInt(response.data[i].fallecidos_2021_1000));
                                        total.data.push(parseInt(response.data[i].fatotal20_21));
                                    }
                                }
                                var total = [obj2020, obj2021, total];
                                var multibar1 = multibar;
                                multibar1.xAxis.categories = categorias;
                                multibar1.title.text = 'Distribucion anual de Fallecidos MINSA por Regiones';
                                if (id == 'fallecidos-tab') {
                                    multibar1.yAxis.title.text = 'Fallecidos';
                                } else {
                                    multibar1.yAxis.title.text = 'Fallecidos/Poblacion';
                                }
                                multibar1.series = total;

                                multibar1.plotOptions.series.point.events.click = function(event) {
                                    document.getElementById("mostarchar2").style.display = "";
                                    console.log(event.point.category);

                                    var obj = {
                                        method: 'chartciprliformacionProvinciaMinsa',
                                        departamento: event.point.category,
                                        tipo: 'fallecidos'
                                    }
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response1) {
                                            console.log(response1)
                                            var categorias = [];
                                            var obj2020 = {
                                                name: '2020',
                                                data: [],
                                                color: '#056A09',
                                                visible: false
                                            }
                                            var obj2021 = {
                                                name: '2021',
                                                data: [],
                                                color: '#C70027',
                                                visible: false
                                            }
                                            var totalp = {
                                                name: 'total',
                                                data: [],
                                                color: '#2f7ed8'

                                            }
                                            for (i = 0; i < response1.data.length; i++) {
                                                categorias.push(response1.data[i].provincia);
                                                if (id == 'fallecidos-tab') {
                                                    if (response1.data[i].anio2020 == null) {
                                                        response1.data[i].anio2020 = "0";
                                                    }
                                                    if (response1.data[i].anio2021 == null) {
                                                        response1.data[i].anio2021 = "0";
                                                    }
                                                    obj2020.data.push(parseInt(response1.data[i].anio2020));
                                                    obj2021.data.push(parseInt(response1.data[i].anio2021));
                                                    totalp.data.push(parseInt(response1.data[i].anio2020) + parseInt(response1.data[i].anio2021));
                                                } else {
                                                    if (response1.data[i].anio2020 == null) {
                                                        response1.data[i].anio2020 = "0";
                                                    }
                                                    if (response1.data[i].anio2021 == null) {
                                                        response1.data[i].anio2021 = "0";
                                                    }
                                                    obj2020.data.push(parseInt(response1.data[i].anio2020) / parseInt(response1.data[i].poblacion) * 10000);
                                                    obj2021.data.push(parseInt(response1.data[i].anio2021) / parseInt(response1.data[i].poblacion) * 10000);
                                                    totalp.data.push((parseInt(response1.data[i].anio2020) / parseInt(response1.data[i].poblacion) * 10000) + (parseInt(response1.data[i].anio2021) / parseInt(response1.data[i].poblacion) * 10000));
                                                }
                                            }
                                            var totalp = [obj2020, obj2021, totalp];
                                            var multibar2 = multibar;
                                            multibar2.xAxis.categories = categorias;
                                            multibar2.title.text = 'Distribución anual de fallecidos MINSA por provincias, región ' + event.point.category;
                                            multibar2.series = totalp;
                                            console.log(totalp);
                                            if (id == 'fallecidos-tab') {
                                                multibar2.yAxis.title.text = 'Fallecidos';
                                            } else {
                                                multibar2.yAxis.title.text = 'Fallecidos/Poblacion';
                                            }

                                            multibar2.plotOptions.series.point.events.click = function(event) {
                                                document.getElementById("mostarchar3").style.display = "";
                                                var obj2 = {
                                                    method: 'chartciprliformacionDistritoMinsa',
                                                    provincia: event.point.category,
                                                    region: obj.departamento,
                                                    tipo: 'fallecidos'
                                                }
                                                $.getJSON({
                                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj2)),
                                                    success: function(response2) {
                                                        console.log(response2);
                                                        var categorias = [];

                                                        var obj2020 = {
                                                            name: '2020',
                                                            data: [],
                                                            color: '#056A09',
                                                            visible: false
                                                        }
                                                        var obj2021 = {
                                                            name: '2021',
                                                            data: [],
                                                            color: '#C70027',
                                                            visible: false

                                                        }
                                                        var totaldx = {
                                                            name: 'total',
                                                            data: [],
                                                            color: '#2f7ed8'

                                                        }
                                                        for (i = 0; i < response2.data.length; i++) {
                                                            categorias.push(response2.data[i].distrito);
                                                            if (id == 'fallecidos-tab') {
                                                                if (response2.data[i].anio2020 == null) {
                                                                    response2.data[i].anio2020 = "0";
                                                                }
                                                                if (response2.data[i].anio2021 == null) {
                                                                    response2.data[i].anio2021 = "0";
                                                                }
                                                                obj2020.data.push(parseInt(response2.data[i].anio2020));
                                                                obj2021.data.push(parseInt(response2.data[i].anio2021));
                                                                totaldx.data.push(parseInt(response2.data[i].anio2020) + parseInt(response2.data[i].anio2021));
                                                            } else {
                                                                if (response2.data[i].anio2020 == null) {
                                                                    response2.data[i].anio2020 = "0";
                                                                }
                                                                if (response2.data[i].anio2021 == null) {
                                                                    response2.data[i].anio2021 = "0";
                                                                }
                                                                obj2020.data.push(parseInt(response2.data[i].anio2020) / parseInt(response2.data[i].poblacion) * 10000);
                                                                obj2021.data.push(parseInt(response2.data[i].anio2021) / parseInt(response2.data[i].poblacion) * 10000);
                                                                totaldx.data.push((parseInt(response2.data[i].anio2020) / parseInt(response2.data[i].poblacion) * 10000) + (parseInt(response2.data[i].anio2021) / parseInt(response2.data[i].poblacion)) * 10000);
                                                            }
                                                        }
                                                        var totald = [obj2020, obj2021, totaldx];
                                                        var multibar3 = multibar;
                                                        multibar3.xAxis.categories = categorias;
                                                        multibar3.title.text = 'Distribución anual de fallecidos MINSA por distritos, provincia ' + event.point.category + ', región ' + obj.departamento;
                                                        multibar3.series = totald;
                                                        console.log(totald);
                                                        if (id == 'fallecidos-tab') {
                                                            multibar3.yAxis.title.text = 'Fallecidos';
                                                        } else {
                                                            multibar3.yAxis.title.text = 'Fallecidos/Poblacion';
                                                        }

                                                        multibar3.plotOptions.series.point.events.click = function(event) {

                                                        };
                                                        Highcharts.chart('chart_3', multibar3);
                                                    }
                                                });
                                            }
                                            Highcharts.chart('chart_2', multibar2);
                                        }
                                    });
                                }
                                Highcharts.chart('chart1', multibar1);
                            }
                        });
                        var obj = {
                            titulo: 'Minsa',
                            tipo: id
                        }

                        $.post({
                            url: 'divresrmenyestadisticasSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getResumenSalud').innerHTML = response;
                            }
                        });
                    }
                });

            }


        }
        if (params.classList.contains('tabSubEstadisticaSina')) {
            var id = params.getAttribute('id');
            changeTabPanelSuibEstadistica(id);
            //cuando uno le da clic en resumen
            if (id == 'resumen-tab') {
                var obj = {
                    tab: 'resumen-tab',
                    tipo: 'SINADEF'
                }
                $.post({
                    url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('divresumeninformacion123').innerHTML = response;

                        var obj = {
                            method: 'quintiles_mes_anual_sinadef',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                console.log(response1);
                                var categorias = [];

                                var obj2 = {
                                    name: '2018',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#A61A1A',
                                    data: []
                                };
                                var obj3 = {
                                    name: '2019',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#8BBC21',
                                    data: []
                                };
                                var obj4 = {
                                    name: '2020',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#F28F43',
                                    data: []
                                };
                                var obj5 = {
                                    name: '2021',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#000',
                                    data: []
                                };
                                var obj6 = {
                                    name: 'proyectado',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: 'red',
                                    data: []
                                };

                                for (i = 0; i < response1.data.length; i++) {

                                    categorias.push(response1.data[i].mes);
                                    obj2.data.push(parseInt(response1.data[i].anio2018));
                                    obj3.data.push(parseInt(response1.data[i].anio2019));
                                    obj4.data.push(parseInt(response1.data[i].anio2020));

                                    if (response1.data[i].anio2021 != 0 && i == 0) {
                                        obj5.data.push(parseInt(response1.data[i].anio2021));
                                    }


                                }
                                obj6.data.push(21968);
                                obj6.data.push(29949);
                                obj6.data.push(43601);
                                obj6.data.push(50718);
                                var total = [obj2, obj3, obj4, obj6, obj5];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos Sinadef a nivel nacional';
                                Chart1.subtitle.text = '(2018-2021)';
                                Chart1.series = total;
                                Highcharts.chart('chartSinadef4', Chart1);
                                console.log(total);
                            }
                        });
                        var obj = {
                            method: 'quintiles_sem_anual_sinadef',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response2) {
                                console.log(response2);
                                var categorias = [];

                                var obj2 = {
                                    name: '2018',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#A61A1A',
                                    data: []
                                };
                                var obj3 = {
                                    name: '2019',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#8BBC21',
                                    data: []
                                };
                                var obj4 = {
                                    name: '2020',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#F28F43',
                                    data: []
                                };
                                var obj5 = {
                                    name: '2021',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: '#000',
                                    data: []
                                };
                                var obj6 = {
                                    name: 'proyectado',
                                    type: 'spline',
                                    yAxis: 1,
                                    color: 'red',
                                    data: []
                                };

                                for (i = 0; i < response2.data.length; i++) {

                                    categorias.push(response2.data[i].semana);
                                    obj2.data.push(parseInt(response2.data[i].anio2018));
                                    obj3.data.push(parseInt(response2.data[i].anio2019));
                                    // obj4.data.push(parseInt(response2.data[i].anio2020));

                                    // if (response2.data[i].anio2021 != 0 && i == 0) {
                                    //     obj5.data.push(parseInt(response2.data[i].anio2021));
                                    // }

                                    obj4.data.push(parseInt(response2.data[i].anio2020));
                                    if (response2.data[i].s2021 != 0 && i <= 5) {
                                        obj5.data.push(parseInt(response2.data[i].anio2021));
                                    }
                                    if (i <= 17) {
                                        obj6.data.push(parseInt(response2.data[i].anio2021))
                                    }
                                }
                                // obj6.data.push(21886);
                                // obj6.data.push(28849);
                                // obj6.data.push(41875);
                                // obj6.data.push(47661);
                                var total = [obj2, obj3, obj4, obj6, obj5];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos Sindef a nivel nacional';
                                Chart1.subtitle.text = '(2018-2021)';
                                Chart1.series = total;
                                Highcharts.chart('chartSinadef5', Chart1);
                                console.log(total);
                            }
                        });
                        var obj = {
                            method: 'quintiles_sinadef_tab',
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                console.log(response1);
                                var categorias = [];
                                var categorias1 = [];
                                var categorias2 = [];
                                var obj1 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj1_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };
                                var obj2 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj2_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };
                                var obj3 = {
                                    name: 'Entidades',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#4572A7',
                                    data: []
                                };
                                var obj3_Pob = {
                                    name: 'Fallecidos',
                                    type: 'spline',
                                    color: '#A7454F',
                                    data: []
                                };

                                for (i = 0; i < response1.data.length; i++) {
                                    if (response1.data[i].tipo == 1) {
                                        categorias.push(response1.data[i].texto);
                                        obj1.data.push(parseInt(response1.data[i].entidades));
                                        obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    } else if (response1.data[i].tipo == 2) {
                                        categorias1.push(response1.data[i].texto);
                                        obj2.data.push(parseInt(response1.data[i].entidades));
                                        obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    } else if (response1.data[i].tipo == 3) {
                                        categorias2.push(response1.data[i].texto);
                                        obj3.data.push(parseInt(response1.data[i].entidades));
                                        obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                    }

                                }
                                var total = [obj3, obj3_Pob];
                                var Chart3 = dualChart;
                                Chart3.xAxis[0].categories = categorias1;
                                Chart3.title.text = 'Fallecidos SINADEF Distrito';
                                Chart3.series = total;
                                Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef3', Chart3);
                                var total = [obj2, obj2_Pob];
                                var Chart2 = dualChart;
                                Chart2.xAxis[0].categories = categorias1;
                                Chart2.title.text = 'Fallecidos SINADEF Provincia';
                                Chart2.series = total;
                                Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef2', Chart2);
                                var total = [obj1, obj1_Pob];
                                var Chart1 = dualChart;
                                Chart1.xAxis[0].categories = categorias;
                                Chart1.title.text = 'Fallecidos SINADEF Region';
                                Chart1.series = total;
                                Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                Highcharts.chart('chartSinadef1', Chart1);
                            }
                        });
                        var obj = {
                            tipo: 'sinadef',
                        }
                        $.post({
                            url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getResumenSalud').innerHTML = response;
                                map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                }).addTo(map3);
                                tile1 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_sinadef_departamentos',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map3);
                                map3.invalidateSize();
                                map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                }).addTo(map4);
                                tile2 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_sinadef_provincias',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map4);
                                map4.invalidateSize();
                                map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                tile3 = L.tileLayer.betterWms(wmsrootUrl, {
                                    layers: 'colaboraccion_2020:gen_fallecidos_sinadef_distritos',
                                    tiled: true,
                                    format: 'image/png',
                                    minZoom: 0,
                                    continuousWorld: true,
                                    transparent: true
                                }).addTo(map5);
                                map5.invalidateSize();
                            }
                        });

                    }
                });
            } else {
                var obj = {
                        tab: id
                    }
                    //sinadef_fallecidos
                $.post({
                    url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {

                        document.getElementById('divresumeninformacion123').innerHTML = response;
                        var obj = {
                            method: 'chartciprliformacionSinadef'
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {

                                var categorias = [];
                                var obj2017 = {
                                    name: '2017',
                                    data: [],
                                    visible: false
                                }
                                var obj2018 = {
                                    name: '2018',
                                    data: [],
                                    visible: false,
                                    color: '#A61A1A'

                                }
                                var obj2019 = {
                                    name: '2019',
                                    data: [],
                                    visible: false,
                                    color: '#8BBC21'
                                }
                                var obj2020 = {
                                    name: '2020',
                                    data: [],
                                    visible: false,
                                    color: '#F28F43'
                                }
                                var obj2021 = {
                                    name: '2021',
                                    data: [],
                                    visible: false,
                                    color: '#000'
                                }
                                var total = {
                                    name: 'total',
                                    data: [],
                                    color: '#4897f1'
                                }
                                for (i = 0; i < response.data.length; i++) {
                                    categorias.push(response.data[i].departamento_domicilio);
                                    if (id == 'fallecidos-tab') {
                                        obj2018.data.push(parseInt(response.data[i].anio2018));
                                        obj2019.data.push(parseInt(response.data[i].anio2019));
                                        obj2020.data.push(parseInt(response.data[i].anio2020));
                                        obj2021.data.push(parseInt(response.data[i].anio2021));
                                        total.data.push(parseInt(response.data[i].total));

                                    } else {
                                        obj2018.data.push(parseInt(response.data[i].fallecidos_2018_1000));
                                        obj2019.data.push(parseInt(response.data[i].fallecidos_2019_1000));
                                        obj2020.data.push(parseInt(response.data[i].fallecidos_2020_1000));
                                        obj2021.data.push(parseInt(response.data[i].fallecidos_2021_1000));
                                        total.data.push(parseInt(response.data[i].fatotal20_21));
                                    }

                                }
                                var total = [obj2018, obj2019, obj2020, obj2021, total];
                                var multibar1 = multibar;
                                multibar1.xAxis.categories = categorias;
                                multibar1.title.text = 'Distribucion de anual de fallecidos ' + document.getElementById("txttipoCIPRLcANONM").value + ' por Regiones';
                                multibar1.series = total;
                                console.log(multibar1);
                                if (id == 'fallecidos-tab') {
                                    multibar1.yAxis.title.text = 'Fallecidos';
                                } else {
                                    multibar1.yAxis.title.text = 'Fallecidos/Poblacion';
                                }

                                multibar1.plotOptions.series.point.events.click = function(event) {
                                    document.getElementById("mostarchar2").style.display = "";

                                    var obj = {
                                        method: 'chartciprliformacionSinadefProvincia',
                                        departamento: event.point.category
                                    }

                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response1) {
                                            console.log(response1);
                                            var categorias = [];

                                            var obj2018 = {
                                                name: '2018',
                                                data: [],
                                                visible: false,
                                                color: '#A61A1A'
                                            }
                                            var obj2019 = {
                                                name: '2019',
                                                data: [],
                                                visible: false,
                                                color: '#8BBC21'
                                            }
                                            var obj2020 = {
                                                name: '2020',
                                                data: [],
                                                visible: false,
                                                color: '#F28F43'
                                            }
                                            var obj2021 = {
                                                name: '2021',
                                                data: [],
                                                visible: false,
                                                color: '#000'
                                            }
                                            var total = {
                                                name: 'Total',
                                                data: [],

                                                color: '#4897f1'
                                            }

                                            for (i = 0; i < response1.data.length; i++) {
                                                categorias.push(response1.data[i].provincia_domicilio);

                                                if (id == 'fallecidos-tab') {
                                                    obj2018.data.push(parseInt(response1.data[i].anio2018));
                                                    obj2019.data.push(parseInt(response1.data[i].anio2019));
                                                    obj2020.data.push(parseInt(response1.data[i].anio2020));
                                                    obj2021.data.push(parseInt(response1.data[i].anio2021));
                                                    total.data.push(parseInt(response1.data[i].total));
                                                } else {
                                                    obj2018.data.push(parseInt(response1.data[i].fallecidos_2018_1000));
                                                    obj2019.data.push(parseInt(response1.data[i].fallecidos_2019_1000));
                                                    obj2020.data.push(parseInt(response1.data[i].fallecidos_2020_1000));
                                                    obj2021.data.push(parseInt(response1.data[i].fallecidos_2021_1000));
                                                    total.data.push(parseInt(response1.data[i].fatotalp20_21));
                                                }
                                            }
                                            var total = [obj2018, obj2019, obj2020, obj2021, total];
                                            var multibar2 = multibar;
                                            multibar2.xAxis.categories = categorias;
                                            multibar2.title.text = 'Distribución anual de Fallecidos' + document.getElementById("txttipoCIPRLcANONM").value + ' por provincias, región ' + event.point.category;
                                            multibar2.series = total;
                                            if (id == 'fallecidos-tab') {
                                                multibar2.yAxis.title.text = 'Fallecidos';
                                            } else {
                                                multibar2.yAxis.title.text = 'Fallecidos/Poblacion';
                                            }
                                            multibar2.plotOptions.series.point.events.click = function(event) {
                                                document.getElementById("mostarchar3").style.display = "";
                                                var obj1 = {
                                                    method: 'chartciprliformacionSinadefDistrito',
                                                    provincia: event.point.category,
                                                    region: obj.departamento,

                                                }
                                                $.getJSON({
                                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                                    success: function(response2) {
                                                        console.log(response2);
                                                        var categorias = [];
                                                        var obj2018 = {
                                                            name: '2018',
                                                            data: [],
                                                            visible: false,
                                                            color: '#A61A1A'
                                                        }
                                                        var obj2019 = {
                                                            name: '2019',
                                                            data: [],
                                                            visible: false,
                                                            color: '#8BBC21'
                                                        }
                                                        var obj2020 = {
                                                            name: '2020',
                                                            data: [],
                                                            visible: false,
                                                            color: '#F28F43'
                                                        }
                                                        var obj2021 = {
                                                            name: '2021',
                                                            data: [],
                                                            visible: false,
                                                            color: '#000'
                                                        }
                                                        var total = {
                                                            name: 'Total',
                                                            data: [],

                                                            color: '#4897f1'
                                                        }
                                                        for (i = 0; i < response2.data.length; i++) {
                                                            categorias.push(response2.data[i].distrito);

                                                            if (id == 'fallecidos-tab') {
                                                                obj2018.data.push(parseInt(response2.data[i].anio2018));
                                                                obj2019.data.push(parseInt(response2.data[i].anio2019));
                                                                obj2020.data.push(parseInt(response2.data[i].anio2020));
                                                                obj2021.data.push(parseInt(response2.data[i].anio2021));
                                                                total.data.push(parseInt(response2.data[i].anio2018) + parseInt(response2.data[i].anio2019) + parseInt(response2.data[i].anio2020) + parseInt(response2.data[i].anio2021));
                                                                if (response2.data[i].anio2018 == null) {
                                                                    response2.data[i].anio2018 = "0";
                                                                }
                                                                if (response2.data[i].anio2019 == null) {
                                                                    response2.data[i].anio2019 = "0";
                                                                }
                                                                if (response2.data[i].anio2020 == null) {
                                                                    response2.data[i].anio2020 = "0";
                                                                }
                                                                if (response2.data[i].anio2021 == null) {
                                                                    response2.data[i].anio2021 = "0";
                                                                }
                                                            } else {
                                                                if (response2.data[i].anio2018 == null) {
                                                                    response2.data[i].anio2018 = "0";
                                                                }
                                                                if (response2.data[i].anio2019 == null) {
                                                                    response2.data[i].anio2019 = "0";
                                                                }
                                                                if (response2.data[i].anio2020 == null) {
                                                                    response2.data[i].anio2020 = "0";
                                                                }
                                                                if (response2.data[i].anio2021 == null) {
                                                                    response2.data[i].anio2021 = "0";
                                                                }
                                                                obj2018.data.push(parseInt(response2.data[i].anio2018) / parseInt(response2.data[i].poblacion) * 10000);
                                                                obj2019.data.push(parseInt(response2.data[i].anio2019) / parseInt(response2.data[i].poblacion) * 10000);
                                                                obj2020.data.push(parseInt(response2.data[i].anio2020) / parseInt(response2.data[i].poblacion) * 10000);
                                                                obj2021.data.push(parseInt(response2.data[i].anio2021) / parseInt(response2.data[i].poblacion) * 10000);
                                                                total.data.push((parseInt(response2.data[i].anio2018) / parseInt(response2.data[i].poblacion) * 10000) + (parseInt(response2.data[i].anio2019) / parseInt(response2.data[i].poblacion) * 10000) + (parseInt(response2.data[i].anio2020) / parseInt(response2.data[i].poblacion) * 10000) + (parseInt(response2.data[i].anio2021) / parseInt(response2.data[i].poblacion)) * 10000);

                                                            }
                                                        }
                                                        var total = [obj2018, obj2019, obj2020, obj2021, total];


                                                        var multibar3 = multibar;
                                                        multibar3.xAxis.categories = categorias;
                                                        multibar3.title.text = 'Distribución anual de fallecidos SINADEF por distritos, provincia ' + event.point.category + ', región ' + obj.departamento;
                                                        multibar3.series = total;
                                                        if (id == 'fallecidos-tab') {
                                                            multibar3.yAxis.title.text = 'Fallecidos';
                                                        } else {
                                                            multibar3.yAxis.title.text = 'Fallecidos/Poblacion';
                                                        }
                                                        multibar3.plotOptions.series.point.events.click = function(event) {

                                                        };
                                                        Highcharts.chart('chart_3', multibar3);
                                                    }
                                                });
                                            }
                                            Highcharts.chart('chart_2', multibar2);
                                        }
                                    });
                                }
                                Highcharts.chart('chart1', multibar1);
                            }
                        });
                        var obj = {
                            titulo: 'Sinadef'
                        }

                        $.post({
                            url: 'divresrmenyestadisticasSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getResumenSalud').innerHTML = response;
                            }
                        });
                    }
                });

            }
        }
        if (params.classList.contains('ddlEstadisticaEmpresa')) {

            $('#ddlEstadisticaEmpresa').change(function() {
                var ddl = $("#ddlEstadisticaEmpresa").find(":selected").attr("value");
                var obj = {
                    id: ddl
                }
                var nombre = "";
                if (ddl == 1) {
                    nombre = 'IBT HEALTH SOCIEDAD ANONIMA CERRADA - IBT HEALTH S.A.C.';
                } else if (ddl == 2) {
                    nombre = 'IBT, LLC SUCURSAL DEL PERU';
                } else if (ddl == 3) {
                    nombre = 'VILLA MARIA DEL TRIUNFO SALUD S.A.C';
                } else if (ddl == 4) {
                    nombre = 'CALLAO SALUD S.A.C';
                }
                $.post({
                    url: 'divresrmenyestadisticasEmpresas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                    success: function(response) {
                        document.getElementById('getDivEmpresasEstadistica').innerHTML = response;
                        if (ddl != 0) {
                            var obj = {
                                method: 'chartEmpresasEstadisticaFinanciera',
                                id: ddl
                            }
                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response1) {
                                    var categorias = ['2015', '2016', '2017', '2018', '2019'];
                                    var obj1 = {
                                        name: 'VENTAS',
                                        data: []
                                    }
                                    var obj2 = {
                                        name: 'EBITDA',
                                        color: '#d11507',
                                        data: []
                                    }
                                    var obj3 = {
                                        name: 'UTILIDAD NETA',
                                        color: '#14f894',
                                        data: []
                                    }
                                    var obj4 = {
                                        name: 'ACTIVO CORRIENTE',
                                        color: '#0678b7',
                                        data: []
                                    }
                                    var obj5 = {
                                        name: 'ACTIVO NO CORRIENTE',
                                        color: '#75cbfb',
                                        data: []
                                    }
                                    var obj6 = {
                                        name: 'PASIVO CORRIENTE',
                                        color: '#ae0519',
                                        data: []
                                    }
                                    var obj7 = {
                                        name: 'PASIVO NO CORRIENTE',
                                        color: '#fa6783',
                                        data: []
                                    }
                                    var obj8 = {
                                        name: 'PATRIMONIO TOTAL',
                                        color: '#f7d12b',
                                        data: []
                                    }
                                    var obj9 = {
                                        name: 'DEUDA DIRECTA SBS',
                                        color: '#b8f95c',
                                        data: []
                                    }
                                    var obj10 = {
                                        name: 'DEUDA INDIRECTA SBS (CARTAS FIANZA)',
                                        color: '#285700',
                                        data: []
                                    }


                                    for (i = 0; i < response1.data.length; i++) {
                                        if (response1.data[i].idanalisis == 1) {
                                            obj1.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 2) {
                                            obj2.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 3) {
                                            obj3.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 4) {
                                            obj4.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 5) {
                                            obj5.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 6) {
                                            obj6.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 7) {
                                            obj7.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 8) {
                                            obj8.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 9) {
                                            obj9.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        } else if (response1.data[i].idanalisis == 10) {
                                            obj10.data.push(parseInt(response1.data[i].a2015), parseInt(response1.data[i].a2016), parseInt(response1.data[i].a2017), parseInt(response1.data[i].a2018), parseInt(response1.data[i].a2019));
                                        }


                                    }
                                    var total = [obj1, obj2, obj3];
                                    var multibar2 = multibar;
                                    multibar2.xAxis.categories = categorias;
                                    multibar2.title.text = nombre;
                                    multibar2.series = total;
                                    multibar2.subtitle.text = '';
                                    multibar2.yAxis.title.text = ""
                                    Highcharts.chart('chartEmpresasAnalisi', multibar2);

                                    var total1 = [obj4, obj5, obj6, obj7, obj8, obj9, obj10];
                                    var multibar3 = multibar;
                                    multibar3.xAxis.categories = categorias;
                                    multibar3.title.text = nombre;
                                    multibar3.series = total1;
                                    multibar3.subtitle.text = '';
                                    multibar3.yAxis.title.text = ""
                                    Highcharts.chart('chartEmpresasAnalisi1', multibar3);
                                }
                            });
                        }

                    }
                });
            });
        }
        if (params.classList.contains('drawObjetoList')) {
            var id = params.getAttribute('id');
            var array = id.split('&');

            var obj = {
                method: array[1] == 'Poligono' ? 'getRectanguloDraw' : 'getCirculoDraw',
                id: array[0]
            }
            $.getJSON({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    var tmp = [];
                    objDrawLayerGroup = L.layerGroup().addTo(map);
                    if (obj.method == 'getRectanguloDraw') {

                        var tmp = [];
                        var tmp1 = [];
                        var tmp2 = [];
                        response.data[0].lats.split(',').forEach(function(row) {
                            tmp.push(parseFloat(row));
                        });

                        response.data[0].lngs.split(',').forEach(function(row) {
                            tmp1.push(parseFloat(row));
                        });
                        for (i = 0; i < tmp.length; i++) {
                            tmp2.push([tmp[i], tmp1[i]]);
                        }

                        var poly = new L.polygon(tmp2);
                        poly.on('click', function(e) {


                        });
                        poly.addTo(objDrawLayerGroup)


                    } else {
                        L.circle(L.latLng(response.data[0].lat, response.data[0].lng, 5), parseFloat(response.data[0].radion), {
                            opacity: 0.5,
                            weight: 1,
                            fillOpacity: 0.4
                        }).addTo(objDrawLayerGroup);
                    }
                }
            });
        }

        //clic cabecera popup
        if (params.classList.contains('tabEstadistica')) {
            var id = params.getAttribute('id');
            changeTabPanelEstadistica(id);

            if (id == 'ciprl-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'CIPRL';

            } else if (id == 'cannon-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'CANON';

            } else if (id == 'minsa-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'MINSA';

            } else if (id == 'sinadef-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'SINADEF';

            } else if (id == 'proyectado-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'PROYECTADO';

            } else if (id == 'empresas-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'EMPRESAS';

            } else if (id == 'macro-tab') {
                document.getElementById("txttipoCIPRLcANONM").value = 'MACROINDICADORES';
            }
            var obj = {
                titulo: document.getElementById("txttipoCIPRLcANONM").value,
            }

            $.post({
                url: 'divresrmenyestadisticas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('divresumeninformacion').innerHTML = response;
                    if (document.getElementById("txttipoCIPRLcANONM").value == 'CANON') {
                        var obj = {
                            method: 'chartciprliformacion'
                        }

                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                var categorias = [];
                                var obj2018 = {
                                    name: '2018',
                                    data: []
                                }
                                var obj2019 = {
                                    name: '2019',
                                    data: []
                                }
                                var obj2020 = {
                                    name: '2020',
                                    data: []
                                }
                                for (i = 0; i < response.data.length; i++) {
                                    categorias.push(response.data[i].departamento);
                                    if (id == 'ciprl-tab') {
                                        obj2018.data.push(Number((parseInt(response.data[i].ciprl2018) / 1000000).toFixed(3)));
                                        obj2019.data.push(Number((parseInt(response.data[i].ciprl2019) / 1000000).toFixed(3)));
                                        obj2020.data.push(Number((parseInt(response.data[i].ciprl2020) / 1000000).toFixed(3)));

                                    } else {
                                        obj2018.data.push(Number((parseInt(response.data[i].canon2018) / 1000000).toFixed(3)));
                                        obj2019.data.push(Number((parseInt(response.data[i].canon2019) / 1000000).toFixed(3)));
                                        obj2020.data.push(Number((parseInt(response.data[i].canon2020) / 1000000).toFixed(3)));
                                    }
                                }
                                var total = [obj2018, obj2019, obj2020];
                                var multibar1 = multibar;
                                multibar.xAxis.categories = categorias;
                                multibar.title.text = 'Distribucion de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por Regiones';
                                multibar.series = total;
                                multibar.subtitle.text = '(millones de soles)'
                                multibar.plotOptions.series.point.events.click = function(event) {
                                    document.getElementById("mostarchar2").style.display = "";

                                    var obj = {
                                        method: 'chartciprliformacionProvincia',
                                        departamento: event.point.category
                                    }
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response1) {
                                            var categorias = [];
                                            var obj2018 = {
                                                name: '2018',
                                                data: []
                                            }
                                            var obj2019 = {
                                                name: '2019',
                                                data: []
                                            }
                                            var obj2020 = {
                                                name: '2020',
                                                data: []
                                            }
                                            for (i = 0; i < response1.data.length; i++) {
                                                categorias.push(response1.data[i].nombprov);
                                                if (id == 'ciprl-tab') {
                                                    obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                                    obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                                    obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                                } else {
                                                    obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                                    obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                                    obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                                }
                                            }
                                            var total = [obj2018, obj2019, obj2020];
                                            multibar1.xAxis.categories = categorias;
                                            multibar1.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por provincias, región ' + event.point.category;
                                            multibar1.series = total;
                                            multibar1.subtitle.text = '(millones de soles)'
                                            multibar1.plotOptions.series.point.events.click = function(event) {
                                                document.getElementById("mostarchar3").style.display = "";
                                                var obj1 = {
                                                    method: 'chartciprliformacionDistrito',
                                                    provincia: event.point.category,
                                                    region: obj.departamento
                                                }
                                                $.getJSON({
                                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                                    success: function(response1) {
                                                        var categorias = [];
                                                        var obj2018 = {
                                                            name: '2018',
                                                            data: []
                                                        }
                                                        var obj2019 = {
                                                            name: '2019',
                                                            data: []
                                                        }
                                                        var obj2020 = {
                                                            name: '2020',
                                                            data: []
                                                        }
                                                        for (i = 0; i < response1.data.length; i++) {
                                                            categorias.push(response1.data[i].distrito);
                                                            if (id == 'ciprl-tab') {
                                                                obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                                                obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                                                obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                                            } else {
                                                                obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                                                obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                                                obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                                            }
                                                        }
                                                        var total = [obj2018, obj2019, obj2020];
                                                        var multibar2 = multibar;
                                                        multibar2.xAxis.categories = categorias;
                                                        multibar2.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por distritos, provincia ' + event.point.category + ', región ' + obj.departamento;
                                                        multibar2.series = total;
                                                        multibar2.subtitle.text = '(millones de soles)';
                                                        multibar2.plotOptions.series.point.events.click = function(event) {
                                                            console.log('erfgh');
                                                        };
                                                        Highcharts.chart('chart_3', multibar2);
                                                    }
                                                });
                                            }
                                            Highcharts.chart('chart_2', multibar1);

                                        }
                                    });
                                }
                                Highcharts.chart('chart1', multibar);
                            }
                        });
                    } else if (document.getElementById("txttipoCIPRLcANONM").value == 'MINSA') {
                        var obj = {
                            tab: 'resumen-tab',
                            tipo: 'COVID'
                        }
                        console.log("aqui entro");

                        $.post({
                            url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('divresumeninformacion123').innerHTML = response;
                                var obj = {
                                    method: 'quintiles_minsa_tab',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response1) {
                                        console.log(response1);
                                        var categorias = [];
                                        var categorias1 = [];
                                        var categorias2 = [];
                                        var obj1 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj1_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };
                                        var obj2 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };
                                        var obj3 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj3_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            if (response1.data[i].tipo == 1) {
                                                categorias.push(response1.data[i].texto);
                                                obj1.data.push(parseInt(response1.data[i].entidades));
                                                obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            } else if (response1.data[i].tipo == 2) {
                                                categorias1.push(response1.data[i].texto);
                                                obj2.data.push(parseInt(response1.data[i].entidades));
                                                obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            } else if (response1.data[i].tipo == 3) {
                                                categorias2.push(response1.data[i].texto);
                                                obj3.data.push(parseInt(response1.data[i].entidades));
                                                obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            }

                                        }
                                        var total = [obj3, obj3_Pob];
                                        var Chart3 = dualChart;
                                        Chart3.xAxis[0].categories = categorias1;
                                        Chart3.title.text = 'Fallecidos COVID-19 Distrito';
                                        Chart3.series = total;
                                        Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef3', Chart3);
                                        var total = [obj2, obj2_Pob];
                                        var Chart2 = dualChart;
                                        Chart2.xAxis[0].categories = categorias1;
                                        Chart2.title.text = 'Fallecidos COVID-19 Provincia';
                                        Chart2.series = total;
                                        Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef2', Chart2);
                                        var total = [obj1, obj1_Pob];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos COVID-19 Region';
                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef1', Chart1);
                                    }
                                });
                                // var obj = {
                                //     method: 'quintiles_mes_anual_covid',
                                // }
                                // $.getJSON({
                                //     url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                //     success: function(response1) {
                                //         console.log(response1);
                                //         var categorias = [];
                                //         var obj1 = {
                                //             name: '2020',
                                //             type: 'column',
                                //             yAxis: 0,
                                //             color: '#2f7ed8',
                                //             data: []
                                //         };

                                //         for (i = 0; i < response1.data.length; i++) {

                                //             categorias.push(response1.data[i].meses);
                                //             obj1.data.push(parseInt(response1.data[i].fallecidos));
                                //         }
                                //         var total = [obj1];

                                //         var Chart1 = dualChart;
                                //         Chart1.xAxis[0].categories = categorias;
                                //         Chart1.title.text = 'Fallecidos COVID a nivel nacional';
                                //         Chart1.subtitle.text = '(2020)';
                                //         Chart1.series = total;
                                //         Highcharts.chart('chartSinadef4', Chart1);

                                //     }
                                // });
                                var obj = {
                                    method: 'quintiles_mes_anual_covid',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response1) {
                                        console.log(response1);
                                        var categorias = [];

                                        var obj4 = {
                                            name: '2020',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#F28F43',
                                            data: []
                                        };
                                        var obj5 = {
                                            name: '2021',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#000',
                                            data: []
                                        };
                                        var obj6 = {
                                            name: 'proyectado',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: 'red',
                                            data: []
                                        };
                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].meses);
                                            obj4.data.push(parseInt(response1.data[i].s2020));
                                            if (response1.data[i].s2021 != 0) {
                                                obj5.data.push(parseInt(response1.data[i].s2021));
                                            }

                                        }
                                        obj6.data.push(3457);
                                        obj6.data.push(6278);
                                        obj6.data.push(9938);
                                        obj6.data.push(11838);

                                        var total = [obj4, obj6, obj5];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos Covid a nivel nacional';
                                        Chart1.subtitle.text = '(2020-2021)';
                                        Chart1.series = total;
                                        Highcharts.chart('chartSinadef4', Chart1);
                                    }
                                });
                                var obj = {
                                    method: 'quintil_semanal_covidgrafico',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response1) {
                                        console.log(response1);
                                        var categorias = [];

                                        var obj4 = {
                                            name: '2020',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#F28F43',
                                            data: []
                                        };
                                        var obj5 = {
                                            name: '2021',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#000',
                                            data: []
                                        };
                                        var obj6 = {
                                            name: 'proyectado',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: 'red',
                                            data: []
                                        };
                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].semana);

                                            obj4.data.push(parseInt(response1.data[i].anio2020));
                                            if (response1.data[i].s2021 != 0 && i <= 6) {
                                                obj5.data.push(parseInt(response1.data[i].anio2021));
                                            }
                                            if (i <= 16) {
                                                obj6.data.push(parseInt(response1.data[i].anio2021))
                                            }
                                        }
                                        var total = [obj4, obj6, obj5];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos Covid a nivel nacional';
                                        Chart1.subtitle.text = '(2020-2021)';
                                        Chart1.series = total;
                                        Highcharts.chart('chartSinadef5', Chart1);
                                        console.log(total);

                                    }
                                });

                                var obj = {
                                    tipo: 'covid',
                                }
                                $.post({
                                    url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('getResumenSalud').innerHTML = response;
                                        map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                        }).addTo(map3);
                                        tile4 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_minsa_departamentos',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map3);
                                        map3.invalidateSize();
                                        map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                        }).addTo(map4);
                                        tile5 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_minsa_provincias',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map4);
                                        map4.invalidateSize();
                                        map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                        tile6 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_minsa_distritos',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map5);
                                        map5.invalidateSize();
                                    }
                                });

                            }
                        });

                    } else if (document.getElementById("txttipoCIPRLcANONM").value == 'SINADEF') {
                        var obj = {
                            tab: 'resumen-tab',
                            tipo: 'SINADEF'
                        }
                        $.post({
                            url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('divresumeninformacion123').innerHTML = response;
                                var obj = {
                                    method: 'quintiles_sinadef_tab',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response1) {
                                        console.log(response1);
                                        var categorias = [];
                                        var categorias1 = [];
                                        var categorias2 = [];
                                        var obj1 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj1_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };
                                        var obj2 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };
                                        var obj3 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj3_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            if (response1.data[i].tipo == 1) {
                                                categorias.push(response1.data[i].texto);
                                                obj1.data.push(parseInt(response1.data[i].entidades));
                                                obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            } else if (response1.data[i].tipo == 2) {
                                                categorias1.push(response1.data[i].texto);
                                                obj2.data.push(parseInt(response1.data[i].entidades));
                                                obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            } else if (response1.data[i].tipo == 3) {
                                                categorias2.push(response1.data[i].texto);
                                                obj3.data.push(parseInt(response1.data[i].entidades));
                                                obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                            }

                                        }
                                        var total = [obj3, obj3_Pob];
                                        var Chart3 = dualChart;
                                        Chart3.xAxis[0].categories = categorias1;
                                        Chart3.title.text = 'Fallecidos SINADEF Distrito';
                                        Chart3.series = total;
                                        Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef3', Chart3);
                                        var total = [obj2, obj2_Pob];
                                        var Chart2 = dualChart;
                                        Chart2.xAxis[0].categories = categorias1;
                                        Chart2.title.text = 'Fallecidos SINADEF Provincia';
                                        Chart2.series = total;
                                        Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef2', Chart2);
                                        var total = [obj1, obj1_Pob];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos SINADEF Region';
                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadef1', Chart1);
                                    }
                                });
                                var obj = {
                                    method: 'quintiles_mes_anual_sinadef',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response1) {
                                        console.log(response1);
                                        var categorias = [];

                                        var obj2 = {
                                            name: '2018',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#A61A1A',
                                            data: []
                                        };
                                        var obj3 = {
                                            name: '2019',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#8BBC21',
                                            data: []
                                        };
                                        var obj4 = {
                                            name: '2020',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#F28F43',
                                            data: []
                                        };
                                        var obj5 = {
                                            name: '2021',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#000',
                                            data: []
                                        };
                                        var obj6 = {
                                            name: 'proyectado',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: 'red',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {

                                            categorias.push(response1.data[i].mes);
                                            obj2.data.push(parseInt(response1.data[i].anio2018));
                                            obj3.data.push(parseInt(response1.data[i].anio2019));
                                            obj4.data.push(parseInt(response1.data[i].anio2020));

                                            if (response1.data[i].anio2021 != 0 && i == 0) {
                                                obj5.data.push(parseInt(response1.data[i].anio2021));
                                            }


                                        }
                                        obj6.data.push(21886);
                                        obj6.data.push(28849);
                                        obj6.data.push(41875);
                                        obj6.data.push(47661);
                                        var total = [obj2, obj3, obj4, obj6, obj5];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos Sindef a nivel nacional';
                                        Chart1.subtitle.text = '(2018-2021)';
                                        Chart1.series = total;
                                        Highcharts.chart('chartSinadef4', Chart1);
                                        console.log(total);
                                    }
                                });
                                var obj = {
                                    method: 'quintiles_sem_anual_sinadef',
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response2) {
                                        console.log(response2);
                                        var categorias = [];

                                        var obj2 = {
                                            name: '2018',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#A61A1A',
                                            data: []
                                        };
                                        var obj3 = {
                                            name: '2019',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#8BBC21',
                                            data: []
                                        };
                                        var obj4 = {
                                            name: '2020',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#F28F43',
                                            data: []
                                        };
                                        var obj5 = {
                                            name: '2021',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: '#000',
                                            data: []
                                        };
                                        var obj6 = {
                                            name: 'proyectado',
                                            type: 'spline',
                                            yAxis: 1,
                                            color: 'red',
                                            data: []
                                        };

                                        for (i = 0; i < response2.data.length; i++) {

                                            categorias.push(response2.data[i].semana);
                                            obj2.data.push(parseInt(response2.data[i].anio2018));
                                            obj3.data.push(parseInt(response2.data[i].anio2019));
                                            // obj4.data.push(parseInt(response2.data[i].anio2020));

                                            // if (response2.data[i].anio2021 != 0 && i == 0) {
                                            //     obj5.data.push(parseInt(response2.data[i].anio2021));
                                            // }

                                            obj4.data.push(parseInt(response2.data[i].anio2020));
                                            if (response2.data[i].s2021 != 0 && i <= 5) {
                                                obj5.data.push(parseInt(response2.data[i].anio2021));
                                            }
                                            if (i <= 17) {
                                                obj6.data.push(parseInt(response2.data[i].anio2021))
                                            }
                                        }
                                        // obj6.data.push(21886);
                                        // obj6.data.push(28849);
                                        // obj6.data.push(41875);
                                        // obj6.data.push(47661);
                                        var total = [obj2, obj3, obj4, obj6, obj5];
                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos Sindef a nivel nacional';
                                        Chart1.subtitle.text = '(2018-2021)';
                                        Chart1.series = total;
                                        Highcharts.chart('chartSinadef5', Chart1);
                                        console.log(total);
                                    }
                                });

                                var obj = {
                                    tipo: 'sinadef',
                                }
                                $.post({
                                    url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('getResumenSalud').innerHTML = response;
                                        map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                        }).addTo(map3);
                                        tile1 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_departamentos',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map3);
                                        map3.invalidateSize();
                                        map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                        }).addTo(map4);
                                        tile2 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_provincias',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map4);
                                        map4.invalidateSize();
                                        map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                        tile3 = L.tileLayer.betterWms(wmsrootUrl, {
                                            layers: 'colaboraccion_2020:gen_fallecidos_sinadef_distritos',
                                            tiled: true,
                                            format: 'image/png',
                                            minZoom: 0,
                                            continuousWorld: true,
                                            transparent: true
                                        }).addTo(map5);
                                        map5.invalidateSize();
                                    }
                                });

                            }
                        });



                    } else if (document.getElementById("txttipoCIPRLcANONM").value == 'PROYECTADO') {
                        var obj = {
                            method: 'proyectados_fallecidos_anio'
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {

                                var ob1 = {
                                    name: 'Fallecidos',
                                    color: '#4F81BD',
                                    data: [parseInt(response1.data[0].a2018), parseInt(response1.data[0].a2019), parseInt(response1.data[0].a2019)]
                                }
                                var ob2 = {
                                    name: 'Diferencia',
                                    color: '#C0504D',
                                    data: [0, 0, parseInt(response1.data[0].diferencia)]
                                }

                                var Chart = stackedBarProyectado;
                                Chart.xAxis.categories = ['Año 2018', 'Año 2019', 'Año 2020'];
                                Chart.title.text = 'Fallecidos durante periodo de pandemia (01 Ene - 31 Dic)';
                                Chart.subtitle.text = 'años 2018, 2019 y 2020*';
                                Chart.series = [ob2, ob1];
                                Highcharts.chart('chart2', Chart);
                            }
                        });

                        var obj = {
                            method: 'proyectado_fallecidos'
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response1) {
                                var categorias = [];
                                var obj1 = {
                                    name: 'Prom. diario (semanal COVID)',
                                    type: 'column',
                                    yAxis: 1,
                                    color: '#3e5f8a',
                                    data: []
                                }
                                var obj2 = {
                                    name: 'Prom. diario (semanal SINADEF)',
                                    type: 'column',
                                    yAxis: 1,

                                    color: '#d11507',
                                    data: []
                                }
                                var obj3 = {
                                    name: 'Proyección semanal COVID',
                                    type: 'spline',
                                    color: '#328dd8',
                                    data: []
                                }
                                var obj4 = {
                                    name: 'Proyección semanal SINADEF',
                                    type: 'spline',
                                    color: '#cc6600',
                                    data: []
                                }

                                for (i = 0; i < response1.data.length; i++) {
                                    categorias.push(response1.data[i].periodo);
                                    obj1.data.push(parseInt(response1.data[i].prom_diario_covid));
                                    obj2.data.push(parseInt(response1.data[i].prom_diario_sinadef));
                                    obj3.data.push(parseInt(response1.data[i].proyeccion_covid));
                                    obj4.data.push(parseInt(response1.data[i].proyeccion_sinadef));

                                }
                                var total = [obj1, obj2, obj3, obj4];



                                var Chart = dualChart;
                                Chart.xAxis[0].categories = categorias;
                                Chart.title.text = 'Comparativo de Fallecidos en PERU en pandemia COVID19, promedio diario de Fallecidos semanal y acumulado SINADEF (2020 menos 2019) vs COVID MINSA';
                                Chart.subtitle.text = 'Setiembre a Diciembre 2020';
                                Chart.series = total;
                                Highcharts.chart('chart1', Chart);
                            }
                        });
                    } else if (document.getElementById("txttipoCIPRLcANONM").value == 'CIPRL') {
                        var obj = {
                            method: 'chartciprliformacion'
                        }
                        $.getJSON({
                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                var categorias = [];
                                var obj2018 = {
                                    name: '2018',
                                    data: []
                                }
                                var obj2019 = {
                                    name: '2019',
                                    data: []
                                }
                                var obj2020 = {
                                    name: '2020',
                                    data: []
                                }
                                for (i = 0; i < response.data.length; i++) {
                                    categorias.push(response.data[i].departamento);
                                    if (id == 'ciprl-tab') {
                                        obj2018.data.push(Number((parseInt(response.data[i].ciprl2018) / 1000000).toFixed(2)));
                                        obj2019.data.push(Number((parseInt(response.data[i].ciprl2019) / 1000000).toFixed(2)));
                                        obj2020.data.push(Number((parseInt(response.data[i].ciprl2020) / 1000000).toFixed(2)));

                                    } else {
                                        obj2018.data.push(Number((parseInt(response.data[i].canon2018) / 1000000).toFixed(2)));
                                        obj2019.data.push(Number((parseInt(response.data[i].canon2019) / 1000000).toFixed(2)));
                                        obj2020.data.push(Number((parseInt(response.data[i].canon2020) / 1000000).toFixed(2)));
                                    }

                                }
                                var total = [obj2018, obj2019, obj2020];
                                var multibar1 = multibar;
                                multibar.xAxis.categories = categorias;
                                multibar.title.text = 'Distribucion de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por Regiones';
                                multibar.series = total;
                                multibar.subtitle.text = '(millones de soles)'
                                multibar.plotOptions.series.point.events.click = function(event) {
                                    document.getElementById("mostarchar2").style.display = "";

                                    var obj = {
                                        method: 'chartciprliformacionProvincia',
                                        departamento: event.point.category
                                    }
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response1) {
                                            var categorias = [];
                                            var obj2018 = {
                                                name: '2018',
                                                data: []
                                            }
                                            var obj2019 = {
                                                name: '2019',
                                                data: []
                                            }
                                            var obj2020 = {
                                                name: '2020',
                                                data: []
                                            }
                                            for (i = 0; i < response1.data.length; i++) {
                                                categorias.push(response1.data[i].nombprov);
                                                if (id == 'ciprl-tab') {
                                                    obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                                    obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                                    obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                                } else {
                                                    obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                                    obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                                    obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                                }
                                            }
                                            var total = [obj2018, obj2019, obj2020];
                                            multibar1.xAxis.categories = categorias;
                                            multibar1.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por provincias, región ' + event.point.category;
                                            multibar1.series = total;
                                            multibar1.subtitle.text = '(millones de soles)'
                                            multibar1.plotOptions.series.point.events.click = function(event) {
                                                document.getElementById("mostarchar3").style.display = "";
                                                var obj1 = {
                                                    method: 'chartciprliformacionDistrito',
                                                    provincia: event.point.category,
                                                    region: obj.departamento
                                                }
                                                $.getJSON({
                                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                                    success: function(response1) {
                                                        var categorias = [];
                                                        var obj2018 = {
                                                            name: '2018',
                                                            data: []
                                                        }
                                                        var obj2019 = {
                                                            name: '2019',
                                                            data: []
                                                        }
                                                        var obj2020 = {
                                                            name: '2020',
                                                            data: []
                                                        }
                                                        for (i = 0; i < response1.data.length; i++) {
                                                            categorias.push(response1.data[i].distrito);
                                                            if (id == 'ciprl-tab') {
                                                                obj2018.data.push(Number((parseInt(response1.data[i].ciprl2018) / 1000000).toFixed(2)));
                                                                obj2019.data.push(Number((parseInt(response1.data[i].ciprl2019) / 1000000).toFixed(2)));
                                                                obj2020.data.push(Number((parseInt(response1.data[i].ciprl2020) / 1000000).toFixed(2)));

                                                            } else {
                                                                obj2018.data.push(Number((parseInt(response1.data[i].canon2018) / 1000000).toFixed(2)));
                                                                obj2019.data.push(Number((parseInt(response1.data[i].canon2019) / 1000000).toFixed(2)));
                                                                obj2020.data.push(Number((parseInt(response1.data[i].canon2020) / 1000000).toFixed(2)));
                                                            }
                                                        }
                                                        var total = [obj2018, obj2019, obj2020];
                                                        var multibar2 = multibar;
                                                        multibar2.xAxis.categories = categorias;
                                                        multibar2.title.text = 'Distribución anual de ' + document.getElementById("txttipoCIPRLcANONM").value + ' por distritos, provincia ' + event.point.category + ', región ' + obj.departamento;
                                                        multibar2.series = total;
                                                        multibar2.subtitle.text = '(millones de soles)';
                                                        multibar2.plotOptions.series.point.events.click = function(event) {
                                                            console.log('erfgh');
                                                        };
                                                        Highcharts.chart('chart_3', multibar2);
                                                    }
                                                });
                                            }
                                            Highcharts.chart('chart_2', multibar1);

                                        }
                                    });
                                }
                                Highcharts.chart('chart1', multibar);
                            }
                        });
                        var obj = {
                            titulo: 'Ciprl',
                            tabla: 'gobiernos'
                        }

                        $.post({
                            url: 'divresrmenyestadisticasSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {

                                document.getElementById('getResumenSalud').innerHTML = response;
                            }
                        });
                    } else if (document.getElementById("txttipoCIPRLcANONM").value == 'MACROINDICADORES') {
                        var obj = {
                            tipo: 'region',
                            forma: 'asc'
                        }
                        $.post({
                            url: 'divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getDivEmpresasEstadistica').innerHTML = response;


                            }
                        });
                        var obj = {
                            tipo: 'funcion',
                            forma: 'asc'
                        }
                        $.post({
                            url: 'divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                document.getElementById('getDivEmpresasEstadistica1').innerHTML = response;


                            }
                        });
                    }

                }
            });
        }
        if (params.classList.contains('lnkAmpliarSalud')) {
            var id = params.getAttribute('id');
            switch (id) {
                case 'sinadef':
                    var obj = {
                        tipo: 'SINADEF',
                    }
                    $.post({
                        url: 'popupindicadoresycredenciales.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            dialog7.setLocation([90, 90]);
                            dialog7.setContent(response);
                            var obj = {
                                titulo: 'SINADEF'
                            }
                            changeTabPanelEstadistica('sinadef-tab');
                            $.post({
                                url: 'divresrmenyestadisticas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    document.getElementById('divresumeninformacion').innerHTML = response;

                                    var obj = {
                                        tab: 'resumen-tab',
                                        tipo: 'SINADEF'
                                    }
                                    $.post({
                                        url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response) {
                                            console.log(response);
                                            document.getElementById('divresumeninformacion123').innerHTML = response;
                                            var obj = {
                                                method: 'quintiles_mes_anual_sinadef',
                                            }
                                            $.getJSON({
                                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response1) {
                                                    console.log(response1);
                                                    var categorias = [];
                                                    var obj1 = {
                                                        name: '2017',
                                                        type: 'spline',
                                                        yAxis: 1,
                                                        color: '#2f7ed8',
                                                        data: []
                                                    };
                                                    var obj2 = {
                                                        name: '2018',
                                                        type: 'spline',
                                                        yAxis: 1,
                                                        color: '#A61A1A',
                                                        data: []
                                                    };
                                                    var obj3 = {
                                                        name: '2019',
                                                        type: 'spline',
                                                        yAxis: 1,
                                                        color: '#8BBC21',
                                                        data: []
                                                    };
                                                    var obj4 = {
                                                        name: '2020',
                                                        type: 'spline',
                                                        yAxis: 1,
                                                        color: '#F28F43',
                                                        data: []
                                                    };
                                                    var obj5 = {
                                                        name: '2021',
                                                        type: 'spline',
                                                        yAxis: 1,
                                                        color: '#000',
                                                        data: []
                                                    };

                                                    for (i = 0; i < response1.data.length; i++) {

                                                        categorias.push(response1.data[i].meses);
                                                        obj1.data.push(parseInt(response1.data[i].s2017));
                                                        obj2.data.push(parseInt(response1.data[i].s2018));
                                                        obj3.data.push(parseInt(response1.data[i].s2019));
                                                        obj4.data.push(parseInt(response1.data[i].s2020));

                                                        if (response1.data[i].s2021 != 0) {
                                                            obj5.data.push(parseInt(response1.data[i].s2021));
                                                        }


                                                    }
                                                    var total = [obj1, obj2, obj3, obj4, obj5];

                                                    var Chart1 = dualChart;
                                                    Chart1.xAxis[0].categories = categorias;
                                                    Chart1.title.text = 'Fallecidos SINADEF a nivel nacional';
                                                    Chart1.subtitle.text = '(2017-2021)';
                                                    Chart1.series = total;
                                                    Highcharts.chart('chartSinadef4', Chart1);

                                                }
                                            });
                                            var obj = {
                                                method: 'quintiles_sinadef_tab',
                                            }
                                            $.getJSON({
                                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response1) {
                                                    console.log(response1);
                                                    var categorias = [];
                                                    var categorias1 = [];
                                                    var categorias2 = [];
                                                    var obj1 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj1_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };
                                                    var obj2 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj2_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };
                                                    var obj3 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj3_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };

                                                    for (i = 0; i < response1.data.length; i++) {
                                                        if (response1.data[i].tipo == 1) {
                                                            categorias.push(response1.data[i].texto);
                                                            obj1.data.push(parseInt(response1.data[i].entidades));
                                                            obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        } else if (response1.data[i].tipo == 2) {
                                                            categorias1.push(response1.data[i].texto);
                                                            obj2.data.push(parseInt(response1.data[i].entidades));
                                                            obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        } else if (response1.data[i].tipo == 3) {
                                                            categorias2.push(response1.data[i].texto);
                                                            obj3.data.push(parseInt(response1.data[i].entidades));
                                                            obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        }

                                                    }
                                                    var total = [obj3, obj3_Pob];
                                                    var Chart3 = dualChart;
                                                    Chart3.xAxis[0].categories = categorias1;
                                                    Chart3.title.text = 'Fallecidos SINADEF Distrito';
                                                    Chart3.series = total;
                                                    Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef3', Chart3);
                                                    var total = [obj2, obj2_Pob];
                                                    var Chart2 = dualChart;
                                                    Chart2.xAxis[0].categories = categorias1;
                                                    Chart2.title.text = 'Fallecidos SINADEF Provincia';
                                                    Chart2.series = total;
                                                    Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef2', Chart2);
                                                    var total = [obj1, obj1_Pob];
                                                    var Chart1 = dualChart;
                                                    Chart1.xAxis[0].categories = categorias;
                                                    Chart1.title.text = 'Fallecidos SINADEF Region';
                                                    Chart1.series = total;
                                                    Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef1', Chart1);
                                                }
                                            });
                                            var obj = {
                                                tipo: 'sinadef',
                                            }
                                            $.post({
                                                url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response) {
                                                    document.getElementById('getResumenSalud').innerHTML = response;
                                                    map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                                    }).addTo(map3);
                                                    tile1 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_sinadef_departamentos',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map3);
                                                    map3.invalidateSize();
                                                    map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                                    }).addTo(map4);
                                                    tile2 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_sinadef_provincias',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map4);
                                                    map4.invalidateSize();
                                                    map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                                    tile3 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_sinadef_distritos',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map5);
                                                    map5.invalidateSize();
                                                }
                                            });

                                        }
                                    });
                                }
                            });
                        }
                    });
                    break;
                case 'covid':
                    var obj = {
                        tipo: 'SINADEF'
                    }
                    $.post({
                        url: 'popupindicadoresycredenciales.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            dialog7.setLocation([90, 90]);
                            dialog7.setContent(response);
                            var obj = {
                                titulo: 'SINADEF'
                            }
                            changeTabPanelEstadistica('minsa-tab');
                            $.post({
                                url: 'divresrmenyestadisticas.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    document.getElementById('divresumeninformacion').innerHTML = response;
                                    var obj = {
                                        tab: 'resumen-tab',
                                        tipo: 'COVID'
                                    }
                                    $.post({
                                        url: 'divresrmenyestadisticasSubTabSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response) {
                                            document.getElementById('divresumeninformacion123').innerHTML = response;
                                            var obj = {
                                                method: 'quintiles_mes_anual_covid',
                                            }
                                            $.getJSON({
                                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response1) {
                                                    console.log(response1);
                                                    var categorias = [];
                                                    var obj1 = {
                                                        name: '2020',
                                                        type: 'column',
                                                        yAxis: 0,
                                                        color: '#2f7ed8',
                                                        data: []
                                                    };

                                                    for (i = 0; i < response1.data.length; i++) {

                                                        categorias.push(response1.data[i].meses);
                                                        obj1.data.push(parseInt(response1.data[i].fallecidos));
                                                    }
                                                    var total = [obj1];

                                                    var Chart1 = dualChart;
                                                    Chart1.xAxis[0].categories = categorias;
                                                    Chart1.title.text = 'Fallecidos COVID a nivel nacional';
                                                    Chart1.subtitle.text = '(2020)';
                                                    Chart1.series = total;
                                                    Highcharts.chart('chartSinadef4', Chart1);

                                                }
                                            });
                                            var obj = {
                                                method: 'quintiles_minsa_tab',
                                            }
                                            $.getJSON({
                                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response1) {
                                                    console.log(response1);
                                                    var categorias = [];
                                                    var categorias1 = [];
                                                    var categorias2 = [];
                                                    var obj1 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj1_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };
                                                    var obj2 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj2_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };
                                                    var obj3 = {
                                                        name: 'Entidades',
                                                        type: 'column',
                                                        yAxis: 1,
                                                        color: '#4572A7',
                                                        data: []
                                                    };
                                                    var obj3_Pob = {
                                                        name: 'Fallecidos',
                                                        type: 'spline',
                                                        color: '#A7454F',
                                                        data: []
                                                    };

                                                    for (i = 0; i < response1.data.length; i++) {
                                                        if (response1.data[i].tipo == 1) {
                                                            categorias.push(response1.data[i].texto);
                                                            obj1.data.push(parseInt(response1.data[i].entidades));
                                                            obj1_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        } else if (response1.data[i].tipo == 2) {
                                                            categorias1.push(response1.data[i].texto);
                                                            obj2.data.push(parseInt(response1.data[i].entidades));
                                                            obj2_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        } else if (response1.data[i].tipo == 3) {
                                                            categorias2.push(response1.data[i].texto);
                                                            obj3.data.push(parseInt(response1.data[i].entidades));
                                                            obj3_Pob.data.push(parseInt(response1.data[i].fallecidos));
                                                        }

                                                    }
                                                    var total = [obj3, obj3_Pob];
                                                    var Chart3 = dualChart;
                                                    Chart3.xAxis[0].categories = categorias1;
                                                    Chart3.title.text = 'Fallecidos COVID-19 Distrito';
                                                    Chart3.series = total;
                                                    Chart3.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef3', Chart3);
                                                    var total = [obj2, obj2_Pob];
                                                    var Chart2 = dualChart;
                                                    Chart2.xAxis[0].categories = categorias1;
                                                    Chart2.title.text = 'Fallecidos COVID-19 Provincia';
                                                    Chart2.series = total;
                                                    Chart2.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef2', Chart2);
                                                    var total = [obj1, obj1_Pob];
                                                    var Chart1 = dualChart;
                                                    Chart1.xAxis[0].categories = categorias;
                                                    Chart1.title.text = 'Fallecidos COVID-19 Region';
                                                    Chart1.series = total;
                                                    Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                                    Highcharts.chart('chartSinadef1', Chart1);
                                                }
                                            });
                                            var obj = {
                                                tipo: 'covid',
                                            }
                                            $.post({
                                                url: 'divresrmenyestadisticasResumeTab.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                success: function(response) {
                                                    document.getElementById('getResumenSalud').innerHTML = response;
                                                    map3 = L.map('map3').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                                    }).addTo(map3);
                                                    tile4 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_minsa_departamentos',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map3);
                                                    map3.invalidateSize();
                                                    map4 = L.map('map4').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                                                    }).addTo(map4);
                                                    tile5 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_minsa_provincias',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map4);
                                                    map4.invalidateSize();
                                                    map5 = L.map('map5').setView([-9.33, -74.44], 5);
                                                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {}).addTo(map5);
                                                    tile6 = L.tileLayer.betterWms(wmsrootUrl, {
                                                        layers: 'colaboraccion_2020:gen_fallecidos_minsa_distritos',
                                                        tiled: true,
                                                        format: 'image/png',
                                                        minZoom: 0,
                                                        continuousWorld: true,
                                                        transparent: true
                                                    }).addTo(map5);
                                                    map5.invalidateSize();
                                                }
                                            });

                                        }
                                    });
                                }
                            });
                        }
                    });


                    break;
            }

        }
        if (params.classList.contains('lnkAmpliarEntidad')) {

            var id = params.getAttribute('id');

            var obj = {
                method: 'fichaEntidadAmpliar',
                tipo: id.length,
                ubigeo: id
            }

            $.getJSON({
                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    console.log(response);
                    var envio = response.data[0];
                    $.get({
                        url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                        success: function(response) {
                            envio.method = 'ciprlXregion';
                            if (screen.width <= 1224) {

                                dialog9.setLocation([95, 50]);

                            }
                            dialog9.setContent(response);
                            if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                    success: function(response) {
                                        if (Object.keys(response).length > 0) {
                                            var chart = window['combinateChart'];
                                            chart.series = response.data.series;
                                            chart.plotOptions.series.pointStart = response.data.pointStart;
                                            Highcharts.chart('chart_' + envio.codubigeo, chart);
                                        }
                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_sinadef',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos SINADEF por región';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadefEntidad', Chart1);

                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_covid',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos COVID-19';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartCovidEntidad', Chart1);

                                    }
                                });
                            }
                        }
                    });
                }
            });


        }
        if (params.classList.contains('lnkAmpliarInfoResumenP')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');
                    var obj = {
                        departamento: params.getAttribute('id'),
                        tipo: document.getElementById("txttipoCIPRLcANONM").value
                    }
                    $.post({
                        url: 'TablaresrmenyestadisticasProvincia.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divinformacionProvincia_' + params.getAttribute('id')).innerHTML = response;
                        }
                    });
                }
            }
        }
        if (params.classList.contains('lnkAmpliarMeses')) {
            var id = params.getAttribute('id');
            switch (id.length) {
                case 2:
                    var obj = {
                        tipo: id.length,
                        departamento: id
                    }
                    break;
                case 4:
                    var obj = {
                        tipo: id.length,
                        departamento: id.substring(0, 2),
                        provincia: id
                    }
                    break;
                case 6:
                    var obj = {
                        tipo: id.length,
                        departamento: id.substring(0, 2),
                        provincia: id.substring(0, 4),
                        distrito: id
                    }
                    break;
            }
            $.post({
                url: 'divInformacionSaludMes.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    dialog10.setLocation([90, 90]);
                    dialog10.setContent(response);
                    obj.method = "SludxMesesMinsa";
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            console.log(response1.data);
                            var categorias = [];
                            var obj2020 = {
                                name: 'Fallecidos',
                                type: 'column',
                                yAxis: 1,
                                data: []
                            }
                            var obj2020_division = {
                                name: 'fallecidos/pob.',
                                type: 'spline',
                                data: []
                            }

                            for (i = 0; i < response1.data.length; i++) {
                                categorias.push(response1.data[i].mes);
                                obj2020.data.push(parseInt(response1.data[i].fallecidos));
                                obj2020_division.data.push(Number((parseInt(response1.data[i].fallecidos) / parseInt(response1.data[i].poblacion) * 1000).toFixed(2)));

                            }
                            var total = [obj2020, obj2020_division];



                            var Chart = dualChart;
                            Chart.xAxis[0].categories = categorias;
                            Chart.title.text = 'Distribución por Meses de Fallecidos';
                            Chart.series = total;
                            Chart.subtitle.text = 'Población: ' + new Intl.NumberFormat().format(response1.data[0].poblacion);
                            Highcharts.chart('chartMes1', Chart);
                        }
                    });

                }
            });


        }
        if (params.classList.contains('lnkAmpliarMesesSinadef')) {
            var id = params.getAttribute('id');
            switch (id.length) {
                case 2:
                    var obj = {
                        tipo: id.length,
                        departamento: id
                    }
                    break;
                case 4:
                    var obj = {
                        tipo: id.length,
                        departamento: id.substring(0, 2),
                        provincia: id
                    }
                    break;
                case 6:
                    var obj = {
                        tipo: id.length,
                        departamento: id.substring(0, 2),
                        provincia: id.substring(0, 4),
                        distrito: id
                    }
                    break;
            }
            $.post({
                url: 'divInformacionSaludMesSinadef.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    dialog11.setLocation([90, 90]);
                    dialog11.setContent(response);
                    obj.method = "SludxMesesSinadef";
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response1) {
                            console.log(response1.data);
                            var categorias = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Setiembre', 'Octubre', 'Noviembre', 'Diciembre'];
                            var obj2017 = {
                                name: 'Falle:2017',
                                type: 'column',
                                yAxis: 1,
                                color: '#2f7ed8',
                                data: []
                            };
                            var obj2018 = {
                                name: 'Falle:2018',
                                type: 'column',
                                color: '#A61A1A',
                                yAxis: 1,
                                data: []
                            };
                            var obj2019 = {
                                name: 'Falle:2019',
                                type: 'column',
                                color: '#8BBC21',
                                yAxis: 1,
                                data: []
                            };
                            var obj2020 = {
                                name: 'Falle:2020',
                                type: 'column',
                                color: '#F28F43',
                                yAxis: 1,
                                data: []
                            }
                            var obj2017_Pob = {
                                name: 'Falle/Pob:2017 ',
                                type: 'spline',
                                color: '#2f7ed8',
                                data: []
                            };
                            var obj2018_Pob = {
                                name: 'Falle/Pob:2018',
                                type: 'spline',
                                color: '#A61A1A',
                                data: []
                            };
                            var obj2019_Pob = {
                                name: 'Falle/Pob:2019',
                                color: '#8BBC21',
                                type: 'spline',

                                data: []
                            };
                            var obj2020_Pob = {
                                name: 'Falle/Pob:2020',
                                type: 'spline',
                                color: '#F28F43',
                                data: []
                            }
                            for (i = 0; i < response1.data.length; i++) {
                                if (response1.data[i].anio == 2017) {
                                    obj2017.data.push(parseInt(response1.data[i].total));
                                    obj2017_Pob.data.push(Number((parseInt(response1.data[i].total) / parseInt(response1.data[i].poblacion) * 1000).toFixed(2)));
                                } else if (response1.data[i].anio == 2018) {
                                    obj2018.data.push(parseInt(response1.data[i].total));
                                    obj2018_Pob.data.push(Number((parseInt(response1.data[i].total) / parseInt(response1.data[i].poblacion) * 1000).toFixed(2)));
                                } else if (response1.data[i].anio == 2019) {
                                    obj2019.data.push(parseInt(response1.data[i].total));
                                    obj2019_Pob.data.push(Number((parseInt(response1.data[i].total) / parseInt(response1.data[i].poblacion) * 1000).toFixed(2)));
                                } else if (response1.data[i].anio == 2020) {
                                    obj2020.data.push(parseInt(response1.data[i].total));
                                    obj2020_Pob.data.push(Number((parseInt(response1.data[i].total) / parseInt(response1.data[i].poblacion) * 1000).toFixed(2)));
                                }

                            }
                            var total = [obj2017, obj2018, obj2019, obj2020, obj2017_Pob, obj2018_Pob, obj2019_Pob, obj2020_Pob];

                            var Chart1 = dualChart;
                            Chart1.xAxis[0].categories = categorias;
                            Chart1.title.text = 'Distribución por Meses de Fallecidos';
                            Chart1.series = total;
                            Chart1.subtitle.text = 'Población: ' + new Intl.NumberFormat().format(response1.data[0].poblacion);
                            Highcharts.chart('chartMes', Chart1);


                        }
                    });

                }
            });


        }
        if (params.classList.contains('lnkAmpliarInfoResumenPSalud')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                    var obj = {
                        departamento: params.getAttribute('id'), //ubigeo
                        nomdepar: params.innerText, //nombre del departamento
                        tipo: document.getElementById("txttipoCIPRLcANONM").value //minsa || sinadef
                    }
                    console.log(obj);
                    $.post({
                        url: 'TablaresrmenyestadisticasProvinciaSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('divinformacionProvinciaSalud_' + params.getAttribute('id')).innerHTML = response;
                        }
                    });
                }
            }
        }
        if (params.classList.contains('lnkAmpliarInfoResumenDSalud')) {
            var id = params.getAttribute('data-event');

            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');
                    var obj = {
                        departamento: params.getAttribute('id').substring(0, 2), //departamento en codigo 00
                        provincia: params.getAttribute('id'), //provincia codigo 0000
                        tipo: document.getElementById("txttipoCIPRLcANONM").value
                    }
                    console.log(obj);
                    $.post({
                        url: 'TablaresrmenyestadisticasDistritoSalud.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {

                            document.getElementById('divinformacionDistritoSalud_' + params.getAttribute('id')).innerHTML = response;
                        }
                    });
                }
            }
        }
        if (params.classList.contains('lnkAmpliarInfoResumenD')) {
            var id = params.getAttribute('data-event');

            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');
                    var obj = {
                        departamento: params.getAttribute('id').substring(0, 2),
                        provincia: params.getAttribute('id'),
                        tipo: document.getElementById("txttipoCIPRLcANONM").value
                    }
                    console.log(obj);
                    $.post({
                        url: 'TablaresrmenyestadisticasDistrito.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {

                            document.getElementById('divinformacionDistrito_' + params.getAttribute('id')).innerHTML = response;
                        }
                    });
                }
            }
        }
        if (params.classList.contains('lnkAmpliar')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkMostrarOcultar')) {
            var mostrar = $("[id='mdfyBottmUser']");
            var ocultar = $("[id='showmdfyBottmUser']");
            if (mostrar.is(':visible')) {
                mostrar.css('display', 'none');
                ocultar.css('display', '');
                $(".txtedit").prop('readonly', true);
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                $(".txtedit").removeAttr('readonly');
            }

        }
        if (params.classList.contains('lnkAmpliarDIALOEmpresa')) {


            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog8.setLocation([10, 10]);
                dialog8.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog8.setLocation([10, 10]);
                dialog8.setSize([modal_lg.height() * 0.80, 550]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOUniversidas')) {


            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog12.setLocation([10, 10]);
                dialog12.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog12.setLocation([10, 10]);
                dialog12.setSize([modal_lg.height() * 0.80, 550]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOG')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog.setLocation([10, 10]);
                dialog.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog.setLocation([10, 10]);
                dialog.setSize([modal_lg.height() * 0.80, 550]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOGoleoducto')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog3.setLocation([10, 10]);
                dialog3.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog3.setLocation([10, 10]);
                dialog3.setSize([modal_lg.height() * 0.80, 750]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOGlineaFerrea')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog4.setLocation([10, 10]);
                dialog4.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog4.setLocation([10, 10]);
                dialog4.setSize([modal_lg.height() * 0.80, 750]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOGlogistico')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog5.setLocation([10, 10]);
                dialog5.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog5.setLocation([10, 10]);
                dialog5.setSize([modal_lg.height() * 0.80, 750]);
            }
        }
        if (params.classList.contains('lnkAmpliarDIALOGHidrovias')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");
            if (mostrar.is(':visible')) {
                dialog6.setLocation([10, 10]);
                dialog6.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                dialog6.setLocation([10, 10]);
                dialog6.setSize([modal_lg.height() * 0.80, 750]);
            }
        }
        if (params.classList.contains('lnkAmpliarESTADISTICAS')) {
            var mostrar = $("[id='maxi']");
            var ocultar = $("[id='mini']");

            if (mostrar.is(':visible')) {
                dialog7.setLocation([10, 10]);
                dialog7.setSize([screen.width, screen.height * 0.95]);
                mostrar.css('display', 'none');
                ocultar.css('display', '');
            } else {
                mostrar.css('display', '');
                ocultar.css('display', 'none');
                $('#char1').width(300);
                dialog7.setLocation([10, 10]);
                dialog7.setSize([modal_lg.height() * 0.80, 750]);
            }
        }

        if (params.classList.contains('closeSidebar')) {
            sidebar.close('home');
        }
        if (params.classList.contains('oleodcutoFuncionTabla')) {
            var id = params.getAttribute('id');
            changeTabPanelOleoducto(id, id.substring(3));
            var url = "http://95.217.44.43:4000/getubigeosGeojson";
            axios.post(url, { tipo: 'oleoducto', corredor: 1 })
                .then(function(response) {
                    var obj = {
                        method: 'pipsObjetosFuncion',
                        distritos: " (" + response.data.dist.join(',') + ")"
                    }
                    switch (id) {
                        case 'tabministerio-funcionOleoducto':
                            obj.nivelgobierno = 3;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipMinisteriosOleoducto').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabregiones-funcionOleoducto':
                            obj.nivelgobierno = 2;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipRegionesOleoducto').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabprovincias-funcionOleoducto':
                            obj.nivelgobierno = 5;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipProvinciasOleoducto').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabdistritos-funcionOleoducto':
                            obj.nivelgobierno = 1;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipDistritasOleoducto').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabuniversidades-funcionOleoducto':
                            obj.nivelgobierno = 4;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipUniversidadesOleoducto').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
        if (params.classList.contains('lnkAmpliarOleoducto')) {
            var id = params.getAttribute('data-event');
            var cod = params.getAttribute('id');
            var array = cod.split('$');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');
                    var obj = {
                        nivel: array[1],
                        funcion: array[0],
                        distritos: document.getElementById("distritosObjetos").value
                    }
                    $.get({
                        url: 'divpipProjectsOleoductoGrupoFuncional.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            document.getElementById('tableoleoductoGrupoFuncional' + array[0] + array[1]).innerHTML = response;
                        }
                    });

                }
            }
        }
        if (params.classList.contains('lnkAmpliarCentrosLinea')) {

            var id1 = params.getAttribute('id');
            var id = params.getAttribute('data-event');

            var envio = {
                ubigeo: id.substring(0, 6),
                tipo: id1
            }
            $.get({
                url: 'divObjetosInstituciones.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                success: function(response) {
                    document.getElementById('objetosInfraUbi' + id).innerHTML = response;
                }
            });

            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                } else {
                    target.css('display', '');

                }
            }

        }
        if (params.classList.contains('ferreaFuncionTabla')) {
            var id = params.getAttribute('id');
            changeTabPanelferrea(id, id.substring(3));
            var url = "http://95.217.44.43:4000/getubigeosGeojson";
            axios.post(url, { tipo: 'lineaferrea', corredor: document.getElementById("txtLFid").value })
                .then(function(response) {
                    var obj = {
                        method: 'pipsObjetosFuncion',
                        distritos: " (" + response.data.dist.join(',') + ")"
                    }
                    switch (id) {
                        case 'tabministerio-funcionferrea':
                            obj.nivelgobierno = 3;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipMinisteriosferrea').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabregiones-funcionferrea':
                            obj.nivelgobierno = 2;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipRegionesferrea').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabprovincias-funcionferrea':
                            obj.nivelgobierno = 5;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipProvinciasferrea').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabdistritos-funcionferrea':
                            obj.nivelgobierno = 1;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipDistritasferrea').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabuniversidades-funcionferrea':
                            obj.nivelgobierno = 4;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipUniversidadesferrea').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
        if (params.classList.contains('logisiticoFuncionTabla')) {
            var id = params.getAttribute('id');
            changeTabPanellogistico(id, id.substring(3));
            var url = "http://95.217.44.43:4000/getubigeosGeojson";
            axios.post(url, { tipo: 'corredorlogistico', corredor: document.getElementById("txtLogisticoid").value })
                .then(function(response) {
                    var obj = {
                        method: 'pipsObjetosFuncion',
                        distritos: " (" + response.data.dist.join(',') + ")"
                    }
                    switch (id) {
                        case 'tabministerio-funcionlogistico':
                            obj.nivelgobierno = 3;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipMinisterioslogistico').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabregiones-funcionlogistico':
                            obj.nivelgobierno = 2;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipRegioneslogistico').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabprovincias-funcionlogistico':
                            obj.nivelgobierno = 5;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipProvinciaslogistico').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabdistritos-funcionlogistico':
                            obj.nivelgobierno = 1;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipDistritaslogistico').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabuniversidades-funcionlogistico':
                            obj.nivelgobierno = 4;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipUniversidadeslogistico').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }
        if (params.classList.contains('hidroviasFuncionTabla')) {
            var id = params.getAttribute('id');
            changeTabPanellhidrovias(id, id.substring(3));
            var url = "http://95.217.44.43:4000/getubigeosGeojson";
            axios.post(url, { tipo: 'hidrografia', corredor: document.getElementById("txtHidroid").value })
                .then(function(response) {
                    var obj = {
                        method: 'pipsObjetosFuncion',
                        distritos: " (" + response.data.dist.join(',') + ")"
                    }
                    switch (id) {
                        case 'tabministerio-funcionhidrovias':
                            obj.nivelgobierno = 3;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipMinisterioshidrovias').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabregiones-funcionhidrovias':
                            obj.nivelgobierno = 2;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipRegioneshidrovias').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabprovincias-funcionhidrovias':
                            obj.nivelgobierno = 5;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipProvinciashidrovias').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabdistritos-funcionhidrovias':
                            obj.nivelgobierno = 1;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipDistritashidrovias').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                        case 'tabuniversidades-funcionhidrovias':
                            obj.nivelgobierno = 4;
                            setTimeout(function() {
                                $.post({
                                    url: 'divtablafuncionObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                    success: function(response) {
                                        document.getElementById('divpipUniversidadeshidrovias').innerHTML = response;
                                    }
                                });
                            }, 100);
                            break;
                    }
                })
                .catch(function(error) {
                    console.log(error);
                });
        }

    }
    var changeTabPanelOleoducto = function(params, params2) {
        $('#tabministerio-funcionOleoducto').removeClass('active');
        $('#tabregiones-funcionOleoducto').removeClass('active');
        $('#tabprovincias-funcionOleoducto').removeClass('active');
        $('#tabdistritos-funcionOleoducto').removeClass('active');
        $('#tabuniversidades-funcionOleoducto').removeClass('active');
        $('#ministerio-funcionOleoducto').removeClass('show active');
        $('#regiones-funcionOleoducto').removeClass('show active');
        $('#provincias-funcionOleoducto').removeClass('show active');
        $('#distritos-funcionOleoducto').removeClass('show active');
        $('#universidades-funcionOleoducto').removeClass('show active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
        var elemento1 = document.getElementById(params2);
        elemento1.className += " show active";
    }
    var changeTabPanelferrea = function(params, params2) {
        $('#tabministerio-funcionferrea').removeClass('active');
        $('#tabregiones-funcionferrea').removeClass('active');
        $('#tabprovincias-funcionferrea').removeClass('active');
        $('#tabdistritos-funcionferrea').removeClass('active');
        $('#tabuniversidades-funcionferrea').removeClass('active');
        $('#ministerio-funcionferrea').removeClass('show active');
        $('#regiones-funcionferrea').removeClass('show active');
        $('#provincias-funcionferrea').removeClass('show active');
        $('#distritos-funcionferrea').removeClass('show active');
        $('#universidades-funcionferrea').removeClass('show active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
        var elemento1 = document.getElementById(params2);
        elemento1.className += " show active";
    }
    var changeTabPanellogistico = function(params, params2) {
        $('#tabministerio-funcionlogistico').removeClass('active');
        $('#tabregiones-funcionlogistico').removeClass('active');
        $('#tabprovincias-funcionlogistico').removeClass('active');
        $('#tabdistritos-funcionlogistico').removeClass('active');
        $('#tabuniversidades-funcionlogistico').removeClass('active');
        $('#ministerio-funcionlogistico').removeClass('show active');
        $('#regiones-funcionlogistico').removeClass('show active');
        $('#provincias-funcionlogistico').removeClass('show active');
        $('#distritos-funcionlogistico').removeClass('show active');
        $('#universidades-funcionlogistico').removeClass('show active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
        var elemento1 = document.getElementById(params2);
        elemento1.className += " show active";
    }
    var changeTabPanellhidrovias = function(params, params2) {
        $('#tabministerio-funcionhidrovias').removeClass('active');
        $('#tabregiones-funcionhidrovias').removeClass('active');
        $('#tabprovincias-funcionhidrovias').removeClass('active');
        $('#tabdistritos-funcionhidrovias').removeClass('active');
        $('#tabuniversidades-funcionhidrovias').removeClass('active');
        $('#ministerio-funcionhidrovias').removeClass('show active');
        $('#regiones-funcionhidrovias').removeClass('show active');
        $('#provincias-funcionhidrovias').removeClass('show active');
        $('#distritos-funcionhidrovias').removeClass('show active');
        $('#universidades-funcionhidrovias').removeClass('show active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
        var elemento1 = document.getElementById(params2);
        elemento1.className += " show active";
    }
    var filterLayer = function() {

        var addTo = false;
        var ddlRegion = filterArr('filterParams', 'key', 'ddlRegion').selected;
        var ddlProvincia = filterArr('filterParams', 'key', 'ddlProvincia').selected;
        var infoCIPRL = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL');
        var infoMinsa = filterGroup('overLayers', 'baseMinsa', 'infoMinsa');
        var infoSinadef = filterGroup('overLayers', 'baseSinadef', 'infoSinadef');

        var txtMontoCIPRL = filterArr('filterParams', 'key', 'txtMontoCIPRL').selected;
        var txtPoblacion = filterArr('filterParams', 'key', 'txtPoblacion').selected;
        var txtCondition = "";
        if (txtMontoCIPRL.txtMontoCIPRLIni.toString().length > 0 && txtMontoCIPRL.txtMontoCIPRLFin.toString().length > 0) {
            txtCondition = txtCondition + ' and monto_ciprl between ' + txtMontoCIPRL.txtMontoCIPRLIni + ' and ' + txtMontoCIPRL.txtMontoCIPRLFin;
        }

        if (txtPoblacion.txtPoblacionIni.toString().length > 0 && txtPoblacion.txtPoblacionFin.toString().length > 0) {
            txtCondition = txtCondition + ' and poblacion between ' + txtPoblacion.txtPoblacionIni + ' and ' + txtPoblacion.txtPoblacionFin;

        }
        if (ddlRegion.length > 0 && ddlProvincia.length == 0) {

            lyrDraw([lyrRegion, lyrProvincia, lyrDistrito], "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            lyrDraw([lyrRegionMinsa, lyrProvinciaMinsa, lyrDistritoMinsa], "coddpto in (" + ddlRegion.join(',') + ")");
            lyrDraw([lyrRegionSinadef, lyrProvinciaSinadef, lyrDistritoSinadef], "coddpto in (" + ddlRegion.join(',') + ")");

            loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            loadSaludXubigeo('vw_fallecidos_sinadef_departamentos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_sinadef_provincias_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrProv, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_sinadef_distritos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrDist, "coddpto in (" + ddlRegion.join(',') + ")");

            loadSaludXubigeo('vw_fallecidos_minsa_departamentos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_minsa_provincias', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrProv, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_minsa_distritos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrDist, "coddpto in (" + ddlRegion.join(',') + ")");

            loadComisarias("ubigeoregion in (" + ddlRegion.join(',') + ")");
            loadHospitales("coddpto in (" + ddlRegion.join(',') + ")");
            //export excel
            var obj = {
                consulta_region: "co.coddpto in (" + ddlRegion.join(',') + ")" + txtCondition,
                consulta_provincia: "geo.coddpto in (" + ddlRegion.join(',') + ")" + txtCondition,
                consulta_distrito: "geo.coddpto in (" + ddlRegion.join(',') + ")" + txtCondition,
            };
            $.post({
                url: 'navTableExcelFiltro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('divTableexcelfiltroProycts').innerHTML = response;
                }
            });

        } else if (ddlRegion.length > 0 && ddlProvincia.length > 0) {
            lyrDraw([lyrRegion], "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            lyrDraw([lyrProvincia, lyrDistrito], "codprov in (" + ddlProvincia.join(',') + ")" + txtCondition);
            lyrDraw([lyrRegionMinsa], "coddpto in (" + ddlRegion.join(',') + ")");
            lyrDraw([lyrProvinciaMinsa], "ubigeo in (" + ddlProvincia.join(',') + ")");
            lyrDraw([lyrDistritoMinsa], "codprov in (" + ddlProvincia.join(',') + ")");
            lyrDraw([lyrRegionSinadef], "coddpto in (" + ddlRegion.join(',') + ")");
            lyrDraw([lyrProvinciaSinadef], "ubigeo in (" + ddlProvincia.join(',') + ")");
            lyrDraw([lyrDistritoSinadef], "codprov in (" + ddlProvincia.join(',') + ")");
            loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")" + txtCondition);
            loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, "codprov in (" + ddlProvincia.join(',') + ")" + txtCondition);
            loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, "codprov in (" + ddlProvincia.join(',') + ")" + txtCondition);
            loadSaludXubigeo('vw_fallecidos_sinadef_departamentos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_sinadef_provincias_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrProv, "ubigeo in (" + ddlProvincia.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_sinadef_distritos_info', filterGroup('overLayers', 'baseSinadef', 'infoSinadef').lyrDist, "codprov in (" + ddlProvincia.join(',') + ")");

            loadSaludXubigeo('vw_fallecidos_minsa_departamentos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrRegion, "coddpto in (" + ddlRegion.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_minsa_provincias', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrProv, "ubigeo in (" + ddlProvincia.join(',') + ")");
            loadSaludXubigeo('vw_fallecidos_minsa_distritos_info', filterGroup('overLayers', 'baseMinsa', 'infoMinsa').lyrDist, "codprov in (" + ddlProvincia.join(',') + ")");
            loadComisarias("ubigeoprov in (" + ddlProvincia.join(',') + ")");
            loadHospitales("codprov in (" + ddlProvincia.join(',') + ")");
            //export excel
            var obj = {
                consulta_region: "co.coddpto in (" + ddlRegion.join(',') + ")" + txtCondition,
                consulta_provincia: "geo.codprov in (" + ddlProvincia.join(',') + ")" + txtCondition,
                consulta_distrito: "geo.codprov in (" + ddlProvincia.join(',') + ")" + txtCondition,
            };
            $.post({
                url: 'navTableExcelFiltro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                success: function(response) {
                    document.getElementById('divTableexcelfiltroProycts').innerHTML = response;
                }
            });

        }

        if (map.hasLayer(infoCIPRL.layer)) {
            addTo = true;
            map.removeLayer(infoCIPRL.layer);
        }
        if (map.hasLayer(infoMinsa.layer)) {
            addTo = true;
            map.removeLayer(infoMinsa.layer);
        }
        if (map.hasLayer(infoSinadef.layer)) {
            addTo = true;
            map.removeLayer(infoSinadef.layer);
        }
        if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'region').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrRegion;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'provincia').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrProv;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'distrito').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrDist;
        }
        if (map.hasLayer(filterGroup('overLayers', 'baseSinadef', 'region_sinadef').layer)) {
            infoSinadef.layer = infoSinadef.lyrRegion;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseSinadef', 'provincia_sinadef').layer)) {
            infoSinadef.layer = infoSinadef.lyrProv;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseSinadef', 'distrito_sinadef').layer)) {
            infoSinadef.layer = infoSinadef.lyrDist;
        }
        if (map.hasLayer(filterGroup('overLayers', 'baseMinsa', 'region_minsa').layer)) {
            infoMinsa.layer = infoMinsa.lyrRegion;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseMinsa', 'provincia_minsa').layer)) {
            infoMinsa.layer = infoMinsa.lyrProv;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseMinsa', 'distrito_minsa').layer)) {
            infoMinsa.layer = infoMinsa.lyrDist;
        }

        if (addTo) infoCIPRL.layer.addTo(map);
        if (addTo) infoMinsa.layer.addTo(map);
        if (addTo) infoSinadef.layer.addTo(map);
    }
    var filterLayerGeojson = function(region, provincia, distrito) {
        var addTo = false;
        var infoCIPRL = filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL');
        var txtMontoCIPRL = filterArr('filterParams', 'key', 'txtMontoCIPRL').selected;
        var txtPoblacion = filterArr('filterParams', 'key', 'txtPoblacion').selected;
        var txtCondition = "";
        if (txtMontoCIPRL.txtMontoCIPRLIni.toString().length > 0 && txtMontoCIPRL.txtMontoCIPRLFin.toString().length > 0) {
            txtCondition = txtCondition + ' and monto_ciprl between ' + txtMontoCIPRL.txtMontoCIPRLIni + ' and ' + txtMontoCIPRL.txtMontoCIPRLFin;
        }

        if (txtPoblacion.txtPoblacionIni.toString().length > 0 && txtPoblacion.txtPoblacionFin.toString().length > 0) {
            txtCondition = txtCondition + ' and poblacion between ' + txtPoblacion.txtPoblacionIni + ' and ' + txtPoblacion.txtPoblacionFin;

        }
        lyrDraw([lyrRegion], "codubigeo in (" + region.join(',') + ")" + txtCondition);
        loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo in (" + region.join(',') + ")" + txtCondition);
        lyrDraw([lyrProvincia], "codubigeo in (" + provincia.join(',') + ")" + txtCondition);
        loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, "codubigeo in (" + provincia.join(',') + ")" + txtCondition);
        lyrDraw([lyrDistrito], "codubigeo in (" + distrito.join(',') + ")" + txtCondition);
        loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, "codubigeo in (" + distrito.join(',') + ")" + txtCondition);
        loadComisarias("ubigeo  in (" + distrito.join(',') + ")");
        loadHospitales("ubigeo  in (" + distrito.join(',') + ")");
        if (map.hasLayer(infoCIPRL.layer)) {
            addTo = true;
            map.removeLayer(infoCIPRL.layer);
        }

        if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'region').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrRegion;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'provincia').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrProv;
        } else if (map.hasLayer(filterGroup('overLayers', 'baseUbigeo', 'distrito').layer)) {
            infoCIPRL.layer = infoCIPRL.lyrDist;
        }

        if (addTo) infoCIPRL.layer.addTo(map);


        //export excel
        var obj = {
            consulta_region: "codubigeo in (" + region.join(',') + ")" + txtCondition,
            consulta_provincia: "codubigeo in (" + provincia.join(',') + ")" + txtCondition,
            consulta_distrito: "codubigeo in (" + distrito.join(',') + ")" + txtCondition,
        };
        console.log("codubigeo in (" + provincia.join(',') + ")" + txtCondition);
        $.post({
            url: 'navTableExcelFiltro.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                document.getElementById('divTableexcelfiltroProycts').innerHTML = response;
            }
        });
    }
    var getLyrDrawUbigeos = function() {
        var tmp = [];
        clearLayers(clusters.projects);
        if (tmpVias.length != 0) {
            tmp.push(tmpVias.join(','));
        }
        if (tmpLineas.length != 0) {
            tmp.push(tmpLineas.join(','));
        }
        if (tmpOleoducto.length != 0) {
            tmp.push(tmpOleoducto.join(','));
        }
        if (tmpAccidentes.length != 0) {
            tmp.push(tmpAccidentes.join(','));
        }
        if (tmpHidrografias.length != 0) {
            tmp.push(tmpHidrografias.join(','));
        }
        if (tmpMineros.length != 0) {
            tmp.push(tmpMineros.join(','));
        }

        var dpto = [];
        var prov = [];
        var dist = [];
        var obj = {
            method: 'ubigeoMultiobjeto',
            consulta: tmp.join(',')
        }
        $.getJSON({
            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
            success: function(response) {
                for (i = 0; i < response.data.length; i++) {
                    if (response.data[i].idnivel == 1) {
                        dpto.push("'" + response.data[i].ubigeo + "'");
                    } else if (response.data[i].idnivel == 2) {
                        prov.push("'" + response.data[i].ubigeo + "'");
                    } else if (response.data[i].idnivel == 3) {
                        dist.push("'" + response.data[i].ubigeo + "'");
                    }
                }

                lyrDraw([lyrRegion], "codubigeo in (" + dpto.join(',') + ")");
                loadCiprlXubigeo('vw_dpto_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrRegion, "codubigeo in (" + dpto.join(',') + ")");
                lyrDraw([lyrProvincia], "codubigeo in (" + prov.join(',') + ")");
                loadCiprlXubigeo('vw_provincia_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrProv, "codubigeo in (" + prov.join(',') + ")");
                lyrDraw([lyrDistrito], "codubigeo in (" + dist.join(',') + ")");
                loadCiprlXubigeo('vw_distrito_geojson_info', filterGroup('overLayers', 'baseUbigeo', 'infoCIPRL').lyrDist, "codubigeo in (" + dist.join(',') + ")");
                filterGroup('overLayers', 'baseExtras', 'comisarias').layer.clearLayers();
                filterGroup('overLayers', 'baseExtras', 'hospitales').layer.clearLayers();
                loadComisarias("ubigeo in (" + dist.join(',') + ")");
                loadHospitales("ubigeo in (" + dist.join(',') + ")");
                var regionChk = $('input[data-id="region"]:checked').length;
                var provinciaChk = $('input[data-id="provincia"]:checked').length;
                var distritoChk = $('input[data-id="distrito"]:checked').length;
                if (regionChk == 1) {
                    var consulta = "iddpto in (" + dpto.join(',') + ")";
                    var tmp1 = [];
                    $.each(dpto, function(i, item) {
                        tmp1.push(Number(item.substring(1, 3)));
                    });
                    var consultaphp = "coddpto in (" + tmp1.join(',') + ")";
                } else if (provinciaChk == 1) {
                    var consulta = "codubigeo in (" + prov.join(',') + ")" + " or iddist in (" + dist.join(',') + ")";
                    var consultaphp = "codubigeo in (" + prov.join(',') + ")" + " or iddist in (" + dist.join(',') + ")";
                } else if (distritoChk == 1) {
                    var consulta = "iddist in (" + dist.join(',') + ")";
                    var consultaphp = "iddist in (" + dist.join(',') + ")";
                } else {
                    var tmp2 = [];
                    $.each(dpto, function(i, item) {
                        tmp2.push(Number(item.substring(1, 3)));
                    });
                    var consultaphp = "coddpto in (" + tmp2.join(',') + ")" + " or codubigeo in (" + prov.join(',') + ")" + " or iddist in (" + dist.join(',') + ")";
                    var consulta = "iddpto in (" + dpto.join(',') + ")" + " or codubigeo in (" + prov.join(',') + ")" + " or iddist in (" + dist.join(',') + ")";
                }
                iniLoadAlert('Cargando Proyectos', 'Cambiando Capa CIPRL', 2000);
                counterSum(consultaphp);
                loadProjects(consulta);

            }
        });
    }
    var chartCompanies = function(codempresa, result) {
            Highcharts.chart('chart_' + codempresa, {
                title: {
                    text: 'Evoluci�n de Ingresos y Utilidades'
                },

                yAxis: {
                    title: {
                        text: 'Soles'
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },

                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: result.pointStart
                    }
                },

                series: [{
                    name: 'Ingresos',
                    data: result.series.ingresos
                }, {
                    name: 'Utilidades',
                    data: result.series.utilidades
                }],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            });
        }
        //Aqui vienen Los lOAADS
    var loadColleges = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_universidades'
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-university',
                                markerColor: feature.properties.color,
                                shape: 'penta',
                                prefix: 'fas'
                            })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(clusters['universidades'][1]);
                    }
                });

                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;
                    delete obj.method;

                    $.get({
                        url: 'popupColleges.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            obj.method = 'ciprlXuniv';
                            dialog12.setLocation([95, 50]);
                            dialog12.setContent(response);


                            dialog12.unfreeze();

                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    if (Object.keys(response).length > 0) {
                                        var chart = window['combinateChart'];
                                        chart.subtitle.text = obj.universidad;
                                        chart.series = response.data.series;
                                        chart.plotOptions.series.pointStart = response.data.pointStart;
                                        Highcharts.chart('chart_' + obj.coduniv, chart);

                                        chartPie({ 'container': 'pieDistrito', 'method': 'indXfuncion', 'codnivel': 1, 'codfase': 4, 'codubigeo': obj.coddpto });
                                        chartPie({ 'container': 'pieProvincia', 'method': 'indXfuncion', 'codnivel': 1, 'codfase': 6, 'codubigeo': obj.coddpto });
                                        chartPie({ 'container': 'pieRegion', 'method': 'indXfuncion', 'codnivel': 1, 'codfase': 2, 'codubigeo': obj.coddpto });
                                    }
                                }
                            });
                        }
                    });
                });
            }
        });

        var lyr = filterGroup('overLayers', 'baseLayer', 'universidades').layer;
        lyr.addLayer(clusters['universidades'][1]);
    }
    var loadCompanies = function(cql_filter) {
        layerGroupCirculo = L.layerGroup().addTo(map);

        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_empresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {

                var markers = L.geoJson(data, {

                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-briefcase',
                                markerColor: 'black',
                                shape: 'square',
                                prefix: 'fas'
                            })
                        });

                        var muestra = $("#txtradioInfl").val();

                        if (muestra != undefined && $("#txtRuc").val() != undefined) {

                            if (muestra != "" && $("#txtRuc").val() != "") {
                                L.circle(L.latLng(latlng.lat, latlng.lng, 5), muestra * 1000, {
                                    opacity: 0.5,
                                    weight: 1,
                                    fillOpacity: 0.4
                                }).addTo(layerGroupCirculo);
                                circleEmpresa = {
                                    method: 'draw_area_influencia_comisarias',
                                    lat: latlng.lat,
                                    lng: latlng.lng,
                                    radio: muestra * 1000
                                }

                                circleEmpresa.radio = $("#txtradioInfl").val() * 1000;

                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(circleEmpresa)),
                                    success: function(response) {
                                        var tmp = [];
                                        for (i = 0; i < response.data.length; i++) {
                                            tmp.push("'" + response.data[i].cod_pnp + "'");
                                        }
                                        console.log(response);
                                        loadComisarias("cod_pnp in (" + tmp.join(',') + ")");
                                    }
                                });
                                circleEmpresa.method = 'draw_area_influencia_comisarias_';
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(circleEmpresa)),
                                    success: function(response) {

                                        var tmp = [];
                                        for (i = 0; i < response.data.length; i++) {
                                            tmp.push(response.data[i].cu);
                                        }
                                        $('input[data-id="hospitales"]').removeAttr('disabled');
                                        loadHospitales('cu in (' + tmp.join(',') + ') ');

                                    }
                                });
                            }


                        }



                        marker.bindPopup("", { closeButton: true, minWidth: 650 });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(clusters['companies'][1]);
                    }
                });

                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;
                    obj.method = 'empconceptos';

                    $.get({
                        url: 'popupCompanies.php?obj=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            //e.layer._popup.setContent(response);
                            dialog8.setLocation([90, 90]);

                            dialog8.setContent(response);
                            dialog8.unfreeze();
                            var obj = e.layer.feature.properties;

                            obj.method = 'empconceptos';
                            console.log(obj)
                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {

                                    if (Object.keys(response).length > 0) {
                                        chartCompanies(obj.ruc, response.data);

                                    }
                                }
                            });
                            var obj1 = {

                                empresa: e.layer.feature.properties.empresa
                            };
                            $.post({
                                url: 'popupCompaniesTabla.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                success: function(response) {
                                    document.getElementById('divTablaproyectosComnpanies').innerHTML = response;
                                }
                            });


                            /*const url = "https://api.sunat.cloud/ruc/" + e.layer.feature.properties.ruc; // site that doesn’t send Access-Control-*
                            fetch( url) // https://cors-anywhere.herokuapp.com/https://example.com
                                .then(response => response.text())
                                .then(contents => {
                                    var rpta = JSON.parse(contents)
                                     $("#contribuyente_condicion").val(rpta.contribuyente_condicion);
                                    $("#contribuyente_condicion").val(rpta.contribuyente_condicion);
                                    $("#contribuyente_estado").val(rpta.contribuyente_estado);
                                    $("#contribuyente_tipo").val(rpta.contribuyente_tipo);
                                    $("#domicilio_fiscal").val(rpta.domicilio_fiscal);
                                    $("#fecha_actividad").val(rpta.fecha_actividad);
                                    $("#fecha_inscripcion").val(rpta.fecha_inscripcion);
                                    $("#razon_social").val(rpta.razon_social);
                                    $("#nombre_comercial").val(rpta.nombre_comercial);
                                    $("#sistema_emision").val(rpta.sistema_emision);
                                    $("#sistema_contabilidad").val(rpta.sistema_contabilidad);
                                     var htmTable = `<table class="table table-detail">
                                        <tr class="table-dark">
                                            <th class="text-dark"><b>Nombre</b></th>
                                            <th class="text-dark"><b>Cargo</b></th>
                                            <th class="text-dark"><b>Desde</b></th>
                                           
                                        </tr>`;
                                    $.each(rpta.representante_legal, function (i, obj) {
                                        console.log(obj);
                                        htmTable += '<tr>';
                                        htmTable += '<td style="text-align:center">' + obj.nombre + '</td>';
                                        htmTable += '<td style="text-align:center">' + obj.cargo + '</td>';
                                        htmTable += '<td style="text-align:center">' + obj.desde + '</td>';
                                        htmTable += '</tr>';
                                    });
                                    htmTable += `</table>`;
                                    $('#txtrepresentantes').html(htmTable);
                                	
                                    
                                }
                                )
                                .catch(() => console.log("Can’t access " + url + " response. Blocked by browser?"))*/
                            var url = "http://95.217.44.43:4000/getSunat";
                            axios.post(url, { ruc: e.layer.feature.properties.ruc })

                            .then(function(response) {
                                    console.log(response.data);
                                    $("#contribuyente_condicion").val(response.data.condicion);
                                    $("#actividad_exterior").val(response.data.actExterior);
                                    $("#emision_electronica").val(response.data.emision_electronica); //
                                    $("#estado").val(response.data.estado);
                                    $("#fecha_inscripcion").val(response.data.fechaInscripcion);
                                    $("#inicio_actividades").val(response.data.inicio_actividades);
                                    $("#nombre_comercial").val(response.data.nombreComercial);
                                    $("#razon_social").val(response.data.razonSocial);
                                    $("#sistema_contabilidad").val(response.data.sistContabilidad);
                                    $("#sistema_emision").val(response.data.sistEmsion);
                                    $("#tipo").val(response.data.tipo);
                                    $("#direccion").val(response.data.direccion);
                                    var htmTable = `<table class="table table-sm table-detail">
												<tr>
											
													<th style="background:#ddebf8"><b>descripcion</b></th>
												   
												   
												</tr>`;
                                    $.each(response.data.actEconomicas, function(i, obj) {

                                        htmTable += '<tr>';
                                        htmTable += '<td style="text-align:center;text-align:center">' + obj + '</td>';

                                        htmTable += '</tr>';
                                    });
                                    htmTable += `</table>`;
                                    $('#txtactividadEconomica').html(htmTable);
                                    var htmTable1 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8"><b>Nombre</b></th>
											
										</tr>`;
                                    $.each(response.data.cpPago, function(i, obj) {

                                        htmTable1 += '<tr>';
                                        htmTable1 += '<td style="text-align:center">' + obj + '</td>';
                                        htmTable1 += '</tr>';
                                    });
                                    htmTable1 += `</table>`;
                                    $('#txtrepresentantes').html(htmTable1);
                                    var htmTable2 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Nombre del Padrón</b></th>
									
										</tr>`;
                                    $.each(response.data.padrones, function(i, obj) {

                                        htmTable2 += '<tr>';
                                        htmTable2 += '<td style="text-align:center">' + obj + '</td>';

                                        htmTable2 += '</tr>';
                                    });
                                    htmTable2 += `</table>`;
                                    $('#txtTrabajadores').html(htmTable2);
                                    var htmTable3 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
										</tr>`;
                                    $.each(response.data.sistElectronica, function(i, obj) {

                                        htmTable3 += '<tr>';
                                        htmTable3 += '<td style="text-align:center">' + obj + '</td>';
                                        htmTable3 += '</tr>';
                                    });
                                    htmTable3 += `</table>`;
                                    $('#txtcomprobante_electronico').html(htmTable3);
                                })
                                .catch(function(error) {
                                    console.log(error);
                                });

                            direccionesTemporalruc = e.layer.feature.properties.ruc;
                            map111 = L.map('map111', { zoomControl: false }).setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map111);
                            new L.Control.Zoom({ position: 'topright' }).addTo(map111);
                            map111.invalidateSize();
                            loadCompaniesPrincipal("ruc='" + e.layer.feature.properties.ruc + "'")
                            var obj = {
                                method: 'empresas_expo_impo',
                                ruc: e.layer.feature.properties.ruc
                            };

                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {

                                    console.log(response.data[0]);
                                    var htmTable3 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2019</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2018</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2017</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2016</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2015</b></th>
											<th style="background:#ddebf8;text-align:center"><b>2014</b></th>
										</tr>`;
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Importaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Exportaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += `</table>`;
                                    $('#txtexportacionesTable').html(htmTable3);
                                    Highcharts.chart('txtexportaciones', {

                                        title: {
                                            text: 'Exportaciones e Importaciones 2014-2019'
                                        },

                                        subtitle: {
                                            text: '(millones de soles)'
                                        },

                                        yAxis: {
                                            title: {
                                                text: 'S/.'
                                            }
                                        },

                                        xAxis: {
                                            accessibility: {
                                                rangeDescription: 'Range: 2014-2019'
                                            }
                                        },

                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'middle'
                                        },

                                        plotOptions: {
                                            series: {
                                                label: {
                                                    connectorAllowed: false
                                                },
                                                pointStart: 2010
                                            }
                                        },

                                        series: [{
                                                name: 'Importacion',
                                                data: [parseInt(response.data[0].usdimpo2019), parseInt(response.data[0].usdimpo2018), parseInt(response.data[0].usdimpo2017), parseInt(response.data[0].usdimpo2016), parseInt(response.data[0].usdimpo2015), parseInt(response.data[0].usdimpo2014)]
                                            },
                                            {
                                                name: 'Exportacion',
                                                data: [parseInt(response.data[0].usdexpo2019), parseInt(response.data[0].usdexpo2018), parseInt(response.data[0].usdexpo2017), parseInt(response.data[0].usdexpo2016), parseInt(response.data[0].usdexpo2015), parseInt(response.data[0].usdexpo2014)]
                                            }
                                        ],

                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                },
                                                chartOptions: {
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                    });
                                }
                            });
                            var obj = {
                                method: 'empresas_ubigeos',
                                ruc: e.layer.feature.properties.ruc
                            };

                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {


                                    if (response.data[0].can == 0) {
                                        var obj = {
                                            method: 'anexos_sunat',
                                            ruc: e.layer.feature.properties.ruc
                                        };
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                            success: function(response1) {

                                                for (i = 0; i < response1.data.length; i++) {
                                                    direcciones = response1.data[i].dir;

                                                    var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + direcciones + "&key=AIzaSyBXYr807NR6Kb92jrYUP940byIl4VwsmcE";
                                                    axios.get(url).then(function(response) {
                                                        var obj = {
                                                            method: 'insert_empresa',
                                                            ruc: e.layer.feature.properties.ruc,
                                                            lng: parseFloat(response.data.results[0].geometry.location.lng),
                                                            lat: parseFloat(response.data.results[0].geometry.location.lat),

                                                        };

                                                        $.getJSON({
                                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                                            success: function(response) {

                                                            }
                                                        });

                                                    }).catch(function(error) {
                                                        console.log(error);
                                                    });
                                                    direcciones = "";
                                                }
                                            }
                                        });

                                    }
                                    /*for (i = 0; i < response.data.length; i++) {
                                        direccionesTemporal.push(response.data[i].dir);
                                        console.log(response.data[i].dir);
                                    }*/
                                }
                            });

                        }
                    });
                });
            }
        });

        var lyr = filterGroup('overLayers', 'baseLayer', 'empresas').layer;

        for (var key in clusters['companies']) {
            if (clusters['companies'].hasOwnProperty(key)) {
                if (Object.keys(clusters['companies'][key]).length > 0) {
                    lyr.addLayer(clusters['companies'][key]);
                }
            }
        }
    }
    var getMes = function(mes) {
        var mm;
        switch (mes) {
            case 1:
                mm = 'Enero';
                break;
            case 2:
                mm = 'Febrero';
                break;
            case 3:
                mm = 'Marzo';
                break;
            case 4:
                mm = 'Abril';
                break;
            case 5:
                mm = 'Mayo';
                break;
            case 6:
                mm = 'Junio';
                break;
            case 7:
                mm = 'Julio';
                break;
            case 8:
                mm = 'Agosto';
                break;
            case 9:
                mm = 'Setiembre';
                break;
            case 10:
                mm = 'Octubre';
                break;
            case 11:
                mm = 'Noviembre';
                break;
            case 12:
                mm = 'Diciembre';
                break;
        }
        return mm;
    }
    var deleteGeo = function() {
        if (layerGroup != undefined) { layerGroup.clearLayers(); }

    }
    var loadGeoEmpresas = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_geoEmpresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                layerGroup = L.layerGroup().addTo(map2);

                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        LayerGorupTemporal.push(feature);
                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-number',
                                number: feature.properties.id,
                                markerColor: feature.properties.estado,
                                shape: 'circle',
                                prefix: 'fas'
                            })
                        }).addTo(layerGroup);
                        return marker;
                    },
                });
            }
        });
    }
    var loadCompaniesAnexos = function(cql_filter) {
        layerLatLngCoordenadas = L.layerGroup().addTo(map111);

        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'gen_anexos_empresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {

                var markers = L.geoJson(data, {

                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-home',
                                markerColor: 'red',
                                shape: 'circle',
                                prefix: 'fas'
                            })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(layerLatLngCoordenadas);
                    }
                });

            }
        });


    }
    var loadCompaniesPrincipal = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_empresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {

                var markers = L.geoJson(data, {

                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-briefcase',
                                markerColor: 'black',
                                shape: 'square',
                                prefix: 'fas'
                            })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(map111);
                    }
                });

            }
        });


    }
    var loadConflicts = function(cql_filter) {
        var lyr = filterGroup('overLayers', 'baseExtras', 'conflictos').layer;
        var minesIcon = L.ExtraMarkers.icon({
            innerHTML: '<img src="assets/app/img/Mine2.png" width="20" height="20" style="margin-top: 10px; cursor: pointer"/>',
            markerColor: 'white'
        });

        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'gen_conflictos_sociales'
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: minesIcon
                        });

                        marker.bindPopup("Cargando...", { closeButton: true, minWidth: 650 });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });

                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;

                    $.get({
                        url: 'popupConflicts.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            e.layer._popup.setContent(response);
                        }
                    });
                });
            }
        });
    }
    var loadProjectsDraw = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_proyectos',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: feature.properties.nivel,
                                iconColor: feature.properties.fase == 'white' ? 'black' : 'white',
                                markerColor: feature.properties.fase,
                                draggable: true,
                                shape: 'circle',
                                prefix: 'fas'
                            })
                        });
                        marker.bindPopup("Cargando...", { closeButton: true, minWidth: 600 });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        if (clusters['projects'].hasOwnProperty([feature.properties.codfase])) {
                            layer.addTo(map10);
                        }
                    }
                });
                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;
                    obj.ddlFase = cloneSelect(ddlFase2, 'ddlFase', obj.codproyecto, obj.codfase);
                    obj.ddlNivel = cloneSelect(ddlNivel2, 'ddlNivel', obj.codproyecto, obj.codnivelgobierno);
                    $.get({
                        url: 'popupProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            e.layer._popup.setContent(response);
                        }
                    });

                });
            }
        });

    }
    var loadProjects = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_proyectos',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: feature.properties.nivel,
                                iconColor: feature.properties.fase == 'white' ? 'black' : 'white',
                                markerColor: feature.properties.fase,
                                draggable: true,
                                shape: 'circle',
                                prefix: 'fas'
                            })
                        });
                        marker.bindPopup("Cargando...", { closeButton: true, minWidth: 600 });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        if (clusters['projects'].hasOwnProperty([feature.properties.codfase])) {
                            layer.addTo(clusters['projects'][feature.properties.codfase]);
                        }
                    }
                });
                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;
                    obj.ddlFase = cloneSelect(ddlFase2, 'ddlFase', obj.codproyecto, obj.codfase);
                    obj.ddlNivel = cloneSelect(ddlNivel2, 'ddlNivel', obj.codproyecto, obj.codnivelgobierno);
                    $.get({
                        url: 'popupProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            e.layer._popup.setContent(response);
                        }
                    });

                });
            }
        });
        var lyr = filterGroup('overLayers', 'baseLayer', 'proyectos').layer;
        for (var key in clusters['projects']) {
            if (clusters['projects'].hasOwnProperty(key)) {
                if (Object.keys(clusters['projects'][key]).length > 0) {
                    lyr.addLayer(clusters['projects'][key]);
                }
            }
        }

    }
    var mapaCalorPrueba = function(cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_proyectos',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var tmp = [];
                var l = L.geoJson();

                $.each(data.features, function(i, item) {
                    tmp.push([Number(item.properties.latitud), Number(item.properties.longitud), item.properties.densidad]);

                });
                L.heatLayer(tmp, { minOpacity: 0.5 }).addTo(clusters.Mapas_de_Calor.lyrGroup);
            }
        });
    }
    var loadOleoductoInfo = function() {
        var lyr = filterGroup('overLayers', 'baseMineria', 'oleoducto').layer;
        var icon = L.divIcon({ html: '<img src="assets/app/img/info4.png" border="0" title="Oleoducto Norperuano"/>', className: 'infoOleoducto' });
        var marker = L.marker([-4.914122, -78.153794], { icon: icon }).on('click', onClick);

        clusters.oleoducto.items.push({ markers: marker });
        L.layerGroup([marker]).addTo(lyr);

        function onClick(e) {
            var obj = { method: 'distXoleoducto' }
            $.get({
                url: 'popupOleoducto.php',
                success: function(response) {
                    dialog3.setContent(response);
                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            var barChart1 = barChart;
                            if (Object.keys(response).length > 0) {
                                barChart.xAxis.categories = response.data.categories;
                                barChart.series = response.data.series;
                                barChart.title.text = 'Distribucion de CANON, CIPRL Y PIM por tramos';
                                barChart.plotOptions.series.point.events.click = function(event) {
                                    document.getElementById("chart_distXoleoducto12").style.display = "";
                                    var objphp = {
                                        method: 'distxOleoductoxTramo',
                                        ramal: event.point.category
                                    };
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                        success: function(response1) {
                                            barChart1.xAxis.categories = response1.data.categories;
                                            barChart1.title.text = 'Distribucion por Distritos de CANON, CIPRL Y PIM del Ramal ' + event.point.category + " del Oleoducto";
                                            barChart1.series = response1.data.series;
                                            Highcharts.chart('chart_distXoleoducto1', barChart1);
                                        }
                                    });
                                }
                                Highcharts.chart('chart_distXoleoducto', barChart);
                            }
                        }
                    });
                    var url = "http://95.217.44.43:4000/getubigeosGeojson";
                    axios.post(url, { tipo: 'oleoducto', corredor: 1 })
                        .then(function(response) {
                            var obj = {
                                distritos: " (" + response.data.dist.join(',') + ")"
                            }
                            $.post({
                                url: 'divubigeoInfraObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    document.getElementById('divinfraObjetos').innerHTML = response;
                                }
                            });
                        })
                        .catch(function(error) {
                            console.log(error);
                        });

                }
            });
        }
    }
    var loadOleoducto = function() {
        loadOleoductoInfo();
        var lyr = filterGroup('overLayers', 'baseMineria', 'oleoducto').layer;
        const url = "http://95.217.44.43:4000/getOleoducto";
        axios.get(url).then(function(response) {
            var geoData = L.geoJSON(response.data[0].vo_geom, {
                onEachFeature: function(fData, fLayer) {
                    fLayer.on('mouseover', function(e) {
                        var layer = e.target;
                        layer.setStyle({
                            weight: 7,
                            color: '#4040ff',
                            dashArray: '',
                            fillOpacity: 0.7
                        });

                        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                            layer.bringToFront();
                        }
                    });

                    fLayer.on('mouseout', function(e) {
                        geoData.resetStyle(e.target);
                    });

                    fLayer.addTo(lyr);
                },
                style: function(feature) {
                    return {
                        fillColor: '#00000000',
                        fillOpacity: 0.65,
                        color: '#00000000',
                        weight: 5,
                        opacity: 0.9
                    };
                }
            });
            geoData.on('click', function(e) {
                if (tmpOleoducto.indexOf("'oleoducto_1'") == -1) {
                    tmpOleoducto.push("'oleoducto_1'");
                    toastr.success("Usted marco Oleoducto norperuano", "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
                } else {
                    tmpOleoducto.splice(tmpOleoducto.indexOf("'oleoducto_1'"), 1);
                    toastr.warning("Usted desmarco Oleoducto norperuano", "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
                }

                document.getElementById("txtTipoFiltro").value = 'oleoducto';
                document.getElementById('txtCorredor').value = 1;
                getLyrDrawUbigeos();
            });

        }).catch(function(error) {
            console.log(error);
        });

    }
    var loadLineaFerreaInfo = function() {
        var lyr = filterGroup('overLayers', 'baseExtras', 'ferreas').layer;
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_info_ferrea',
                        sql: ''
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info4.png" title="' + feature.properties.nam + '" border="0"/>', className: 'info' })
                        });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });
                markers.on('click', function(e) {
                    console.log(e.layer.feature.properties);
                    var url = "http://95.217.44.43:4000/getubigeosGeojson";
                    axios.post(url, { tipo: 'lineaferrea', corredor: e.layer.feature.properties.gid })
                        .then(function(response) {
                            var obj = {
                                distritos: " (" + response.data.dist.join(',') + ")",
                                nombre: e.layer.feature.properties.nam,
                                id: e.layer.feature.properties.gid
                            }
                            $.post({
                                url: 'popuplineaferrea.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    dialog4.setContent(response);
                                    var obj1 = {
                                        method: 'distXlineaFerrea',
                                        nam: e.layer.feature.properties.nam,
                                    }
                                    var barChart1 = barChart;
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            if (Object.keys(response).length > 0) {
                                                barChart.xAxis.categories = response.data.categories;
                                                barChart.series = response.data.series;
                                                barChart.title.text = 'Distribución por Región de CANON, CIPRL Y PIM del ' + e.layer.feature.properties.nam;
                                                barChart.plotOptions.series.point.events.click = function(event) {
                                                    document.getElementById("chart_distXlineaFerrea12").style.display = "";
                                                    var objphp = {
                                                        method: 'distXlineaFerreawidthDistrito',
                                                        region: event.point.category,
                                                        nam: e.layer.feature.properties.nam
                                                    };
                                                    $.getJSON({
                                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                                        success: function(response1) {
                                                            barChart1.xAxis.categories = response1.data.categories;
                                                            barChart1.title.text = 'Distribución por Distritos de CANON, CIPRL Y PIM de la Region ' + event.point.category + " del " + e.layer.feature.properties.nam;
                                                            barChart1.series = response1.data.series;
                                                            Highcharts.chart('chart_distXlineaFerrea1', barChart1);
                                                        }
                                                    });
                                                }
                                                Highcharts.chart('chart_distXlineaFerrea', barChart);
                                            }
                                        }
                                    });
                                    var obj1 = {
                                        distritos: obj.distritos
                                    }
                                    $.post({
                                        url: 'divubigeoInfraObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            document.getElementById('divinfraObjetosferrea').innerHTML = response;
                                        }
                                    });
                                }
                            });
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                });
            }
        });

    }
    var loadLineaFerrea = function() {
        loadLineaFerreaInfo();
        var lyr = filterGroup('overLayers', 'baseExtras', 'ferreas').layer;
        const url = "http://95.217.44.43:4000/getLineaFerrea";
        axios.get(url).then(function(response) {
                var geoData = L.geoJSON(response.data[0].vo_geom, {
                    onEachFeature: function(fData, fLayer) {
                        fLayer.on('mouseover', function(e) {
                            var layer = e.target;
                            layer.setStyle({
                                weight: 7,
                                color: '#4040ff',
                                dashArray: '',
                                fillOpacity: 0.7
                            });

                            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                                layer.bringToFront();
                            }
                        });

                        fLayer.on('mouseout', function(e) {
                            geoData.resetStyle(e.target);
                        });

                        fLayer.addTo(lyr);
                    },
                    style: function(feature) {
                        return {
                            fillColor: '#00000000',
                            fillOpacity: 0.65,
                            color: '#00000000',
                            weight: 5,
                            opacity: 0.9
                        };
                    }
                });
                geoData.on('click', function(e) {
                    if (tmpLineas.indexOf("'lineaferrea_" + e.layer.feature.properties.gid + "'") == -1) {
                        tmpLineas.push("'lineaferrea_" + e.layer.feature.properties.gid + "'");
                        toastr.success("Usted marco " + e.layer.feature.properties.nam, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
                    } else {
                        tmpLineas.splice(tmpLineas.indexOf("'lineaferrea_" + e.layer.feature.properties.gid + "'"), 1);
                        toastr.warning("Usted desmarco " + e.layer.feature.properties.nam, "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
                    }
                    document.getElementById("txtTipoFiltro").value = 'lineaferrea';
                    document.getElementById("txtCorredor").value = e.layer.feature.properties.gid;
                    getLyrDrawUbigeos();
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    var loadCorredorMineroInfo = function() {
        var lyr = filterGroup('overLayers', 'baseMineria', 'corredor').layer;
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_corredor_ubigeo',
                        sql: ''
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info4.png" title="' + feature.properties.descripcion + '" border="0"/>', className: 'info' })
                        });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });
                markers.on('click', function(e) {});
            }
        });
    }
    var loadCorredorMinero = function() {
        loadCorredorMineroInfo();
        var lyr = filterGroup('overLayers', 'baseMineria', 'corredor').layer;
        const url = "http://95.217.44.43:4000/getCorredoresMineros";
        axios.get(url).then(function(response) {
                var geoData = L.geoJSON(response.data[0].vo_geom, {
                    onEachFeature: function(fData, fLayer) {
                        fLayer.on('mouseover', function(e) {
                            var layer = e.target;
                            layer.setStyle({
                                weight: 7,
                                color: '#4040ff',
                                dashArray: '',
                                fillOpacity: 0.7
                            });

                            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                                layer.bringToFront();
                            }
                        });

                        fLayer.on('mouseout', function(e) {
                            geoData.resetStyle(e.target);
                        });

                        fLayer.addTo(lyr);
                    },
                    style: function(feature) {
                        return {
                            fillColor: '#00000000',
                            fillOpacity: 0.65,
                            color: '#00000000',
                            weight: 7,
                            opacity: 0.9
                        };
                    }
                });
                geoData.on('click', function(e) {
                    if (tmpMineros.indexOf("'minero_" + e.layer.feature.properties.id + "'") == -1) {
                        tmpMineros.push("'minero_" + e.layer.feature.properties.id + "'");
                        toastr.success("Usted marco " + e.layer.feature.properties.descripcion, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
                    } else {
                        tmpMineros.splice(tmpMineros.indexOf("'minero_" + e.layer.feature.properties.id + "'"), 1);
                        toastr.warning("Usted desmarco " + e.layer.feature.properties.descripcion, "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
                    }
                    document.getElementById("txtTipoFiltro").value = 'corredorminero';
                    document.getElementById("txtCorredor").value = e.layer.feature.properties.id;

                    getLyrDrawUbigeos();
                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    var loadCorredoresLoginsticosInfo = function() {
        var lyr = filterGroup('overLayers', 'baseExtras', 'corrprincipales').layer;
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_info_corredoreslogisticos',
                        sql: ''
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info4.png" title="' + feature.properties.corredor + '" border="0"/>', className: 'info' })
                        });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });
                markers.on('click', function(e) {
                    var url = "http://95.217.44.43:4000/getubigeosGeojson";
                    axios.post(url, { tipo: 'corredorlogistico', corredor: e.layer.feature.properties.id })
                        .then(function(response) {
                            var obj = {
                                distritos: " (" + response.data.dist.join(',') + ")",
                                nombre: e.layer.feature.properties.corredor,
                                id: e.layer.feature.properties.id
                            }
                            console.log(e.layer.feature.properties);
                            $.post({
                                url: 'popuplogistico.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    dialog5.setContent(response);
                                    var obj11 = {
                                        method: 'distxCorredoresLogisticosxRuta',
                                        ruta: e.layer.feature.properties.corredor,
                                    }

                                    var barChart1 = barChart;
                                    var ruta;
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj11)),
                                        success: function(response) {
                                            if (Object.keys(response).length > 0) {
                                                barChart.xAxis.categories = response.data.categories;
                                                barChart.series = response.data.series;
                                                barChart.title.text = 'Distribución por Rutas de CANON, CIPRL Y PIM del ' + e.layer.feature.properties.corredor;
                                                barChart.plotOptions.series.point.events.click = function(event) {
                                                    document.getElementById("chart_distXCorredoresLogisticos2").style.display = "";
                                                    ruta = event.point.category;
                                                    var objphp = {
                                                        method: 'distxCorredoresLogisticosxRegion',
                                                        ruta: event.point.category,
                                                        corredor: e.layer.feature.properties.corredor
                                                    };
                                                    $.getJSON({
                                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                                        success: function(response1) {
                                                            var barChart2 = barChart;
                                                            barChart1.xAxis.categories = response1.data.categories;
                                                            barChart1.title.text = 'Distribución por Regiones de CANON, CIPRL Y PIM de la Ruta ' + event.point.category + " del " + e.layer.feature.properties.corredor;
                                                            barChart1.series = response1.data.series;
                                                            barChart.plotOptions.series.point.events.click = function(event) {
                                                                document.getElementById("chart_distXCorredoresLogisticos3").style.display = "";
                                                                var objphp = {
                                                                    method: 'distxCorredoresLogisticosxdistrito',
                                                                    region: event.point.category,
                                                                    ruta: ruta,
                                                                    corredor: e.layer.feature.properties.corredor
                                                                };
                                                                $.getJSON({
                                                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                                                    success: function(response2) {
                                                                        barChart2.xAxis.categories = response2.data.categories;
                                                                        barChart2.title.text = 'Distribución por Distritos de CANON, CIPRL Y PIM de la Region ' + event.point.category + " del " + e.layer.feature.properties.corredor;
                                                                        barChart2.series = response2.data.series;

                                                                        Highcharts.chart('chart_distXCorredoresLogisticos4', barChart2);

                                                                    }
                                                                });
                                                            };
                                                            Highcharts.chart('chart_distXCorredoresLogisticos1', barChart1);
                                                        }
                                                    });
                                                }
                                                Highcharts.chart('chart_distXCorredoresLogisticos', barChart);
                                            }
                                        }
                                    });

                                    var obj1 = {
                                        distritos: obj.distritos
                                    }
                                    $.post({
                                        url: 'divubigeoInfraObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {

                                            document.getElementById('divinfraObjetoslogistico').innerHTML = response;

                                        }
                                    });
                                    var obj2 = {
                                        corredor: e.layer.feature.properties.corredor
                                    }
                                    $.post({
                                        url: 'divAccidentesXCorredor.php?data=' + encodeURIComponent(JSON.stringify(obj2)),
                                        success: function(response) {

                                            document.getElementById('divaccidentesLogisiticos').innerHTML = response;

                                        }
                                    });



                                }
                            });
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                });
            }
        });
    }
    var handleLayerVias = function(layer) {
        layer.setStyle({
            fillColor: '#00000000',
            fillOpacity: 0.65,
            color: '#00000000',
            weight: 8,
            opacity: 0.9
        });
        layer.on('click', function(e) {
            if (tmpVias.indexOf("'corredorlogistico_" + e.target.feature.properties.gid + "'") == -1) {
                tmpVias.push("'corredorlogistico_" + e.target.feature.properties.gid + "'");
                toastr.success("Usted marco " + e.target.feature.properties.corredor, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
            } else {
                tmpVias.splice(tmpVias.indexOf("'corredorlogistico_" + e.target.feature.properties.gid + "'"), 1);
                toastr.warning("Usted desmarco " + e.target.feature.properties.corredor, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
            }
            document.getElementById("txtTipoFiltro").value = 'corredorlogistico';
            document.getElementById("txtCorredor").value = e.target.feature.properties.gid;

            getLyrDrawUbigeos();

        });
        layer.on({
            mouseover: function enterLayerVias() {
                this.bringToFront();
                this.setStyle({
                    weight: 7,
                    color: '#4040ff',
                    dashArray: '',
                    fillOpacity: 0.4
                });
            },
            mouseout: function leaveLayerVias() {
                this.bringToBack();
                this.setStyle({
                    fillColor: '#00000000',
                    fillOpacity: 0.65,
                    color: '#00000000',
                    weight: 8,
                    opacity: 0.9
                });
            }
        });
    }
    var loadCorredorLogistico = function() {
        loadCorredoresLoginsticosInfo();
        var lyr = filterGroup('overLayers', 'baseExtras', 'corrprincipales').layer;
        $.getJSON('assets/app/json/corredoressviales.json', function(data) {
            topoLayerVias.addData(data);
            topoLayerVias.addTo(lyr);
            topoLayerVias.eachLayer(handleLayerVias);
        });
    }
    var loadHidrografiaInfo = function() {
        var lyr = filterGroup('overLayers', 'baseExtras', 'corrhidrovias').layer;
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'gen_infohidrografia',
                        sql: ''
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info4.png" title="' + feature.properties.nombre + '" border="0"/>', className: 'info' })
                        });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });
                markers.on('click', function(e) {
                    console.log(e.layer.feature.properties);
                    var url = "http://95.217.44.43:4000/getubigeosGeojson";
                    axios.post(url, { tipo: 'hidrografia', corredor: e.layer.feature.properties.id })
                        .then(function(response) {
                            var obj = {
                                distritos: " (" + response.data.dist.join(',') + ")",
                                nombre: e.layer.feature.properties.nombre,
                                id: e.layer.feature.properties.id
                            }
                            $.post({
                                url: 'popuphidrovias.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    dialog6.setContent(response);
                                    var obj1 = {
                                        method: 'distXhidrovias',
                                        id: e.layer.feature.properties.id,
                                    }
                                    var barChart1 = barChart;
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            if (Object.keys(response).length > 0) {
                                                barChart.xAxis.categories = response.data.categories;
                                                barChart.series = response.data.series;
                                                barChart.title.text = 'Distribución por Región de CANON, CIPRL Y PIM del ' + e.layer.feature.properties.nombre;
                                                barChart.plotOptions.series.point.events.click = function(event) {
                                                    document.getElementById("chart_distXhidrovias12").style.display = "";
                                                    var objphp = {
                                                        method: 'distXhidroviaswidthDistrito',
                                                        region: event.point.category,
                                                        id: e.layer.feature.properties.id
                                                    };
                                                    $.getJSON({
                                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objphp)),
                                                        success: function(response1) {
                                                            barChart1.xAxis.categories = response1.data.categories;
                                                            barChart1.title.text = 'Distribución por Distritos de CANON, CIPRL Y PIM de la Region ' + event.point.category + " del " + e.layer.feature.properties.nombre;
                                                            barChart1.series = response1.data.series;
                                                            Highcharts.chart('chart_distXhidrovias1', barChart1);
                                                        }
                                                    });
                                                }
                                                Highcharts.chart('chart_distXhidrovias', barChart);
                                            }
                                        }
                                    });
                                    var obj1 = {
                                        distritos: obj.distritos
                                    }
                                    $.post({
                                        url: 'divubigeoInfraObjetos.php?data=' + encodeURIComponent(JSON.stringify(obj1)),
                                        success: function(response) {
                                            document.getElementById('divinfraObjetoshidrovias').innerHTML = response;
                                        }
                                    });
                                }
                            });
                        })
                        .catch(function(error) {
                            console.log(error);
                        });
                });
            }
        });
    }
    var handleLayerHidrografia = function(layer) {
        layer.setStyle({
            fillColor: '#00000000',
            fillOpacity: 0.65,
            color: '#00000000',
            weight: 8,
            opacity: 0.9
        });
        layer.on('click', function(e) {

            if (tmpHidrografias.indexOf("'hidrografia_" + e.target.feature.properties.id + "'") == -1) {
                tmpHidrografias.push("'hidrografia_" + e.target.feature.properties.id + "'");
                toastr.success("Usted marco " + e.target.feature.properties.nombre, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
            } else {
                tmpHidrografias.splice(tmpHidrografias.indexOf("'hidrografia_" + e.target.feature.properties.id + "'"), 1);
                toastr.warning("Usted desmarco " + e.target.feature.properties.nombre, "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
            }
            document.getElementById("txtTipoFiltro").value = 'hidrografia';
            document.getElementById("txtCorredor").value = e.target.feature.properties.id;

            getLyrDrawUbigeos();
        });
        layer.on({
            mouseover: function enterLayerVias() {
                this.bringToFront();
                this.setStyle({
                    weight: 7,
                    color: '#4040ff',
                    dashArray: '',
                    fillOpacity: 0.4
                });
            },
            mouseout: function leaveLayerVias() {
                this.bringToBack();
                this.setStyle({
                    fillColor: '#00000000',
                    fillOpacity: 0.65,
                    color: '#00000000',
                    weight: 8,
                    opacity: 0.9
                });
            }
        });
    }
    var loadHidrografia = function() {
        loadHidrografiaInfo();
        var lyr = filterGroup('overLayers', 'baseExtras', 'corrhidrovias').layer;
        $.getJSON('assets/app/json/hidro.json', function(data) {
            topoLayerHidro.addData(data);
            topoLayerHidro.addTo(lyr);
            topoLayerHidro.eachLayer(handleLayerHidrografia);
        });
    }
    var loadAccidentes = function() {
        var lyr = filterGroup('overLayers', 'baseExtras', 'accidentes').layer;
        const url = "http://95.217.44.43:4000/getAccidentesTramos";
        axios.get(url).then(function(response) {
                var geoData = L.geoJSON(response.data[0].vo_geom, {
                    onEachFeature: function(fData, fLayer) {
                        fLayer.on('mouseover', function(e) {
                            var layer = e.target;
                            layer.setStyle({
                                weight: 7,
                                color: '#4040ff',
                                dashArray: '',
                                fillOpacity: 0.7
                            });

                            if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
                                layer.bringToFront();
                            }
                        });

                        fLayer.on('mouseout', function(e) {
                            geoData.resetStyle(e.target);
                        });

                        fLayer.addTo(lyr);
                    },
                    style: function(feature) {
                        return {
                            fillColor: '#00000000',
                            fillOpacity: 0.65,
                            color: '#00000000',
                            weight: 7,
                            opacity: 0.9
                        };
                    }
                });
                geoData.on('click', function(e) {
                    if (tmpAccidentes.indexOf("'accidentes_" + e.layer.feature.properties.gid + "'") == -1) {
                        tmpAccidentes.push("'accidentes_" + e.layer.feature.properties.gid + "'");
                        toastr.success("Usted marco " + e.layer.feature.properties.tca, "Correcto", { timeOut: 2000, closeButton: true, progressBar: true });
                    } else {
                        tmpAccidentes.splice(tmpAccidentes.indexOf("'accidentes_" + e.target.feature.properties.gid + "'"), 1);
                        toastr.warning("Usted desmarco " + e.layer.feature.properties.tca, "Advertencia", { timeOut: 2000, closeButton: true, progressBar: true });
                    }
                    document.getElementById("txtTipoFiltro").value = 'accidentes';
                    document.getElementById("txtCorredor").value = e.layer.feature.properties.gid;

                    getLyrDrawUbigeos();

                });
            })
            .catch(function(error) {
                console.log(error);
            });
    }
    var wordCAloud1 = function(obj1) {
        Highcharts.chart(obj1.div, {

            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function() {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>  <b>Monto S/ : </b>' + point.soles + '<br/>';
                }
            }
        });
    }
    var chartPie = function(data) {

        $.getJSON({
            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(data)),
            success: function(response) {
                var obj = window['pieChart'];
                if (response.data.data.length == 0) {
                    obj.title.text = 'Sin proyectos';
                } else {
                    obj.title.text = '';
                    $('#' + data.container).closest('.card').find('.card-footer').html(response.data.total + ' PIPs')
                }

                obj.series = [response.data];
                Highcharts.chart(data.container, obj);
            }
        });
    }
    var loadCiprlXubigeo = function(layerName, group, cql_filter) {
        group.clearLayers();

        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: layerName,
                        sql: cql_filter.length == 0 ? '' : cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info.png" title="' + feature.properties.nomubigeo + '" border="0"/>', className: 'info' })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(group);
                    }
                });
                markers.on('click', function(e) {
                    var data = e.layer.feature.properties;
                    delete data.method;

                    $.get({
                        url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(data)),
                        success: function(response) {
                            data.method = 'ciprlXregion';
                            if (screen.width <= 1224) {

                                dialog9.setLocation([95, 50]);

                            }
                            dialog9.setContent(response);
                            if (data.idnivel == 1 || data.idnivel == 2 || data.idnivel == 3) {
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(data)),
                                    success: function(response) {
                                        console.log(response);
                                        if (Object.keys(response).length > 0) {
                                            var chart = window['combinateChart'];
                                            chart.series = response.data.series;
                                            chart.plotOptions.series.pointStart = response.data.pointStart;
                                            Highcharts.chart('chart_' + data.codubigeo, chart);
                                        }
                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_covid',
                                    idnivel: data.idnivel
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos COVID-19';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartCovidEntidad', Chart1);

                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_sinadef',
                                    idnivel: data.idnivel
                                }
                                $.getJSON({
                                    url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                    success: function(response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos SINADEF';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadefEntidad', Chart1);

                                    }
                                });
                            }
                            chartPie({ 'container': 'pieDistrito', 'method': 'indXfuncion', 'codnivel': data.idnivel, 'codfase': 4, 'codubigeo': padWithZeroes(data.codubigeo, 2) });
                            //chartPie({ 'container': 'pieProvincia', 'method': 'indXfuncion', 'codnivel': data.idnivel, 'codfase': 6, 'codubigeo': padWithZeroes(data.codubigeo, 2) });
                            chartPie({ 'container': 'pieRegion', 'method': 'indXfuncion', 'codnivel': data.idnivel, 'codfase': 2, 'codubigeo': padWithZeroes(data.codubigeo, 2) });

                            var obj = {
                                idnivel: data.idnivel,
                                codubigeo: document.getElementById('codUbigeoPIP').value,
                                idnivGob: 02
                            };
                            $.post({
                                url: 'divpipProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                success: function(response) {
                                    document.getElementById('divpipRegiones').innerHTML = response;
                                }
                            });

                            var objWord = {
                                method: 'wordRbtnPipsTotal',
                                codubigeo: document.getElementById('codUbigeoPIP').value,
                                codnivel: data.idnivel,
                                tipoChange: 'ofq.nomfuncionproyecto'
                            };
                            $.getJSON({
                                url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objWord)),
                                success: function(response) {
                                    var word = JSON.parse(response.data[0].subprograma);
                                    console.log(word);
                                    var objEnvioWord = {
                                        data: word,
                                        div: 'divword2'
                                    };

                                    wordCAloud1(objEnvioWord);
                                }
                            });
                            var objCPE = {
                                tipo: 'ciprl',
                                checked: 1,
                                region: document.getElementById('coddptoPF').value
                            }

                            $.post({
                                url: 'divCiprlPoblacionElectoresQuintil.php?data=' + encodeURIComponent(JSON.stringify(objCPE)),
                                success: function(response) {
                                    document.getElementById('ciprlpoelec').innerHTML = response;
                                }
                            });
                        }
                    });
                });
            }
        });
    }
    var loadSaludXubigeo = function(layerName, group, cql_filter) {
        group.clearLayers();

        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: layerName,
                        sql: cql_filter.length == 0 ? '' : cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.divIcon({ html: '<img src="assets/app/img/info.png" title="' + feature.properties.nomubigeo + '" border="0"/>', className: 'info' })
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(group);
                    }
                });
                markers.on('click', function(e) {
                    var data = e.layer.feature.properties;
                    var obj = {
                        method: 'fichaEntidadAmpliar',
                        tipo: data.tipo,
                        ubigeo: data.ubigeo
                    }

                    $.getJSON({
                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            console.log(response);
                            var envio = response.data[0];
                            $.get({
                                url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                success: function(response) {
                                    envio.method = 'ciprlXregion';
                                    if (screen.width <= 1224) {

                                        dialog9.setLocation([95, 50]);

                                    }
                                    dialog9.setContent(response);
                                    if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)),
                                            success: function(response) {
                                                if (Object.keys(response).length > 0) {
                                                    var chart = window['combinateChart'];
                                                    chart.series = response.data.series;
                                                    chart.plotOptions.series.pointStart = response.data.pointStart;
                                                    Highcharts.chart('chart_' + envio.codubigeo, chart);
                                                }
                                            }
                                        });
                                        var objQ = {
                                            method: 'quintiles_sinadef',
                                            idnivel: envio.idnivel
                                        }
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                            success: function(response1) {
                                                var categorias = [];
                                                var obj2017 = {
                                                    name: 'Entidades',
                                                    type: 'column',
                                                    yAxis: 1,
                                                    color: '#4572A7',
                                                    data: []
                                                };
                                                var obj2017_Pob = {
                                                    name: 'Fallecidos',
                                                    type: 'spline',
                                                    color: '#A7454F',
                                                    data: []
                                                };

                                                for (i = 0; i < response1.data.length; i++) {
                                                    categorias.push(response1.data[i].texto);
                                                    obj2017.data.push(parseInt(response1.data[i].entidades));
                                                    obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                                }
                                                var total = [obj2017, obj2017_Pob];

                                                var Chart1 = dualChart;
                                                Chart1.xAxis[0].categories = categorias;
                                                Chart1.title.text = 'Fallecidos SINADEF';

                                                Chart1.series = total;
                                                Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                                Highcharts.chart('chartSinadefEntidad', Chart1);

                                            }
                                        });
                                        var objQ = {
                                            method: 'quintiles_covid',
                                            idnivel: envio.idnivel
                                        }
                                        $.getJSON({
                                            url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)),
                                            success: function(response1) {
                                                var categorias = [];
                                                var obj2017 = {
                                                    name: 'Entidades',
                                                    type: 'column',
                                                    yAxis: 1,
                                                    color: '#4572A7',
                                                    data: []
                                                };
                                                var obj2017_Pob = {
                                                    name: 'Fallecidos',
                                                    type: 'spline',
                                                    color: '#A7454F',
                                                    data: []
                                                };

                                                for (i = 0; i < response1.data.length; i++) {
                                                    categorias.push(response1.data[i].texto);
                                                    obj2017.data.push(parseInt(response1.data[i].entidades));
                                                    obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                                }
                                                var total = [obj2017, obj2017_Pob];

                                                var Chart1 = dualChart;
                                                Chart1.xAxis[0].categories = categorias;
                                                Chart1.title.text = 'Fallecidos COVID-19';

                                                Chart1.series = total;
                                                Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                                Highcharts.chart('chartCovidEntidad', Chart1);

                                            }
                                        });
                                    }
                                    chartPie({ 'container': 'pieDistrito', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 4, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });
                                    //chartPie({ 'container': 'pieProvincia', 'method': 'indXfuncion', 'codnivel': data.idnivel, 'codfase': 6, 'codubigeo': padWithZeroes(data.codubigeo, 2) });
                                    chartPie({ 'container': 'pieRegion', 'method': 'indXfuncion', 'codnivel': envio.idnivel, 'codfase': 2, 'codubigeo': padWithZeroes(envio.codubigeo, 2) });

                                    var obj = {
                                        idnivel: envio.idnivel,
                                        codubigeo: envio.codubigeo,
                                        idnivGob: 02
                                    };
                                    $.post({
                                        url: 'divpipProjects.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                                        success: function(response) {
                                            document.getElementById('divpipRegiones').innerHTML = response;
                                        }
                                    });

                                    var objWord = {
                                        method: 'wordRbtnPipsTotal',
                                        codubigeo: envio.codubigeo,
                                        codnivel: envio.idnivel,
                                        tipoChange: 'ofq.nomfuncionproyecto'
                                    };
                                    $.getJSON({
                                        url: 'php/app.php?data=' + encodeURIComponent(JSON.stringify(objWord)),
                                        success: function(response) {
                                            var word = JSON.parse(response.data[0].subprograma);
                                            console.log(word);
                                            var objEnvioWord = {
                                                data: word,
                                                div: 'divword2'
                                            };

                                            wordCAloud1(objEnvioWord);
                                        }
                                    });
                                    var objCPE = {
                                        tipo: 'ciprl',
                                        checked: 1,
                                        region: document.getElementById('coddptoPF').value
                                    }

                                    $.post({
                                        url: 'divCiprlPoblacionElectoresQuintil.php?data=' + encodeURIComponent(JSON.stringify(objCPE)),
                                        success: function(response) {
                                            document.getElementById('ciprlpoelec').innerHTML = response;
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
    }
    var loadComisarias = function(cql_filter) {
        var lyr = filterGroup('overLayers', 'baseExtras', 'comisarias').layer;
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'gen_comisarias',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function(data) {
                var markers = L.geoJson(data, {
                    pointToLayer: function(feature, latlng) {
                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-school',
                                iconColor: 'white',
                                markerColor: 'blue',
                                draggable: true,
                                shape: 'circle',
                                prefix: 'fas'
                            })
                        });
                        marker.bindPopup("Cargando...", { closeButton: true, minWidth: 600 });
                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        layer.addTo(lyr);
                    }
                });
                markers.on('click', function(e) {
                    var obj = e.layer.feature.properties;
                    $.get({
                        url: 'popupComisarias.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                        success: function(response) {
                            e.layer._popup.setContent(response);
                        }
                    });
                });
            }
        });
    }
    var loadHospitales = function(cql_filter) {
            var lyr = filterGroup('overLayers', 'baseExtras', 'hospitales').layer;
            $.ajax({
                url: owsrootUrl + L.Util.getParamString(
                    L.Util.extend(
                        defaultParameters({
                            layerName: 'gen_hospitales',
                            sql: cql_filter
                        })
                    )),
                dataType: 'json',
                success: function(data) {
                    var markers = L.geoJson(data, {
                        pointToLayer: function(feature, latlng) {
                            var marker = L.marker(latlng, {
                                icon: L.ExtraMarkers.icon({
                                    icon: 'fa-hospital',
                                    iconColor: 'black',
                                    markerColor: 'white',
                                    draggable: true,
                                    shape: 'circle',
                                    prefix: 'fas'
                                })
                            });
                            marker.bindPopup("Cargando...", { closeButton: true, minWidth: 600 });

                            return marker;
                        },
                        onEachFeature: function(feature, layer) {
                            layer.addTo(lyr);
                        }
                    });
                    markers.on('click', function(e) {
                        var obj = e.layer.feature.properties;
                        $.get({
                            url: 'popupHospitales.php?data=' + encodeURIComponent(JSON.stringify(obj)),
                            success: function(response) {
                                e.layer._popup.setContent(response);
                            }
                        });
                    });
                }
            });


        }
        //Aqui terminan los loads

    var ddlProvincia = function() {
        var tmp = [];
        var text = '';
        var data = filterArr('filterParams', 'key', 'ddlRegion').selected;

        $('#ddlProvincia optgroup option').toggle(true);

        for (var i = 0; i < data.length; i++) {
            text = $('#ddlRegion option[value="' + padWithZeroes(data[i], 2) + '"]').text();
            tmp.push(
                '[label!="' + text + '"]'
            );
        }
        $('#ddlProvincia optgroup' + tmp.join('') + ' option').toggle(false);
        $('#ddlProvincia').selectpicker('refresh');
    }
    var ddlProvincia1 = function() {
        var tmp = [];
        var text = '';
        var data = filterArr('filterParams', 'key', 'ddlRegion3').selected;

        $('#ddlProvincia2 optgroup option').toggle(true);

        for (var i = 0; i < data.length; i++) {
            text = $('#ddlRegion3 option[value="' + padWithZeroes(data[i], 2) + '"]').text();
            tmp.push(
                '[label!="' + text + '"]'
            );
        }

        $('#ddlProvincia2 optgroup' + tmp.join('') + ' option').toggle(false);
        $('#ddlProvincia2').selectpicker('refresh');
    }

    var defaultParameters = function(obj) {
        var params = {
            service: 'WFS',
            version: '1.0.0',
            request: 'GetFeature',
            typeName: 'colaboraccion_20:' + obj.layerName,
            outputFormat: 'application/json'
        };
        if (obj.hasOwnProperty('sql')) {
            if (obj.sql.length > 0) {
                params.cql_filter = obj.sql;
            }
        }
        return params;
    }
    var counterSum = function(sql) {
        var data = { method: 'counterSum', sql_filter: sql };
        $.get({
            url: 'counterMap.php?obj=' + encodeURIComponent(JSON.stringify(data)),
            success: function(response) {
                $('.counterSum').html(response);
            }
        });
    }
    var counterEmpresa = function(sql) {
        var data = { method: 'counterSumEmpresa', sql_filter: sql };
        $.get({
            url: 'counterEmpresa.php?obj=' + encodeURIComponent(JSON.stringify(data)),
            success: function(response) {
                $('#counterEmpresa').html(response);
            }
        });
    }
    var createCluster = function(fase) {
        return L.markerClusterGroup({
            iconCreateFunction: function(cluster) {
                return new L.DivIcon({
                    className: 'marker-cluster marker-cluster-' + fase,
                    iconSize: [40, 40],
                    html: '<div><span>' + cluster.getChildCount() + '</span></div>'
                });
            }
        });
    }
    var clearLayers = function(cluster) {
        for (var key in cluster) {
            if (cluster.hasOwnProperty(key)) {
                if (Object.keys(cluster[key]).length > 0) {
                    cluster[key].clearLayers();
                }
            }
        }
    }
    var clearFilter = function(lyr) {
        filterArr('filterParams', 'lyr', lyr, -1).forEach(function(row) {
            switch (Array.isArray(row.selected)) {
                case true:
                    row.selected = [];
                    break;
                case false:
                    if (Object.keys(row.selected).length > 0) {
                        $.each(row.selected, function(key, value) {
                            row.selected[key] = '';
                        });
                    }
                    break;
            }
        });
    }
    var format = function(type, value) {
        if (value.trim().length == 0) {
            return '';
        }

        switch (type) {
            case 'Number':
                return Number(value);
                break;
            case 'String':
                return "'" + value + "'";
                break;
            case 'Date':
                var dt = value.split('/');
                return "'" + dt[2] + '.' + dt[1] + '.' + dt[0] + "'";
                break;
        }
    }
    var padWithZeroes = function(number, length) {
        var my_string = '' + number;
        while (my_string.length < length) {
            my_string = '0' + my_string;
        }

        return my_string;
    }
    var lyrDraw = function(lyrs, cql_filter) {
        lyrs.forEach(function(lyr) {
            lyr.wmsParams.cql_filter = cql_filter;
            lyr.redraw();
        });
    }
    var filterArr = function(arr, key, value, nRows = 1) {
        var data = window[arr].filter(function(param) {
            return param[key] == value;
        });

        if (nRows == 1) {
            return data[0];
        }

        return data;
    }
    var findBootEnv = function() {
        let envs = ['xs', 'sm', 'md', 'lg', 'xl'];

        let el = document.createElement('div');
        document.body.appendChild(el);

        let curEnv = envs.shift();

        for (let env of envs.reverse()) {
            el.classList.add(`d-${env}-none`);

            if (window.getComputedStyle(el).display === 'none') {
                curEnv = env;
                break;
            }
        }

        document.body.removeChild(el);
        return curEnv;
    }
    var filterGroup = function(arr, groupKey, itemKey) {
        return window[arr].filter(p => p.key == groupKey).shift().layers.filter(p => p.key == itemKey).shift();
    }
    var filter = function(lyr, exception = '') {
        var cql_filter = [];
        var values = [];
        var row = [];

        filterArr('filterParams', 'lyr', lyr, -1).forEach(function(row) {

            if (row.active != 'N') {
                switch (Array.isArray(row.selected)) {
                    case true:
                        if (lyr == 'projects' && row.key != exception) {
                            if (row.selected.length > 0) {
                                cql_filter.push(row.column + " in(" + row.selected.join(',') + ")");

                            }
                        } else if (lyr == 'companies' && row.key != exception) {
                            if (row.selected.length > 0) {
                                cql_filter.push(row.column + " in(" + row.selected.join(',') + ")");
                            }
                        } else if (lyr == 'projects1' && row.key != exception) {
                            if (row.selected.length > 0) {
                                cql_filter.push(row.column + " in(" + row.selected.join(',') + ")");
                            }
                        }
                        break;
                    case false:
                        values = [];

                        $.each(row.selected, function(idx, val) {
                            if (val.toString().length > 0) {
                                values.push(val);
                            }
                        });

                        if (values.length == 1) {
                            if (row.hasOwnProperty("operator")) {
                                cql_filter.push(row.column + " like " + values);

                            } else {
                                cql_filter.push(row.column + " = " + values);
                            }
                        }

                        if (values.length == 2) {
                            cql_filter.push(row.column + " between " + values.join(' and '));
                        }
                        break;
                }
            }
        });

        if (cql_filter.length == 0) {
            cql_filter.push("1 = 1");
        }


        return cql_filter.join(' and ');
    }
    var formatNumber = function(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }
    var cloneSelect = function(select, id, key, option) {
        select.find('option[selected]').removeAttr("selected");
        select.find('option[value=' + option + ']').attr("selected", "selected");
        return `<select id="${id}_${key}" class="form-control form-control-sm" style="display: none;">` + select.html() + `</select>`;
    }

    var ddlPrograma = function() {
        var tmp = [];
        var text = '';
        var data = filterArr('filterParams', 'key', 'ddlFuncion').selected;

        $('#ddlPrograma optgroup option').toggle(true);

        for (var i = 0; i < data.length; i++) {
            text = $('#ddlFuncion option[value="' + data[i] + '"]').text();
            tmp.push(
                '[label!="' + text + '"]'
            );
        }

        $('#ddlPrograma optgroup' + tmp.join('') + ' option').toggle(false);
        $('#ddlPrograma').selectpicker('refresh');
    }
    var ddlSubPrograma = function() {
        var tmp = [];
        var text = '';
        var data = filterArr('filterParams', 'key', 'ddlPrograma').selected;

        $('#ddlSubPrograma optgroup option').toggle(true);

        for (var i = 0; i < data.length; i++) {
            text = $('#ddlPrograma option[value="' + data[i] + '"]').text();
            tmp.push(
                '[label!="' + text + '"]'
            );
        }

        $('#ddlSubPrograma optgroup' + tmp.join('') + ' option').toggle(false);
        $('#ddlSubPrograma').selectpicker('refresh');
    }
    var dropdownlist = function(obj) {
        var lst = [];
        var obj;
        var row;

        obj.forEach(function(row) {
            obj = filterArr('filterParams', 'key', row.field);
            lst = $.map($('#' + row.field).attr('data-val').split(","), function(value) { return parseInt(value); });

            obj.data = lst;

            if (row.selected) {
                obj.selected = lst;
            }
        });
    }
    return {
        init: function() {
            return init();
        },
        createCluster: function(fase) {
            return createCluster(fase);
        },
        events: function(params) {
            return events(params);
        },
        format: function(type, value) {
            return format(type, value);
        },
        formatNumber: function(num) {
            return formatNumber(num);
        },
        filterArr: function(arr, key, value, nRows = 1) {
            return filterArr(arr, key, value, nRows = 1);
        },
    }
}();