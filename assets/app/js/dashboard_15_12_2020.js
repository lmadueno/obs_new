var dualChart = {
    chart: {
        zoomType: 'xy'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: ''
    },
    xAxis: [{
        categories: [],
        crosshair: true
    }],
    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }, { // Secondary yAxis
        title: {
            text: '',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[0]
            }
        },
        opposite: true
    }],
    tooltip: {
        shared: true
    },
    series: []
}
var combinateChart = {
    title: {
        text: ''
    },
    credits: {
        enabled: false
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        allowDecimals: false
    },
    yAxis: {
        title: {
            text: 'Soles'
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        valuePrefix: "S/. ",
        valueDecimals: 2,
        shared: true
    },
    plotOptions: {
        series: {
            label: {
                connectorAllowed: false
            },
            pointStart: 0
        }
    },
    series: []
};
var direccionesTemporalruc;
var map111;
var owsrootUrl = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/ows';
var obj_indicador_empresa_2;
var App = function () {
    var init = function () {
        document.body.style.zoom = "80%";

    }
    var dashboarentidades_partidos = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.csn,
                obj.msn,
                obj.ccn,
                obj.mcn
            ]
        });
    }
    var dashBoardProyects_perfiles_grupoprograma = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(this.category);
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'Monto',
                data: obj.monto
            },
            {
                type: 'spline',
                name: 'Cantidad',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }
            ]
        });

    }
    var dashBoardProyects_perfiles_grupofuncion = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                var tiutlo = this.category;

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_subprograma', programa: this.category, perfil: obj.perfil, funcion: obj.funcion })), success: function (response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function (i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomsubprograma);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por division funcional  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_division_programa'
                                        }
                                        dashBoardProyects_perfiles_grupoprograma(obj);
                                        $("#chart_dash_proyectos_funcion_division_programa").css("display", "");
                                    }
                                });
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'Monto',
                data: obj.monto
            },
            {
                type: 'spline',
                name: 'Cantidad',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }
            ]
        });

    }
    var dashBoardProyects_perfiles_funcion = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                var tiutlo = this.category;
                                console.log(obj.obj_envion);
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_programa', funcion: this.category, perfil: obj.obj_envion })), success: function (response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function (i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomprogramaproyecto);
                                        });
                                        var obj1 = {
                                            titulo: 'Analisis por grupo funcional  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            perfil: obj.obj_envion,
                                            funcion: tiutlo,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_division'
                                        }
                                        dashBoardProyects_perfiles_grupofuncion(obj1);
                                        $("#chart_dash_proyectos_funcion_division").css("display", "");

                                    }
                                });
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'Monto',
                data: obj.monto
            },
            {
                type: 'spline',
                name: 'Cantidad',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }
            ]
        });

    }
    var dashBoardProyects_perfiles = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                var tiutlo = this.category;

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_funcion', perfil: this.category })), success: function (response) {
                                        var monto = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function (i, obj) {
                                            monto.push(parseInt(obj.monto));

                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomfuncionproyecto);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por funcion  ' + tiutlo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            monto: monto,
                                            obj_envion: tiutlo,
                                            cantidad: cantidad,
                                            div: 'chart_dash_proyectos_funcion_'
                                        }

                                        dashBoardProyects_perfiles_funcion(obj);
                                        $("#chart_dash_proyectos_funcion_").css("display", "");

                                    }
                                });
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'Monto',
                data: obj.monto
            },
            {
                type: 'spline',
                name: 'Cantidad',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'white'
                }
            }
            ]
        });

    }
    var dashBoardEmpresas_top = function (obj) {
        Highcharts.chart(obj.div, {
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: {
                labels: {
                    format: '{value:,.0f}'
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            },
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                alert(this.category)
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'OxI',
                data: obj.oxi
            }, {
                type: 'spline',
                name: 'Utilidad',
                data: obj.utilidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'black'
                }
            }
            ]
        });

    }
    var dashBoardEmpresas_top_tamaño_sector = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'OxI',
                data: obj.oxi
            },
            {
                type: 'column',
                name: 'Utilidad',
                data: obj.utilidad,
                color: '#A7454F',
            }, {
                type: 'spline',
                name: 'Cantidad',
                color: '#000000',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'black'
                }
            }
            ]
        });

    }
    var dashBoardEmpresas_top_tamaño = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: {
                categories: obj.categories
            },
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Millones de Soles',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            labels: {
                items: [{
                    html: '',
                    style: {
                        left: '50px',
                        top: '18px',
                        color: ( // theme
                            Highcharts.defaultOptions.title.style &&
                            Highcharts.defaultOptions.title.style.color
                        ) || 'black'
                    }
                }]
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                var tamanio;
                                var titulo = this.category;
                                console.log(this.category);
                                if (this.category == 'Gran empresa') {
                                    tamanio = 2;
                                } else if (this.category == 'Mediana empresa') {
                                    tamanio = 3;
                                } else {
                                    tamanio = 4;
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2_sector', tamanio: tamanio })), success: function (response) {
                                        var oxi = [];
                                        var utilidad = [];
                                        var categories = [];
                                        var cantidad = [];
                                        $.each(response.data, function (i, obj) {
                                            oxi.push(parseInt(obj.oxi));
                                            utilidad.push(parseInt(obj.utilidad));
                                            cantidad.push(parseInt(obj.cantidad));
                                            categories.push(obj.nomsector);
                                        });
                                        var obj = {
                                            titulo: 'Analisis por sectores de ' + titulo,
                                            subtitulo: 'millones de soles',
                                            categories: categories,
                                            oxi: oxi,
                                            utilidad: utilidad,
                                            cantidad: cantidad,
                                            div: 'chart_dash_indicador2_'
                                        }
                                        dashBoardEmpresas_top_tamaño_sector(obj);
                                    }
                                });
                                $("#chart_dash_indicador2_").css("display", "");
                            }
                        }
                    }
                }

            }
            ,
            series: [{
                type: 'column',
                name: 'OxI',
                data: obj.oxi
            },
            {
                type: 'column',
                name: 'Utilidad',
                color: '#A7454F',
                data: obj.utilidad
            }, {
                type: 'spline',
                name: 'Cantidad',
                color: '#000000',
                yAxis: 1,
                data: obj.cantidad,
                marker: {
                    lineWidth: 2,
                    lineColor: Highcharts.getOptions().colors[3],
                    fillColor: 'black'
                }
            }
            ]
        });

    }
    var dashboarProyectos_congreso = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                $("#chart_mef_empresas_1").css("display", "");

                                var obj = {
                                    method: 'macro_mef_sub',
                                    entidad: this.category
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                        var cantidad = [];
                                        var resto = [];
                                        var total = [];
                                        var categories = [];
                                        $.each(response.data, function (i, obj) {
                                            cantidad.push(parseInt(obj.cantidad));
                                            resto.push(parseInt(obj.resto));
                                            total.push(parseInt(obj.total));
                                            categories.push(obj.nivel_g);
                                        });
                                        var obj_cantidad = {
                                            name: 'Cantidad',
                                            type: 'spline',
                                            color: '#000000',
                                            yAxis: 1,
                                            data: cantidad,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_monto = {
                                            name: 'Costo Total',
                                            type: 'column',
                                            data: total,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_resto = {
                                            name: 'Avance Ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            data: resto,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var titulo = response.data[0].region_r;
                                        var obj = {
                                            titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 por Región ' + titulo,
                                            subtitulo: '',
                                            categories: categories,
                                            cantidad: obj_cantidad,
                                            monto: obj_monto,
                                            monto_usado: obj_resto,
                                            div: 'chart_mef_empresas_1'
                                        }

                                        dashboarProyectos_congreso_sub(obj, response.data[0].region_r);
                                    }
                                });

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var dashboarProyectos_congreso_sub = function (obj, entidad) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value:,.0f}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                $("#chart_mef_empresas_2").css("display", "");
                                var obj = {
                                    method: 'macro_mef_sub_entidad',
                                    entidad: entidad,
                                    gobierno: this.category
                                }
                                var titulo = this.category;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                        var cantidad = [];
                                        var resto = [];
                                        var total = [];
                                        var categories = [];
                                        $.each(response.data, function (i, obj) {
                                            cantidad.push(parseInt(obj.cantidad));
                                            resto.push(parseInt(obj.resto));
                                            total.push(parseInt(obj.total));
                                            categories.push(obj.entidad);
                                        });
                                        var obj_cantidad = {
                                            name: 'Cantidad',
                                            type: 'spline',
                                            color: '#000000',
                                            yAxis: 1,
                                            data: cantidad,
                                            tooltip: {
                                                valueSuffix: ''
                                            },

                                        }
                                        var obj_monto = {
                                            name: 'Costo Total',
                                            type: 'column',
                                            data: total,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }
                                        var obj_resto = {
                                            name: 'Avance Ejecucion',
                                            type: 'column',
                                            color: '#A7454F',
                                            data: resto,
                                            tooltip: {
                                                valueSuffix: ''
                                            }

                                        }

                                        var obj = {
                                            titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 en  ' + entidad + ' por ' + titulo,
                                            subtitulo: '',
                                            categories: categories,
                                            cantidad: obj_cantidad,
                                            monto: obj_monto,
                                            monto_usado: obj_resto,
                                            div: 'chart_mef_empresas_2'
                                        }

                                        dashboarProyectos_congreso_sub_2(obj);
                                    }
                                });

                            }
                        }
                    }
                },
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var dashboarProyectos_congreso_sub_2 = function (obj) {
        Highcharts.chart(obj.div, {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: obj.titulo
            },
            subtitle: {
                text: obj.subtitulo
            },
            xAxis: [{
                categories: obj.categories,
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: '',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value}',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },

            series: [
                obj.cantidad,
                obj.monto,
                obj.monto_usado
            ]
        });
    }
    var wordcloud_Function = function (obj1) {
        Highcharts.chart(obj1.div, {

            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(this.name);
                                var obj = {
                                    method: 'macro_mef_Funcion_programa',
                                    consulta: this.name,
                                }
                                var tmp = [];
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                        $.each(response.data, function (i, obj) {
                                            tmp.push({ name: obj.nomprogramaproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_Function_2({ div: 'chart_mef_empresas_1_funcion', data: tmp });
                                    }
                                });
                                var obj = {
                                    method: 'macro_mef_Funcion_subprograma',
                                    consulta: this.name,
                                }
                                var tmp1 = [];
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                        $.each(response.data, function (i, obj) {
                                            tmp1.push({ name: obj.nomsubprograma, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_Function_2({ div: 'chart_mef_empresas_2_funcion', data: tmp1 });
                                    }
                                });
                                $("#chart_mef_empresas_1_funcion").css("display", "");
                                $("#chart_mef_empresas_2_funcion").css("display", "");
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_Function_2 = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_tamnio_ind = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(this.name);
                                if (this.name == 'GRAN EMPRESA') {
                                    tamanio = 2;
                                } else if (this.name == 'MEDIANA EMPRESA') {
                                    tamanio = 3;
                                } else {
                                    tamanio = 4;
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2_sector', tamanio: tamanio })), success: function (response) {
                                        var tmp = [];
                                        $.each(response.data, function (i, obj) {
                                            tmp.push({ name: obj.nomsector, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_tamnio_ind_sector({ div: 'chart_dash_indicador2_', data: tmp });
                                    }
                                });
                                $("#chart_dash_indicador2_").css("display", "");
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_tamnio_ind_sector = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(this.name);
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    //-----------------------------------------------
    var wordcloud_perfiles_ = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {

                                var tiutlo = this.name;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_funcion', perfil: this.name })), success: function (response) {
                                        var tmp = [];
                                        $.each(response.data, function (i, obj) {
                                            tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion({ div: 'chart_dash_proyectos_funcion_', data: tmp, obj_envion: tiutlo });
                                        $("#chart_dash_proyectos_funcion_").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(obj1);
                                var titulo = this.name;
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_programa', funcion: this.name, perfil: obj1.obj_envion })), success: function (response) {
                                        var tmp = [];
                                        $.each(response.data, function (i, obj) {
                                            tmp.push({ name: obj.nomprogramaproyecto, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion_programa({
                                            div: 'chart_dash_proyectos_funcion_division', data: tmp, perfil: obj1.obj_envion,
                                            funcion: titulo
                                        });
                                        $("#chart_dash_proyectos_funcion_division").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion_programa = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                console.log(obj1);

                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind_subprograma', programa: this.name, perfil: obj1.perfil, funcion: obj1.funcion })), success: function (response) {
                                        var tmp = [];
                                        $.each(response.data, function (i, obj) {
                                            tmp.push({ name: obj.nomsubprograma, weight: parseInt(obj.cantidad) });
                                        });
                                        wordcloud_ind_funcion_subprograma({ div: 'chart_dash_proyectos_funcion_division_programa', data: tmp });
                                        $("#chart_dash_proyectos_funcion_division_programa").css("display", "");
                                    }
                                });

                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_ind_funcion_subprograma = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
    var wordcloud_mdl_partidos = function (obj1) {
        Highcharts.chart(obj1.div, {
            plotOptions: {
                series: {
                    turboThreshold: 5000,
                    cursor: 'pointer',
                    point: {
                        events: {

                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                    orientations: 5
                },
                type: 'wordcloud',
                data: obj1.data,
                name: 'wdsfg'
            }],
            title: {
                text: ''
            },
            tooltip: {
                pointFormatter: function () {
                    var point = this;
                    return '<span style="color:blue">\u25CF</span><b> Cantidad Proyectos : </b>' + point.weight + '<br/><span style="color:red">\u25CF</span>';
                }
            }
        });
    }
	var changeTabPanelEstadistica = function (params) {
        $('#empresa').removeClass('active');
        $('#proyectos').removeClass('active');
        $('#entidades').removeClass('active');
        $('#salud').removeClass('active');
        var elemento = document.getElementById(params);
        elemento.className += " active";
    }
    var events = function (params) {
        switch (params.id) {
            case 'principal':
                var htmTable1 = ` <div class="row">
                <div class="col-lg-3">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Empresas</h4> 
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="empresa_dash"  href="#"  onclick="App.events(this);"> <img src="../assets/app/img/iconos-Empresas.png" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Empresas
                                        privadas con y sin
                                        participación el mecanismo
                                        OxI, registrados en
                                        PROINVERSIÓN y Ranking de
                                        empresas en el Perú.
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Proyectos</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12" >
                                    <a id="proyectos_dash"  href="#"  onclick="App.events(this);"><img src="../assets/app/img/iconos-Proyectos.png" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Perfiles de
                                        proyectos de inversión por
                                        función, división funciona y
                                        grupo funcional, sector, área
                                        geográfica de intervención y
                                        proyectos priorizados,
                                        registrados en INVIERTE PE.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                             <h4 class="m-b-0 text-white text-center">Entidades</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="entidades_dash"  href="#"  onclick="App.events(this);"><img src="../assets/app/img/iconos-Entidades.png" width="165" alt="" loading="lazy"></i></a>
                                </div>
                                <div class="col-lg-12"  style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Entidades por
                                        niveles de gobierno con
                                        proyectos de inversión con
                                        perfiles viables con y sin
                                        ejecución, registrados en el
                                        MEF.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                             <h4 class="m-b-0 text-white text-center">Salud</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id=""  href="#"  onclick="App.events(this);"><img src="../assets/app/img/iconos-Salud.png" width="125" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores del sector
                                        Salud, impacto del
                                        COVID19 por niveles de
                                        gobierno, según registro
                                        MINSA y SINADEF.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`;
                $('#content_dashboard').html(htmTable1);
                break;
            
            case 'empresa_dash':
				 changeTabPanelEstadistica("empresa");
                $.post({
                    url: 'empresas_dashboard.php', success: function (response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = {
                            method: 'macro_top'
                        }
                        var oxi = [];
                        var utilidad = [];
                        var categories = [];
                        var categories1 = [];
                        var oxi1 = [];
                        var utilidad1 = [];

                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                var htmTable1 = `<table class="table table-sm table-detail">
												<tr>
													<th style="background:#ddebf8;text-align:center"><b>#</b></th>
													<th style="background:#ddebf8;text-align:center"><b>RUC</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Razon Social</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Fuente</b></th>
                                                </tr>`;
                                var htmTable2 = `<table class="table table-sm table-detail">
												<tr>
                                                    <th style="background:#ddebf8;text-align:center"><b>#</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>RUC</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Razon Social</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                                    <th style="background:#ddebf8;text-align:center"><b>Fuente</b></th>
                                                </tr>`;
                                var cont = 0;
                                var cont1 = 0;
                                var sumOxi = 0;
                                var sumUtilidad = 0;
                                var sumOxi1 = 0;
                                var sumUtilidad1 = 0;
                                $.each(response.data, function (i, obj) {
                                    if (obj.participa == 'Participa' && oxi.length < 25) {
                                        cont++;
                                        oxi.push(parseInt(obj.oxi));
                                        utilidad.push(parseInt(obj.utilidad));
                                        categories.push(obj.razon_social);
                                        sumOxi += parseInt(obj.oxi);
                                        sumUtilidad += parseInt(obj.utilidad);
                                        htmTable1 += '<tr>';
                                        htmTable1 += '<td style="text-align:center">' + cont + '</td>';
                                        htmTable1 += '<td style="text-align:center"><button class="btn btn-link lnkModalEmpresas" id="' + obj.ruc + '" onclick="App.events(this);" data-toggle="modal" data-target="#exampleModal">' + obj.ruc + '</button></td>';
                                        htmTable1 += '<td style="text-align:left">' + obj.razon_social + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                        htmTable1 += '<td style="text-align:left">' + obj.fuente + '</td>';
                                        htmTable1 += '</tr>';

                                    } else if (obj.participa == 'No Participa' && oxi1.length < 25) {
                                        cont1++;
                                        oxi1.push(parseInt(obj.oxi));
                                        utilidad1.push(parseInt(obj.utilidad));
                                        categories1.push(obj.razon_social);
                                        sumOxi1 += parseInt(obj.oxi);
                                        sumUtilidad1 += parseInt(obj.utilidad);
                                        htmTable2 += '<tr>';
                                        htmTable2 += '<td style="text-align:center">' + cont1 + '</td>';
                                        htmTable2 += '<td style="text-align:center"><button class="btn btn-link lnkModalEmpresas" id="' + obj.ruc + '" onclick="App.events(this);" data-toggle="modal" data-target="#exampleModal">' + obj.ruc + '</button></td>';
                                        htmTable2 += '<td style="text-align:left">' + obj.razon_social + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                        htmTable2 += '<td style="text-align:left">' + obj.fuente + '</td>';
                                        htmTable2 += '</tr>';
                                    }
                                });
                                htmTable1 += '<tr>';
                                htmTable1 += '<td style="text-align:center">Total</td>';
                                htmTable1 += '<td style="text-align:center"></td>';
                                htmTable1 += '<td style="text-align:left"></td>';
								     htmTable1 += '<td style="text-align:center">' + sumUtilidad.toLocaleString('es-MX') + '</td>';
                                htmTable1 += '<td style="text-align:center">' + sumOxi.toLocaleString('es-MX') + '</td>';
                           
                                htmTable1 += '<td style="text-align:left"></td>';
                                htmTable1 += '</tr>';
                                htmTable1 += `
                                            <tr>
                                                <td colspan="6"> 
                                                    <table class="table table-sm table-detail">
                                                    <tr>
                                                        <th style="background:#ddebf8;text-align:center">*</th>
                                                        <th style="text-align:left">Data calculada a partir de los ingresos (*0.3 dividido entre 2) <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="background:#ddebf8;text-align:center">**</th>
                                                        <th style="text-align:left">Data mostrada por el <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                                    </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>`;
                                $('#html_empresas_top_25').html(htmTable1);
                                htmTable2 += '<tr>';
                                htmTable2 += '<td style="text-align:center">Total</td>';
                                htmTable2 += '<td style="text-align:center"></td>';
                                htmTable2 += '<td style="text-align:left"></td>';
								  htmTable2 += '<td style="text-align:center">' + sumUtilidad1.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:center">' + sumOxi1.toLocaleString('es-MX') + '</td>';
                              
                                htmTable2 += '<td style="text-align:left"></td>';
                                htmTable2 += '</tr>';
                                htmTable2 += `
                                <tr>
                                    <td colspan="6"> 
                                        <table class="table table-sm ">
                                        <tr>
                                            <th style="background:#ddebf8;text-align:center">*</th>
                                            <th style="text-align:left">Data calculada a partir de los ingresos (*0.3 dividido entre 2) <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                        </tr>
                                        <tr>
                                            <th style="background:#ddebf8;text-align:center">**</th>
                                            <th style="text-align:left">Data mostrada por el <a href="https://ptp.pe/" target="_blank" >TP10000</a></th>
                                        </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>`;
                                $('#html_empresas_top_25_no').html(htmTable2);
                                var obj = {
                                    titulo: 'Top empresas con mayor utilidad al 2019',
                                    subtitulo: 'Con participacion en Oxi',
                                    categories: categories,
                                    oxi: oxi,
                                    utilidad: utilidad,
                                    div: 'empresas_top_25'
                                }

                                dashBoardEmpresas_top(obj);
                                var obj = {
                                    titulo: 'Top empresas con mayor utilidad al 2019',
                                    subtitulo: 'Sin participacion en Oxi',
                                    categories: categories1,
                                    oxi: oxi1,
                                    utilidad: utilidad1,
                                    div: 'empresas_top_25_no'
                                }

                                dashBoardEmpresas_top(obj);

                            }
                        });

                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2' })), success: function (response) {
                                var cont = 0;
                                var sumOxi = 0;
                                var sumUtilidad = 0;
                                var sumcantidad = 0;
                                var oxi = [];
                                var utilidad = [];
                                var cantidad = [];
                                var categories = [];
                                var htmTable2 = `<table class="table table-sm table-detail">
                                <tr>
                                    <th style="background:#ddebf8;text-align:center"><b>#</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Tamaño</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Cantidad</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Utilidad</b></th>
                                    <th style="background:#ddebf8;text-align:center"><b>Disponibilidad OxI</b></th>
                                </tr>`;
                                $.each(response.data, function (i, obj) {
                                    cont++;
                                    oxi.push(parseInt(obj.oxi));
                                    utilidad.push(parseInt(obj.utilidad));
                                    categories.push(obj.nomtamanio);
                                    cantidad.push(parseInt(obj.cantidad));
                                    sumOxi += parseInt(obj.oxi);
                                    sumUtilidad += parseInt(obj.utilidad);
                                    sumcantidad += parseInt(obj.cantidad);
                                    htmTable2 += '<tr>';
                                    htmTable2 += '<td style="text-align:center">' + cont + '</td>';
                                    htmTable2 += '<td style="text-align:center"><button class="btn btn-link lnlAmpliarTamanio" id="' + obj.nomtamanio + '" data-event="lnkProvXrutas_' + obj.nomtamanio + '" onclick="App.events(this);">' + obj.nomtamanio + '</button></td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.cantidad).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.utilidad).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '<td style="text-align:center">' + parseInt(obj.oxi).toLocaleString('es-MX') + '</td>';
                                    htmTable2 += '</tr>';
                                    htmTable2 += '<tr  data-target="lnkProvXrutas_' + obj.nomtamanio + '" style="display: none;">';
                                    htmTable2 += `<td colspan="8">
                                        <div class="card">
                                            <div class="card-header">
                                                Sectores
                                            </div>
                                            <div class="card-body">`;
                                    htmTable2 += '<div id="div_' + obj.nomtamanio + '"></div>';
                                    htmTable2 += `</div>
                                        </div>
                                    </td>
                                    </tr>`;

                                });
                                htmTable2 += '<tr>';
                                htmTable2 += '<td style="text-align:center">Total</td>';
                                htmTable2 += '<td style="text-align:center"></td>';
                                htmTable2 += '<td style="text-align:center">' + sumcantidad.toLocaleString('es-MX') + '</td>';
								    htmTable2 += '<td style="text-align:center">' + sumUtilidad.toLocaleString('es-MX') + '</td>';
                                htmTable2 += '<td style="text-align:center">' + sumOxi.toLocaleString('es-MX') + '</td>';
                            
                                htmTable2 += '</tr>';
                                htmTable2 += `</table>`;
                                console.log(sumcantidad);
                                $('#html_dash_indicador2').html(htmTable2);

                                var obj = {
                                    titulo: 'Disponibilidad Oxi y cantidad por tamaño de empresa',
                                    subtitulo: '',
                                    categories: categories,
                                    oxi: oxi,
                                    cantidad: cantidad,
                                    utilidad: utilidad,
                                    div: 'chart_dash_indicador2'
                                }

                                dashBoardEmpresas_top_tamaño(obj);
                            }
                        });

                    }
                });
                break;
            case 'proyectos_dash':
				 changeTabPanelEstadistica("proyectos");
                $.post({
                    url: 'proyectos_dashboard.php', success: function (response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = {
                            method: 'macro_mef',
                            tipo: 'region',
                            forma: 'asc'
                        }
                        var cantidad = [];
                        var resto = [];
                        var total = [];
                        var categories = [];
                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                $.each(response.data, function (i, obj) {
                                    cantidad.push(parseInt(obj.cantidad));
                                    resto.push(parseInt(obj.resto));
                                    total.push(parseInt(obj.total));
                                    categories.push(obj.region);

                                });
                                var obj_cantidad = {
                                    name: 'Cantidad',
                                    type: 'spline',
                                    color: '#000000',
                                    yAxis: 1,
                                    data: cantidad,
                                    tooltip: {
                                        valueSuffix: ''
                                    },

                                }
                                var obj_monto = {
                                    name: 'Costo Total',
                                    type: 'column',

                                    data: total,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj_resto = {
                                    name: 'Avance Ejecucion',

                                    color: '#A7454F',
                                    type: 'column',
                                    data: resto,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj = {
                                    titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 por Región',
                                    subtitulo: '',
                                    categories: categories,
                                    cantidad: obj_cantidad,
                                    monto: obj_monto,
                                    monto_usado: obj_resto,
                                    div: 'chart_mef_empresas'
                                }
                                dashboarProyectos_congreso(obj);
                            }
                        });
                        var obj = {
                            tipo: 'region',
                            forma: 'asc'
                        }
                        $.post({
                            url: '../divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                document.getElementById('html_mef_empresas').innerHTML = response;
                            }
                        });
                        var obj = {
                            method: 'macro_mef_Funcion',
                            tipo: 'funcion',
                            forma: 'asc'
                        }
                        var tmp = [];
                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                $.each(response.data, function (i, obj) {
                                    cantidad.push(parseInt(obj.cantidad));
                                    categories.push(obj.funcion_);
                                    tmp.push({ name: obj.funcion_, weight: parseInt(obj.cantidad) });
                                });
                                wordcloud_Function({ div: 'chart_mef_empresas_funcion', data: tmp });
                            }
                        });
                        var obj = {
                            tipo: 'funcion',
                            forma: 'asc'
                        }
                        $.post({
                            url: '../divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                document.getElementById('html_mef_empresas_2').innerHTML = response;
                            }
                        });
                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind' })), success: function (response) {
                                var cantidad = [];
                                var monto = [];
                                var categories = [];
                                $.each(response.data, function (i, obj) {
                                    cantidad.push(parseInt(obj.cantidad));
                                    monto.push(parseInt(obj.monto));
                                    categories.push(obj.nomfase);
                                });
                                var obj = {
                                    titulo: 'Grafico de Analisis de perfiles',
                                    subtitulo: '',
                                    categories: categories,
                                    monto: monto,
                                    cantidad: cantidad,
                                    div: 'chart_dash_proyectos_funcion'
                                }
                                dashBoardProyects_perfiles(obj);
                            }
                        });
                    }
                });

                break;
            case 'entidades_dash':
               changeTabPanelEstadistica('entidades');
			   $.post({
                    url: 'entidades_dashboard.php', success: function (response) {
                        document.getElementById('content_dashboard').innerHTML = response;
                        var obj = {
                            method: 'pi_partidospoliticos'
                        }
                        $.getJSON({
                            url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                var categories = [];
                                var csn = [];
                                var msn = [];
                                var ccn = [];
                                var mcn = [];
                                $.each(response.data, function (i, obj) {
                                    csn.push(parseInt(obj.cantidad_pro_s));
                                    msn.push(parseInt(obj.monto_pro_s));
                                    ccn.push(parseInt(obj.cantidad_pro));
                                    mcn.push(parseInt(obj.monto_pro));
                                    categories.push(obj.nomnivelgobierno);
                                });
                                var obj_csn = {
                                    name: 'Cantidad PI viables sin ejecucion',
                                    type: 'spline',
                                    color: '#0070c0',
                                    yAxis: 1,
                                    data: csn,
                                    tooltip: {
                                        valueSuffix: ''
                                    },

                                }
                                var obj_msn = {
                                    name: 'Monto PI viables sin ejecucion',
                                    type: 'column',
                                    color: '#A7454F',
                                    yAxis: 0,
                                    data: msn,
                                    tooltip: {
                                        valueSuffix: ''
                                    },

                                }
                                var obj_ccn = {
                                    name: 'Cantidad PI viables con ejecucion',
                                    type: 'spline',
                                    color: '#92d050',
                                    yAxis: 1,
                                    data: ccn,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj_mcn = {
                                    name: 'Monto PI viables con ejecucion',
                                    type: 'column',
                                    yAxis: 0,
                                    color: '#50497a',
                                    data: mcn,
                                    tooltip: {
                                        valueSuffix: ''
                                    }

                                }
                                var obj = {
                                    titulo: 'Grafico de Proyectos de Inversion(PI) cantidad y monto',
                                    subtitulo: '',
                                    categories: categories,
                                    csn: obj_csn,
                                    msn: obj_msn,
                                    ccn: obj_ccn,
                                    mcn: obj_mcn,
                                    div: 'chart_mef_entidades'
                                }
                                dashboarentidades_partidos(obj)

                            }
                        });
                    }
                });
                break;
        }
        if (params.classList.contains('lnkModalEntidades')) {
            var id = params.getAttribute('id');
            var obj = {
                method: 'fichaEntidadAmpliar',
                tipo: id.length,
                ubigeo: id
            }
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    var envio = response.data[0];
                    $.get({
                        url: 'popupFicha.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                            envio.method = 'ciprlXregion';
                            $('#modal__').html(response);
                            $(".estilodashboard").css("background", "#1976d2");
                            $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                            if (envio.idnivel == 1 || envio.idnivel == 2 || envio.idnivel == 3) {
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                                        console.log(response.data);
                                        if (Object.keys(response).length > 0) {
                                            var chart = combinateChart;
                                            chart.series = response.data.series;
                                            chart.plotOptions.series.pointStart = response.data.pointStart;
                                            console.log(chart);
                                            Highcharts.chart('chart_' + envio.codubigeo, chart);
                                        }
                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_sinadef',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)), success: function (response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos SINADEF por región';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartSinadefEntidad', Chart1);

                                    }
                                });
                                var objQ = {
                                    method: 'quintiles_covid',
                                    idnivel: envio.idnivel
                                }
                                $.getJSON({
                                    url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(objQ)), success: function (response1) {
                                        var categorias = [];
                                        var obj2017 = {
                                            name: 'Entidades',
                                            type: 'column',
                                            yAxis: 1,
                                            color: '#4572A7',
                                            data: []
                                        };
                                        var obj2017_Pob = {
                                            name: 'Fallecidos',
                                            type: 'spline',
                                            color: '#A7454F',
                                            data: []
                                        };

                                        for (i = 0; i < response1.data.length; i++) {
                                            categorias.push(response1.data[i].texto);
                                            obj2017.data.push(parseInt(response1.data[i].entidades));
                                            obj2017_Pob.data.push(parseInt(response1.data[i].fallecidos));

                                        }
                                        var total = [obj2017, obj2017_Pob];

                                        var Chart1 = dualChart;
                                        Chart1.xAxis[0].categories = categorias;
                                        Chart1.title.text = 'Fallecidos COVID-19';

                                        Chart1.series = total;
                                        Chart1.subtitle.text = '(fallecidos/pob.*1000)';
                                        Highcharts.chart('chartCovidEntidad', Chart1);

                                    }
                                });
                            }
                        }
                    });
                }
            });

        }
        if (params.classList.contains('lnkModalEmpresas')) {
            var id = params.getAttribute('id');

            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ ruc: id, method: 'modal_Companies' })), success: function (response) {
                    var obj = response.data[0];
                    var empresa = response.data[0].empresa;
                    $.get({
                        url: '../popupCompanies.php?obj=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                            $('#modal__').html(response);
                            $(".estilodashboard").css("background", "#1976d2");
                            $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                            obj.method = 'empconceptos';
                            $.getJSON({
                                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                                    console.log(response);
                                    if (Object.keys(response).length > 0) {
                                        chartCompanies(obj.ruc, response.data);

                                    }
                                }
                            });
                            var obj1 = {

                                empresa: empresa
                            };
                            $.post({
                                url: '../popupCompaniesTabla.php?data=' + encodeURIComponent(JSON.stringify(obj1)), success: function (response) {
                                    console.log(response);
                                    document.getElementById('divTablaproyectosComnpanies').innerHTML = response;
                                }
                            });
                            var url = "http://5.9.118.78:4000/getRucSunat";
                            axios.post(url, { nruc: id })
                                .then(function (response) {
                                    console.log(response);
                                    $("#contribuyente_condicion").val(response.data.result.condicion);
                                    $("#actividad_exterior").val(response.data.result.actividad_exterior);
                                    $("#emision_electronica").val(response.data.result.emision_electronica);
                                    $("#estado").val(response.data.result.estado);
                                    $("#fecha_inscripcion").val(response.data.result.fecha_inscripcion);
                                    $("#inicio_actividades").val(response.data.result.inicio_actividades);
                                    $("#nombre_comercial").val(response.data.result.nombre_comercial);
                                    $("#razon_social").val(response.data.result.razon_social);
                                    $("#sistema_contabilidad").val(response.data.result.sistema_contabilidad);
                                    $("#sistema_emision").val(response.data.result.sistema_emision);
                                    $("#tipo").val(response.data.result.tipo);
                                    $("#direccion").val(response.data.result.direccion);
                                    var htmTable = `<table class="table table-sm table-detail">
												<tr>
													<th style="background:#ddebf8"><b>CIU</b></th>
													<th style="background:#ddebf8"><b>descripcion</b></th>
												   
												   
												</tr>`;
                                    $.each(response.data.result.actividad_economica, function (i, obj) {
                                        htmTable += '<tr>';
                                        htmTable += '<td style="text-align:center;text-align:center">' + obj.ciiu + '</td>';
                                        htmTable += '<td style="text-align:center;text-align:center">' + obj.descripcion + '</td>';

                                        htmTable += '</tr>';
                                    });
                                    htmTable += `</table>`;
                                    $('#txtactividadEconomica').html(htmTable);
                                    var htmTable1 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8"><b>Nombre</b></th>
											<th style="background:#ddebf8"><b>Cargo</b></th>
											<th style="background:#ddebf8"><b>Desde</b></th>
										</tr>`;
                                    $.each(response.data.result.representantes_legales, function (i, obj) {

                                        htmTable1 += '<tr>';
                                        htmTable1 += '<td style="text-align:center">' + obj.nombre + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + obj.cargo + '</td>';
                                        htmTable1 += '<td style="text-align:center">' + obj.desde + '</td>';
                                        htmTable1 += '</tr>';
                                    });
                                    htmTable1 += `</table>`;
                                    $('#txtrepresentantes').html(htmTable1);
                                    var htmTable2 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Periodo</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Año</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Mes</b></th>
											<th style="background:#ddebf8;text-align:center"><b>Total de Trabajadores</b></th>
										</tr>`;
                                    $.each(response.data.result.cantidad_trabajadores, function (i, obj) {
                                        console.log(obj);
                                        htmTable2 += '<tr>';
                                        htmTable2 += '<td style="text-align:center">' + obj.periodo + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + obj.anio + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + getMes(parseInt(obj.mes)) + '</td>';
                                        htmTable2 += '<td style="text-align:center">' + obj.total_trabajadores + '</td>';
                                        htmTable2 += '</tr>';
                                    });
                                    htmTable2 += `</table>`;
                                    $('#txtTrabajadores').html(htmTable2);
                                    var htmTable3 = `<table class="table table-sm table-detail">
										<tr>
											<th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
										</tr>`;
                                    $.each(response.data.result.comprobante_electronico, function (i, obj) {

                                        htmTable3 += '<tr>';
                                        htmTable3 += '<td style="text-align:center">' + obj + '</td>';
                                        htmTable3 += '</tr>';
                                    });
                                    htmTable3 += `</table>`;
                                    $('#txtcomprobante_electronico').html(htmTable3);
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                            direccionesTemporalruc = id;
                            map111 = L.map('map111', { zoomControl: false }).setView([-9.33, -74.44], 5);
                            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {

                            }).addTo(map111);
                            new L.Control.Zoom({ position: 'topright' }).addTo(map111);
                            loadCompaniesPrincipal("ruc='" + id + "'");
                            var obj2 = {
                                method: 'empresas_expo_impo',
                                ruc: id
                            };

                            $.getJSON({
                                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj2)), success: function (response) {

                                    console.log(response.data[0]);
                                    var htmTable3 = `<table class="table table-sm table-detail">
                                    <tr>
                                        <th style="background:#ddebf8;text-align:center"><b>Tipo</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2019</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2018</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2017</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2016</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2015</b></th>
                                        <th style="background:#ddebf8;text-align:center"><b>2014</b></th>
                                    </tr>`;
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Importaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdimpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += '<tr>';
                                    htmTable3 += '<td style="text-align:center">Exportaciones</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2019).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2018).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2017).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2016).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2015).toLocaleString() + '</td>';
                                    htmTable3 += '<td style="text-align:center">' + parseInt(response.data[0].usdexpo2014).toLocaleString() + '</td>';
                                    htmTable3 += '</tr>';
                                    htmTable3 += `</table>`;
                                    $('#txtexportacionesTable').html(htmTable3);
                                    Highcharts.chart('txtexportaciones', {

                                        title: {
                                            text: 'Exportaciones e Importaciones 2014-2019'
                                        },

                                        subtitle: {
                                            text: '(millones de soles)'
                                        },

                                        yAxis: {
                                            title: {
                                                text: 'S/.'
                                            }
                                        },

                                        xAxis: {
                                            accessibility: {
                                                rangeDescription: 'Range: 2014-2019'
                                            }
                                        },

                                        legend: {
                                            layout: 'vertical',
                                            align: 'right',
                                            verticalAlign: 'middle'
                                        },

                                        plotOptions: {
                                            series: {
                                                label: {
                                                    connectorAllowed: false
                                                },
                                                pointStart: 2010
                                            }
                                        },

                                        series: [{
                                            name: 'Importacion',
                                            data: [parseInt(response.data[0].usdimpo2019), parseInt(response.data[0].usdimpo2018), parseInt(response.data[0].usdimpo2017), parseInt(response.data[0].usdimpo2016), parseInt(response.data[0].usdimpo2015), parseInt(response.data[0].usdimpo2014)]
                                        },
                                        {
                                            name: 'Exportacion',
                                            data: [parseInt(response.data[0].usdexpo2019), parseInt(response.data[0].usdexpo2018), parseInt(response.data[0].usdexpo2017), parseInt(response.data[0].usdexpo2016), parseInt(response.data[0].usdexpo2015), parseInt(response.data[0].usdexpo2014)]
                                        }
                                        ],

                                        responsive: {
                                            rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                },
                                                chartOptions: {
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'bottom'
                                                    }
                                                }
                                            }]
                                        }

                                    });
                                }
                            });
                        }
                    });



                }
            });
        }
        if (params.classList.contains('macroKeys')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: '../divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    document.getElementById('html_mef_empresas').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('macroKeysFuncion')) {
            var id = params.getAttribute('id');
            var array = id.split('_');
            var obj = {
                tipo: array[0],
                forma: array[1]
            }
            $.post({
                url: '../divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    document.getElementById('html_mef_empresas_2').innerHTML = response;

                }
            });

        }
        if (params.classList.contains('generar_Mef')) {
            $("#chart_mef_empresas_1").css("display", "none");
            $("#chart_mef_empresas_2").css("display", "none");
            var obj = {
                method: 'macro_mef',
                tipo: $("#ddl_empresas_mef_tipo").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma").find(":selected").attr("value")
            }
            var cantidad = [];
            var resto = [];
            var total = [];
            var categories = [];
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    $.each(response.data, function (i, obj) {
                        cantidad.push(parseInt(obj.cantidad));
                        resto.push(parseInt(obj.resto));
                        total.push(parseInt(obj.total));
                        categories.push(obj.region);
                    });
                    var obj_cantidad = {
                        name: 'Cantidad',
                        type: 'spline',
                        color: '#000000',
                        yAxis: 1,
                        data: cantidad,
                        tooltip: {
                            valueSuffix: ''
                        },

                    }
                    var obj_monto = {
                        name: 'Costo Total',
                        type: 'column',
                        data: total,
                        tooltip: {
                            valueSuffix: ''
                        }

                    }
                    var obj_resto = {
                        name: 'Avance Ejecucion',
                        type: 'column',
                        color: '#A7454F',
                        data: resto,
                        tooltip: {
                            valueSuffix: ''
                        }

                    }
                    var obj = {
                        titulo: 'Listado de Inversiones Solicitadas para su financiamiento 2021 por Región',
                        subtitulo: '',
                        categories: categories,
                        cantidad: obj_cantidad,
                        monto: obj_monto,
                        monto_usado: obj_resto,
                        div: 'chart_mef_empresas'
                    }

                    dashboarProyectos_congreso(obj);
                }
            });
            var obj = {
                tipo: $("#ddl_empresas_mef_tipo").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma").find(":selected").attr("value")
            }
            $.post({
                url: '../divresrmenyestadisticasMacro.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    document.getElementById('html_mef_empresas').innerHTML = response;
                }
            });

        }
        if (params.classList.contains('generar_Mef_funcion')) {
            $("#chart_mef_empresas_1_funcion").css("display", "none");
            $("#chart_mef_empresas_2_funcion").css("display", "none");
            var obj = {
                method: 'macro_mef_Funcion',
                tipo: $("#ddl_empresas_mef_tipo_funcion").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma_funcion").find(":selected").attr("value")
            }
            var tmp = [];
            var cantidad = [];
            var categories = [];
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {

                    $.each(response.data, function (i, obj) {
                        cantidad.push(parseInt(obj.cantidad));
                        categories.push(obj.funcion_);
                        tmp.push({ name: obj.funcion_, weight: parseInt(obj.cantidad) });
                    });
                    wordcloud_Function({ div: 'chart_mef_empresas_funcion', data: tmp });
                }
            });
            var obj = {
                tipo: $("#ddl_empresas_mef_tipo_funcion").find(":selected").attr("value"),
                forma: $("#ddl_empresas_mef_forma_funcion").find(":selected").attr("value")
            }
            $.post({
                url: '../divresrmenyestadisticasMacroFuncion.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                    document.getElementById('html_mef_empresas_2').innerHTML = response;
                }
            });

        }
        if (params.classList.contains('lnkAmpliar')) {
            var id = params.getAttribute('data-event');
            var target = $("[data-target='" + id + "']");

            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_empresa')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');


            var target = $("[data-target='" + id + "']");
            var envio = {
                region: id1
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_nivelGobierno.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_nivel')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                region: array[0],
                gobierno: array[1]
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_entidades.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_cui')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                region: array[0],
                gobierno: array[1],
                entidad: array[2]
            }
            $.get({
                url: '../divresrmenyestadisticasMacro_cui.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('closemodal')) {
            $('#exampleModal').modal('hide')
        }
        if (params.classList.contains('ddlGraficos_tamano')) {
            $("#chart_dash_indicador2_").css("display", "none");
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'indicador_dash_2' })), success: function (response) {
                    var cont = 0;
                    var sumOxi = 0;
                    var sumUtilidad = 0;
                    var oxi = [];
                    var utilidad = [];
                    var cantidad = [];
                    var categories = [];
                    var tmp = [];
                    $.each(response.data, function (i, obj) {
                        cont++;
                        oxi.push(parseInt(obj.oxi));
                        utilidad.push(parseInt(obj.utilidad));
                        categories.push(obj.nomtamanio);
                        cantidad.push(parseInt(obj.cantidad));
                        sumOxi += parseInt(obj.oxi);
                        sumUtilidad += parseInt(obj.utilidad);
                        tmp.push({ name: obj.nomtamanio, weight: parseInt(obj.cantidad) });
                    });
                    var obj = {
                        titulo: 'Titulo del Grafico',
                        subtitulo: 'millones de soles',
                        categories: categories,
                        oxi: oxi,
                        cantidad: cantidad,
                        utilidad: utilidad,
                        div: 'chart_dash_indicador2'
                    }


                    if ($("#ddlGraficos_tamano").find(":selected").attr("value") == 1) {
                        dashBoardEmpresas_top_tamaño(obj);
                    } else {
                        wordcloud_tamnio_ind({ div: 'chart_dash_indicador2', data: tmp });
                    }
                }
            });

        }
        if (params.classList.contains('lnlAmpliarTamanio')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var tamnio;
            if (id1 == 'Gran empresa') {
                tamnio = 2;
            } else if (id1 == 'Mediana empresa') {
                tamnio = 3;
            } else if (id1 == 'Pequeña empresa') {
                tamnio = 4;
            }else{
                tamnio = 1;
            }
            var target = $("[data-target='" + id + "']");
            var envio = {
                tamanio: tamnio
            }
            console.log(envio);
            $.get({
                url: '../divresrmenyestadisticasEmpresasTamnio.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpliar_mef_sector')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('$');

            var target = $("[data-target='" + id + "']");
            var envio = {
                tamanio: array[1],
                sector: array[0]
            }
            $.get({
                url: '../divresrmenyestadisticasEmpresasSector.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('ddlGraficos_funcion_dashP')) {
            $("#chart_dash_proyectos_funcion_").css("display", "none");
            $("#chart_dash_proyectos_funcion_division").css("display", "none");
            $("#chart_dash_proyectos_funcion_division_programa").css("display", "none");
            $.getJSON({
                url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify({ method: 'dashboard_perfiles_ind' })), success: function (response) {
                    var monto = [];
                    var cantidad = [];
                    var categories = [];
                    var tmp = [];
                    $.each(response.data, function (i, obj) {
                        monto.push(parseInt(obj.monto));
                        categories.push(obj.nomfase);
                        cantidad.push(parseInt(obj.cantidad));
                        tmp.push({ name: obj.nomfase, weight: parseInt(obj.cantidad) });
                    });
                    var obj = {
                        titulo: 'Grafico de Analisis de perfiles',
                        subtitulo: '',
                        categories: categories,
                        monto: monto,
                        cantidad: cantidad,
                        div: 'chart_dash_proyectos_funcion'
                    }


                    if ($("#ddlGraficos_funcion_dashP").find(":selected").attr("value") == 1) {
                        dashBoardProyects_perfiles(obj);
                    } else {
                        wordcloud_perfiles_({ div: 'chart_dash_proyectos_funcion', data: tmp });
                    }
                }
            });

        }
        if (params.classList.contains('lnkNivelGobiernoPartido')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var envio = 0;
            if (id1 == 'GOBIERNO REGIONAL') {
                envio = 1;
            } else if (id1 == 'GOBIERNO PROVINCIAL') {
                envio = 2;
            } else if (id1 == 'GOBIERNO DISTRITAL') {
                envio = 3;
            }

            var target = $("[data-target='" + id + "']");
            var envio = {
                envio: envio,
                texto: id1
            }
            $.get({
                url: '../divresrmenyestadisticasMacroDashboardEntidades.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnlEntidadPartido_sub')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var target = $("[data-target='" + id + "']");
            var envio = {
                nivel: params.getAttribute('id').split('-')[1],
                partido: params.getAttribute('id').split('-')[0]
            }

            $.get({
                url: '../divresrmenyestadisticasMacroDashboardEntidades_sub.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpluarProyectosFase')) {
            var id = params.getAttribute('id').split('-');
            $.get({
                url: 'modalentidades.php', success: function (response) {
                    $('#modal__').html(response);
                    $(".estilodashboard").css("background", "#1976d2");
                    $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                    var obj = {
                        method: 'modal_funcion_entidades',
                        ubigeo: id[0],
                        nivel: id[1]
                    }
                    $.getJSON({
                        url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                            var tmp = [];
                            $.each(response.data, function (i, obj) {
                                tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                            });
                            wordcloud_mdl_partidos({ div: 'ml_char1', data: tmp });
                        }
                    });
                    
                    var envio = {
                        ubigeo: id[0],
                        nivel: id[1],
                        tipo:'funcion'
                    }
                    $.get({
                        url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                            document.getElementById('ml_html1').innerHTML = response;
                        }
                    });
                }
            });
        }
        if (params.classList.contains('lnkprogramaProyectosModal')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                funcion: array[0],
                ubigeo: array[2],
                nivel:array[1],
                tipo:'programa'
            }
            console.log(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnksubprogramaProyectosModal')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                funcion: array[1],
                programa:array[0],
                ubigeo: array[3],
                nivel:array[2],
                tipo:'subprograma'
            }
            console.log(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkcui_entidades_perfil')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            var array = id1.split('-');

            var target = $("[data-target='" + id + "']");
            var envio = {
                funcion: array[2],
                programa:array[1],
                ubigeo: array[4],
                nivel:array[3],
                subprograma:array[0],
                tipo:'proyectos'
            }
            console.log(envio);
            $.get({
                url: 'modal_entidades_funcion.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
        if (params.classList.contains('lnkAmpluarProyectosperfil')) {
            var id = params.getAttribute('id').split('-');
            $.get({
                url: 'modalentidades.php', success: function (response) {
                    $('#modal__').html(response);
                    $(".estilodashboard").css("background", "#1976d2");
                    $('.estilodashboard1').html('<a href="#" class="closemodal text-white" id="monto_desc" onclick="App.events(this);" ><i class="fas fa-times"></i></a>');
                    var obj = {
                        method: 'modal_funcion_entidades_perfil',
                        ubigeo: id[0],
                        nivel: id[1]
                    }
                    $.getJSON({
                        url: '../php/app.php?data=' + encodeURIComponent(JSON.stringify(obj)), success: function (response) {
                            var tmp = [];
                            $.each(response.data, function (i, obj) {
                                tmp.push({ name: obj.nomfuncionproyecto, weight: parseInt(obj.cantidad) });
                            });
                            wordcloud_mdl_partidos({ div: 'ml_char1', data: tmp });
                        }
                    });
                }
            });
        }
		if (params.classList.contains('lnkAmpliar_totales')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');

            var target = $("[data-target='" + id + "']");
            var envio = {
                partido: id1
            }
            $.get({
                url: '../entidades_totales_partidos.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }
		 if (params.classList.contains('lnkAmpliar_totales_nivel')) {
            var id = params.getAttribute('data-event');
            var id1 = params.getAttribute('id');
            console.log(id);
            console.log(id1);
            var array = id1.split('$');
            var target = $("[data-target='" + id + "']");
            var envio = {
                partido: array[0],
                nivel: array[1]
            }
            $.get({
                url: '../entidades_totales_partidos_nivel.php?data=' + encodeURIComponent(JSON.stringify(envio)), success: function (response) {
                    document.getElementById('div_' + id1).innerHTML = response;
                }
            });
            if (target.length > 0) {
                if (target.is(':visible')) {
                    target.css('display', 'none');
                }
                else {
                    target.css('display', '');

                }
            }
        }

    }
	
    var chartCompanies = function (codempresa, result) {
        Highcharts.chart('chart_' + codempresa, {
            title: {
                text: 'Evoluci�n de Ingresos y Utilidades'
            },

            yAxis: {
                title: {
                    text: 'Soles'
                }
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: result.pointStart
                }
            },

            series: [{
                name: 'Ingresos',
                data: result.series.ingresos
            }, {
                name: 'Utilidades',
                data: result.series.utilidades
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    }
    var getMes = function (mes) {
        var mm;
        switch (mes) {
            case 1:
                mm = 'Enero';
                break;
            case 2:
                mm = 'Febrero';
                break;
            case 3:
                mm = 'Marzo';
                break;
            case 4:
                mm = 'Abril';
                break;
            case 5:
                mm = 'Mayo';
                break;
            case 6:
                mm = 'Junio';
                break;
            case 7:
                mm = 'Julio';
                break;
            case 8:
                mm = 'Agosto';
                break;
            case 9:
                mm = 'Setiembre';
                break;
            case 10:
                mm = 'Octubre';
                break;
            case 11:
                mm = 'Noviembre';
                break;
            case 12:
                mm = 'Diciembre';
                break;
        }
        return mm;
    }
    var defaultParameters = function (obj) {
        var params = {
            service: 'WFS',
            version: '1.0.0',
            request: 'GetFeature',
            typeName: 'colaboraccion_20:' + obj.layerName,
            outputFormat: 'application/json'
        };
        if (obj.hasOwnProperty('sql')) {
            if (obj.sql.length > 0) {
                params.cql_filter = obj.sql;
            }
        }
        return params;
    }
    var loadCompaniesPrincipal = function (cql_filter) {
        $.ajax({
            url: owsrootUrl + L.Util.getParamString(
                L.Util.extend(
                    defaultParameters({
                        layerName: 'vw_empresas',
                        sql: cql_filter
                    })
                )),
            dataType: 'json',
            success: function (data) {

                var markers = L.geoJson(data, {

                    pointToLayer: function (feature, latlng) {

                        var marker = L.marker(latlng, {
                            icon: L.ExtraMarkers.icon({
                                icon: 'fa-briefcase',
                                markerColor: 'black',
                                shape: 'square',
                                prefix: 'fas'
                            })
                        });

                        return marker;
                    },
                    onEachFeature: function (feature, layer) {
                        layer.addTo(map111);
                    }
                });

            }
        });


    }
    return {
        init: function () {
            return init();
        },

        events: function (params) {
            return events(params);
        }
    }
}();