var AppObsV2 = function() {


    const NIVEL_DE_GOBIERNO_REGIONAL = 1;
    const NIVEL_DE_GOBIERNO_PROVINCIAL = 2;
    const NIVEL_DE_GOBIERNO_DISTRITAL = 3;
    const API_REST_GEOSERVER = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=colaboraccion_2020';

    const layers = [
        {
            dataId: 'region',
            name: 'lyrRegion',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia',
            name: 'lyrProvincia',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito',
            name: 'lyrDistrito',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_minsa2',
            name: 'lyrRegionMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_minsa2',
            name: 'lyrProvinciaMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_minsa2',
            name: 'lyrDistritoMinsa2',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_sinadef2',
            name: 'lyrRegionSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_sinadef2',
            name: 'lyrProvinciaSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_sinadef2',
            name: 'lyrDistritoSinadef2',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_oxigeno',
            name: 'lyrRegionOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_oxigeno',
            name: 'lyrProvinciaOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_oxigeno',
            name: 'lyrDistritoOxigeno',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        },
        {
            dataId: 'region_vacunas',
            name: 'lyrRegionVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_REGIONAL
        },
        {
            dataId: 'provincia_vacunas',
            name: 'lyrProvinciaVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_PROVINCIAL
        },
        {
            dataId: 'distrito_vacunas',
            name: 'lyrDistritoVacunas',
            nivelGobierno: NIVEL_DE_GOBIERNO_DISTRITAL
        }
    ];
    let groupLayerHealthOxygen = new L.layerGroup();
	let groupLayerHealthVaccine = new L.layerGroup();
	let elementId = '';
    let infoRegion, infoProvince, infoDistrict, legendDates = [];

    let init = () => {		
		setDateLegend();
        getInfoUbigeoRegion();
        getInfoUbigeoProvince();
        getInfoUbigeoDistrict();

        document.addEventListener('click', function(e) {
			
			//Oxigeno
            var layersOxygenName = ["region_oxigeno", "provincia_oxigeno", "distrito_oxigeno"];
			
            if (e.target && layersOxygenName.includes(e.target.getAttribute("data-id"))) {
                const levelGovernment = layersOxygenName.indexOf(e.target.getAttribute("data-id"));

                switch (levelGovernment) {
                    case NIVEL_DE_GOBIERNO_REGIONAL - 1:
                        addInfoLayerToMap(groupLayerHealthOxygen, infoRegion, handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case NIVEL_DE_GOBIERNO_PROVINCIAL - 1:
                        addInfoLayerToMap(groupLayerHealthOxygen, infoProvince, handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case NIVEL_DE_GOBIERNO_DISTRITAL - 1:
                        addInfoLayerToMap(groupLayerHealthOxygen, infoDistrict, handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;
                }
				
				
				if(document.querySelector('.legendOx') != null) document.querySelector('.legendOx').remove();
				var legend = L.control({ position: "bottomleft" });
				legend.onAdd = function(map) {
					var div = L.DomUtil.create("div", "legendOx");
					div.innerHTML = setLegend('Mapa de Oxigeno requerido(m3/hora)', `Fecha de actualización ${findUpdateDate('oxigeno')}`);
					return div;
				};
				legend.addTo(map);
            }
			
			//Vacunas			
			var layersVaccineName = ["region_vacunas", "provincia_vacunas", "distrito_vacunas"];
			
			if (e.target && layersVaccineName.includes(e.target.getAttribute("data-id"))) {
                const levelGovernment = layersVaccineName.indexOf(e.target.getAttribute("data-id"));
				
                switch (levelGovernment) {
                    case NIVEL_DE_GOBIERNO_REGIONAL - 1:
                        addInfoLayerToMap(groupLayerHealthVaccine, infoRegion, handleClickLayerRegion, NIVEL_DE_GOBIERNO_REGIONAL);
                        break;
                    case NIVEL_DE_GOBIERNO_PROVINCIAL - 1:
                        addInfoLayerToMap(groupLayerHealthVaccine, infoProvince, handleClickLayerRegion, NIVEL_DE_GOBIERNO_PROVINCIAL);
                        break;
                    case NIVEL_DE_GOBIERNO_DISTRITAL - 1:
                        addInfoLayerToMap(groupLayerHealthVaccine, infoDistrict, handleClickLayerRegion, NIVEL_DE_GOBIERNO_DISTRITAL);
                        break;
                }
				
				if(document.querySelector('.legendVac') != null) document.querySelector('.legendVac').remove();
				var legend = L.control({ position: "bottomleft" });
				legend.onAdd = function(map) {
					var div = L.DomUtil.create("div", "legendVac");
					div.innerHTML = setLegend('Mapa de Vacunas(Dosis 1 y 2/población)', `Fecha de actualización ${findUpdateDate('vacunas')}`);
					return div;
				};
				legend.addTo(map);
            }
			
			//Leyenda Oxigeno
            if (e.target && e.target.getAttribute("data-id") == 'infoOxigeno') {
                if (e.target.checked) {
                    groupLayerHealthOxygen.addTo(map);
                } else {
                    groupLayerHealthOxygen.removeLayer(map);
					map.removeLayer(groupLayerHealthOxygen);			
                }
            }
			
			//Leyenda Vacunas
            if (e.target && e.target.getAttribute("data-id") == 'infoVacunas') {
                if (e.target.checked) {
                    groupLayerHealthVaccine.addTo(map);
                } else {
                    groupLayerHealthVaccine.removeLayer(map);
					map.removeLayer(groupLayerHealthVaccine);			
                }
            }

			//Apagar capa salud
            if (e.target && e.target.getAttribute("data-id") == 'apagarsalud') {                
				document.querySelector('.legendSalud').style.display = "none";
            }
			
			//Apagar capa oxigeno
			if (e.target && e.target.getAttribute("data-id") == 'apagaroxigeno') {
                groupLayerHealthOxygen.clearLayers();
				document.querySelector('.legendOx').remove();
            }
			
			//Apagar capa oxigeno
			if (e.target && e.target.getAttribute("data-id") == 'apagaroxigeno') {
                groupLayerHealthVaccine.clearLayers();
            }
        });
    }

    let addInfoLayerToMap = (containerLayer, contentLayer, callBackClick, nivelGobierno) => {
        containerLayer.clearLayers();

        var markers = L.geoJson(contentLayer, {
            pointToLayer: function(feature, latlng) {
                var marker = L.marker(latlng, {
                    icon: L.divIcon({ html: '<img src="assets/app/img/info.png" title="' + feature.properties.nomubigeo + '" border="0"/>', className: 'info' })
                });
                return marker;
            }
        }).addTo(containerLayer);

        markers.on('click', function(e) {
            var data = e.layer.feature.properties;
            callBackClick(nivelGobierno, data);
        });
    }

    let handleClickLayerRegion = (nivelGobierno, data) => {
        App.mostrarModalEntidad(data);
    }

    let getInfoUbigeoRegion = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_dpto_geojson_info&outputFormat=application%2Fjson`);
        const infoUbigeoRegion = await response.json();
        infoRegion = infoUbigeoRegion.features;
    }

    let getInfoUbigeoProvince = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_provincia_geojson_info&outputFormat=application%2Fjson`);
        const infoUbigeoProvince = await response.json();
        infoProvince = infoUbigeoProvince.features;
    }

    let getInfoUbigeoDistrict = async() => {
        const response = await fetch(`${API_REST_GEOSERVER}:vw_distrito_geojson_info&outputFormat=application%2Fjson`);
        const infoUbigeoDistrict = await response.json();
        infoDistrict = infoUbigeoDistrict.features;
    }

    let filterHealthLayers = () => {
        const regionId = filterParams.find(x => x.key == 'ddlRegion').selected;
        const provinceId = filterParams.find(x => x.key == 'ddlProvincia').selected;
        const layerProvinceNames = layers.filter(x => x.nivelGobierno == NIVEL_DE_GOBIERNO_PROVINCIAL || x.nivelGobierno == NIVEL_DE_GOBIERNO_DISTRITAL);
        const queryFilterRegion = "coddpto IN (" + regionId.join(',') + ")";
        const queryFilterProvince = "codprov IN (" + provinceId.join(',') + ")";

        if (regionId.length > 0) layers.forEach(layer => filterLayer(layer, queryFilterRegion));
        //if(provinceId.length > 0) layers.forEach(layer => filterLayer(layer, queryFilterProvince));
    }

    let filterLayer = ({ name }, queryFilter = '1 = 1') => {
        /*window[name].wmsParams.cql_filter = queryFilter;
        window[name].redraw();*/
    }
	
	let setLegend = (title = '', subtitle = '') => {
		let html = '';
		const colors = ["037E34", "A2E07A", "ECFC25", "FC8A25", "E73E3E"];
        const labels = ["Muy Bajo", "Bajo", "Medio", "Alto", "Muy Alto"];
		
		html+= `<h4>${title}</h4>`;
		html+=  labels.map( (label, i) => `<i style="background: #${colors[i]}"></i><span>${label}</span><br>`).join('');
		html+= `<h4 style="text-align: left">${subtitle}</h4>`;
		return html;		
	}
	
	let setDateLegend = async () => {
		legendDates = await getDateLegend();
	}
	
	let getDateLegend = async () => {
		const objCoordenadas = { method: 'fecha_actualizacion_data' };
		const response = await fetch(`http://observatorio.colaboraccion.pe/php/app.php?data=${encodeURIComponent(JSON.stringify(objCoordenadas))}`);
		const data = await response.json();
		return data;
	}
	
	let findUpdateDate = (type) => {
		return legendDates.find(x => x.tipo == type).fecha;
	}

    return {
        init: function() {
            return init();
        },
        filterHealthLayers: function() {
            return filterHealthLayers();
        },
        findUpdateDate: function(type) {
            return findUpdateDate(type);
        }
    }
}();
