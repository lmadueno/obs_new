<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
    if($data->titulo=='Minsa'){
?>
							<table class="table table-sm table-detail" width="100%" >
								<thead>
                                    <tr> 
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold" width="1%"></th>
										<th class="text-center bold" width="1%"></th>
										<th class="text-left bold" >Región</th>
										<th class="text-center bold" > Poblacion</th> 
                                        <th class="text-center bold" > Fallecidos</th>     
                                        <th class="text-center bold" > Fallecidos/Pob</th> 
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;  
                                    $s2020=0;   
                                    $sPoblacion=0;   
                                    $sPoblacionfall=0;
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacionSalud','tipo'=> $data->titulo]);
                                  
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
                                        $s2020=   $s2020+$item->total ;	
                                        $sPoblacion=   $sPoblacion+$item->poblacion ;	
                                        $sPoblacionfall=   $sPoblacionfall+$item->fatotal20_21;											
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" title="Ficha Entidad">
                                            <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                                                <i class="fas fa-file-contract"></i>
                                            </a>													
                                        </td>
										<td class="text-center" title="Resumen de Fallecidos por Mes">
                                            <a class="lnkAmpliarMeses" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                                                <i class="fas fa-calendar-alt"></i>
                                            </a>													
                                        </td>
                                        <td class="text-left">
                                            <a class="lnkAmpliarInfoResumenPSalud" id="<?php echo $item->ubigeo;?>" data-event="lnkProvXrutas_<?php echo $i?>" href="#" onclick="App.events(this); return false;">
                                                <?php echo $item->departamento;?>
                                            </a>													
                                        </td>
                                        <td class="text-center" title="<?php echo number_format($item->poblacion)?>"><?php echo number_format($item->poblacion)?></td>
                                        <td class="text-center" title="<?php echo number_format($item->total)?>"><?php echo number_format($item->total)?></td>												
                                        <td class="text-center" title="<?php echo number_format($item->fatotal20_21)?>"><?php echo number_format($item->fatotal20_21,2)?></td>												

                                    </tr>
									<tr data-target="lnkProvXrutas_<?php echo $i?>" style="display: none;">
										<td colspan="7">
											<div id="divinformacionProvinciaSalud_<?php echo $item->ubigeo;?>">
											</div>
										</td>
									</tr>
								<?php } ?>
									<tr>
                                        <td ></td>
										<td ></td>
										<td ></td>
                                        <td >Total</td>
                                        <td class="text-center" ><?php echo number_format($sPoblacion)?></td>	
                                        <td class="text-center" ><?php echo number_format($s2020)?></td>	
                                        <td class="text-center" ><?php echo number_format($s2020/$sPoblacion,2);?></td>												
                                    </tr>
								</tbody>
							</table>
<?php
    }else if($data->titulo=='Sinadef'){
?>
    <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
    <table class="table table-sm table-detail" width="100%">
        <tr>
            <th class="text-center bold vert-middle" colspan="5" style=" background: #2f7ed8; color: white; ">General</th>											
            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2018</th>
            <th></th>
            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2019</th>
            <th></th>
            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2020</th>	
            <th></th>
            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2021</th>	
            														
        </tr> 
        <tr>
        
            <th class="text-center bold vert-middle">#</th>
            <th class="text-center bold vert-middle"></th>
            <th class="text-center bold vert-middle"></th>
            <th class="text-left bold vert-middle">Region</th>
            <th class="text-center bold vert-middle">Poblacion</th>
            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>	
            <th style="border-bottom: 2px solid #dee2e600;"></th>
            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
            <th style="border-bottom: 2px solid #dee2e600;"></th>
            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
            <th style="border-bottom: 2px solid #dee2e600;"></th>
            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
            													
        </tr>   

        <?php 
            $i=0;  
            $Suma1=0 ; 
            $Suma2=0 ;                          
            $Suma3=0 ;
            $Suma5=0 ;
            $Suma7=0 ;
            $pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacionSalud','tipo'=> $data->titulo]);
            foreach ($pipxUbigeoDet as $item){
                $i++;  
                $Suma1+= $item->anio2018;   
                $Suma2+= $item->poblacion;                       
                $Suma3+= $item->anio2019;
                $Suma5+= $item->anio2020;
                $Suma7+= $item->anio2021;                                                                     
        ?>	
        <tr>
            <td  class="text-center"><?php echo $i;?></td>
            <td class="text-center" title="Ficha Entidad">
                <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                    <i class="fas fa-file-contract"></i>
                </a>													
            </td>
            <td class="text-center" title="Resumen de Fallecidos por Mes">
                <a class="lnkAmpliarMesesSinadef" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                    <i class="fas fa-calendar-alt"></i>
                </a>													
            </td>
            <td class="text-left">
                <a class="lnkAmpliarInfoResumenPSalud" id="<?php echo $item->ubigeo;?>" data-event="lnkProvXrutas_<?php echo $i?>" href="#" onclick="App.events(this); return false;">
                    <?php echo $item->departamento_domicilio;?>
                </a>													
            </td>
            <td class="text-center"><?php echo number_format($item->poblacion)?></td>
            <td class="text-center"><?php echo number_format($item->anio2018)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2018_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2019)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2019_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2020)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2020_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2021)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2021_1000, 1),2)?></td>    
        </tr>
            
        <tr data-target="lnkProvXrutas_<?php echo $i?>" style="display: none;">
            <td colspan="19">
                <div id="divinformacionProvinciaSalud_<?php echo $item->ubigeo;?>"></div>
            </td>
        </tr>
        <?php
            }
        ?>
        <tr>	
            <td  class="text-center"></td>
            <td  class="text-center"></td>
            <td  class="text-center"></td>
            <td  class="text-left">Total</td>
            <td  class="text-center"><?php echo number_format($Suma2);?></td>
            <td  class="text-center"><?php echo number_format($Suma1);?></td>
            <td  class="text-center"></td>
            <td></td>
            <td  class="text-center"><?php echo number_format($Suma3);?></td>
            <td  class="text-center"></td>
            <td></td>
            <td  class="text-center"><?php echo number_format($Suma5);?></td>
            <td  class="text-center"></td>
            <td></td>
            <td  class="text-center"><?php echo number_format($Suma7);?></td>
            <td  class="text-center"></td>                                   
        </tr>
    </table>
</div>

<?php
    }else if($data->titulo=='Ciprl'){
        if($data->tabla=='gobiernos'){
        ?>  

							<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold" width="1%"></th>
										<th class="text-center bold" width="15%">Región</th>
										<th class="text-center bold" width="15%"> 2018</th>
										<th class="text-center bold" width="15%"> 2019</th>
										<th class="text-center bold" width="15%"> 2020</th>      
										<th class="text-center bold" width="15%"> 2021</th>      
										<th class="text-center bold" width="15%">Total</th>   
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$s2021= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacion','tipo'=> 'CIPRL']);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->s2018 ;	
										$s2019=   $s2019+$item->s2019 ;	
										$s2020=   $s2020+$item->s2020 ;	
										$s2021=   $s2021+$item->s2021 ;	
										$stotal=   $stotal+$item->total ;												
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" title="Ficha Entidad">
                                            <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                                                <i class="fas fa-file-contract"></i>
                                            </a>													
                                        </td>
                                        <td class="text-left">
                                            <a class="lnkAmpliarInfoResumenP" id="<?php echo $item->ubigeo;?>" data-event="lnkProvXrutas_<?php echo $i?>" href="#" onclick="App.events(this); return false;">
                                                <?php echo $item->departamento;?>
                                            </a>													
                                        </td>
                                        <td class="text-center" title="<?php echo number_format($item->s2018)?>"><?php echo number_format(round($item->s2018/1000000, 1),2)?></td>												
                                        <td class="text-center" title="<?php echo number_format($item->s2019)?>"><?php echo number_format(round($item->s2019/1000000, 1),2)?></td>
                                        <td class="text-center" title="<?php echo number_format($item->s2020)?>"><?php echo number_format(round($item->s2020/1000000, 1),2)?></td>
                                        <td class="text-center" title="<?php echo number_format($item->s2021)?>"><?php echo number_format(round($item->s2021/1000000, 1),2)?></td>
										<td class="text-center" title="<?php echo number_format($item->total)?>"><?php echo number_format(round(($item->total)/1000000, 1),2)?></td>

                                    </tr>
									<tr data-target="lnkProvXrutas_<?php echo $i?>" style="display: none;">
										<td colspan="8">
											<div id="divinformacionProvincia_<?php echo $item->ubigeo;?>">
											</div>
										</td>
									</tr>
								<?php } ?>
									<tr>
                                        <td ></td>
										<td ></td>
                                        <td >Total</td>
                                        <td class="text-center" ><?php echo number_format(round($s2018/1000000, 1),2)?></td>												
                                        <td class="text-center"><?php echo number_format(round($s2019/1000000, 1),2)?></td>
                                        <td class="text-center"><?php echo number_format(round($s2020/1000000, 1))?></td>
                                        <td class="text-center"><?php echo number_format(round($s2021/1000000, 1))?></td>
										<td class="text-center"><?php echo number_format(round(($stotal)/1000000, 1),2)?></td>

                                    </tr>
								</tbody>
							</table>
		
    <?php
    }else{
    ?>

							<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold" width="3%">#</th>
                                        <th class="text-center bold" width="27%">Universidad</th>
										<th class="text-center bold" width="10%">Región</th>
										<th class="text-center bold" width="15%"> 2018</th>
										<th class="text-center bold" width="15%"> 2019</th>
										<th class="text-center bold" width="15%"> 2020</th>      
										<th class="text-center bold" width="15%"> 2021</th>      
										<th class="text-center bold" width="15%">Total</th>   
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$s2021= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'getCiprlUniversidades']);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->s2018 ;	
										$s2019=   $s2019+$item->s2019 ;	
										$s2020=   $s2020+$item->s2020 ;	
										$s2021=   $s2021+$item->s2021 ;	
										$stotal=   $stotal+$item->total ;												
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td  class="text-left"><?php echo $item->universidad;?></td>
                                        <td  class="text-left"><?php echo $item->departamento;?></td>
                                        <td class="text-right" title="<?php echo number_format($item->s2018)?>"><?php echo number_format(round($item->s2018/1000000, 1),2)?></td>												
                                        <td class="text-right" title="<?php echo number_format($item->s2019)?>"><?php echo number_format(round($item->s2019/1000000, 1),2)?></td>
                                        <td class="text-right" title="<?php echo number_format($item->s2020)?>"><?php echo number_format(round($item->s2020/1000000, 1),2)?></td>
                                        <td class="text-right" title="<?php echo number_format($item->s2021)?>"><?php echo number_format(round($item->s2021/1000000, 1),2)?></td>
										<td class="text-right" title="<?php echo number_format($item->total)?>"><?php echo number_format(round(($item->total)/1000000, 1),2)?></td>

                                    </tr>
									
								<?php } ?>
									<tr>
                                        <td ></td>
										<td ></td>
                                        <td >Total</td>
                                        <td class="text-right" ><?php echo number_format(round($s2018/1000000, 1),2)?></td>												
                                        <td class="text-right"><?php echo number_format(round($s2019/1000000, 1),2)?></td>
                                        <td class="text-right"><?php echo number_format(round($s2020/1000000, 1))?></td>
                                        <td class="text-right"><?php echo number_format(round($s2021/1000000, 1))?></td>
										<td class="text-right"><?php echo number_format(round(($stotal)/1000000, 1),2)?></td>

                                    </tr>
								</tbody>
							</table>

    <?php
    }
    ?>
<?php
    }
?>