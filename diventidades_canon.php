<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>


<div class="row">
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				<h5 class="m-b-0 text-black text-center">Reporte de Consulta de Tope CIPRL x Departamento
				</h5>
			</div>

			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Departamentos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_ciprl" id="ddlDepartamentos_ciprl" name="ddlDepartamentos_ciprl" onchange="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Provincias</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_ciprl" id="ddlProvincias_ciprl" name="ddlProvincias_ciprl" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Distritos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_ciprl" id="ddlDistritos_ciprl" name="ddlDistritos_ciprl" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Accion</label>
															<button type="button" class="form-control input-sm btn btn-primary filtrociprl" id="filtrociprl" name="filtrociprl" onclick="App.events(this); return false;">Filtrar</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				<h5 class="m-b-0 text-black text-center">[1] Beneficiarios del 40% de CANON, según población de cada Distrito
				</h5>
			</div>
			<div class="card-body">
				<div class="row">
					<br>
					<table class="table table-hover table-sm" data-target="datos" width="100%">
						<thead>
							<tr>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center" width="4%">#</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Quintil</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Distritos</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Población</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Promedio anual</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Monto minimo</th>
								<th scope="col" class="text-center bold" style="background:#ddebf8;text-align:center">Monto Maximo</th>
							</tr>
						</thead>
						<tbody id="tbl_dist_canon1" style="text-align: center;">
							<tr>
								<td>1
								</td>
								<td>Q1
								</td>
								<td>24
								</td>
								<td>116,035
								</td>
								<td>S/.2,936
								</td>
								<td>S/.1,754
								</td>
								<td>S/.15,683
								</td>
							</tr>
							<tr>
								<td>2
								</td>
								<td>Q2
								</td>
								<td>74
								</td>
								<td>273,181
								</td>
								<td>S/.950
								</td>
								<td>S/.596
								</td>
								<td>S/.1,706
								</td>
							</tr>
							<tr>
								<td>3
								</td>
								<td>Q3
								</td>
								<td>154
								</td>
								<td>577,937
								</td>
								<td>S/.455
								</td>
								<td>S/.378
								</td>
								<td>S/.588
								</td>
							</tr>
							<tr>
								<td>4
								</td>
								<td>Q4
								</td>
								<td>262
								</td>
								<td>2,022,597
								</td>
								<td>S/.268
								</td>
								<td>S/.182
								</td>
								<td>S/.182
								</td>
							</tr>
							<tr>
								<td>5
								</td>
								<td>Q5
								</td>
								<td>1,360
								</td>
								<td>26,392,139
								</td>
								<td>S/.52
								</td>
								<td>S/.0
								</td>
								<td>S/.181
								</td>
							</tr>
						</tbody>
					</table>
				</div>

			</div>
			
		</div>
		<br>
		<div class="card">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				<h5 class="m-b-0 text-black text-center">[2] Beneficiarios del 40% de CANON, Monto anual poblador por distrito

				</h5>
			</div>
			
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Departamentos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_canon" id="ddlDepartamentos_canon" name="ddlDepartamentos_canon" onchange="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Provincias</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_canon" id="ddlProvincias_canon" name="ddlProvincias_canon" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Distritos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_canon" id="ddlDistritos_canon" name="ddlDistritos_canon" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Accion</label>
															<button type="button" class="form-control input-sm btn btn-primary filtrocanon" id="filtrocanon" name="filtrocanon" onclick="App.events(this); return false;">Filtrar</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><br>
				<div class="row">
					<br>
					<table class="table table-hover table-sm" data-target="datos" width="100%">
						<thead>
							<tr>
								<th scope="col" class="text-center bold" width="4%">#</th>
								<th scope="col" class="text-center bold">Ciudad</th>
								<th scope="col" class="text-center bold">CIPRL</th>
								<th scope="col" class="text-center bold">CANON anual</th>
								<th scope="col" class="text-center bold">40% CANON anual</th>
								<th scope="col" class="text-center bold">Población</th>
								<th scope="col" class="text-center bold">Monto anual</th>
							</tr>
						</thead>
						<tbody id="tbl_dist_canon" style="text-align: center;"></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


</div>