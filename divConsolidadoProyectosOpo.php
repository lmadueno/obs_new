<?php
require 'php/app.php';

   $data = json_decode($_GET['data']);	

   $opo    = dropDownList((object) ['method' => 'consolidadototalOportunidades']);
    
?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            OPORTUNIDADES DE PROYECTOS VIABLES
        <br>
        </div>
        <div class="col-lg-12" style="text-align:center">
           <div id="idconsolidadoOportunidadesGrafico" ></div>
        </div>
        <div class="col-lg-12" style="text-align:center">
            (Perfil Viable sin Expediente Técnico)
        </div>
        
            <div class="row">
                <div class="col-12">
                    <table class="table table-sm table-detail">
                        <tr>
                            <th class="text-center bold" width="5%">#</th>
                            <th class="text-center bold" width="20%">NOMBRE</th>
                            <th class="text-center bold" width="15%">PROYEC.</th>
                            <th class="text-center bold" width="15%">MONTO</th>
                        </tr>
                            <?php  $j=0;

                            foreach ($opo as $key2){  $j++ ;

                            ?>
                        <tr>
                            <td class="text-center" ><?php    echo ($j)?></td>
                            <td class="text-left" ><?php echo (strtoupper($key2->nom))?></td>
                            <td class="text-right" ><?php  echo number_format($key2->proyecto)?></td>
                            <td class="text-right" ><?php  echo number_format($key2->monto)?></td>  
                        </tr>   
                       
                            <?php }  ?>
                    </table>
                </div>    
            </div> 
            <div class="scrollable" style="overflow-y: auto;max-height: 550px;">
                <div id="idconsolidadoOportunidades"></div>
            </div>
    </div>
</div>
     

 
