import { BaseObs, html } from "./base-obs.js";

export class CheckboxesObs extends BaseObs {

    static get properties() {
        return {
            data: { type: Array },
            selected: { type: Array }
        };
    }

    createRenderRoot() {
        return this;
    }

    constructor() {
        super();
        this.data = [{
            columnName: 'cruza_corredor',
            name: 'Corredor minero'
        },
        {
            columnName: 'cruza_oleoducto',
            name: 'Oleoducto'
        },
        {  
            columnName: 'cruza_via',          
            name: 'Corredores principales'
        },
        {
            columnName: 'cruza_lineaferrea',
            name: 'Líneas férreas'
        }];
        this.selected = [];
    }

    handleClickCheckbox(e, columnName = ''){
        const index = this.selected.indexOf(columnName);

        if (index > -1) {
            this.selected.splice(index, 1);
        }else{
            this.selected.push(columnName);
        }
        
        this.dispatchEvent(new CustomEvent('event-onchange', {
            detail: this.selected
        }));
    }

    render() {
        return html`
        ${this.data.map(item =>        
            html`
            <div>
                <div class="form-check form-check-inline">                
                    <input class="form-check-input layers" type="checkbox" @click="${(e) => this.handleClickCheckbox(e, item.columnName)}">
                    <label class="form-check-label not-bold">${item.name}</label>
                </div>
            </div>`
        )}`;
    }
}

customElements.define('checkboxes-obs', CheckboxesObs);