import { BaseObs, html } from "./base-obs.js";

const API_REST_GEOSERVER = 'http://95.217.44.43:8080/geoserver/colaboraccion_2020/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=colaboraccion_2020';

export class InfoLayerObs extends BaseObs {

    static get properties() {
        return {
            departamentosIds: { type: Array },
            provinciasIds: { type: Array },
            distritosIds: { type: Array },
            montoCiprlIni: { type: Number },
            montoCiprlFin: { type: Number },
            poblacionIni: { type: Number },
            poblacionFin: { type: Number },
            corredorMinero: { type: Boolean },
            oleoducto: { type: Boolean },
            corredorPrincipales: { type: Boolean },
            lineasFerreas: { type: Boolean },

            departamentos: { type: Array },
            provincias: { type: Array },
            distritos: { type: Array },
        };
    }

    async firstUpdated() {
        await this.get('lit/json/info_region.json');
        this.departamentos = this.responseData.features;

        await this.get('lit/json/info_provincia.json');
        this.provincias = this.responseData.features;

        await this.get('lit/json/info_distrito.json');
        this.distritos = this.responseData.features;
    }

    getInfoUbigeos(nivel_gobierno = 1) {
        if (nivel_gobierno == 1) return this.getInfo([...this.departamentos], 1);
        if (nivel_gobierno == 2) return this.getInfo([...this.provincias], 2);
        if (nivel_gobierno == 3) return this.getInfo([...this.distritos], 3);
    }

    getInfo(ubigeos = [], nivel_gobierno = 0) {
        const filtros_capas = [];

        const montoCiprlFin = this.montoCiprlIni > 0 && this.montoCiprlFin == 0 ? 99999999999 : this.montoCiprlFin;
        const poblacionFin = this.poblacionIni > 0 && this.poblacionFin == 0 ? 99999999999 : this.poblacionFin;

        if (this.corredorMinero) filtros_capas.push({ capa: "cruza_corredor" })
        if (this.oleoducto) filtros_capas.push({ capa: "cruza_oleoducto" })
        if (this.corredorPrincipales) filtros_capas.push({ capa: "cruza_via" })
        if (this.lineasFerreas) filtros_capas.push({ capa: "cruza_lineaferrea" })

        if (this.departamentosIds.length > 0)
            ubigeos = ubigeos.filter(({ properties }) => this.departamentosIds.includes(properties.id_departamento));

        if ((nivel_gobierno == 2 && this.provinciasIds.length > 0) || (nivel_gobierno == 3 && this.distritosIds.length == 0 && this.provinciasIds > 0))
            ubigeos = ubigeos.filter(({ properties }) => this.provinciasIds.includes(properties.id_provincia));

        if (nivel_gobierno == 3 && this.distritosIds.length > 0)
            ubigeos = ubigeos.filter(({ properties }) => this.distritosIds.includes(properties.id_distrito));

        if (this.montoCiprlIni > 0 || this.montoCiprlFin > 0)
            ubigeos = ubigeos.filter(({ properties }) => properties.ciprl >= this.montoCiprlIni && properties.ciprl <= montoCiprlFin);

        if (this.poblacionIni > 0 || this.poblacionFin > 0)
            ubigeos = ubigeos.filter(({ properties }) => properties.poblacion >= this.poblacionIni && properties.poblacion <= poblacionFin);

        if (filtros_capas.length > 0)
            ubigeos = ubigeos.filter(({ properties }) => filtros_capas.some(({ capa }) => properties[capa] > 0))

        console.log(ubigeos);
        return ubigeos;
    }

    constructor() {
        super();
        this.departamentosIds = [];
        this.provinciasIds = [];
        this.distritosIds = [];
        this.montoCiprlIni = 0;
        this.montoCiprlFin = 0;
        this.poblacionIni = 0;
        this.poblacionFin = 0;
        this.corredorMinero = false;
        this.oleoducto = false;
        this.corredorPrincipales = false;
        this.lineasFerreas = false;

        this.departamentos = [];
        this.provincias = [];
        this.distritos = [];
    }

    /*render() {
        return html`XXXXXXXXXX`;
    }*/
}

customElements.define('infolayer-obs', InfoLayerObs);