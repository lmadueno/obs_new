import { LitElement, html, css } from "https://unpkg.com/lit-element@2.3.1?module";
import 'https://unpkg.com/@material/mwc-textfield@^0.19.1/mwc-textfield.js?module';

export { html, css };

export class BaseObs extends LitElement {
    static get properties() {
        return {
            responseData: { type: Array }
        };
    }

    async get(url = '', parameters = {}) {
        this.responseData = await this.fetchUrl("get", url, parameters);
    }

    async fetchUrl(_method, _url, _data) {
        return new Promise((resolve, reject) => {
            axios({
                method: _method,
                url: _url,
                data: _data
            }).then(function(response) {
                resolve(response.data);
            }).catch(function(error) {
                console.log("Fetch Error");
                console.log(error);
                reject("this Error:" + error);
            });
        });
    }

    constructor() {
        super();
        this.responseData = [];
    }
}

customElements.define('base-obs', BaseObs);