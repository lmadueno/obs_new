/* filter-obs.js */
export const GEOSERVER_URI = 'http://78.46.16.8:8080/geoserver/colaboraccion_2020/ows?service=WFS&version=1.0.0&request=GetFeature';
export const MONTO_CIPRL_INI = 'MONTO_CIPRL_INI';
export const MONTO_CIPRL_FIN = 'MONTO_CIPRL_FIN';
export const POBLACION_INI = 'POBLACION_INI';
export const POBLACION_FIN = 'POBLACION_FIN';
//CIPRL
export const CIPRL_GR = 'CIPRL_GR';
export const CIPRL_MP = 'CIPRL_MP';
export const CIPRL_MD = 'CIPRL_MD';
//SALUD
export const SALUD_GR_COVID = 'SALUD_GR_COVID';
export const SALUD_MP_COVID = 'SALUD_MP_COVID';
export const SALUD_MD_COVID = 'SALUD_MD_COVID';
export const SALUD_GR_SINADEF = 'SALUD_GR_SINADEF';
export const SALUD_MP_SINADEF = 'SALUD_MP_SINADEF';
export const SALUD_MD_SINADEF = 'SALUD_MD_SINADEF';
//OXIGENO
export const OXIGENO_GR = 'OXIGENO_GR';
export const OXIGENO_MP = 'OXIGENO_MP';
export const OXIGENO_MD = 'OXIGENO_MD';
//VACUNAS
export const VACUNAS_GR = 'VACUNAS_GR';
export const VACUNAS_MP = 'VACUNAS_MP';
export const VACUNAS_MD = 'VACUNAS_MD';