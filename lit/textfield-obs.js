import { BaseObs, html } from "./base-obs.js";

export class TextFieldObs extends BaseObs {

    static get properties() {
        return {
            value: { type: String },
            placeholder: { type: String },
        };
    }

    createRenderRoot() {
        return this;
    }

    handleOnKeyUp(e) {
        if (e.which >= 37 && e.which <= 40) return;
        this.value = e.target.value;
        e.target.value = this.numberWithCommas(this.value);
    }

    numberWithCommas(x) {
        return x.toString().replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    constructor() {
        super();
        this.value = '';
        this.placeholder = '';
    }

    render() {
        return html`<input type="text" class="form-control form-control-sm" placeholder="${this.placeholder}"
    @keyup="${(e) => this.handleOnKeyUp(e)}" />`;
    }
}

customElements.define('textfield-obs', TextFieldObs);