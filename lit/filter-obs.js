import { BaseObs, html } from "./base-obs.js";

import './select-obs.js';
import './checkboxes-obs.js';
import './textfield-obs.js';

import { GEOSERVER_URI, MONTO_CIPRL_INI, MONTO_CIPRL_FIN, POBLACION_INI, POBLACION_FIN } from "./const-obs.js";

export class FilterObs extends BaseObs {
    static get properties() {
        return {
            isOpen: { type: Boolean },
            displayProvincia: { type: Boolean },
            displayDistrito: { type: Boolean },

            ubigeos: { type: Array },
            departamentos: { type: Array },
            provincias: { type: Array },
            distritos: { type: Array },
            montoCiprlIni: { type: String },
            montoCiprlFin: { type: Number },
            poblacionIni: { type: Number },
            poblacionFin: { type: Number },

            departamentosElegidos: { type: Array },
            provinciasElegidas: { type: Array },
            distritosElegidos: { type: Array },
            capasElegidas: { type: Array }
        };
    }

    createRenderRoot() {
        return this;
    }

    getDepartamentos() {
        return this.ubigeos.find(x => x.ubigeo == 'departamentos').data;
    }

    getProvincias() {
        return this.ubigeos.find(x => x.ubigeo == 'provincias').data;
    }

    getDistritos() {
        return this.ubigeos.find(x => x.ubigeo == 'distritos').data;
    }

    addUbigeo(ubigeo = '', data) {
        this.ubigeos.push({
            'ubigeo': ubigeo,
            'data': data.features.reduce((acc, { properties }) => (acc.push(properties), acc), [])
        });
    }

    async firstUpdated() {
        await this.get(`${GEOSERVER_URI}&typeName=colaboraccion_2020:departamentos&maxFeatures=2000&outputFormat=application%2Fjson`);
        this.addUbigeo('departamentos', this.responseData);
        this.departamentos = this.getDepartamentos();

        await this.get(`${GEOSERVER_URI}&typeName=colaboraccion_2020:provincias&maxFeatures=2000&outputFormat=application%2Fjson`);
        this.addUbigeo('provincias', this.responseData);

        await this.get(`${GEOSERVER_URI}&typeName=colaboraccion_2020:distritos_f&maxFeatures=2000&outputFormat=application%2Fjson`);
        this.addUbigeo('distritos', this.responseData);
    }

    handleChangeDepartamentos(event) {
        const detail = event.detail;		
        this.departamentosElegidos = event.detail;
		if(detail.length == 0){
			this.provinciasElegidas = [];
			this.distritosElegidos = [];
		}
        if (this.displayProvincia)
            this.provincias = this.getDepartamentos().filter(x => detail.indexOf(x.id_departamento) > -1).map((y) => ({
                ...y,
                data: this.getProvincias().filter(z => z.id_departamento == y.id_departamento)
            }));
    }

    handleChangeProvincias(event) {
        const detail = event.detail;
        this.provinciasElegidas = detail;
		if(detail.length == 0){
			this.distritosElegidos = [];
		}

        if (this.displayDistrito)
            this.distritos = this.getProvincias().filter(x => detail.indexOf(x.id_provincia) > -1).map((y) => ({
                ...y,
                data: this.getDistritos().filter(z => z.id_provincia == y.id_provincia)
            }));
    }

    handleChangeDistritos(event) {
        const detail = event.detail;
        this.distritosElegidos = detail;
    }

    handleChangeCapas(event) {
        const detail = event.detail;
        this.capasElegidas = detail;
    }

    stringToNumber(number = '') {
        return Number(number.replace(/,/g, ''));
    }

    setProperty(e, property) {
        switch (property) {
            case MONTO_CIPRL_INI:
                this.montoCiprlIni = this.stringToNumber(e.target.value);
                break;
            case MONTO_CIPRL_FIN:
                this.montoCiprlFin = this.stringToNumber(e.target.value);
                break;
            case POBLACION_INI:
                this.poblacionIni = this.stringToNumber(e.target.value);
                break;
            case POBLACION_FIN:
                this.poblacionFin = this.stringToNumber(e.target.value);
                break;
        }
    }

    handleClickFiltrar() {
        const cqlFilter = [];
        const montoCiprlFin = this.montoCiprlIni > 0 && this.montoCiprlFin == 0 ? 99999999999 : this.montoCiprlFin;
        const poblacionFin = this.poblacionIni > 0 && this.poblacionFin == 0 ? 99999999999 : this.poblacionFin;

        if (this.departamentosElegidos.length > 0) cqlFilter.push({ key: 'departamento', query: `id_departamento in (${this.departamentosElegidos.join(',')})` });
        if (this.provinciasElegidas.length > 0) cqlFilter.push({ key: 'provincia', query: `id_provincia in (${this.provinciasElegidas.join(',')})` });
        if (this.distritosElegidos.length > 0) cqlFilter.push({ key: 'distrito', query: `id_distrito in (${this.distritosElegidos.join(',')})` });
        if (this.montoCiprlIni > 0 || this.montoCiprlFin > 0) cqlFilter.push({ key: 'ciprl', query: `ciprl between ${this.montoCiprlIni} and ${montoCiprlFin}` });
        if (this.poblacionIni > 0 || this.poblacionFin > 0) cqlFilter.push({ key: 'poblacion', query: `poblacion between ${this.poblacionIni} and ${poblacionFin}` });
        if (this.capasElegidas.length > 0) cqlFilter.push({ key: 'capas', items: this.capasElegidas, query: `(${this.capasElegidas.map(layer => `${layer} > 0`).join(" or ")})` });
				
		this.dispatchEvent(new CustomEvent("message", { detail: { query: cqlFilter } }));
	}

	handleClickLimpiar() {
		this.provincias = [];
		this.provincias = [];
		this.distritos = [];
		this.montoCiprlIni = 0;
		this.montoCiprlFin = 0;
		this.poblacionIni = 0;
		this.poblacionFin = 0;
	}

	constructor() {
		super();
		this.isOpen = true;
		this.displayProvincia = true;
		this.displayDistrito = true;
		this.ubigeos = [];
		this.regiones = [];
		this.provincias = [];
		this.distritos = [];
		this.montoCiprlIni = 0;
		this.montoCiprlFin = 0;
		this.poblacionIni = 0;
		this.poblacionFin = 0;
		this.departamentosElegidos = [];
		this.provinciasElegidas = [];
		this.distritosElegidos = [];
		this.capasElegidas = [];
	}
	
	handleClickMostrarOcultar() {
		this.isOpen = !this.isOpen;
	}

	render() {
		return html`
		<div>
			<b class="${this.isOpen ? 'hide' : ''}">Mostrar criterios de búsqueda</b>
			<a href="javascript:void" style="position: absolute; right: 5px;" @click="${this.handleClickMostrarOcultar}"><i class="fa fa-${this.isOpen ? 'arrow-down' : 'arrow-up'}"></i></a>			
		</div>
		
		<div class="container-fluid ${!this.isOpen ? 'hide' : ''}">			
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Región</label>
						<div class="row">
							<div class="col">
								<select-obs identifier="ddlDepartamento" .itemsData="${this.departamentos}" @event-onchange="${this.handleChangeDepartamentos}" columnNameValue="id_departamento" columnNameLabel="departamento"></select-obs>
							</div>
						</div>
					</div>
				</div>
			</div>
		
			${this.displayProvincia && this.provincias.length > 0 ? html`
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Provincia</label>
						<div class="row">
							<div class="col">
								<select-obs identifier="ddlProvincia" isOptionGroup="true" .itemsData="${this.provincias}"
								@event-onchange="${this.handleChangeProvincias}" columnNameOptionGroup="departamento"
								columnNameValue="id_provincia" columnNameLabel="provincia">
								</select-obs>
							</div>
						</div>						
					</div>
				</div>
			</div>` : html``}
		
			${this.displayDistrito && this.distritos.length > 0 ? html`
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Distrito</label>
						<div class="row">
							<div class="col">
								<select-obs identifier="ddlDistrito" isOptionGroup="true" .itemsData="${this.distritos}"
								@event-onchange="${this.handleChangeDistritos}" columnNameOptionGroup="provincia"
								columnNameValue="id_distrito" columnNameLabel="distrito">
								</select-obs>
							</div>
						</div>							
					</div>
				</div>
			</div>` : html``}
		
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Monto CIPRL(Nuevos Soles)</label>
						<div class="row">
							<div class="col">
								<textfield-obs placeholder="0" @change="${(e) => this.setProperty(e, MONTO_CIPRL_INI)}">
								</textfield-obs>
							</div>
							<div class="col">
								<textfield-obs placeholder="1,060,091,958" @change="${(e) => this.setProperty(e, MONTO_CIPRL_FIN)}">
								</textfield-obs>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Población(Habitantes)</label>
						<div class="row">
							<div class="col">
								<textfield-obs placeholder="0" @change="${(e) => this.setProperty(e, POBLACION_INI)}">
								</textfield-obs>
							</div>
							<div class="col">
								<textfield-obs placeholder="9,485,400" @change="${(e) => this.setProperty(e, POBLACION_FIN)}">
								</textfield-obs>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<label class="bold">Capas</label>
						<checkboxes-obs @event-onchange="${this.handleChangeCapas}"></checkboxes-obs>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12" style="text-align: center;">
					<button type="button" class="btn btn-primary btn-sm" @click="${this.handleClickFiltrar}">Filtrar</button>
					<button type="button" class="btn btn-danger btn-sm" @click="${this.handleClickLimpiar}" style="display: none;">Limpiar</button>
				</div>
			</div>
		</div>`;
	}
}

customElements.define('filter-obs', FilterObs);
