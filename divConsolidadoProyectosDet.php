<?php
require 'php/app.php';

   $data = json_decode($_GET['data']);	
   
   $regi  = dropDownList((object) ['method' => 'consolidadototalProyectoRPD','tipo'=>$data->tipo,'nivel'=>$data->nivel]);
 
?>


<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-12" style="text-align:center">
            <div class="row">
               
                    <div class="col-lg-12" style="text-align:center !important">
                            
                        <label class="radio-inline" style="padding-left:1em"><input type="radio" name="rbtncprpd" id="rbtnCantProy" value="rbtnCantProy" <?php if($data->checked==1){?>checked<?php } ?>>CANTIDAD</label>
                        <label class="radio-inline" style="padding-left:2em"><input type="radio" name="rbtncprpd" id="rbtnMontoProy" value="rbtnMontoProy" <?php if($data->checked==2){?>checked<?php } ?>>MONTO</label>
                            
                    </div>
                    <div class="col-lg-12">
                        <table class="table table-sm table-detail">
                            <tr>
                                <th class="text-center bold" width="5%">#</th>
                                <?php if ($data->nivel==5 || $data->nivel==1) {?>
                                <th class="text-center bold" width="20%">DEPARTAMENTO</th>
                                <?php }?>
                                <?php if ($data->nivel==1) {?>
                                <th class="text-center bold" width="20%">PROVINCIA</th>
                                <?php }?>
                                <th class="text-center bold" width="20%">ENTIDAD</th>
                                <th class="text-center bold" width="15%"># PROYECTOS</th>
                                <th class="text-center bold" width="15%">MONTO</th>
                                <th class="text-center bold" width="15%">CIPRL</th>
                            </tr>
                             <?php  $i=0;
                                
                                foreach ($regi  as $key2){  $i++ ;
                                    
                            ?> 
                            <tr>
                                <td class="text-center"><?php echo ($i)?></td>
                                <?php if ($data->nivel==5 || $data->nivel==1) {?>    
                                <td class="text-center"><?php echo (strtoupper($key2->dept))?></td>
                                <?php }?>
                                <?php if ($data->nivel==1) {?>
                                <td class="text-center"><?php echo (strtoupper($key2->prov))?></td>
                                <?php }?>
                                <td class="text-center">
                                    <a href="#" class="lnkconsolidadoDetalle" onclick="App.events(this); return false;" data-event='<?php echo ($key2->ubigeo),'-',($data->nivel)?>'>
                                    
                                        <?php echo (strtoupper($key2->nom))?>
                                    </a>
                                </td>
                                <td class="text-right"><?php echo number_format($key2->proyecto)?></td>
                                <td class="text-right"><?php echo number_format($key2->monto)?></td>
                                <td class="text-right"><?php echo number_format($key2->ciprl)?></td>
                            </tr>
                            <?php }  ?>
                        </table>
                    </div>
                    
            
            </div> 
        </div>
    </div>
</div>
