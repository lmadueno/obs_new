<?php

    $data = json_decode($_GET['data']);		
?>
<style>
	.form-group,.form-control {
  				
		font-size: 10px !important;
				
			 }	
             .highcharts-title {
    	fill: #434348;
    	font-weight: bold;
    	font-size: 12px !important;
	}
	.bold{
		font-weight: bold;
		background: #1976d226;
		color: #000;
	}
	.table thead th {
		vertical-align: bottom;
		border-bottom: 2px solid #fefeff;
	}	 
</style>
<input type="hidden" id="txttipoCIPRLcANONM" value="<?php echo $data->tipo ?>">
<div class="container"  width= "100%"!important  height="100%"!important>
    <div class="card card-outline-info" >
        <div class="card-header">
			<div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white">INDICADORES Y ESTADISTICAS</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarESTADISTICAS" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarESTADISTICAS" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
			</div>
        </div> 
        <ul class="nav nav-tabs" id="myTab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active tabEstadistica" style="color: #1976D2; !important;" id="macro-tab" href="#" onclick="App.events(this)" title="Analisis Macro Indicadores"><i class="fa fa-briefcase"></i><b style="padding-left:0.5em;">MacroIndicadores</b></a>
                </li>
                <li class="nav-item">
					<a class="nav-link tabEstadistica" style="color: black; !important;" id="ciprl-tab" href="#" onclick="App.events(this)" title="Analisis de Ciprl por años"><i class="fa fa-file-invoice-dollar"></i><b style="padding-left:0.5em;">CIPRL</b></a>
                </li>   
                <li class="nav-item">
					<a class="nav-link  tabEstadistica" id="cannon-tab" style="color: black; !important;"  href="#" onclick="App.events(this)" title="Analisis de Canon por años"><i class="fa fa-hand-holding-usd"></i><b style="padding-left:0.5em;">CANON</b></a>
                </li>  
                <li class="nav-item">
					<a class="nav-link  tabEstadistica" id="minsa-tab" style="color: black; !important;"  href="#" onclick="App.events(this)" title="Analisis de Minsa por años"><i class="fa fa-hospital"></i><b style="padding-left:0.5em;">COVID-19</b></a>
                </li>  
                <li class="nav-item">
					<a class="nav-link  tabEstadistica" id="sinadef-tab" style="color: black; !important;"  href="#" onclick="App.events(this)" title="Analisis de Minsa por años"><i class="fa fa-clinic-medical"></i><b style="padding-left:0.5em;">SINADEF</b></a>
                </li> 
                <li class="nav-item">
					<a class="nav-link  tabEstadistica" id="proyectado-tab" style="color: black; !important;"  href="#" onclick="App.events(this)" title="Proyectado de Fallecidos"><i class="fa fa-book-dead"></i><b style="padding-left:0.5em;">Proyectado Fallecidos</b></a>
                </li>
                <li class="nav-item">
					<a class="nav-link  tabEstadistica" id="empresas-tab" style="color: black; !important;"  href="#" onclick="App.events(this)" title="Proyectado de Fallecidos"><i class="fa fa-building"></i><b style="padding-left:0.5em;">Empresas</b></a>
                </li>  
        </ul>
        <div class="card-body">
            <div class="tab-content">
                <div id="divresumeninformacion"></div>
            </div> 
        </div>   
    </div>     
</div>
