<?php
 require 'php/app.php';
 $data   = json_decode($_GET['data']);
 $excelEmpresas = dropDownList((object) ['method' => 'gen_tablaciprl','tipo' => $data->tipo]);
 $temp=0;
 $temp2=0;
 ?>


<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            Monto CIPRL X Distrito en Millones de Soles
        </div>
    </div>
    <div class="col-6">
        <table class="table table-sm table-detail">

            <thead>																				
            <tr>
                <th class="text-center" width= "10%"><b>Quantil</b></th>
                <th class="text-center" width= "20%"><b>CIPRL</b></th>
                <th class="text-center" width= "20%"><b>Entidades</b></th>
                <th class="text-center" width= "20%"><b>CIPRL%</b></th>
                <th class="text-center" width= "20%"><b>Entidades %</b></th>
            </tr>	
            </thead>                                                
        <?php foreach ($excelEmpresas as $item){ 
            $temp=$temp+$item->ciprl;
            $temp2=$temp2+$item->cantidad;
            ?> 
            
            <tr>
				<td class="text-left">
					<a class="lnkAmpliar" data-event="lnkProvXcorredor_<?php echo $item->id?>" href="#" onclick="App.events(this); return false;">
						<b><?php echo $item->nombre;?></b>
					</a>													
				</td>
				<td class="text-right"><?php echo number_format($item->ciprl);?> M</td>												
				<td class="text-right"><?php echo number_format($item->cantidad);?></td>
				<td class="text-right"><?php echo number_format($item->ciprl_porcentaje);?>%</td>
				<td class="text-right"><?php echo number_format($item->entidades);?>%</td>						
			</tr>
            <tr data-target="lnkProvXcorredor_<?php echo $item->id?>" style="display: none;">
                <td colspan="5">
                    <div class="card">
						<div class="card-header card-special">
							Entidades
						</div>
                        <div class="card-body">
							<div class="row">
								<div class="col-lg-12">
                                    <table class="table table-sm table-detail">
                                        <thead>
											<tr>
                                                <th class="text-center bold" width="15%">Region</th>
                                                <th class="text-center bold" width="15%">Provincia</th>
                                                <th class="text-center bold" width="15%">Distrito</th>
												<th class="text-center bold" width="10%">CIPRL</th>
												<th class="text-center bold" width="10%">Canon</th>
												<th class="text-center bold" width="10%">PIM</th>
											</tr>

										</thead>  
                                        <?php 
                                        
                                        $excelentidad = dropDownList((object) ['method' => 'gen_tablaciprl_det1','quantil' => $item->id]);
                                        foreach ($excelentidad as $item1){ ?> 
                                            <tr>
                                                <td class="text-left"><?php echo $item1->departamento?></td>
                                                <td class="text-left"><?php echo $item1->provincia?></td>
                                                <td class="text-left"><?php echo $item1->distrito?></td>
                                                <td class="text-right"><?php echo round($item1->ciprl/1000000, 1)?> M</td>
										        <td class="text-left"><?php echo round($item1->canon/1000000, 1)?> M</td>
										        <td class="text-left"><?php echo round($item1->pim/1000000, 1)?> M</td>
                                            </tr>
                                        <?php  } ?>  
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                
                </td>
            </tr>
        <?php  } ?>   
            <tr>
                <td class="text-right  bold vert-middle">Total :</td>
                <td class="text-right  bold vert-middle"><?php echo number_format($temp);?> M</td>                           
                <td class="text-right  bold vert-middle"><?php echo ($temp2);?></td>
                <td class="text-right  bold vert-middle"></td>
                <td class="text-right  bold vert-middle"></td>
            </tr>                       
    
    
    
    
        </table>
    </div>
    <div class="col-6">
        <div id="idTablaciprlGraficoDistritosGraf" style="height: 230px; width: 100%"></div>                                       
    </div>
</div>