
<?php
	require 'php/app.php';
    $data   = json_decode($_GET['data']);
    $pipxUbigeo = dropDownList((object) ['method' => 'pipsObjetosFuncion','nivelgobierno'=>$data->nivelgobierno,'distritos'=>$data->distritos]);
    $tempPIP1=0;
    $tempPIP2=0;
    $tempPIP3=0;
    $tempPIP4=0;
    $tempPIP5=0;
    $tempPIP6=0; 								
?>
<input type="hidden" value="<?php echo $data->distritos?>" id="distritosObjetos">
<div class="row">
	<div class="col-lg-12">
        <table class="table table-sm table-detail"> 
            <thead>										
				<tr>
				    <th class="text-center bold" rowspan="2" style="vertical-align:middle">Función</th>
					<th class="text-center bold" colspan="2" width="30%">Formulación</th>
				    <th></th>
					<th class="text-center bold" colspan="2" width="30%">Evaluación</th>
					<th></th>
					<th class="text-center bold" colspan="2" width="30%">Perfil Viable</th>
					<th></th>
					<th class="text-center bold" colspan="2" width="30%">Total</th>
				</tr>	
				<tr>
				    <th class="text-center bold" style="vertical-align:middle">Cant</th>
					<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
				    <th></th>
					<th class="text-center bold" style="vertical-align:middle">Cant</th>
					<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
					<th></th>
				    <th class="text-center bold" style="vertical-align:middle">Cant</th>
					<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
					<th></th>
					<th class="text-center bold" style="vertical-align:middle">Cant</th>
					<th class="text-center bold" style="vertical-align:middle">Monto S/</th>
				</tr>	
            </thead>  
            <tbody>
            <?php
                foreach ($pipxUbigeo as $key){ 
                    $tempPIP1+=$key->_canformulacion;
                    $tempPIP2+=$key->_sumformulacion;
                    $tempPIP3+=$key->_canevaluacion;
                    $tempPIP4+=$key->_sumevaluacion;
                    $tempPIP5+=$key->_canperfil;
                    $tempPIP6+=$key->_sumperfil;  								
            ?> 
                <tr>
                    <td class="text-left">
                        <a class="lnkAmpliarOleoducto" id="<?php echo $key->_codfuncion.'$'.$data->nivelgobierno?>" data-event="lnkProvXrutas_<?php echo $key->_codfuncion.$data->nivelgobierno?>" href="#" onclick="App.events(this); return false;">
                            <?php echo ucfirst(strtolower($key->_funcion))?>
                        </a>			                                                    
                    </td>														                                                       
                    <td class="text-center">
                        <?php echo number_format($key->_canformulacion)?>
                    </td>
                    <td class="text-center">
                        <?php echo number_format($key->_sumformulacion)?>
                    </td>
                    <td></td> 
                    <td class="text-center">
                        <?php echo number_format($key->_canevaluacion)?>
                    </td>
                    <td class="text-center">
                        <?php echo number_format($key->_sumevaluacion)?>
                    </td>
                    <td></td> 
                    <td class="text-center">
                        <?php echo number_format($key->_canperfil)?>
                    </td>
                    <td class="text-center">
                        <?php echo number_format($key->_sumperfil)?>
                    </td>
                    <td></td> 
                    <td class="text-center">
                        <?php echo number_format($key->_canperfil+$key->_canformulacion+$key->_canevaluacion)?>
                    </td>
                    <td class="text-center">
                        <?php echo number_format($key->_sumperfil+$key->_sumformulacion+$key->_canformulacion)?>
                    </td>                                            
                </tr>
                <tr data-target="lnkProvXrutas_<?php echo $key->_codfuncion.$data->nivelgobierno?>" style="display: none;">
                    <td colspan="12">
                        <div class="card">
                            <div class="card-body">
								<div class="row">
									<div class="col-lg-12">
                                        <div id="tableoleoductoGrupoFuncional<?php echo $key->_codfuncion.$data->nivelgobierno?>"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>              
                </tr>
            <?php
	        }
            ?>
                <tr>
					<td class="text-left">TOTAL :</td>
					<td class="text-center"><?php echo ($tempPIP1)?></td>
					<td class="text-center"><?php echo number_format($tempPIP2)?></td>
					<td></td>
					<td class="text-center"><?php echo ($tempPIP3)?></td>
					<td class="text-center"><?php echo number_format($tempPIP4)?></td>
					<td></td>
					<td class="text-center"><?php echo ($tempPIP5)?></td>
					<td class="text-center"><?php echo number_format($tempPIP6)?></td>
					<td></td>
					<td class="text-center"><?php echo ($tempPIP1+$tempPIP3+$tempPIP5)?></td>
					<td class="text-center"><?php echo number_format($tempPIP2+$tempPIP4+$tempPIP6)?></td>
				</tr>	
            </tbody>
        </table>
    </div>
</div>
