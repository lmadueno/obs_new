<?php
    require 'php/app.php';   
			$data = json_decode($_GET['data']);	
			$user = dropDownList((object) ['method' => $data->method,'id' =>$data->id]);     
?> 
<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<div class="card card-outline-info" >
    <div class="card-header">
		<div class="row">
			<div class="col-lg-12">
				<h6 class="m-b-0 text-white">DATOS GENERALES DE SESSION</h6>				
            </div>
		</div>
    </div>  
    <div class="card-body">
       <div class="row">
           <div class="col-12">
                <table class="table table-sm table-detail" >
                    <tr>
                        <td class="text-center bold"  width="15%" >Documento</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm txtedit" id="txtnroM" value="<?php echo $user{0}->nro_documento;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Apellido Paterno</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm txtedit" id="txtapepatM" value="<?php echo $user{0}->ape_pat;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Apellido Materno</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm txtedit" id="txtapematM" value="<?php echo $user{0}->ape_mat;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Nombres</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm txtedit" id="txtnombresM" value="<?php echo $user{0}->nombres;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Correo Electronico</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm txtedit" id="txtcorreoM"  value="<?php echo $user{0}->correo;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Clave</td>
						<td class="text-center " width="45%">
                         
                                <div class="row">
                                    <div class="col-10">
                                        <input type="password" class="form-control form-control-sm txtedit" id="txtclaveM" value="<?php echo $user{0}->pwd;?>" readonly>
                                    </div>
                                    <div class="col-1">
                                        <button class="btn btn-outline-secondary btn-sm"  onclick="App.events(this); return false;" type="button" id="showpwd"><i class="fa fa-eye"></i></button>
                                    </div>
                                </div>
                          
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Empresa</td>
						<td class="text-center " width="45%">
                            <input type="text" class="form-control form-control-sm" id="txtempresaM" value="<?php echo $user{0}->empresa;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td class="text-center bold"  width="15%" >Rol</td>
						<td class="text-center " width="45%" >
                            <input type="text" class="form-control form-control-sm" id="txtrolM" value="<?php echo $user{0}->descripcion;?>" readonly>
                        </td>   																						
                    </tr>
                    <tr>
                        <td colspan="2"  class="text-center " style="padding-top:1em !important" id="showmdfyBottmUser">
                            <button type="button" id="mostrarUsuario" onclick="App.events(this); return false;" class="btn btn-secondary btn-sm lnkMostrarOcultar"><i class="fa fa-edit"></i> Modificar</button>
                        </td>																						
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center "  id="mdfyBottmUser" style="display:none;padding-top:1em !important">
                            <button type="button" id="modificarUsuario"  onclick="App.events(this); return false;" class="btn btn-primary btn-sm"><i class="fa fa-check"></i>Grabar</button>
                            <button type="button" onclick="App.events(this); return false;" class="btn btn-danger btn-sm lnkMostrarOcultar" ><i class="fa fa-times"></i> Cancelar</button>
                        </td>																		
                    </tr>
                </table>
           </div>
       </div>     
    </div>     
</div>
