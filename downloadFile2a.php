<?php
require 'php/spreadsheet/vendor/autoload.php';
$nFilas = isset($_SERVER['argv'][1])?intval($_SERVER['argv'][1]):85000;
ini_set('memory_limit', '2G');

$inicio = microtime(true);
// $objPHPExcel = new PhpOffice\PhpSpreadsheet\Spreadsheet();
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$spreadsheet = new Spreadsheet();
$spreadsheet->getProperties()
->setCreator('Colaboraccion')
->setLastModifiedBy('Colaboraccion')
->setTitle('Proyectos PIP')
->setSubject('Proyectos PIP')
->setDescription('Proyectos PIP')
->setKeywords('Proyectos PIP')
->setCategory('Proyectos PIP');
$objWorksheet = $spreadsheet->getActiveSheet();
$objWorksheet->setTitle('Proyectos');

for ($fila = 1; $fila <= $nFilas; $fila++) {
  
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('C' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('B' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('D' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('J' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('K' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('L' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('M' . $fila,$fila);
  $spreadsheet->setActiveSheetIndex(0)->setCellValue('N' . $fila,$fila);
}

$cell_st =[
	'font' =>['bold' => true],
	'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
	'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
];

$spreadsheet->getActiveSheet()->getStyle('A1:N1')->applyFromArray($cell_st);

$objWorksheet
->setCellValue('A1', 'Código')
->setCellValue('B1', 'Región')
->setCellValue('C1', 'Organización')
->setCellValue('D1', 'Oportunidad')
->setCellValue('E1', 'Función')
->setCellValue('F1', 'Programa')
->setCellValue('G1', 'Fase')
->setCellValue('H1', 'Nivel')
->setCellValue('I1', 'Monto Asignado')
->setCellValue('J1', 'SNIP')
->setCellValue('K1', 'Cod. Único')
->setCellValue('L1', 'Origen')
->setCellValue('M1', 'F. Registro')
->setCellValue('N1', 'Estado');
$objWriter = new Xlsx($spreadsheet);
$objWriter->save("resultadosss.xlsx");

echo implode(' ', [
  $nFilas,
  memory_get_peak_usage(true),
  microtime(true) - $inicio,
  filesize("resultadosss.xlsx")
]), PHP_EOL;