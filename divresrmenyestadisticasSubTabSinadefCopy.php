<?php
require 'php/app.php';
	$data = json_decode($_GET['data']);	
?>
	<?php
		if($data->tab=='resumen-tab'){
	?>	
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header card-special text-center font-weight-bold">
									Fallecidos <?php echo $data->tipo;?> a nivel nacional </div>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="col lg-4">
								<div id="chartSinadef4_"></div>
							</div>
						</div>
						<div class="col-lg-12">
						<?php
							if($data->tipo=='SINADEF'){
						?>	
						<table class="table table-sm table-detail" width="100%">
							<thead>
									<tr>
										<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">General</th>
										<th></th>											
										<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2017</th>
										<th></th>
										<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2018</th>
										<th></th>
										<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2019</th>	
										<th></th>
										<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2020</th>	
																								
									</tr> 
                                    <tr> 
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold" >Mes</th>
										<th style="border-bottom: 2px solid #dee2e600;"></th>
										<th class="text-center bold" > Fallecidos 2017</th> 
										<th class="text-center bold" > %</th> 
										<th style="border-bottom: 2px solid #dee2e600;"></th>
                                        <th class="text-center bold" > Fallecidos 2018</th>
										<th class="text-center bold" > %</th>  
										<th style="border-bottom: 2px solid #dee2e600;"></th>  
                                        <th class="text-center bold" > Fallecidos 2019</th>
										<th class="text-center bold" > %</th> 
										<th style="border-bottom: 2px solid #dee2e600;"></th> 
										<th class="text-center bold" > Fallecidos 2020</th> 
										<th class="text-center bold" > %</th> 
										
                                    </tr>
									<?php 
									$i=0;  
									$s2017=0;
									$s2018=0;  
									$s2019=0;  
									$s2020=0;  
								
                                    
									$pipxUbigeoDet = dropDownList((object) ['method' => 'tabla__mes_sinadef']);
									$TotalesFallecidos = dropDownList((object) ['method' => 'tabla__mes_sinadef_totales']);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2017=$item->s2017+$s2017;
										$s2018=$item->s2018+$s2018;  
										$s2019=$item->s2019+$s2019;  
										$s2020=$item->s2020+$s2020;  
										
																	
                                	?>
									<tr> 
										<td class="text-center"><?php echo ($i)?></td>
										<td class="text-center"><?php echo ($item->meses)?></td>	
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($item->s2017)?></td>
										<td class="text-center"><?php echo number_format(round((($item->s2017*100)/$TotalesFallecidos{0}->t1), 1),2)?></td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($item->s2018)?></td>	
										<td class="text-center"><?php echo number_format(round((($item->s2018*100)/$TotalesFallecidos{0}->t2), 1),2)?></td>
										<td class="text-center"></td>											
										<td class="text-center"><?php echo number_format($item->s2019)?></td>
										<td class="text-center"><?php echo number_format(round((($item->s2019*100)/$TotalesFallecidos{0}->t3), 1),2)?></td>	
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($item->s2020)?></td>
										<td class="text-center"><?php echo number_format(round((($item->s2020*100)/$TotalesFallecidos{0}->t4), 1),2)?></td>
									</tr>
									<?php } ?>
									<tr> 
										<td class="text-center"></td>	
										<td class="text-center">Total</td>
										<td class="text-center"></td>	
										<td class="text-center"><?php echo number_format($s2017)?></td>		
										<td class="text-center">100</td>
										<td class="text-center"></td>												
										<td class="text-center"><?php echo number_format($s2018)?></td>	
										<td class="text-center">100</td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($s2019)?></td>
										<td class="text-center">100</td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($s2020)?></td>
										<td class="text-center">100</td>
																				
									</tr>
                            </thead>
						</table>
						<?php
							}else{
						?>
						<table class="table table-sm table-detail" width="100%">
							<thead>
                                    <tr> 
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold" >Mes</th>
										<th class="text-center bold" >Fallecidos</th>   
                                    </tr>
									<?php 
									$i=0;  
								
									$sTotal=0;     
                                    
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'quintil_mes_covid']);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
									
										$sTotal=$sTotal+$item->fallecidos; 									
                                	?>
									<tr> 
										<td class="text-center"><?php echo ($i)?></td>
										<td class="text-center"><?php echo ($item->meses)?></td>
										<td class="text-center"><?php echo ($item->fallecidos)?></td>	
																					
									</tr>
									<?php } ?>
									<tr> 
										<td class="text-center"></td>	
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($sTotal)?></td>												
									</tr>
                            </thead>
						</table>
						<?php
							}
						?>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="card">
								<div class="card-header card-special text-center font-weight-bold">
									Distribucion de fallecidos <?php echo $data->tipo;?> por quintiles </div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						
						<div class="col lg-4">
							<div id="chartSinadef1_" style="width: 400px; height: 250px; margin: 0 auto"></div>
						</div>
						<div class="col lg-4">
							<div id="chartSinadef2_" style="width: 400px; height: 250px; margin: 0 auto"></div>
						</div>
						<div class="col lg-4">
							<div id="chartSinadef3_" style="width: 400px; height: 250px; margin: 0 auto"></div>
						</div>
					</div>
					
					<?php
						}else{	
					?>	
					<div class="row">
                         <div class="col-lg-12">
                            <div id="chart1"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>	
                        </div>
                        <div class="col-lg-12" id="mostarchar2" style="display:none;text-align:center">
                            <div id="chart_2"  data-target="lnkProvXrutas1" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>	
                            <a class="lnkAmpliar" data-event="lnkProvXrutas1" href="#" onclick="App.events(this); return false;">
                                Mostrar/Ocultar grafico de Provincias
                            </a>
                        </div>
						<div class="col-lg-12" id="mostarchar3" style="display:none;text-align:center">
                            <div id="chart_3"  data-target="lnkProvXrutas2" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>	
                            <a class="lnkAmpliar" data-event="lnkProvXrutas2" href="#" onclick="App.events(this); return false;">
                                Mostrar/Ocultar grafico de Distritos
                            </a>
                        </div>
                    </div>	
					<?php
						}
					?>
	