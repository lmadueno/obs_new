<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>

<div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-header" style="background:#ddebf8">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="m-b-0 text-black text-center"> Proyectos de Inversion (PI) cantidad y monto por niveles de gobiernos </h4>
                        </div>
                        <div class="col-lg-12" style="text-align:center">
                            Fuente : <a href="https://aplicaciones007.jne.gob.pe/srop_publico/Consulta/OrganizacionPolitica?fbclid=IwAR1r9zl9fGmruL1bBBfToSE5WoOiw9qIj3v_lQFy1o6zKA7zYEzdgw12vb4" target="_blank"> JNE Directorio de Organizaciones Políticas </a>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="chart_mef_entidades"></div>
                        </div>
                        <div class="col-lg-12">
                            <table class="table table-sm table-detail" width="100%">
                                <tr>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Nivel de Gobierno</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad Autoridades</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">CIPRL</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables sin ejecución </th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables sin ejecución </th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables con ejecución </th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables con ejecución </th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI Viables </th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI Viables </th>
                                </tr>
                                <?php
                                $i = 0;
                                $s1 = 0;
                                $s2 = 0;
                                $s3 = 0;
                                $s4 = 0;
                                $s5 = 0;
                                $s6 = 0;
                                $s7 = 0;
                                $s8 = 0;
                                $pipxUbigeoDet = dropDownList((object) ['method' => 'pi_partidospoliticos']);
                                // echo json_encode($pipxUbigeoDet);
                                foreach ($pipxUbigeoDet as $item) {
                                    $i++;
                                    $s1 =   $s1 + $item->entidades;
                                    $s2 =   $s2 + $item->cantidad_pro_s;
                                    $s3 =   $s3 + $item->monto_pro_s;
                                    $s4 =   $s4 + $item->cantidad_pro;
                                    $s5 =   $s5 + $item->monto_pro;
                                    $s6 =   $s6 + $item->cantidad_pro_s + $item->cantidad_pro;
                                    $s7 =   $s7 + $item->monto_pro + $item->monto_pro_s;
                                    $s8 =   $s8 + $item->ciprl;
                                ?>
                                    <tr>
                                    <?php if($item->nomnivelgobierno!='GOBIERNO PROVINCIAL'){?>

                                        <td class="text-center"><?php echo $i; ?></td>
                                        <td class="text-center"><button class="btn btn-link lnkNivelGobiernoPartido" id="<?php echo ($item->nomnivelgobierno) ?>" data-event="lnkProvXrutas_<?php echo ($item->nomnivelgobierno) ?>" onclick="App.events(this);"><?php echo ($item->nomnivelgobierno) ?></button></td>
                                        <td class="text-center"><?php echo number_format($item->entidades) ?></td>
                                        <td class="text-center"><?php echo number_format($item->ciprl) ?></td>
                                        <td class="text-center"><?php echo number_format($item->cantidad_pro_s) ?></td>

                                        <td class="text-center"><?php echo number_format($item->monto_pro_s) ?></td>
                                        <td class="text-center"><?php echo number_format($item->cantidad_pro) ?></td>
                                        <td class="text-center"><?php echo number_format($item->monto_pro) ?></td>
                                        <td class="text-center"><?php echo number_format($item->cantidad_pro_s + $item->cantidad_pro) ?></td>
                                        <td class="text-center"><?php echo number_format($item->monto_pro + $item->monto_pro_s) ?></td>
                                    <?php } ?>
                                    </tr>
                                    <tr data-target="lnkProvXrutas_<?php echo $item->nomnivelgobierno ?>" style="display: none;">
                                        <td colspan="10">
                                            <div class="card">
                                                <div class="card-header">
                                                    Partidos Politicos
                                                </div>
                                                <div class="card-body">
                                                    <div id="div_<?php echo $item->nomnivelgobierno ?>"></div>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td class="text-center"></td>
                                    <td class="text-center"><button class="btn btn-link lnkAmpliar" data-event="lnkProvXrutas_123" onclick="App.events(this);">Total</button></td>
                                    <td class="text-center">1899</td>
                                    <td class="text-center"><?php echo number_format($s8) ?></td>
                                    <td class="text-center"><?php echo number_format($s2) ?></td>
                                    <td class="text-center"><?php echo number_format($s3) ?></td>
                                    <td class="text-center"><?php echo number_format($s4) ?></td>
                                    <td class="text-center"><?php echo number_format($s5) ?></td>
                                    <td class="text-center"><?php echo number_format($s6) ?></td>
                                    <td class="text-center"><?php echo number_format($s7) ?></td>
                                </tr>

                                <!-- fila total -->
                                <tr data-target="lnkProvXrutas_123" style="display: none;">
                                    <td colspan="9">
                                        <div class="card">
                                            <div class="card-header">
                                                Total
                                            </div>
                                            <div class="card-body">
                                                <table class="table table-sm table-detail" width="100%">
                                                    <tr>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Partidos Politicos</th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">GR</th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">MP </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">MD </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center"> Total</th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">CIPRL </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables sin ejecución </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables sin ejecución </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables con ejecución </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables con ejecución </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables </th>
                                                        <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables </th>
                                                    </tr>
                                                    <?php
                                                    $i = 0;
                                                    $s1 = 0;
                                                    $s2 = 0;
                                                    $s3 = 0;
                                                    $s4 = 0;
                                                    $s5 = 0;
                                                    $s6 = 0;
                                                    $s7 = 0;
                                                    $s8 = 0;
                                                    $s9 = 0;
                                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'pi_partidospoliticos_totales']);
                                                    foreach ($pipxUbigeoDet as $item) {
                                                        $i++;
                                                        $s1 =   $s1 + $item->cantidad_departamento;
                                                        $s2 =   $s2 + $item->cantidad_provincia;
                                                        $s3 =   $s3 + $item->cantidad_distrito;
                                                        $s4 =   $s4 + $item->cantidad_total;
                                                        $s5 =   $s5 + ($item->cantidad_fase_departamento + $item->cantidad_fase_provincia + $item->cantidad_fase_distrito);
                                                        $s6 =   $s6 + ($item->monto_fase_departamento + $item->monto_fase_provincia + $item->monto_fase_distrito);
                                                        $s7 =   $s7 + ($item->cantidad_viable_departamento + $item->cantidad_viable_provincia + $item->cantidad_viable_distrito);
                                                        $s8 =   $s8 + ($item->monto_viable_departamento + $item->monto_viable_provincia + $item->monto_viable_distrito);
                                                        $s9 =   $s9 + ($item->ciprl_dpto + $item->ciprl_prov + $item->ciprl_dist);
                                                    ?>
                                                        <tr>
                                                            <td class="text-center"><?php echo $i; ?></td>
                                                            <td class="text-center">
                                                                <buttom class="btn btn-link lnkAmpliar_totales" id="<?php echo ($item->partido) ?>" data-event="lnkProvXrutas_<?php echo ($item->partido) ?>" href="#" onclick="App.events(this);">
                                                                    <?php echo ($item->partido) ?>
                                                                </buttom>
                                                            </td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_departamento) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_provincia) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_total) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->ciprl_dpto + $item->ciprl_prov + $item->ciprl_dist) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_fase_departamento + $item->cantidad_fase_provincia + $item->cantidad_fase_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->monto_fase_departamento + $item->monto_fase_provincia + $item->monto_fase_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_viable_departamento + $item->cantidad_viable_provincia + $item->cantidad_viable_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->monto_viable_departamento + $item->monto_viable_provincia + $item->monto_viable_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->cantidad_fase_departamento + $item->cantidad_fase_provincia + $item->cantidad_fase_distrito + $item->cantidad_viable_departamento + $item->cantidad_viable_provincia + $item->cantidad_viable_distrito) ?></td>
                                                            <td class="text-center"><?php echo number_format($item->monto_fase_departamento + $item->monto_fase_provincia + $item->monto_fase_distrito + $item->monto_viable_departamento + $item->monto_viable_provincia + $item->monto_viable_distrito) ?></td>
                                                        </tr>
                                                        <tr data-target="lnkProvXrutas_<?php echo $item->partido ?>" style="display: none;">
                                                            <td colspan="13">
                                                                <div class="card">
                                                                    <div class="card-header">
                                                                        Niveles de Gobierno
                                                                    </div>
                                                                    <div class="card-body">
                                                                        <div id="div_<?php echo $item->partido ?>"></div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td class="text-center"></td>
                                                        <td class="text-center">Total</td>
                                                        <td class="text-center"><?php echo number_format($s1) ?></td>
                                                        <td class="text-center"><?php echo number_format($s2) ?></td>
                                                        <td class="text-center"><?php echo number_format($s3) ?></td>
                                                        <td class="text-center"><?php echo number_format($s4) ?></td>
                                                        <td class="text-center"><?php echo number_format($s9) ?></td>
                                                        <td class="text-center"><?php echo number_format($s5) ?></td>
                                                        <td class="text-center"><?php echo number_format($s6) ?></td>
                                                        <td class="text-center"><?php echo number_format($s7) ?></td>
                                                        <td class="text-center"><?php echo number_format($s8) ?></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>