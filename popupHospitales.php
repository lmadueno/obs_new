
<?php

			$data = json_decode($_GET['data']);	
			
?>
	<div class="card card-outline-info">
		<div class="card-header">
			<div class="row">
				<div class="col-lg-9">
					<h6 class="m-b-0 text-white"><?php echo (($data->nombre));?></h6>				
				</div>
				<div class="col-lg-3 text-right">
					<a href="#" id="lnkCerrar" onclick="App.events(this); return false;"><i class="fa fa-times fa-white"></i></a>
				</div>
			</div>
		</div>
		<div class="card-body">				
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Región</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->departamento));?>	" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Provincia</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->provincia)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Distrito</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->distrito)); ?>" readonly>
					</div>
				</div>
			</div>
							
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group not-bottom">
						<label>Tipo</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->tipo)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group not-bottom">
						<label>Clasificación</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->clasificacion)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group not-bottom">
						<label>Nombre</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->nombre)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="form-group not-bottom">
						<label>Unidad Ejecutora</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->unidad_ejecutora)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Categoria</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo (($data->categoria)); ?>" readonly>
					</div>
				</div>
			</div>
		</div>
	</div>						
