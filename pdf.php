<?php
$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;

require_once $path . '/php/app.php';
require_once $path . '/php/mpdf/vendor/autoload.php';
require_once $path . '/php/jpgraph/jpgraph.php';
require_once $path . '/php/jpgraph/jpgraph_bar.php';
require_once $path . '/php/jpgraph/jpgraph_line.php';
require_once $path . '/php/jpgraph/jpgraph_pie.php';
require_once $path . '/php/jpgraph/jpgraph_pie3d.php';

if(count($_GET) > 0){
	if($_GET['data']){
		$data   = json_decode($_GET['data']);
		$obj    = dropDownList((object) ['method' => 'DatosXubigeo', 'codnivel' => $data->idnivel, 'codUbigeo' => $data->codubigeo]);
		$entidad = '';
		
		if(count((array)$obj) > 0){
			$obj = $obj{0};
			
			switch($data->idnivel){
				case 1:
					$entidad = 'Gobierno Regional de ' . ucwords(strtolower($obj->region));
				break;
				case 2:
					$entidad = 'Gobierno Provincial de ' . ucwords(strtolower($obj->provincia));
				break;
				case 3:
					$entidad = 'Gobierno Distrital de ' . ucwords(strtolower($obj->distrito));
				break;
			}
			
			$mpdf = new \Mpdf\Mpdf([
				'default_font_size' => 9,
				'default_font' => 'dejavusans',
				'mode' => 'c',
				'margin_left' => 32,
				'margin_right' => 25,
				'margin_top' => 25,
				'margin_bottom' => 20,
				'margin_header' => 10,
				'margin_footer' => 10
			]);

			$mpdf->mirrorMargins = 0;	// Use different Odd/Even headers and footers and mirror margins

			$header = '
			<table width="100%" border="0" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
			<td width="33%" style="vertical-align:middle;color:#000">Ficha Socioeconómica</span></td>
			<td width="33%" align="center"><img src="http://colaboraccion.pe/wp-content/uploads/2019/10/logo_colabora_blanco.png" width="126px" /></td>
			<td width="33%" style="vertical-align:middle;text-align: right;color:#000;">' . $entidad . '</td>
			</tr></table>';

			$headerEven = '
			<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-family: serif; font-size: 9pt; color: #000088;"><tr>
			<td width="33%"><span style="font-weight: bold;">Outer header</span></td>
			<td width="33%" align="center"><img src="http://colaboraccion.pe/wp-content/uploads/2019/10/logo_colabora_blanco.png" width="126px" /></td>
			<td width="33%" style="text-align: right;">Inner header p <span style="font-size:14pt;">{PAGENO}</span></td>
			</tr></table>';

			$footer = '<table width="100%" border="0" style="font-size:7.5pt;"><tr><td style="color: #686668">Av. República de Panamá Nº 3418 Of. 2401, San Isidro – Lima <br>(51) 1 500 7877   •   info@colaboraccion.pe   •   www.colaboraccion.pe</td><td align="right">Página <span style="font-size:9pt;">{PAGENO}</span></td></tr></table>';
			/*$footer = '<div align="left" style="font-size:8.5pt; color: #686668">Av. República de Panamá Nº 3418 Of. 2401, San Isidro – Lima <br>(51) 1 500 7877   •   info@colaboraccion.pe   •   www.colaboraccion.pe</div>';*/

			$html = '
			<body>
			<style>
				.table {
					border-collapse: collapse;
					margin-bottom: 1rem;
					color: #212529;
				}
				.table thead th {		
					background-color: #f1f1f1;
					border-color: #f1f1f1;
					height: 15px;
				}
				.table tbody td {
					border-bottom: 1px solid #ddd;
					padding-left: 10px;
					border-color: #f1f1f1;
					height: 15px;
				}
				.text-center{
					text-align: center;		
				}
				.text-right{
					text-align: right;		
				}
				#tblDatos tr td{
					padding-left: 5px;
					padding-right: 5px;
					height: 20px;
				}
				#tblDatos tr td:nth-child(odd){
					font-weight: bold;
					background-color: #f1f1f1;
				}
				#tblReglaFiscal{
					margin-right: 10px;
					margin-left: 10px;					
				}
				#tblReglaFiscal tbody tr td{
					font-size: 11px;
				}
				#tblReglaFiscal tr:nth-child(2) th{					
					font-weight: normal;
					font-size: 10px;
				}
				.text-align{
					text-align:center;
				}
			</style>			
			<table id="tblDatos" border="0" width="100%">
				<tbody>
					<tr>
						<td>Región</td>
						<td>' . ucwords(strtolower($obj->region)) . '</td>
						<td>Ubigeo</td>
						<td>' . codigoUbigeo($data->idnivel, $obj) . '</td>
					</tr>
					<tr>
						<td style="font-weight:bold">Provincia</td>
						<td>' . ucwords(strtolower($obj->provincia)) . '</td>
						<td style="font-weight:bold">Distrito</td>
						<td>' . ucwords(strtolower($obj->distrito)) . '</td>
					</tr>
					<tr>
						<td style="font-weight:bold">Autoridad</td>
						<td>' . ucwords(strtolower($obj->autoridad)) . '</td>
						<td style="font-weight:bold">Cargo</td>
						<td>' . ucfirst(strtolower($obj->cargo)) . '</td>
					</tr>
					<tr>
						<td style="font-weight:bold">Población</td>
						<td class="text-right">' . number_format($obj->poblacion) . ' (' . number_format((float)$obj->porc_poblacion, 2, '.', '') . '%)</td>
						<td style="font-weight:bold">Población Nac.</td>
						<td class="text-right">' . number_format($obj->poblacion_n) . '</td>
					</tr>
					<tr>
						<td style="font-weight:bold">Electores</td>
						<td class="text-right">' . number_format($obj->electores) . ' (' . number_format((float)$obj->porc_electores, 2, '.', '') . '%)</td>
						<td style="font-weight:bold">Electores Nac.</td>
						<td class="text-right">' . number_format($obj->electores_n) . '</td>
					</tr>
					<tr>
						<td style="font-weight:bold">Partido</td>
						<td colspan="3">' . ucwords(mb_strtolower($obj->partido)) . '</td>
					</tr>
				</tbody>
			</table>
			<br>
			<div style="border: 1px solid #efefeffc!important;">
				' . chart_bar($data->idnivel, $data->codubigeo, $data->coddpto) . '
				<br><br>
				<table class="table" border="1" width="400px" style="margin-left: 90px;">	
					<thead>
						<tr>
							<th width="10%" class="text-center">Año</th>
							<th width="20%" class="text-center">Canon MEF</th>
							<th width="20%" class="text-center">Ciprl MEF</th>
							<th width="25%" class="text-center">Regla Fiscal R1</th>
							<th width="25%" class="text-center">Regla Fiscal R2</th>
						</tr>
					</thead>
					<tbody> 
						' . ciprlXregion($data->idnivel,  $data->codubigeo) . '
					</tbody>
				</table>				
				<p style="font-family: Arial; font-weight:bold; font-size:13px;margin-left:10px;">Evolución de Regla Fiscal</p>
				<table id="tblReglaFiscal" class="table" border="1" width="100%">	
					<thead>
						<tr>
							<th class="text-center" colspan="6" style=" background: #2f7ed8; color: white;">R1: Regla Fiscal del Saldo de Deuda Total</th>
							<th></th>
							<th class="text-center" colspan="3" style=" background: #2f7ed8; color: white;">R2: Regla Fiscal de Ahorro en Cta. Corriente</th>
						</tr>
						<tr>
							<th class="text-center">Año</th>
							<th class="text-center">Saldo de Deuda Total <sup>1/</sup></th>
							<th class="text-center">Promo. ICT 2015-2018</th>
							<th class="text-center">Límite Ley N° 29230 <sup>2/</sup></th>
							<th class="text-center">Espacio Total</th>
							<th class="text-center">SDT/Prom. ICT 2015-2018 %</th>
							<th style="border-bottom: 2px solid #dee2e600;"></th>
							<th class="text-center" title="Ingreso Corriente Total">Ingreso Corriente Total</th>
							<th class="text-center">Gasto Corriente Total</th>
							<th class="text-center">Ahorro en Cta. Corriente</th>
						</tr>
					</thead>
					<tbody> 
						' . reglasFiscales($data->idnivel, $obj->codubigeo) . '
					</tbody>
				</table>
				<p style="margin-left: 10px;font-size: 11px;text-align: justify;">
					1/  El Saldo de Deuda Total esta compuesta por la información disponible del Saldo de Pasivos, Deuda Exigible con entidades del Estado y Deuda Real con el Sistema Privado de Pensiones a diciembre del 2018.
				</p>
				<p style="margin-left: 10px;font-size: 11px;text-align: justify;">
					2/ Corresponde a la suma de los flujos transferidos por concepto de Recursos Determinados provenientes de Canon, Sobrecanon, Regalías, Renta de Aduanas y Participaciones de los años 2016 y 2017, más el tope presupuestal por el mismo concepto incluido en el Presupuesto Institucional de Apertura 2018, aprobado mediante  Ley N° 30693 – Ley de Presupuesto del Sector Público para el Año Fiscal 2018, informado a través de Memorando N° 179-2018-EF/50.03 por la Dirección General de Presupuesto Público del Ministerio de Economía y Finanzas.
				</p>
			</div>			
			<br>			
			' . proyAdjudicados($data->codubigeo) . '
			<pagebreak orientation="portrait" />
			<div>
				<h3>Distribución de proyectos por función</h3>
				<table class="table" border="1" width="100%">	
					<thead>
						<tr>
							<th width="33%">Formulación</th>
							<th width="33%">Perfil Viable</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>							
							<td class="text-align">' . chart_pie($data->idnivel, 2,  $data->codubigeo, 'fo') . '</td>
							<td class="text-align">' . chart_pie($data->idnivel, 4,  $data->codubigeo, 'pv') . '</td>							
						</tr>
					</tbody>
				</table>				
				<table class="table" border="1" width="100%">	
					<thead>
						<tr>
							<th width="2%" rowspan="2"></th>
							<th width="48%" rowspan="2">Función</th>
							<th width="15%" colspan="2">Formulación</th>
							<th width="15%" colspan="2">Perfil Viable</th>
							<th width="15%" colspan="2">Total</th>
						</tr>
						<tr>
							<th class="text-center bold" style="vertical-align:middle">Cant.</th>
							<th class="text-center bold" style="vertical-align:middle">S/.</th>
							<th class="text-center bold" style="vertical-align:middle">Cant.</th>
							<th class="text-center bold" style="vertical-align:middle">S/.</th>
							<th class="text-center bold" style="vertical-align:middle">Cant.</th>
							<th class="text-center bold" style="vertical-align:middle">S/.</th>
						</tr>	
					</thead>
					<tbody> 
						' . proyXfase($data->idnivel,  $data->codubigeo) . '
					</tbody>
				</table>	
				<h3>Empresas por Ubigeo</h3>
				<table class="table" border="1" width="100%">	
					<thead>
						<tr>
							<th width="15%">Ranking</th>
							<th width="20%">RUC</th>
							<th width="35%">Nombre</th>
							<th width="30%">Monto OxI</th>

						</tr>
					</thead>
					<tbody> 
						' . funcionEmpresasXUbigeo($data->idnivel,  $data->codubigeo) . '
					</tbody>
				</table>
				
				<h3>Congresistas</h3>
				<table class="table" border="1" width="100%">	
					<thead>
						<tr>
							<th width="15%">#</th>
							<th width="20%">Nombre</th>
							<th width="35%">Partido</th>
							<th width="30%">Bancada</th>

						</tr>
					</thead>
					<tbody> 
						' . congresistas($data->codubigeo) . '
					</tbody>
				</table>	

				

			</div>
			</body>';			
			$mpdf->WriteHTML('<style>table {
			  border-collapse: collapse;
			}

			tr {
			  border: none;
			}

			td:first-child {
			  border-right: solid 1px #f00;			  
			}</style><div style="text-align: center;"><br><br><br><br><br><br><br><br><img src="http://colaboraccion.pe/wp-content/uploads/2017/06/colaboraccion-logo.png" style="opacity: 0.5;"/></div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><div style="text-align: center;"><B>DOCUMENTO</B><br>FICHA SOCIOECONÓMICA REGIÓN ' . $obj->region . '<br><br><br><table id="tblFooter" width="100%" border="1"><tr><td width="33%" align="center" style="color: #9c9c9c;">CREADO POR</td><td width="33%" align="center" style="color: #9c9c9c;">FECHA</td><td width="33%" align="center" style="color: #9c9c9c;">REGIÓN</td></tr><tr><td width="33%" align="center">ColaborAcción</td><td width="33%" align="center">' . date("d/m/Y")  . '</td><td width="33%" align="center">' . ucwords(strtolower($obj->region)) . '</td></tr></table></div>');			

			// Set the new Header before you AddPage
			$mpdf->SetHTMLHeader($header);
			$mpdf->AddPage();
			
			// Set the new Footer after you AddPage
			$mpdf->SetHTMLFooter($footer);
			$mpdf->WriteHTML($html);
			
			$mpdf->Output();
		}
	}
}

function codigoUbigeo($idnivel, $obj){
	if($idnivel == 1){
		return $obj->ubigeo_dept;
	} elseif ($idnivel == 2) {
		return $obj->ubigeo_prov;
	} elseif ($idnivel == 3) {
		return $obj->ubigeo_dist;
	}
}

function ciprlXregion($idnivel, $codubigeo){
	$html = '';
	$ciprlXregion = dropDownList((object) ['method' => 'ciprlXregionDet', 'idnivel' => $idnivel, 'codubigeo' => $codubigeo]);	
	
	foreach ($ciprlXregion as $key){
		$html .= '<tr>';
			$html .= '<td class="text-center">'. $key->ano . '</td>';
			$html .= '<td class="text-right">'. number_format($key->canon) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->ciprl) . '</td>';
			$html .= '<td class="text-center">'. (strlen($key->cumple_r1) > 0 ? $key->cumple_r1 : 'Sin data') . '</td>';
			$html .= '<td class="text-center">'. (strlen($key->cumple_r2) > 0 ? $key->cumple_r2 : 'Sin data') . '</td>';
		$html .= '</tr>';
	}
	
	return $html;
}

function proyAdjudicados($codubigeo){
	$html = '';
	$monto_inversion = 0;
	$proyAdjudicados = dropDownList((object) ['method' => 'proyAdjudicados', 'codubigeo' => $codubigeo]);
	
	if(count($proyAdjudicados) > 0){
		$html = '<pagebreak orientation="landscape" />';
		$html .= '<div>';
			$html .= '<p style="font-family: Arial; font-weight:bold; font-size:13px;">Lista de Proyectos Concluidos y Adjudicados 2009 - 2019 (Millones de Soles)</p>';
			$html .= '<table class="table" border="1" width="100%">';
				$html .= '<thead>';			
					$html .= '<tr>';
						$html .= '<th class="text-center">SNIP</th>';
						$html .= '<th class="text-center">Nombre del Proyecto</th>';
						$html .= '<th class="text-center">Empresa</th>';
						$html .= '<th class="text-center">Sector</th>';
						$html .= '<th class="text-center">F. Buena Pro</th>';
						$html .= '<th class="text-center">Monto de Inversión<sup>(1)</sup></th>';
						$html .= '<th class="text-center">F. Firma de Convenio</th>';
						$html .= '<th class="text-center">Población Beneficiada</th>';
						$html .= '<th class="text-center">Estado</th>';
					$html .= '<tr>';
				$html .= '</thead>';
				$html .= '<tbody>';	
					foreach ($proyAdjudicados as $key){
						$monto_inversion = $monto_inversion + $key->monto_inversion;						
						$html .= '<tr>';
							$html .= '<td valign="top" width="5px">'. $key->snip . '</td>';
							$html .= '<td valign="top" align="justify" width="250px">'. ucfirst($key->nombre_proyecto) . '</td>';
							$html .= '<td valign="top" width="140px">'. $key->empresa . '</td>';
							$html .= '<td valign="top" width="80px">'. $key->sector . '</td>';
							$html .= '<td valign="top" width="10px">'. $key->fecha_buena_pro . '</td>';
							$html .= '<td valign="top" align="right" width="80px">'. $key->monto_inversion . '</td>';
							$html .= '<td valign="top" width="10px">'. $key->fecha_firma . '</td>';
							$html .= '<td valign="top" align="right" width="10px">'. $key->poblacion_beneficiada . '</td>';
							$html .= '<td valign="top" width="10px">'. $key->estado . '</td>';
						$html .= '<tr>';
					}
					$html .= '<tr>';
						$html .= '<td colspan="5" align="right"><b>Total</b></td>';
						$html .= '<td align="right">' . $monto_inversion . '</td>';
					$html .= '<tr>';
				$html .= '</tbody>';
			$html .= '</table>';
			$html .= '<p style="font-size: 11px;">
				Fuente  y Elaboración: Dirección de Inversiones Descentralizas - ProInversión<br>
				(1) Al 25 de marzo de 2019
				</p>';
		$html .= '</div>';		
	}
	
	return $html;
}

function reglasFiscales($codnivel, $codubigeo){
	$html = '';
	$reglasFiscales = dropDownList((object) ['method' => 'reglasFiscales', 'codnivel' => $codnivel, 'codubigeo' => $codubigeo]);	
	
	foreach ($reglasFiscales as $key){
		$html .= '<tr>';		
			$html .= '<td class="text-center">'. $key->anio . '</td>';
			$html .= '<td class="text-right">'. number_format($key->deuda) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->ict) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->limite) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->espacio) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->sdt) . '%</td>';
			$html .= '<td class="text-right"></td>';
			$html .= '<td class="text-right">'. number_format($key->ingreso_corriente) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->gasto_corriente) . '</td>';
			$html .= '<td class="text-right">'. number_format($key->ahorro) . '</td>';
		$html .= '</tr>';
	}
	
	return $html;
}

function indXregion($idnivel, $codubigeo){
	$html = '';
	$indCab = dropDownList((object) ['method' => 'indCabXubigeo', 'idNivel' => $idnivel, 'codUbigeo' => $codubigeo]);
	
	foreach ($indCab as $key){
		$indDet = dropDownList((object) ['method' => 'indDetXubigeo', 'idNivel' => $idnivel, 'codUbigeo' => $codubigeo, 'idInd' => $key->idind]);
		
		$html .= '<div>';			
			$html .= '<table border="0" width="100%">';
				$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<td width="90%"><h3>' . str_replace('tic','TIC',ucfirst(strtolower($key->nomind))) . '</h3></td>';						
						$html .= '<td style="' . setBackgroundglobal2($key->ind_global) . '" class="text-center">' . $key->ind_global . '</td>';
					$html .= '</tr>';
				$html .= '</thead>';
			$html .= '</table>';
			$html .= '<br>';
			$html .= '<table class="table" border="1" width="100%">';
				$html .= '<thead>';
					$html .= '<tr>';
						$html .= '<th width="30%">Indicador</th>';
						$html .= '<th width="58%">Descripción</th>';
						$html .= '<th width="12%">Valor</th>';
					$html .= '</tr>';
				$html .= '</thead>';
				$html .= '<tbody>';
					foreach ($indDet as $key){
						$html .= '<tr>';
							$html .= '<td>' . ucfirst(strtolower($key->nomind)) . '</td>';
							$html .= '<td>' . ucfirst(strtolower($key->desind)) . '</td>';
							$html .= '<td class="text-right">' . number_format($key->valorind) . '</td>';
						$html .= '</tr>';
					}
				$html .= '</tbody>';
			$html .= '</table>';
		$html .= '</div>';
	}
	
	return $html;
}

function proyXfase($codnivel, $codubigeo){
	$i = 0;
	$html = '';
	$temp=0;$temp1=0;$temp2=0;$temp3=0;$temp4=0;$temp5=0;$sum=0;$sum1=0;$sum2=0;$sum3=0;$sum4=0;$sum5=0;
	$proyXfase = dropDownList((object) ['method' => 'funcionXUbigeo', 'codnivel' => $codnivel, 'codubigeo' => $codubigeo]);	
	
	foreach ($proyXfase as $key){
		$html .= '<tr>';
			$html .= '<td style="background-color: ' . getColor($i) .';"></td>';
			$html .= '<td>' . ucfirst(strtolower($key->nomfuncion)) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->formulacion) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->monto_formulacion) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->perfil_viable) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->monto_perfil) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->suma_total) . '</td>';
			$html .= '<td class="text-right">' . number_format($key->monto_total) . '</td>';	
		$html .= '</tr>';
		$i++;
		$sum+= $key->formulacion;
	    $sum1+= ($key->monto_formulacion);
		$sum2+= ($key->perfil_viable);
		$sum3+= ($key->monto_perfil); 
	    $sum4+= ($key->suma_total);
		$sum5+= ($key->monto_total);
	}
	$proyXfase2 = dropDownList((object) ['method' => 'funcionXUbigeoOtros', 'codnivel' => $codnivel, 'codubigeo' => $codubigeo]);
	foreach ($proyXfase2 as $key){
	
		$temp+= ($key->formulacion);
		$temp1+= ($key->monto_formulacion);
		$temp2+= ($key->perfil_viable);
		$temp3+= ($key->monto_perfil);
		$temp4+= ($key->suma_total);
		$temp5+= ($key->monto_total);
	}
	$html .= '<tr>';
		$html .= '<td></td>';
		$html .='<td>Otros</td>';										
		$html .='<td class="text-right">' . number_format($temp) . '</td>';										
		$html .='<td class="text-right">' . number_format($temp1) . '</td>';																	
		$html .='<td class="text-right">' . number_format($temp2) . '</td>';																	
		$html .='<td class="text-right">' . number_format($temp3) . '</td>';																	
		$html .='<td class="text-right">' . number_format($temp4) . '</td>';																	
		$html .='<td class="text-right">' . number_format($temp5) . '</td>';
	$html .= '</tr>';
	$html .= '<tr>';
		$html .= '<td></td>';
		$html .= '<td><strong>Total</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum + $temp) . '</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum1 + $temp1) . '</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum2 + $temp2) . '</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum3 + $temp3) . '</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum4 + $temp4) . '</strong></td>';
		$html .= '<td class="text-right"><strong>' . number_format($sum5 + $temp5) . '</strong></td>';
	$html .= '</tr>';
	
	return $html;
}

function funcionEmpresasXUbigeo($codnivel, $codubigeo){
	$tmp=0;
	$html = '';
	$proyXfase = dropDownList((object) ['method' => 'funcionEmpresasXUbigeo', 'codnivel' => $codnivel, 'codUbigeo' => $codubigeo]);	
	
	foreach ($proyXfase as $key){
		$tmp++;
		$html .= '<tr>';
			$html .= '<td class="text-center">' . $key->ranking . '</td>';		
			$html .= '<td class="text-center">' . $key->rucempresa . '</td>';
			$html .= '<td class="text-left">' . $key->nomempresa . '</td>';
			$html .= '<td class="text-right">' . number_format($key->disponibleoxi) . '</td>';		
		$html .= '</tr>';
	}
	return $html;
}

function congresistas($codubigeo){
	$tmp=0;
	$html = '';
	$congresistas = dropDownList((object) ['method' => 'congresistasXRegion', 'codUbigeo' => $codubigeo]);
	$i=0;
	foreach ($congresistas as $key){
		$i++;
		$html .= '<tr>';
			$html .= '<td class="text-center">'. $i . '</td>';
			$html .= '<td class="text-center">'. $key->apellidos . '</td>';
			$html .= '<td class="text-left">'. $key->parido . '</td>';
			$html .= '<td class="text-left">'. $key->bancada . '</td>';
			
		$html .= '</tr>';
	}
	
	return $html;
}



function getColor($id){
	$color = 'transparent';
	$colors  = array('#0A3C64','#1D71B3','#30ABED','#75C6F4','#D5EBFF');
	
	if(isset($colors[$id])) $color = $colors[$id];
	
	return $color;
}

function chart_bar($idnivel, $codubigeo, $coddpto){
	$data = dropDownList((object) ['method' => 'ciprlXregion', 'idnivel' => $idnivel, 'codubigeo' => $codubigeo, 'coddpto' => $coddpto]);	
	$l1datay = $data->series[1]->data;
	$l2datay = $data->series[0]->data;
	
	$datax   = array('2014','2015','2016','2017','2018');
	 
	// Create the graph. 
	$graph = new Graph(580,300,'auto');    
	$graph->SetScale('textlin');
	$graph->SetFrame(false);
	 
	$graph->img->SetMargin(40,20,46,80);
	$graph->SetShadow();
	 
	// Create the linear error plot
	$l1plot=new LinePlot($l1datay);
	$l1plot->SetBarCenter();
	$l1plot->SetColor('red');
	$l1plot->SetWeight(2);
	$l1plot->SetLegend('Ciprl MEF');
	$l1plot->mark->SetType(MARK_X,'',1.0);
	 
	// Create the bar plot
	$bplot = new BarPlot($l2datay);
	$bplot->SetFillColor('orange');
	$bplot->SetLegend('Canon MEF');
	 
	// Add the plots to t'he graph
	$graph->Add($bplot);
	$graph->Add($l1plot);
	 
	$graph->title->Set('Evolución de Canon y Tope CIPRL');
	$graph->title->SetFont(FF_ARIAL,FS_BOLD,12);
	 
	$graph->xaxis->SetTickLabels($datax);

	$graph->legend->SetFrameWeight(1);
	$graph->legend->SetColumns(6);
	$graph->legend->SetColor('#4E4E4E','#4e4e4e');
	$graph->legend->SetPos(0.5,0.95,'center','bottom');
	 
	// Display the graph
	$img = $graph->Stroke(_IMG_HANDLER);
	ob_start();
	imagepng($img);
	$img_data = ob_get_contents();
	ob_end_clean();
	
	return '<img src="data:image/png;base64,'.base64_encode($img_data).'" style="margin-top:10px;"/>';	
}

function chart_pie($codnivel, $codfase, $codubigeo, $column){	
	$data = dropDownList((object) ['method' => 'funcionXUbigeoChart', 'codnivel' => $codnivel, 'codubigeo' => $codubigeo]);
		
	if(count($data) > 0){
		array_pop($data);
		$data = array_map('intval', array_column($data, $column));
	}
	
	if(array_sum($data) > 0){
		// Create the Pie Graph. 
		$graph = new PieGraph(200,143);

		$theme_class= new VividTheme;
		$graph->SetTheme($theme_class);

		// Create
		$p1 = new PiePlot($data);
		$graph->Add($p1);

		$p1->ShowBorder();
		$p1->SetColor('red');
		$p1->ExplodeSlice(1);
		$p1->value->Show(false);
		$p1->SetSliceColors(array('#0A3C64','#1D71B3','#30ABED','#75C6F4','#D5EBFF'));
		
		// Display the graph
		$img = $graph->Stroke(_IMG_HANDLER);
		ob_start();
		imagepng($img);
		$img_data = ob_get_contents();
		ob_end_clean();
		
		return '<img src="data:image/png;base64,'.base64_encode($img_data).'"/>';
	}
}