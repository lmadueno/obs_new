﻿<?php
	require 'php/app.php';
	$data   = json_decode($_GET['data']);
	$provimcia='';
	if($data->id==1){
		$provimcia="('080801','040504','080803','080708','080802','040517','080804','080701','030506','080701','080706','080707','030504','030708','040519','040514','040504','040104','040103','040128','040519','040514','040504','040104','040103','040128','040704','040125','040701','040108','040124','040128')";
	}else if($data->id==3){
		$provimcia="('150722','150714','150118','150103','150111','150107','150727','120808','150717','150132','150732','150728','150701','120810','150705','120801','120805','070103','070101','150135','150128','150101')";
	}
	else if($data->id==4){
		$provimcia="('060108','060101','060105','061201','061204','061202','130703','130705','130702','060502','060507','060508','130401','130704','061203')";
	}
	else if($data->id==5){
		//$provimcia="('1901','1506','1501','0701')";
		$provimcia="('070106','150102','070101','070105','150139','150102','150604','150601','150611','150603','150607','150609','150612','150602','150608','150610','190104','190112','190111','190101')";
	}
	else if($data->id==2){
		//$provimcia="('1901','1506','1501','0701')";
		$provimcia="('020802','021807','021809','020105','020103','020804','020106','020111','020611','020110','020112','020803','020801','021806','021801','020109','020101')";
	}

	
	$provXrutas = dropDownList((object) ['method' => 'provXrutas2','provincia'=>$provimcia]);	
	
	if(count((array)$provXrutas) > 0){				
?>
<style>
	.bold{
		font-weight: bold;
		background: #1976d226;
		color: #000;
	}
	.table thead th {
		vertical-align: bottom;
		border-bottom: 2px solid #fefeff;
	}
</style>
<div class="card card-outline-info">
	<div class="card-header">
		<div class="row">
			<div class="col-lg-11">
				<h6 class="m-b-0 text-white"><?php echo $data->descripcion?> </h6>				
			</div>
			<div class="col-lg-1">
				
				<a href=""  title="Expander" class="expand_" onclick="App.events(this); return false;" data-event='<?php echo ($data->id)?>'><i class="fas fa-expand" style="color:white"  aria-hidden="true"></i></a>				
			</div>
		</div>
	</div>	
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header card-special text-center font-weight-bold">
						Distribución de canon, ciprl y pia por vías
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
								<div id="chart_provXrutas" style="min-width: 310px; height: 320px; margin: 0 auto;display:none;"></div>	
							</div>
						</div>														
					</div>
					<div class="card-footer text-center">
						<a class="lnkAmpliar" data-event="lnkProvXrutas" href="#" onclick="App.events(this); return false;">
							Mostrar/Ocultar tabla resumen
						</a>
					</div>
					<div data-target="lnkProvXrutas" class="card-body">
						<div class="row">
							<div class="col-lg-12">
								<table class="table table-sm table-detail" width="100%">
									<thead>
										<tr>
											<th class="text-center bold" width="9%">Vía</th>
											<th class="text-center bold" width="55%">Nombre</th>
											<th class="text-center bold" width="7%">Distritos</th>
											<th class="text-center bold" width="7%">Población</th>
											<th class="text-center bold" width="7%">Canon</th>
											<th class="text-center bold" width="7%">CIPRL</th>
											<th class="text-center bold" width="8%">PIA</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$poblacion  = 0;
											$distritos  = 0;
											$canon      = 0;
											$ciprl      = 0;
											$pia        = 0;
											
											foreach ($provXrutas as $item){
												$poblacion  = $poblacion + $item->poblacion;
												$distritos  = $distritos + $item->ndistritos;
												$canon      = $canon + $item->canon;
												$ciprl      = $ciprl + $item->ciprl;
												$pia        = $pia + $item->pia;
										?>											
											<tr>
												<td><?php echo $item->ccodruta;?></td>
												<td><input type="text" style="width: 100%" value="<?php echo ucwords(strtolower($item->cnomruta))?>" readonly /></td>
												<td class="text-right">
													<a class="lnkAmpliar"  data-event="lnkProvXcorredor_<?php echo $item->id?>" href="#" onclick="App.events(this); return false;">
														<?php echo $item->ndistritos;?>
													</a>													
												</td>
												<td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->poblacion/1000, 1)?>K</td>												
												<td class="text-right" title="<?php echo number_format($item->canon)?>"><?php echo round($item->canon/1000000, 1)?>M</td>
												<td class="text-right" title="<?php echo number_format($item->ciprl)?>"><?php echo round($item->ciprl/1000000, 1)?>M</td>
												<td class="text-right" title="<?php echo number_format($item->pia)?>"><?php echo round($item->pia/1000000, 1)?>M</td>						
											</tr>
											<tr data-target="lnkProvXcorredor_<?php echo $item->id?>" style="display: none;">
												<td colspan="8">
													<div class="card">
														<div class="card-header card-special">
															Distritos
														</div>
														<div class="card-body">
															<div class="row">
																<div class="col-lg-12">
																	<table class="table table-sm table-detail">
																		<thead>
																			<tr>
																				<th class="text-center bold" width="15%">Distrito</th>
																				<th class="text-center bold" width="35%">Partido Político</th>
																				<th class="text-center bold" width="10%">Población</th>
																				<th class="text-center bold" width="10%">Electores</th>
																				<th class="text-center bold" width="10%">Canon</th>
																				<th class="text-center bold" width="10%">CIPRL</th>
																				<th class="text-center bold" width="10%">PIA</th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php
																				$provXrutasDet = dropDownList((object) ['method' => 'provXrutasDet2', 'ccodruta' => $item->ccodruta,'provincia' => $provimcia]);

																				$poblaciond = 0;
																				$electoresd = 0;
																				$canond     = 0;
																				$ciprld     = 0;
																				$piad       = 0;
																				foreach ($provXrutasDet as $item){																					
																					$poblaciond  = $poblaciond + $item->poblacion;
																					$electoresd  = $electoresd + $item->electores;
																					$canond = $canond + $item->canon;
																					$ciprld = $ciprld + $item->ciprl;
																					$piad   = $piad + $item->pia;
																			?>
																			<tr>
																				<td><input type="text" style="width: 100%" value="<?php echo ucwords(strtolower($item->distrito))?>" readonly /></td>
																				<td><input type="text" style="width: 100%" value="<?php echo ucwords(strtolower($item->partido))?>" readonly /></td>							
																				<td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->poblacion/1000, 1)?>K</td>
																				<td class="text-right" title="<?php echo number_format($item->electores)?>"><?php echo round($item->electores/1000, 1)?>K</td>
																				<td class="text-right" title="<?php echo number_format($item->canon)?>"><?php echo round($item->canon/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($item->ciprl)?>"><?php echo round($item->ciprl/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($item->pia)?>"><?php echo round($item->pia/1000000, 1)?>M</td>						
																			</tr>
																			<?php } ?>
																			<tr>
																				<td></td>
																				<td><b>Total</b></td>							
																				<td class="text-right" title="<?php echo number_format($poblaciond)?>"><?php echo round($poblaciond/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($electoresd)?>"><?php echo round($electoresd/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($canond)?>"><?php echo round($canond/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($ciprld)?>"><?php echo round($ciprld/1000000, 1)?>M</td>
																				<td class="text-right" title="<?php echo number_format($piad)?>"><?php echo round($piad/1000000, 1)?>M</td>						
																			</tr>
																		</tbody>
																	</table>
																</div>
															</div>														
														</div>
													</div>													
												</td>
											</tr>
										<?php } ?>
										<tr>
											<td></td>
											<td><b>Total</b></td>												
											<td class="text-right"><?php echo $distritos?></td>
											<td class="text-right" title="<?php echo number_format($poblacion)?>"><?php echo round($poblacion/1000000, 1)?>M</td>
											<td class="text-right" title="<?php echo number_format($canon)?>"><?php echo round($canon/1000000, 1)?>M</td>
											<td class="text-right" title="<?php echo number_format($ciprl)?>"><?php echo round($ciprl/1000000, 1)?>M</td>
											<td class="text-right" title="<?php echo number_format($pia)?>"><?php echo round($pia/1000000, 1)?>M</td>												
											<td></td>								
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>									
			</div>
		</div>
		<?php 
			$ciaMineras= dropDownList((object) ['method' => 'unidadesMinerasDet', 'corredor' =>  $data->id]);
			foreach ($ciaMineras as $data2){	?>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Unidades Mineras de la <a href="" class="openModalDialogEmpresas" onclick="App.events(this); return false;" data-event='<?php echo ($data2->ruc)?>'><?php echo $data2->descripcion?></a></td>
						</div>
						<div class="card-body">
							<div class="col-lg-12">
							
								<table class="table table-sm table-detail">
									<tr>
                                        <th class="text-center bold" style="vertical-align:middle" width="20%">Nombre</th>
                                        <th class="text-center bold" style="vertical-align:middle">Región</th>
                                        <th class="text-center bold" style="vertical-align:middle">Provincia</th>
										<th class="text-center bold" style="vertical-align:middle">Distrito</th>
										<th class="text-center bold" style="vertical-align:middle">Producción</th>
										<th class="text-center bold" style="vertical-align:middle">Altitud</th>
										<th class="text-center bold" style="vertical-align:middle">Capacidad</th>
									</tr>
									<?php $ciaMinerasEmpresas= dropDownList((object) ['method' => 'unidadesMinerasxEmpresa', 'corredor' =>  $data->id, 'empresa' =>  $data2->id]);
							foreach ($ciaMinerasEmpresas as $data1){	?>
									<tr>
                                        <td class="text-left"><?php echo $data1->nombre?></td>
										<td class="text-left"><?php echo $data1->departamento?></td>
										<td class="text-left"><?php echo $data1->provincia?></td>
                                        <td class="text-left"><a href="" class="openModalDialogCIPRL" onclick="App.events(this); return false;" data-event='<?php echo ($data1->ubigeo)?>'><?php echo $data1->distrito?></a></td>
										<td class="text-left"><?php echo $data1->produccion?></td>
										<td class="text-right"><?php echo $data1->altitud?> msnm.</td>
										<td class="text-left"><?php echo $data1->capacidad?> tpd</td>
									</tr>
										<?php } ?>
								</table>  
						
							</div>
						</div>	
					</div>
				</div>
			</div>		
		<?php } ?>

	</div>
</div>
<?php
}
?>
