<?php
	require 'php/app.php';		

			$data = json_decode($_GET['data']);	
		
?>
<?php
			
			
			if($data->tipo=="hospitales"){
		
?>
												<div class="card">
													<div class="card-header card-special">
															Centros de Salud
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail">
																	<tr>
																		<th class="text-center bold" >Nombre</th>
																		<th class="text-center bold" >Tipo</th>
																		<th class="text-center bold" >Clasificacion</th>
																		<th class="text-center bold" >Categoria</th>
																	</tr>
																	<?php	
																		$oleoducto = dropDownList((object) ['method' => 'objetosInfraHospital','ubigeo' => $data->ubigeo]);	
																		foreach ($oleoducto as $item){	
																	?>
																	<tr>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->nombre))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->tipo))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->clasificacion))?></td>
																		<td class="text-left"><?php echo  ($item->categoria)?></td>
																	</tr>

																	<?php	
																		}
																	?>	
																</table>
															</div>
														</div>
													</div>
												</div>
	<?php	
		}	else if($data->tipo=="comisarias"){	
    ?>									
										
												<div class="card">
													<div class="card-header card-special">
															Comisarias
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail">
																	<tr>
																		<th class="text-center bold" >Comisaria</th>
																		<th class="text-center bold" >División Policial</th>
																		<th class="text-center bold" >Región Policial</th>
																		
																	</tr>
																	<?php	
																		$oleoducto = dropDownList((object) ['method' => 'objetosInfraComisarias','ubigeo' => $data->ubigeo]);	
																		foreach ($oleoducto as $item){	
																	?>
																	<tr>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->comisaria))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->divpol))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->regpol))?></td>
																	</tr>

																	<?php	
																		}
																	?>	
																</table>
															</div>
														</div>
													</div>
												</div>
<?php	
}	else if($data->tipo=="colegios"){	
?>
												<div class="card">
													<div class="card-header card-special">
															Colegios
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail">
																	<tr>
																		<th class="text-center bold" >Nombre</th>
																		<th class="text-center bold" >Ugel</th>
																		<th class="text-center bold" >Nivel</th>
																		<th class="text-center bold" >Direccion</th>
																	</tr>
																	<?php	
																		$oleoducto = dropDownList((object) ['method' => 'objetosInfraColegios','ubigeo' => $data->ubigeo]);	
																		foreach ($oleoducto as $item){	
																	?>
																	<tr>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->cen_edu))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->dre_ugel))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->niv_mod))?></td>
																		<td class="text-left"><?php echo  ucwords(strtolower($item->dir_cen))?></td>
																	</tr>

																	<?php	
																		}
																	?>	
																</table>
															</div>
														</div>
													</div>
												</div>	

<?php
}
?>

                                            	
											
											
												
												

											