<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Nivel Gobierno</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Costo Total</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Avance Ejecución</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Monto Pendiente</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'macro_mef_sub','region'=>$data->region]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->cantidad ;	
										$s2019=   $s2019+$item->total ;	
										$s2020=   $s2020+$item->diferencia ;	
										$stotal=   $stotal+$item->resto ;												
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" >
											<a class="lnkAmpliar_mef_nivel" data-event="lnkProvXrutas_<?php echo $data->region.'$'.$item->nivel_g?>" id="<?php echo $data->region.'$'.$item->nivel_g?>" href="#"  onclick="App.events(this);">
												<?php echo ($item->nivel_g)?>
											</a>
										</td>												
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->total)?></td>		
										<td class="text-center" ><?php echo number_format($item->diferencia)?></td>		
										<td class="text-center" ><?php echo number_format($item->resto)?></td>	
                                    </tr>
									<tr data-target="lnkProvXrutas_<?php echo $data->region.'$'.$item->nivel_g?>" style="display: none;">
										<td colspan="8">
											<div class="card">
												<div class="card-header">
													Entidades
												</div>
												<div class="card-body">
													<div id="div_<?php echo $data->region.'$'.$item->nivel_g?>"></div>
												</div>
											</div>
											
										</td>
									</tr>
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
                                        <td class="text-center" ><?php echo number_format($s2018)?></td>												
                                        <td class="text-center"><?php echo number_format($s2019)?></td>
                                        <td class="text-center"><?php echo number_format($s2020)?></td>
										<td class="text-center"><?php echo number_format($stotal)?></td>

                                    </tr>
								</tbody>
							</table>