
<?php
	require 'php/app.php';
    $data   = json_decode($_GET['data']);
?>
<table class="table table-sm table-detail">
    <thead>										
		<tr>
				                                    <th class="text-center bold" rowspan="2" style="vertical-align:middle">División Funcional</th>
                                                    <th class="text-center bold" colspan="2" width="30%">Formulación</th>
                                                    <th></th>
                                                    <th class="text-center bold" colspan="2" width="30%">Evaluación</th>
                                                    <th></th>
                                                    <th class="text-center bold" colspan="2" width="30%">Perfil Viable</th>
                                                    <th></th>
                                                    <th class="text-center bold" colspan="2" width="30%">Total</th>
				                                </tr>	
                                                <tr>
                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                    <th></th>
                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                    <th></th>
                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                    <th></th>
                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
        </tr>	
    </thead>  
    <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'pipsObjetosPrograma','nivelgobierno'=>$data->nivel, 'funcion' => $data->funcion,'distritos'=>$data->distritos]);?>
    <?php foreach ($pipxUbigeoDet as $key1){ ?>
        <tr>
                                                    <td class="text-left">
                                                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $data->funcion.$data->nivel.$key1->_funcion?>" href="#" onclick="App.events(this); return false;">
                                                            <?php echo ucfirst(strtolower($key1->_funcion))?>
                                                        </a>			                                                    
                                                    </td>														                                                       
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_canformulacion)?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_sumformulacion)?>
                                                    </td>
                                                    <td></td> 
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_canevaluacion)?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_sumevaluacion)?>
                                                    </td>
                                                    <td></td> 
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_canperfil)?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_sumperfil)?>
                                                    </td>
                                                    <td></td> 
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_canperfil+$key1->_canformulacion+$key1->_canevaluacion)?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php echo number_format($key1->_sumperfil+$key1->_sumformulacion+$key1->_canformulacion)?>
                                                    </td>                         
                                                </tr>
                                                <tr data-target="lnkProvXrutas_<?php echo $data->funcion.$data->nivel.$key1->_funcion?>" style="display: none;">
                                                    <td colspan="12">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
                                                                        <table class="table table-sm table-detail">
                                                                            <thead>										
                                                                                <tr>
                                                                                    <th class="text-center bold" rowspan="2" style="vertical-align:middle">Grupo Funcional</th>
                                                                                    <th class="text-center bold" colspan="2" width="30%">Formulación</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" colspan="2" width="30%">Evaluación</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" colspan="2" width="30%">Perfil Viable</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" colspan="2" width="30%">Total</th>
                                                                                </tr>	
                                                                                <tr>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                                                    <th></th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Cant</th>
                                                                                    <th class="text-center bold" style="vertical-align:middle">Monto S/</th>
                                                                                </tr>	
                                                                            </thead>  
                                                                            <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'pipsObjetossubPrograma','nivelgobierno'=>$data->nivel, 'funcion'=> $data->funcion,'programa' => $key1->_codfuncion,'distritos'=>$data->distritos]);?>
                                                                            <?php 
                                                                                                                    
                                                                                foreach ($pipxUbigeoDet as $key2){ 
                                                                                                                        
                                                                            ?>
                                                                                <tr>
                                                                                    <td class="text-left">
                                                                                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $data->funcion.$data->nivel.$key2->_funcion?>" href="#" onclick="App.events(this); return false;">
                                                                                            <?php echo ucfirst(strtolower($key2->_funcion))?>
                                                                                        </a>			                                                    
                                                                                    </td>														                                                       
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_canformulacion)?>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_sumformulacion)?>
                                                                                    </td>
                                                                                    <td></td> 
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_canevaluacion)?>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_sumevaluacion)?>
                                                                                    </td>
                                                                                    <td></td> 
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_canperfil)?>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_sumperfil)?>
                                                                                    </td>
                                                                                    <td></td> 
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_canperfil+$key2->_canformulacion+$key2->_canevaluacion)?>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <?php echo number_format($key2->_sumperfil+$key2->_sumformulacion+$key2->_canformulacion)?>
                                                                                    </td>                         
                                                                                </tr>
                                                                                <tr data-target="lnkProvXrutas_<?php echo $data->funcion.$data->nivel.$key2->_funcion?>" style="display: none;">
                                                                                    <td colspan="12">
                                                                                        <div class="card">
                                                                                            <div class="card-body">
                                                                                                <div class="row">
                                                                                                    <div class="col-lg-12">
                                                                                                        <table class="table table-sm table-detail">
                                                                                                            <tr>
                                                                                                                <th class="text-center bold" style="vertical-align:middle">Codigo</th>
                                                                                                                <th class="text-center bold" style="vertical-align:middle">Tipo</th>
                                                                                                                <th class="text-center bold" style="vertical-align:middle">Fase</th>
                                                                                                                <th class="text-center bold" style="vertical-align:middle">Fecha</th>
                                                                                                                <th class="text-center bold" style="vertical-align:middle">Monto</th>
                                                                                                            </tr>
                                                                                                            <?php $pipxUbigeoDet1 = dropDownList((object) ['method' => 'pipsObjetosCodUnico','nivel'=>$data->nivel, 'funcion'=> $data->funcion,'programa' => $key1->_codfuncion,'subprograma'=>$key2->_codfuncion,'distritos'=>$data->distritos]);?>
                                                                                                            <?php 
                                                                                                                $temporalMonto=0;
                                                                                                                foreach ($pipxUbigeoDet1 as $key3){ 
                                                                                                            ?>
                                                                                                            <tr>
                                                                                                                <td class="text-center"><a href="" class="link_" onclick="App.events(this); return false;" data-event='<?php echo ucfirst($key3->codunico)?>'><?php echo ucfirst(strtolower($key3->codunico))?></a></td>                                          
                                                                                                                <td class="text-center"><?php echo ucfirst(strtolower($key3->tipo))?></td>
                                                                                                                <td class="text-center"><?php echo ucfirst(strtolower($key3->fase))?></td>
                                                                                                                <td class="text-center"><?php echo ($key3->fecregistro)?></td>
                                                                                                                <td class="text-center"><?php echo number_format($key3->monto)?></td>
                                                                                                            </tr>
                                                                                                            <?php } ?>

                                                                                                        </table>
                                                                                                    </div>
                                                                                                </div>	
                                                                                            </div>
                                                                                        </div>
                                                                                    </td>                                        
                                                                                </tr>    
                                                                            <?php } ?> 
                                                                        </table>   
                                                                    </div>
                                                                </div>
                                                            </div>
                                                         </div>
                                                    </td>

    <?php } ?>    
</table>