<?php

			$data = json_decode($_GET['data']);	
			$codproyecto = $data->codproyecto;
?>
<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
	<div class="card card-outline-info">
		<div class="card-header estilodashboard">
			<div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white">INFORMACIÓN DEL PROYECTO</h6>				
                </div>
                <div class="col-lg-1 estilodashboard1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarEntidad" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarEntidad" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
		</div>
		<div class="card-body">				
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group not-bottom">
						<label>Organización</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->organizacion;?>" title="<?php echo $data->organizacion;?>" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>PIA</label>
						<input type="text" class="form-control form-control-sm text-right" value="<?php echo number_format($data->pia);?>" readonly>
					</div>
				</div>
				<div class="col-lg-3" style="display: none;">
					<div class="form-groupnot-bottom">
						<label>CANON</label>
						<input type="text" class="form-control form-control-sm" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>TOPE CIPRL</label>
						<input type="text" class="form-control form-control-sm text-right" value="<?php echo number_format($data->tope_ciprl);?>" readonly>
					</div>
				</div>
			</div>
							
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group not-bottom">
						<label>Oportunidad</label>
						<textarea class="form-control" cols="105" rows="3" readonly><?php echo $data->oportunidad;?></textarea>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Función</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->funcion;?>" title="<?php echo $data->funcion;?>" readonly>
					</div>
				</div>
				<div class="col-lg-4">
						<div class="form-group not-bottom">
							<label>División Funcional</label>
							<input type="text" class="form-control form-control-sm" value="<?php echo $data->programaproyecto;?>" title="<?php echo $data->programaproyecto;?>" readonly>
						</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Grupo Funcional</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->programa;?>" title="<?php echo $data->programa;?>" readonly>
					</div>
				</div>
				
			</div>
			<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group not-bottom">
							<label>Monto asignado</label>
							<input type="text" id="txtMonto_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="form-control form-control-sm text-right" value="<?php echo number_format($data->monto);?>" readonly>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="form-group not-bottom">
							<label>Monto Actualizado</label>
							<input type="text" class="form-control form-control-sm text-right" value="<?php echo number_format($data->montoactualizado);?>" readonly>
						</div>
					</div>
					
			</div>	
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>SNIP</label>
						<a target='_blank' class="form-control form-control-sm" style="border: 0;text-decoration: underline;" href='https://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $data->codigosnip;?>'><?php echo $data->codigosnip;?></a>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->codigosnip;?>" readonly style="display: none;">
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Código Único</label>
						<a target='_blank' class="form-control form-control-sm" style="border: 0;text-decoration: underline;" href='https://ofi5.mef.gob.pe/invierte/ejecucion/traeListaEjecucionSimplePublica/<?php echo $data->codigounico;?>'><?php echo $data->codigounico;?></a>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->codigounico;?>" readonly style="display: none;">
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Estado</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->estado;?>" readonly>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Origen</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->origen;?>" readonly>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Región</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->region;?>" readonly>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Fase</label>							
						<input type="text" id="txtFase_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="form-control form-control-sm" value="<?php echo $data->fasegobierno;?>" readonly>
						
					</div>						
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>Nivel</label>
						<input type="text" id="txtNivel_<?php echo $codproyecto;?>" class="form-control form-control-sm" value="<?php echo $data->nivelgobierno;?>" readonly>
					
					</div>
				</div>
				<div class="col-lg-3">
					<div class="form-group not-bottom">
						<label>F. Registro</label>
						<input type="text" class="form-control form-control-sm" value="<?php echo $data->fecregistro2;?>" readonly>
					</div>
				</div>
			</div>	
			<div class="row">
				<?php 

					if ($data->en_ejecucion == 'SI'){ ?>
					<div class="col-lg-12">
						<div class="form-group not-bottom">
						<label>En Ejecucion</label>
						<input type="text" class="form-control form-control-sm text-right" value="<?php 
						
						echo ($data->en_ejecucion);?>" readonly>
						</div>
					</div>
							
				<?php }?>
			</div>
		</div>
		
		<div class="card-footer">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="button-group">
						<button type="button" id="btnGuardar_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="btn btn-warning btn-sm cs-guardar" style="display: none;" onclick="App.events(this)">
							Guardar&nbsp;<i class="fa fa-save"></i>
						</button>
						
						<button type="button" id="btnCancelar_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="btn btn-danger btn-sm cs-cancelar" style="display: none;" onclick="App.events(this)">
							Cancelar&nbsp;<i class="fa fa-times"></i>
						</button>
						
						<button type="button" id="btnActualizar_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="btn btn-success btn-sm cs-actualizar" onclick="App.events(this)">
							Actualizar datos&nbsp;<i class="fa fa-edit"></i>
						</button>	
						
						<button type="button" id="btnAnular_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="btn btn-danger btn-sm cs-anular-seg" style="<?php echo $data->flag_seguimiento == 1 ? '' : 'display: none;' ?>" onclick="App.events(this)">
							Dejar de seguir&nbsp;<i class="fa fa-check"></i>
						</button>						
						
						<button type="button" id="btnSeguir_<?php echo $codproyecto;?>" data-id="<?php echo $codproyecto;?>" class="btn btn-info btn-sm cs-seg" style="<?php echo $data->flag_seguimiento == 1 ? 'display: none;' : ''; ?>" onclick="App.events(this)">
							Enviar a seguimiento&nbsp;&nbsp;&nbsp;<i class="fa fa-check"></i>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
