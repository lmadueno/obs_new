
<div class="card card-outline-info">
		<div class="card-header estilodashboard">
		    <div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white">Titulo</h6>				
                </div>
                <div class="col-lg-1 estilodashboard1" style="text-align:right !important"></div>
            </div>
        </div>
		<div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-outline-info">
                            <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                            Titulo 1
                            </div> 
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12"><div id="ml_char1"></div></div>
                                    <div class="col-lg-12"><div id="ml_char2"></div></div>
                                    <div class="col-lg-12"><div id="ml_char3"></div></div>
                                    <div class="col-lg-12"><div id="ml_html1"></div></div>
                                    <div class="col-lg-12"><div id="ml_html2"></div></div>
                                    <div class="col-lg-12"><div id="ml_html3"></div></div>
                                </div>
                            </div>                    
                    </div>
                </div>
            </div>
        </div>
</div>