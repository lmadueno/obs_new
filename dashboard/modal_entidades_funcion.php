<?php
    require '../php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<?php
   if( $data->tipo=='funcion'){
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
								<tr> 
									<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
									<th class="text-center bold" style="background:#ddebf8;text-align:center">Funcion</th>	
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto</th>
                                </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s1=0;
									$s2=0;

                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'modal_funcion_entidades','nivel'=>$data->nivel,'ubigeo'=>$data->ubigeo]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
		
										$s1=   $s1+$item->cantidad ;	
                                        $s2=   $s2+$item->monto ;	
																				
                                ?>	
									<tr>
										<td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" > <button class="btn btn-link lnkprogramaProyectosModal" id="<?php echo  ($item->nomfuncionproyecto.'-'.$data->nivel.'-'.$data->ubigeo)?>"  data-event="lnkProvXrutas_<?php echo ($item->nomfuncionproyecto.'-'.$data->nivel.'-'.$data->ubigeo)?>"  onclick="App.events(this);"  > <?php echo ($item->nomfuncionproyecto)?></button></td>
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
										<td class="text-center" ><?php echo number_format($item->monto)?></td>	
                                    </tr>
                                    <tr data-target="lnkProvXrutas_<?php echo ($item->nomfuncionproyecto.'-'.$data->nivel.'-'.$data->ubigeo)?>" style="display: none;">
										<td colspan="4">
											<div class="card">
												<div class="card-header">
													Division Funcional
												</div>
												<div class="card-body">
													<div id="div_<?php echo  ($item->nomfuncionproyecto.'-'.$data->nivel.'-'.$data->ubigeo)?>"></div>
												</div>
											</div>
											
										</td>
									</tr>     
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
	
										<td class="text-center" ><?php echo number_format($s1)?></td>		
										<td class="text-center" ><?php echo number_format($s2)?></td>	
									

                                    </tr>
								</tbody>
</table>
<?php
   }else if( $data->tipo=='programa'){
?>
    <table class="table table-sm table-detail" width="100%">
								<thead>
								<tr> 
									<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
									<th class="text-center bold" style="background:#ddebf8;text-align:center">División Funcional</th>	
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto</th>
                                </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s1=0;
									$s2=0;

                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'modal_programa_entidades_perfil','nivel'=>$data->nivel,'ubigeo'=>$data->ubigeo,'funcion'=>$data->funcion]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
		
										$s1=   $s1+$item->cantidad ;	
                                        $s2=   $s2+$item->monto ;	
																				
                                ?>	
									<tr>
										<td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" > <button class="btn btn-link lnksubprogramaProyectosModal" id="<?php echo ($item->nomprogramaproyecto.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>" onclick="App.events(this);" data-event="lnkProvXrutas_<?php echo ($item->nomprogramaproyecto.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>" > <?php echo ($item->nomprogramaproyecto)?></button></td>
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
										<td class="text-center" ><?php echo number_format($item->monto)?></td>	

                                    </tr>
                                    <tr data-target="lnkProvXrutas_<?php echo ($item->nomprogramaproyecto.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>" style="display: none;">
										<td colspan="4">
											<div class="card">
												<div class="card-header">
                                                    Grupo Funcional
												</div>
												<div class="card-body">
													<div id="div_<?php echo  ($item->nomprogramaproyecto.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>"></div>
												</div>
											</div>
											
										</td>
									</tr> 
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
	
										<td class="text-center" ><?php echo number_format($s1)?></td>		
										<td class="text-center" ><?php echo number_format($s2)?></td>	
									

                                    </tr>
								</tbody>
</table>


<?php
   }else if( $data->tipo=='subprograma'){
?>
    <table class="table table-sm table-detail" width="100%">
								<thead>
								<tr> 
									<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
									<th class="text-center bold" style="background:#ddebf8;text-align:center">Grupo Funcional</th>	
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto</th>
                                </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s1=0;
									$s2=0;

                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'modal_subprograma_entidades_perfil','nivel'=>$data->nivel,'ubigeo'=>$data->ubigeo,'funcion'=>$data->funcion,'programa'=>$data->programa]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
		
										$s1=   $s1+$item->cantidad ;	
                                        $s2=   $s2+$item->monto ;	
																				
                                ?>	
									<tr>
										<td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" > <button class="btn btn-link lnkcui_entidades_perfil" id="<?php echo ($item->nomsubprograma.'-'.$data->programa.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>" onclick="App.events(this);"  data-event="lnkProvXrutas_<?php echo ($item->nomsubprograma.'-'.$data->programa.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>"> <?php echo ($item->nomsubprograma)?></button></td>
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
										<td class="text-center" ><?php echo number_format($item->monto)?></td>	

                                    </tr>
                                    <tr data-target="lnkProvXrutas_<?php echo ($item->nomsubprograma.'-'.$data->programa.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>" style="display: none;">
										<td colspan="4">
											<div class="card">
												<div class="card-header">
                                                    Proyectos
												</div>
												<div class="card-body">
													<div id="div_<?php echo  ($item->nomsubprograma.'-'.$data->programa.'-'.$data->funcion.'-'.$data->nivel.'-'.$data->ubigeo)?>"></div>
												</div>
											</div>
											
										</td>
									</tr> 
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
	
										<td class="text-center" ><?php echo number_format($s1)?></td>		
										<td class="text-center" ><?php echo number_format($s2)?></td>	
									

                                    </tr>
								</tbody>
</table>


<?php
   }else if( $data->tipo=='proyectos'){
?>
    <table class="table table-sm table-detail" width="100%">
								<thead>
								<tr> 
									<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
									<th class="text-center bold" style="background:#ddebf8;text-align:center">CUI</th>	
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Descripcion</th>
                                    <th class="text-center bold" style="background:#ddebf8;text-align:center">Monto</th>
                                </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
							
									$s2=0;

                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'modal_cui_entidades_perfil','nivel'=>$data->nivel,'ubigeo'=>$data->ubigeo,'funcion'=>$data->funcion,'programa'=>$data->programa,'subprograma'=>$data->subprograma]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
		
								
                                        $s2=   $s2+$item->montoactualizado ;	
																				
                                ?>	
									<tr>
										<td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" > <button class="btn btn-link lnkCUI_Modal" id="<?php echo ($item->codigounico)?>" onclick="App.events(this);"  > <?php echo ($item->codigounico)?></button></td>
										<td class="text-center" ><?php echo ($item->descripcion)?></td>		
										<td class="text-center" ><?php echo number_format($item->montoactualizado)?></td>	

                                    </tr>
                                    <tr data-target="lnkProvXrutas_<?php echo ($item->codigounico)?>" style="display: none;">
										<td colspan="4">
											<div class="card">
												<div class="card-header">
                                                   Detalle 
												</div>
												<div class="card-body">
													<div id="div_<?php echo  ($item->codigounico)?>"></div>
												</div>
											</div>
											
										</td>
									</tr> 
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
	
										<td class="text-center" ></td>		
										<td class="text-center" ><?php echo number_format($s2)?></td>	
									

                                    </tr>
								</tbody>
</table>


<?php
   }
?>