
<div class="card">
    <div class="card-header" style="background:#304e9c">
        <h4 class="m-b-0 text-white text-center">Indicadores de Empresas</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                    Top 25 Empresas Que Participan Oxi
                    </div> 
                    <div class="card-body">
                        <div id="empresas_top_25"></div>
                    </div>
                    <div class="card-footer text-center">
                        <a class="lnkAmpliar" data-event="lnkCanonCiprl" href="#" onclick="App.events(this); return false;">
                            Mostrar/Ocultar tabla resumen
                        </a>
                    </div>
                    <div data-target="lnkCanonCiprl" class="card-body" style="display: none;">
                        <div id="html_empresas_top_25"></div>
                    </div>
                                            
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                    Top 25 Empresas Que No Participan Oxi
                    </div> 
                    <div class="card-body">
                        <div id="empresas_top_25_no"></div>
                    </div>
                    <div class="card-footer text-center">
                        <a class="lnkAmpliar" data-event="lnkCanonCiprl1" href="#" onclick="App.events(this); return false;">
                            Mostrar/Ocultar tabla resumen
                        </a>
                    </div>
                    <div data-target="lnkCanonCiprl1" class="card-body" style="display: none;">
                        <div id="html_empresas_top_25_no"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="padding-top:1em;">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                        Analisis de por tamaño de Empresas
                    </div> 
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12" style="text-align:center">
                                <select class="form-control ddlGraficos_tamano" id="ddlGraficos_tamano" onchange="App.events(this); return false;">
                                    <option value="1" ><b>Grafico Barras</b></option>
                                    <option value="2" ><b>WordCloud</b></option>
                                </select>
                            </div>
                            <div class="col-lg-12" style="padding-top:0.5em;">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div id="chart_dash_indicador2"></div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div id="chart_dash_indicador2_"></div>
                                    </div>
                                </div>
                               
                                
                            </div>
                            <div class="col-lg-12">
                                <div id="html_dash_indicador2"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



