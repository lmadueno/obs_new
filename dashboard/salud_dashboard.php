<ul class="nav nav-tabs" id="myTab" role="tablist">

    <li class="nav-item">
        <a class="nav-link active edad_salud" style="color: #1976D2;!important;" id="monto" href="#" onclick="App.events(this)" title="montos"><b style="padding-left:0.8em;"> Indicador COVID-19 y SINADEF por Edad</b></a>
    </li>
    <li class="nav-item">
        <a class="nav-link active covid_sinadef_salud" style="color: black; !important;" id="evolucion" href="#" onclick="App.events(this)" title="evolucion"><b style="padding-left:0.8em;">Indicadores COVID-19 y SINADEF</b></a>
    </li>
    <li class="nav-item">
        <a class="nav-link active oxigeno_salud" style="color: black; !important;" id="rango" href="#" onclick="App.events(this)" title="rango"><b style="padding-left:0.8em;"> Demanda de Oxígeno en base a casos positivos COVID-19</b></a>
    </li>
    <li class="nav-item">
        <a class="nav-link active vacunas_salud" style="color: black; !important;" id="brecha" x href="#" onclick="App.events(this)" title="brecha"><b style="padding-left:0.8em;"> Indicadores de Vacunación COVID-19</b></a>
    </li>

</ul>
<br>
<div id="div_salud_edad">

</div>
<!-- <div id="ver2" >
    <div class="row">
        <div class="col-lg-12">
            <div id="div_costo_salud"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-6">
            <div id="div_minsa_proyectado"></div>
        </div>
        <div class="col-lg-6">
            <div id="div_sinadef_proyectado"></div>
        </div>
    </div>

</div> -->
<!-- <div id="accordion">

    <div class="card">
        <div class="card-header" id="headingTwo" style="background:#ddebf8">
            <h5 class="mb-0">
                <button class="btn btn-link edad_salud" href="#" onclick="App.events(this); return false;" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" style="color:#000;font-weight: bold; ">
                    Indicador COVID-19 y SINADEF por Edad
                </button>
            </h5>
        </div>
        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="div_salud_edad"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header" id="headingOne" style="background:#ddebf8">
            <h5 class="mb-0">
                <button class="btn btn-link collapsed covid_sinadef_salud" href="#" onclick="App.events(this); return false;" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="color:#000;font-weight: bold; ">
                    Indicadores COVID-19 y SINADEF
                </button>
            </h5>
        </div>
        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
            <div class="card-body">
                <div class="card">
                    <div class="card-header" style="background:#304e9c">
                        <h4 class="m-b-0 text-white text-center">Indicadores de Salud <a href="#" data-toggle="modal" data-target="#exampleModal"><img src="../assets/app/img/signo.png" width="20px" height="20px" style="vertical-align:initial"></a> </h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="div_costo_salud"></div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-6">
                                <div id="div_minsa_proyectado"></div>
                            </div>
                            <div class="col-lg-6">
                                <div id="div_sinadef_proyectado"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingTree" style="background:#ddebf8">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed oxigeno_salud" href="#" onclick="App.events(this); return false;" data-toggle="collapse" data-target="#collapseTree" aria-expanded="false" aria-controls="collapseTree" style="color:#000;font-weight: bold; ">
                        Demanda de Oxígeno en base a casos positivos COVID-19
                    </button>
                </h5>
            </div>
            <div id="collapseTree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="div_salud_oxigeno"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-header" id="headingFour" style="background:#ddebf8">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed vacunas_salud" href="#" onclick="App.events(this); return false;" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour" style="color:#000;font-weight: bold; ">
                        Indicadores de Vacunación COVID-19
                    </button>
                </h5>
            </div>
            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="div_salud_vacuna"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div> -->