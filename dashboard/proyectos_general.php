<?php
    require '../php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<?php
    if($data->tipo=='perfil'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>	
        <th class="text-center bold" style="padding-bottom: 0.8em;">#</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Perfil</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Monto</th>
    </tr>	
    <?php 
        $i=0;
        $s1=0;
        $s2=0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'dashboard_perfiles_ind']);
        foreach ($pipxUbigeoDet as $item){
            $i++; 
            $s1= $s1+$item->cantidad;
            $s2= $s2+$item->monto;
													
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
        <td class="text-center" > <button class="btn btn-link lnkAmpliar_GenralFuncion"  data-event="lnkProvXrutas_<?php echo ($item->nomfase)?>" id="<?php echo ($item->nomfase)?>" onclick="App.events(this);" > <?php echo ($item->nomfase)?></button></td>
        <td class="text-center" ><?php echo number_format($item->cantidad)?></td>	
        <td class="text-center" ><?php echo number_format($item->monto)?></td>		
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo ($item->nomfase)?>" style="display: none;">
										<td colspan="5">
											<div class="card">
											
												<div class="card-body">
													<div id="div_<?php echo  ($item->nomfase)?>"></div>
												</div>
											</div>
											
										</td>
	</tr> 
    <?php
    }
    ?>	
     <tr>
        <td  class="text-center"></td>
		<td class="text-center" >Total</td>
        <td class="text-center" ><?php echo number_format($s1)?></td>	
        <td class="text-center" ><?php echo number_format($s2)?></td>		
    </tr>				
</table>
<?php
    }else  if($data->tipo=='funcion'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>	
        <th class="text-center bold" style="padding-bottom: 0.8em;">#</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Función</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Monto</th>
    </tr>	
    <?php 
        $i=0;
        $s1=0;
        $s2=0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'dashboard_perfiles_ind_funcion','perfil' => $data->perfil]);
        foreach ($pipxUbigeoDet as $item){
            $i++; 
            $s1= $s1+$item->cantidad;
            $s2= $s2+$item->monto;
													
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
	
        <td class="text-center" > <button class="btn btn-link lnkAmpliar_Genralprograma"  data-event="lnkProvXrutas_<?php echo ($data->perfil.'$'.$item->nomfuncionproyecto)?>" id="<?php echo ($data->perfil.'$'.$item->nomfuncionproyecto)?>" onclick="App.events(this);" > <?php echo ($item->nomfuncionproyecto)?></button></td>
        <td class="text-center" ><?php echo number_format($item->cantidad)?></td>	
        <td class="text-center" ><?php echo number_format($item->monto)?></td>		
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo  ($data->perfil.'$'.$item->nomfuncionproyecto)?>" style="display: none;">
										<td colspan="5">
											<div class="card">
												
												<div class="card-body">
													<div id="div_<?php echo   ($data->perfil.'$'.$item->nomfuncionproyecto)?>"></div>
												</div>
											</div>
											
										</td>
	</tr> 
    <?php
    }
    ?>	
     <tr>
        <td  class="text-center"></td>
		<td class="text-center" >Total</td>
        <td class="text-center" ><?php echo number_format($s1)?></td>	
        <td class="text-center" ><?php echo number_format($s2)?></td>		
    </tr>				
</table>
<?php
    }else  if($data->tipo=='programa'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>	
        <th class="text-center bold" style="padding-bottom: 0.8em;">#</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Division Funcional</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Monto</th>
    </tr>	
    <?php 
        $i=0;
        $s1=0;
        $s2=0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'dashboard_perfiles_ind_programa','perfil' => $data->perfil,'funcion' => $data->funcion]);
        foreach ($pipxUbigeoDet as $item){
            $i++; 
            $s1= $s1+$item->cantidad;
            $s2= $s2+$item->monto;
													
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
	
        <td class="text-center" > <button class="btn btn-link lnkAmpliar_GenralSubprograma"  data-event="lnkProvXrutas_<?php echo ($data->perfil.'$'.$data->funcion.'$'.$item->nomprogramaproyecto)?>" id="<?php echo ($data->perfil.'$'.$data->funcion.'$'.$item->nomprogramaproyecto)?>" onclick="App.events(this);" > <?php echo ($item->nomprogramaproyecto)?></button></td>
        <td class="text-center" ><?php echo number_format($item->cantidad)?></td>	
        <td class="text-center" ><?php echo number_format($item->monto)?></td>		
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo  ($data->perfil.'$'.$data->funcion.'$'.$item->nomprogramaproyecto)?>" style="display: none;">
										<td colspan="5">
											<div class="card">
												
												<div class="card-body">
													<div id="div_<?php echo  ($data->perfil.'$'.$data->funcion.'$'.$item->nomprogramaproyecto)?>"></div>
												</div>
											</div>
											
										</td>
	</tr> 
    <?php
    }
    ?>	
     <tr>
        <td  class="text-center"></td>
		<td class="text-center" >Total</td>
        <td class="text-center" ><?php echo number_format($s1)?></td>	
        <td class="text-center" ><?php echo number_format($s2)?></td>		
    </tr>				
</table>
<?php
    }else  if($data->tipo=='subprograma'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>	
        <th class="text-center bold" style="padding-bottom: 0.8em;">#</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Grupo Funcional</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Monto</th>
    </tr>	
    <?php 
        $i=0;
        $s1=0;
        $s2=0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'dashboard_perfiles_ind_subprograma','perfil' => $data->perfil,'funcion' => $data->funcion,'programa' => $data->programa]);
        foreach ($pipxUbigeoDet as $item){
            $i++; 
            $s1= $s1+$item->cantidad;
            $s2= $s2+$item->monto;
													
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
	
        <td class="text-center" > <button class="btn btn-link lnkAmpliar_GenralCUI"  data-event="lnkProvXrutas_<?php echo ($data->perfil.'$'.$data->funcion.'$'.$data->programa.'$'.$item->nomsubprograma)?>" id="<?php echo ($data->perfil.'$'.$data->funcion.'$'.$data->programa.'$'.$item->nomsubprograma)?>" onclick="App.events(this);" > <?php echo ($item->nomsubprograma)?></button></td>
        <td class="text-center" ><?php echo number_format($item->cantidad)?></td>	
        <td class="text-center" ><?php echo number_format($item->monto)?></td>		
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo  ($data->perfil.'$'.$data->funcion.'$'.$data->programa.'$'.$item->nomsubprograma)?>" style="display: none;">
										<td colspan="5">
											<div class="card">
												
												<div class="card-body">
													<div id="div_<?php echo  ($data->perfil.'$'.$data->funcion.'$'.$data->programa.'$'.$item->nomsubprograma)?>"></div>
												</div>
											</div>
											
										</td>
	</tr> 
    <?php
    }
    ?>	
     <tr>
        <td  class="text-center"></td>
		<td class="text-center" >Total</td>
        <td class="text-center" ><?php echo number_format($s1)?></td>	
        <td class="text-center" ><?php echo number_format($s2)?></td>		
    </tr>				
</table>
<?php
    }else  if($data->tipo=='cui'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>	
        <th class="text-center bold" style="padding-bottom: 0.8em;">#</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Codigo Unico</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Descripcion</th>

        <th class="text-center bold" style="padding-bottom: 0.8em;">Nivel</th>
        <th class="text-center bold" style="padding-bottom: 0.8em;">Monto</th>
    </tr>	
    <?php 
        $i=0;

        $s2=0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'dashboard_perfiles_ind_cui','perfil' => $data->perfil,'funcion' => $data->funcion,'programa' => $data->programa,'subprograma' => $data->subprograma]);
        foreach ($pipxUbigeoDet as $item){
            $i++; 
        
            $s2= $s2+$item->montoactualizado;
													
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
	
        <td class="text-center" > <button class="btn btn-link openModalProyectos"  data-toggle="modal" data-target="#exampleModal" id="<?php echo ($item->codigounico)?>" onclick="App.events(this);"><?php echo ($item->codigounico)?></button></td>
        <td class="text-center" ><?php echo ($item->descripcion)?></td>	
        <td class="text-center" ><?php echo ($item->ubicacion)?></td>	
        <td class="text-center" ><?php echo ($item->nivel)?></td>	
        <td class="text-center" ><?php echo number_format($item->montoactualizado)?></td>	

    </tr>
   
    <?php
    }
    ?>	
     <tr>
        <td  class="text-center"></td>
		<td class="text-center" >Total</td>
        <td  class="text-center"></td>
        <td  class="text-center"></td>
        <td  class="text-center"></td>
        <td class="text-center" ><?php echo number_format($s2)?></td>		
    </tr>				
</table>
<?php
    }
?>