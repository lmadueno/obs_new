<?php
require '../php/appdash.php';
// $data = json_decode($_GET['data']);

?>
<div class="card">
    <div class="card-header" style="background:#304e9c">
        <h4 class="m-b-0 text-white text-center">Indicadores de Empresas</h4>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            <strong>Leyenda:</strong>
                            <hr>
                            <ul>
                                <li>
                                    (*) Calculo para Capacidad OxI: ((Facturado 2020 + Facturado 2019 + Facturado 2018)/3) x 10% x 30% x 50%
                                </li>
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="padding-top:1em;">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                        Listado de Empresas con Capacidad Oxi mayor a 5 Millones
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-outline-info">
                                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                        Tabla total de Empresas por Rubros y Maxima de Facturacion(2018,2019,2020)
                                    </div>
                                    <table class="table table-hover table-sm" data-target="datos" width="100%">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-center bold">#</th>
                                                <th scope="col" class="text-center bold">Rubros</th>
                                                <th scope="col" class="text-center bold">Cantidad CIIU's</th>
                                                <th scope="col" class="text-center bold">Empresas</th>
                                                <th scope="col" class="text-center bold">* Capacidad OxI/Fact. </th>
                                                <th scope="col" class="text-center bold">Facturado 2018</th>
                                                <th scope="col" class="text-center bold">Facturado 2019</th>
                                                <th scope="col" class="text-center bold">Facturado 2020</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabla_empresas_acumulado" style="text-align: center;">
                                            <?php
                                            $i = 0;
                                            $cantidad = 0;
                                            $c_ciiu = 0;
                                            $aa2020 = 0;
                                            $aa2019 = 0;
                                            $aa2018 = 0;
                                            $aabz = 0;
                                            $pipxUbigeoDet = dropDownList((object) ['method' => 'empresas_rubro']);
                                            // echo json_encode($pipxUbigeoDet);
                                            foreach ($pipxUbigeoDet as $item) {
                                                $i++;
                                                $c_ciiu = $c_ciiu + intVal($item->c_ciiu);
                                                $cantidad = $cantidad + intVal($item->cantidad);
                                                $aa2020 = $aa2020 + intVal($item->a2020);
                                                $aa2019 = $aa2019 + intVal($item->a2019);
                                                $aa2018 = $aa2018 + intVal($item->a2018);
                                                $aabz = $aabz + intVal($item->sum_monto_oxi);
                                            ?>
                                                <tr>
                                                    <?php if (true) { ?>

                                                        <td class="text-center"><?php echo $i; ?></td>
                                                        <!-- <td class="text-center"><button class="btn btn-link lnkNivelGobiernoPartido" id="<?php echo ($item->nomnivelgobierno) ?>" data-event="lnkProvXrutas_<?php echo ($item->nomnivelgobierno) ?>" onclick="App.events(this);"><?php echo ($item->nomnivelgobierno) ?></button></td> -->
                                                        <td class="text-center"><?php echo ($item->sector_esp) ?></td>
                                                        <td class="text-center"><?php echo ($item->c_ciiu) ?></td>
                                                        <td class="text-center">
                                                            <buttom class="btn btn-link lnkAmpliar_salud" id="<?php echo ($item->sector_esp) ?>" data-event="lnk_<?php echo ($item->sector_esp) ?>" href="#" onclick="App.events(this);">
                                                                <?php echo ($item->cantidad) ?>
                                                            </buttom>
                                                        </td>
                                                        <td class="text-center"><?php echo number_format($item->sum_monto_oxi) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->a2020) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->a2019) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->a2018) ?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr data-target="lnk_<?php echo $item->sector_esp ?>" style="display: none;">
                                                    <td colspan="11">
                                                        <div class="card">
                                                            <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                                                Detalle
                                                            </div>
                                                            <div class="card-body" style="height: 500px; overflow: auto;">
                                                                <table class="table table-hover table-sm" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col" class="text-center bold" width="4%">#</th>
                                                                            <th scope="col" class="text-center bold">RUC</th>
                                                                            <th scope="col" class="text-center bold">Razon Social</th>
                                                                            <th scope="col" class="text-center bold">Facturado 2020</th>
                                                                            <th scope="col" class="text-center bold">Facturado 2019</th>
                                                                            <th scope="col" class="text-center bold">Facturado 2018</th>
                                                                            <th scope="col" class="text-center bold">Capacidad Oxi</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $p = 0;
                                                                        $a2020 = 0;
                                                                        $a2019 = 0;
                                                                        $a2018 = 0;
                                                                        $abz = 0;

                                                                        $pipxUbigeoDet = dropDownList((object) ['method' => 'empresas_rubro_detalle', 'sector' => $item->sector_esp]);

                                                                        foreach ($pipxUbigeoDet as $items) {
                                                                            $p++;
                                                                            $a2020 = $a2020 + intVal($items->facturado_2020_soles_maximo);
                                                                            $a2019 = $a2019 + intVal($items->facturado_2019_soles_maximo);
                                                                            $a2018 = $a2018 + intVal($items->facturado_2018_soles_maximo);
                                                                            $abz = $abz + intVal($items->abz);
                                                                        ?>
                                                                            <tr>
                                                                                <td scope="row" class="text-center" width="4%"><?php echo ($p) ?></td>
                                                                                <td scope="row" class="text-center" width="4%">
                                                                                    <buttom class="btn btn-link modal_empresa" id="<?php echo ($items->ruc) ?>" data-event="lnk_<?php echo ($items->ruc) ?>" href="#" data-toggle="modal" data-target="#exampleModal" onclick="App.events(this);">
                                                                                        <?php echo ($items->ruc) ?>
                                                                                    </buttom>
                                                                                </td>
                                                                                <td scope="row" class="text-center"><?php echo $items->razn_social ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->facturado_2020_soles_maximo) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->facturado_2019_soles_maximo) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->facturado_2018_soles_maximo) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->abz) ?></td>

                                                                            </tr>

                                                                        <?php
                                                                        } ?>
                                                                        <tr>
                                                                            <td scope="row" class="text-center" width="4%"></td>
                                                                            <td scope="row" class="text-center"></td>
                                                                            <td scope="row" class="text-center">Total Acumulado</td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($a2020) ?></td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($a2019) ?></td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($a2018) ?></td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($abz) ?></td>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td scope="row" class="text-center"></td>
                                                <td scope="row" class="text-center">Total Acumulado</td>
                                                <td scope="row" class="text-center"><?php echo number_format($c_ciiu) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($cantidad) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($aa2020) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($aa2019) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($aa2018) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($aabz) ?></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-outline-info">
                                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                    Tabla total de Empresas por Rubros y Maxima de Utilidad (2018,2019,2020)
                                    </div>
                                    <table id="myTable" class="table table-hover table-sm" data-target="datos" width="100%">
                                        <thead>
                                            <tr>
                                                <th scope="col" class="text-center bold" onclick="App.sort(0);">#</th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(1);">Rubros</th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(2);">Cantidad CIIU's</th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(3);">Empresas</th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(4);">* Capacidad OxI/Fact. </th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(5);">* Capacidad OxI/Utilidad. </th>
                                                <th scope="col" class="text-center bold" onclick="App.sort(6);">Indicador Ultimo Año</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tabla_empresas_acumulado" style="text-align: center;">
                                            <?php
                                            $i = 0;
                                            $cantidad_0 = 0;
                                            $c_ciiu_0 = 0;
                                            $oxi_a_1 = 0;
                                            $oxi_s_2 = 0;
                                            $pipxUbigeoDet = dropDownList((object) ['method' => 'empresas_rubro_tbl2']);
                                            foreach ($pipxUbigeoDet as $item) {
                                                $i++;
                                                $c_ciiu_0 = $c_ciiu_0 + intVal($item->c_ciiu_u);
                                                $cantidad_0 = $cantidad_0 + intVal($item->cantidad_u);
                                                $oxi_a_1 = $oxi_a_1 + intVal($item->sum_monto_oxi_a);
                                                $oxi_s_2 = $oxi_s_2 + intVal($item->sum_monto_oxi_u);
                                            ?>
                                                <tr>
                                                    <?php if (true) { ?>

                                                        <td class="text-center"><?php echo $i; ?></td>
                                                        <td class="text-center"><?php echo ($item->sector_esp_u) ?></td>
                                                        <td class="text-center">
                                                            <buttom class="btn btn-link lnkAmpliar_salud" id="<?php echo ($item->sector_esp_u) . "l2" ?>" data-event="lnk_<?php echo ($item->sector_esp_u) . "l2" ?>" href="#" onclick="App.events(this);">
                                                                <?php echo ($item->c_ciiu_u) ?>
                                                            </buttom>
                                                        </td>
                                                        <td class="text-center"><?php echo ($item->cantidad_u) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->sum_monto_oxi_a) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->sum_monto_oxi_u) ?></td>
                                                        <td class="text-center"><?php echo number_format($item->indicador_u_a_u, 2, '.', '').'%' ?></td>
                                                    <?php } ?>
                                                </tr>
                                                <tr data-target="lnk_<?php echo $item->sector_esp_u . "l2" ?>" style="display: none;">
                                                    <td colspan="11">
                                                        <div class="card">
                                                            <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                                                Detalle
                                                            </div>
                                                            <div class="card-body" style="height: 400px; overflow: auto;">
                                                                <table class="table table-hover table-sm" width="100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th scope="col" class="text-center bold" width="4%">#</th>
                                                                            <th scope="col" class="text-center bold">Descripcion CIIU</th>
                                                                            <th scope="col" class="text-center bold">CIIU</th>
                                                                            <th scope="col" class="text-center bold">Empresas</th>
                                                                            <th scope="col" class="text-center bold">*Capacidad OxI/Fact.</th>
                                                                            <th scope="col" class="text-center bold">*Capacidad OxI/Utilidad</th>
                                                                            <th scope="col" class="text-center bold">Indicador Ultimo Año</th>
                                                                            <th scope="col" class="text-center bold">Indicador OxI Ingresos %</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php
                                                                        $p = 0;
                                                                        $ciiu_1 = 0;
                                                                        $cantidad_s_1 = 0;
                                                                        $oxi_a_1 = 0;
                                                                        $oxi_s_1 = 0;

                                                                        $pipxUbigeoDet = dropDownList((object) ['method' => 'empresas_rubro_detalle_tbl2', 'sector' => $item->sector_esp_u]);

                                                                        foreach ($pipxUbigeoDet as $items) {
                                                                            $p++;
                                                                            $ciiu_1 = $ciiu_1 + intVal($items->ciiu);
                                                                            $cantidad_s_1 = $cantidad_s_1 + intVal($items->cantidad_s);
                                                                            $oxi_a_1 = $oxi_a_1 + intVal($items->sum_monto_oxi_a);
                                                                            $oxi_s_1 = $oxi_s_1 + intVal($items->sum_monto_oxi_s);
                                                                        ?>
                                                                            <tr>
                                                                                <td scope="row" class="text-center" width="4%"><?php echo ($p) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo $items->descripcin ?></td>
                                                                                <td scope="row" class="text-center"><?php echo $items->ciiu ?></td>
                                                                                <td scope="row" class="text-center" width="4%">
                                                                                    <buttom class="btn btn-link lnkAmpliar_salud" id="<?php echo ($items->cantidad_s) . "l3" ?>" data-event="lnk_<?php echo ($items->cantidad_s) . "l3" ?>" href="#" onclick="App.events(this);">
                                                                                        <?php echo ($items->cantidad_s) ?>
                                                                                    </buttom>
                                                                                </td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->sum_monto_oxi_a) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->sum_monto_oxi_s) ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->indicador_oxi_ingreso_s, 2, '.', '') ?></td>
                                                                                <td scope="row" class="text-center"><?php echo number_format($items->indicador_u_a_s, 2, '.', '').'%' ?></td>

                                                                            </tr>
                                                                            <!-- aqui detalle nivel3 -->
                                                                            <tr data-target="lnk_<?php echo $items->cantidad_s . "l3" ?>" style="display: none;">
                                                                                <td colspan="11">
                                                                                    <div class="card">
                                                                                        <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                                                                            Detalle
                                                                                        </div>
                                                                                        <div class="card-body" style="height: 400px; overflow: auto;">
                                                                                            <table class="table table-hover table-sm" width="100%">
                                                                                                <thead>
                                                                                                    <tr>
                                                                                                        <th scope="col" class="text-center bold" width="4%">#</th>
                                                                                                        <th scope="col" class="text-center bold">Ruc</th>
                                                                                                        <th scope="col" class="text-center bold">Razon Social</th>
                                                                                                        <th scope="col" class="text-center bold">CIIU</th>
                                                                                                        <th scope="col" class="text-center bold">Rubro</th>
                                                                                                        <th scope="col" class="text-center bold">*Capacidad OxI/Fact.</th>
                                                                                                        <th scope="col" class="text-center bold">*Capacidad OxI/Utilidad</th>
                                                                                                        <th scope="col" class="text-center bold">Indicador Ultimo Año</th>
                                                                                                        <th scope="col" class="text-center bold">Indicador OxI Ingresos %</th>
                                                                                                    </tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                    <?php
                                                                                                    $px = 0;
                                                                                                    $ciiu_2 = 0;
                                                                                                    $cantidad_s_2 = 0;
                                                                                                    $oxi_a_2 = 0;
                                                                                                    $oxi_s_2 = 0;

                                                                                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'empresas_rubro_detalle_tbl3', 'ciiu' => $items->ciiu]);

                                                                                                    foreach ($pipxUbigeoDet as $items) {
                                                                                                        $px++;
                                                                                                        $oxi_a_2 = $oxi_a_2 + intVal($items->capa_oxi);
                                                                                                        $oxi_s_2 = $oxi_s_2 + intVal($items->abz);
                                                                                                    ?>
                                                                                                        <tr>
                                                                                                            <td scope="row" class="text-center" width="4%"><?php echo ($px) ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo $items->ruc ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo $items->razn_social ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo $items->ciiu ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo $items->sector_esp ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo number_format($items->capa_oxi) ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo number_format($items->abz) ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo number_format($items->indicador_u_a, 2, '.', '') ?></td>
                                                                                                            <td scope="row" class="text-center"><?php echo number_format($items->indicador_oxi_ingreso*100, 2, '.', '').'%' ?></td>

                                                                                                        </tr>

                                                                                                    <?php
                                                                                                    } ?>
                                                                                                    <tr>
                                                                                                        <td scope="row" class="text-center" width="4%"></td>
                                                                                                        <td scope="row" class="text-center"></td>
                                                                                                        <td scope="row" class="text-center"></td>
                                                                                                        <td scope="row" class="text-center"></td>
                                                                                                        <td scope="row" class="text-center">Total Acumulado</td>
                                                                                                        <td scope="row" class="text-center"><?php echo number_format($oxi_a_2) ?></td>
                                                                                                        <td scope="row" class="text-center"><?php echo number_format($oxi_s_2) ?></td>

                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>

                                                                                        </div>
                                                                                    </div>

                                                                                </td>
                                                                            </tr>
                                                                            <!-- aqui detalle nivel3 end -->
                                                                        <?php
                                                                        } ?>
                                                                        <tr>
                                                                            <td scope="row" class="text-center" width="4%"></td>
                                                                            <td scope="row" class="text-center"></td>
                                                                            <td scope="row" class="text-center">Total Acumulado</td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($cantidad_s_1) ?></td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($oxi_a_1) ?></td>
                                                                            <td scope="row" class="text-center"><?php echo number_format($oxi_s_1) ?></td>

                                                                        </tr>
                                                                    </tbody>
                                                                </table>

                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td scope="row" class="text-center"></td>
                                                <td scope="row" class="text-center">Total Acumulado</td>
                                                <td scope="row" class="text-center"><?php echo number_format($c_ciiu_0) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($cantidad_0) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($oxi_a_1) ?></td>
                                                <td scope="row" class="text-center"><?php echo number_format($oxi_s_2) ?></td>

                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card card-outline-info">
                                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                                        Top de empresas con Capacidad OXI mayor a 5M
                                    </div>
                                    <div style="height: 500px; overflow: auto;">
                                        <table  class="table table-hover table-sm" data-target="datos">
                                            <thead>
                                                <tr>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(0);">#</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(1);">RUC</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(2);">Razon Social</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(3);">CIIU</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(4);">Rubro</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(5);">Monto Oxi Calculado</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(6);">Ingresos 2020</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(7);">Ingresos 2019</th>
                                                    <th scope="col" class="text-center bold" onclick="App.sort(8);">Ingresos 2018</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tabla_empresas" style="text-align: center;" class="">

                                            </tbody>
                                        </table>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>