<div class="card">
    <div class="card-header" style="background:#304e9c">
        <h4 class="m-b-0 text-white text-center">Indicadores de Proyectos de Inversión Publica</h4>
        <h6 class="m-b-0 text-white text-center">Fuente: <a style='color:white' href="http://ofi5.mef.gob.pe/inviertePub/ConsultaPublica/ConsultaAvanzada">invierte.pe</a></h6>

    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>Leyenda:</strong>
                    <hr>
                    <ul>
                        <li>
                            (*) Fecha de Actualización : 31 - Julio 2021
                        </li>
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <br>
              

                <ul class="nav nav-tabs" id="myTab" role="tablist">

                    <li class="nav-item">
                        <a class="nav-link active evolucion" style="color: #1976D2;!important;" id="monto" href="#" onclick="App.events(this)" title="montos"><b style="padding-left:0.8em;">Proyectos de inversión por monto</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active evolucion" style="color: black; !important;" id="evolucion" href="#" onclick="App.events(this)" title="evolucion"><b style="padding-left:0.8em;">Proyectos de inversión por cantidad</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active rango" style="color: black; !important;" id="rango" href="#" onclick="App.events(this)" title="rango"><b style="padding-left:0.8em;">Rangos Proyectos de Inversión</b></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active brecha" style="color: black; !important;" id="brecha" x href="#" onclick="App.events(this)" title="brecha"><b style="padding-left:0.8em;">Brechas Proyectos de Inversión</b></a>
                    </li>
                    <!-- <li class="nav-item">
                        <a class="nav-link active general" style="color: black; !important;" id="general" x href="#" onclick="App.events(this)" title="general"><b style="padding-left:0.8em;">Proyectos en General</b></a>
                    </li> -->
                    <li>
                    <a class="nav-link active adjudicados" style="color: black; !important;" id="adjudicados" x href="#" onclick="App.events(this)" title="adjudicados"><b style="padding-left:0.8em;">Adjudicados</b></a>

                    </li>

                </ul>
                <br>
                <div id="html_dash_proyectos_funcion">

                </div>
            </div>

        </div>

    </div>
</div>