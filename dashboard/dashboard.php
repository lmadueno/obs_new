<?php
session_start();
if (!isset($_SESSION['sesion'])) {
    header('Location: ../login.html');
}    

// var_dump($_SESSION['sesion']);
?>
<!DOCTYPE html>
<html>

<head>
    <title>Observatorio - Dashboard</title>

    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> -->

    <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet">

    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" /> -->
    <link rel="icon" type="image/png" href="../assets/app/img/icon.png" />
    <link rel="stylesheet" href="../assets/base/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="../assets/highcharts/highcharts.css"> -->
    <link rel="stylesheet" href="../assets/multiselect/multi-select.css" />
    <!-- <link rel="stylesheet" href="../assets/daterangepicker/daterangepicker.css"> -->
    <link rel="stylesheet" href="../assets/select/bootstrap-select.min.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==" crossorigin="" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.css">
    <link rel="stylesheet" href="../assets/extramarkers/css/leaflet.extra-markers.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>



    <style>
        .navbar.navbar-expand-lg.navbar-light {
            background: #ddebf8;
        }

        .highcharts-credits {
            display: none;
        }

        @media (min-width: 768px) {
            .modal-xl {
                width: 70%;
                max-width: 1200px;
            }
        }

        @media (min-width: 992px) {
            .modal-lg {
            max-width: 1500px;
            }
        }

        .accordion.width {
            border: 1px solid rgba(0, 0, 0, 0.125);
            display: flex;
        }

        .accordion.width .card {
            flex-direction: row;
            flex-grow: 0;
            flex-shrink: 1;
            min-width: min-content;
        }

        .accordion.width .card .card-header {
            cursor: pointer;
            transform: rotate(180deg);
            writing-mode: vertical-rl;
        }

        .accordion.width .card .card-header:not(.collapsed) {
            pointer-events: none;
        }

        .collapsing.width {
            transition: width 0.35s ease;
            height: auto;
            width: 0;
        }
    </style>


</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light ">
        <a class="navbar-brand" href="#" id="principal" onclick="App.events(this);">
            <img src="../assets/app/img/logo_lateral_v1.png" width="120px" alt="" loading="lazy">
        </a>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item active" id="empresa">
                    <a class="nav-link" id="empresa_dash" href="#" onclick="App.events(this);">Empresas</a>
                </li>
                <li class="nav-item" id="proyectos">
                    <a class="nav-link" id="proyectos_dash" href="#" onclick="App.events(this);" href="#">Proyectos</a>
                </li>
                <li class="nav-item" id="entidades">
                    <a class="nav-link" id="entidades_dash" href="#" onclick="App.events(this);" href="#">Entidades</a>
                </li>
                <li class="nav-item" id="politica">
                    <a class="nav-link" id="politica_dash" href="#" onclick="App.events(this);" href="#">Politica</a>
                </li>
                <li class="nav-item" id="salud">
                    <a class="nav-link" id="salud_dash" href="#" onclick="App.events(this);">Salud</a>
                </li>
                <!-- <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Cerrar Session
                    </a>
                </li> -->
            </ul>
            <!-- <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>

            </form> -->

        </div>
    </nav>
    <!-- toodo el contenido del dashboard -->
    <div id="content_dashboard" style="margin:20px">

        <div class="container" style="padding-left: 200px;padding-right : 200px;margin-top: 80px;">
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Empresas</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="empresa_dash" href="#" onclick="App.events(this);"> <img src="../assets/app/img/iconos-Empresas.png" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Empresas
                                        privadas con y sin
                                        participación el mecanismo
                                        OxI, registrados en
                                        PROINVERSIÓN y Ranking de
                                        empresas en el Perú.
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Proyectos</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="proyectos_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Proyectos.png" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Perfiles de
                                        proyectos de inversión por
                                        función, división funciona y
                                        grupo funcional, sector, área
                                        geográfica de intervención y
                                        proyectos priorizados,
                                        registrados en INVIERTE PE.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Entidades</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="entidades_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Entidades.png" width="165" alt="" loading="lazy"></i></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Entidades por
                                        niveles de gobierno con
                                        proyectos de inversión con
                                        perfiles viables con y sin
                                        ejecución, registrados en el
                                        MEF.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <br>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Salud</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="salud_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/iconos-Salud.png" width="125" alt="" loading="lazy"></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores del sector
                                        Salud, impacto del
                                        COVID19 por niveles de
                                        gobierno, según registro
                                        MINSA y SINADEF.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-outline-info">
                        <div class="card-header card-special text-center font-weight-bold" style="background:#304e9c">
                            <h4 class="m-b-0 text-white text-center">Política</h4>
                        </div>
                        <div class="card-body" style="text-align:center">
                            <div class="row">
                                <div class="col-lg-12">
                                    <a id="politica_dash" href="#" onclick="App.events(this);"><img src="../assets/app/img/Icono_Politica.png" width="106" alt="" loading="lazy"></i></a>
                                </div>
                                <div class="col-lg-12" style="text-align:justify;padding-top:20px">
                                    <p style="text-align:center">
                                        Indicadores de Políticas Públicas por niveles de gobierno, información para oportunidades que pueden ser aprovechadas en la gestión pública
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-lg" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div id="m_empresa">

                        </div>
                    </div>
                </div>
                <div class="modal-footer">|
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->

    <!-- <script type="text/javascript" src="../assets/select/bootstrap-select.min.js"></script> -->

    <script type="text/javascript" src="../assets/base/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="../assets/base/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../assets/base/js/popper.min.js"></script>
    <script type="text/javascript" src="../assets/base/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/base/js/bootstrap-select.min.js"></script>
    <script type="text/javascript" src="../assets/leaflet/leaflet.js"></script>
    <script type="text/javascript" src="../assets/extramarkers/js/leaflet.extra-markers.min.js"></script>
    <script type="text/javascript" src="../assets/betterwms/L.TileLayer.BetterWMS.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/http-client/4.3.1/http-client.min.js"></script>
    <!-- <script src="https://code.highcharts.com/highcharts.js"></script> -->
    <script src="https://code.highcharts.com/stock/highstock.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/heatmap.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/wordcloud.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script type="text/javascript" src="../assets/app/js/dashboard.js"></script>
    <script type="module" src="../public/bundledash.js"></script>
    <!-- <script type="module" src="../public/proadjudicados-obs.js"></script> -->
    <!-- <script type="module" src="../lit/proadjudicados-obs.js"></script>	 -->

    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" /> -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script>
        $(document).ready(function() {
            App.init();

        });
    </script>

</body>

</html>