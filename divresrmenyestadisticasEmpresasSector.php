<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Ruc</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Razon Social</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Utilidad</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Oxi</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	   
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'indicador_dash_2_empresas','tamanio'=>$data->tamanio,'sector'=>$data->sector]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->utilidad ;	
										$s2019=   $s2019+$item->disponibleoxi ;	

																					
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" >
											<button class="btn btn-link lnkModalEmpresas"  data-toggle="modal" data-target="#exampleModal" id="<?php echo $item->rucempresa?>"   onclick="App.events(this);">
												<?php echo ($item->rucempresa)?>
											</button>
										</td>												
										<td class="text-center" ><?php echo ($item->nomempresa)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->utilidad)?></td>		
										<td class="text-center" ><?php echo number_format($item->disponibleoxi)?></td>		
                                    </tr>
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
										<td ></td>
                                        <td class="text-center" ><?php echo number_format($s2018)?></td>												
                                        <td class="text-center"><?php echo number_format($s2019)?></td>

                                    </tr>
								</tbody>
							</table>