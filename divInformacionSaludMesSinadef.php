<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
    if($data->tipo==2){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesSinadef','tipo'=> $data->tipo,'departamento'=> $data->departamento]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'departamento'=> $data->departamento]);
    }else if($data->tipo==4){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesSinadef','tipo'=> $data->tipo,'departamento'=> $data->departamento,'provincia'=> $data->provincia]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'provincia'=> $data->provincia]);

    }else if($data->tipo==6){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesSinadef','tipo'=> $data->tipo,'departamento'=> $data->departamento,'provincia'=> $data->provincia,'distrito'=> $data->distrito]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'distrito'=> $data->distrito]);
    }
   
?>

<div class="row">
    <div class="col-lg-12"> 
        <div class="card">
            <div class="card-header card-special text-center font-weight-bold">
                Informacion y estadisticas por Mes de <?php echo $titulo{0}->titulo;?> - SINADEF
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="chartMes"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>	
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header card-special text-center font-weight-bold">
                                Consolidado anual por Mes de la <?php echo $titulo{0}->titulo;?>  - SINADEF
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-detail">
                                    <thead>
                                        <tr>
                                            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; "></th>											
                                            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2017</th>
                                            <th></th>
                                            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2018</th>
                                            <th></th>
                                            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2019</th>	
                                            <th></th>
                                            <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2020</th>	
                                        														
                                        </tr> 
                                        <tr> 
                                            <th class="text-center bold" width="4%">#</th>
                                            <th class="text-center bold" >Mes</th> 
                                            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
                                            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th> 
                                            <th class="text-center bold"></th>   
                                            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
                                            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th> 
                                            <th class="text-center bold"></th>     
                                            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
                                            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th> 
                                            <th class="text-center bold"></th>   
                                            <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
                                            <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i=0;   
                                       
                                        $Suma1=0 ;
                                       
                                        $Suma3=0 ;
                                        
                                        $Suma5=0 ;
                                        
                                        $Suma7=0 ;
                                       
                                        
                                        
                                        foreach ($pipxUbigeoDet as $item){
                                            $i++;  
                                            $Suma1+= $item->s2017;
                                          
                                            $Suma3+= $item->s2018;
                                         
                                            $Suma5+= $item->s2019;
                                     
                                            $Suma7+= $item->s2020;
                                        
                                            												
                                        ?> 
                                            <tr>	
                                                <td  class="text-center"><?php echo $i;?></td>
                                                <td  class="text-center"><?php echo $item->meses;?></td>
                                                <td  class="text-center"><?php echo number_format($item->s2017);?></td>
                                                <td  class="text-center"><?php echo number_format(round($item->sp2017, 1),2);?></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($item->s2018);?></td>
                                                <td  class="text-center"><?php echo number_format(round($item->sp2018, 1),2);?></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($item->s2019);?></td>
                                                <td  class="text-center"><?php echo number_format(round($item->sp2019, 1),2);?></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($item->s2020);?></td>
                                                <td  class="text-center"><?php echo number_format(round($item->sp2020, 1),2);?></td>
                                                
                                         </tr>
                                        <?php } ?>
                                        <tr>	
                                                <td  class="text-center"></td>
                                                <td  class="text-center">Total</td>
                                                <td  class="text-center"><?php echo number_format($Suma1);?></td>
                                                <td  class="text-center"></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($Suma3);?></td>
                                                <td  class="text-center"></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($Suma5);?></td>
                                                <td  class="text-center"></td>
                                                <td></td>
                                                <td  class="text-center"><?php echo number_format($Suma7);?></td>
                                                <td  class="text-center"></td>
                                                
                                                
                                         </tr>
                                    </tbody>
                                </table>
                            </div> 
                        </div>   
                    </div>
                </div>




            </div>
        </div>
    </div>
</div>
