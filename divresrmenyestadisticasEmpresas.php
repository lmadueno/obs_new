<?php
    require 'php/app.php';		
	$data = json_decode($_GET['data']);	
?>
<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
                            Analisis Financiero
                </div>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-6">
							<div id="chartEmpresasAnalisi"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>	
						</div>
						<div class="col-lg-6">
							<div id="chartEmpresasAnalisi1"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>
						</div>
						<div class="col-lg-12">
							<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold">ANALISIS FINANCIERO</th>
										<th class="text-center bold" >2015</th>
										<th class="text-center bold" >2016</th>
										<th class="text-center bold" >2017</th>
										<th class="text-center bold" >2018</th>
										<th class="text-center bold" >2019</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
								  
									$t1=0;
									$t2=0;
									$t3=0;
									$t4=0;
									$t5=0;
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'analisisempresas_financiero','id' => $data->id ]);
                                    foreach ($pipxUbigeoDet as $item){
										
										$t1=$item->a2015+$t1;
										$t2=$item->a2016+$t2;
										$t3=$item->a2017+$t3;
										$t4=$item->a2018+$t4;
										$t5=$item->a2019+$t5;									
                                ?>	
									<tr>
										<td  class="text-center"><?php echo $item->idanalisis;?></td>
										<td  class="text-left"><?php echo $item->descripcion;?></td>
										<td class="text-center" ><?php echo number_format($item->a2015)?></td>												
										<td class="text-center" ><?php echo number_format($item->a2016)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->a2017)?></td>		
										<td class="text-center" ><?php echo number_format($item->a2018)?></td>		
										<td class="text-center" ><?php echo number_format($item->a2019)?></td>	
                                    </tr>
									
								<?php } ?>
									<tr>
										<td ></td>
                                        <td >Total</td>
                                        <td class="text-center" ><?php echo number_format($t1)?></td>												
                                        <td class="text-center"><?php echo number_format($t2)?></td>
                                        <td class="text-center"><?php echo number_format($t3)?></td>
										<td class="text-center"><?php echo number_format($t4)?></td>
										<td class="text-center"><?php echo number_format($t5)?></td>

                                    </tr>
								</tbody>
							</table>
						</div>
					</div>
				</div> 
			</div>   
		</div>
	</div>