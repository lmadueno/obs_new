        
<?php
	require 'php/app.php';        
?> 
<style>
.switch {
              position: relative;
              display: inline-block;
              width: 60px;
              height: 34px;
            }
    .switch input { 
                  opacity: 0;
                  width: 0;
                  height: 0;
                }
    .slider {
                  position: absolute;
                  cursor: pointer;
                  top: 0;
                  left: 0;
                  right: 0;
                  bottom: 0;
                  background-color: #ccc;
                  -webkit-transition: .4s;
                  transition: .4s;
                }

    .slider:before {
                  position: absolute;
                  content: "";
                  height: 26px;
                  width: 26px;
                  left: 4px;
                  bottom: 4px;
                  background-color: white;
                  -webkit-transition: .4s;
                  transition: .4s;
                }

                input:checked + .slider {
                  background-color: #2196F3;
                }

                input:focus + .slider {
                  box-shadow: 0 0 1px #2196F3;
                }

                input:checked + .slider:before {
                  -webkit-transform: translateX(26px);
                  -ms-transform: translateX(26px);
                  transform: translateX(26px);
                }

                /* Rounded sliders */
        .slider.round {
                  border-radius: 34px;
                }

        .slider.round:before {
                  border-radius: 50%;
                }


</style>
<table class="table table-sm table-detail">
    <tr>
        <th class="text-center bold"  width="3%" >#</th>
		<th class="text-center bold" width="27%">Apellidos y Nombres</th>
        <th class="text-center bold" width="20%">Empresa</th>
        <th class="text-center bold" width="20%">Correo</th>
        <th class="text-center bold" width="10%">Estado</th>
        <th class="text-center bold" width="10%">Rol</th>
        <th class="text-center bold" width="5%"></th>
        <th class="text-center bold" width="5%"></th>																								
    </tr>
    <?php 
        $i=0;
        $tablaUsert = dropDownList((object) ['method' => 'getTablaUser']);
        foreach ($tablaUsert as $key){
            $i++;
    ?> 
    <tr>
        <th class="text-left"><?php  echo $i?></th>
		<th class="text-left"><?php   echo ($key->apellidos)?></th>
        <th class="text-left"><?php   echo ($key->empresa)?></th>
        <th class="text-left"><?php   echo ($key->correo)?></th>
        <th class="text-left"><?php   echo ($key->idestado == 1 ? "Activo" : "Inactivo")?></th>
        <th class="text-left">
            <a class="lnkAmpliar"  data-event="lnkProvXrutas_<?php echo $i?>" href="#" onclick="App.events(this); return false;">
                <?php   echo ($key->descripcion)?>
            </a>
        </th>
        <th class="text-right">
            <button type="button" id="enviarCorreoUser" title="Enviar Correo Electronico" data-id="<?php echo $key->correo.'$'.$key->id?>" class="btn btn-primary btn-sm"  onclick="App.events(this)">
                <i class="fa fa-envelope-open-text"></i>
			</button>
        </th>	
        <th class="text-left">
            <label class="switch">
                <input type="checkbox" id="changeestadouser" <?php echo ($key->idestado == 1 ? "checked" : "")?> 
                data-id="<?php echo $key->correo.'$'.$key->id.'$'.$key->idestado?>" onclick="App.events(this)">
                <span class="slider round"></span>
            </label>
        </th>																						
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo $i?>" style="display: none;">
        <td colspan="8">
           <div class="card card-header card-special text-center font-weight-bold">
                <div class="card-body">
                    <table class="table table-sm table-detail">
                        <tr>
                            <td class="text-center bold"  width="15%" >Perfil</td>
                            <td class="text-center " width="80%">
                                <select class="form-control form-control-sm" id="ddlrolChange_<?php echo $key->id?>">
                                    <option value="0" >Seleccione un Perfil de Usuario</option>
                                    <option value="1" >Administrador</option>
									<option value="2" >Basico</option>
									<option value="3" >Avanzado</option>
                                </select>  		
                            </td> 
                            <td class="text-right"> 
                                
                                <button type="button" id="changePerfil" title="Cambiar Perfil de Usuario" data-id="<?php echo $key->id?>" class="btn btn-primary btn-sm"  onclick="App.events(this)">
                                    <i class="fa fa-save"></i>
			                    </button>
                            </td>																						
                        </tr>
                    </table>
                </div>
           </div>            
        </td>
    </tr>      
    <?php 
        }
    ?> 
    <tr>
        <th class="text-left"></th>
		<th class="text-left"></th>
        <th class="text-left"></th>
        <th class="text-left"></th>
        <th class="text-left"></th>
        <th class="text-left bold">Total</th>
        <th class="text-left bold"></th>
        <th class="text-left bold"><?php echo ($i)?></th>																								
    </tr>
</table>