<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);


?>
<br>

<div class="alert alert-primary alert-dismissible fade show" role="alert">
	<strong>Leyenda:</strong>
	<hr>
	<ul>
		<li>
			Se considera infectados COVID-19 el promedio móvil 7 desde inicio de pandemia.
		</li>
		<li>
			El 15% de los pacientes infectados requieren Oxigenoterapia.
		</li>
		<li>
			El 75% de pacientes que requieren Oxigenoterapia, necesitan 0.6 m3/hora.

		</li>
		<li>
			El 25% de pacientes que requieren Oxigenoterapia, necesitan 1.8 m3/hora.

		</li>
		<li>
			La necesidad de Oxigeno por quintiles (Rojo, Naranja, Amarillo, verde claro y verde oscuro)

		</li>

	</ul>
	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
	</button>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Analisis Casos Positivos COVID-19
			</div>
			<br>

			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">
									<div class="card-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="card card-outline-info">
													<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
														<div class="row">
															<div class="col-lg-12">
																<div class="panel panel-default">
																	<div class="panel-body">
																		<div class="row">
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Departamentos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_oxigeno" id="ddlDepartamentos_oxigeno" name="ddlDepartamentos_oxigeno" onchange="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Provincias</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_oxigeno" id="ddlProvincias_oxigeno" name="ddlProvincias_oxigeno" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Distritos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_minsa" id="ddlDistritos_oxigeno" name="ddlDistritos_oxigeno" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Accion</label>
																					<button type="button" class="form-control input-sm btn btn-primary filtrooxigeno" id="filtrooxigeno" name="filtrooxigeno" onclick="App.events(this); return false;">Filtrar</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
											DEMANDA DE OXIGENO POR DEPARTAMENTO

										</div>
										<table class="table table-sm table-detail" data-target="tabla2" width="100%">
											<thead>
												<tr>
													<th class="text-center bold" style="background:#ddebf8;" width="4%">#</th>
													<th class="text-center bold" style="background:#ddebf8;">Departamento</th>
													<th class="text-center bold" style="background:#ddebf8;">Positivos</th>
													<th class="text-center bold" style="background:#ddebf8;">Promedio 10 días</th>
													<th class="text-center bold" style="background:#ddebf8;">Pacientes</th>
													<th class="text-center bold" style="background:#ddebf8;">Demanda m3/h</th>
												</tr>
											</thead>
											<tbody id="tabla_oxigeno">
												
											</tbody>
										</table>
									</div>

								</div>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Demanda promedio Oxigeno m3/hora por Departamento (01 Mayo)
										</div>
										<div id='chartoxigeno_v2_1' style=" height: 800px; margin: 0 auto"></div>
									</div>
								</div>
							</div>
						</div>


					</div>


				</div>
				<br><br>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Incremento de la demanda de Oxígeno (m3/hora) en base a acumulado de infectados 10 días


						</div>
						<div id='chartoxigeno_v2_2'></div>
					</div>
					<!-- <div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Distribución de vacunados por grupo riesgo


						</div>
						<div id='chartvacuna_5'></div>
					</div>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Proceso de vacunación segun Genero

						</div>
						<div id='chartvacuna_6'></div>
					</div> -->
				</div>

			</div>

		</div>
	</div>
</div>
<br>

<br>