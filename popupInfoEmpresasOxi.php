<?php
	require 'php/app.php';
	
	/*if(count($_GET) > 0){
		if($_GET['data']){
			$data   = json_decode($_GET['data']);
			$obj    = dropDownList((object) ['method' => 'DatosXubigeo', 'codnivel' => $data->idnivel, 'codUbigeo' => $data->idnivel == 1 ? $data->coddpto : $data->codubigeo]);
			
			if(count((array)$obj) > 0){
            $obj = $obj{0};*/
            
?>
<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<!--1150PX-->
<div >
<div class="container"  width= "100%"!important  height="100%"!important>	
    <div class="card card-outline-info" >
        <div class="card-header">
			<div class="row">
				<div class="col-lg-11">
					<h6 class="m-b-0 text-white" style="text-align:center !important">RESUMEN DE INFORMACION</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <a class="lnkAmpliarDIALOG" data-event="dialog4" href="#" onclick="App.events(this); return false;">
                        <i class="fas fa-compress text-white"></i>
                    </a>
                                  
                </div>
			</div>
        </div>
            <div class="row hide">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
			        	<li class="nav-item">
					        <a class="nav-link active" id="empresas-tab"  href="#" onclick="App.events(this)" title="Empresas OxI"><i class="fa fa-city"></i><b style="padding-left:0.5em;">Empresas</b></a>
				        </li>
				         <li class="nav-item">
					        <a class="nav-link" id="regiones-tab"  href="#" onclick="App.events(this)" title="Gobiernos Regionales OxI"><i class="fa fa-building"></i><b style="padding-left:0.5em;">Regiones</b></a>
                        </li>
                         <li class="nav-item">
					        <a class="nav-link" id="provincias-tab" href="#" onclick="App.events(this)" title="Municipalidades Provinciales OxI"><i class="fa fa-place-of-worship"></i><b style="padding-left:0.5em;">Provincias</b></a>
				        </li>
				       <li class="nav-item">
					        <a class="nav-link" id="distritos-tab" href="#" onclick="App.events(this)" title="Municipalidades Distritales OxI"><i class="fa fa-home"></i><b style="padding-left:0.5em;">Distritos</b></a>
                        </li>
                        <li class="nav-item">
					        <a class="nav-link" id="ministerios-tab" href="#" onclick="App.events(this)" title="Ministerios OxI"><i class="fa fa-hospital"></i><b style="padding-left:0.5em;">Ministerios</b></a>
                        </li>
                        <li class="nav-item">
					        <a class="nav-link" id="universidades-tab" href="#" onclick="App.events(this)" title="Universidades Nacionales OxI"><i class="fa fa-university"></i><b style="padding-left:0.5em;">Universidades</b></a>
                        </li>
                        <li class="nav-item">
					        <a class="nav-link" id="politica-tab" href="#" onclick="App.events(this)" title="Congresistas por Región"><i class="fa fa-balance-scale"></i><b style="padding-left:0.5em;">Politica</b></a>
                        </li>  
                        <li class="nav-item">
					        <a class="nav-link" id="proyecto-tab" href="#" onclick="App.events(this)" title="Consolidado de Proyectos"><i class="fa fa-gavel"></i><b style="padding-left:0.5em;">Proyecto</b></a>
                        </li>
                        <li class="nav-item">
					        <a class="nav-link" id="oportunidad-tab" href="#" onclick="App.events(this)" title="Oportunidad de proyectos viables"><i class="fa fa-podcast"></i><b style="padding-left:0.5em;">Oportunidades</b></a>
                        </li>    
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="empresas">
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                             <div class="card-header card-special text-center font-weight-bold">
                                                 Ranking de empresas por monto de inversión en obras por Impuestos 2009 - 2019
                                             </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                     Inversión OxI de Empresas
                                                </div>
                                                 <span style="text-align:center;">(Millones de soles)</span>
                                                 <div class="card-body">
                                                     <div id="chartContainerEmpresas" style="height: 290px; margin: 0 auto;width: 100%;"></div>
                                                 </div>
                                            </div>
                                        </div>
                                        <div class="col-6" style="padding-top:11em;" >
                                            <div class="row">
                                                <div class="col-12">
                                                     <p align="justify">Un total de 103 empresas han participado en el mecanismo OxI y han realizado una inversión de 4,295.20 Millones de soles en el periodo 2009 al 2019. El Top 10 de estas empresas concentran una inversion de 3,399.40 Millones de soles equivalente al 79.14%.</p>
                                                </div>
                                                <div class="col-12" style="padding-top:1em">                                                  
                                                    <table class="table table-sm table-detail">																																				
                                                        <tr>
                                                            <th class="text-center bold" >Descripción</th>
												            <th class="text-center bold" width="40px">Total</th>
                                                            <th class="text-center bold" width="60px">Top 10</th>
                                                            <th class="text-center bold">Diferencia</th>																								
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center bold">Empresas</th>
												            <th class="text-center">
                                                            <a id="btnEmpresasTotal" href="#" onclick="App.events(this); return false;">
                                                                   103
                                                                </a>
                                                            </th>
                                                            <th class="text-center "> 
                                                                <a id="btnTop10Empresas" href="#" onclick="App.events(this); return false;">
                                                                   10
                                                                </a>
                                                            </th>
                                                            <th class="text-center ">
                                                                <a id="btnRestosEmpresas" href="#" onclick="App.events(this); return false;">
                                                                   93
                                                                </a>
                                                            </th>																								
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center bold">Cantidad de Proyectos</th>
												            <th class="text-center">370</th>
                                                            <th class="text-center ">205</th>
                                                            <th class="text-center ">165</th>																								
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center bold">Inversión (Millones de Soles)</th>
												            <th class="text-center">4,295.20</th>
                                                            <th class="text-center ">3,399.40</th>
                                                            <th class="text-center ">895.80</th>																								
                                                        </tr>
                                                        <tr>
                                                            <th class="text-center bold">Total Inversión</th>
												            <th class="text-center">100%</th>
                                                            <th class="text-center ">79.14%</th>
                                                            <th class="text-center ">20.86%</th>																								
                                                        </tr>
								                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=188&sec=0" target="blank">Proinversión</a></h6>
                                    <br>
                                    <div id="idGeoempresas"></div>     
                                    <br>
                                    <div class="row">
                                        <div id="idtablaempresas"></div>                                                                               
                                    </div>
                                    <br>
                                   
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 750px;">
                                                <div id="chartContainerEmpresas1"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                   
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 750px;">
                                                <div id="chartADCONEmpresas1"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="regiones">                         
                            <div class="card card-outline-info">
                                <div class="card-body">                  
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Evolución CIPRL y monto utilizado en Gobiernos Regionales en Millones de Soles
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center" width= "10%"><b>Año</b></th>
                                                        <th class="text-center" width= "35%"><b>Tope CIPRL</b></th>
                                                        <th class="text-center" width= "30%"><b>Monto Utilizado</b></th>
                                                        <th class="text-center" width= "30%"><b>%</b></th>
                                                  </tr>	
                                                </thead>
                                                <tr>
                                                     <th class="text-center">2011</th>
                                                     <th class="text-right">4,084.83</th>
                                                     <th class="text-right">276.74</th>
                                                     <th class="text-right">6.75</th>
                                                     
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2012</th>
                                                     <th class="text-right">5,005.53</th>
                                                     <th class="text-right">1.49</th>
                                                     <th class="text-right">0.03</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2013</th>
                                                     <th class="text-right">5,995.65</th>
                                                     <th class="text-right">253.40</th>
                                                     <th class="text-right">4.23</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2014</th>
                                                     <th class="text-right">6,060.72</th>
                                                     <th class="text-right">243.06</th>
                                                     <th class="text-right">4.01</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2015</th>
                                                     <th class="text-right">5,520.61</th>
                                                     <th class="text-right">195.11</th>
                                                     <th class="text-right">3.53</th>
                                                </tr>	                   
                                                <tr>
                                                     <th class="text-center">2016</th>
                                                     <th class="text-right">4,565.47</th>
                                                     <th class="text-right">198.15</th>
                                                     <th class="text-right">4.34</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2017</th>
                                                     <th class="text-right">3,680.25</th>
                                                     <th class="text-right">300.62</th>
                                                     <th class="text-right">8.17</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2018</th>
                                                     <th class="text-right">1,482.66</th>
                                                     <th class="text-right">30.83</th>
                                                     <th class="text-right">2.08</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2019*</th>
                                                     <th class="text-right">1,873.63</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center bold vert-middle">Total</th>
                                                     <th class="text-right bold vert-middle">34,184.52</th>
                                                     <th class="text-right bold vert-middle">1499.40</th>
                                                     <th class="text-right bold vert-middle">3.58</th>
                                                </tr>
                                          </table>
                                        </div>
                                        <div class="col-8">
                                            <div id="container"  style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ranking de Gobiernos Regionales (GR) por monto de inversión en obras por impuestos 2009 - 2019
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Gobiernos Regionales (Concluidos)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerGobiernosRegionales1" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>                                                       
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            <p align="justify">Para el año 2019 se ha otorgado un monto de 1,873.64 Millones de Soles de Tope CIPRL para los Gobiernos Regionales.</p>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            <p align="justify">Actualmente 12 Gobiernos Regionales tienen 43 proyectos en estado 'Concluido' por un monto de 1080.19 Millones de soles.</p>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;padding-button:3em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Gobiernos Regionales(GR)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">25</a></th>
                                                                    <th class="text-center ">12</th>
                                                                    <th class="text-center ">13</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">43</th>
                                                                    <th class="text-center ">43</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">1080.19</th>
                                                                    <th class="text-center ">1080.19</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">48%</th>
                                                                    <th class="text-center ">52%</th>																							
                                                                </tr> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                     Inversión OxI de Gobiernos Regionales (Adjudicados)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerGobiernosRegionales" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            <p align="justify">Para el año 2019 se ha otorgado un monto de 1,873.64 Millones de Soles de Tope CIPRL para los Gobiernos Regionales.</p>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            <p align="justify">Actualmente 9 Gobiernos Regionales tienen 23 proyectos en estado 'Adjudicado' por un monto de 419.21 Millones de soles. En el año 2018 los gobiernos regionales adjudicaron 30.83 Millones de soles.</p>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Gobierno Regionales(GR)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">25</a></th>
                                                                    <th class="text-center ">9</th>
                                                                    <th class="text-center ">16</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">23</th>
                                                                    <th class="text-center ">23</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">419.21</th>
                                                                    <th class="text-center ">419.21</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">36%</th>
                                                                    <th class="text-center ">64%</th>																							
                                                                </tr>
                                                            </table>	
                                                        </div>
                                                    </div>
                                                </div>        
                                            </div>
                                        </div>
                                       
                                    </div>                                    
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Gobiernos Regionales con OxI Concluido 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaRegionesConcluidas"></div>
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 hasta el 2010</td>																								
			                                        </tr>																
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="chartContainerGobiernosRegionales3" style="height: 520px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>                                   
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Gobiernos Regionales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaRegionesAdjudicadas"></div>
                                           
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 hasta el 2013</td>																								
			                                        </tr>												
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="chartContainerGobiernosRegionales2" style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                </div>           
                            </div> 
                        </div>
                        <div class="tab-pane fade" id="provincias">
                            <div class="card card-outline-info">
                                <div class="card-body">
                                <div id="idTablaciprlGraficoProvincias"></div>
                                <br>
                                <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Evolución CIPRL y monto utilizado en Municipalidades Provinciales en Millones de Soles
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center" width= "10%"><b>Año</b></th>
                                                        <th class="text-center" width= "35%"><b>Tope CIPRL</b></th>
                                                        <th class="text-center" width= "30%"><b>Monto Utilizado</b></th>
                                                        <th class="text-center" width= "30%"><b>%</b></th>
                                                  </tr>	
                                                </thead>                                                
                                                <tr>                                                      		                                         	
                                                     <th class="text-center">2011</th>
                                                     <th class="text-right"> 4836.87</th>
                                                     <th class="text-right"> 5.62</th>
                                                     <th class="text-right">0.12</th>
                                                     
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2012</th>
                                                     <th class="text-right">4543.83</th>
                                                     <th class="text-right">50.90</th>
                                                     <th class="text-right">1.12</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2013</th>
                                                     <th class="text-right">4298.36</th>
                                                     <th class="text-right">151.00</th>
                                                     <th class="text-right">3.51</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2014</th>
                                                     <th class="text-right">4633.20</th>
                                                     <th class="text-right">80.02</th>
                                                     <th class="text-right">1.73</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2015</th>
                                                     <th class="text-right">4008.13</th>
                                                     <th class="text-right">139.44</th>
                                                     <th class="text-right">3.48</th>
                                                </tr>	                   
                                                <tr>
                                                     <th class="text-center">2016</th>
                                                     <th class="text-right">2918.37</th>
                                                     <th class="text-right">259.78</th>
                                                     <th class="text-right">8.90</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2017</th>
                                                     <th class="text-right">2691.04</th>
                                                     <th class="text-right">298.10</th>
                                                     <th class="text-right">11.08</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2018</th>
                                                     <th class="text-right">1623.31</th>
                                                     <th class="text-right">128.61</th>
                                                     <th class="text-right">7.92</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2019*</th>
                                                     <th class="text-right">1585.51</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center bold vert-middle">Total</th>
                                                     <th class="text-right bold vert-middle">31,138.62</th>
                                                     <th class="text-right bold vert-middle">853.20</th>
                                                     <th class="text-right bold vert-middle">2.74</th>
                                                </tr>
                                          </table>
                                        </div>
                                        <div class="col-8">
                                            <div id="container1"  style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ranking de Municipalidades Provinciales (MP) por monto de inversión en obras por impuestos 2009 - 2019
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                     Inversión OxI de Municipalidades Provinciales (Adjudicados)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerProvinciasAdjudicadas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            Actualmente 22 (11.2%) Municipalidades Provinciales (MP) de un total de 196 tienen adjudicado 29 proyectos por un monto de 266 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Municipalidades Provinciales(MP)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado" target="blank">196</a></th>
                                                                    <th class="text-center ">22</th>
                                                                    <th class="text-center ">174</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">29</th>
                                                                    <th class="text-center ">29</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">266</th>
                                                                    <th class="text-center ">266</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">11.22%</th>
                                                                    <th class="text-center ">88.78%</th>																							
                                                                </tr>
                                                            </table>	
                                                        </div>
                                                    </div>
                                                </div>        
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Municipalidades Provinciales (Concluidos)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerProvinciasConcluidas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>                                                       
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                             Actualmente 28 (14.28%) Municipalidades Provinciales (MP) de un total de 196, han concluido 63 proyectos por un monto de 587.20 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Municipalidades Provinciales(MP)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">196</a></th>
                                                                    <th class="text-center ">28</th>
                                                                    <th class="text-center ">168</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">63</th>
                                                                    <th class="text-center ">63</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">587.20</th>
                                                                    <th class="text-center ">587.20</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">14.28%</th>
                                                                    <th class="text-center ">85.72%</th>																							
                                                                </tr> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>                                   
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Municipalidades Provinciales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaProvinciasAdjudicadas"></div>
                                           
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 hasta el 2010</td>																								
			                                        </tr>												
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                                                <div id="chartContainerProvincialesStakedAdjudicado" style="height: 720px; width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Municipalidades Provinciales con OxI Concluido 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaProvinciasConcluidas"></div>
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>														
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                                                <div id="chartContainerProvincialesStakedConcluido" style="height: 720px; width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>           
                            </div>
                        </div>
                        <div class="tab-pane fade" id="distritos">
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div id="idTablaciprlGraficoDistritos"></div>
                                     <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Evolución CIPRL y monto utilizado en Municipalidades Distritales en Millones de Soles
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center" width= "10%"><b>Año</b></th>
                                                        <th class="text-center" width= "35%"><b>Tope CIPRL</b></th>
                                                        <th class="text-center" width= "30%"><b>Monto Utilizado</b></th>
                                                        <th class="text-center" width= "30%"><b>%</b></th>
                                                  </tr>	
                                                </thead>
                                                <tr>                                                	
                                                     <th class="text-center">2011</th>
                                                     <th class="text-right"> 18,956.35</th>
                                                     <th class="text-right"> 5.34</th>
                                                     <th class="text-right">0.03</th>
                                                     
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2012</th>
                                                     <th class="text-right">18,593.46</th>
                                                     <th class="text-right">142.66</th>
                                                     <th class="text-right">0.77</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2013</th>
                                                     <th class="text-right">17,548.33</th>
                                                     <th class="text-right">268.66</th>
                                                     <th class="text-right">1.53</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2014</th>
                                                     <th class="text-right">18,195.35</th>
                                                     <th class="text-right">243.06</th>
                                                     <th class="text-right">1.34</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2015</th>
                                                     <th class="text-right">15,480.23</th>
                                                     <th class="text-right">114.47</th>
                                                     <th class="text-right">0.74</th>
                                                </tr>	                   
                                                <tr>
                                                     <th class="text-center">2016</th>
                                                     <th class="text-right">11,953.96</th>
                                                     <th class="text-right">259.78</th>
                                                     <th class="text-right">2.17</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2017</th>
                                                     <th class="text-right">10,639.81</th>
                                                     <th class="text-right">298.10</th>
                                                     <th class="text-right">2.80</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2018</th>
                                                     <th class="text-right">7,476.87</th>
                                                     <th class="text-right">236.13</th>
                                                     <th class="text-right">3.16</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2019*</th>
                                                     <th class="text-right">9,036.28</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center bold vert-middle">Total</th>
                                                     <th class="text-right bold vert-middle">127,880.64</th>
                                                     <th class="text-right bold vert-middle">1439.21</th>
                                                     <th class="text-right bold vert-middle">1.13</th>
                                                </tr>
                                          </table>
                                        </div>
                                        <div class="col-8">
                                            <div id="container2"  style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ranking de Municipalidades Distritales (MD) por monto de inversión en obras por impuestos 2009 - 2019
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                     Inversión OxI de Municipalidades Distritales (Adjudicados)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerDistritalesAdjudicadas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em;">
                                                            Actualmente 33 (1.76%) Municipalidades Distritales (MD) de un total de 1874 tienen adjudicado 51 proyectos por un monto de 444.22 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Municipalidades Distritales(MD)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado" target="blank">1874</a></th>
                                                                    <th class="text-center ">33</th>
                                                                    <th class="text-center ">1841</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">51</th>
                                                                    <th class="text-center ">51</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">444.22</th>
                                                                    <th class="text-center ">444.22</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">1.76%</th>
                                                                    <th class="text-center ">98.24%</th>																							
                                                                </tr>
                                                            </table>	
                                                        </div>
                                                    </div>
                                                </div>        
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Municipalidades Distritales (Concluidos)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerDistritosConcluidas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>                                                       
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                             Actualmente 96 (14.28%) Municipalidades Distritale (MD) de un total de 1874 han concluido 123 proyectos por un monto de 994.99 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <table class="table table-sm table-detail">
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Municipalidades Distritales (MD)</th>
												                    <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">1874</a></th>
                                                                    <th class="text-center ">96</th>
                                                                    <th class="text-center ">1778</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">123</th>
                                                                    <th class="text-center ">123</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">994.99 </th>
                                                                    <th class="text-center ">0</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">5.12%</th>
                                                                    <th class="text-center ">94.88%</th>																							
                                                                </tr> 
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>                                   
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Municipalidades Distritales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaDistritosAdjudicadas"></div>
                                           
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI del año 2009</td>																								
			                                        </tr>												
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                                                <div id="chartContainerDistritalesStakedAdjudicado" style="height: 720px; width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Municipalidades Distritales con OxI Concluido 2015 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <div id="idtablaDistritosConcluidas"></div>
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>														
	                                            </table>											
	                                        </div>
                                         </div>   
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="scrollable" style="overflow-y: auto;max-height: 750px;">
                                                <div id="chartContainerDistritalesStakedConcluido" style="height: 1820px; width: 100%"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>           
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ministerios">
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ranking de Ministerios Nacionales (MIN) por monto de inversión en obras por impuestos(OxI) 2015 - 2018
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                     Inversión OxI de Ministerios (Adjudicados)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerMinisterios" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>                                                       
                                                        <div class="col-12" style="padding-top:1em;">
                                                            Solo 7 (36.84%) Ministerios (MIN) de un total de 19, han adjudicado 30 proyectos por un monto de 640 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                        <?php $excelEmpresas = dropDownList((object) ['method' => 'ministeriosOXIExcelAdjudicados']);?>
                                                            <?php  
                                                                $temp=0;
                                                                $temp1=0;
                                                                $temp2=0;
                                                                $temp3=0;
                                                                $temp4=0;
                                                                $temp5=0;
                                                            ?>
                                                            <?php foreach ($excelEmpresas as $key){ 										
												                $temp+=$key->suma2015;
												                $temp1+=$key->suma2016;
												                $temp2+=$key->suma2017;
                                                                $temp3+=$key->suma2018;	
                                                                									
                                                                $temp4+=$key->total;
                                                        	
                                                            } ?>   
                                                            <table class="table table-sm table-detail">																																				
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Ministerios</th>
												                    <th class="text-center"><a href="http://www.pcm.gob.pe/entidades-pcm/" target="blank">19</a></th>
                                                                    <th class="text-center ">7</th>
                                                                    <th class="text-center ">12</th>																								
                                                                </tr>
                                                              
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">30</th>
                                                                    <th class="text-center ">30</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">640</th>
                                                                    <th class="text-center ">640</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">36.84%</th>
                                                                    <th class="text-center ">63.16%</th>																								
                                                                </tr>
								                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Ministerios (Concluidos)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerMinisterios1" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>
                                                        <br>
                                                        <div class="col-12" style="padding-top:1em">
                                                             Solo 2 (10.52%) Ministerios (MIN) de un total de 19, han concluido 3 proyectos por un monto de 9.4 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2018
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                        <?php $excelEmpresas = dropDownList((object) ['method' => 'ministeriosOXIExcelAdjudicados']);?>
                                                            <?php  
                                                                $temp=0;
                                                                $temp1=0;
                                                                $temp2=0;
                                                                $temp3=0;
                                                                $temp4=0;
                                                                $temp5=0;
                                                            ?>
                                                            <?php foreach ($excelEmpresas as $key){ 										
												                $temp+=$key->suma2015;
												                $temp1+=$key->suma2016;
												                $temp2+=$key->suma2017;
												                $temp3+=$key->suma2018;										
                                                                $temp4+=$key->total;
                                                                $temp5+=$key->suma2019;
                                                        	
                                                            } ?>   
                                                            <table class="table table-sm table-detail">																																				
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Ministerios</th>
												                    <th class="text-center"><a href="http://www.pcm.gob.pe/entidades-pcm/" target="blank">19</a></th>
                                                                    <th class="text-center ">2</th>
                                                                    <th class="text-center ">17</th>																								
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">3</th>
                                                                    <th class="text-center ">3</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">9.4</th>
                                                                    <th class="text-center ">9.4</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">10.52%</th>
                                                                    <th class="text-center ">89.48%</th>																								
                                                                </tr>
								                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                       
                                        <!-- <div class="col-lg-12" style="padding-top:1em;">
                                                <div id="charcontainerMin" style="height: 320px; width: 100%"></div>
                                        </div> -->
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ministerios con OxI Adjudicado 2015 - 2018 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center bold vert-middle">#</th>
                                                        <th class="text-center bold vert-middle">Abreviatura</th>
                                                        <th class="text-center bold vert-middle">Ministerio</th>
                                                        
                                                        <th class="text-center bold vert-middle">2016</th>
                                                        <th class="text-center bold vert-middle">2017</th>
                                                        <th class="text-center bold vert-middle">2018</th>
                                                        <th class="text-center bold vert-middle">2019*</th>
                                                        <th class="text-center bold vert-middle">Total</th>
                                                    </tr>	
                                                </thead>
                                                    <?php foreach ($excelEmpresas as $key){ ?>
                                                    <tr>
												        <td class="text-center"><?php $temp5++;  echo ($temp5)?></td>                                                                                                  
                                                        <td class="text-left"><?php     echo $key->nombre?></td>
                                                        <td class="text-left"><?php     echo $key->abreviatura?></td>   
												        <td class="text-right"><?php    echo ($key->suma2016)?></td>
												        <td class="text-right"><?php    echo ($key->suma2017)?></td>
												        <td class="text-right"><?php    echo ($key->suma2018)?></td>
                                                        <td class="text-right"><?php    echo ($key->suma2019)?></td>											
                                                        <td class="text-right">
                                                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre?>" href="#" onclick="App.events(this); return false;">
                                                                    <?php    echo ($key->total)?>
													        </a>			                                                    
                                                        </td>
                                                    </tr>
                                                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre?>" style="display: none;">
                                                        <td colspan="9">
                                                            <div class="card">
														        <div class="card-header card-special">
															     Lista de Proyectos
                                                                </div>
                                                                <div class="card-body">
															        <div class="row">
																        <div class="col-lg-12">
                                                                            <table class="table table-sm table-detail">
                                                                               
																			        <tr>
                                                                                        <th class="text-center bold" width="5%">#</th>
																				        <th class="text-center bold" width="15%">Codigo Unico</th>
																				        <th class="text-center bold" width="40%">Nombre Proyecto</th>
																				        <th class="text-center bold" width="20%">Empresa</th>
                                                                                        <th class="text-center bold" width="5%">Monto</th>
                                                                                        <th class="text-center bold" width="15%">Fecha</th>

                                                                                    </tr>
                                                                                    <?php 
                                                                                        $estadoMinisterio='Adjudicado';
                                                                                        $tempMinisDet=0;
                                                                                        $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                                                    ?>
                                                                                    <?php foreach ($tabladet as $key){ ?>
                                                                                    <tr>
                                                                                        <td class="text-center" ><?php $tempMinisDet++;  echo ($tempMinisDet)?></td>
                                                                                        <td class="text-center" ><a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip;?>' target="blank"><?php echo $key->snip; ?></td>
                                                                                        <td class="text-left" ><?php    echo ucfirst($key->nombre_proyecto)?></td>
																				        <td class="text-left" ><?php    echo ($key->empresa)?></td>
                                                                                        <td class="text-right" ><?php    echo ($key->monto_inversion)?></td>
                                                                                        <td class="text-center" ><?php    echo ($key->fecha_buena_pro)?></td>
                                                                                    </tr>
                                                                                <?php }?>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td class="text-center"></td>
                                                        
                                                        <td class="text-center"></td>
												        <td class="text-left"><b>TOTAL</b></td>
												        
												        <td class="text-right"><?php echo ($temp1)?></td>
												        <td class="text-right"><?php echo ($temp2)?></td>
												        <td class="text-right"><?php echo ($temp3)?></td>
                                                        <td class="text-right">0</td>											
												        <td class="text-right"><?php echo ($temp4)?></td>																								
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center"></td>
												        
												        
												        <td class="text-right"></td>
												        <td class="text-right"></td>
												        <td class="text-right"></td>											
												        <td class="text-right"></td>																								
                                                    </tr>
                                            </table>
                                         </div>  
                                         <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2015</td>																								
			                                        </tr>												
	                                            </table>											
	                                     </div>         
                                       
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="chartContainerMinisterios2" style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ministerios con OxI Concluidos 2009 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center bold vert-middle">#</th>
                                                        <th class="text-center bold vert-middle">Ministerio</th>
                                                        <th class="text-center bold vert-middle">Abreviatura</th>
                                                        
                                                        <th class="text-center bold vert-middle">2016</th>
                                                        <th class="text-center bold vert-middle">2017</th>
                                                        <th class="text-center bold vert-middle">2018</th>
                                                        <th class="text-center bold vert-middle">2019</th>
                                                        <th class="text-center bold vert-middle">Total</th>
                                                    </tr>	
                                                </thead>
                                                    <?php $excelEmpresas1 = dropDownList((object) ['method' => 'ministeriosOXIExcelConcluidos']);
                                                        $temp=0;
                                                        $temp1=0;
                                                        $temp2=0;
                                                        $temp3=0;
                                                        $temp4=0;
                                                        $temp5=0;
                                                        $tempMinisDet=0;
                                                    ?>
                                                    <?php foreach ($excelEmpresas1 as $key){                                                         
                                                        
												        $temp1+=$key->suma2016;
												        $temp2+=$key->suma2017;
												        $temp3+=$key->suma2018;										
                                                        $temp4+=$key->total;
                                                        $temp5+=$key->suma2019;
                                                        ?>
                                                    <tr>
												        <td class="text-center"><?php $temp5++;  echo ($temp5)?></td>
                                                        <td class="text-left"><?php     echo $key->nombre?></td>
                                                        <td class="text-left"><?php     echo $key->abreviatura?></td>                                             
												    
												        <td class="text-right"><?php    echo ($key->suma2016)?></td>
												        <td class="text-right"><?php    echo ($key->suma2017)?></td>
												        <td class="text-right"><?php    echo ($key->suma2018)?></td>
                                                        <td class="text-right"><?php    echo ($key->suma2019)?></td>											
                                                        <td class="text-right"><a href=""></a>
                                                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre?>" href="#" onclick="App.events(this); return false;">
                                                                    <?php    echo ($key->total)?>
													        </a>			                                                    
                                                        </td>
                                                    </tr>
                                                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre?>" style="display: none;">
                                                        <td colspan="9">
                                                            <div class="card">
														        <div class="card-header card-special">
															        Lista de Proyectos
                                                                </div>
                                                                <div class="card-body">
															        <div class="row">
																        <div class="col-lg-12">
                                                                            <table class="table table-sm table-detail">
                                                                                <thead>
																			        <tr>
                                                                                        <th class="text-center bold" width="5%">#</th>
																				        <th class="text-center bold" width="15%">Codigo Unico</th>
																				        <th class="text-center bold" width="40%">Nombre Proyecto</th>
																				        <th class="text-center bold" width="20%">Empresa</th>
                                                                                        <th class="text-center bold" width="5%">Monto</th>
                                                                                        <th class="text-center bold" width="15%">Fecha</th>

                                                                                    </tr>
                                                                                </thead>
                                                                                <?php 
                                                                                    $estadoMinisterio='Concluido';
                                                                                    $tempMinisDet=0;
                                                                                    $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                                                ?>
                                                                                <?php foreach ($tabladet as $key){ ?>
                                                                                    <tr>
                                                                                        <td class="text-center" ><?php $tempMinisDet++;  echo ($tempMinisDet)?></td>
                                                                                        <td class="text-center" ><?php    echo ($key->snip)?></td>
                                                                                        <td class="text-left" ><?php    echo ucfirst($key->nombre_proyecto)?></td>
																				        <td class="text-left" ><?php    echo ($key->empresa)?></td>
                                                                                        <td class="text-right" ><?php    echo ($key->monto_inversion)?></td>
                                                                                        <td class="text-center" ><?php    echo ($key->fecha_buena_pro)?></td>

                                                                                    </td>  

                                                                                <?php }?>
																		   
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>

                                                    <?php } ?>
                                                    <tr>
                                                        <td class="text-center"></td>
                                                        
                                                        <td class="text-center"></td>
												        <td class="text-left"><b>TOTAL</b></td>
												       
												        <td class="text-right"><?php echo ($temp1)?></td>
												        <td class="text-right"><?php echo ($temp2)?></td>
												        <td class="text-right"><?php echo ($temp3)?></td>
                                                        <td class="text-right">0</td>											
												        <td class="text-right"><?php echo ($temp4)?></td>																								
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center"></td>
												        
												        <td class="text-right"></td>
												        <td class="text-right"></td>
												        <td class="text-right"></td>
												        <td class="text-right"></td>											
												        <td class="text-right"></td>																								
                                                    </tr>
                                            </table>
                                         </div>
                                         <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2015</td>																								
			                                        </tr>												
	                                            </table>											
	                                     </div>     
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="chartContainerMinisterios3" style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                PMI por Ministerios 2015 - 2018 (Millones de Soles)
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                        <?php 
                                            $tempMinisDet=0;
                                            $tabladet = dropDownList((object) ['method' => 'ministeriosPMI']);
                                        ?>
                                            <table class="table table-sm table-detail">
                                                 <thead>
											        <tr>
                                                        <th class="text-center bold vert-middle" rowspan="2">#</th>
                                                        <th class="text-center bold vert-middle" rowspan="2">Ministerio</th>
												        <th class="text-center bold vert-middle" colspan="2" >2015</th>
												        <th class="text-center bold vert-middle" colspan="2">2016</th>
												        <th class="text-center bold vert-middle" colspan="2" >2017</th>
                                                        <th class="text-center bold vert-middle" colspan="2">2018</th>
                                                        <th class="text-center bold vert-middle" colspan="2">Promedio</th>
											        </tr>											
											        <tr>
                                                         
												        <th class="text-center bold vert-middle">M S/.</th>
                                                        <th class="text-center bold vert-middle">%</th>
												        <th class="text-center bold vert-middle">M S/.</th>
												        <th class="text-center bold vert-middle">%</th>
                                                        <th class="text-center bold vert-middle">M S/.</th>
                                                        <th class="text-center bold vert-middle">%</th>
                                                        <th class="text-center bold vert-middle">M S/.</th>
                                                        <th class="text-center bold vert-middle">%</th>
                                                        <th class="text-center bold vert-middle">M S/.</th>
                                                        <th class="text-center bold vert-middle">%</th>
                                                        
											        </tr>	
										        </thead> 
                                                 <?php foreach ($tabladet as $key){ ?>
                                                    <tr>
                                                        <td class="text-center" ><?php $tempMinisDet++;  echo ($tempMinisDet)?></td>
                                                        
                                                        <td class="text-left" ><?php    echo ucfirst(strtolower($key->ministerio))?></td>
                                                        <td class="text-right"><?php    echo number_format($key->s2015)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->p2015)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->s2016)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->p2016)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->s2017)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->p2017)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->s2018)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->p2018)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->promedio)?></td>
                                                        <td class="text-right"><?php    echo number_format($key->promediop)?></td>

                                                    </td>  

                                                <?php }?>                                                                 
                                            </table>
                                        </div>                                          
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="universidades">
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Evolución CIPRL y monto utilizado en Universidades Nacionales en Millones de Soles
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center" width= "10%"><b>Año</b></th>
                                                        <th class="text-center" width= "35%"><b>Tope CIPRL</b></th>
                                                        <th class="text-center" width= "30%"><b>Monto Utilizado</b></th>
                                                        <th class="text-center" width= "30%"><b>%</b></th>
                                                  </tr>	
                                                </thead>                                               
                                                <tr>
                                                     <th class="text-center">2014</th>
                                                     <th class="text-right">1,066.53</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0.00</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2015</th>
                                                     <th class="text-right">1,107.33</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0.00</th>
                                                </tr>	                   
                                                <tr>
                                                     <th class="text-center">2016</th>
                                                     <th class="text-right">975.88</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0.00</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2017</th>
                                                     <th class="text-right">686.47</th>
                                                     <th class="text-right">0.00</th>
                                                     <th class="text-right">0.00</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2018</th>
                                                     <th class="text-right">736.90</th>
                                                     <th class="text-right">12.38</th>
                                                     <th class="text-right">1.68</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center">2019*</th>
                                                     <th class="text-right">956.40</th>
                                                     <th class="text-right">18.43</th>
                                                     <th class="text-right">1.93</th>
                                                </tr>
                                                <tr>
                                                     <th class="text-center bold vert-middle">Total</th>
                                                     <th class="text-right bold vert-middle">5,529.51</th>
                                                     <th class="text-right bold vert-middle">30.81</th>
                                                     <th class="text-right bold vert-middle">0.56</th>
                                                </tr>
                                          </table>
                                        </div>
                                        <div class="col-8">
                                            <div id="container3"  style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>                                              
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Ranking de Universidades Nacionales (UN) por monto de inversión en obras por impuestos(OxI) 2018 - 2019
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Universidades (Adjudicados)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div id="chartContainerUniversidades" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                                        </div>                                                       
                                                        <div class="col-12" style="padding-top:1em;">
                                                            Solo 3 (5.89%) Universidades Nacionales (UN) de un total de 51 han adjudicado 4 proyectos por un monto de 30.81 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                                                            <?php  
                                                                $excelEmpresas = dropDownList((object) ['method' => 'UniversidadesOXIExcelAjudicaods']);
                                                                                    
                                                                $temp2=0;
                                                                $temp3=0;
                                                                $temp4=0;                                                           
                                                            ?>
                                                            <?php foreach ($excelEmpresas as $key){ 										
												                $temp2+=$key->suma4;
												                $temp3+=$key->suma5;										
                                                                $temp4+=$key->total;
                                                        	
                                                            } ?>   
                                                            <table class="table table-sm table-detail">																																				
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Universidades</th>
												                    <th class="text-center"><a href="https://www.sunedu.gob.pe/universidades-publicas/" target="blank">51</a></th>
                                                                    <th class="text-center ">3</th>
                                                                    <th class="text-center ">49</th>																								
                                                                </tr>
                                                              
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">4</th>
                                                                    <th class="text-center ">4</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">30.81</th>
                                                                    <th class="text-center ">30.81</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">5.89%</th>
                                                                    <th class="text-center ">94.11%</th>																								
                                                                </tr>
								                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="card">
                                                <div class="card-header card-white text-center font-weight-bold">
                                                    Inversión OxI de Universidades (Concluidos)
                                                </div>
                                                <span style="text-align:center;">(Millones de soles)</span>
                                                <div class="card-body">
                                                        <div class="col-12" style="padding-top:26.5em;">
                                                            0 Universidades Nacionales (UN) de un total de 51 no han concluido proyectos mediante el mecanismo OxI en el periodo 2015 al 2019
                                                        </div>
                                                        <div class="col-12" style="padding-top:1em">
                
                                                            <table class="table table-sm table-detail">																																				
                                                                <tr>
                                                                    <th class="text-center bold" ></th>
												                    <th class="text-center bold" width="40px">Total</th>
                                                                    <th class="text-center bold" width="60px">OxI</th>
                                                                    <th class="text-center bold">Sin OxI</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Universidades</th>
												                    <th class="text-center"><a href="https://www.sunedu.gob.pe/universidades-publicas/" target="blank">51</a></th>
                                                                    <th class="text-center ">0</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                              
                                                                <tr>
                                                                    <th class="text-center bold">Cantidad de Proyectos</th>
												                    <th class="text-center">0</th>
                                                                    <th class="text-center ">0</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
												                    <th class="text-center">0</th>
                                                                    <th class="text-center ">0</th>
                                                                    <th class="text-center ">0</th>																								
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center bold">Total Inversión</th>
												                    <th class="text-center">100%</th>
                                                                    <th class="text-center ">0%</th>
                                                                    <th class="text-center ">0%</th>																								
                                                                </tr>
								                            </table>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Universidades Nacionales con OxI Adjudicado 2018 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>																				
                                                    <tr>
                                                        <th class="text-center bold vert-middle">#</th>
                                                        <th class="text-center bold vert-middle">Region</th>
                                                        <th class="text-center bold vert-middle">Universidad</th>
                                                        <th class="text-center bold vert-middle">2018</th>
                                                        <th class="text-center bold vert-middle">2019</th>
                                                        <th class="text-center bold vert-middle">Total</th>
                                                    </tr>	
                                                </thead>
                                                    
                                                    <?php foreach ($excelEmpresas as $key){ ?>
                                                    <tr>
												        <td class="text-center"><?php $temp5++;  echo $key->id ?></td>
                                                        <td class="text-left"><?php     echo $key->region?></td>                                             
                                                        <td class="text-left"><?php     echo $key->nombre?></td>
												        <td class="text-right"><?php    echo ($key->suma4)?></td>
												        <td class="text-right"><?php    echo ($key->suma5)?></td>											
                                                        <td class="text-right"><a href=""></a>
                                                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre?>" href="#" onclick="App.events(this); return false;">
                                                                    <?php    echo ($key->total)?>
													        </a>			                                                    
                                                        </td>
                                                    </tr>
                                                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre?>" style="display: none;">
                                                    <td colspan="8">
                                                        <div class="card">
														    <div class="card-header card-special">
															    Lista de Proyectos
                                                            </div>
                                                            <div class="card-body">
															    <div class="row">
																    <div class="col-lg-12">
                                                                        <table class="table table-sm table-detail">
                                                                            <thead>
																			    <tr>
                                                                                    <th class="text-center bold" width="5%">#</th>
																				    <th class="text-center bold" width="15%">Codigo Unico</th>
																				    <th class="text-center bold" width="40%">Nombre Proyecto</th>
																				    <th class="text-center bold" width="20%">Empresa</th>
                                                                                    <th class="text-center bold" width="5%">Monto</th>
                                                                                    <th class="text-center bold" width="15%">Fecha</th>

                                                                                </tr>
                                                                                <?php 
                                                                                    $estadoMinisterio='Adjudicado';
                                                                                    $tempMinisDet=0;
                                                                                    $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                                                ?>
                                                                                <?php foreach ($tabladet as $key){ ?>
                                                                                <tr>
                                                                                    <td class="text-center" ><?php $tempMinisDet++;  echo ($tempMinisDet)?></td>
                                                                                    <td class="text-center" ><a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip;?>' target="blank"><?php echo $key->snip; ?></td>
                                                                                    <td class="text-left" ><?php    echo ucfirst($key->nombre_proyecto)?></td>
																				    <td class="text-left" ><?php    echo ($key->empresa)?></td>
                                                                                    <td class="text-right" ><?php    echo ($key->monto_inversion)?></td>
                                                                                    <td class="text-center" ><?php    echo ($key->fecha_buena_pro)?></td>

                                                                                </td>  

                                                                                <?php }?>
																		    </thead>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td class="text-center"></td>
                                                        
                                                        <td class="text-center"></td>
												        <td class="text-left"><b>TOTAL</b></td>
												        <td class="text-right"><?php echo ($temp2)?></td>
												        <td class="text-right"><?php echo ($temp3)?></td>											
												        <td class="text-right"><?php echo ($temp4)?></td>																								
                                                    </tr>
                                                    <tr>
                                                        <td class="text-center"></td>
												        
												        <td class="text-right"></td>
												        <td class="text-right"></td>
												        <td class="text-right"></td>
												        <td class="text-right"></td>											
												        <td class="text-right"></td>																								
                                                    </tr>
                                            </table>
                                         </div>           
                                         <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2017</td>																								
			                                        </tr>												
	                                            </table>											
	                                     </div>         
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-12">
                                            <div id="chartContainerUniversidades1" style="height: 320px; width: 100%"></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                            Universidades Nacionales con OxI Concluidos 2015 - 2019 (Millones de Soles)
                                            </div>
                                         </div>
                                         <div class="col-lg-12">
                                            No posee Ninguna Obra por Impuesto Concluida
                                            <div class="offset-md-2 col-lg-8">
		                                        <h3><b>Leyenda</b></h3>
		                                        <table class="table table-sm table-detail">																										
			                                        <tr>
				                                        <td class="text-center bold" style="width:20%"><b> * </b></th>
				                                        <td class="text-center ">Al Primer Trimestre (2019)</td>																								
                                                    </tr>
                                                    <tr>
				                                        <td class="text-center bold" style="width:20%">Nota</th>
				                                        <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2019</td>																								
			                                        </tr>												
	                                            </table>											
	                                     </div>         
                                         </div>     
                                    </div>
                                    <br>                                   
                                </div>
                            </div>
                        </div> 
                        <div class="tab-pane fade" id="politica"> 
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="card-header card-special text-center font-weight-bold">
                                                Resultado de Elecciones de Congresistas por Partido Político 
                                                <br>
                                                
                                            </div>
                                            <div class="col-lg-12" style="text-align:center">
                                            (Elecciones Presidenciales 2016)
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                        <?php  $map3 = dropDownList((object) ['method' => 'partidosPoliticos']); ?>
                                        <table class="table table-sm table-detail">
                                            <tr>
                                                <th class="text-center bold" width="5%"><input type="checkbox" id="cbox_1" name="checkbox_PartidoTotal" checked></th>
					                            <th class="text-center bold" width="5%">#</th>
					                            <th class="text-center bold" width="80%">Partido Politico</th>
                                                <th class="text-center bold" width="5%">Congresistas</th>
                                                <th class="text-center bold" width="30%">Votantes</th>
                                            </tr>
                                            <?php  $j=0;
                                            foreach ($map3 as $key){  $j++ ;?>
                                            <tr>
                                                <td class="text-center" ><input type="checkbox" id="cbox_1<?php echo $key->nombre?>" name="checkbox_Partido" checked></td>
                                                <td class="text-center" ><?php    echo ($j)?></td>
                                                <td class="text-left" ><?php echo ucfirst(strtolower($key->nombre))?></td>
                                                <td class="text-center">
													<a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo ($key->id)?>" href="#" onclick="App.events(this); return false;">
														<?php echo $key->cantidad;?>
													</a>													
                                                </td>
                                                <td class="text-right" ><?php echo number_format($key->poblacion)?></td>  
                                            </tr>
                                            <tr data-target="lnkProvXrutas_<?php echo ($key->id)?>" style="display: none;">
												<td colspan="5">
													<div class="card">
														<div class="card-header card-special">
															Relación de Congresistas por Partido Político
														</div>
														<div class="card-body">
															<div class="row">
																<div class="col-lg-12">
																	<table class="table table-sm table-detail">
																		<thead>
																			<tr>
																				<th class="text-center bold" width="5%">#</th>
                                                                                <th class="text-center bold" width="35%">Apellidos</th>
                                                                                <th class="text-center bold" width="45%">Población</th>
																			</tr>
																		</thead>
																		<tbody>
																			<?php																				
																				$distXoleoducto = dropDownList((object) ['method' => 'congresistasXPartido', 'descripcion' => $key->id]);																				
                                                                                $i=0;
                                                                                foreach ($distXoleoducto as $item){	
                                                                                    $i++;																				
																			?>
																			<tr>
                                                                                <td class="text-left" ><?php    echo ($i)?></td>
                                                                                <td class="text-left" ><?php    echo ($item->apellidos)?></td>
                                                                                <td class="text-right" ><?php echo number_format($item->votantes)?></td>									
																			</tr>
																			<?php } ?>
																			
																		</tbody>
																	</table>
																</div>
															</div>														
														</div>
													</div>													
												</td>
											</tr>
                                            <?php }  ?>
                                            <tr>
                                                <th class="text-center bold" width="5%">Total</th>
					                            <th class="text-center bold" width="5%"></th>
					                            <th class="text-center bold" width="5%"></th>
                                                <th class="text-right bold" width="50%">130</th>
                                                <th class="text-center bold" width="50%">3,953,178</th>
                                            </tr>
                                        </table>
                                        <h6>Fuente: <a href="http://www.congreso.gob.pe/pleno/congresistas/" target="blank">http://www.congreso.gob.pe/pleno/congresistas/</a></h6>                                         
                                        </div>
                                        <div class="col-lg-6">
                                            <div id="map3" style="height:450px; width:100%;"></div>    
                                        </div>
                                        
                                    </div> 
                                    <br> 
                                    <div id="divCongresistas">
                                        <img src="assets/app/img/loading.gif"  alt="Smiley face" height="42" width="42">                                                  
                                    </div>   
                                                            
                                </div> 
                            </div> 
                        </div>  
                        <div class="tab-pane fade" id="proyecto"> 
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div id="proyectosrpd"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="oportunidad"> 
                            <div class="card card-outline-info">
                                <div class="card-body">
                                    <div id="proyectosopo"></div>
                                </div>
                            </div>
                        </div>                                                          
                    </div>
                </div>
            </div>
        </div>		
    </div>
</div>
</div>
