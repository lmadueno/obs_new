<?php
	require 'php/app.php';
	
	if(count($_GET) > 0){
		if($_GET['data']){
			$data = json_decode($_GET['data']);
			$obj    = dropDownList((object) ['method' => 'DatosXubigeo', 'codnivel' => 1, 'codUbigeo' => $data->coddpto]);			
			
			if(count((array)$obj) > 0){
				$obj = $obj{0};
?>
	<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
	</style>
	<div class="card card-outline-info">			
		<div class="card-header">
		    <div class="row">
                <div class="col-lg-11">
					<h6 class="m-b-0 text-white">Información de la universidad</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOUniversidas" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOUniversidas" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
        </div>	
		<div class="card-body">				
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-sm">
						<tr>
							<td class="bold" width="18%">Región</td>
							<td>
								<?php 
									echo ucwords(strtolower($obj->region)); 
									$route = 'assets/app/pdf/' . $obj->codnivel . '_' . $obj->codubigeo;
									
									if($obj->codnivel == 1){										
								?>								
									<?php if (file_exists($route .'.pdf')) { ?>
										<a href="<?php echo $route . '.pdf';?>" target="_blank" title="Educación"><img src="assets/app/img/pdf.png"/></a>
									<?php } ?>															
								<?php } ?>								
							<td class="align-middle bold" width="18%">Ubigeo</td>
							<td class="text-center">
								<?php echo $obj->ubigeo_dept; ?>
							</td>
						</tr>
						<tr>
							<td class="bold">Autoridad</td>
							<td><?php echo ucwords(strtolower($obj->autoridad)); ?></td>
							<td class="bold">Cargo</td>
							<td><?php echo ucfirst(strtolower($obj->cargo)); ?></td>
						</tr>
						<tr>
							<td class="bold">Población</td>
							<td class="text-right"><?php echo number_format($obj->poblacion); ?> (<?php echo number_format((float)$obj->porc_poblacion, 2, '.', '')?>%)</td>
							<td class="bold">Población Nac.</td>
							<td class="text-right"><?php echo number_format($obj->poblacion_n); ?></td>
						</tr>
						<tr>
							<td class="bold">Electores</td>
							<td class="text-right"><?php echo number_format($obj->electores); ?> (<?php echo number_format((float)$obj->porc_electores, 2, '.', '')?>%)</td>
							<td class="bold">Electores Nac.</td>
							<td class="text-right"><?php echo number_format($obj->electores_n); ?></td>
						</tr>
						<tr>
							<td class="bold">Partido</td>
							<td colspan="3"><?php echo ucwords(strtolower($obj->partido)); ?></td>
						</tr>
						<tr>
							<td class="text-center" colspan="4">Datos de la universidad</td>							
						</tr>
						<tr>
							<td class="bold">Nombre</td>
							<td colspan="3">
								<?php echo ucwords(strtolower($data->universidad)); ?>
							</td>							
						</tr>
						<tr>
							<td class="bold">F. de creación</td>
							<td>
								<?php echo $data->fechacreacion?>
							</td>
							<td class="bold">Región / Provincia</td>
							<td>
								<?php echo ucwords(strtolower($data->departamento)) . '/ ' . ucwords(strtolower($data->provincia))?>
							</td>
						</tr>
						<tr>
							<td class="bold">Nro. Documento</td>
							<td>
								<?php echo $data->dispositivolegal?>
							</td>
							<td class="bold">Licenciamiento</td>
							<td>
								<?php echo $data->licenciamiento?>
							</td>
						</tr>						
					</table>								
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Evolución de Canon y Tope CIPRL
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<div id="chart_<?php echo $data->coduniv;?>" style="min-width: 310px; height: 220px; margin: 0 auto"></div>	
								</div>
							</div>														
						</div>
						<div class="card-footer text-center">
							<a class="lnkAmpliar" data-event="lnkCanonCiprl" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar tabla resumen
							</a>
						</div>
						<div data-target="lnkCanonCiprl" class="card-body" style="display: none;">
							<div class="row">
								<div class="offset-md-2 col-lg-8">
									<table class="table table-sm table-detail">
										<?php $ciprlXunivDet = dropDownList((object) ['method' => 'ciprlXunivDet', 'coduniv' => $data->coduniv]);?>
										<thead>										
											<tr>
												<th class="text-center bold">Año</th>
												<th class="text-center bold">Canon MEF</th>
												<th class="text-center bold">Ciprl MEF</th>												
											</tr>	
										</thead>
										<tbody>
										<?php foreach ($ciprlXunivDet as $key){ ?>
											<tr>
												<td class="text-center"><?php echo $key->ano?></td>
											    <?php if($key->ano==2021) { ?>
												<td class="text-right"><?php echo number_format($key->canon_mef)?></td>
												<td class="text-right"><?php echo number_format($key->ciprl_mef)?>*</td>
												<?php } else{ ?>
												<td class="text-right"><?php echo number_format($key->canon_mef)?></td>
												<td class="text-right"><?php echo number_format($key->ciprl_mef)?></td>
											  <?php } ?>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
								<div class="offset-md-2 col-lg-8">
										<h3>Leyenda</h3>
										<table class="table table-sm table-detail">																											
											<tr>
												<td class="text-center bold"><b> * </b></th>
												<td class="text-center ">Al Primer Trimestre (2021)</td>																								
											</tr>												
									</table>	
											
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<?php $proyAdjudicados = dropDownList((object) ['method' => 'UniversidadesAdjudicados', 'codubigeo' => $data->universidad]);?>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Lista de Proyectos Concluidos y Adjudicados 2009 - 2019 (Millones de Soles)
						</div>
						<?php if(count($proyAdjudicados) > 0){ ?>
						<div class="card-body">
							<div class="scrollable" style="overflow-y: auto;max-height: 240px;">								
								<?php foreach ($proyAdjudicados as $key){ ?>
									<table class="table table-sm">
										<tr>
											<td class="bold" width="25%">Nombre del Proyecto</td>
											<td colspan="3" align="justify"><?php echo ucfirst($key->nombre_proyecto); ?></td>																			
										</tr>
										<tr>
											<td class="bold"><a href='https://ofi5.mef.gob.pe/invierte/formato/verProyectoCU/<?php echo $key->snip;?>' target="blank">Codigo Unico</a> <a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip;?>' target="blank">(SNIP)</a></td>
											<td width="20%"><?php echo $key->snip; ?></td>
											<td class="bold" width="20%">Empresa</td>
											<td><?php echo $key->empresa; ?></td>
										</tr>
										<tr>
											<td class="bold">Sector</td>
											<td><?php echo $key->sector; ?></td>
											<td class="bold">F. Buena Pro</td>
											<td><?php echo $key->fecha_buena_pro; ?></td>
										</tr>
										<tr>
											<td class="bold">Monto de Inversión</td>
											<td class="text-right"><?php echo $key->monto_inversion; ?>MM</td>
											<td class="bold">F. Firma de Convenio</td>
											<td><?php echo $key->fecha_firma; ?></td>
										</tr>
										<tr>
											<td class="bold">Población Beneficiada</td>
											<td class="text-right"><?php echo number_format($key->poblacion_beneficiada)	; ?></td>
											<td class="bold">Estado</td>
											<td><?php echo $key->estado; ?></td>
										</tr>
									</table>
								<?php } ?>
							</div>
						</div>
						
							<?php } else{ ?>

							<div class="card-body">
								La entidad no ha aplicado el mecanismo OxI.
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<br>


	<?php $proyPriorizados = dropDownList((object) ['method' => 'UniversidadesPriorizadas', 'codubigeo' => $data->universidad]);?>
			
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Lista de Proyectos Priorizados 2009 - 2019 (Millones de Soles)
						</div>
						<?php if(count($proyPriorizados) > 0){ ?>
						<div class="card-body">
							<div class="scrollable" style="overflow-y: auto;max-height: 240px;">								
								<?php foreach ($proyPriorizados as $key){ ?>
									<table class="table table-sm">
										<tr>
											<td class="bold" width="25%">Nombre del Proyecto</td>
											<td colspan="3" align="justify"><?php echo ucfirst($key->nombre_proyecto); ?></td>																			
										</tr>
										<tr>
											<td class="bold"><a href='https://ofi5.mef.gob.pe/invierte/formato/verProyectoCU/<?php echo $key->coigounico;?>' target="blank">Codigo Unico</a> <a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->coigounico;?>' target="blank">(SNIP)</a></td>
											<td width="20%"><?php echo $key->coigounico;?></td>
											<td class="bold" width="20%">Sector</td>
											<td><?php echo $key->sector; ?></td>
										</tr>
										
										<tr>
											<td class="bold">Monto de Inversión (S/.)</td>
											<td class="text-right"><?php echo $key->monto; ?>MM</td>
											<td class="bold">Acuerdo</td>
											<td><?php echo $key->acuerdo; ?></td>
										</tr>
										
									</table>
								<?php } ?>
							</div>
						</div>
						
							<?php } else{ ?>

							<div class="card-body">
									La entidad no ha priorizado ningún proyecto OxI.
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<br>



			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Distribución de proyectos por función
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="card">
										<div class="card-header card-white text-center font-weight-bold">
											Formulación
										</div>
										<div class="card-body">
											<div id="pieRegion" style="height: 110px; margin: 0 auto"></div>
										</div>
										<div class="card-footer text-center font-weight-bold">
											-
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card">
										<div class="card-header card-white text-center font-weight-bold">
											Perfil Viable
										</div>
										<div class="card-body">
											<div id="pieDistrito" style="height: 110px; margin: 0 auto"></div>
										</div>
										<div class="card-footer text-center font-weight-bold">
											-
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card">
										<div class="card-header card-white text-center font-weight-bold">
											Expediente Técnico
										</div>
										<div class="card-body">
											<div id="pieProvincia" style="height: 110px; margin: 0 auto"></div>
										</div>
										<div class="card-footer text-center font-weight-bold">
											-
										</div>
									</div>
								</div>								
							</div>
						</div>
						<div class="card-footer text-center">
							<a class="lnkAmpliar" data-event="lnkFuncionXUbigeo" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar tabla resumen
							</a>
						</div>
						<div data-target="lnkFuncionXUbigeo" class="card-body" style="display: none;">
							<div class="row">
								<div class="col-lg-12">
									<table class="table table-sm table-detail">										
										<?php $ciprlXunivDet = dropDownList((object) ['method' => 'funcionXUbigeoChart', 'codnivel' => 1, 'codubigeo' => $data->coddpto]);?>
										<thead>										
											<tr>
												<th class="text-center bold">Función</th>
												<th class="text-center bold">Formulación</th>
												<th class="text-center bold">Perfil Viable</th>
												<th class="text-center bold">Expediente Técnico</th>
											</tr>	
										</thead>
										<tbody>
										<?php foreach ($ciprlXunivDet as $key){ ?>
											<tr>
												<td><?php echo ucfirst(strtolower($key->funcion))?></td>
												<td class="text-right"><?php echo number_format($key->fo)?></td>
												<td class="text-right"><?php echo number_format($key->pv)?></td>
												<td class="text-right"><?php echo number_format($key->et)?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
			}
		}
	}
?>