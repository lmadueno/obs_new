<?php
require 'php/app.php';

   $data = json_decode($_GET['data']);	
   
   $regi  = dropDownList((object) ['method' => 'consolidadototalOportxFun','tipo'=>$data->tipo1]);
 
?>


<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-12" style="text-align:center">
            <div class="row">
               
                    <div class="col-lg-12" style="text-align:center !important">
                            
                        <label class="radio-inline" style="padding-left:1em"><input type="radio" name="rbtnopor" id="rbtnFunProy" value="rbtnFunProy" <?php if($data->checked1==3){?>checked<?php } ?>>FUNCION</label>
                        <label class="radio-inline" style="padding-left:2em"><input type="radio" name="rbtnopor" id="rbtnDivProy" value="rbtnDivProy" <?php if($data->checked1==4){?>checked<?php } ?>>DIVISION FUNCIONAL</label>
                        <label class="radio-inline" style="padding-left:2em"><input type="radio" name="rbtnopor" id="rbtnGruProy" value="rbtnGruProy" <?php if($data->checked1==5){?>checked<?php } ?>>GRUPO FUNCIONAL</label>
                            
                    </div>
                    <div class="col-lg-12">
                        <table class="table table-sm table-detail">
                            <tr>
                                <th class="text-center bold" width="5%">#</th>
                                <?php if ($data->tipo1=='funcion'){?>
                                <th class="text-left bold" width="45%">FUNCION</th>
                                <?php }  ?>
                                <?php if ($data->tipo1=='division'){?>
                                <th class="text-left bold" width="45%">DIVISION FUNCIONAL</th>
                                <?php }  ?>
                                <?php if ($data->tipo1=='grupo'){?>
                                <th class="text-left bold" width="45%">GRUPO FUNCIONAL</th>
                                <?php }  ?>
                                <th class="text-right bold" width="25%">CANTIDAD</th>
                                <th class="text-right bold" width="25%">MONTO</th>
                            </tr>
                            <?php  
                                $i=0;
                                foreach ($regi  as $key){  $i++ ;    
                            ?>        
                            <tr>
                                <td class="text-center"><?php echo ($i)?></td>
                                <td class="text-left">
                                    <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->cod?>" href="#" onclick="App.events(this); return false;">
                                        <?php echo ($key->nom)?>
                                    </a>
                                </td>
                                <td class="text-right"><?php echo number_format($key->cantidad)?></td>
                                <td class="text-right"><?php echo number_format($key->monto)?></td>                               
                            </tr>   
                            <tr data-target="lnkProvXrutas_<?php echo $key->cod?>" style="display: none;">
                                <td colspan="4">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-sm table-detail">
												<tr>
                                                    <th class="text-left bold" width="45%">NOMBRE</th>
                                                    <th class="text-right bold" width="25%">CANTIDAD</th>
                                                    <th class="text-right bold" width="25%">MONTO</th>
                                                </tr>
                                                <?php $NivelGob = dropDownList((object) ['method' => 'oportunidadesXFuncionXNivelGob','tipo'=>$data->tipo1,'codfuncion'=>$key->cod]);?>
                                                <?php
                                                    foreach($NivelGob as $n1){
                                                ?>
                                                <tr>
                                                    <td class="text-lef">
                                                        <a class="lnkAmpliar_oportunidadDtella" data-event="lnkProvXrutas__<?php echo $n1->nivel?>" id="<?php echo $data->tipo1.'-'.$key->cod.'-'.$n1->nivel?>" href="#" onclick="App.events(this); return false;">
                                                            <?php echo ($n1->nom)?>
                                                        </a>
                                                    </td>
                                                    <td class="text-right"><?php echo number_format($n1->cantidad) ?></td>
                                                    <td class="text-right"><?php echo number_format($n1->monto) ?></td>
                                                </tr>
                                                <tr data-target="lnkProvXrutas__<?php echo $n1->nivel?>"  style="display: none;">
                                                    <td colspan="4">
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div id="tabladetallexfuncion_<?php echo $data->tipo1.$key->cod.$n1->nivel?>"></div>
                                                            </div>
                                                        </div>
                                                    </td>   
                                                </tr>  
                                                <?php }  ?>  
                                                
                                                      
                                            </table>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <?php }  ?>    
                        </table>
                    </div>
                    
            
            </div> 
        </div>
    </div>
</div>
