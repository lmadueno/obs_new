<?php
// CREATE PHPSPREADSHEET OBJECT
require 'php/spreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$data   = json_decode($_GET['data']);
$data1=1;
$sql="select geo.distrito as region,geo.codubigeo as codubigeo,ciprl,ind.valor as poblacion,canon,pim from observatorio.gen_distrito_geojson geo
inner join obs.gen_distritos_det det on geo.codubigeo=det.ubigeo and ano=2019
inner join obs.gen_indicadores_dist_detalle ind on ind.ubigeo = geo.codubigeo and ind.cod_indicador = '0301'";
$nombreExcel="Distritos.xlsx";	

$connect = pg_connect("host=localhost port=5432 dbname=colaboraccion user=postgres password=geoserver");
$result = pg_query($connect, $sql.' where '.$data->consulta_distrito );
pg_close($connect);

// CREATE A NEW SPREADSHEET + SET METADATA
$spreadsheet = new Spreadsheet();
 
// NEW WORKSHEET
$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle('Entidades');
$fecha=date("d/m/Y");

$rowCount = 3;

while ($item = pg_fetch_object($result)) {
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('B' . $rowCount, $item->codubigeo);
    $spreadsheet->setActiveSheetIndex(0)->setCellValue('C' . $rowCount, $item->region);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('D' . $rowCount, $item->poblacion);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . $rowCount, $item->ciprl);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $rowCount, $item->canon);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $rowCount, $item->pim);
	$rowCount++;
	
}

$cell_st =[
	'font' =>['bold' => true],
	'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
	'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
];

$spreadsheet->getActiveSheet()->getStyle('B2:G2')->applyFromArray($cell_st);

foreach (range('A','G') as $col) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);  
}
if($data1==2){
$sheet->setCellValue('C1','Entidades Regionales');
}else if($data1==5){
$sheet->setCellValue('C1','Entidades Provinciales');
}else if($data1==1){
$sheet->setCellValue('C1','Entidades Distritales');
}
$sheet
->setCellValue('A1','Fecha:')
->setCellValue('B1',$fecha);
$sheet
->setCellValue('B2', 'Código')
->setCellValue('C2', 'Entidad')
->setCellValue('D2', 'Población')
->setCellValue('E2', 'CIPRL')
->setCellValue('F2', 'CANON')
->setCellValue('G2', 'PIM');
 

// OUTPUT
$writer = new Xlsx($spreadsheet);

// OR FORCE DOWNLOAD
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$nombreExcel.'"');
header('Cache-Control: max-age=0');
header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');
$writer->save('php://output');
 
?>