<?php
require 'php/app.php';
$data = json_decode($_GET['data']);
?>
<?php
if ($data->tab == 'resumen-tab') {
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Fallecidos <?php echo $data->tipo; ?> a nivel nacional
				</div>
			</div>
		</div>
	</div>

	<?php if ($data->tipo == 'COVID') {  ?>
		<div class="row">
			<div class="col-lg-6">
				<div id="chartSinadef4"></div>

			</div>
			<div class="col-lg-6">
				<table class="table table-sm table-detail" width="100%">
					<thead>
						<tr>
							<th class="text-center bold" width="4%">#</th>
							<th class="text-center bold">Mes</th>
							<th class="text-center bold">Año 2020</th>
							<th class="text-center bold">Año 2021</th>
						</tr>
						<?php
						$i = 0;

						$sTotal = 0;
						$stotal = 0;
						$pipxUbigeoDet = dropDownList((object) ['method' => 'quintil_mes_covid']);
						foreach ($pipxUbigeoDet as $item) {
							$i++;

							$sTotal = $sTotal + $item->s2020;
							$stotal = $stotal + $item->s2021;
						?>
							<tr>
								<td class="text-center"><?php echo ($i) ?></td>
								<td class="text-center"><?php echo ($item->meses) ?></td>
								<td class="text-center"><?php echo ($item->s2020) ?></td>
								<td class="text-center"><?php echo ($item->s2021) ?></td>

							</tr>
						<?php } ?>
						<tr>
							<td class="text-center"></td>
							<td class="text-center">Total</td>
					
							<td class="text-center"><?php echo number_format($sTotal) ?></td>
							<td class="text-center"><?php echo number_format($stotal) ?></td>

						</tr>
					</thead>
				</table>
			</div>

		</div>
	<?php } else { ?>
		<div class="col-lg-12">
			<div class="col lg-4">
				<!-- chart de resumen covid -->
				<div id="chartSinadef4"></div>
			</div>
		</div>

	<?php } ?>

	<div class="col-lg-12">
		<?php
		if ($data->tipo == 'SINADEF') {
		?>
			<table class="table table-sm table-detail" width="100%">
				<thead>
					<tr>
						<th class="text-center bold vert-middle" colspan="2">General</th>

						<th></th>
						<th class="text-center bold vert-middle">2018</th>
						<th></th>
						<th class="text-center bold vert-middle">2019</th>
						<th></th>
						<th class="text-center bold vert-middle">2020</th>
						<th></th>
						<th class="text-center bold vert-middle">2021</th>

					</tr>

					<?php
					$i = 0;
					$s2017 = 0;
					$s2018 = 0;
					$s2019 = 0;
					$s2020 = 0;
					$s2021 = 0;


					$pipxUbigeoDet = dropDownList((object) ['method' => 'tabla__mes_sinadef']);
					foreach ($pipxUbigeoDet as $item) {
						$i++;

						$s2018 =  intval($item->anio2018) + intval($s2018);
						$s2019 =  intval($item->anio2019) + intval($s2019);
						$s2020 =  intval($item->anio2020) + intval($s2020);
						$s2021 =  intval($item->anio2021) + intval($s2021);


					?>
						<tr>
							<td class="text-center"><?php echo ($i) ?></td>
							<td class="text-center"><?php echo ($item->mes) ?></td>


							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2018) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2019) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2020) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2021) ?></td>


						</tr>
					<?php } ?>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">Total</td>


						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2018) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2019) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2020) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2021) ?></td>


					</tr>
				</thead>
			</table>
			<div id="chartSinadef5"></div>

			<table class="table table-sm table-detail" width="100%">
				<thead>
					<tr>
						<th class="text-center bold vert-middle" colspan="2">Semana</th>

						<th></th>
						<th class="text-center bold vert-middle">2018</th>
						<th></th>
						<th class="text-center bold vert-middle">2019</th>
						<th></th>
						<th class="text-center bold vert-middle">2020</th>
						<th></th>
						<th class="text-center bold vert-middle">2021</th>

					</tr>

					<?php
					$i = 0;
					$s2017 = 0;
					$s2018 = 0;
					$s2019 = 0;
					$s2020 = 0;
					$s2021 = 0;


					$pipxUbigeoDet = dropDownList((object) ['method' => 'tabla__sem_sinadef']);
					foreach ($pipxUbigeoDet as $item) {
						$i++;

						$s2018 =  intval($item->anio2018) + intval($s2018);
						$s2019 =  intval($item->anio2019) + intval($s2019);
						$s2020 =  intval($item->anio2020) + intval($s2020);
						$s2021 =  intval($item->anio2021) + intval($s2021);


					?>
						<tr>
							<td class="text-center"><?php echo ($i) ?></td>
							<td class="text-center"><?php echo ($item->semana) ?></td>


							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2018) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2019) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2020) ?></td>

							<td class="text-center"></td>
							<td class="text-center"><?php echo intval($item->anio2021) ?></td>


						</tr>
					<?php } ?>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">Total</td>


						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2018) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2019) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2020) ?></td>

						<td class="text-center"></td>
						<td class="text-center"><?php echo intval($s2021) ?></td>


					</tr>
				</thead>
			</table>
		<?php
		} else {
		?>



			<div class="col-lg-12">
				<div class="col lg-4">
					<!-- chart de resumen semanal covid -->
					<div id="chartSinadef5"></div>
				</div>
			</div>

			<table class="table table-sm table-detail" width="100%">
				<thead>
					<tr>
						<th class="text-center bold" width="4%">#</th>
						<th class="text-center bold">Semanas del año</th>
						<th class="text-center bold">Semana</th>
						<th class="text-center bold">Año 2020</th>
						<th class="text-center bold">Semana</th>
						<th class="text-center bold">Año 2021</th>
					</tr>
					<?php
					$i = 0;

					$sTotal = 0;
					$stotal = 0;
					$pipxUbigeoDet = dropDownList((object) ['method' => 'quintil_semanal_covid']);
					foreach ($pipxUbigeoDet as $item) {
						$i++;

						$sTotal = $sTotal + intval($item->anio2020);
						$stotal = $stotal + intval($item->anio2021);
					?>
						<tr>
							<td class="text-center"><?php echo ($i) ?></td>
							<td class="text-center"><?php echo ($item->semana) ?></td>
							<td class="text-center"><?php echo ($item->f2020) ?></td>
							<td class="text-center"><?php echo ($item->anio2020) ?></td>
							<td class="text-center"><?php echo ($item->f2021) ?></td>
							<td class="text-center"><?php echo ($item->anio2021) ?></td>

						</tr>
					<?php } ?>
					<tr>
						<td class="text-center"></td>
						<td class="text-center">Total</td>
						<td class="text-center"></td>
						<td class="text-center"><?php echo number_format($sTotal) ?></td>
						<td class="text-center"></td>
						<td class="text-center"><?php echo number_format($stotal) ?></td>

					</tr>
				</thead>
			</table>
		<?php
		}
		?>
	</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-header card-special text-center font-weight-bold">
					Distribucion de fallecidos <?php echo $data->tipo; ?> por quintiles </div>
			</div>
		</div>
	</div>
	</div>
	<div class="row">

		<div class="col lg-4">
			<div id="chartSinadef1" style="width: 400px; height: 250px; margin: 0 auto"></div>
		</div>
		<div class="col lg-4">
			<div id="chartSinadef2" style="width: 400px; height: 250px; margin: 0 auto"></div>
		</div>
		<div class="col lg-4">
			<div id="chartSinadef3" style="width: 400px; height: 250px; margin: 0 auto"></div>
		</div>
	</div>

<?php
} else {
?>
	<div class="row">
		<div class="col-lg-12">
			<div id="chart1" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
		</div>
		<div class="col-lg-12" id="mostarchar2" style="display:none;text-align:center">
			<div id="chart_2" data-target="lnkProvXrutas1" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>
			<a class="lnkAmpliar" data-event="lnkProvXrutas1" href="#" onclick="App.events(this); return false;">
				Mostrar/Ocultar grafico de Provincias
			</a>
		</div>
		<div class="col-lg-12" id="mostarchar3" style="display:none;text-align:center">
			<div id="chart_3" data-target="lnkProvXrutas2" style="min-width: 310px; height: 320px; margin: 0 auto;"></div>
			<a class="lnkAmpliar" data-event="lnkProvXrutas2" href="#" onclick="App.events(this); return false;">
				Mostrar/Ocultar grafico de Distritos
			</a>
		</div>
	</div>
<?php
}
?>