<?php
require 'PgSql.php';
$pg = new PgSql();

$codprov = $_GET['codprov'];
$anio = $_GET['anio'];
$categoria = $_GET['categoria'];

$query = "select
 pd.coddpto || pd.codprov || pd.coddist,
 d.nombdist,
 pd.ciprl,
 pd.canon
from obs.gen_distritos_det pd
inner join distrito d on d.iddist = pd.coddpto || pd.codprov || pd.coddist
where
 ano = " . $anio . " and
 idprov = '" . $codprov . "'
group by
 pd.coddpto || pd.codprov || pd.coddist,
 d.nombdist,
 pd.ciprl,
 pd.canon
order by
 pd." . $categoria . " desc";
 
 if(isset($_GET['tabla'])){
			echo json_encode($pg->getRows($query));
			die;
		}
 
$arr = $pg->getRow("select
         row_to_json(x) 
        FROM(
         select
          (select row_to_json(t) from(select 'column' as type) t) as chart,
          (select row_to_json(t) from(select '" . strtoupper($categoria) . " " . $anio . " - ' || (select 'Provincia de ' || initcap(nombprov) from provincia where first_idpr = '" . $codprov . "') as text) t) as title,
		  (select row_to_json(t) from(select round(sum(" . $categoria . ")) as text) t) as subtitle,
          (select row_to_json(t) from(select false as enabled) t) as credits,
          (select Row_to_json(t) FROM(select json_agg(x.nombdist) AS categories) t) AS \"xAxis\",
          (select row_to_json(t) from (select row_to_json(t) as title from(select 'Soles S/' as text) t) t) as \"yAxis\",
          (select row_to_json(t) from(select true as enabled) t) as exporting,
          (  
           select
            array_to_json(array_agg(x)) as series
           from(
			select 'Distritos' as name, JSON_AGG(x." . $categoria . ") as data, '#4d6fa5' as color
           ) x
          )
         from(
		  " . $query . "
         ) x
        ) x");
		
		
echo json_encode($arr->row_to_json);