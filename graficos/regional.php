<?php
require 'PgSql.php';
$pg = new PgSql();

$anio = $_GET['anio'];
$categoria = $_GET['categoria'];

$query = "select
           rd.coddpto,
           d.nombdep,
           rd.ciprl,
           rd.canon
          from obs.gen_regiones_det rd
          inner join departamento d on d.first_iddp = rd.coddpto
          where
           ano = " . $anio . " and
		   d.nombdep not in('LIMA METROPOLITANA')
          group by
           rd.coddpto,
           d.nombdep,
           rd.ciprl,
           rd.canon
          order by
           rd." . $categoria . " desc";
		   
		   if(isset($_GET['tabla'])){
			echo json_encode($pg->getRows($query));
			die;
		}
		
$arr = $pg->getRow("select
         row_to_json(x) 
        FROM(
         select
          (select row_to_json(t) from(select 'column' as type) t) as chart,
          (select row_to_json(t) from(select '" . strtoupper($categoria) . " " . $anio . "' as text) t) as title,
		  (select row_to_json(t) from(select round(sum(" . $categoria . ")) as text) t) as subtitle,
          (select row_to_json(t) from(select false as enabled) t) as credits,
          (select Row_to_json(t) FROM(select json_agg(x.nombdep) AS categories) t) AS \"xAxis\",
          (select row_to_json(t) from (select row_to_json(t) as title from(select 'Soles S/' as text) t) t) as \"yAxis\",
          (select row_to_json(t) from(select true as enabled) t) as exporting,
          (  
           select
            array_to_json(array_agg(x)) as series
           from(
			select 'Departamentos' as name, JSON_AGG(x." . $categoria . ") as data, '#4d6fa5' as color, JSON_AGG(x.coddpto) as coddpto
           ) x
          )
         from(
		  " . $query . "
         ) x
        ) x");
		
		
echo json_encode($arr->row_to_json);