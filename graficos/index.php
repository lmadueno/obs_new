<html>
	<head>
	</head>
	<body>
		Año
		<select id="ddlAnio">
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020" selected>2020</option>
		</select>
		
		Categoría
		<select id="ddlCategoria">
			<option value="canon">CANON</option>
			<option value="ciprl" selected>CIPRL</option>
		</select>		
		
		<hr>
		
		<div id="contenedor_regional">
			<div id="grafico_regional"></div>
			<table id="tblRegiones" border="1" style="margin: auto;">
				<thead>
					<tr>
						<th>Departamento</th>
						<th id="tdCategoria">CIPRL</th>
					</tr>
				</thead>
			</table>
		</div>
		
		<div id="contenedor_provincial">
			<div id="grafico_provincial"></div>
			<table id="tblProvincias" border="1" style="margin: auto;">
				<thead>
					<tr>
						<th>Provincias</th>
						<th id="tdCategoria">CIPRL</th>
					</tr>
				</thead>
			</table>
		</div>
		
		<div id="contenedor_distrital">
			<div id="grafico_distrital"></div>
			<table id="tblDistritos" border="1" style="margin: auto;">
				<thead>
					<tr>
						<th>Distritos</th>
						<th id="tdCategoria">CIPRL</th>
					</tr>
				</thead>
			</table>
		</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script src="https://code.highcharts.com/modules/accessibility.js"></script>
		<script>
			$(document).ready(function() {
				regional();
				
				$('#contenedor_provincial').hide();
				$('#contenedor_distrital').hide();
				
				$('#ddlAnio, #ddlCategoria').on('change', function() {
					$('#contenedor_distrital').hide();
					$('#contenedor_regional').show();
					if($(this).attr('id') == 'ddlCategoria'){
						$('#tdCategoria').text($(this).val().toUpperCase());
					}
					regional();
				});
				
				function regional(){
					var ddlCategoria = $('#ddlCategoria').val();
					
					$.getJSON('regional.php', { anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {
						var response = JSON.parse(response);
						
						if(response.subtitle.text != null){
							response.subtitle.text = 'S/' + numberWithCommas(response.subtitle.text);
						}
						
						response.plotOptions = {
							series: {
								cursor: 'pointer',
								events: {
									click: function(event) {										
										provincial(this.options.coddpto[event.point.x]);
									}
								}
							}
						};
					
						Highcharts.chart('grafico_regional', response);
					});
					
					$.getJSON('regional.php', { tabla: true, anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {	   $('#tblRegiones tr').not(':first').remove();
					
						$.each(response, function(i, item) {							
							$('#tblRegiones tr:last').after('<tr><td>' + item.nombdep + '</td><td>S/' + numberWithCommas(item[ddlCategoria]) + '</td></tr>');
						});
					});
				}
				
				function provincial(_coddpto){
					var ddlCategoria = $('#ddlCategoria').val();
					
					$.getJSON('provincial.php', {coddpto: _coddpto, anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {
						var response = JSON.parse(response);
						
						if(response.subtitle.text != null){
							response.subtitle.text = 'S/' + numberWithCommas(response.subtitle.text);
						}
						
						response.plotOptions = {
							series: {
								cursor: 'pointer',
								events: {
									click: function(event) {
										distrital(this.options.codprov[event.point.x]);
									}
								}
							}
						};
						
						$('#contenedor_regional').toggle('hide');
						Highcharts.chart('grafico_provincial', response);
						$('#contenedor_provincial').show();
					});
					
					$.getJSON('provincial.php', {tabla: true, coddpto: _coddpto, anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {
						$('#tblProvincias tr').not(':first').remove();						
						$.each(response, function(i, item) {							
							$('#tblProvincias tr:last').after('<tr><td>' + item.nombprov + '</td><td>S/' + numberWithCommas(item[ddlCategoria]) + '</td></tr>');
						});
					});
				}
				
				function distrital(_codprov){
					var ddlCategoria = $('#ddlCategoria').val();
					
					$.getJSON('distrital.php', {codprov: _codprov, anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {
						var response = JSON.parse(response);
						
						if(response.subtitle.text != null){
							response.subtitle.text = 'S/' + numberWithCommas(response.subtitle.text);
						}
						
						$('#contenedor_provincial').toggle('hide');
						Highcharts.chart('grafico_distrital', response);
						$('#contenedor_distrital').show();
					});
					
					$.getJSON('distrital.php', {tabla: true, codprov: _codprov, anio: $('#ddlAnio').val(), categoria: $('#ddlCategoria').val() }, function(response) {	
						$('#tblDistritos tr').not(':first').remove();											
						$.each(response, function(i, item) {							
							$('#tblDistritos tr:last').after('<tr><td>' + item.nombdist + '</td><td>S/' + numberWithCommas(item[ddlCategoria]) + '</td></tr>');
						});
					});
				}
				
				function numberWithCommas(x) {
					return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				}
			});
		</script>
	</body>
</html>