<?php
	require 'php/app.php';
    $data   = json_decode($_GET['data']);
    
?>
    <?php $excelEmpresas = dropDownList((object) ['method' => 'distritosOXIExcel','estado' =>  $data->estado]);?>
    <?php  
        $temp=0;
        $temp1=0;
        $temp2=0;
        $temp3=0;
        $temp4=0;
        $temp5=0;
        $temp6=0;
        $temp7=0;
        $temp8=0;
        $temp9=0;
        $temp10=0;
        $temptotal=0;
    ?>
    <?php foreach ($excelEmpresas as $key){ 										
		$temp+=$key->suma2009;
		$temp1+=$key->suma2010;
		$temp2+=$key->suma2011;
		$temp3+=$key->suma2012;										
        $temp4+=$key->suma2013;
        $temp5+=$key->suma2014;
        $temp6+=$key->suma2015;
        $temp7+=$key->suma2016;
        $temp8+=$key->suma2017;
        $temp9+=$key->suma2018;
        $temp10+=$key->suma2019;
        $temptotal+=$key->total;
                                     	
        } ?>
    <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
        <table class="table table-sm table-detail">
            <thead>																				
                <tr>
                    <th class="text-center bold vert-middle">#</th>
                    <th class="text-center bold vert-middle">Región</th>
                    <th class="text-center bold vert-middle">Provincia</th>
                    <th class="text-center bold vert-middle">Municipalidad Distrital</th>
                    <?php if($temp>0){ ?><th class="text-center bold vert-middle">2009</th><?php } ?>
                    <?php if($temp1>0){  ?><th class="text-center bold vert-middle">2010</th><?php } ?>
                    <th class="text-center bold vert-middle">2011</th>
                   <th class="text-center bold vert-middle">2012</th>
                   <th class="text-center bold vert-middle">2013</th>
                    <th class="text-center bold vert-middle">2014</th>
                    <th class="text-center bold vert-middle">2015</th>
                    <th class="text-center bold vert-middle">2016</th>
                    <th class="text-center bold vert-middle">2017</th>
                    <th class="text-center bold vert-middle">2018</th>
                    <th class="text-center bold vert-middle">2019*</th>
                    <th class="text-center bold vert-middle">Total</th>
                </tr>	
            </thead>
                <?php foreach ($excelEmpresas as $key){ ?>
                <tr>
				    <td class="text-center"><?php   echo $key->id?></td>
                    <td class="text-left"><?php     echo $key->dpto?></td>
                    <td class="text-left"><?php     echo $key->provincia?></td>
                    <td class="text-left"><?php     echo $key->nombre?></td>
				    <?php if($temp>0){ ?><td class="text-right"><?php    echo ($key->suma2009)?></td><?php } ?>
				    <?php if($temp1>0){  ?><td class="text-right"><?php    echo ($key->suma2010)?></td><?php } ?>
                    <td class="text-right"><?php    echo ($key->suma2011)?></td>
                    <td class="text-right"><?php    echo ($key->suma2012)?></td>										
                    <td class="text-right"><?php    echo ($key->suma2013)?></td>
                    <td class="text-right"><?php    echo ($key->suma2014)?></td>
				    <td class="text-right"><?php    echo ($key->suma2015)?></td>
				    <td class="text-right"><?php    echo ($key->suma2016)?></td>
				    <td class="text-right"><?php    echo ($key->suma2017)?></td>											
				    <td class="text-right"><?php    echo ($key->suma2018)?></td>
                    <td class="text-right"><?php    echo ($key->suma2019)?></td>
                    <td class="text-right">
                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre?>" href="#" onclick="App.events(this); return false;">
                            <?php    echo ($key->total)?>
					    </a>			                                                    
                    </td>
			    </tr>
                <tr data-target="lnkProvXrutas_<?php echo $key->nombre?>" style="display: none;">
                    <td colspan="16">
                        <div class="card">
						    <div class="card-header card-special">
							    Lista de Proyectos
                            </div>
                            <div class="card-body">
							    <div class="row">
								    <div class="col-lg-12">
                                        <table class="table table-sm table-detail">
											    <tr>
                                                    <th class="text-center bold" width="5%">#</th>
												    <th class="text-center bold" width="15%">Codigo Unico</th>
												    <th class="text-center bold" width="40%">Nombre Proyecto</th>
												    <th class="text-center bold" width="20%">Empresa</th>
                                                    <th class="text-center bold" width="5%">Monto</th>
                                                    <th class="text-center bold" width="15%">Fecha</th>
                                                </tr>
                                                    <?php 
                                                     
                                                         $tempMinisDet=0;
                                                         $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->entidad,'estado' =>$key->estado]);
                                                    ?>
                                                    <?php foreach ($tabladet as $key){ ?>
                                                <tr>
                                                    <td class="text-center" ><?php $tempMinisDet++;  echo ($tempMinisDet)?></td>
                                                    <td class="text-center" ><a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip;?>' target="blank"><?php echo $key->snip; ?></td>
                                                    <td class="text-left" ><?php    echo ucfirst($key->nombre_proyecto)?></td>
													<td class="text-left" ><?php    echo ($key->empresa)?></td>
                                                    <td class="text-right" ><?php    echo ($key->monto_inversion)?></td>
                                                    <td class="text-center" ><?php    echo ($key->fecha_buena_pro)?></td>
                                                </tr>
                                                <?php }?>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                </tr>
                <?php } ?>
                <tr>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
                    <td class="text-center"></td>
		            <td class="text-left"><b>TOTAL</b></td>
		            <?php if($temp>0){ ?><td class="text-right"><?php echo ($temp)?></td><?php } ?>
		            <?php if($temp1>0){ ?><td class="text-right"><?php echo ($temp1)?></td><?php } ?>
		            <td class="text-right"><?php echo ($temp2)?></td>
		            <td class="text-right"><?php echo ($temp3)?></td>											
		            <td class="text-right"><?php echo ($temp4)?></td>
                    <td class="text-right"><?php echo ($temp5)?></td>
		            <td class="text-right"><?php echo ($temp6)?></td>
		            <td class="text-right"><?php echo ($temp7)?></td>											
		            <td class="text-right"><?php echo ($temp8)?></td>
                    <td class="text-right"><?php echo ($temp9)?></td>
		            <td class="text-right"><?php echo ($temp10)?></td>
		            <td class="text-right"><?php echo ($temptotal)?></td>											
												        																								
                </tr>
              
        </table>
    </div>
    