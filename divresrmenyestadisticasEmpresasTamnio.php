<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Sector</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Cantidad</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Utilidad</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Disponibilidad Oxi</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'indicador_dash_2','tamanio'=>$data->tamanio]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->cantidad ;	
										$s2019=   $s2019+$item->utilidad ;	
										$s2020=   $s2020+$item->oxi ;	
																					
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" >
											<buttom class="btn btn-link lnkAmpliar_mef_sector" data-event="lnkProvXrutas_<?php echo $item->nomsector.'$'.$data->tamanio?>" id="<?php echo $item->nomsector.'$'.$data->tamanio?>" href="#"  onclick="App.events(this);">
												<?php echo ($item->nomsector)?>
											</buttom>
										</td>												
										<td class="text-center" ><?php echo number_format($item->cantidad)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->utilidad)?></td>		
										<td class="text-center" ><?php echo number_format($item->oxi)?></td>		
                                    </tr>
									<tr data-target="lnkProvXrutas_<?php echo $item->nomsector.'$'.$data->tamanio?>" style="display: none;">
										<td colspan="8">
											<div class="card">
												<div class="card-header">
													Entidades
												</div>
												<div class="card-body">
													<div id="div_<?php echo $item->nomsector.'$'.$data->tamanio?>"></div>
												</div>
											</div>
											
										</td>
									</tr>
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
                                        <td class="text-center" ><?php echo number_format($s2018)?></td>												
                                        <td class="text-center"><?php echo number_format($s2019)?></td>
                                        <td class="text-center"><?php echo number_format($s2020)?></td>

                                    </tr>
								</tbody>
							</table>