<?php
    require 'php/app.php';		
    $data   = json_decode($_GET['data']);
?>
<input type="hidden" value="<?php echo $data->distritos?>" id="distritosObjetosInfra">
<table class="table table-sm table-detail" width="100%"> 
                                        <thead>
                                            <tr>
                                                <th class="text-center bold" width="5%">#</th>
                                                <th class="text-center bold" width="25%">Region</th>
                                                <th class="text-center bold" width="20%">CIPRL</th>
                                                <th class="text-center bold" width="15%">Población</th>
                                                <th class="text-center bold" width="15%">C.Educativos</th>
                                                <th class="text-center bold" width="10%">Comisarias</th>
                                                <th class="text-center bold" width="10%">C. Salud</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php	
                                            $oleoducto = dropDownList((object) ['method' => 'objetosInfraRegion','ubigeo' =>$data->distritos]);
                                            $sumr1=0;
                                            $sumr2=0;
                                            $sumr3=0;
                                            $sumr4=0;
                                            $sumr5=0;	
                                            foreach ($oleoducto as $item){
                                                $sumr1+=$item->ciprl;
                                                $sumr2+=$item->poblacion;
                                                $sumr3+=$item->colegios;
                                                $sumr4+=$item->comisarias;
                                                $sumr5+=$item->hospitales;	
                                            ?> 
                                            <tr>
                                                <td class="text-left"><?php echo  ucwords(strtolower($item->numero))?></td>
                                                <td class="text-left">
                                                    <a class="lnkAmpliar"  data-event="<?php echo $item->iddpto?>" href="#" onclick="App.events(this); return false;">
                                                        <?php echo ucwords(strtolower($item->departamen));?>
                                                    </a>													
                                                </td>
                                                <td class="text-right"><?php echo  number_format($item->ciprl)?></td>	
                                                <td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->poblacion/1000, 1)?>K</td>
                                                <td class="text-right"><?php echo  number_format($item->colegios)?></td>	
                                                <td class="text-right"><?php echo  number_format($item->comisarias)?></td>	
                                                <td class="text-right"><?php echo  number_format($item->hospitales)?></td>				
										    </tr>   
                                            <tr data-target="<?php echo $item->iddpto?>" style="display: none;"> 
                                                <td colspan="7">
                                                    <div class="card">
                                                        <div class="card-header card-special">
                                                                Provincias
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-sm table-detail">
                                                                        <tr>
                                                                            <th class="text-center bold" width="5%">#</th>
                                                                            <th class="text-center bold" width="25%">Provincia</th>
                                                                            <th class="text-center bold" width="20%">CIPRL</th>
                                                                            <th class="text-center bold" width="15%">Población</th>
                                                                            <th class="text-center bold" width="15%">C.Educativos</th>
                                                                            <th class="text-center bold" width="10%">Comisarias</th>
                                                                            <th class="text-center bold" width="10%">C. Salud</th>
                                                                        </tr>
                                                                        <?php	
																		 $oleoducto1 = dropDownList((object) ['method' => 'objetosInfraProvincia','dpto'=>$item->iddpto,'ubigeo' =>$data->distritos]);	
																		 $sump1=0;
																		 $sump2=0;
																		 $sump3=0;
																		 $sump4=0;
																		 $sump5=0;
										                                 foreach ($oleoducto1 as $item){
																			$sump1+=$item->ciprl;
																			$sump2+=$item->poblacion;
																			$sump3+=$item->colegios;
																			$sump4+=$item->comisarias;
																			$sump5+=$item->hospitales;
																	?>
                                                                        <tr>
                                                                            <td class="text-left"><?php echo  ucwords(strtolower($item->numero))?></td>
                                                                            <td class="text-left">
                                                                                <a class="lnkAmpliar"  data-event="<?php echo $item->idprov?>" href="#" onclick="App.events(this); return false;">
                                                                                    <?php echo ucwords(strtolower($item->provincia));?>
                                                                                </a>													
                                                                            </td>
                                                                            <td class="text-right"><?php echo  number_format($item->ciprl)?></td>	
                                                                            <td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->poblacion/1000, 1)?>K</td>
                                                                            <td class="text-right"><?php echo  number_format($item->colegios)?></td>	
                                                                            <td class="text-right"><?php echo  number_format($item->comisarias)?></td>	
                                                                            <td class="text-right"><?php echo  number_format($item->hospitales)?></td>				
																	    </tr>
                                                                        <tr data-target="<?php echo $item->idprov?>" style="display: none;">
                                                                            <td colspan="7">
                                                                                <div class="card">
                                                                                    <div class="card-header card-special">
                                                                                        Distritos
                                                                                    </div>
                                                                                    <div class="card-body">
                                                                                        <div class="row">
                                                                                            <div class="col-lg-12">
                                                                                                <table class="table table-sm table-detail"> 
                                                                                                    <tr>
                                                                                                        <th class="text-center bold" width="5%">#</th>
                                                                                                        <th class="text-center bold" width="25%">Distrito</th>
                                                                                                        <th class="text-center bold" width="20%">CIPRL</th>
                                                                                                        <th class="text-center bold" width="15%">Población</th>
                                                                                                        <th class="text-center bold" width="15%">C.Educativos</th>
                                                                                                        <th class="text-center bold" width="10%">Comisarias</th>
                                                                                                        <th class="text-center bold" width="10%">C. Salud</th>
                                                                                                    </tr>
                                                                                                    <?php	
																									 $oleoducto1 = dropDownList((object) ['method' => 'objetosInfraDistritos','prov'=>$item->idprov,'ubigeo'=>$data->distritos]);
																									 $i=0;	
																									 $sumd1=0;
																									 $sumd2=0;
																									 $sumd3=0;
																									 $sumd4=0;
																									 $sumd5=0;
										                                 							foreach ($oleoducto1 as $item){	
																										$i++;
																										$sumd1+=$item->ciprl;
																										$sumd2+=$item->poblacion;
																										$sumd3+=$item->colegios;
																										$sumd4+=$item->comisarias;
																										$sumd5+=$item->hospitales;
																							    ?>
                                                                                                    <tr>
                                                                                                        <td class="text-left"><?php echo  ucwords(strtolower($item->numero))?></td>
                                                                                                        <td class="text-left"><a href="" class="openModalDialogCIPRL" data-event="<?php echo $item->iddist?>" onclick="App.events(this); return false;"><?php echo ucwords(strtolower($item->distrito))?></a></td>		
                                                                                                        <td class="text-right"><?php echo  number_format($item->ciprl)?></td>	
                                                                                                        <td class="text-right" title="<?php echo number_format($item->poblacion)?>"><?php echo round($item->poblacion/1000, 1)?>K</td>
                                                                                                        <td class="text-right">
                                                                                                            <a class="lnkAmpliarCentrosLinea"  id="colegios" data-event="<?php echo $item->iddist.$i?>" href="#" onclick="App.events(this); return false;">
                                                                                                                <?php echo number_format($item->colegios);?>
                                                                                                            </a>													
                                                                                                        </td>
                                                                                                        <td class="text-right">
                                                                                                            <a class="lnkAmpliarCentrosLinea" id="comisarias" data-event="<?php echo $item->iddist.$i?>" href="#" onclick="App.events(this); return false;">
                                                                                                                <?php echo number_format($item->comisarias);?>
                                                                                                            </a>													
                                                                                                        </td>	
                                                                                                        <td class="text-right">
                                                                                                            <a class="lnkAmpliarCentrosLinea" id="hospitales" data-event="<?php echo $item->iddist.$i?>" href="#" onclick="App.events(this); return false;">
                                                                                                                <?php echo number_format($item->hospitales);?>
                                                                                                            </a>													
                                                                                                        </td>			
                                                                                                    </tr>
                                                                                                    <tr data-target="<?php echo $item->iddist.$i?>" style="display: none;">
                                                                                                        <td colspan="8">
                                                                                                            <div id="objetosInfraUbi<?php echo $item->iddist.$i?>">
                                                                                                                <div class="row justify-content-center h-100">
                                                                                                                    <div class="col-sm-8 align-self-center text-center">
                                                                                                                        <img src="assets/app/img/loading.gif" alt="">    
                                                                                                                    </div>
                                                                                                                </div>     
                                                                                                            </div>
                                                                                                        </td>
                                                                                                    </tr>		
																							<?php }?>
                                                                                                    <tr>
                                                                                                        <td class="text-left"></td>
                                                                                                        <td class="text-left"><b>Total</b></td>	
                                                                                                        <td class="text-right"><?php echo  number_format($sumd1)?></td>	
                                                                                                        <td class="text-right"><?php echo round($sumd2/1000, 1)?>K</td>	
                                                                                                        <td class="text-right"><?php echo  number_format($sumd3)?></td>	
                                                                                                        <td class="text-right"><?php echo  number_format($sumd4)?></td>	
                                                                                                        <td class="text-right"><?php echo  number_format($sumd5)?></td>		
                                                                                                    </tr>	
                                                                                                </table> 
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </tr>	
                                                                    <?php	
                                                                        }
                                                                    ?>  
                                                                    
                                                                        <tr>
                                                                            <td class="text-left"></td>
                                                                            <td class="text-left"><b>Total</b></td>	
                                                                            <td class="text-right"><?php echo  number_format($sump1)?></td>	
                                                                            <td class="text-right"><?php echo round($sump2/1000, 1)?>K</td>	
                                                                            <td class="text-right"><?php echo  number_format($sump3)?></td>	
                                                                            <td class="text-right"><?php echo  number_format($sump4)?></td>	
                                                                            <td class="text-right"><?php echo  number_format($sump5)?></td>		
                                                                        </tr>		
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>   
                                            <?php	
                                            }
                                            ?>  
                                            <tr>
                                                <td class="text-left"></td>
                                                <td class="text-left"><b>Total</b></td>	
                                                <td class="text-right"><?php echo  number_format($sumr1)?></td>	
                                                <td class="text-right"><?php echo round($sumr2/1000, 1)?>K</td>	
                                                <td class="text-right"><?php echo  number_format($sumr3)?></td>	
                                                <td class="text-right"><?php echo  number_format($sumr4)?></td>	
                                                <td class="text-right"><?php echo  number_format($sumr5)?></td>		
										    </tr>	                                        
                                        </tbody>                                               
                                    </table> 


