<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold"  style="background:#ddebf8;text-align:center">#</th>
										<th class="text-center bold"  style="background:#ddebf8;text-align:center">Niveles de Gobierno</th>
										<th class="text-center bold"  style="background:#ddebf8;text-align:center">CIPRL</th>
                                        <th class="text-center bold"  style="background:#ddebf8;text-align:center">Cantidad PI viables sin ejecución </th>
                                        <th class="text-center bold"  style="background:#ddebf8;text-align:center">Monto PI viables sin ejecución </th>
                                        <th class="text-center bold"  style="background:#ddebf8;text-align:center">Cantidad PI viables con ejecución  </th>	
                                        <th class="text-center bold"  style="background:#ddebf8;text-align:center">Monto PI viables con ejecución  </th>
										<th class="text-center bold"  style="background:#ddebf8;text-align:center">Cantidad PI viables  </th>	
                                        <th class="text-center bold"  style="background:#ddebf8;text-align:center">Monto PI viables  </th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
									$s1=0;
									$s2=0;  
									$s3=0;  
									$s4=0;  
									$s5=0;
									$s6=0;  
									$s7=0;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'pi_partidospoliticos_totales_div','partido'=>$data->partido]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s1=   $s1+$item->ciprl ;	
										$s2=   $s2+$item->cantidad_ ;	
										$s3=   $s3+$item->monto_ ;	
										$s4=   $s4+$item->cantidad_fase ;	
										$s5=   $s5+$item->monto_fase ;	
										$s6=   $s6+($item->cantidad_fase +$item->cantidad_);	
										$s7=   $s7+($item->monto_fase+ $item->monto_);	  										
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" >
										<buttom class="btn btn-link lnkAmpliar_totales_nivel" data-event="lnkProvXrutas_<?php echo  $data->partido.'$'.$item->nivel?>" id="<?php echo  $data->partido.'$'.$item->nivel?>"   onclick="App.events(this);">
												<?php echo ($item->titulo)?>
											</buttom>
										</td>												
										<td class="text-center" ><?php echo number_format($item->ciprl)?></td>		
                                        <td class="text-center" ><?php echo number_format($item->cantidad_)?></td>		
										<td class="text-center" ><?php echo number_format($item->monto_)?></td>		
										<td class="text-center" ><?php echo number_format($item->cantidad_fase)?></td>	
                                        <td class="text-center" ><?php echo number_format($item->monto_fase)?></td>	
										<td class="text-center" ><?php echo number_format(($item->cantidad_fase +$item->cantidad_))?></td>	
                                        <td class="text-center" ><?php echo number_format(($item->monto_fase+ $item->monto_))?></td>	
                                    </tr>
									<tr data-target="lnkProvXrutas_<?php echo $data->partido.'$'.$item->nivel?>" style="display: none;">
										<td colspan="9">
											<div class="card">
												<div class="card-header">
													Entidades
												</div>
												<div class="card-body">
													<div id="div_<?php echo  $data->partido.'$'.$item->nivel?>"></div>
												</div>
											</div>
											
										</td>
									</tr>
								<?php } ?>
									<tr>
                                        <td  class="text-center"></td>
										<td class="text-center" >
											Totales
										</td>												
										<td class="text-center" ><?php echo number_format($s1)?></td>		
										<td class="text-center" ><?php echo number_format($s2)?></td>	
										<td class="text-center" ><?php echo number_format($s3)?></td>	
										<td class="text-center" ><?php echo number_format($s4)?></td>	
										<td class="text-center" ><?php echo number_format($s5)?></td>
										<td class="text-center" ><?php echo number_format($s6)?></td>	
										<td class="text-center" ><?php echo number_format($s7)?></td>	
                                    </tr>	
								</tbody>
							</table>