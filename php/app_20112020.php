<?php
    session_start();
    require 'PgSql.php';
    if(count($_GET)  > 0){
        if(isset($_GET['data'])){
            $data   = array();
            $params = json_decode($_GET['data']);	
            if(isset($params->method)){
                switch($params->method){
					case 'empresas_expo_impo':
						$pg   = new PgSql();
						$data['data']  = $pg->getRows("select 
								 usdexpo2019,usdexpo2018,usdexpo2017,usdexpo2016,usdexpo2015,usdexpo2014,usdimpo2019,usdimpo2018,usdimpo2017,usdimpo2016,usdimpo2015,usdimpo2014 
								from  observatorio.gen_empresas_exportaciones a 
								inner join observatorio.gen_empresas_importaciones b on a.ruc=b.ruc 
								where a.ruc='".$params->ruc."'");
					break;
					case 'draw_area_influencia_comisarias1':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select cod_pnp from gen_comisarias p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geom::geography)::geometry,x.geom::geometry)");     
                    break;
					case 'draw_area_influencia_comisarias2':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select rucempresa from obs.gen_empresa p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.ubicacion::geography)::geometry,x.geom::geometry)");     
                    break;
					case 'draw_area_influencia_comisarias3':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select cu from gen_hospitales p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geom::geography)::geometry,x.geom::geometry)");     
                    break;
					case 'draw_area_influencia_comisarias':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select cod_pnp from  gen_comisarias x inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(x.geom::geography,1)::geometry,p.geom::geometry) ");     
                    break;
					case 'draw_area_influencia_comisarias_':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select cu from  gen_hospitales x inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(x.geom::geography,1)::geometry,p.geom::geometry) ");     
                    break;
					case 'empresas_ubigeos':
						$pg = new PgSql();
						$data['data'] = $pg->getRows("select count(*) can from observatorio.gen_empresas_ubigeos_anexo where ruc='".$params->ruc."'");     
					break;
					case 'insert_empresa':
						$pg = new PgSql();
						$data['data'] = $pg->getRows("select * from observatorio.fn_getlocalizacion('".$params->lat."','".$params->lng."','".$params->ruc."')");     
					break;
					case 'anexos_sunat':
						$pg = new PgSql();
						$data['data'] = $pg->getRows("with data as(
							select 
							 (select distrito from distrito_f where iddist=substr(ubigeo,1,6)) distrito,
							 tipo_via,
							 nombre_via
							from(
							 select 
							  case
								when length(ubigeo)=6 then ubigeo
								else concat('0',ubigeo)
							   end ubigeo,
							   case
								when tipo_via='-' then ' '
								else tipo_via
							   end tipo_via,
							   case
								when nombre_via='-' then ' '
								else nombre_via
							   end nombre_via,
							   ruc
							 from observatorio.gensunat_anexos) x
							 where ruc='".$params->ruc."')
							 select concat(distrito,', ',tipo_via,' ',nombre_via) dir from data");     
					break;
					case 'proyectados_fallecidos_anio':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("with data as(
                            select sum(a2017) a2017,sum(a2018) a2018,sum(a2019) a2019,sum(a2020) a2020 from observatorio.gen_proyectado_fallecidos_anio)
                            select a2017,a2018,a2019,((a2017+a2018+a2019)/3)a2020,( a2020-((a2017+a2018+a2019)/3)) diferencia from data");     
                    break;
					case 'chartEmpresasEstadisticaFinanciera':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select a2015,a2016,a2017,a2018,a2019,descripcion,idanalisis from observatorio.gen_analisisempresas_financiero a inner join observatorio.gen_empresas_tipo_analisis b
                        on a.idanalisis=b.id where a.id='".$params->id."'");     
                    break;
					case 'proyectado_fallecidos':
						$pg = new PgSql();
						$data['data'] = $pg->getRows("select * from observatorio.gen_proyectado_fallecidos");     
					break;
					 case 'getRectanguloDraw':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.gen_objetos_rectangle where id=".$params->id   );     
                    break;
                    case 'getCirculoDraw':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.gen_objetos_circle where id=".$params->id);    
                    break;
                    case 'gen_objetos_circle':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.fn_circle_draw('".$params->descripcion."','".$params->lat."','".$params->lng."',".$params->radio.")");     
                    break;
                    case 'gen_objetos_rectangle':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.fn_rectangle_draw('".$params->descripcion."','".$params->lats."','".$params->lngs."')");     
                    break;
                    case 'draw_circle_ubigeos':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("(select first_iddp ubigeo from departamento x inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(x.geom::geography,1)::geometry,p.geom::geometry)  group by first_iddp)
                        union all
                        (select first_idpr ubigeo from provincia x inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(x.geom::geography,1)::geometry,p.geom::geometry)  group by first_idpr)
                        union all
                        (select iddist ubigeo from distrito_f x inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(x.geom::geography,1)::geometry,p.geom::geometry)  group by iddist)");     
                    break;
                    case 'draw_rectangle_ubigeos':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("(select first_iddp ubigeo from departamento p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geom::geography)::geometry,x.geom::geometry) group by first_iddp)
                       union all
                       (select first_idpr ubigeo from provincia p  inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geom::geography)::geometry,x.geom::geometry) group by first_idpr)
                        union all
                        (select iddist ubigeo from distrito_f p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geom::geography)::geometry,x.geom::geometry) group by iddist)");     
                    break;
                    case 'draw_rectangle_projects':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select codproyecto from obs.gen_proyecto p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.ubicacion::geography)::geometry,x.geom::geometry)");     
                    break;
                    case 'proyectado_fallecidos':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.gen_proyectado_fallecidos");     
                    break;
                    case 'draw_companies':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select coddpto from (
                            select
                              
                             em.coddpto::integer as coddpto,
                             em.ubicacion
                         
                            from obs.gen_empresa em 
                            inner join obs.gen_sector se on  se.codsector = em.codsector 
                            inner join obs.gen_grupoeconomico ge on ge.codgrupo = em.codgrupo
                            inner join obs.gen_departamento dp on dp.coddpto = em.coddpto
                            inner join obs.gen_emp_tamanio tm on tm.codtamanio = em.codtamanio) x 
                            inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                                                        on st_intersects(st_buffer(x.ubicacion::geography,1)::geometry,p.geom::geometry) group by coddpto");     
                    break;
                    case 'draw_circle_wordCloud':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select   funcion,count(*) can from observatorio.gen_proyectos d inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                        on st_intersects(st_buffer(d.geometry::geography,1)::geometry,p.geom::geometry)  group by funcion");     
                    break;
                    case 'draw_rectangle_wordCloud':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select funcion,count(*) can from observatorio.gen_proyectos p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geometry::geography)::geometry,x.geom::geometry) group by funcion");     
                    break;
                    case 'draw_rectangle_wordCloud_new':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select programaproyecto funcion,count(*) can from observatorio.gen_proyectos p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geometry::geography)::geometry,x.geom::geometry) where ".$params->consulta."  group by programaproyecto");     
                    break;
                    case 'draw_circle':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select concat(coddpto,codprov,coddist)ubigeo from observatorio.gen_proyectos d inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                            on st_intersects(st_buffer(d.geometry::geography,1)::geometry,p.geom::geometry)  group by concat(coddpto,codprov,coddist)");     
                    break;
                    case 'draw_rectangle':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select concat(coddpto,codprov,coddist)ubigeo from observatorio.gen_proyectos p inner join 
                        (SELECT ST_GeomFromText('".$params->poligono."')::geography geom) x on st_intersects((p.geometry::geography)::geometry,x.geom::geometry) group by concat(coddpto,codprov,coddist)");     
                    break;
                   
					case 'quintiles_mes_anual_sinadef':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select 
                            (select * from obs.fn_nombre_mes(mes)) meses, 
                            count(*) s2017,
                            (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2018) s2018,
                            (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2019)s2019,
                            (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2020)s2020   
                            from obs.tmp_fallecidos_sinadef x where anio=2017 group by mes order by mes");     
                    break;
                    case 'quintiles_mes_anual_covid':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select (select * from obs.fn_nombre_mes(mes)) meses,fallecidos  from(
                            select 
                              substr(fecha_fallecimiento,5,2)::int mes,
                              count(*) fallecidos
                             from obs.tmp_fallecidos_minsa group by substr(fecha_fallecimiento,5,2) order by substr(fecha_fallecimiento,5,2))x");     
                    break;
					 case 'quintiles_covid':
                        $pg = new PgSql();
                        if($params->idnivel==1){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='covid'group by quantil order by quantil");
                        }else if($params->idnivel==2){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='covid'group by quantil order by quantil");
                        }else if($params->idnivel==3){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='covid'group by quantil order by quantil");
                        }           
                    break;
					 case 'quintiles_minsa_tab':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("(select 1::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='covid' group by quantil order by quantil) 
                            union all  
                            (select 2::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_provincia_salud where tipo='covid' group by quantil order by quantil )
                            union all
                            (select 3::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_distrito_salud where tipo='covid' group by quantil order by quantil)");     
                    break;
					case 'quintiles_sinadef_tab':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("(select 1::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='sinadef'group by quantil order by quantil) 
                            union all  
                            (select 2::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_provincia_salud where tipo='sinadef'group by quantil order by quantil )
                            union all
                            (select 3::int tipo,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_distrito_salud where tipo='sinadef'group by quantil order by quantil)");     
                    break;
					case 'quintiles_sinadef':
                        $pg = new PgSql();
                        if($params->idnivel==1){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='sinadef'group by quantil order by quantil");
                        }else if($params->idnivel==2){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_provincia_salud where tipo='sinadef'group by quantil order by quantil");
                        }else if($params->idnivel==3){
                            $data['data'] = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_distrito_salud where tipo='sinadef'group by quantil order by quantil");
                        }           
                    break;
					case 'ubigeoMultiobjeto':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select * from observatorio.gen_ubigeo_multiobjeto where gid in (".$params->consulta.")");
                    break;
					 case 'chartStakedBar':
                        $s2009   = array();
                        $s2010   = array();
                        $s2011   = array();
                        $s2012   = array();
                        $s2013   = array();
                        $s2014   = array();
                        $s2015   = array();
                        $s2016   = array();
                        $s2017   = array();
                        $s2018   = array();
                        $s2019   = array();
                        $titulo   = array();
                        if($params->tipo == "empresas"){
                            $pg = new PgSql();								
                            $obj = $pg->getRows("select nombre,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2018,suma2019 from ".$params->tabla." order by total desc limit ".$params->limitFin." OFFSET ".$params->limitInicio."");
                            foreach ($obj as $valor){
                                $s2009[] = array(
                                    'y'=>(double)$valor->suma2009,
                                    'color'=>'#EC33FF'
                                );
                                $s2010[] = array(
                                    'y'=>(int)$valor->suma2010,
                                    'color'=>'#9C33FF'
                                );
                                $s2011[]= array(
                                    'y'=>(int)$valor->suma2011,
                                    'color'=>'#4F81BD'
                                );
                                $s2012[] = array(
                                    'y'=>(int)$valor->suma2012,
                                    'color'=>'#33BBFF'
                                );
                                $s2013[] = array(
                                    'y'=>(double)$valor->suma2013,
                                    'color'=>'#33FFE3'
                                );
                                $s2014[] = array(
                                    'y'=>(int)$valor->suma2014,
                                    'color'=>'gray'
                                );
                                $s2015[]= array(
                                    'y'=>(int)$valor->suma2015,
                                    'color'=>'#86FF33'
                                );
                                $s2016[] = array(
                                    'y'=>(int)$valor->suma2016,
                                    'color'=>'#E0FF33'
                                );
                                $s2017[] = array(
                                    'y'=>(int)$valor->suma2017,
                                    'color'=>'#FFCE33'
                                );
                                $s2018[]= array(
                                    'y'=>(int)$valor->suma2018,
                                    'color'=>'#FF9633'
                                );
                                $s2019[] = array(
                                    'y'=>(int)$valor->suma2019,
                                    'color'=>'#C0504D'
                                );
                                $titulo[] = array(
                                    $valor->nombre,
                                );	
                            }								
                                $data['data'] = array(
                                    'data2009' => $s2009,
                                    'data2010' => $s2010,
                                    'data2011' => $s2011,
                                    'data2012' => $s2012,
                                    'data2013' => $s2013,
                                    'data2014' => $s2014,
                                    'data2015' => $s2015,
                                    'data2016' => $s2016,
                                    'data2017' => $s2017,
                                    'data2018' => $s2018,
                                    'data2019' => $s2019,
                                    'datatitulo' =>$titulo,			
                                );
                            }
                            else if($params->tipo == "empresas1"){
                                $pg = new PgSql();								
                                $obj = $pg->getRows("select idempresas,nombre,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2015+suma2016+suma2017+suma2018 as total,suma2018,suma2019 from obs.gen_empresas_oxi_excel order by total  desc limit ".$params->limitFin." OFFSET ".$params->limitInicio."");
                                foreach ($obj as $valor){
                                    $s2009[] = array(
                                        
                                        'color'=>'#EC33FF'
                                    );
                                    $s2010[] = array(
                                        
                                        'color'=>'#9C33FF'
                                    );
                                    $s2011[]= array(
                                        
                                        'color'=>'#4F81BD'
                                    );
                                    $s2012[] = array(
                                        
                                        'color'=>'#33BBFF'
                                    );
                                    $s2013[] = array(
                                        
                                        'color'=>'#33FFE3'
                                    );
                                    $s2014[] = array(
                                        
                                        'color'=>'gray'
                                    );
                                    $s2015[]= array(
                                        'y'=>(int)$valor->suma2015,
                                        'color'=>'#86FF33'
                                    );
                                    $s2016[] = array(
                                        'y'=>(int)$valor->suma2016,
                                        'color'=>'#E0FF33'
                                    );
                                    $s2017[] = array(
                                        'y'=>(int)$valor->suma2017,
                                        'color'=>'#FFCE33'
                                    );
                                    $s2018[]= array(
                                        'y'=>(int)$valor->suma2018,
                                        'color'=>'#FF9633'
                                    );
                                    $s2019[] = array(
                                        'y'=>(int)$valor->suma2019,
                                        'color'=>'#C0504D'
                                    );
                                    $titulo[] = array(
                                        $valor->nombre,
                                    );	
                                }								
                                    $data['data'] = array(
                                        'data2009' => $s2009,
                                        'data2010' => $s2010,
                                        'data2011' => $s2011,
                                        'data2012' => $s2012,
                                        'data2013' => $s2013,
                                        'data2014' => $s2014,
                                        'data2015' => $s2015,
                                        'data2016' => $s2016,
                                        'data2017' => $s2017,
                                        'data2018' => $s2018,
                                        'data2019' => $s2019,
                                        'datatitulo' =>$titulo,			
                                    );
                                }
                            else if($params->tipo == "Gobiernos"){
                                $pg = new PgSql();
                                $obj = $pg->getRows("select nombre,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2018,suma2019 from ".$params->tabla." WHERE estado='".$params->estado."' order by total desc");
                                foreach ($obj as $valor){
                                    $s2009[] = array(
                                        'y'=>(double)$valor->suma2009,
                                        'color'=>'#EC33FF'
                                    );
                                    $s2010[] = array(
                                        'y'=>(int)$valor->suma2010,
                                        'color'=>'#9C33FF'
                                    );
                                    $s2011[]= array(
                                        'y'=>(int)$valor->suma2011,
                                        'color'=>'#4F81BD'
                                    );
                                    $s2012[] = array(
                                        'y'=>(int)$valor->suma2012,
                                        'color'=>'#33BBFF'
                                    );
                                    $s2013[] = array(
                                        'y'=>(double)$valor->suma2013,
                                        'color'=>'#33FFE3'
                                    );
                                    $s2014[] = array(
                                        'y'=>(int)$valor->suma2014,
                                            'color'=>'gray'
                                        );
                                    $s2015[]= array(
                                        'y'=>(int)$valor->suma2015,
                                        'color'=>'#86FF33'
                                    );
                                    $s2016[] = array(
                                        'y'=>(int)$valor->suma2016,
                                        'color'=>'#E0FF33'
                                    );
                                    $s2017[] = array(
                                        'y'=>(int)$valor->suma2017,
                                        'color'=>'#FFCE33'
                                    );
                                    $s2018[]= array(
                                        'y'=>(int)$valor->suma2018,
                                        'color'=>'#FF9633'
                                    );
                                    $s2019[] = array(
                                        'y'=>(int)$valor->suma2019,
                                        'color'=>'#C0504D'
                                    );
                                    $titulo[] = array(
                                        $valor->nombre,
                                    );	
                                    }								
                                    $data['data'] = array(
                                        'data2009' => $s2009,
                                        'data2010' => $s2010,
                                        'data2011' => $s2011,
                                        'data2012' => $s2012,
                                        'data2013' => $s2013,
                                        'data2014' => $s2014,
                                        'data2015' => $s2015,
                                        'data2016' => $s2016,
                                        'data2017' => $s2017,
                                        'data2018' => $s2018,
                                        'data2019' => $s2019,
                                        'datatitulo' =>$titulo,			
                                    );
                                }
                                else if($params->tipo == "Universidades"){
                                    $pg = new PgSql();
                                    $obj = $pg->getRows("select nombre,suma1,suma2,suma3,suma4,suma5 from obs.gen_universidades_oxi_excel");
                                    foreach ($obj as $valor){
                                        $s2009[] = array(
                                            
                                            'color'=>'#EC33FF'
                                        );
                                        $s2010[] = array(
                                            
                                            'color'=>'#9C33FF'
                                        );
                                        $s2011[]= array(
                                            
                                            'color'=>'#4F81BD'
                                        );
                                        $s2012[] = array(
                                            
                                            'color'=>'#33BBFF'
                                        );
                                        $s2013[] = array(
                                            
                                            'color'=>'#33FFE3'
                                        );
                                        $s2014[] = array(
                                            
                                                'color'=>'gray'
                                            );
                                        $s2015[]= array(
                                            'y'=>(int)$valor->suma1,
                                            'color'=>'#86FF33'
                                        );
                                        $s2016[] = array(
                                            'y'=>(int)$valor->suma2,
                                            'color'=>'#E0FF33'
                                        );
                                        $s2017[] = array(
                                            'y'=>(int)$valor->suma3,
                                            'color'=>'#FFCE33'
                                        );
                                        $s2018[]= array(
                                            'y'=>(int)$valor->suma4,
                                            'color'=>'#FF9633'
                                        );
                                        $s2019[] = array(
                                            'y'=>(int)$valor->suma5,
                                            'color'=>'#C0504D'
                                        );
                                        $titulo[] = array(
                                            $valor->nombre,
                                        );	
                                        }								
                                        $data['data'] = array(
                                            'data2009' => $s2009,
                                            'data2010' => $s2010,
                                            'data2011' => $s2011,
                                            'data2012' => $s2012,
                                            'data2013' => $s2013,
                                            'data2014' => $s2014,
                                            'data2015' => $s2015,
                                            'data2016' => $s2016,
                                            'data2017' => $s2017,
                                            'data2018' => $s2018,
                                            'data2019' => $s2019,
                                            'datatitulo' =>$titulo,			
                                        );
                                    }
        
                    
                    break;
                    case 'cirplxMontoOxi':
                        $pg = new PgSql();								
                        $data['data'] = $pg->getRows("
                        with data as(
                         select * from (select 
                         '2011'::text as ano,
                         4084.83::numeric as ciprl,
                         (select sum(ano2011) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2012'::text as ano,
                         5005.53::numeric as ciprl,
                         (select sum(ano2012) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2013'::text as ano,
                         5995.65::numeric as ciprl,
                         (select sum(ano2013) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2014'::text as ano,
                         6060.72::numeric as ciprl,
                         (select sum(ano2014) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                          union all 
                         select * from (select 
                         '2015'::text as ano,
                         5520.61::numeric as ciprl,
                         (select sum(ano2015) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2016'::text as ano,
                         4565.47::numeric as ciprl,
                         (select sum(ano2016) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2017'::text as ano,
                         3680.25::numeric as ciprl,
                         (select sum(ano2017) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                          union all 
                         select * from (select 
                         '2018'::text as ano,
                         (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2018)x) ciprl,
                         (select sum(ano2018) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         union all 
                         select * from (select 
                         '2019'::text as ano,
                         (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2019)x) ciprl,
                         (select sum(ano2019) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                          union all 
                         select * from (select 
                         '2020'::text as ano,
                         (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2020)x) ciprl,
                         (select sum(ano2020) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                         ) select ano,ciprl,oxi,(oxi*100)/ciprl porcentaje from data order by ano");
                    break;
					case 'chartPieEmpresaOxi'://ojop este falta subir
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("with data as(
                            select 
                              sum(top10) top10,
                              (select sum(total) from observatorio.gen_temporal_empresas_oxi)total
                             from (select total top10 from observatorio.gen_temporal_empresas_oxi order by total desc limit 10)x)
                             select  top10,(total-top10) total from data");
                    break; 
					 case 'empresasOXIExcel':
                        $pg = new PgSql();
                        if($params->desdeLimite == 1){
                            $data['data'] = $pg->getRows("select * from observatorio.gen_temporal_empresas_oxi order by total desc
                            limit " . $params->limitFin . " offset " . $params->limitInicio . "");
                        } elseif ($params->desdeLimite == 2) {
                            $data['data'] = $pg->getRows("select nombre,ano2015,ano2016,ano2017,ano2018,ano2019,ano2020,(ano2015+ano2016+ano2017+ano2018+ano2019+ano2020 ) total from (select *  from observatorio.gen_temporal_empresas_oxi)x
                            order by total desc
                            limit " . $params->limitFin . " offset " . $params->limitInicio . "");
                        }
                    break;
                    case 'chartStakedEmpresas':
                        $sAdjudicado   = array();
                        $sConcluido   = array();
                        $pg = new PgSql();								
                                $obj = $pg->getRows("select distinct a.empresa as empresa,(select sum(monto_inversion) from obs.gen_proyectos_adjudicados where estado='Concluido' and empresa like '%'||a.empresa::text||'%') as concluido,
                                (select sum(monto_inversion) from obs.gen_proyectos_adjudicados where estado='Adjudicado' and empresa like '%'||a.empresa::text||'%') as adjudicado,
                                (select sum(monto_inversion) from obs.gen_proyectos_adjudicados where empresa like '%'||a.empresa::text||'%') from obs.gen_proyectos_adjudicados a 
                                inner join obs.gen_empresas_oxi_excel b 
                                on a.empresa=b.nombre order by (select sum(monto_inversion) from obs.gen_proyectos_adjudicados where empresa like '%'||a.empresa::text||'%') desc limit ".$params->limitFin." OFFSET ".$params->limitInicio."");
                                foreach ($obj as $valor){
                                    $sAdjudicado[] = array(
                                        'y'=>(double)$valor->adjudicado,
                                        'color'=>'#ffc107'
                                    );
                                    $sConcluido[] = array(									
                                        'y'=>(double)$valor->concluido,
                                        'color'=>'#007bff'
                                    );
                                    $titulo[] = array(
                                        $valor->empresa,
                                    );
                                }
                                $data['data'] = array(
                                    'sAdjudicado' => $sAdjudicado,
                                    'sConcluido' => $sConcluido,
                                    'datatitulo' =>$titulo,	
                                );
                    break;	
					case 'ciprlXuniv':
                        $pg    = new PgSql();
                        $arr   = array();
                        $obj   = $pg->getRows("select
                         'column' as type,
                         'Canon MEF' as name,
                         '#2f7ed8' as color,
                         (select string_agg(round(canon_mef)::text, ',') from(select canon_mef from obs.gen_universidades_det where coduniv = " . $params->coduniv . " and ano between 2014 and 2020 order by ano asc) x) as data,
                         (select min(ano) from obs.gen_universidades_det where coduniv = " . $params->coduniv . " and ano between 2014 and 2019) as scale
                        union all
                        select
                         'spline' as type,
                         'CIPRL MEF' as name,
                         '#c42525' as color,
                         (select string_agg(ciprl_mef::text, ',') from(select case ciprl_mef when 0 then 'null' else round(ciprl_mef)::text end as ciprl_mef from obs.gen_universidades_det where coduniv = " . $params->coduniv . " and ano between 2014 and 2020 order by ano asc) x) as data,
                         0");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "name"  => $valor->name,
                                "color" => $valor->color,
                                "data"  => isNullOrEmpty(array_map('intval', explode(',', $valor->data)))
                            ));
                        }
                        $data['data'] = array(
                            'pointStart' => (int)$obj{0}->scale,
                            'series'     => $arr
                        );
                        
                         
                    break;
					case 'getCiprlUniversidades':
                        $pg = new PgSql();
                        $data['data'] = $pg->getRows("select 
                        universidad,
                        (select ciprl_mef from obs.gen_universidades_det  where  ano=2017 and coduniv=x.coduniv) s2017,
                        (select ciprl_mef from obs.gen_universidades_det  where  ano=2018 and coduniv=x.coduniv)s2018,
                        (select ciprl_mef from obs.gen_universidades_det  where  ano=2019 and coduniv=x.coduniv)s2019,
                        (select ciprl_mef from obs.gen_universidades_det  where  ano=2020 and coduniv=x.coduniv)s2020
                        from obs.gen_universidades x where coduniv not in(1)");
                    break;
					case 'getSnipForEmpresa':
					$pg = new PgSql();
					$data['data'] = $pg->getRows("select snip as snips from obs.gen_proyectos_adjudicados
								where upper(empresa) like'%" . $params->empresa . "%'");
					break;
					 case 'empconceptos':
                        $pg = new PgSql();
                        $utilidades = array();
                        $ingresos   = array();
                        $obj 		= $pg->getRows("SELECT * from obs.fn_proyecto_conceptos('" . $params->ruc . "')");
                        
                        foreach ($obj as $valor){
                            if($valor->campo =="UTILIDAD"){
                                array_push($utilidades, (int)$valor->monto);
                            }else{
                                array_push($ingresos, (int)$valor->monto);
                            }
                        }
                        
                        $data['data'] = array(
                            'pointStart' =>  (int)reset($obj)->ano,
                            'series'     =>  array(
                                'ingresos'   => $ingresos,
                                'utilidades' => $utilidades
                            )
                        );
                    break;
					case 'wordRbtnPipsTotal':
                        $pg = new PgSql();
                        if($params->codnivel == 1){
                            $data['data'] = $pg->getRows("select
                                array_to_json(array_agg(x)) as subprograma
                            from(
                            select 
                                ". $params->tipoChange." as name, 
                                count(*) as weight,  
                                sum(monto) as soles
                            from obs.gen_proyecto op 
                            inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                            inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                            inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                            inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                            inner join obs.gen_programaproyecto opp on op.codprogramaproyecto=opp.codprogramaproyecto
                            where op.coddpto='" . $params->codubigeo . "' and op.codnivelgobierno!=2 and op.codfase in(2,3,4)
                            group by ". $params->tipoChange."
                            ) x");
                    
                        }elseif($params->codnivel == 2){
                            $data['data'] = $pg->getRows("select
                                array_to_json(array_agg(x)) as subprograma
                            from(
                            select 
                                ". $params->tipoChange." as name, 
                                count(*) as weight,  
                                sum(monto) as soles
                            from obs.gen_proyecto op 
                            inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                            inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                            inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                            inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                            inner join obs.gen_programaproyecto opp on op.codprogramaproyecto=opp.codprogramaproyecto
                            where  op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.codnivelgobierno!=5 and op.codfase in(2,3,4)
                            group by ". $params->tipoChange."
                            ) x");
                        
                        }	
                        elseif($params->codnivel == 3){
                            $data['data'] = $pg->getRows("select
                                array_to_json(array_agg(x)) as subprograma
                            from(
                            select 
                                ". $params->tipoChange." as name, 
                                count(*) as weight,  
                                sum(monto) as soles
                            from obs.gen_proyecto op 
                            inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                            inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                            inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                            inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                            inner join obs.gen_programaproyecto opp on op.codprogramaproyecto=opp.codprogramaproyecto
                            where  op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.coddist=substring('" . $params->codubigeo . "' from 5 for 2) and op.codnivelgobierno!=1 and op.codfase in(2,3,4)
                            group by ". $params->tipoChange."
                            ) x");
                
                        }
                    break;
                    case 'indXfuncion':
                        $pg    = new PgSql();		
                        $count = 0;
                        
                        $obj = $pg->getRows("select funcion, case " . $params->codfase . " when 4 then pv when 6 then et when 2 then fo end as total from obs.gen_funcion_x_ubigeo where codubigeo = '" . $params->codubigeo . "' and funcion != 'TOTAL'");					
                        $arr   = array();
                        
                        foreach ($obj as $key){
                            $tmp = array();								
                            $tmp['name'] = $key->funcion;
                            $tmp['y']    = (int)$key->total;
                            $count      += (int)$key->total;
                            array_push($arr, $tmp);
                        }
                        
                        $data['data'] = array(
                            'name' => 'Función',
                            'colorByPoint' => true,
                            'data'  => $arr,
                            'total' => number_format($count)
                        );
                    break;
					case 'fichaEntidadAmpliar':
                        $pg    = new PgSql();
                        if($params->tipo==2){
                            $data['data']  = $pg->getRows("select 
                                idnivel,
                                codubigeo,
                                coddpto,
                                nomubigeo,
                                monto_ciprl,
                                tope_ciprl,
                                quantil,
                                cruza_via,
                                poblacion
                             from observatorio.gen_dpto_geojson where codubigeo='".$params->ubigeo."'");
                        }else if($params->tipo==4){
                            $data['data']  = $pg->getRows("select 
                                idnivel,
                                codprov,
                                codubigeo,
                                coddpto,
                                nomubigeo,
                                monto_ciprl,
                                tope_ciprl,
                                quantil,
                                cruza_via,
                                poblacion
                             from observatorio.gen_provincia_geojson where codubigeo='".$params->ubigeo."'");
                        }else if($params->tipo==6){
                            $data['data']  = $pg->getRows("select 
                                idnivel,
                                codprov,
                                codubigeo,
                                coddpto,
                                nomubigeo,
                                monto_ciprl,
                                tope_ciprl,
                                quantil,
                                cruza_via,
                                poblacion
                             from observatorio.gen_distrito_geojson where codubigeo='".$params->ubigeo."'");
                        }      
                       
                    break;
					case 'ciprlXregion':
                        $pg    = new PgSql();
                        $arr   = array();
                        if($params->idnivel == 1){
                            $obj   = $pg->getRows("select
                             'column' as type,
                             'Canon MEF' as name,
                             '#2f7ed8' as color,
                             (select string_agg(round(canon)::text, ',') from(select canon from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
                            union all
                           select
                            'spline' as type,
                            'CIPRL MEF' as name,
                            '#c42525' as color,
                            (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl)::text end as ciprl from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                            0
                             union all  
                            select 'column' as type,
                             'PIM Proyectos' as name,
                             '#9BBB58' as color,
                             (select string_agg(round(pim)::text, ',') from(select pim from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
                            
                            ");
                        }elseif ($params->idnivel == 2) {						
                            $obj   = $pg->getRows("select
                             'column' as type,
                             'Canon MEF' as name,
                             '#2f7ed8' as color,
                             (select string_agg(round(canon)::text, ',') from(select canon from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
                            union all
                           select
                            'spline' as type,
                            'CIPRL MEF' as name,
                            '#c42525' as color,
                            (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl)::text end as ciprl from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                            0
                            union all  
                            select 'column' as type,
                             'PIM Proyectos' as name,
                             '#9BBB58' as color,
                             (select string_agg(round(pim)::text, ',') from(select pim from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
                            
                            
                            ");					
                        }elseif ($params->idnivel == 3) {						
                            $obj   = $pg->getRows("select
                             'column' as type,
                             'Canon MEF' as name,
                             '#2f7ed8' as color,
                             (select string_agg(round(canon)::text, ',') from(select canon from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
                            union all
                           select
                            'spline' as type,
                            'CIPRL MEF' as name,
                            '#c42525' as color,
                            (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl)::text end as ciprl from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                            0
                             union all  
                            select 'column' as type,
                             'PIM Proyectos' as name,
                             '#9BBB58' as color,
                             (select string_agg(round(pim)::text, ',') from(select pim from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020 order by ano asc) x) as data,
                             (select min(ano) from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2020) as scale
    
                            ");					
                        }
                        
                        foreach ($obj as $valor){
                        
                                            
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "name"  => $valor->name,
                                "color" => $valor->color,
                                "data"  => isNullOrEmpty(array_map('intval', explode(',', $valor->data)))
                            ));
                        }
                        
                        $data['data'] = array(
                            'pointStart' => (int)$obj{0}->scale,
                            'series'     => $arr
                        );
                    break;
					
					 case 'chartciprliformacion':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("with data as(
                            select 
                               departamento,
                               ciprl ciprl2018,
                               canon canon2018,
                              (select ciprl from obs.gen_regiones_det where ano in (2019) and coddpto= x.coddpto) ciprl2019,
                              (select ciprl from obs.gen_regiones_det where ano in (2020) and coddpto= x.coddpto) ciprl2020,
                              (select canon from obs.gen_regiones_det where ano in (2019) and coddpto= x.coddpto) canon2019,
                              (select canon from obs.gen_regiones_det where ano in (2020) and coddpto= x.coddpto) canon2020
                             from obs.gen_regiones_det x 
                             where ano in (2018))
                             select 
                                *
                              from data order by departamento");
                    break;  
					case 'chartciprliformacionMinsa':
                        $pg    = new PgSql();
                        if($params->tipo=='fallecidos'){
                            $data['data']   = $pg->getRows("select
                            x.nombdep,
                            x.fallecidos,
                            (fallecidos /  poblacion) * 10000 as fallecidos_poblacion_1000
                           from(
                           select
                            d.nombdep as nombdep,
                            d.first_iddp || '0000' as ubigeo,
                            (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,2) = d.first_iddp) as fallecidos,
                            (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = d.first_iddp and cod_indicador = '0301') as poblacion
                           from departamento d
                           where
                            d.first_iddp not in ('40', '26') 
                           group by
                            d.nombdep,
                            d.first_iddp
                           order by
                            1
                           ) x order by nombdep
                           ");
                        }
                        
                    break;   
                    case 'chartciprliformacionProvincia':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("with data as(
                            select 
                              nombprov,
                              ciprl ciprl2018,
                              (select ciprl from obs.gen_provincias_det d1  where ano=2019 and ubigeo=d.ubigeo) ciprl2019,
                              (select ciprl from obs.gen_provincias_det d1  where ano=2020 and ubigeo=d.ubigeo) ciprl2020,
                              canon canon2018,
                              (select canon from obs.gen_provincias_det d1  where ano=2019 and ubigeo=d.ubigeo) canon2019,
                              (select canon from obs.gen_provincias_det d1  where ano=2020 and ubigeo=d.ubigeo) canon2020
                             from obs.gen_provincias_det d  inner join provincia p on d.ubigeo=p.first_idpr where ano=2018 and first_nomb='" . $params->departamento . "')
                             select * from data order by nombprov");
                    break;
					 case 'chartciprliformacionProvinciaMinsa':
                        $pg    = new PgSql();
                        if($params->tipo=='fallecidos'){
                            $data['data']   = $pg->getRows("with data as(
                                select
                                 x.ubigeo,
                                 (select nombdep from departamento where first_iddp = substr(x.ubigeo,1,2)) as dpto,
                                 x.nombprov,
                                 x.poblacion,
                                 x.fallecidos,
                                 (fallecidos /  poblacion) * 1000 as fallecidos_poblacion_1000
                                from(
                                select
                                 p.nombprov as nombprov,
                                 p.first_idpr as ubigeo, 
                                 (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,4) = p.first_idpr) as fallecidos,
                                 (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov = p.first_idpr and cod_indicador = '0301') as poblacion
                                from provincia p
                                where
                                 (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,4) = p.first_idpr) > 0 
                                group by
                                 p.nombprov,
                                 p.first_idpr
                                order by
                                 1
                                ) x )
                                select * from data where dpto='".$params->departamento."' order by nombprov");
                        }
                        
                    break;  
                    case 'chartciprliformacionDistrito':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("with data as(   
                            select 
                             distrito,
                             ciprl ciprl2018,
                            (select ciprl from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2019) ciprl2019,
                            (select ciprl from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2020) ciprl2020,
                             canon canon2018,
                            (select canon from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2019) canon2019,
                            (select canon from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2020) canon2020
                           from obs.gen_distritos_det d inner join distrito_f f on d.ubigeo=f.iddist where ano=2018 and provincia='" . $params->provincia . "' and departamen='" . $params->region . "')
                           select * from data  order by distrito");
                    break;      
					case 'chartciprliformacionDistritoMinsa':
                        $pg    = new PgSql();
                        if($params->tipo=='fallecidos'){
                            $data['data']   = $pg->getRows("select
                            x.nombprov,
                            x.nombdist,
                            x.poblacion,
                            x.fallecidos,
                            (fallecidos /  poblacion) * 1000 as fallecidos_poblacion_1000,
                            ntile(10) over (order by ((fallecidos /  poblacion) * 1000)) as quantil
                           from(
                           select
                            d.departamen,
                            d.provincia as nombprov,
                            d.distrito as nombdist,
                            d.iddist as ubigeo,
                            (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,6) = d.iddist) as fallecidos,
                            (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov||coddist = d.iddist and cod_indicador = '0301') as poblacion
                           from distrito_f d
                           where
                            (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,6) = d.iddist) > 0
                           group by
                            d.departamen,
                            d.provincia,
                            d.distrito,
                            d.iddist
                           order by
                            1
                           ) x where nombprov='" . $params->provincia . "' AND departamen='" . $params->region . "'
                           order by  nombdist");
                        }
                        
                    break; 
					case 'chartciprliformacionSinadef':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("select * from (
                            select  
                            d.nombdep as nombdep,
                            (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2017,
                            (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2018,
                            (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2019,
                            (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2020,
                             (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = d.first_iddp and cod_indicador = '0301') as poblacion
                            from departamento d
                            where
                             d.first_iddp not in ('40', '26') )x order by nombdep");
                    break;  
                    case 'chartciprliformacionSinadefProvincia':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows(" select 
                        nombprov,
                       (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2017,
                       (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2018,
                       (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2019,
                       (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2020,
                       (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov = p.first_idpr and cod_indicador = '0301') as poblacion
                         from provincia p
                         where  first_nomb='" . $params->departamento . "' order by nombprov
                       ");
                    break;
                    case 'chartciprliformacionSinadefDistrito':
                        $pg    = new PgSql();
                        $data['data']   = $pg->getRows("select 
                        distrito,
                        (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2017,
                         (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2018,
                         (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2019,
                         (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2020,
                        (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov||coddist = p.iddist and cod_indicador = '0301') as poblacion
                        from distrito_f p
                        where departamen='" . $params->region . "' and provincia='" . $params->provincia . "' order by distrito
                       ");
                    break;      
					case 'SludxMesesMinsa':
                        $pg    = new PgSql();
                        if($params->tipo==2){
                            $data['data']  = $pg->getRows("select
                            CASE 
                             WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                             WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                             WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                             WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                             WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                             WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
							 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
							 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'

                            END mes,
                            count(*) fallecidos,
                            (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = '" . $params->departamento . "' and cod_indicador = '0301') as poblacion
                           from obs.tmp_fallecidos_minsa 
                           where departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') 
                           group by substr(fecha_fallecimiento,5,2) 
                           order by substr(fecha_fallecimiento,5,2) asc
                           ");
                        }else if($params->tipo==4){
                            $data['data']  = $pg->getRows("select
                            CASE 
                             WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                             WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                             WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                             WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                             WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                             WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
							 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
							 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'
							 
                            END mes,
                            (select sum(valor) from obs.gen_indicadores_prov_detalle where ubigeo = '" . $params->provincia . "' and cod_indicador = '0301') as poblacion,
                            count(*) fallecidos
                           from obs.tmp_fallecidos_minsa 
                           where 
                            departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                            provincia=(select nombprov from provincia where first_idpr ='" . $params->provincia . "' limit 1) 
                           group by substr(fecha_fallecimiento,5,2) 
                           order by substr(fecha_fallecimiento,5,2) asc
                           ");
                        }else if($params->tipo==6){
                            $data['data']  = $pg->getRows("select
                            CASE 
                             WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                             WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                             WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                             WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                             WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                             WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
							 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
							 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'
                            END mes,
                            (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = '" . $params->distrito . "' and cod_indicador = '0301') as poblacion,
                            count(*) fallecidos
                           from obs.tmp_fallecidos_minsa 
                           where 
                            departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                            provincia=(select nombprov from provincia where first_idpr ='" . $params->provincia . "' limit 1) and
                            distrito=(select distrito from distrito_f where iddist ='" . $params->distrito . "'  limit 1 )
                           group by substr(fecha_fallecimiento,5,2) 
                           order by substr(fecha_fallecimiento,5,2) asc
                           ");
                        }
                        
                    break;  
					case 'SludxMesesSinadef':
                        $pg    = new PgSql();
                        if($params->tipo==2){
                            $data['data']  = $pg->getRows("
                            with data as( 
                            select 
                             count(*) total,
                             (select * from obs.fn_nombre_mes(mes)) meses,
                             (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = '" . $params->departamento . "' and cod_indicador = '0301') as poblacion,
                             anio,
                             mes
                            from obs.tmp_fallecidos_sinadef 
                            where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "')
                            group by mes,anio)
                            select * from data 
                            order by mes,anio");
                        }else if($params->tipo==4){
                            $data['data']  = $pg->getRows(" with data as( 
                                select 
                                 count(*) total,
                                 (select * from obs.fn_nombre_mes(mes)) meses,
                                 (select sum(valor) from obs.gen_indicadores_prov_detalle where ubigeo = '" . $params->provincia . "' and cod_indicador = '0301') as poblacion,
                                 anio,
                                 mes
                                from obs.tmp_fallecidos_sinadef 
                                where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and 
                                provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') 
                                group by mes,anio)
                                select * from data 
                                order by mes,anio");
                        }else if($params->tipo==6){
                            $data['data']  = $pg->getRows(" with data as( 
                                select 
                                 count(*) total,
                                 (select * from obs.fn_nombre_mes(mes)) meses,
                                 (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = '" . $params->distrito . "' and cod_indicador = '0301') as poblacion,
                                 anio,
                                 mes
                                from obs.tmp_fallecidos_sinadef 
                                where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and 
                                provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') and
                                distrito_domicilio=(select distrito from distrito_f where iddpto='" . $params->departamento . "' and idprov='" . $params->provincia . "' and iddist='" . $params->distrito . "')
                                group by mes,anio)
                                select * from data 
                                order by mes,anio");
                        }
                        
                    break; 			
                    case 'distxCorredoresLogisticosxRuta':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select
                             ruta,
                             sum(canon) as canon,
                             sum(ciprl) as ciprl,
                             sum(pim) as pim,
                             sum(valor) as valor
                            from obs.gen_corredoresprincipales p
                            inner join distrito_f d on st_intersects(p.geom,d.geom) 
                            inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                            inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'		
                            where corredor='".$params->ruta."' 
                            group by 
                             ruta)
                            select 
                                                                            'column' as type,
                                                                            0 as yAxis,
                                                                            'Canon' as name,
                                                                            string_agg(round(canon/1000000,1)::text, ',') as data,
                                                                            string_agg(ruta, ',') as categories,
                                                                            'M' as valueSuffix
                                                                           from data
                                                                           union all
                                                                           select
                                                                           'column' as type,
                                                                           0 as yAxis,
                                                                           'CIPRL' as name,
                                                                           string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                                           '' as categories,
                                                                           'M' as valueSuffix
                                                                           from data
                                                                           union all
                                                                            select 
                                                                           'column' as type,
                                                                            0 as yAxis,
                                                                            'PIM' as name,
                                                                            string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                                                                            '' as categories,
                                                                            'M' as valueSuffix
                                                                            from data
                                                                           union all
                                                                           select 
                                                                           'spline' as type,
                                                                           1 as yAxis,
                                                                           'Poblacion' as name,
                                                                           string_agg(round(valor/1000,1)::text, ',') as data,
                                                                           '' as categories,
                                                                           'K' as valueSuffix
                                                                           from data
                           ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );
                    break;
                    case 'distxCorredoresLogisticosxRegion':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                             d.first_iddp,
                             nombdep,
                             canon,
                             ciprl,
                             pim,
                             valor
                            from obs.gen_corredoresprincipales l
                            inner join departamento d on st_intersects(l.geom,d.geom) 
                            inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
                            inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
                            where corredor='".$params->corredor."' and ruta ='".$params->ruta."'
                            group by 
                             d.first_iddp,
                             nombdep,
                             canon,
                             ciprl,
                             pim,
                             valor)
                             select
                             'column' as type,
                             0 as yAxis,
                            'Canon' as name,
                                                                             string_agg(round(canon/1000000,1)::text, ',') as data,
                                                                             string_agg(nombdep, ',') as categories,
                                                                             'M' as valueSuffix
                                                                            from data
                                                                            union all
                                                                            select
                                                                            'column' as type,
                                                                            0 as yAxis,
                                                                            'CIPRL' as name,
                                                                            string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                                            '' as categories,
                                                                            'M' as valueSuffix
                                                                            from data
                                                                            union all
                                                                             select 
                                                                            'column' as type,
                                                                             0 as yAxis,
                                                                             'PIM' as name,
                                                                             string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                                                                             '' as categories,
                                                                             'M' as valueSuffix
                                                                             from data
                                                                            union all
                                                                            select 
                                                                            'spline' as type,
                                                                            1 as yAxis,
                                                                            'Poblacion' as name,
                                                                            string_agg(round(valor/1000,1)::text, ',') as data,
                                                                            '' as categories,
                                                                            'K' as valueSuffix
                                                                            from data
                           ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );
                    break;
                    case 'distxCorredoresLogisticosxdistrito':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select
                           
                             distrito,
                             canon ,
                             ciprl,
                             pim,
                             valor
                            from obs.gen_corredoresprincipales p
                            inner join distrito_f d on st_intersects(p.geom,d.geom) 
                            inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                            inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'		
                            where corredor='".$params->corredor."' and ruta ='".$params->ruta."' and departamen='".$params->region."'
                            order by iddist
                            )select
                           'column' as type,
                                                                            0 as yAxis,
                                                                            'Canon' as name,
                                                                            string_agg(round(canon/1000000,1)::text, ',') as data,
                                                                            string_agg(distrito, ',') as categories,
                                                                            'M' as valueSuffix
                                                                           from data
                                                                           union all
                                                                           select
                                                                           'column' as type,
                                                                           0 as yAxis,
                                                                           'CIPRL' as name,
                                                                           string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                                           '' as categories,
                                                                           'M' as valueSuffix
                                                                           from data
                                                                           union all
                                                                            select 
                                                                           'column' as type,
                                                                            0 as yAxis,
                                                                            'PIM' as name,
                                                                            string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                                                                            '' as categories,
                                                                            'M' as valueSuffix
                                                                            from data
                                                                           union all
                                                                           select 
                                                                           'spline' as type,
                                                                           1 as yAxis,
                                                                           'Poblacion' as name,
                                                                           string_agg(round(valor/1000,1)::text, ',') as data,
                                                                           '' as categories,
                                                                           'K' as valueSuffix
                                                                           from data
                           ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );
                    break;
                    case 'distXlineaFerrea':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                            d.first_iddp,
                            nombdep,
                            canon,
                            ciprl,
                            pim,
                            valor
                            from obs.gen_lineaferrea l
                            inner join departamento d on st_intersects(l.geom,d.geom) 
                            inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
                            inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
                            where nam='" . $params->nam ."'
                            GROUP BY d.first_iddp,
                                nombdep,
                                canon,
                                ciprl,
                                pim,
                                valor)						
                            select 
                                                     'column' as type,
                                                     0 as yAxis,
                                                     'Canon' as name,
                                                     string_agg(round(canon/1000000,1)::text, ',') as data,
                                                     string_agg(nombdep, ',') as categories,
                                                     'M' as valueSuffix
                                                    from data
                                                    union all
                                                    select
                                                    'column' as type,
                                                    0 as yAxis,
                                                    'CIPRL' as name,
                                                    string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                    '' as categories,
                                                    'M' as valueSuffix
                                                    from data
                                                    union all
                                                     select 
                                                    'column' as type,
                                                     0 as yAxis,
                                                     'PIM' as name,
                                                     string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                                                     '' as categories,
                                                     'M' as valueSuffix
                                                     from data
                                                    union all
                                                    select 
                                                    'spline' as type,
                                                    1 as yAxis,
                                                    'Poblacion' as name,
                                                    string_agg(round(valor/1000,1)::text, ',') as data,
                                                    '' as categories,
                                                    'K' as valueSuffix
                                                    from data
                            ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );	
                    break;
                    case 'distXlineaFerreawidthDistrito':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                             iddist,
                             d.distrito,
                             ciprl,
                             canon,
                             pim,
                             valor 
                            from obs.gen_lineaferrea l 
                            inner join distrito_f d on st_intersects(l.geom,d.geom) 
                            inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                            inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'
                            where nam='" . $params->nam ."' and departamen='" . $params->region ."' group by iddist,d.distrito,
                             ciprl,
                             canon,
                             pim,
                             valor 
                            order by iddist)
                            select 
                             'column' as type,
                             0 as yAxis,
                             'Canon' as name,
                             string_agg(round(canon/1000000,1)::text, ',') as data,
                             string_agg(distrito, ',') as categories,
                             'M' as valueSuffix
                            from data
                            union all
                            select
                            'column' as type,
                            0 as yAxis,
                            'CIPRL' as name,
                            string_agg(round(ciprl/1000000,1)::text, ',') as data,
                            '' as categories,
                            'M' as valueSuffix
                            from data
                            union all
                             select 
                            'column' as type,
                             0 as yAxis,
                             'PIM' as name,
                             string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                             '' as categories,
                             'M' as valueSuffix
                             from data
                            union all
                            select 
                            'spline' as type,
                            1 as yAxis,
                            'Poblacion' as name,
                            string_agg(round(valor/1000,1)::text, ',') as data,
                            '' as categories,
                            'K' as valueSuffix
                            from data");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );	
                    break;
                    case 'distXoleoducto':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as(
                            select
                                    ROW_NUMBER () OVER (ORDER BY ramal),
                                         x.ramal,
                                         x.tramo,      
                                         (select count(*) from regexp_split_to_table(x.distritos, ',')) as ndistritos,
                                         x.distritos,
                                     x.distritos_in,
                                         (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and cod_indicador = '0301') as poblacion,
                                         (select sum(canon) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as canon,
                                         (select sum(ciprl) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as ciprl, 
                                         (select sum(pim) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as pia			 
                                        from(
                                        select
                                         x.ramal,
                                         x.tramo,     
                                         (select string_agg(x.iddist, ',') from (select z.iddist from obs.gen_oleoducto y inner join distrito z on st_intersects(z.geom, y.geom) where tramo = x.tramo group by z.gid) x) as distritos,
                                     (select string_agg(x.iddist, ''',''') from (select z.iddist from obs.gen_oleoducto y inner join distrito z on st_intersects(z.geom, y.geom) where tramo = x.tramo group by z.gid) x) as distritos_in
                                        from obs.gen_oleoducto x
                                        group by
                                         x.ramal, 
                                         x.tramo			 
                                        ) x where x.ramal not in ('Sin información'))
                                        select 
                                                 'column' as type,
                                                 0 as yAxis,
                                                 'Canon' as name,
                                                 string_agg(round(canon/1000000,1)::text, ',') as data,
                                                 string_agg(concat(ramal,'-',tramo), ',') as categories,
                                                 'M' as valueSuffix
                                                from data
                                                union all
                                                select
                                                 'column' as type,
                                                 0 as yAxis,
                                                 'CIPRL' as name,
                                                 string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                 '' as categories,
                                                 'M' as valueSuffix
                                                from data
                                                union all
                                                select 
                                                 'column' as type,
                                                 0 as yAxis,
                                                 'PIM' as name,
                                                 string_agg(round(pia::numeric/1000000,1)::text, ',') as data,
                                                 '' as categories,
                                                 'M' as valueSuffix
                                                from data
                                                union all
                                                select 
                                                 'spline' as type,
                                                 1 as yAxis,
                                                 'Poblacion' as name,
                                                 string_agg(round(poblacion/1000,1)::text, ',') as data,
                                                 '' as categories,
                                                 'K' as valueSuffix
                                                from data
                            ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );	
                    break;
                    case 'distxOleoductoxTramo':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                             iddist,
                             d.distrito,
                             ciprl,
                             canon,
                             pim,
                             valor  
                            from obs.gen_oleoducto o 
                            inner join distrito_f d on st_intersects(o.geom,d.geom) 
                            inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                            inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'						 
                            where concat(o.ramal,'-',o.tramo)='".$params->ramal."')
                            select
                             'column' as type,
                             0 as yAxis,
                             'Canon' as name,
                             string_agg(round(canon/1000000,1)::text, ',') as data,
                             string_agg(distrito, ',') as categories,
                             'M' as valueSuffix
                            from data
                            union all
                            select
                             'column' as type,
                             0 as yAxis,
                             'CIPRL' as name,
                             string_agg(round(ciprl/1000000,1)::text, ',') as data,
                             '' as categories,
                             'M' as valueSuffix
                            from data
                            union all
                            select 
                             'column' as type,
                             0 as yAxis,
                             'PIM' as name,
                             string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                             '' as categories,
                             'M' as valueSuffix
                            from data
                            union all
                            select 
                             'spline' as type,
                             1 as yAxis,
                             'Poblacion' as name,
                             string_agg(round(valor/1000,1)::text, ',') as data,
                             '' as categories,
                             'K' as valueSuffix
                            from data");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );
                    break;
                    case 'distXhidrovias':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                            d.first_iddp,
                            nombdep,
                            canon,
                            ciprl,
                            pim,
                            valor
                            from obs_new.gen_hidrografia l
                            inner join departamento d on st_intersects(l.geom,d.geom) 
                            inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
                            inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
                            where l.id='" . $params->id ."'
                            GROUP BY d.first_iddp,
                                nombdep,
                                canon,
                                ciprl,
                                pim,
                                valor)						
                            select 
                                                     'column' as type,
                                                     0 as yAxis,
                                                     'Canon' as name,
                                                     string_agg(round(canon/1000000,1)::text, ',') as data,
                                                     string_agg(nombdep, ',') as categories,
                                                     'M' as valueSuffix
                                                    from data
                                                    union all
                                                    select
                                                    'column' as type,
                                                    0 as yAxis,
                                                    'CIPRL' as name,
                                                    string_agg(round(ciprl/1000000,1)::text, ',') as data,
                                                    '' as categories,
                                                    'M' as valueSuffix
                                                    from data
                                                    union all
                                                     select 
                                                    'column' as type,
                                                     0 as yAxis,
                                                     'PIM' as name,
                                                     string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                                                     '' as categories,
                                                     'M' as valueSuffix
                                                     from data
                                                    union all
                                                    select 
                                                    'spline' as type,
                                                    1 as yAxis,
                                                    'Poblacion' as name,
                                                    string_agg(round(valor/1000,1)::text, ',') as data,
                                                    '' as categories,
                                                    'K' as valueSuffix
                                                    from data
                            ");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );	
                    break;
                    case 'distXhidroviaswidthDistrito':
                        $pg  = new PgSql();
                        $arr   = array();
                        $obj = $pg->getRows("with data as( 
                            select 
                             iddist,
                             d.distrito,
                             ciprl,
                             canon,
                             pim,
                             valor 
                            from obs_new.gen_hidrografia l 
                            inner join distrito_f d on st_intersects(l.geom,d.geom) 
                            inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                            inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'
                            where l.id='" . $params->id ."' and departamen='" . $params->region ."' group by iddist,d.distrito,
                             ciprl,
                             canon,
                             pim,
                             valor 
                            order by iddist)
                            select 
                             'column' as type,
                             0 as yAxis,
                             'Canon' as name,
                             string_agg(round(canon/1000000,1)::text, ',') as data,
                             string_agg(distrito, ',') as categories,
                             'M' as valueSuffix
                            from data
                            union all
                            select
                            'column' as type,
                            0 as yAxis,
                            'CIPRL' as name,
                            string_agg(round(ciprl/1000000,1)::text, ',') as data,
                            '' as categories,
                            'M' as valueSuffix
                            from data
                            union all
                             select 
                            'column' as type,
                             0 as yAxis,
                             'PIM' as name,
                             string_agg(round(pim::numeric/1000000,1)::text, ',') as data,
                             '' as categories,
                             'M' as valueSuffix
                             from data
                            union all
                            select 
                            'spline' as type,
                            1 as yAxis,
                            'Poblacion' as name,
                            string_agg(round(valor/1000,1)::text, ',') as data,
                            '' as categories,
                            'K' as valueSuffix
                            from data");
                        
                        foreach ($obj as $valor){
                            array_push($arr, array(
                                "type"  => $valor->type,
                                "yAxis" => (int)$valor->yaxis,
                                "name"  => $valor->name,
                                "data"  => isNullOrEmpty(array_map('floatval', explode(',', $valor->data))),
                                "tooltip" => array(
                                    'valueSuffix' => $valor->valuesuffix
                                )
                            ));
                        }
                        
                        $data['data'] = array(
                            'categories' => explode(',', $obj{0}->categories),
                            'series'     => $arr
                        );	
                    break;
                }
                echo json_encode($data);
            }
        }
        else if(isset($_GET['q'])){
            $data   = array();
            $searchTerm = strtoupper($_GET['q']);	
            $pg = new PgSql();
            if(strlen($searchTerm)>=4){
                $data= $pg->getRows("select ubigeo as id,initcap(concat(nomdist,', ',nomprov)) as name from obs.gen_distrito where nomdist like '%" . $searchTerm . "%'");
            }
            echo json_encode($data);
        }else if(isset($_GET['term'])){
			$data   = array();
			$searchTerm = strtoupper($_GET['term']);	
			$pg = new PgSql();
			$data= $pg->getRows("select nomempresa as value,nomempresa as label from obs.gen_empresa where nomempresa like '%" . $searchTerm . "%'");
			echo json_encode($data);
		}
    }

    function dropDownList($params){
        $data   = array();
        switch($params->method){
			case 'analisisEmpresaOxi':
                $pg = new PgSql();								
                $data = $pg->getRows("with data as(  
                    select 
                       sum(top10) top10,
                       (select sum(con) from(select count(empresa) con from observatorio.gen_temporal_adjudicado group by empresa order by sum(monto) desc limit 10)x) cantidad10,
                       (select sum(d.monto) from observatorio.gen_temporal_adjudicado d)as total,
                       (select count(empresa) from observatorio.gen_temporal_adjudicado)as cantidad_proyectos,
                       (select count(*) from observatorio.gen_temporal_empresas_oxi ) cantidad_empresa
                    from (select total top10 from observatorio.gen_temporal_empresas_oxi order by total desc limit 10)x)
                    select 'Empresas'::text celda1,cantidad_empresa celda2,10 celda3,(cantidad_empresa-10) celda4 from data
                     union all
                    select 'Cantidad de Proyectos'::text,cantidad_proyectos,cantidad10,(cantidad_proyectos-cantidad10) from data
                     union all
                    select 'Inversión (Millones de Soles)'::text,total,top10,(total-top10) from data
                     union all
                    select 'Total Inversión(%)'::text,100,ROUND((top10*100/total),2),ROUND((100-(top10*100/total)),2) from data");
            break;
            case 'empresasOXIExcelGeo':
                $pg = new PgSql();								
                $data = $pg->getRows("select empresa,count(empresa) cantidad from observatorio.gen_temporal_adjudicado group by empresa order by count(empresa) desc");
            break;
			 case 'empresasOXIExcel':
                $pg = new PgSql();
                if($params->desdeLimite == 1){
                    $data = $pg->getRows("select * from observatorio.gen_temporal_empresas_oxi order by total desc
                    limit " . $params->limitFin . " offset " . $params->limitInicio . "");
                } elseif ($params->desdeLimite == 2) {
                    $data = $pg->getRows("select nombre,ano2015,ano2016,ano2017,ano2018,ano2019,ano2020,(ano2015+ano2016+ano2017+ano2018+ano2019+ano2020 ) total from (select *  from observatorio.gen_temporal_empresas_oxi)x
                    order by total desc
                    limit " . $params->limitFin . " offset " . $params->limitInicio . "");
                }
            break;
			case 'funcionXUbigeoChart':
                $pg   = new PgSql();
                $data = $pg->getRows("(select funcion,fo,pv,et,sum(fo+pv+et) as total from obs.gen_funcion_x_ubigeo where codubigeo = '" . $params->codubigeo . "' and codnivel = " . $params->codnivel . " and funcion != 'TOTAL' group by funcion,fo,pv,et order by sum(fo+pv+et) desc) limit 5");
            break;
            case 'UniversidadesAdjudicados':
                $pg = new PgSql();
                $data = $pg->getRows("select nombre_proyecto,snip,empresa,sector,to_char(fecha_buena_pro, 'DD/MM/YYYY') as fecha_buena_pro,monto_inversion,to_char(fecha_firma, 'DD/MM/YYYY') as fecha_firma,poblacion_beneficiada,estado from obs.gen_proyectos_adjudicados where coduniv=(select coduniv::text from obs.gen_universidades where universidad='" . $params->codubigeo . "')");
            break;
            case 'UniversidadesPriorizadas':
                $pg = new PgSql();
                $data = $pg->getRows("select sector,monto,nombre_proyecto,coigounico,acuerdo from obs.gen_proyectos_priorizados where universidad=(select coduniv::text from obs.gen_universidades where universidad= '" . $params->codubigeo . "')");
                break;
            case 'ciprlXunivDet':
                $pg   = new PgSql();	
                $data = $pg->getRows("select ano, canon_mef, ciprl_mef from obs.gen_universidades_det where coduniv = " . $params->coduniv . " and ano between 2014 and 2020 order by ano");
            break;
			case 'getCiprlUniversidades':
                $pg = new PgSql();
                $data = $pg->getRows("
                select coduniv,universidad,departamento,s2018,s2019,s2020,(s2018+s2019+s2020)total from (select 
                coduniv,
                universidad,
                departamento,
                (select ciprl_mef from obs.gen_universidades_det  where  ano=2017 and coduniv=x.coduniv) s2017,
                (select ciprl_mef from obs.gen_universidades_det  where  ano=2018 and coduniv=x.coduniv)s2018,
                (select ciprl_mef from obs.gen_universidades_det  where  ano=2019 and coduniv=x.coduniv)s2019,
                (select ciprl_mef from obs.gen_universidades_det  where  ano=2020 and coduniv=x.coduniv)s2020
                from obs.gen_universidades x where coduniv not in(1))x
                order by universidad");
            break;
			case 'getSnipForEmpresa':
                $pg = new PgSql();
                $data = $pg->getRows("select nombre_proyecto,snip,fecha_buena_pro,estado,monto_inversion from obs.gen_proyectos_adjudicados where UPPER(empresa) like'%" .$params->empresa. "%'");
            break;
			case 'ciprlXregion':
                $pg    = new PgSql();
                $arr   = array();
                if($params->idnivel == 1){
                    $obj   = $pg->getRows("select
                     'column' as type,
                     'Canon MEF' as name,
                     '#2f7ed8' as color,
                     (select string_agg(round(canon)::text, ',') from(select canon / 1000000 as canon from obs.gen_regiones_det where coddpto = '" . $params->coddpto . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                     (select min(ano) from obs.gen_regiones_det where coddpto = '" . $params->coddpto . "' and ano between 2014 and 2018) as scale
                    union all
                   select
                    'spline' as type,
                    'CIPRL MEF' as name,
                    '#c42525' as color,
                    (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl / 1000000)::text end as ciprl from obs.gen_regiones_det where coddpto = '" . $params->codubigeo . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                    0");
                }elseif ($params->idnivel == 2) {						
                    $obj   = $pg->getRows("select
                     'column' as type,
                     'Canon MEF' as name,
                     '#2f7ed8' as color,
                     (select string_agg(round(canon)::text, ',') from(select canon / 1000000 as canon from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                     (select min(ano) from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018) as scale
                    union all
                   select
                    'spline' as type,
                    'CIPRL MEF' as name,
                    '#c42525' as color,
                    (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl / 1000000)::text end as ciprl from obs.gen_provincias_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                    0");					
                }elseif ($params->idnivel == 3) {						
                    $obj   = $pg->getRows("select
                     'column' as type,
                     'Canon MEF' as name,
                     '#2f7ed8' as color,
                     (select string_agg(round(canon)::text, ',') from(select canon / 1000000 as canon from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                     (select min(ano) from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018) as scale
                    union all
                   select
                    'spline' as type,
                    'CIPRL MEF' as name,
                    '#c42525' as color,
                    (select string_agg(ciprl::text, ',') from(select case ciprl when 0 then 'null' else round(ciprl / 1000000)::text end as ciprl from obs.gen_distritos_det where ubigeo = '" . $params->codubigeo . "' and ano between 2014 and 2018 order by ano asc) x) as data,
                    0");					
                }
                
                foreach ($obj as $valor){
                    array_push($arr, (object)array(
                        "type"  => $valor->type,
                        "data"  => isNullOrEmpty(array_map('intval', explode(',', $valor->data)))
                    ));
                }
                
                $data = (object)array(
                    'pointStart' => (int)$obj{0}->scale,
                    'series'     => $arr
                );
            break;
            case 'funcionXUbigeoChart':
                $pg   = new PgSql();
                $data = $pg->getRows("(select funcion,fo,pv,et,sum(fo+pv+et) as total from obs.gen_funcion_x_ubigeo where codubigeo = '" . $params->codubigeo . "' and codnivel = " . $params->codnivel . " and funcion != 'TOTAL' group by funcion,fo,pv,et order by sum(fo+pv+et) desc) limit 5");
            break;
            case 'funcionXUbigeo':
                $pg   = new PgSql();
                $data = $pg->getRows("select nomfun as nomfuncion,monto_eva,eva,cod,formu as formulacion,monto_for as monto_formulacion,perfil_vi as perfil_viable,monto_per as monto_perfil,sum_tot as suma_total,mon_tot as monto_total from obs.sps_funcionxubigeo(" . $params->codnivel . ",'" . $params->codubigeo . "')");
            break;
            case 'funcionXUbigeoOtros':
                $pg   = new PgSql();
                $data = $pg->getRows("select nomfun as nomfuncion,formu as formulacion,monto_for as monto_formulacion,perfil_vi as perfil_viable,monto_per as monto_perfil,sum_tot as suma_total,mon_tot as monto_total from obs.sps_funcionxubigeootros(" . $params->codnivel . ",'" . $params->codubigeo . "')");
            break;  
            case 'quintilCiprlPoblaElect1':
                $pg   = new PgSql();
                if($params->tipo=='ciprl'){
                    $data = $pg->getRows("select  ubigeo_prov,nom_prov,ubigeo_dist,nom_dist, tope_ciprl ,valor , num_electores from obs.gen_ciprl_poblacion_electores c
                    inner join obs.gen_indicadores_dist_detalle p on c.ubigeo_distrito = p.ubigeo
                    inner join obs.gen_distrito_ciprl l on c.ubigeo_distrito =l.ubigeo
                    inner join obs.gen_elecciones_municipales e on c.ubigeo_distrito = e.ubigeo_dist
                    where p.cod_indicador='0301' and quintil_ciprl='".$params->quintil."' AND substring(ubigeo_distrito,1,2) = '".$params->region."' order by tope_ciprl desc");
                }
                else if ($params->tipo=='poblacion'){
                    $data = $pg->getRows("select  ubigeo_prov,nom_prov,ubigeo_dist,nom_dist, tope_ciprl ,valor , num_electores from obs.gen_ciprl_poblacion_electores c
                    inner join obs.gen_indicadores_dist_detalle p on c.ubigeo_distrito = p.ubigeo
                    inner join obs.gen_distrito_ciprl l on c.ubigeo_distrito =l.ubigeo
                    inner join obs.gen_elecciones_municipales e on c.ubigeo_distrito = e.ubigeo_dist
                    where p.cod_indicador='0301' and quintil_poblacion='".$params->quintil."' AND substring(ubigeo_distrito,1,2) = '".$params->region."' order by valor desc");
                }
                else if ($params->tipo=='electores'){
                    $data = $pg->getRows("
                    select  ubigeo_prov,nom_prov,ubigeo_dist,nom_dist, tope_ciprl ,valor , num_electores from obs.gen_ciprl_poblacion_electores c
                    inner join obs.gen_indicadores_dist_detalle p on c.ubigeo_distrito = p.ubigeo
                    inner join obs.gen_distrito_ciprl l on c.ubigeo_distrito =l.ubigeo
                    inner join obs.gen_elecciones_municipales e on c.ubigeo_distrito = e.ubigeo_dist
                    where p.cod_indicador='0301' and quintil_electores='".$params->quintil."' AND substring(ubigeo_distrito,1,2) = '".$params->region."' order by num_electores desc");
                }	
            break;
            case 'quintilCiprlPoblaElect':
                $pg   = new PgSql();
                if($params->tipo=='ciprl'){
                    $data = $pg->getRows("select 
                    quintil_ciprl as quintil,
                   (select count(*) from obs.gen_ciprl_poblacion_electores where quintil_ciprl=x.quintil_ciprl and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as cantidad,
                   (select sum (tope_ciprl) from obs.gen_distrito_ciprl d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_ciprl=x.quintil_ciprl and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as ciprl,
                   (select sum (valor) from obs.gen_indicadores_dist_detalle d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_ciprl=x.quintil_ciprl and substring(ubigeo_distrito,1,2) = '".$params->region."' and d.cod_indicador='0301') as poblacion,
                   (select sum (num_electores ) from obs.gen_elecciones_municipales e inner join obs.gen_ciprl_poblacion_electores c on e.ubigeo_dist = c.ubigeo_distrito where c.quintil_ciprl=x.quintil_ciprl and substring(ubigeo_distrito,1,2) = '".$params->region."') as electores
                    from obs.gen_ciprl_poblacion_electores x 
                    group by quintil
                    order by quintil");
                }
                else if ($params->tipo=='poblacion'){
                    $data = $pg->getRows(" select 
                    quintil_poblacion as quintil,
                   (select count(*) from obs.gen_ciprl_poblacion_electores where quintil_poblacion=x.quintil_poblacion and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as Cantidad,
                   (select sum (tope_ciprl) from obs.gen_distrito_ciprl d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_poblacion=x.quintil_poblacion and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as CIPRL,
                   (select sum (valor) from obs.gen_indicadores_dist_detalle d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_poblacion=x.quintil_poblacion and substring(ubigeo_distrito,1,2) = '".$params->region."' and d.cod_indicador='0301') as Poblacion,
                   (select sum (num_electores ) from obs.gen_elecciones_municipales e inner join obs.gen_ciprl_poblacion_electores c on e.ubigeo_dist = c.ubigeo_distrito where c.quintil_poblacion=x.quintil_poblacion and substring(ubigeo_distrito,1,2) = '".$params->region."') as Electores
                    from obs.gen_ciprl_poblacion_electores x 
                    group by quintil
                    order by quintil");
                }
                else if ($params->tipo=='electores'){
                    $data = $pg->getRows("select 
                    quintil_electores as quintil,
                   (select count(*) from obs.gen_ciprl_poblacion_electores where quintil_electores=x.quintil_electores and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as Cantidad,
                   (select sum (tope_ciprl) from obs.gen_distrito_ciprl d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_electores=x.quintil_electores and substring(ubigeo_distrito,1,2) = '".$params->region."' ) as CIPRL,
                   (select sum (valor) from obs.gen_indicadores_dist_detalle d inner join obs.gen_ciprl_poblacion_electores c on d.ubigeo = c.ubigeo_distrito where c.quintil_electores=x.quintil_electores and substring(ubigeo_distrito,1,2) = '".$params->region."' and d.cod_indicador='0301') as Poblacion,
                   (select sum (num_electores ) from obs.gen_elecciones_municipales e inner join obs.gen_ciprl_poblacion_electores c on e.ubigeo_dist = c.ubigeo_distrito where c.quintil_electores=x.quintil_electores and substring(ubigeo_distrito,1,2) = '".$params->region."') as Electores
                    from obs.gen_ciprl_poblacion_electores x 
                    group by quintil
                    order by quintil");
                }	
            break;
            case 'funcionEmpresasXUbigeo':
                $pg   = new PgSql();
                if($params->codnivel == 1){
                    $data = $pg->getRows("select ROW_NUMBER () OVER (ORDER BY disponibleoxi desc) as ranking,disponibleoxi::int,initcap(Concat(nomempresa,'')) as nomempresa,rucempresa from obs.gen_empresa where coddpto='" . $params->codUbigeo . "'  limit 100");				
                }elseif ($params->codnivel == 2) {
                    $data = $pg->getRows("select ROW_NUMBER () OVER (ORDER BY disponibleoxi desc) as ranking,disponibleoxi::int,initcap(Concat(nomempresa,'')) as nomempresa,rucempresa from obs.gen_empresa where coddpto=substring('". $params->codUbigeo ."' from 1 for 2) and codprov=substring('". $params->codUbigeo ."' from 3 for 2) limit 100");				
                }elseif ($params->codnivel == 3) {
                    $data = $pg->getRows("select ROW_NUMBER () OVER (ORDER BY disponibleoxi desc) as ranking,disponibleoxi::int,initcap(Concat(nomempresa,'')) as nomempresa,rucempresa from obs.gen_empresa where coddpto=substring('". $params->codUbigeo ."' from 1 for 2) and codprov=substring('". $params->codUbigeo ."' from 3 for 2)  and coddist=substring('". $params->codUbigeo ."' from 5 for 2) limit 100");				
                }
            break;
            case 'congresistasXRegion':
                $pg = new PgSql();
                $data = $pg->getRows("select concat(ape_pat,' ',nom) as  apellidos,parido,bancada,votantes from obs.gen_partidos_ubigeo where ubigeo=(select nomdpto from obs.gen_departamento where coddpto='".$params->codUbigeo."')");
            break;
            case 'pipfuncionCount':
                $pg   = new PgSql();
                if($params->idnivel == 1){	
                    $data = $pg->getRows("select count(*) as result from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto='" . $params->codubigeo . "' and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }
                else if($params->idnivel == 2){	
                    $data = $pg->getRows("select count(*) as result from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and codprov=substring('" . $params->codubigeo . "' from 3 for 2)  and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }else if($params->idnivel == 3){	
                    $data = $pg->getRows("select count(*) as result from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and coddist=substring('" . $params->codubigeo . "' from 5 for 2) and codprov=substring('" . $params->codubigeo . "' from 3 for 2)  and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }
            break;
            case 'pipFuncionSuma':
                $pg   = new PgSql();
                if($params->idnivel == 1){	
                    $data = $pg->getRows("select case when sum(montoactualizado) is null then 0 else sum(montoactualizado) end as result  from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto='" . $params->codubigeo . "' and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }
                else if($params->idnivel == 2){	
                    $data = $pg->getRows("select sum(montoactualizado) as result from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and codprov=substring('" . $params->codubigeo . "' from 3 for 2) and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }
                else if($params->idnivel == 3){	
                    $data = $pg->getRows("select sum(montoactualizado) as result from obs.gen_proyecto
                                          where codfuncionproyecto=" . $params->funcion . " and coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and coddist=substring('" . $params->codubigeo . "' from 5 for 2) and codprov=substring('" . $params->codubigeo . "' from 3 for 2)  and codnivelgobierno=" . $params->idnivGob . " and codfase=" . $params->codfase . "");
                }
            break;
            case 'pipfuncionXUbigeoOtros':
                $pg   = new PgSql();
                if($params->idnivel == 1){	
                    $data = $pg->getRows("select distinct ofq.nomfuncionproyecto as funcion, op.codfuncionproyecto as codfuncion
                                         from obs.gen_proyecto op inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                                         where op.coddpto='" . $params->codubigeo . "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4) order by op.codfuncionproyecto");
                }else if($params->idnivel == 2){	
                    $data = $pg->getRows("select distinct ofq.nomfuncionproyecto as funcion, op.codfuncionproyecto as codfuncion
                                         from obs.gen_proyecto op inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                                        where  op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2)  and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4) order by op.codfuncionproyecto");
                }else if($params->idnivel == 3){	
                    $data = $pg->getRows("select distinct ofq.nomfuncionproyecto as funcion, op.codfuncionproyecto as codfuncion
                                         from obs.gen_proyecto op inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                                        where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.coddist=substring('" . $params->codubigeo . "' from 5 for 2) and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4) order by op.codfuncionproyecto");
                }								
            break;
            case 'pipfuncionXUbigeoOtrosDet':
                $pg   = new PgSql();
                if(($params->idnivel == 1)){
                    $data = $pg->getRows("select count(subprograma) as cantidad,nomprogramaproyecto,sum(monto) as total from 
                    (select  op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto='" . $params->codubigeo . "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfuncionproyecto=" . $params->codfuncion . " and op.codfase in(2,3,4) )x
                    group by nomprogramaproyecto
                    order by sum(monto) desc");				
                }
                else if($params->idnivel == 2){					
                    $data = $pg->getRows("select count(subprograma) as cantidad,nomprogramaproyecto,sum(monto) as total from 
                    (select op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.codnivelgobierno=" . $params->idnivGob . " and op.codfuncionproyecto=" . $params->codfuncion . " and op.codfase in(2,3,4) )x
                    group by nomprogramaproyecto
                    order by sum(monto) desc");				
                }
                else if($params->idnivel == 3){					
                    $data = $pg->getRows("select count(subprograma) as cantidad,nomprogramaproyecto,sum(monto) as total from 
                    (select op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma ,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.coddist=substring('" . $params->codubigeo . "' from 5 for 2) and op.codnivelgobierno=" . $params->idnivGob . "  and op.codfuncionproyecto=" . $params->codfuncion . " and op.codfase in(2,3,4) )x
                    group by nomprogramaproyecto
                    order by sum(monto) desc");				
                }
                
            break;
            case 'pipfuncionXUbigeoOtrosDet3':
                $pg   = new PgSql();
                if(($params->idnivel == 1)){
                    $data = $pg->getRows("select count(subprograma) as cantidad,subprograma,sum(monto) as total from 
                    (select  op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto='". $params->codubigeo . "' and op.codfuncionproyecto=" . $params->codfuncion . " and pp.nomprogramaproyecto='" . $params->nomprogramaproyecto. "' and op.codnivelgobierno=" . $params->idnivGob . "  and op.codfase in(2,3,4) )x
                    group by subprograma
                    order by sum(monto) desc ");				
                }
                else if($params->idnivel == 2){					
                    $data = $pg->getRows("select count(subprograma) as cantidad,subprograma,sum(monto) as total from 
                    (select  op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.codfuncionproyecto=" . $params->codfuncion . " and pp.nomprogramaproyecto='" . $params->nomprogramaproyecto. "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4))x
                    group by subprograma
                    order by sum(monto) desc ");
                }
                else if($params->idnivel == 3){					
                    $data = $pg->getRows("select count(subprograma) as cantidad,subprograma,sum(monto) as total from 
                    (select  op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma,pp.nomprogramaproyecto
                    from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_programaproyecto pp on op.codprogramaproyecto = pp.codprogramaproyecto
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and  op.coddist=substring('" . $params->codubigeo . "' from 5 for 2) and op.codfuncionproyecto=" . $params->codfuncion . " and pp.nomprogramaproyecto='" . $params->nomprogramaproyecto. "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4))x
                    group by subprograma
                    order by sum(monto) desc ");}
                
               break;
            case 'pipfuncionXUbigeoOtrosDet4':
                $pg   = new PgSql();
                if(($params->idnivel == 1)){
                    $data = $pg->getRows("select 
                    op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto='" . $params->codubigeo . "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfuncionproyecto=" . $params->codfuncion . " and ogp.nomsubprograma='" . $params->subprograma . "' and op.codfase in(2,3,4)");				
                }
                else if($params->idnivel == 2){					
                    $data = $pg->getRows("select 
                    op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and op.codfuncionproyecto=" . $params->codfuncion . " and ogp.nomsubprograma='" . $params->subprograma . "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4)");				
                }
                else if($params->idnivel == 3){					
                    $data = $pg->getRows("select 
                    op.codigounico as codunico,op.montoactualizado as monto,otp.nomtipoproyecto as tipo,ofp.nomfase as funcion,fecregistro as programas,ogp.nomsubprograma as subprograma from obs.gen_proyecto op 
                    inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto
                    inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
                    inner join obs.gen_subprogramaproyecto ogp on op.codsubprogramaproyecto=ogp.codsubprogramaproyecto
                    inner join obs.gen_tipoproyecto  otp on op.codtipoproyecto=otp.codtipoproyecto
                    where op.coddpto=substring('" . $params->codubigeo . "' from 1 for 2) and op.codprov=substring('" . $params->codubigeo . "' from 3 for 2) and  op.coddist=substring('" . $params->codubigeo . "' from 5 for 2) and op.codfuncionproyecto=" . $params->codfuncion . " and ogp.nomsubprograma='" . $params->subprograma . "' and op.codnivelgobierno=" . $params->idnivGob . " and op.codfase in(2,3,4)");
                }
                
               break;
            case 'proyPriorizados':
                $pg = new PgSql();
                $data = $pg->getRows("select sector,monto,nombre_proyecto,coigounico,acuerdo from obs.gen_proyectos_priorizados where coigounico not in (select snip from obs.gen_proyectos_adjudicados where ubigeo= '" . $params->codubigeo . "') and ubigeo= '" . $params->codubigeo . "'");
            break;	
            case 'proyAdjudicados':
                $pg = new PgSql();
                $data = $pg->getRows("select nombre_proyecto,snip,empresa,sector,to_char(fecha_buena_pro, 'DD/MM/YYYY') as fecha_buena_pro,monto_inversion,to_char(fecha_firma, 'DD/MM/YYYY') as fecha_firma,poblacion_beneficiada,estado from obs.gen_proyectos_adjudicados where codubigeo = '" . $params->codubigeo . "'");
            break;
            case 'indCabXubigeo':
                $pg   = new PgSql();						
                $data = $pg->getRows("select * from obs.fn_ficha_indicador_cabecera(" . $params->idNivel . ", '" . $params->codUbigeo . "')");
            break;
            case 'indDetXubigeo':
                $pg   = new PgSql();
                $data = $pg->getRows("select * from obs.fn_ficha_indicador_detalle(" . $params->idNivel . ", '" . $params->codUbigeo . "', '" . $params->idInd . "')");
            break;
            case 'chartciprliformacion':
                $pg    = new PgSql();
                if($params->tipo=='CIPRL'){
                    $data   = $pg->getRows("with data as(
                        select 
                        coddpto ubigeo,
                        departamento,
                        ciprl ciprl2018,
                       (select ciprl from obs.gen_regiones_det where ano in (2019) and coddpto= x.coddpto) ciprl2019,
                       (select ciprl from obs.gen_regiones_det where ano in (2020) and coddpto= x.coddpto) ciprl2020 
                      from obs.gen_regiones_det x 
                      where ano in (2018))
                      select 
                        ubigeo,departamento,ciprl2018 s2018,ciprl2019 s2019,ciprl2020 s2020,(ciprl2018+ciprl2019+ciprl2020)total
                       from data order by departamento" );
                }else{
                    $data   = $pg->getRows("with data as(
                        select 
                            coddpto ubigeo,
                            departamento,
                            canon canon2018,
                            (select canon from obs.gen_regiones_det where ano in (2019) and coddpto= x.coddpto) canon2019,
                            (select canon from obs.gen_regiones_det where ano in (2020) and coddpto= x.coddpto) canon2020
                            from obs.gen_regiones_det x 
                            where ano in (2018))
                            select 
                                ubigeo,departamento,canon2018 s2018,canon2019 s2019,canon2020 s2020,(canon2018+canon2019+canon2020) total
                            from data order by departamento" ); 
                }
               
            break;  
			case 'chartciprliformacionSalud':
                $pg    = new PgSql();
                if($params->tipo=='Minsa'){
                    $data   = $pg->getRows("select			    
                    x.ubigeo,
                    x.nombdep,
                    poblacion,
                    x.fallecidos,
                    (fallecidos /  poblacion) * 1000 as fallecidos_poblacion_1000
                   from(
                   select
                    d.nombdep as nombdep,
                    d.first_iddp  as ubigeo,
                    (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,2) = d.first_iddp and LENGTH(ubigeo)=2) as fallecidos,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = d.first_iddp and cod_indicador = '0301') as poblacion
                   from departamento d
                   where
                    d.first_iddp not in ('40', '26') 
                   group by
                    d.nombdep,
                    d.first_iddp
                   order by
                    1
                   ) x order by nombdep" );
                }else{
                    $data   = $pg->getRows("select 
                    ubigeo,
                    nombdep,
					poblacion,
                    fallecidos2017,
                    fallecidos2018,
                    fallecidos2019,
                    fallecidos2020,
                    (fallecidos2017 /  poblacion) * 1000 as fapob_2017,
                    (fallecidos2018 /  poblacion) * 1000 as fapob_2018,
                    (fallecidos2019 /  poblacion) * 1000 as fapob_2019,
                    (fallecidos2020 /  poblacion) * 1000 as fapob_2020,
                    (fallecidos2017+fallecidos2018+fallecidos2019+fallecidos2020) total,
                    (((fallecidos2017 /  poblacion) * 1000)+((fallecidos2018 /  poblacion) * 1000)+((fallecidos2019 /  poblacion) * 1000)+((fallecidos2020 /  poblacion) * 1000)) total_pob
                    from (
                   select  
                   d.first_iddp ubigeo,
                   d.nombdep as nombdep,
                   (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2017,
                   (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2018,
                   (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2019,
                   (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = d.first_iddp) as fallecidos2020,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = d.first_iddp and cod_indicador = '0301') as poblacion
                   from departamento d
                   where
                    d.first_iddp not in ('40', '26') )x order by nombdep
                   " ); 
                }
               
            break;  
            case 'chartciprliformacionProvincia':
                $pg    = new PgSql();
                if($params->tipo=='CIPRL'){
                    $data   = $pg->getRows(" with data as(
                        select 
                          ubigeo,
                          nombprov,
                          ciprl ciprl2018,
                          (select ciprl from obs.gen_provincias_det d1  where ano=2019 and ubigeo=d.ubigeo) ciprl2019,
                          (select ciprl from obs.gen_provincias_det d1  where ano=2020 and ubigeo=d.ubigeo) ciprl2020
                         from obs.gen_provincias_det d  inner join provincia p on d.ubigeo=p.first_idpr where ano=2018 and SUBSTRING (p.first_idpr, 1, 2)='" . $params->departamento . "')
                         select  ubigeo,nombprov,ciprl2018 s2018,ciprl2019 s2019,ciprl2020 s2020,(ciprl2018+ciprl2019+ciprl2020) total from data order by nombprov" );
                }else{
                    $data   = $pg->getRows(" with data as(
                        select 
                          ubigeo,
                          nombprov,
                          canon canon2018,
                          (select canon from obs.gen_provincias_det d1  where ano=2019 and ubigeo=d.ubigeo) canon2019,
                          (select canon from obs.gen_provincias_det d1  where ano=2020 and ubigeo=d.ubigeo) canon2020
                         from obs.gen_provincias_det d  inner join provincia p on d.ubigeo=p.first_idpr where ano=2018 and SUBSTRING (p.first_idpr, 1, 2)='" . $params->departamento . "')
                         select  ubigeo,nombprov,canon2018 s2018,canon2019 s2019,canon2020 s2020,(canon2018+canon2019+canon2020) total
                          from data order by nombprov"); 
                }
                
            break;
			 case 'chartciprliformacionProvinciaMinsa':
                $pg    = new PgSql();
                if($params->tipo=='MINSA'){
                    $data   = $pg->getRows("with data as(
                        select
                         x.ubigeo,
                         substr( x.ubigeo,1,2) as dpto,
                         x.nombprov,
                         x.poblacion,
                         x.fallecidos,
                         (fallecidos /  poblacion) * 1000 as fallecidos_poblacion_1000
                        from(
                        select
                         p.nombprov as nombprov,
                         p.first_idpr as ubigeo, 
                         (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,4) = p.first_idpr and LENGTH(ubigeo)=4) as fallecidos,
                         (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov = p.first_idpr and cod_indicador = '0301') as poblacion
                        from provincia p
                        where
                         (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,4) = p.first_idpr) > 0 
                        group by
                         p.nombprov,
                         p.first_idpr
                        order by
                         1
                        ) x )
                        select * from data where dpto='".$params->departamento."' order by nombprov");
                }else{
                    $data   = $pg->getRows("select 
                    ubigeo,
                    nombprov,
                    fallecidos2017,
                    fallecidos2018,
                    fallecidos2019,
                    fallecidos2020,
					poblacion,
                    (fallecidos2017 /  poblacion) * 1000 as fapob_2017,
                    (fallecidos2018 /  poblacion) * 1000 as fapob_2018,
                    (fallecidos2019 /  poblacion) * 1000 as fapob_2019,
                    (fallecidos2020 /  poblacion) * 1000 as fapob_2020,
                    (fallecidos2017+fallecidos2018+fallecidos2019+fallecidos2020) total,
                    (((fallecidos2017 /  poblacion) * 1000)+((fallecidos2018 /  poblacion) * 1000)+((fallecidos2019 /  poblacion) * 1000)+((fallecidos2020 /  poblacion) * 1000)) total_pob
                   from(
                   select 
                     first_idpr ubigeo,
                     nombprov,
                    (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2017,
                    (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2018,
                    (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2019,
                    (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.first_idpr) as fallecidos2020,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov = p.first_idpr and cod_indicador = '0301') as poblacion
                    from provincia p
                   where  substr(first_idpr,1,2)='".$params->departamento."')x order by nombprov");
                }
                
            break;    
            case 'chartciprliformacionDistrito':
                $pg    = new PgSql();
                if($params->tipo=='CIPRL'){
                    $data   = $pg->getRows(" with data as(   
                        select 
                         ubigeo,
                         distrito,
                         ciprl ciprl2018,
                        (select ciprl from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2019) ciprl2019,
                        (select ciprl from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2020) ciprl2020
                        
                       from obs.gen_distritos_det d inner join distrito_f f on d.ubigeo=f.iddist where ano=2018 and idprov='" . $params->provincia . "' and iddpto='" . $params->region . "')
                       select ubigeo,distrito,ciprl2018 s2018,ciprl2019 s2019,ciprl2020 s2020,(ciprl2018+ciprl2019+ciprl2020) total from data order by distrito" );
                }else{
                    $data   = $pg->getRows(" with data as(   
                        select 
                         ubigeo,
                         distrito,
                         canon canon2018,
                        (select canon from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2019) canon2019,
                        (select canon from obs.gen_distritos_det d1 where d1.ubigeo=d.ubigeo and ano=2020) canon2020
                       from obs.gen_distritos_det d inner join distrito_f f on d.ubigeo=f.iddist where ano=2018 and idprov='" . $params->provincia . "' and iddpto='" . $params->region . "')
                       select ubigeo,distrito,canon2018 s2018,canon2019 s2019,canon2020 s2020,(canon2018+canon2019+canon2020) total
                       from data order by distrito"); 
                }
            break;    
			case 'chartciprliformacionDistritoMinsa':
                $pg    = new PgSql();
                if($params->tipo=='MINSA'){
                    $data  = $pg->getRows("select
                    x.ubigeo,
                    x.nombprov,
                    x.nombdist,
                    x.poblacion,
                    x.fallecidos,
                    (fallecidos /  poblacion) * 1000 as fallecidos_poblacion_1000
                   from(
                   select
                    d.departamen,
                    d.provincia as nombprov,
                    d.distrito as nombdist,
                    d.iddist as ubigeo,
                    (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,6) = d.iddist) as fallecidos,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov||coddist = d.iddist and cod_indicador = '0301') as poblacion
                   from distrito_f d
                   where
                    (select sum(fallecidos) from obs.gen_fallecidos_minsa where substr(ubigeo,1,6) = d.iddist) > 0
                   group by
                    d.departamen,
                    d.provincia,
                    d.distrito,
                    d.iddist
                   order by
                    1
                   ) x where substr(x.ubigeo,1,4)='" . $params->provincia . "' AND  substr(x.ubigeo,1,2)='" . $params->region . "' order by nombdist
                   ");
                }else{
                    $data  = $pg->getRows("select 
                    ubigeo,
                     distrito,
                     fallecidos2017,
                     fallecidos2018,
                     fallecidos2019,
                     fallecidos2020,
					 poblacion,
                     (fallecidos2017 /  poblacion) * 1000 as fapob_2017,
                     (fallecidos2018 /  poblacion) * 1000 as fapob_2018,
                     (fallecidos2019 /  poblacion) * 1000 as fapob_2019,
                     (fallecidos2020 /  poblacion) * 1000 as fapob_2020,
                     (fallecidos2017+fallecidos2018+fallecidos2019+fallecidos2020) total,
                     (((fallecidos2017 /  poblacion) * 1000)+((fallecidos2018 /  poblacion) * 1000)+((fallecidos2019 /  poblacion) * 1000)+((fallecidos2020 /  poblacion) * 1000)) total_pob
                    
                     from(
                    select 
                    iddist ubigeo,
                    distrito,
                    (select fallecidos_2017 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2017,
                     (select fallecidos_2018 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2018,
                     (select fallecidos_2019 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2019,
                     (select fallecidos_2020 from obs.gen_fallecidos_sinadef_anio where ubigeo = p.iddist) as fallecidos2020,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto||codprov||coddist = p.iddist and cod_indicador = '0301') as poblacion
                    from distrito_f p
                    where p.iddpto='" . $params->region . "' and idprov='" . $params->provincia . "')x order by distrito"); 
                }
                
            break; 
             case 'SludxMesesMinsa':
                $pg    = new PgSql();
                if($params->tipo==2){
                    $data  = $pg->getRows("select
                    CASE 
                     WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                     WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                     WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                     WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                     WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                     WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
					 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
					 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'
                    END mes,
                    count(*) fallecidos,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = '" . $params->departamento . "' and cod_indicador = '0301') as poblacion
                   from obs.tmp_fallecidos_minsa 
                   where departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') 
                   group by substr(fecha_fallecimiento,5,2) 
                   order by substr(fecha_fallecimiento,5,2) asc
                   ");
                }else if($params->tipo==4){
                    $data  = $pg->getRows("select
                    CASE 
                     WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                     WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                     WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                     WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                     WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                     WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
					 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
					 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'
                    END mes,
                    (select sum(valor) from obs.gen_indicadores_prov_detalle where ubigeo = '" . $params->provincia . "' and cod_indicador = '0301') as poblacion,
                    count(*) fallecidos
                   from obs.tmp_fallecidos_minsa 
                   where 
                    departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                    provincia=(select nombprov from provincia where first_idpr ='" . $params->provincia . "' limit 1) 
                   group by substr(fecha_fallecimiento,5,2) 
                   order by substr(fecha_fallecimiento,5,2) asc
                   ");
                }else if($params->tipo==6){
                    $data  = $pg->getRows("select
                    CASE 
                     WHEN substr(fecha_fallecimiento,5,2) ='03' THEN 'Marzo'
                     WHEN substr(fecha_fallecimiento,5,2) ='04' THEN 'Abril'
                     WHEN substr(fecha_fallecimiento,5,2) ='05' THEN 'Mayo'
                     WHEN substr(fecha_fallecimiento,5,2) ='06' THEN 'Junio'
                     WHEN substr(fecha_fallecimiento,5,2) ='07' THEN 'Julio'
                     WHEN substr(fecha_fallecimiento,5,2) ='08' THEN 'Agosto'
					 WHEN substr(fecha_fallecimiento,5,2) ='09' THEN 'Setiembre'
					 WHEN substr(fecha_fallecimiento,5,2) ='10' THEN 'Octubre'
                    END mes,
                    (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = '" . $params->distrito . "' and cod_indicador = '0301') as poblacion,
                    count(*) fallecidos
                   from obs.tmp_fallecidos_minsa 
                   where 
                    departamento =(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                    provincia=(select nombprov from provincia where first_idpr ='" . $params->provincia . "' limit 1) and
                    distrito=(select distrito from distrito_f where iddist ='" . $params->distrito . "'  limit 1 )
                   group by substr(fecha_fallecimiento,5,2) 
                   order by substr(fecha_fallecimiento,5,2) asc
                   ");
                }
                
            break;   
			case 'SludxMesesSinadef':
                $pg    = new PgSql();
                if($params->tipo==2){
                    $data  = $pg->getRows("with data as( 
                        select 
                         count(*) s2017,
                         (select * from obs.fn_nombre_mes(mes)) meses,
                         (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = '" . $params->departamento . "' and cod_indicador = '0301') as poblacion,
                         mes,
                         (select count(*) from obs.tmp_fallecidos_sinadef where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and mes=x.mes and anio=2018  ) s2018,
                         (select count(*) from obs.tmp_fallecidos_sinadef where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and mes=x.mes and anio=2019  ) s2019,
                         (select count(*) from obs.tmp_fallecidos_sinadef where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and mes=x.mes and anio=2020  ) s2020
                        from obs.tmp_fallecidos_sinadef x
                        where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and anio=2017
                        group by mes
                        order by mes)
                        select mes,meses,s2017,s2018,s2019,s2020,(s2017+s2018+s2019+s2020) total,((s2017/poblacion)*1000) sp2017,
                        ((s2018/poblacion)*1000) sp2018,((s2019/poblacion)*1000) sp2019,((s2020/poblacion)*1000) sp2020,
                        (((s2017+s2018+s2019+s2020)/poblacion)*1000)stp
                         from data order by mes");
                }else if($params->tipo==4){
                    $data  = $pg->getRows("with data as( 
                        select 
                         count(*) s2017,
                         (select * from obs.fn_nombre_mes(mes)) meses,
                         (select sum(valor) from obs.gen_indicadores_dist_detalle where coddpto = '" . $params->departamento . "' and cod_indicador = '0301') as poblacion,
                         mes,
                         (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') 
                           and mes=x.mes and anio=2018  ) s2018,
                         (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') 
                           and mes=x.mes and anio=2019  ) s2019,
                           (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') 
                           and mes=x.mes and anio=2020  ) s2020
                        from obs.tmp_fallecidos_sinadef x
                        where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and anio=2017 and 
                         provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') 
                        group by mes
                        order by mes)
                        select mes,meses,s2017,s2018,s2019,s2020,(s2017+s2018+s2019+s2020) total,((s2017/poblacion)*1000) sp2017,
                        ((s2018/poblacion)*1000) sp2018,((s2019/poblacion)*1000) sp2019,((s2020/poblacion)*1000) sp2020,
                        (((s2017+s2018+s2019+s2020)/poblacion)*1000)stp
                         from data order by mes");
                }else if($params->tipo==6){
                    $data  = $pg->getRows("with data as( 
                        select 
                         count(*) s2017,
                         (select * from obs.fn_nombre_mes(mes)) meses,
                         (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = '" . $params->distrito . "' and cod_indicador = '0301') as poblacion,
                         mes,
                         (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') and
                           distrito_domicilio=(select distrito from distrito_f where iddpto='" . $params->departamento . "' and idprov='" . $params->provincia . "' and iddist='" . $params->distrito . "')
                           and mes=x.mes and anio=2018  ) s2018,
                         (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') and
                           distrito_domicilio=(select distrito from distrito_f where iddpto='" . $params->departamento . "' and idprov='" . $params->provincia . "' and iddist='" . $params->distrito . "')
                           and mes=x.mes and anio=2019  ) s2019,
                           (select count(*) from obs.tmp_fallecidos_sinadef 
                          where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and
                           provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') and
                           distrito_domicilio=(select distrito from distrito_f where iddpto='" . $params->departamento . "' and idprov='" . $params->provincia . "' and iddist='" . $params->distrito . "')
                           and mes=x.mes and anio=2020  ) s2020
                        from obs.tmp_fallecidos_sinadef x
                        where departamento_domicilio=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and anio=2017 and 
                         provincia_domicilio=(select nombprov from provincia where first_nomb=(select nombdep from departamento where first_iddp='" . $params->departamento . "') and first_idpr='" . $params->provincia . "') and
                         distrito_domicilio=(select distrito from distrito_f where iddpto='" . $params->departamento . "' and idprov='" . $params->provincia . "' and iddist='" . $params->distrito . "')
                        group by mes
                        order by mes)
                        select mes,meses,s2017,s2018,s2019,s2020,(s2017+s2018+s2019+s2020) total,((s2017/poblacion)*1000) sp2017,
                        ((s2018/poblacion)*1000) sp2018,((s2019/poblacion)*1000) sp2019,((s2020/poblacion)*1000) sp2020,
                        (((s2017+s2018+s2019+s2020)/poblacion)*1000)stp
                         from data order by mes");
                }
                
            break;   
		    case 'tituloSalud':
                $pg   = new PgSql();
                if($params->tipo == 2){
                    $data = $pg->getRows("select concat('Región ',nombdep) titulo from departamento  where first_iddp='" . $params->departamento . "'");				
                }elseif ($params->tipo == 4) {
                    $data = $pg->getRows("select concat('Región ',first_nomb,', Provincia ',nombprov) titulo from provincia where first_idpr='" . $params->provincia . "'");				
                }elseif ($params->tipo == 6) {
                    $data = $pg->getRows("select concat('Región ',departamen,', Provincia ',provincia,', Distrito de ',distrito) titulo from distrito_f where iddist='" . $params->distrito . "'");				
                }	
            break;
			case 'DatosXubigeo':
                $pg   = new PgSql();
                if($params->codnivel == 1){
                    $data = $pg->getRows("select * from obs.gen_ficha_cabecera f
                    inner join obs.gen_elecciones_municipales m on m.ubigeo_dpto = f.codubigeo
                    where codnivel = " . $params->codnivel . "  and m.ubigeo_dpto = '" . $params->codUbigeo . "' and m.ubigeo_prov is null");				
                }elseif ($params->codnivel == 2) {
                    $data = $pg->getRows("select * from obs.gen_ficha_cabecera f
                    inner join obs.gen_elecciones_municipales m on m.ubigeo_prov = f.codubigeo
                    where codnivel = " . $params->codnivel . " and m.ubigeo_prov = '" . $params->codUbigeo . "' and m.ubigeo_dist is null");				
                }elseif ($params->codnivel == 3) {
                    $data = $pg->getRows("select * from obs.gen_ficha_cabecera f
                    inner join obs.gen_elecciones_municipales m on m.ubigeo_dist = f.codubigeo
                    where codnivel = " . $params->codnivel . "  and m.ubigeo_dist = '" . $params->codUbigeo . "'");				
                }			
            break;
            case 'ciprlXregionDet':
                $pg   = new PgSql();	
                if($params->idnivel == 1){
                    $data = $pg->getRows("select x.ano, x.canon, x.ciprl,x.pim, y.cumple_r1, y.cumple_r2 from obs.gen_regiones_det x left join obs.gen_reglas_fiscales y on y.codubigeo = RPAD(x.coddpto, 6, '0') and y.anio = x.ano where coddpto = '" . $params->codubigeo . "' order by ano asc");				
                }elseif ($params->idnivel == 2) {
                    $data = $pg->getRows("select x.ano, x.canon, x.ciprl,x.pim, y.cumple_r1, y.cumple_r2 from obs.gen_provincias_det x left join obs.gen_reglas_fiscales y on y.codubigeo = RPAD(x.ubigeo, 6, '01') and y.anio = x.ano where ubigeo = '" . $params->codubigeo . "' order by ano asc");				
                }elseif ($params->idnivel == 3) {
                    $data = $pg->getRows("select x.ano, x.canon, x.ciprl,x.pim, y.cumple_r1, y.cumple_r2 from obs.gen_distritos_det x left join obs.gen_reglas_fiscales y on y.codubigeo = x.ubigeo and y.anio = x.ano where ubigeo = '" . $params->codubigeo . "' order by ano asc");				
                }
            break;
            case 'reglasFiscales':
                $pg    = new PgSql();
                if($params->codnivel == 1 || $params->codnivel == 3){
                    $data = $pg->getRows("select * from obs.gen_reglas_fiscales where codubigeo = '" . str_pad($params->codubigeo,6,"0") . "' order by anio asc");
                } elseif ($params->codnivel == 2) {
                    $data = $pg->getRows("select * from obs.gen_reglas_fiscales where codubigeo = '" . str_pad($params->codubigeo,6,"01") . "' order by anio asc");
                }
            break;			
            case 'counterSum':
                $pg   = new PgSql();
			    $sql  = "select  count(*) as contador,sum(montoactualizado) / 1000000 as total from observatorio.gen_proyectos where ";
			    $data = $pg->getRows($sql . ' ' . $params->sql_filter);
            break;
            case 'coddpto':
                $pg   = new PgSql();
                $data = $pg->getRows("SELECT codigo, valor from obs.fn_filtros_list('coddpto','','') where codigo <> '99'");	
            break;
            case 'provincias':
                $pg   = new PgSql();			
                $data = $pg->getRows("select * from obs.gen_dpto_prov ");
            break;
            case 'codfase':
                $pg   = new PgSql();
                $data = $pg->getRows("SELECT codfase::text as codigo,  nomfase::text as valor, glosafase FROM obs.gen_faseproyecto where flag = 1 order by valor");
            break;
            case 'codnivelgobierno':
                $pg   = new PgSql();
                $data = $pg->getRows("SELECT codnivelgobierno::text as codigo,  nomnivelgobierno::text as valor, glosanivelgobierno FROM obs.gen_nivelgobierno order by 2");			
            break;
            case 'codfuncionproyecto':
                $pg   = new PgSql();
                $data = $pg->getRows("select f.codfuncionproyecto as codigo,f.nomfuncionproyecto as valor from obs.gen_proyecto p inner join obs.gen_funcionproyecto f on p.codfuncionproyecto=f.codfuncionproyecto group by f.codfuncionproyecto,f.nomfuncionproyecto order by f.nomfuncionproyecto asc");			
            break;
            case 'ProgramaXfuncion'://OBSERVACION POR TEMA DE MEJORA se implemewnto el not in por sobre carga de la consulta de proyectos 
                $pg = new PgSql();
                $data = $pg->getRows("select
                        x.nomfuncionproyecto as nomfuncion,
                        string_agg(y.codprogramaproyecto::text, ',') as codprograma,
                        string_agg(y.nomprogramaproyecto::text, ',') as nomprograma 
                    from obs.gen_funcionproyecto x
                    inner join obs.gen_programaproyecto y on y.codfuncionproyecto = x.codfuncionproyecto
                    where x.codfuncionproyecto NOT IN (1,2,12,25)
                    group by x.nomfuncionproyecto
                    order by 1");
            break;
            case 'SubProgramaXfuncion':
                $pg = new PgSql();
                $data = $pg->getRows("select
                        x.nomprogramaproyecto as nomprogram,
                        string_agg(y.codsubprogramaproyecto::text, ',') as codsubprograma,
                        string_agg(y.nomsubprograma::text, ',') as nomsubprograma 
                    from obs.gen_programaproyecto x
                    inner join obs.gen_subprogramaproyecto y on y.codprogramaproyecto = x.codprogramaproyecto
                    group by x.nomprogramaproyecto
                    order by 1");
            break;
            case 'minMaxProjects':
                $pg   = new PgSql();
                $data = $pg->getRows("select
                 min(tope_cipril) as min_tope_cipril,
                 round(max(tope_cipril) / 1000000) as max_tope_cipril,
                 min(monto) as min_monto,
                 round(max(monto) / 1000000) as max_monto,
                 (select CEIL(max(valor) / 1000) from obs.gen_indicadores_region_detalle where cod_indicador = '0301') as max_poblacion
                FROM obs.gen_proyecto pr
                JOIN obs.gen_funcionproyecto fg ON fg.codfuncionproyecto = pr.codfuncionproyecto
                JOIN obs.gen_origenproyecto op ON op.codorigen = pr.codorigen
                JOIN obs.gen_departamento dp ON dp.coddpto = pr.coddpto
                JOIN obs.gen_faseproyecto fp ON fp.codfase = pr.codfase
                JOIN obs.gen_nivelgobierno ng ON ng.codnivelgobierno = pr.codnivelgobierno
                JOIN obs.gen_subprogramaproyecto pp ON pp.codprogramaproyecto = pr.codprogramaproyecto AND pp.codsubprogramaproyecto = pr.codsubprogramaproyecto");
            break;
            case 'getTablaUser':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                    concat(p.ape_pat,' ',ape_mat,' ',nombres) as apellidos,
                    p.gid as id,
                    p.empresa,
                    r.descripcion,
                    p.correo,
                    u.idestado
                  from  observatorio.gen_usuarios u 
                  inner join observatorio.gen_personas p on p.gid=u.idpersona
                  inner join observatorio.gen_roles r on p.id_rol=r.gid
                  order by concat(p.ape_pat,' ',ape_mat,' ',nombres) asc");	
            break;
            case 'usercredentials':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                    p.ape_pat,
                    p.ape_mat,
                    p.nombres,
                    p.empresa,
                    r.descripcion,
                    p.correo,
                    u.pwd,
                    p.nro_documento
                  from  observatorio.gen_usuarios u 
                  inner join observatorio.gen_personas p on p.gid=u.idpersona
                  inner join observatorio.gen_roles r on p.id_rol=r.gid
                  where p.gid=".$params->id."");	
            break;
            case 'cantidadFiltroUbigeoExcel':
                $pg   = new PgSql();
                $data = $pg->getRows("select
                (select count(*)
                from observatorio.gen_distrito_geojson geo
                inner join obs.gen_distritos_det det on geo.codubigeo=det.ubigeo and ano=2019
                inner join obs.gen_indicadores_dist_detalle ind on ind.ubigeo = geo.codubigeo and ind.cod_indicador = '0301' where ".$params->distrito.") as distrito,
                (select count(*)
                from observatorio.gen_dpto_geojson co 
                inner join obs.gen_regiones_det dp on dp.coddpto=co.codubigeo and ano=2019
                inner join obs_new.gen_indicadores_region_detalle ind on ind.coddpto =  co.codubigeo and ind.cod_indicador = '0301' where ".$params->region.") as dpto,
                (select count(*) from observatorio.gen_provincia_geojson geo
                inner join obs.gen_provincias_det det on det.ubigeo=geo.codubigeo and ano=2019
                inner join obs.gen_indicadores_prov_detalle ind on ind.ubigeo = geo.codubigeo and ind.cod_indicador = '0301' where ".$params->provincia.") as provincia");
            break;
            case 'oleoducto':
                $pg   = new PgSql();
                $data = $pg->getRows("select
                 ROW_NUMBER () OVER (ORDER BY ramal),
                 x.ramal,
                 x.tramo,      
                 (select count(*) from regexp_split_to_table(x.distritos, ',')) as ndistritos,
                 x.distritos,
                 x.distritos_in,
                 (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and cod_indicador = '0301') as poblacion,
                 (select sum(canon) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as canon,
                 (select sum(ciprl) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as ciprl, 
                 (select sum(pim) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as pia			 
                from(
                select
                 x.ramal,
                 x.tramo,     
                 (select string_agg(x.iddist, ',') from (select z.iddist from obs.gen_oleoducto y inner join distrito z on st_intersects(z.geom, y.geom) where tramo = x.tramo group by z.gid) x) as distritos,
                 (select string_agg(x.iddist, ''',''') from (select z.iddist from obs.gen_oleoducto y inner join distrito z on st_intersects(z.geom, y.geom) where tramo = x.tramo group by z.gid) x) as distritos_in
                from obs.gen_oleoducto x
                group by
                 x.ramal, 
                 x.tramo			 
                ) x where x.ramal not in ('Sin información')");
            break;
            case 'distXoleoducto':
                $pg   = new PgSql();
                $data = $pg->getRows("select
                 dis.iddist	as id, 			 
                 dis.nombdist as distrito,
                 ind.valor as poblacion,
                 disd.canon,
                 disd.ciprl,
                 dpia.pia,
                 aut.partido,
                 aut.electores
                from distrito dis            
                left join obs.gen_autoridades_distritales aut on aut.ubigeo = dis.iddist
                inner join obs.gen_indicadores_dist_detalle ind on ind.ubigeo = dis.iddist and ind.cod_indicador = '0301'
                left join obs.gen_distritos_det disd on disd.ubigeo = dis.iddist and disd.ano = 2018
                left join obs.gen_distrito_pia dpia on dpia.ubigeo = dis.iddist and dpia.ano = 2019
                where
                 dis.iddist in(" . $params->distritos_in . ")             
                order by
                 COALESCE(disd.ciprl, 0) desc");
            break;
            case 'pipsObjetosFuncion':
                $pg = new PgSql();								
                $data = $pg->getRows("select 
                codfuncion as _codfuncion,
                funcion as _funcion,
               (select 
                 sum(montoactualizado) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=x.codfuncion)::numeric as _sumformulacion,
               (select 
                 count(1) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=x.codfuncion)::numeric as _canformulacion,       
               (select 
                 sum(montoactualizado) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=x.codfuncion)::numeric as _sumevaluacion,
               (select 
                 count(1) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=x.codfuncion)::numeric as _canevaluacion,
               (select 
                 sum(montoactualizado) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=x.codfuncion)::numeric as _sumperfil,
               (select 
                 count(1) 
                from obs.gen_proyecto op 
                inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
                where concat(coddpto,codprov,coddist) in ".$params->distritos."
                      and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=x.codfuncion)::numeric as _canperfil  
            
              from (
               select op.codfuncionproyecto as codfuncion ,ofq.nomfuncionproyecto as funcion 
               from obs.gen_proyecto op 
               inner join obs.gen_funcionproyecto ofq on op.codfuncionproyecto=ofq.codfuncionproyecto 
               where concat(coddpto,codprov,coddist) in ".$params->distritos."
                     and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2,3,4)  
               group by op.codfuncionproyecto,ofq.nomfuncionproyecto 
               order by ofq.nomfuncionproyecto) x");
            break;
            case 'pipsObjetosPrograma':
                $pg = new PgSql();								
                $data = $pg->getRows("select 
                codprogramaproyecto::integer as _codfuncion, 
                nomprogramaproyecto::text as _funcion,
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto 
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _sumformulacion,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _canformulacion,
                       
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _sumevaluacion,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _canevaluacion,
               
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _sumperfil,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=x.codprogramaproyecto)::numeric as _canperfil
               from
               (select 
                nomprogramaproyecto,
                pp.codprogramaproyecto
               from obs.gen_proyecto op 
               inner join obs.gen_programaproyecto pp on op.codprogramaproyecto=pp.codprogramaproyecto
               where concat(coddpto,codprov,coddist) in ".$params->distritos."
                     and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2,3,4) and op.codfuncionproyecto=".$params->funcion."
               group by nomprogramaproyecto,pp.codprogramaproyecto
               order by nomprogramaproyecto desc) x");
            break;
            case 'pipsObjetossubPrograma':
                $pg = new PgSql();								
                $data = $pg->getRows("select 
                codsubprogramaproyecto as _codfuncion,
                nomsubprograma as _funcion,
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _sumformulacion,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _canformulacion,
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _sumevaluacion,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(3) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _canevaluacion,
                (select 
                  sum(montoactualizado)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _sumperfil,
                (select 
                  count(1)
                 from obs.gen_proyecto op 
                 inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
                 where concat(coddpto,codprov,coddist) in ".$params->distritos."
                       and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(4) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa." and pp.codsubprogramaproyecto=x.codsubprogramaproyecto)::numeric as _canperfil
               from
               (select 
                nomsubprograma,
                op.codsubprogramaproyecto
               from obs.gen_proyecto op 
               inner join obs.gen_subprogramaproyecto pp on op.codsubprogramaproyecto=pp.codsubprogramaproyecto
               where concat(coddpto,codprov,coddist) in ".$params->distritos."
                     and op.codnivelgobierno=".$params->nivelgobierno." and op.codfase in(2,3,4) and op.codfuncionproyecto=".$params->funcion." and op.codprogramaproyecto=".$params->programa."
               group by nomsubprograma,op.codsubprogramaproyecto
               order by nomsubprograma desc) x");
            break;	
            case 'pipsObjetosCodUnico':
                $pg = new PgSql();								
                $data = $pg->getRows("select 
                op.codigounico as codunico,
                op.montoactualizado as monto,
                ofp.nomfase as fase,
                otp.nomtipoproyecto as tipo,
                fecregistro
               from obs.gen_proyecto op 
               inner join obs.gen_faseproyecto ofp on op.codfase=ofp.codfase
               inner join obs.gen_tipoproyecto otp on op.codtipoproyecto=otp.codtipoproyecto
               where concat(coddpto,codprov,coddist) in ". $params->distritos ."				
                     and op.codnivelgobierno=". $params->nivel ." and op.codfuncionproyecto=". $params->funcion ." and op.codprogramaproyecto=". $params->programa ." and op.codsubprogramaproyecto=". $params->subprograma ." and op.codfase in(2,3,4)");
            break;
            case 'objetosInfraRegion':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                ROW_NUMBER () OVER (ORDER BY iddpto) as numero,
                iddpto,
            departamen,
            (select count(1) from gen_comisarias where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))  ) as comisarias,
            (select count(1) from gen_hospitales where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as hospitales,
            (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and cod_indicador = '0301') as poblacion,
            (select count(1) from gen_colegios where codubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as colegios,
            (select sum(ciprl) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as ciprl 
               from(
                select 
             iddpto,
             departamen,
             string_agg(d.ubigeo, ',') as distritos
                from obs.gen_distrito_ciprl_2 d
                inner join distrito_f df on d.ubigeo=df.iddist
                where ubigeo in ". $params->ubigeo ."
                group by departamen,iddpto
                order by departamen)x");
            break;
            case 'objetosInfraProvincia':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                ROW_NUMBER () OVER (ORDER BY provincia) as numero,
                idprov,
            provincia,
            (select count(1) from gen_comisarias where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))  ) as comisarias,
            (select count(1) from gen_hospitales where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as hospitales,
            (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and cod_indicador = '0301') as poblacion,
            (select count(1) from gen_colegios where codubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as colegios,
            (select sum(ciprl) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as ciprl 
               from(
                select 
             idprov,
             df.provincia,
             string_agg(d.ubigeo, ',') as distritos
                from obs.gen_distrito_ciprl_2 d
                inner join distrito_f df on d.ubigeo=df.iddist
                where iddpto='". $params->dpto ."' and d.ubigeo in ". $params->ubigeo ."
                group by df.provincia,idprov
                order by df.provincia)x");
            break;
            case 'objetosInfraDistritos':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                ROW_NUMBER () OVER (ORDER BY distrito) as numero,
                iddist,
            distrito,
            (select count(1) from gen_comisarias where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))  ) as comisarias,
            (select count(1) from gen_hospitales where ubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as hospitales,
            (select sum(valor) from obs.gen_indicadores_dist_detalle where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and cod_indicador = '0301') as poblacion,
            (select count(1) from gen_colegios where codubigeo=ANY (regexp_split_to_array(x.distritos, ','))) as colegios,
            (select sum(ciprl) from obs.gen_distritos_det where ubigeo = ANY (regexp_split_to_array(x.distritos, ',')) and ano = 2019) as ciprl 
               from(
                select 
             iddist,
             df.distrito,
             string_agg(d.ubigeo, ',') as distritos
                from obs.gen_distrito_ciprl_2 d
                inner join distrito_f df on d.ubigeo=df.iddist
                where idprov='". $params->prov ."'  and d.ubigeo in ". $params->ubigeo ."
                group by df.distrito,iddist
                order by df.distrito)x");
            break;	
            case 'objetosInfraHospital':
                $pg = new PgSql();								
                $data = $pg->getRows("select * from gen_hospitales where ubigeo='". $params->ubigeo ."' order by categoria");
            break;	
            case 'objetosInfraColegios':
                $pg = new PgSql();								
                $data = $pg->getRows("select * from gen_colegios where codubigeo='". $params->ubigeo ."' order by niv_mod");
            break;
            case 'objetosInfraComisarias':
                $pg = new PgSql();								
                $data = $pg->getRows("select * from gen_comisarias where ubigeo='". $params->ubigeo ."'");
            break;	
            case 'lineaFerreaTabla':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor,
                (select 
                  count(distinct iddist)
                from obs.gen_lineaferrea l 
                inner join distrito_f d on st_intersects(l.geom,d.geom) 
                where nam='" . $params->nam ."' and departamen=nombdep
                ) as cantidad
                from obs.gen_lineaferrea l
                inner join departamento d on st_intersects(l.geom,d.geom) 
                inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
                inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
                where nam='" . $params->nam ."'
                GROUP BY d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor");
            break;
            case 'hidroviasTabla':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor,
                (select 
                  count(distinct iddist)
                from obs_new.gen_hidrografia l 
                inner join distrito_f d on st_intersects(l.geom,d.geom) 
                where l.id='" . $params->id ."' and departamen=nombdep
                ) as cantidad
                from obs_new.gen_hidrografia l
                inner join departamento d on st_intersects(l.geom,d.geom) 
                inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
                inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
                where l.id='" . $params->id ."'
                GROUP BY d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor");
            break;
            case 'lineaFerreaTablaDistrito':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                iddist,
                d.distrito,
                ciprl,
                canon,
                pim,
                valor,
                aut.partido,
                aut.electores 
               from obs.gen_lineaferrea l 
               inner join distrito_f d on st_intersects(l.geom,d.geom) 
               inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
               inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'
               left join obs.gen_autoridades_distritales aut on aut.ubigeo = d.iddist
               where nam='" . $params->nam ."' and departamen='" . $params->region ."' group by iddist,d.distrito,
                ciprl,
                canon,
                pim,
                valor, 
                aut.partido,
                aut.electores  
               order by iddist");
            break;
            case 'hidroviasTablaDistrito':
                $pg   = new PgSql();
                $data = $pg->getRows("select 
                iddist,
                d.distrito,
                ciprl,
                canon,
                pim,
                valor,
                aut.partido,
                aut.electores 
               from obs_new.gen_hidrografia l 
               inner join distrito_f d on st_intersects(l.geom,d.geom) 
               inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
               inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'
               left join obs.gen_autoridades_distritales aut on aut.ubigeo = d.iddist
               where l.id='" . $params->id ."' and departamen='" . $params->region ."' group by iddist,d.distrito,
                ciprl,
                canon,
                pim,
                valor, 
                aut.partido,
                aut.electores  
               order by iddist");
            break;
			case 'misDraws':
                $pg = new PgSql();
                $data = $pg->getRows("select id,descripcion,'Poligono'::text tipo,'fa-draw-polygon' icono  from observatorio.gen_objetos_rectangle
                    union all
                    (select id,descripcion,'Circulo'::text tipo,'fa-circle' icono from observatorio.gen_objetos_circle)");     
            break;
            case 'corredorLogisticoTabla':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                ruta,
                sum(canon) as canon,
                sum(ciprl) as ciprl,
                sum(pim) as pim,
                sum(valor) as valor,
                (select 
                count(DISTINCT nombdep)
              from obs.gen_corredoresprincipales l
              inner join departamento d on st_intersects(l.geom,d.geom) 
              where corredor='".$params->nam."' and ruta =p.ruta
                ) as cantidad
               from obs.gen_corredoresprincipales p
               inner join distrito_f d on st_intersects(p.geom,d.geom) 
               inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
               inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'		
               where corredor='". $params->nam ."' group by 
               ruta");
            break;	
            case 'corredorLogisticoTablaRegion':
                $pg = new PgSql();								
                $data = $pg->getRows("select 
                d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor,
                (select
                            count(*)
                            from obs.gen_corredoresprincipales p
                            inner join distrito_f d on st_intersects(p.geom,d.geom) 
                            where corredor='".$params->nam."' and ruta ='".$params->ruta."' and iddpto=first_iddp) as cantidad
               from obs.gen_corredoresprincipales l
               inner join departamento d on st_intersects(l.geom,d.geom) 
               inner join obs.gen_regiones_det d2 on d.first_iddp=d2.coddpto and ano=2019
               inner join obs.gen_indicadores_region_detalle dd on d.first_iddp=dd.coddpto and cod_indicador = '0301'
               where corredor='".$params->nam."' and ruta ='".$params->ruta."'
               group by 
                d.first_iddp,
                nombdep,
                canon,
                ciprl,
                pim,
                valor");
            break;	
            case 'corredorLogisticoTablaDistrito':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                iddist,	   
                distrito,
                canon ,
                ciprl,
                pim,
                valor
               from obs.gen_corredoresprincipales p
               inner join distrito_f d on st_intersects(p.geom,d.geom) 
               inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
               inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'		
               where corredor='".$params->nam."' and ruta ='".$params->ruta."' and iddpto='".$params->region."'
               order by distrito");
            break;	
             case 'bqdUbigeo':
                $pg = new PgSql();
                if($params->nivel==1){
                    $sql="select canon,ciprl,pim,ind.valor as poblacion,co.codubigeo,co.nomubigeo as region
                        from observatorio.gen_dpto_geojson co 
                        inner join obs.gen_regiones_det dp on dp.coddpto=co.codubigeo and ano=2019
                        inner join obs_new.gen_indicadores_region_detalle ind on ind.coddpto =  co.codubigeo and ind.cod_indicador = '0301'";
                
                }else if($params->nivel==2){		
                    $sql="select 
                    ind.valor as poblacion,
                    geo.codubigeo as codubigeo,nomubigeo as region,
                    ciprl,canon,pim  from observatorio.gen_provincia_geojson geo
                    inner join obs.gen_provincias_det det on det.ubigeo=geo.codubigeo and ano=2019
                    inner join obs.gen_indicadores_prov_detalle ind on ind.ubigeo = geo.codubigeo and ind.cod_indicador = '0301'";
                }else if($params->nivel==3){		
                    $sql="select geo.nomubigeo as region,geo.codubigeo as codubigeo,ciprl,ind.valor as poblacion,canon,pim 
                    from observatorio.gen_distrito_geojson geo
                    inner join obs.gen_distritos_det det on geo.codubigeo=det.ubigeo and ano=2019
                    inner join obs.gen_indicadores_dist_detalle ind on ind.ubigeo = geo.codubigeo and ind.cod_indicador = '0301'";
                }					
                $data = $pg->getRows($sql." where ".$params->ubigeo);

            break;
            case 'accidentexXcorredor':
                $pg = new PgSql();								
                $data = $pg->getRows("select tca,muertos,heridos,accidentes from obs.gen_corredoresprincipales c inner join obs_new.gen_tramosaccidentes a on st_intersects(c.geom,a.geom) where corredor='".$params->corredor."' group by tca,muertos,heridos,accidentes order by tca");
            break;
            case 'accidentexXcorredorDistritos':
                $pg = new PgSql();								
                $data = $pg->getRows("select
                d.distrito,
                canon,
                ciprl,
                pim,
                valor 
                from obs_new.gen_tramosaccidentes p
                inner join distrito_f d on st_intersects(p.geom,d.geom) 
                inner join obs.gen_distritos_det d2 on d.iddist=d2.ubigeo and ano=2019
                inner join obs.gen_indicadores_dist_detalle dd on d.iddist=dd.ubigeo and cod_indicador = '0301'		
                where tca='".$params->tca."' ");
            break;
			 case 'cirplxMontoOxi':
                $pg = new PgSql();								
                $data = $pg->getRows("
                with data as(
                 select * from (select 
                 '2011'::text as ano,
                 4084.83::numeric as ciprl,
                 (select sum(ano2011) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2012'::text as ano,
                 5005.53::numeric as ciprl,
                 (select sum(ano2012) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2013'::text as ano,
                 5995.65::numeric as ciprl,
                 (select sum(ano2013) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2014'::text as ano,
                 6060.72::numeric as ciprl,
                 (select sum(ano2014) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                  union all 
                 select * from (select 
                 '2015'::text as ano,
                 5520.61::numeric as ciprl,
                 (select sum(ano2015) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2016'::text as ano,
                 4565.47::numeric as ciprl,
                 (select sum(ano2016) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2017'::text as ano,
                 3680.25::numeric as ciprl,
                 (select sum(ano2017) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                  union all 
                 select * from (select 
                 '2018'::text as ano,
                 (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2018)x) ciprl,
                 (select sum(ano2018) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 union all 
                 select * from (select 
                 '2019'::text as ano,
                 (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2019)x) ciprl,
                 (select sum(ano2019) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                  union all 
                 select * from (select 
                 '2020'::text as ano,
                 (select sum(ciprl)/1000000 from (select ciprl from  obs.gen_regiones_det where ano=2020)x) ciprl,
                 (select sum(ano2020) from observatorio.gen_temporal_gobiernos_oxi   where nivel=1) as oxi)x
                 ) select ano,ciprl,oxi,(oxi*100)/ciprl porcentaje from data order by ano");
            break;
            case 'regionesOXIExcel':
                $pg = new PgSql();
                $data = $pg->getRows("select id,entidad,nombre,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2018,suma2019,total,estado from obs.gen_regiones_oxi_excel where estado='" . $params->estado . "'");
            break;
            case 'provinciasOXIExcel':
                $pg = new PgSql();
                $data = $pg->getRows("select id,entidad,nombre,region,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2018,suma2019,total,estado from obs.gen_provincias_oxi_excel where estado='" . $params->estado . "'");
            break;
            case 'distritosOXIExcel':
                $pg = new PgSql();
                $data = $pg->getRows("select id,entidad,nombre,dpto,provincia,suma2009,suma2010,suma2011,suma2012,suma2013,suma2014,suma2015,suma2016,suma2017,suma2018,suma2019,total,estado from obs.gen_distritos_oxi_excel where estado='" . $params->estado . "'");
            break;
			case 'quintiles_sinadef':
                $pg = new PgSql();
                if($params->idNivel==1){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='sinadef' group by quantil order by quantil");
                }else if($params->idNivel==2){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_provincia_salud where tipo='sinadef' group by quantil order by quantil");
                }else if($params->idNivel==3){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_distrito_salud where tipo='sinadef' group by quantil order by quantil");
                }           
            break;
            case 'quintiles_sinadef_msj':
                $pg = new PgSql();
                if($params->idNivel==1){
                    $data = $pg->getRows("select quantil from observatorio.gen_region_salud where ubigeo='" . $params->ubigeo . "' and tipo='sinadef'");
                }else if($params->idNivel==2){
                    $data = $pg->getRows("select quantil from observatorio.gen_provincia_salud where ubigeo='" . $params->ubigeo . "' and tipo='sinadef'");
                }else if($params->idNivel==3){
                    $data = $pg->getRows("select quantil from observatorio.gen_distrito_salud where ubigeo='" . $params->ubigeo . "' and tipo='sinadef'");
                }
            break;
            case 'quintiles_sinadef_det':
                $pg = new PgSql();
                if($params->idNivel==1){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_region_salud where tipo='sinadef' and quantil=" . $params->quantil . "");
                }else if($params->idNivel==2){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,(select concat(first_nomb,', ',nombprov) from provincia where first_idpr=ubigeo) nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_provincia_salud where tipo='sinadef' and quantil=" . $params->quantil . "");
                }else if($params->idNivel==3){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,(select concat(departamen,', ',provincia, ', ',distrito) from distrito_f where iddist=ubigeo) nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_distrito_salud where tipo='sinadef' and quantil=" . $params->quantil . "");
                }
            break;
			case 'quintiles_covid_det':
                $pg = new PgSql();
                if($params->idNivel==1){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_region_salud where tipo='covid' and quantil=" . $params->quantil . "");
                }else if($params->idNivel==2){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,(select concat(first_nomb,', ',nombprov) from provincia where first_idpr=ubigeo) nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_provincia_salud where tipo='covid' and quantil=" . $params->quantil . "");
                }else if($params->idNivel==3){
                    $data = $pg->getRows("select row_number() OVER(order by nomubigeo)id,(select concat(departamen,', ',provincia, ', ',distrito) from distrito_f where iddist=ubigeo) nomubigeo,fallecidos_2019,fallecidos_2020,diferencia,poblacion from observatorio.gen_distrito_salud where tipo='covid' and quantil=" . $params->quantil . "");
                }
            break;
			case 'quintiles_covid':
                $pg = new PgSql();
                if($params->idNivel==1){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_region_salud where tipo='covid' group by quantil order by quantil");
                }else if($params->idNivel==2){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_provincia_salud where tipo='covid' group by quantil order by quantil");
                }else if($params->idNivel==3){
                    $data = $pg->getRows("select quantil,concat('Q',quantil::text) texto,sum(diferencia) fallecidos,sum(fallecidos_2019) fallecidos2019,sum(fallecidos_2020) fallecidos2020,count(*) entidades,sum(poblacion) poblacion from observatorio.gen_distrito_salud where tipo='covid' group by quantil order by quantil");
                }           
            break;
			case 'tabla__mes_sinadef':
                $pg = new PgSql();
                $data = $pg->getRows("select 
                    (select * from obs.fn_nombre_mes(mes)) meses, 
                    count(*) s2017,
                    (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2018) s2018,
                    (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2019) s2019,
                    (select count(*) from obs.tmp_fallecidos_sinadef where mes=x.mes and anio=2020) s2020   
                from obs.tmp_fallecidos_sinadef x where anio=2017 group by mes order by mes");
            break;
			 case 'tabla__mes_sinadef_totales':
                $pg = new PgSql();
                $data = $pg->getRows("select
                (select count(*) from obs.tmp_fallecidos_sinadef where anio=2017) t1,
                (select count(*) from obs.tmp_fallecidos_sinadef where anio=2018) t2,
                (select count(*) from obs.tmp_fallecidos_sinadef where anio=2019) t3,
                (select count(*) from obs.tmp_fallecidos_sinadef where anio=2020) t4");
            break;
            case 'quintil_mes_covid':
                $pg = new PgSql();
                $data = $pg->getRows("select (select * from obs.fn_nombre_mes(mes)) meses,fallecidos  from(
                    select 
                      substr(fecha_fallecimiento,5,2)::int mes,
                      count(*) fallecidos
                     from obs.tmp_fallecidos_minsa group by substr(fecha_fallecimiento,5,2) order by substr(fecha_fallecimiento,5,2))x");
            break;
			case 'PartidoPolitico':
				$pg   = new PgSql();
				$data = $pg->getRows("SELECT * FROM obs.gen_ddlpolitica ");
			break;
			case 'draw_circle_table':
                $pg = new PgSql();
                $data = $pg->getRows("select monto,codigounico,oportunidad,organizacion,funcion,programaproyecto,subprograma,fasegobierno,nivelgobierno from observatorio.gen_proyectos d inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                    on st_intersects(st_buffer(d.geometry::geography,1)::geometry,p.geom::geometry) where concat(coddpto,codprov,coddist) is not null and ".$params->consulta." ");     
            break;
            case 'draw_circle_ciprl_table':
                $pg = new PgSql();
                $data = $pg->getRows("select  monto,codigounico,oportunidad,organizacion,funcion,programaproyecto,subprograma,fasegobierno,nivelgobierno from observatorio.gen_proyectos d inner join (select st_buffer(ST_SetSRID(ST_MakePoint(".$params->lng.", ".$params->lat."),4326)::geography,".$params->radio.") as geom) p 
                    on st_intersects(st_buffer(d.geometry::geography,1)::geometry,p.geom::geometry) where concat(coddpto,codprov,coddist) is not null ");     
            break;
            case 'codtamano':
                $pg   = new PgSql();
                $data = $pg->getRows("select codtamanio as codigo,nomtamanio as valor from obs.gen_emp_tamanio where codtamanio !=1");	
            break;
            //
            case 'codsector':
                $pg   = new PgSql();
                $data = $pg->getRows("select codsector as codigo, nomsector as valor from obs.gen_sector order by nomsector");	
            break;
            case 'codgrupo':
                $pg   = new PgSql();
                $data = $pg->getRows("select codgrupo as codigo,upper(nomgrupo) as valor from obs.gen_grupoeconomico where codgrupo!=1");	
            break;
			case 'proyectado_fallecidos':
                $pg = new PgSql();
                $data = $pg->getRows("select * from observatorio.gen_proyectado_fallecidos");     
            break;
			case 'analisisempresas_financiero':
                $pg = new PgSql();
                $data = $pg->getRows("select a.idanalisis,a2015,a2016,a2017,a2018,a2019,descripcion from observatorio.gen_analisisempresas_financiero a inner join observatorio.gen_empresas_tipo_analisis b
                on a.idanalisis=b.id where a.id='".$params->id."'");     
            break;
			case 'proyectados_fallecidos_anio':
                $pg = new PgSql();
                $data = $pg->getRows("with data as(
                    select sum(a2017) a2017,sum(a2018) a2018,sum(a2019) a2019,sum(a2020) a2020 from observatorio.gen_proyectado_fallecidos_anio)
                    select a2017,a2018,a2019,((a2017+a2018+a2019)/3)a2020,( a2020-((a2017+a2018+a2019)/3)) diferencia from data");     
            break;
			case 'anexos_sunat':
                $pg = new PgSql();
                $data = $pg->getRows("with data as(
					select 
					 (select nombdep from departamento where first_iddp=substr(ubigeo,1,2)) departamento,
					 (select nombprov from provincia where first_idpr=substr(ubigeo,1,4)) provincia,
					 (select distrito from distrito_f where iddist=substr(ubigeo,1,6)) distrito,
					 tipo_via,
					 nombre_via
					from(
					 select 
					  case
						when length(ubigeo)=6 then ubigeo
						else concat('0',ubigeo)
					   end ubigeo,
					   case
						when tipo_via='-' then ' '
						else tipo_via
					   end tipo_via,
					   case
						when nombre_via='-' then ' '
						else nombre_via
					   end nombre_via,
					   ruc
					 from observatorio.gensunat_anexos) x
					 where ruc='".$params->ruc."')
					 select departamento,provincia,distrito,concat(distrito,', ',tipo_via,' ',nombre_via) dir from data");     
            break;
			case 'counterSumEmpresa':
                $pg = new PgSql();
                $data = $pg->getRows("select count(*) contador from(select
					 em.codempresa as codempresa,
					 em.oxi as participaoxi,
					 em.nomempresa as empresa,
					 em.rucempresa as ruc,
					 se.nomsector  as sector,
					 COALESCE(ge.nomgrupo, '')   as grupo,
					 ''::text as origen,
					 dp.nomdpto as region,
					 em.numtrabajadores as nro_trabajadores, 
					 em.numsucursales as nro_sucursales,
					 COALESCE(em.ranking, 0) as ranking,
					 ingresos as ingresos,
					 utilidad as utilidad,
					 em.disponibleoxi as oxi,
					 em.patrimonio as patrimonio, 
					 em.ubicacion, 
					 em.coddpto::integer as coddpto,
					 tm.nomtamanio as tamano,
					 tm.codtamanio as codcodtamanio,
					 se.codsector as codsector,
					 em.viatipo as listado,
					 em.vianombre as resolucion,
					 em.codgrupo as codgroup,
					 case
					  when  em.vianombre='-' then 'NO' 
					  else 'SI' 
					 end as resolucionGrupo
					from obs.gen_empresa em 
					inner join obs.gen_sector se on  se.codsector = em.codsector 
					inner join obs.gen_grupoeconomico ge on ge.codgrupo = em.codgrupo
					inner join obs.gen_departamento dp on dp.coddpto = em.coddpto
					inner join obs.gen_emp_tamanio tm on tm.codtamanio = em.codtamanio
					)x where ".$params->sql_filter);     
            break;
			case 'ingresos_egresos_empresa':
                $pg = new PgSql();
                $data = $pg->getRows("select 'Año 2019' anio,ingresos_2019 ingresos,utilida_2019 utilidad,activo_2019 activo,patirmonio_2019 patrimonio,pasivo_2019 pasivo from observatorio.gen_informacion_financieras where ruc='".$params->ruc."'
				   union all
				  (select 'Año 2018' anio,ingresos2018,utilidad_neta_2018,activos_2018,patrimonio_2018,pasivo_total_2018 from observatorio.gen_informacion_financieras where ruc='".$params->ruc."') ");     
			break;
			case 'macro_top':
                $pg = new PgSql();
                $data = $pg->getRows("select a.ruc,a.utilidad::numeric,a.oxi,razon_social,ROW_NUMBER () OVER (ORDER BY utilidad::numeric desc) numeracion,
					CASE
						 WHEN participa=1  THEN 'Participa'
						 ELSE  'No Participa'
					END participa
				 from observatorio.gen_macroindicadores a 
				inner join observatorio.gen_informacion_financieras b on a.ruc=b.ruc 
				order by utilidad::numeric desc");     
			break;
			case 'macro_mef':
                $pg = new PgSql();
                $data = $pg->getRows("select * from observatorio.fn_macroinversion('".$params->tipo."','".$params->forma."')");     
			break;
			case 'macro_mef_Funcion':
                $pg = new PgSql();
                $data = $pg->getRows("select * from observatorio.fn_macroinversion_funcion('".$params->tipo."','".$params->forma."')");     
			break;
        }
        return $data;
    }
    function listJoin($data){	
        $arr = []; 
                                        
        foreach ($data as $key){
            array_push($arr, (int)$key->codigo);
        }
        
        return implode(',',$arr);
    }
	function setBackground($quantil){
        $bgcolor = '';
        
        switch($quantil){
            case 1:
                $bgcolor = 'quantil-1';
            break;
            case 2:
                $bgcolor = 'quantil-2';
            break;
            case 3:
                $bgcolor = 'quantil-3';
            break;
            case 4:
                $bgcolor = 'quantil-4';
            break;
            case 5:
                $bgcolor = 'quantil-5';
            break;
        }
        
        return $bgcolor;
    }
    
    function setBackgroundglobal($quantil){
        $bgcolor = '';
            if($quantil>=84.1)
            {$bgcolor = 'quantil-1';}
             elseif($quantil>=74.1 && $quantil<=84  )
            {$bgcolor = 'quantil-2';}
            elseif($quantil>=64.1 && $quantil<=74  )
            {$bgcolor = 'quantil-3';}
             elseif($quantil>=54.1 && $quantil<=64  )
             {$bgcolor = 'quantil-4';}
             elseif($quantil>=45.1 && $quantil<=54  )
            {$bgcolor = 'quantil-0';}
            elseif($quantil<=45)
            {$bgcolor = 'quantil-5';}
        return $bgcolor;
    }
    function isNullOrEmpty($arr){
        $tmp = [];
        
        foreach ($arr as $value) {
            if($value == 0){
                array_push($tmp, 0);
            }else{
                array_push($tmp, $value);
            }
        }
        
        return $tmp;
    }
    
    function encodeURIComponent($str) {
        
        $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
        return strtr(rawurlencode($str), $revert);
    }