<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
$method = $_SERVER['REQUEST_METHOD'];
if($method == "OPTIONS") {
    die();
}

require 'PgSql.php';
$type = "MARKERS";

if($_GET && $_GET["cql_filter"]){
	$pg = new PgSql();
	$type = $_GET["type"];
	
	if ($type == "MARKERS")
		$data = $pg->getRows("select id, cod_mod, centro_educativo, latitud, longitud from observatorio.ie_ficha where " . $_GET["cql_filter"]);
	if ($type == "MARKER_DETAIL")
		$data = $pg->getRows("select * from observatorio.ie_ficha where " . $_GET["cql_filter"]);
	
	echo json_encode($data);
}
