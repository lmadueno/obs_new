<?php
class PgSql
{
    private $db;       //The db handle
    public  $num_rows; //Number of rows
    public  $last_id;  //Last insert id
    public  $aff_rows; //Affected rows
    public function __construct()
    {
        require 'config.php';
		ini_set('memory_limit', '-1');
	
        $this->db = pg_connect("host=$host port=$port dbname=$dbname 
                                user=$user password=$passwd");// aqui creas la cadena de copnexion 
        if (!$this->db) exit();
    }
	
	public function getRows($sql)// y esta es la funcion que obtines al realizar los select en la bd
    {
		$rows = array();
		
		try {
			$result = pg_query($this->db, $sql);
			pg_close($this->db);			
			while ($item = pg_fetch_object($result)) {
				$rows[] = $item;// aca obtines toda la consulta y lo encapsulas en un arreglo
			}
		} catch (Exception $e) {
			echo "Ocurri� un eror " . $e;
		}
		return $rows;
	}
}
?>