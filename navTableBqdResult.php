

<?php
    require 'php/app.php';
            $data = json_decode($_GET['data']);	
            $respuesta= dropDownList((object) ['method' => 'bqdUbigeo','ubigeo'=> $data->consulta_region,'nivel'=>1]);
?>

<div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                <div class="row">
                    <div class="col-10">Lista de Entidades Buscadas</div> 
                    <div class="col-2 text-right">
						<a href="#" class="lnkAmpliar"  data-event="lnk_entidadesFiltro1"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down "></i></a>
					</div>
                </div>
            </div>
        </div>
        <div class="col-lg-12" data-target="lnk_entidadesFiltro1">
            <table class="table table-sm table-detail" >   
                <tr>
                    <td>#</td>
                    <td>Región</td>
                    <td>Ciprl</td>
                    <td>Canon</td>
                    <td>PIM</td>
                </tr>
                <?php    
                    $i=0;             
                    foreach ($respuesta as $item){
                        $i++;                     											
                ?>	
                <tr>
                    <td><?php echo number_format($i)?></td>
                    <td class="text-right">
                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $item->region?>" href="#" onclick="App.events(this); return false;">
                            <?php echo $item->region;?>
                        </a>													
                    </td>
                    <td><?php echo number_format($item->ciprl)?></td>
                    <td><?php echo number_format($item->canon)?></td>
                    <td><?php echo number_format($item->pim)?></td>
                </tr>
                <tr data-target="lnkProvXrutas_<?php echo $item->region?>" style="display: none;">
                    <td colspan="5">
                        <div class="card">
                            <div class="card-header card-special">
                                Provincias
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table table-sm table-detail">
                                            <tr>
                                                <td>#</td>
                                                <td>Región</td>
                                                <td>Ciprl</td>
                                                <td>Canon</td>
                                                <td>PIM</td>
                                            </tr>
                                            <?php    
                                                $i=0;  
                                                $respuesta= dropDownList((object) ['method' => 'bqdUbigeo','ubigeo'=> $data->consulta_provincia,'nivel'=>2]);          
                                                foreach ($respuesta as $item){
                                                    $i++;                     											
                                            ?>	
                                            <tr>
                                                <td><?php echo number_format($i)?></td>
                                                <td class="text-right">
                                                    <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $item->region?>" href="#" onclick="App.events(this); return false;">
                                                        <?php echo $item->region;?>
                                                    </a>													
                                                </td>
                                                <td><?php echo number_format($item->ciprl)?></td>
                                                <td><?php echo number_format($item->canon)?></td>
                                                <td><?php echo number_format($item->pim)?></td>
                                            </tr>
                                            <tr data-target="lnkProvXrutas_<?php echo $item->region?>" style="display: none;">
                                                <td colspan="5"> 
                                                    <div class="card">
                                                        <div class="card-header card-special">
                                                            Provincias
                                                        </div>
                                                        <div class="car-body">
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <table class="table table-sm table-detail">
                                                                        <tr>
                                                                            <td>#</td>
                                                                            <td>Región</td>
                                                                            <td>Ciprl</td>
                                                                            <td>Canon</td>
                                                                            <td>PIM</td>
                                                                        </tr>
                                                                        <?php    
                                                                            $i=0;  
                                                                            $respuesta= dropDownList((object) ['method' => 'bqdUbigeo','ubigeo'=> $data->consulta_distrito,'nivel'=>3]);          
                                                                            foreach ($respuesta as $item){
                                                                                $i++;                     											
                                                                        ?>	
                                                                             <tr>
                                                                                <td><?php echo number_format($i)?></td>
                                                                                <td><?php echo ($item->region)?></td>
                                                                                <td><?php echo number_format($item->ciprl)?></td>
                                                                                <td><?php echo number_format($item->canon)?></td>
                                                                                <td><?php echo number_format($item->pim)?></td>
                                                                            </tr>
                                                                        <?php                 
                                                                            }               											
                                                                        ?>
                                                                    </table> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>   
                                            </tr>
                                            <?php                 
                                                }               											
                                            ?>   
                                        </table> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td> 
                </tr>           
                <?php                 
                    }               											
                ?>	
            </table>                 
        </div>
    </div>
