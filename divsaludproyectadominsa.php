<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>

<div class="row">
	<div class="col-lg-6">
		<div class="card">
			<div class="card-header" style="background:#304e9c">
				<h4 class="m-b-0 text-white text-center">COVID-19</h4>
			</div>
			<br>
			<ul class="nav nav-tabs" id="myTab" role="tablist">

				<li class="nav-item">
					<a class="nav-link active estadisticasminsa" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Datos Estadísticos</b></a>
				</li>
				<li class="nav-item">
					<a class="nav-link active estadisticasminsa" style="color: black; !important;" id="geograficos" href="#" onclick="App.events(this);return false;" title="Geograficos"><b style="padding-left:0.5em;">Datos Geográficos</b></a>
				</li>

			</ul>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-primary alert-dismissible fade show" role="alert">
							<strong>Leyenda:</strong>
							<hr>
							<ul>
								<li>
									<?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'covid']);
									echo "(*)Fecha de actualizacion " . $pipxUbigeoDet[0]->fecha;
									?>
								</li>
								<li>
									(*) Fecha de Proyección : 30-Junio 2021
								</li>
							</ul>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Departamentos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_minsa" id="ddlDepartamentos_minsa" name="ddlDepartamentos_minsa" onchange="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Provincias</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_minsa" id="ddlProvincias_minsa" name="ddlProvincias_minsa" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Distritos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_minsa" id="ddlDistritos_minsa" name="ddlDistritos_minsa" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Accion</label>
															<button type="button" class="form-control input-sm btn btn-primary filtrominsa" id="filtrominsa" name="filtrominsa" onclick="App.events(this); return false;">Filtrar</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php if ($data->tab == 'datos') { ?>
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">

							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Indicadores
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div id="chart6" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
								</div>
								<div class="col-lg-6">
									<div id="chart7" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div id="chart9" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
								</div>
								<div class="col-lg-6">
									<div id="chart101" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
								</div>
							</div>
							<!-- chart 1 meses -->
							<div id="chart8" style=" height: 320px;"></div>
							<div class="card-body">
								<div class="row">
									<div class="text-center col-lg-12">

										<a class="nav-link active vertabla" data-event="tabla1" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla Fallecidos COVID-19 a nivel nacional meses por año</b></a>
										<!-- </div> -->
									</div>
								</div>
								<table class="table table-sm table-detail" data-target="tabla1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th class="text-center bold" width="4%">#</th>
											<th class="text-center bold">Mes</th>
											<th class="text-center bold">Año 2020</th>
											<th class="text-center bold">Año 2021</th>
										</tr>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'quintil_mes_covid']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->anio2020);
											$stotal = $stotal + intval($item->anio2021); ?>
											<tr>
												<td class="text-center"><?php echo ($i) ?></td>
												<td class="text-center"><?php echo ($item->mes) ?></td>
												<td class="text-center"><?php echo ($item->anio2020) ?></td>
												<?php if ($i >= 2 && $i <= 4) { ?>
													<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->anio2021) . '*' ?></td>
												<?php } else { ?>
													<td class="text-center"><?php echo ($item->anio2021) ?></td>
												<?php } ?>
											</tr>
										<?php
										} ?>
										<tr>
											<td class="text-center"></td>
											<td class="text-center">Total</td>

											<td class="text-center"><?php echo number_format($sTotal) ?></td>
											<td class="text-center"><?php echo number_format($stotal) ?></td>

										</tr>
									</thead>
								</table>
								<br>

							</div>
							<div id="chart1" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

							<!-- chart2 semanas -->
							<div id="chart2" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
							<div id="chartd" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="tabla2" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla Fallecidos COVID-19 a nivel nacional semanas por año</b></a>
									<!-- </div> -->
								</div>
							</div>
							<table class="table table-sm table-detail" data-target="tabla2" width="100%" style="display: none;">
								<thead>
									<tr>
										<th class="text-center bold" width="4%">#</th>
										<th class="text-center bold">Semanas del año</th>
										<th class="text-center bold">Semana</th>
										<th class="text-center bold">Año 2020</th>
										<th class="text-center bold">Semana</th>
										<th class="text-center bold">Año 2021</th>
									</tr>
									<?php
									$i = 0;

									$sTotal = 0;
									$stotal = 0;
									$pipxUbigeoDet = dropDownList((object) ['method' => 'quintil_semanal_covid']);
									foreach ($pipxUbigeoDet as $item) {
										$i++;

										$sTotal = $sTotal + intval($item->anio2020);
										$stotal = $stotal + intval($item->anio2021); ?>
										<tr>
											<td class="text-center"><?php echo ($i) ?></td>
											<td class="text-center"><?php echo ($item->semana) ?></td>
											<td class="text-center"><?php echo ($item->f2020) ?></td>
											<td class="text-center"><?php echo ($item->anio2020) ?></td>
											<td class="text-center"><?php echo ($item->f2021) ?></td>
											<?php if ($i >= 8  && $i <= 17) { ?>
												<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->anio2021) . '*' ?></td>
											<?php } else { ?>
												<td class="text-center"><?php echo ($item->anio2021) ?></td>
											<?php } ?>

										</tr>
									<?php
									} ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($sTotal) ?></td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo number_format($stotal) ?></td>

									</tr>
								</thead>
							</table>
							<br>
							<div id="chart102" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
							<div id="chart103" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

							<div id="chart5" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="tabla3" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Comparativo anual de fallecidos COVID-19 por Edad, 2017 - 2021*</b></a>
									<!-- </div> -->
								</div>
							</div>

							<table class="table table-sm table-detail" data-target="tabla3" width="100%" style="display: none;">
								<thead>
									<tr>
										<th class="text-center b old" width="4%">#</th>
										<th class="text-center bold">Edades</th>
										<!-- <th class="text-center bold">Semana</th> -->
										<th class="text-center bold">Año 2020</th>
										<!-- <th class="text-center bold">Semana</th> -->
										<th class="text-center bold">Año 2021</th>
									</tr>
									<?php
									$i = 0;

									$sTotal = 0;
									$stotal = 0;
									$pipxUbigeoDet = dropDownList((object) ['method' => 'edadgeneralminsa']);
									foreach ($pipxUbigeoDet as $item) {
										$i++;

										$sTotal = $sTotal + intval($item->a2020);
										$stotal = $stotal + intval($item->a2021); ?>
										<tr>
											<td class="text-center"><?php echo ($i) ?></td>
											<td class="text-center"><?php echo ($i) ?></td>
											<!-- <td class="text-center"><?php echo ($item->f2020) ?></td> -->
											<td class="text-center"><?php echo ($item->a2020) ?></td>
											<!-- <td class="text-center"><?php echo ($item->f2021) ?></td> -->
											<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->a2021) . '' ?></td>

											<!-- <?php if ($i >= 8  && $i <= 17) { ?>
														<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->anio2021) . '*' ?></td>
													<?php } else { ?>
														<td class="text-center"><?php echo ($item->anio2021) ?></td>
													<?php } ?> -->

										</tr>
									<?php
									} ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>

										<td class="text-center"><?php echo number_format($sTotal) ?></td>

										<td class="text-center"><?php echo number_format($stotal) ?></td>

									</tr>
								</thead>
							</table>

						</div>
					</div>
				</div>
		</div>
	</div>

<?php } else if ($data->tab == 'geo') { ?>
	<br>
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div id="getResumenSalud"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<div class="col-lg-6">
	<div class="card">
		<div class="card-header" style="background:#304e9c">
			<h4 class="m-b-0 text-white text-center">SINADEF</h4>
		</div>
		<br>
		<ul class="nav nav-tabs" id="myTab" role="tablist">

			<li class="nav-item">
				<a class="nav-link active estadisticassinadef" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Datos Estadísticos</b></a>
			</li>
			<li class="nav-item">
				<a class="nav-link active estadisticassinadef" style="color: black; !important;" id="geograficos" href="#" onclick="App.events(this); return false;" title="Geograficos"><b style="padding-left:0.5em;">Datos Geográficos</b></a>
			</li>

		</ul>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="alert alert-primary alert-dismissible fade show" role="alert">
						<strong>Leyenda:</strong>
						<hr>
						<ul>
							<li>
								<?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'sinadef']);
								echo "(*)Fecha de actualizacion " . $pipxUbigeoDet[0]->fecha;
								?>
							</li>
							<li>
								(*) Fecha de Proyeccion: 30-Junio 2021
							</li>
						</ul>
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							<div class="row">
								<div class="col-lg-12">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="row">
												<div class="col-lg-3">
													<div class="form-group">
														<label style="padding-left: 10px;">Departamentos</label>
														<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_sinadef" id="ddlDepartamentos_sinadef" name="ddlDepartamentos_sinadef" onchange="App.events(this); return false;">
															<option value="0">[Seleccione]</option>
														</select>
													</div>
												</div>
												<div class="col-lg-3">
													<div class="form-group">
														<label style="padding-left: 10px;">Provincias</label>
														<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_sinadef" id="ddlProvincias_sinadef" name="ddlProvincias_sinadef" onclick="App.events(this); return false;">
															<option value="0">[Seleccione]</option>
														</select>
													</div>
												</div>
												<div class="col-lg-3">
													<div class="form-group">
														<label style="padding-left: 10px;">Distritos</label>
														<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_sinadef" id="ddlDistritos_sinadef" name="ddlDistritos_sinadef" onclick="App.events(this); return false;">
															<option value="0">[Seleccione]</option>
														</select>
													</div>
												</div>
												<div class="col-lg-3">
													<div class="form-group">
														<label style="padding-left: 10px;">Accion</label>
														<button type="button" class="form-control input-sm btn btn-primary filtrominsa_sinadef" id="filtrominsa_sinadef" name="filtrominsa_sinadef" onclick="App.events(this); return false;">Filtrar</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if ($data->tab == 'datos') { ?>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Indicadores
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div id="chart_self" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
							</div>
							<div class="col-lg-6">
								<div id="chart_selh" style="min-width: 310px; height: 800px; margin: 0 auto"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div id="chart_6" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
							</div>
							<div class="col-lg-6">
								<div id="chart_7" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div id="chart_9" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
							</div>
							<div class="col-lg-6">
								<div id="chart_101" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
							</div>
						</div>
						<!-- chart 1 meses -->
						<div id="chart_8" style="min-width: 310px; height: 320px; margin: 0 auto"></div>
						<div class="card-body">
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="tabla10" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla Fallecidos SINADEF a nivel nacional meses por año</b></a>
									<!-- </div> -->
								</div>
							</div>
							<table class="table table-sm table-detail" data-target="tabla10" width="100%" >
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Mes</th>
										<th class="text-center bold">2018</th>
										<th class="text-center bold">2019</th>
										<th class="text-center bold">2020</th>
										<th class="text-center bold">2021</th>

									</tr>									
								</thead>
								<tbody id="tablasinadef1">
							
								</tbody>
							</table>
							<br>

						</div>
						<div id="chart10" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

						<!-- chart2 semanas -->
						<div id="chart20" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
						<div id="chartdd" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
						<div class="row">
							<div class="text-center col-lg-12">

								<a class="nav-link active vertabla" data-event="tabla20" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver tabla Fallecidos SINADEF a nivel nacional semanas por año</b></a>
								<!-- </div> -->
							</div>
						</div>
						<table class="table table-sm table-detail" data-target="tabla20" width="100%" style="display: none;">
							<thead>
								<tr>
									<th class="text-center bold vert-middle" colspan="2">Semana</th>

									<th></th>
									<th class="text-center bold vert-middle">2018</th>
									<th></th>
									<th class="text-center bold vert-middle">2019</th>
									<th></th>
									<th class="text-center bold vert-middle">2020</th>
									<th></th>
									<th class="text-center bold vert-middle">2021</th>

								</tr>

								<?php
								$i = 0;
								$s2017 = 0;
								$s2018 = 0;
								$s2019 = 0;
								$s2020 = 0;
								$s2021 = 0;


								$pipxUbigeoDet = dropDownList((object) ['method' => 'tabla__sem_sinadef']);
								foreach ($pipxUbigeoDet as $item) {
									$i++;

									$s2018 =  intval($item->anio2018) + intval($s2018);
									$s2019 =  intval($item->anio2019) + intval($s2019);
									$s2020 =  intval($item->anio2020) + intval($s2020);
									$s2021 =  intval($item->anio2021) + intval($s2021);


								?>
									<tr>
										<td class="text-center"><?php echo ($i) ?></td>
										<td class="text-center"><?php echo ($item->semana) ?></td>


										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->anio2018) ?></td>

										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->anio2019) ?></td>

										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->anio2020) ?></td>

										<td class="text-center"></td>
										<?php if ($i >= 8  && $i <= 18) { ?>
											<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->anio2021) . '*' ?></td>
										<?php } else { ?>
											<td class="text-center"><?php echo ($item->anio2021) ?></td>
										<?php } ?>


									</tr>
								<?php } ?>
								<tr>
									<td class="text-center"></td>
									<td class="text-center">Total</td>


									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2018) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2019) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2020) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2021) ?></td>


								</tr>
							</thead>
						</table>
						<div id="chart70" style="min-width: 310px; height: 500px; margin: 0 auto"></div>
						<br>
						<div id="chart60" style="min-width: 310px; height: 500px; margin: 0 auto"></div>

						<!-- edades -->
						<div id="chart50" style="min-width: 310px; height: 600px; margin: 0 auto"></div>
						<div class="row">
							<div class="text-center col-lg-12">

								<a class="nav-link active vertabla" data-event="tabla30" style="color: #1976D2; !important;" id="estadisticas" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Comparativo anual de fallecidos SINADEF por Edad, 2017 - 2021*</b></a>
								<!-- </div> -->
							</div>
						</div>
						<table class="table table-sm table-detail" data-target="tabla30" width="100%" style="display: none;">
							<thead>
								<tr>
									<th class="text-center bold vert-middle" colspan="2">#</th>
									<th class="text-center bold vert-middle">Mes</th>

									<th></th>
									<th class="text-center bold vert-middle">2018</th>
									<th></th>
									<th class="text-center bold vert-middle">2019</th>
									<th></th>
									<th class="text-center bold vert-middle">2020</th>
									<th></th>
									<th class="text-center bold vert-middle">2021</th>

								</tr>

								<?php
								$i = 0;
								$s2017 = 0;
								$s2018 = 0;
								$s2019 = 0;
								$s2020 = 0;
								$s2021 = 0;


								$pipxUbigeoDet = dropDownList((object) ['method' => 'edadgeneral']);
								foreach ($pipxUbigeoDet as $item) {
									$i++;

									$s2017 =  intval($item->a2017) + intval($s2017);
									$s2018 =  intval($item->a2018) + intval($s2018);
									$s2019 =  intval($item->a2019) + intval($s2019);
									$s2020 =  intval($item->a2020) + intval($s2020);
									$s2021 =  intval($item->a2021) + intval($s2021);


								?>
									<tr>
										<td class="text-center"><?php echo ($i) ?></td>
										<td class="text-center"><?php echo ($item->edad) ?></td>

										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->a2017) ?></td>
										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->a2018) ?></td>

										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->a2019) ?></td>

										<td class="text-center"></td>
										<td class="text-center"><?php echo intval($item->a2020) ?></td>

										<td class="text-center"></td>
										<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->a2021) . '' ?></td>

										<!-- <?php if ($i >= 8  && $i <= 18) { ?>
														<td class="text-center" style="background-color: #F6FE7F;"><?php echo ($item->anio2021) . '*' ?></td>
													<?php } else { ?>
														<td class="text-center"><?php echo ($item->anio2021) ?></td>
													<?php } ?> -->


									</tr>
								<?php } ?>
								<tr>
									<td class="text-center"></td>
									<td class="text-center">Total</td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2017) ?></td>
									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2018) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2019) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2020) ?></td>

									<td class="text-center"></td>
									<td class="text-center"><?php echo intval($s2021) ?></td>


								</tr>
							</thead>
						</table>

					</div>

				</div>
			</div>
	</div>
<?php } else if ($data->tab == 'geo') { ?>
	<br>
	<div class="card-body">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div id="getResumenSaludsinadef"></div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

	</div>