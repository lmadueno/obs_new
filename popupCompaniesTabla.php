<?php
	require 'php/app.php';
	$data = json_decode($_GET['data']);	
?>
	
			<div class="card">
				<div class="card-header card-special">
					Lista de Proyectos
                </div>
                <div class="card-body">
					<div class="row">
						<div class="col-lg-12">
                            <table class="table table-sm table-detail">
											    <tr>
                                                    <th class="text-center bold" width="5%">#</th>
												    <th class="text-center bold" width="15%">Codigo Unico</th>
												    <th class="text-center bold" width="40%">Nombre Proyecto</th>
												    <th class="text-center bold" width="20%">Estado</th>
                                                    <th class="text-center bold" width="5%">Monto</th>
                                                    <th class="text-center bold" width="15%">Fecha</th>
                                                </tr>
                                                    <?php 
															
														 $tabladet = dropDownList((object) ['method' => 'getSnipForEmpresa', 'empresa' => $data->empresa]);
														 $i=0;
                                                    ?>
                                                    <?php foreach ($tabladet as $key){ ?>
                                                <tr>
                                                    <td class="text-center" ><?php $i++;  echo ($i)?></td>
													<td class="text-center"><a href="" class="link_" onclick="App.events(this); return false;" data-event='<?php echo $key->snip ?>'><?php echo $key->snip?></a></td>
                                                    
                                                    <td class="text-left" ><?php    echo ucfirst($key->nombre_proyecto)?></td>
													<td class="text-left" ><?php    echo ($key->estado)?></td>
                                                    <td class="text-right" ><?php    echo ($key->monto_inversion)?></td>
                                                    <td class="text-center" ><?php    echo ($key->fecha_buena_pro)?></td>
                                                </tr>
                                                <?php }?>
                            </table>
                        </div>
                    </div>
                </div>
			</div>
