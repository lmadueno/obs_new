<?php
require 'php/app.php';

   $data = json_decode($_GET['data']);	
   //print_r($data);
   
   $map    = dropDownList((object) ['method' => 'consolidadototalProyecto']);
    
?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            Consolidado de Proyectos
        <br>
        </div>
        <div class="col-lg-12" style="text-align:center">
            (Proyectos actualizados al 31/10/2019)
        </div>
        
            <div class="row">
                <div class="col-6">
                    <table class="table table-sm table-detail">
                        <tr>
                            <th class="text-center bold" width="5%">#</th>
                            <th class="text-center bold" width="20%">NOMBRE</th>
                            <th class="text-center bold" width="8%">ENTID.</th>
                            <th class="text-center bold" width="8%">PROYEC.</th>
                            <th class="text-center bold" width="15%">MONTO</th>
                            <th class="text-center bold" width="15%">CIPRL</th>
                        </tr>
                            <?php  $j=0;
                            $cont1=0;
                            $cont2=0;
                            $cont3=0;
                            $cont4=0;
                            $cont5=0;
                            foreach ($map as $key){  $j++ ;
                            $cont1+=$key->cantidad;
                            $cont2+=$key->proyecto;
                            $cont3+=$key->monto;
                            $cont5+=$key->ciprl;
                            ?>
                        <tr>
                            <td class="text-center" ><?php    echo ($j)?></td>
                            <td class="text-center" >
                                <a class="lnkAmpliar5" data-event="lnkProvXrutas_<?php echo ($key->id)?>" href="#" onclick="App.events(this); return false;">
                                    <?php echo (strtoupper($key->nom))?> 
                                </a>
                            </td>
                            <td class="text-right" > <?php echo number_format($key->cantidad)?></td>
                            <td class="text-right" ><?php  echo number_format($key->proyecto)?></td>
                            <td class="text-right" ><?php  echo number_format($key->monto)?></td>  
                            <td class="text-right" ><?php  echo number_format($key->ciprl)?></td>
                        </tr>   
                       
                            <?php }  ?>
                        <tr>
                        
                            <th class="text-center bold" width="5%"></th>
                            <th class="text-center bold " width="20%">Total</th>
                            <th class="text-right bold" width="8%"><?php echo number_format($cont1)?></th>
                            <th class="text-right bold" width="8%"><?php echo number_format($cont2)?></th>
                            <th class="text-right bold" width="15%"><?php echo number_format($cont3)?></th>
                            <th class="text-right bold" width="15%"><?php echo number_format($cont5)?></th>
                        </tr>
                    </table>
                </div>
                <div class="col-6">
                    <div id="chartTablaproyectos" style="height: 250px; width: 100%"></div>
                </div>
                    
            </div> 
            <div class="scrollable" style="overflow-y: auto;max-height: 400px;">
                        <div id="idconsolidadoProyectos" style="display: none;">  
                        </div>
            </div>
    </div>
    
</div>
     

 
