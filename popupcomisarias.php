
<?php
			$data = json_decode($_GET['data']);	
			
?>
	<div class="card card-outline-info">
		<div class="card-header">
			<div class="row">
				<div class="col-lg-9">
					<h6 class="m-b-0 text-white"><?php echo (($data->comisaria));?></h6>				
				</div>
				<div class="col-lg-3 text-right">
					<a href="#" id="lnkCerrar" onclick="App.events(this); return false;"><i class="fa fa-times fa-white"></i></a>
				</div>
			</div>
		</div>
		<div class="card-body">				
			<div class="row">
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Región</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->nomdepto));?>	" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Provincia</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->nomprov)); ?>" readonly>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group not-bottom">
						<label>Distrito</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->nomdist)); ?>" readonly>
					</div>
				</div>
			</div>
							
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group not-bottom">
						<label>División Policial</label>
						<input type="text" class="form-control form-control-sm text-left" value="<?php echo ucwords(strtolower($data->divpol)); ?>" readonly>
					</div>
				</div>
			</div>
		</div>
	</div>						
