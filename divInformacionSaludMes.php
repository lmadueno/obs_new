<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
    if($data->tipo==2){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesMinsa','tipo'=> $data->tipo,'departamento'=> $data->departamento]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'departamento'=> $data->departamento]);

    }else if($data->tipo==4){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesMinsa','tipo'=> $data->tipo,'departamento'=> $data->departamento,'provincia'=> $data->provincia]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'provincia'=> $data->provincia]);

    }else if($data->tipo==6){
        $pipxUbigeoDet = dropDownList((object) ['method' => 'SludxMesesMinsa','tipo'=> $data->tipo,'departamento'=> $data->departamento,'provincia'=> $data->provincia,'distrito'=> $data->distrito]);
        $titulo = dropDownList((object) ['method' => 'tituloSalud','tipo'=> $data->tipo,'distrito'=> $data->distrito]);

    }
   
?>

<div class="row">
    <div class="col-lg-12"> 
        <div class="card">
            <div class="card-header card-special text-center font-weight-bold">
                Informacion y estadisticas por Mes de <?php echo $titulo{0}->titulo;?> COVID-19
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="chartMes1"  style="min-width: 310px; height: 320px; margin: 0 auto"></div>	
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header card-special text-center font-weight-bold">
                                        Consolidado anual por Mes de la <?php echo $titulo{0}->titulo;?> COVID-19
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-detail">
                                    <thead>
                                        <tr> 
                                            <th class="text-center bold" width="4%">#</th>
                                            <th class="text-center bold" >Mes</th>   
                                            <th class="text-center bold">Fallecidos</th>   
                                            <th class="text-center bold">Fallecidos/Pob.</th>  
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $i=0;   
                                        $Stotal=0 ;	
                                        $Stotal1=0 ;	
                                        foreach ($pipxUbigeoDet as $item){
                                            $i++;  
                                            $Stotal+=$item->fallecidos ;	
                                            $Stotal1+=round(($item->fallecidos/$item->poblacion)*1000,2) ;
                                         												
                                        ?> 
                                            <tr>	
                                                <td  class="text-center"><?php echo $i;?></td>
                                                <td  class="text-center"><?php echo $item->mes;?></td>
                                                <td  class="text-center"><?php echo $item->fallecidos;?></td>
                                                <td  class="text-center"><?php echo round(($item->fallecidos/$item->poblacion)*1000,2);?></td>
                                         </tr>
                                        <?php } ?>
                                            <tr>	
                                                <td  class="text-center"></td>
                                                <td  class="text-center">Total</td>
                                                <td  class="text-center"><?php echo $Stotal;?></td>
                                                <td  class="text-center"><?php echo round($Stotal1,2);?></td>
                                            </tr>
                                    </tbody>
                                </table>
                                <p style="text-align:center">Solo se muestran los meses con fallecidos</p>
                            </div> 
                        </div>   
                    </div>
                </div>




            </div>
        </div>
    </div>
</div>
