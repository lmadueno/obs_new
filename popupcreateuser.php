
<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<div class="container"  width= "100%"!important  height="100%"!important>
    <div class="card card-outline-info" >
        <div class="card-header">
			<div class="row">
				<div class="col-lg-11">
					<h6 class="m-b-0 text-white">ADMINISTRACIÓN DE USUARIOS</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOG" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarDIALOG" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
			</div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        <div class="row">
                            <div class="col-11">Lista de Usuarios</div>
                            <div class="col-lg-1" style="text-align:right !important">
                                <a class="lnkAmpliar" data-event="ampliartablauser"  href="#" onclick="App.events(this); return false;" title="Agregar Nuevo Usuario">
                                     <i class="fas fa-plus"></i>
                                </a>            
                            </div>
                           
                        </div>
                    </div>
                </div>
                <div class="col-12" style="padding-top:1em">
                    <table class="table table-sm table-detail" data-target="ampliartablauser" style="display: none;">
                        <tr>
                            <td class="text-center bold"  width="15%" >Documento</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Documento de Identidad ..." id="txtnro">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Apellido Paterno</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Apellido Paterno ..." id="txtapepat">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Apellido Materno</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Apellido Materno ..." id="txtapemat">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Nombres</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Nombres ..." id="txtnombres">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Correo Electronico</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Correo Electronico ..." id="txtcorreo">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Empresa</td>
							<td class="text-center " width="45%">
                                <input type="text" class="form-control form-control-sm" placeholder="Empresa ..." id="txtempresa">
                            </td>   																						
                        </tr>
                        <tr>
                            <td class="text-center bold"  width="15%" >Rol</td>
							<td class="text-center " width="45%">
                                <select class="form-control form-control-sm" id="ddlrol">
                                    <option value="0" >Seleccione un rol</option>
                                    <option value="1" >Administrador</option>
									<option value="2" >Basico</option>
									<option value="3" >Avanzado</option>
                                </select>
                            </td>   																						
                        </tr>
                        <tr >
                            <td class="text-center" colspan="2">
                                <button type="button" id="agregarUsuario" onclick="App.events(this); return false;" title="Agregar Usuario" class="btn btn-primary btn-sm">
                                    <i class="fas fa-save"></i>&nbsp;&nbsp;Agregar
                                </button>
                            </td>																						
                        </tr>
                    </table>
                </div>
                <div class="col-12" >
                    <div id="divtablauser"></div>
                </div>
            </div>
        </div>
    </div>
    
    
</div>