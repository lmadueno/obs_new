<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>


<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-10">
                    <img src="assets/app/img/logo_lateral.png" alt="" width="185">
                </div>
                <div class="col-lg-2" style="text-align:right">
                    <a href="#" class="closeSidebar" onclick="App.events(this);" >
                        <img src="assets/app/img/left.jpg" alt="" width="30" height="30" style="margin-top:1em" class="float-right">
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h6 class="m-b-0 text-white">BUSQUEDA RAPIDA DE PROYECTOS Y ENTIDADES</h6>		
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Tipo</label>
								<select id="ddlTipoBusq" title="Busqueda" class="form-control form-control-sm">
									<option value="1">Ubigeo</option>		
									<option value="2">Codigo Unico</option>	
									<option value="3">Descripcion</option>							
								</select>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
                            <input type="text" id="txtProyecto" class="form-control form-control-sm "  >
							</div>
						</div>
                        <div class="col-lg-12 text-center">
							<button type="button" id="searchbox" class="btn btn-sm btn-info waves-effect waves-light" >
								<i class="fa fa-check"></i>
								Buscar
							</button>
							<button type="button" id="clearbox" class="btn btn-sm btn-danger waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-close"></i>
								Limpiar
							</button>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divTablebqdProycts">
	</div>
</div>