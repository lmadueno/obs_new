<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>

<div class="col-lg-12" style="padding-top:1em;">
    <div class="card card-outline-info">
        <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
            Proyectos Adjudicados y concluidos
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <div id="chart_adjudicados">
                      
                    </div>
                </div>
                <div class="col-lg-6">
            <table class="table table-hover table-sm" data-target="datos" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center bold">#</th>
                                    <th scope="col" class="text-center bold">Empresa</th>
                                    <th scope="col" class="text-center bold">#Rank.</th>
                                    <th scope="col" class="text-center bold">M.Soles 2021</th>
                                    <th scope="col" class="text-center bold">#Rank.</th>
                                    <th scope="col" class="text-center bold">M.Soles 2009-2021 </th>
                                </tr>
                            </thead>
                            <tbody id="proyectos_adjudicados" style="text-align: center;">
                                <?php
                                $i = 0;
                                $tmonto2021 = 0;
                                
                                $pipxUbigeoDet = dropDownList((object) ['method' => 'tabla_proyectos_adjudicados']);
                                // echo json_encode($pipxUbigeoDet);
                                foreach ($pipxUbigeoDet as $item) {
                                    $i++;
                                    // $c_ciiu = $c_ciiu + intVal($item->departamento);
                                    // $cantidad = $cantidad + intVal($item->cantidad);
                                    // $aa2020 = $aa2020 + intVal($item->a2020);
                                    // $aa2019 = $aa2019 + intVal($item->a2019);
                                    // $aa2018 = $aa2018 + intVal($item->a2018);
                                    $tmonto2021 = $tmonto2021 + intVal($item->monto2021);
                                ?>
                                    <tr>
                                        <?php if (true) { ?>

                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td class="text-center"><?php echo ($item->empresa); ?></td>
                                            <td class="text-center"><?php echo ($item->posi) ?></td>
                                            <td class="text-center"><?php echo ($item->monto2021) ?></td>
                                            <td class="text-center"><?php echo ($item->posicion) ?></td>
                                            <td class="text-center"><?php echo ($item->monto) ?></td>
                                        <?php } ?>
                                    </tr>

                                <?php } ?>
                                <tr>
                                    <td scope="row" class="text-center"></td>
                                    <td scope="row" class="text-center">Total Acumulado</td>
                                    <td scope="row" class="text-center"></td>
                                    <td scope="row" class="text-center"><?php echo ($tmonto2021) ?></td>
                                   
                                </tr>
                            </tbody>
                        </table>
            </div>
            </div>
           
       
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-info">
                    <div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
                        Tabla de Proyectos Adjudicados
                    </div>
                    <div style="height: 500px; overflow: auto;">

                        <table class="table table-hover table-sm" data-target="datos" width="100%">
                            <thead>
                                <tr>
                                    <th scope="col" class="text-center bold">#</th>
                                    <th scope="col" class="text-center bold">Codigo Unico/SNIP</th>
                                    <th scope="col" class="text-center bold">Departamento</th>
                                    <th scope="col" class="text-center bold">Provincia</th>
                                    <th scope="col" class="text-center bold">Distrito</th>
                                    <th scope="col" class="text-center bold">Entidad Publica </th>
                                    <th scope="col" class="text-center bold">Empresa </th>
                                    <th scope="col" class="text-center bold">Fecha Buena Pro</th>
                                    <th scope="col" class="text-center bold">Estado</th>
                                    <th scope="col" class="text-center bold">Nombre del Proyecto</th>
                                    <th scope="col" class="text-center bold">Sector</th>
                                    <th scope="col" class="text-center bold">Monto de Inv.</th>
                                    <th scope="col" class="text-center bold">Población Benef.</th>
                                </tr>
                            </thead>
                            <tbody id="proyectos_adjudicados" style="text-align: center;">
                                <?php
                                $i = 0;
                                $cantidad = 0;
                                $c_ciiu = 0;
                                $aa2020 = 0;
                                $aa2019 = 0;
                                $aa2018 = 0;
                                $aabz = 0;
                                $pipxUbigeoDet = dropDownList((object) ['method' => 'proyectos_adjudicados']);
                                // echo json_encode($pipxUbigeoDet);
                                foreach ($pipxUbigeoDet as $item) {
                                    $i++;
                                    // $c_ciiu = $c_ciiu + intVal($item->departamento);
                                    // $cantidad = $cantidad + intVal($item->cantidad);
                                    // $aa2020 = $aa2020 + intVal($item->a2020);
                                    // $aa2019 = $aa2019 + intVal($item->a2019);
                                    // $aa2018 = $aa2018 + intVal($item->a2018);
                                    // $aabz = $aabz + intVal($item->sum_monto_oxi);
                                ?>
                                    <tr>
                                        <?php if (true) { ?>

                                            <td class="text-center"><?php echo $i; ?></td>
                                            <!-- <td class="text-center"><?php echo ($item->codigo); ?></td> -->
                                            <td class="text-center"><a target="_blank" href="https://ofi5.mef.gob.pe/invierte/ejecucion/traeListaEjecucionSimplePublica/<?php echo ($item->codigo)?>"><?php echo ($item->codigo); ?></a></td>
                                            <td class="text-center"><?php echo ($item->departamento) ?></td>
                                            <td class="text-center"><?php echo ($item->provincia) ?></td>
                                            <td class="text-center"><?php echo ($item->distrito) ?></td>
                                            <td class="text-center"><?php echo ($item->entidad_publica) ?></td>
                                            <td class="text-center"><?php echo ($item->empresa) ?></td>
                                            <td class="text-center"><?php echo ($item->fecha_buena_pro) ?></td>
                                            <td class="text-center"><?php echo ($item->estado) ?></td>
                                            <td class="text-center"><?php echo ($item->nombre_del_proyecto_de_inversion) ?></td>
                                            <td class="text-center"><?php echo ($item->sector) ?></td>
                                            <td class="text-center"><?php echo $item->monto_de_inversion_millones ?></td>
                                            <td class="text-center"><?php echo $item->poblacion_beneficiada ?></td>
                                        <?php } ?>
                                    </tr>

                                <?php } ?>
                                <tr>
                                    <!-- <td scope="row" class="text-center"></td>
                                    <td scope="row" class="text-center">Total Acumulado</td>
                                    <td scope="row" class="text-center"><?php echo number_format($c_ciiu) ?></td>
                                    <td scope="row" class="text-center"><?php echo number_format($cantidad) ?></td>
                                    <td scope="row" class="text-center"><?php echo number_format($aa2020) ?></td>
                                    <td scope="row" class="text-center"><?php echo number_format($aa2019) ?></td>
                                    <td scope="row" class="text-center"><?php echo number_format($aa2018) ?></td>
                                    <td scope="row" class="text-center"><?php echo number_format($aabz) ?></td> -->

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      
        </div>
    </div>
</div>
</div>