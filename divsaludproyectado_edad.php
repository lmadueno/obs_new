<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);


?>


<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				% Total de Rango de edades por población
			</div>
			<br>
			<div class="row">
				<div class="col-lg-7">
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										<label style="padding-left: 10px;">Fallecidos población y rango de edad por Departamento</label>
										<select style="padding-left: 10px;" class="form-control input-sm ddldepa" id="ddldepa" onchange="App.events(this); return false;">
											<option value="25"><b>NACIONAL</b></option>
											<option value="0"><b>AMAZONAS</b></option>
											<option value="1"><b>ANCASH</b></option>
											<option value="2"><b>APURIMAC</b></option>
											<option value="3"><b>AREQUIPA</b></option>
											<option value="4"><b>AYACUCHO</b></option>
											<option value="5"><b>CAJAMARCA</b></option>
											<option value="6"><b>CALLAO</b></option>
											<option value="7"><b>CUSCO</b></option>
											<option value="8"><b>HUANCAVELICA</b></option>
											<option value="9"><b>HUANUCO</b></option>
											<option value="10"><b>ICA</b></option>
											<option value="11"><b>JUNIN</b></option>
											<option value="12"><b>LA LIBERTAD</b></option>
											<option value="13"><b>LAMBAYEQUE</b></option>
											<option value="14"><b>LIMA</b></option>
											<option value="15"><b>LORETO</b></option>
											<option value="16"><b>MADRE DE DIOS</b></option>
											<option value="17"><b>MOQUEGUA</b></option>
											<option value="18"><b>PASCO</b></option>
											<option value="19"><b>PIURA</b></option>
											<option value="20"><b>PUNO</b></option>
											<option value="21"><b>SAN MARTIN</b></option>
											<option value="22"><b>TACNA</b></option>
											<option value="23"><b>TUMBES</b></option>
											<option value="24"><b>UCAYALI</b></option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							% Población (Hombres y mujeres) segun rango de edades.

						</div>
						<div id='chartedad_11'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							% Fallecidos SINADEF (Hombres y mujeres) por rango de edades.
						</div>
						<div id='chartedad_22'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							% Fallecidos COVID-19 (Hombres y mujeres) por rango de edades.
						</div>
						<div id='chartedad_33'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos1" style="color: #1976D2; !important;" id="datos1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Tabla de datos %poblacion, sinadef y COVID-19</b></a>

					<div class="row">
						<div class="col-lg-12">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos %poblacion, sinadef y COVID-19
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%" style="background:#ddebf8">#</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">Rango de edad</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">% Total Hombres</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">% Total Mujeres</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">% Total </th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">SINADEF</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">% SINADEF</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">COVID-19</th>
											<th scope="col" class="text-center bold" style="background:#ddebf8">% COVID-19</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$total2 = 0;
										$total3 = 0;
										$covid = 0;
										$sinadef = 0;
										$s = array();
										$c = array();
										$departamento = ['AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI'];

										$pipxUbigeoDet = dropDownList((object) ['method' => 'filtro_edad_tabla', 'dpto' => "AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI"]);
										$tabla_sinadef_covid = dropDownList((object) ['method' => 'filtro_edad_tabla_covid', 'dpto' => "AMAZONAS', 'ANCASH', 'APURIMAC', 'AREQUIPA', 'AYACUCHO', 'CAJAMARCA', 'CALLAO', 'CUSCO', 'HUANCAVELICA', 'HUANUCO', 'ICA', 'JUNIN', 'LA LIBERTAD', 'LAMBAYEQUE', 'LIMA', 'LORETO', 'MADRE DE DIOS', 'MOQUEGUA', 'PASCO', 'PIURA', 'PUNO', 'SAN MARTIN', 'TACNA', 'TUMBES', 'UCAYALI"]);
										// echo json_encode($pipxUbigeoDet);
										$categorias = ['0-5 años', '6-12 años', '13-17 años', '18-24 años', '25-39 años', '40-55 años', '56-65 años', '66-75 años', '76 + años'];
										foreach ($tabla_sinadef_covid as $tcovid) {
											$sinadef = $sinadef + intVal($tcovid->sumh);
											$covid = $covid + intVal($tcovid->summ);
											array_push($s, $tcovid->sumh);
											array_push($c, $tcovid->summ);
										}

										foreach ($pipxUbigeoDet as $item) {
											$total = $total + intval($item->h);
											$total2 = $total2 + intval($item->m);
											$total3 = $total3 + intval($item->sumt);
										}
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											// $sTotal = $sTotal + intval($item->h);
											// $stotal = $stotal + intval($item->m);
											// $totals = $total + intval($item->sum);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo $categorias[$i - 1] ?></td>
												<td scope="row" class="text-center"><?php echo (round(($item->h / $total), 2) * 100) . '%' ?></td>
												<td scope="row" class="text-center"><?php echo (round(($item->m / $total2), 2) * 100) . '%' ?></td>
												<td scope="row" class="text-center"><?php echo (round(($item->sumt / $total3), 2) * 100) . '%' ?></td>
												<td scope="row" class="text-center"><?php echo  intval($s[$i - 1]) ?></td>
												<td scope="row" class="text-center"><?php echo (round(($s[$i - 1] / $sinadef), 2) * 100) . '%' ?></td>
												<td scope="row" class="text-center"><?php echo  intVal($c[$i - 1]) ?></td>
												<td scope="row" class="text-center"><?php echo (round(($c[$i - 1] / $covid), 2) * 100) . '%' ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center">100%</td>
											<td scope="row" class="text-center">100%</td>
											<td scope="row" class="text-center">100%</td>
											<td scope="row" class="text-center"><?php echo number_format($sinadef) ?></td>
											<td scope="row" class="text-center">100%</td>
											<td scope="row" class="text-center"><?php echo number_format($covid) ?></td>
											<td scope="row" class="text-center">100%</td>


										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
			<br>
			<!-- <div class="row">
				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Ratio de fallecidos por 2020 / 2019, según rango de edad
						</div>
						<div id='chartedad_calor2020'></div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Ratio de fallecidos por 2021 / 2019, según rango de edad
						</div>
						<div id='chartedad_calor2021'></div>
					</div>
				</div>
			</div> -->
			<br>
			<div class="card-body">

				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Tasa de fallecidos mensual por cada 100 mil habitantes, por rango de edad años 2020 y 2021* - Perú </div>

						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								<div class="row">
									<div class="col-lg-12">
										<div class="panel panel-default">
											<div class="panel-body">
												<div class="row">
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Departamentos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos" id="ddlDepartamentos" name="ddlDepartamentos" onchange="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Provincias</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias" id="ddlProvincias" name="ddlProvincias" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Distritos</label>
															<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos" id="ddlDistritos" name="ddlDistritos" onclick="App.events(this); return false;">
																<option value="0">[Seleccione]</option>
															</select>
														</div>
													</div>
													<div class="col-lg-3">
														<div class="form-group">
															<label style="padding-left: 10px;">Accion</label>
															<button type="button" class="form-control input-sm btn btn-primary filtroedad" id="filtroedad" name="filtroedad" onclick="App.events(this); return false;">Filtrar</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div id='chartedad_calor20_21'></div>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-body">
								<div id='chartedad_calor_regiones' style="min-width: 310px; height: 900px; margin: 0 auto"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>


			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header" style="background:#304e9c">
							<h4 class="m-b-0 text-white text-center"> Sección de Pronostico Mensual, semanal y promedio diario semanal x Edad SINADEF</h4>
						</div>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Mensual
									</div>
									<div id='chart_sinadef_edad_mensual' style=" height: 600px"></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Semanal
									</div>
									<div id='chart_sinadef_edad_semanal' style=" height: 600px"></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Diario
									</div>
									<div id='chart_sinadef_edad_diario' style=" height: 600px"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<br>