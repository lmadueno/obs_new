<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>
<table class="table table-sm table-detail" width="100%">
								<thead>
                                    <tr> 
										<th class="text-center bold">#</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Documento</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Nombre del Proyecto</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">CUI</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Costo Total</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Avance Ejecución</th>
										<th class="text-center bold" style="padding-bottom: 0.8em;">Monto Pendiente</th>
                                    </tr>
                                </thead>
								<tbody>
								<?php 
									$i=0;   
								
									$s2019= 0;	
									$s2020= 0 ;	 
									$stotal= 0 ;     
                                    $pipxUbigeoDet = dropDownList((object) ['method' => 'macro_mef_sub_proyecto','entidad'=>$data->entidad,'gobierno'=>$data->gobierno,'region'=>$data->region]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
								
										$s2019=   $s2019+$item->total ;	
										$s2020=   $s2020+$item->diferencia ;	
										$stotal=   $stotal+$item->resto ;												
                                ?>	
									<tr>
                                        <td  class="text-center"><?php echo $i;?></td>
										<td class="text-center" ><?php echo ($item->documento_p)?></td>	
										<td class="text-center" ><?php echo ($item->nombre_p)?></td>											
										<td class="text-center" ><a href="https://ofi5.mef.gob.pe/invierte/ejecucion/traeListaEjecucionSimplePublica/<?php echo $item->cui;?>" target='_blank'><?php echo ($item->cui)?></a> </td>		
                                        <td class="text-center" ><?php echo number_format($item->total)?></td>		
										<td class="text-center" ><?php echo number_format($item->diferencia)?></td>		
										<td class="text-center" ><?php echo number_format($item->resto)?></td>	
                                    </tr>
								<?php } ?>
									<tr>
                                       
                                        <td ></td>
										<td >Total</td>
										<td ></td>
                                        <td class="text-center" ></td>												
                                        <td class="text-center"><?php echo number_format($s2019)?></td>
                                        <td class="text-center"><?php echo number_format($s2020)?></td>
										<td class="text-center"><?php echo number_format($stotal)?></td>

                                    </tr>
								</tbody>
							</table>