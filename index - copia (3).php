<?php 
    session_start();
    if (!isset($_SESSION['sesion'])) {
        header('Location: ./login.html');
    }    
?> 

<!DOCTYPE html>
<html lang="ja">
	<head>  
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<title>Observatorio de Proyectos</title>
		<link rel="icon" type="image/png" href="assets/app/img/icon.png"/>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" href="assets/base/js/token-input.css">
		<link rel="stylesheet" href="assets/base/css/bootstrap.min.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
		 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
   integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
   crossorigin=""/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.2.3/leaflet.draw.css" />
		<link rel="stylesheet" href="assets/slide/L.Control.SlideMenu.css">
		<link rel="stylesheet" href="assets/rangeslider/ion.rangeSlider.css">		
		<link rel="stylesheet" href="assets/panelayers/css/leaflet-panel-layers.css">
		<link rel="stylesheet" href="assets/toastr/toastr.css">
     	<!-- Estiilos de los Alertasd -->
		<link rel="stylesheet" href="assets/dialog/Leaflet.Dialog.css"/>		
		<link rel="stylesheet" href="assets/select/bootstrap-select.min.css">
		<link rel="stylesheet" href="assets/searchbox/searchbox.min.css">
		<link rel="stylesheet" href="assets/iconlayers/iconLayers.css">
		<link rel="stylesheet" href="assets/extramarkers/css/leaflet.extra-markers.min.css" />
		<link rel="stylesheet" href="assets/markercluster/css/MarkerCluster.css" />
		<link rel="stylesheet" href="assets/markercluster/css/MarkerCluster.Default.css" />
		<link rel="stylesheet" href="assets/attribution/leaflet-control-condended-attribution.css">
		<link rel="stylesheet" href="assets/select/bootstrap-select.min.css">
		<link rel="stylesheet" href="assets/datepicker/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="assets/sidebar/sidebar.css">
		<link rel="stylesheet" href="assets/highcharts/highcharts.css">
		<link rel="stylesheet" href="assets/datatables/datatables.min.css"/>
		<link rel="stylesheet" href="assets/app/css/observatorio.css">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.css">
        <style>
			#wmsContainer {
				cursor: pointer;
			}
			.my-custom-icon {
  background: rgba(256, 256, 256, .2);
  border: 1px solid white;
  box-shadow: 0 0 0 1px rgba(0, 0, 0, 1);
}
.leaflet-control-dialog-grabber {
    position: fixed;
}
			
			.leaflet-top .leaflet-control {
   				 margin-top: 0px;
			}
			.info{
				cursor: pointer;
				color: #1d3e7b;
			}
			.infoOleoducto{
				cursor: pointer;
				color: #8ba3d1;
			}
			.leaflet-panel-layers-grouplabel{
				display: block;
			}
			.leaflet-panel-layers-title{
				display: block;
			}
			.leaflet-panel-layers-item.hide{
				display: none;
			}
			.leaflet-control {
    			margin-left: 17px !important;
			}	
					
			.card-white{
				background: #fff !important;
				border-color: #f7f7f7 !important;
			}
			.card-special{
				background: #ddebf8 !important;
				border-color: #f7f7f7 !important;
			}
			.table-sm td, .table-sm th{
				vertical-align: middle;
			}
			.card-footer {
				padding: 0.30rem 1.25rem !important;
			}
			#modal_lg .input-group-text{
				font-size: 12px;
				width: 150px;
			}
			#modal_lg .form-control{
				font-size: 12px;				
			}
			div.token-input-dropdown {           
   				z-index: 9999 !important;
			}
			 .leaflet-left{
				padding-top:0rem !important;
			}
			.legend {
  				
 				background: white;
 				background: rgba(255, 255, 255, 0.4);
 			    color: #555;
			}
		
			
			.legend .leaflet-control{
				width: 147px !important;
			}
		   .legend i {
 			   width: 18px;
  			   height: 18px;
  			   float: left;
  			   margin: 0 8px 0 0;
  			   opacity: 0.7;
			}
        
		.legend i.icon {
  			  background-size: 18px;
  			  background-color: rgba(255, 255, 255, 1);
		}
		.legend .btn-hove:hover{
			color:black;
		}
		.highcharts-credits{
			display: none !important;
		}
		 .highcharts-color-1{
			fill:#C0504D !important;

		}
		 .highcharts-color-0{
			fill: #4F81BD  !important;	
		}
		.highcharts-legend-item text {
			font: 10px/1.5 "Helvetica Neue", Arial, Helvetica, sans-serif !important;
		}
		.highcharts-title{
			font: 14px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif !important;
			font-weight: bold !important;			
		}
		.ui-autocomplete .ui-front{
			z-index: 10000 !important;
			font: 14px/2.5 "Helvetica Neue", Arial, Helvetica, sans-serif !important;
			font-weight: bold !important;	
		}
		.leaflet-panel-layers-list .leaflet-control-layers-scrollbar{
			width: 150px !important;
			display: none !important;
		}
		.leaflet-panel-layers-list {
			
			display: none !important;
		}
		.leaflet-control-layers-scrollbar{
			
			display: none !important;
		}
		.mostrar{
			display: flex !important;
		}
		.mostrar1{
			display: none !important;
		}
		.highcharts-menu{
			height: 200px !important;
			overflow: scroll  !important;
		} 
		
		#sidebar{
			margin-bottom : 300px !important;
			margin-top : 140px !important;
			margin-left : -1px !important;
	
		}
		.leaflet-draw.leaflet-control{
			margin-left : 10px !important;
		}
		.leaflet-draw.leaflet-control{
			margin-bottom : 2em !important;
		}
		
		.legendSalud {
			padding: 6px 8px;
			background: white;
			background: rgba(255, 255, 255, 0.4);
			/*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
			/*border-radius: 5px;*/
			line-height: 24px;
			color: #555;
			
		}
		.legendSalud h4 {
			text-align: center;
			font-size: 12px;
			margin: 2px 12px 8px;
			color: #777;
		}

		.legendSalud span {
			position: relative;
			bottom: 3px;
		}

		.legendSalud i {
			width: 18px;
			height: 18px;
			float: left;
			margin: 0 8px 0 0;
			opacity: 0.7;
		}

		.legendSalud i.icon {
			background-size: 18px;
			background-color: rgba(255, 255, 255, 1);
		}
		.legendSaludSinadef {
			padding: 6px 8px;
			
			background: white;
			background: rgba(255, 255, 255, 0.4);
			/*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
			/*border-radius: 5px;*/
			line-height: 24px;
			color: #555;
			
		}
		.legendSaludSinadef h4 {
			text-align: center;
			font-size: 12px;
			margin: 2px 12px 8px;
			color: #777;
		}

		.legendSaludSinadef span {
			position: relative;
			bottom: 3px;
		}

		.legendSaludSinadef i {
			width: 18px;
			height: 18px;
			float: left;
			margin: 0 8px 0 0;
			opacity: 0.7;
		}

		.legendSaludSinadef i.icon {
			background-size: 18px;
			background-color: rgba(255, 255, 255, 1);
		}
		
		.zoomstyle {
			padding-right:12em;
		}
		.zoomstyleremove {
			padding-right:0em;
		}
		.leaflet-top.leaflet-left{
			margin-left:-50px;
		}
		.leaflet-draw.leaflet-control{
			margin-left:50px;
		}
		.legendPresidencial {
			padding: 6px 8px;
			
			background: white;
			background: rgba(255, 255, 255, 0.4);
			/*box-shadow: 0 0 15px rgba(0, 0, 0, 0.2);*/
			/*border-radius: 5px;*/
			line-height: 24px;
			color: #555;
			
		}
		.legendPresidencial h4 {
			text-align: center;
			font-size: 12px;
			margin: 2px 12px 8px;
			color: #777;
		}

		.legendPresidencial span {
			position: relative;
			bottom: 3px;
		}

		.legendPresidencial i {
			width: 18px;
			height: 18px;
			float: left;
			margin: 0 8px 0 0;
			opacity: 0.7;
		}

		.legendPresidencial i.icon {
			background-size: 18px;
			background-color: rgba(255, 255, 255, 1);
		}
		#boxcontainer {
			margin-top:20px;
			width:480px;
		}
		#selectSearch {
			margin-left: -40px;
			
		}
		.panel {
			margin-top:85px;
		}
		#map > div.leaflet-control-container > div.leaflet-top.leaflet-right > div.leaflet-panel-layers.expanded.leaflet-control.leaflet-control-layers-expanded > form > div.leaflet-panel-layers-overlays > div:nth-child(6),
		#map > div.leaflet-control-container > div.leaflet-top.leaflet-right > div.leaflet-panel-layers.expanded.leaflet-control.leaflet-control-layers-expanded > form > div.leaflet-panel-layers-overlays > div:nth-child(7),
		#map > div.leaflet-control-container > div.leaflet-top.leaflet-right > div.leaflet-panel-layers.expanded.leaflet-control.leaflet-control-layers-expanded > form > div.leaflet-panel-layers-overlays > div:nth-child(8){
			display: none;
		}
		</style>
	</head>
	<body>	
	
		<input type="hidden" id="txttermoInfografia" value="1">
		<input type="hidden" id="txttermoInfoUbigeo">
		<input type="hidden" id="txtTermoUbigeo">
		<input type="hidden" id="txtiduser" value="<?php echo $_SESSION['sesion']->id ?>">
		<input type="hidden" id="txtidRol" value="<?php echo $_SESSION['sesion']->rol ?>">		
		<input type="hidden" id="txtTipoFiltro" value="proyecto">
		<input type="hidden" id="txtCorredor" >		
		<div id="map" style="position: absolute; width: 100%; height: 100%;"></div>
        <div class="modal fade" id="modal_lg">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Información del proyecto</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						
					</div>
				</div>
			</div>
		</div>
		<div style="display: none;">		
			<select id="ddlFases2" class="form-control form-control-sm">
				<option value="1">Idea</option>
				<option value="2">Formulación</option>										
				<option value="3">Evaluación</option>
				<option value="4">Perfil Viable</option>
				<option value="5">No Viable</option>
				<option value="6">Exp. Técnico</option>
				<option value="7">Licitación</option>
				<option value="8">Adjudicado</option>
				<option value="9">Duplicidad</option>
				<option value="10">No es proyecto</option>
			</select>
			<select id="ddlNivel2" class="form-control form-control-sm">
				<option selected="" value="1">Gobierno Local</option>
				<option value="2">Gobierno Regional</option>
				<option value="3">Gobierno Nacional</option>
				<option value="4">Universidad</option>
			</select>
		</div>
		<script type="text/javascript" src="assets/base/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="assets/base/js/jquery-ui.min.js"></script> 
		<script type="text/javascript" src="assets/base/js/jquery.tokeninput.js"></script>

		<script type="text/javascript" src="assets/base/js/popper.min.js"></script>
		<script type="text/javascript" src="assets/base/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/base/js/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="assets/leaflet/leaflet.js"></script>
		<script type="text/javascript" src="assets/base/js/leaflet-heat.js"></script>
		<script type="text/javascript" src="assets/base/js/leaflet-heatmap.js"></script>
		<script type="text/javascript" src="assets/base/js/jquery.ui.touch-punch.min.js"></script>
		<script type="text/javascript" src="assets/betterwms/L.TileLayer.BetterWMS.js"></script>
		<script type="text/javascript" src="assets/slide/L.Control.SlideMenu.js"></script>
        <script type="text/javascript" src="assets/sidebar/sidebar.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/leaflet-easybutton@2/src/easy-button.js"></script>
		<script src="https://d3js.org/d3.v4.min.js"></script>
		<script src="https://unpkg.com/topojson-client@3"></script>
		<script type="text/javascript" src="assets/rangeslider/ion.rangeSlider.js"></script>	
		
        <script type="text/javascript" src="assets/panelayers/js/leaflet-panel-layers.js"></script>
		
		<script type="text/javascript" src="assets/dialog/Leaflet.Dialog.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/http-client/4.3.1/http-client.min.js"></script>
		
		<script src="assets/toastr/toastr.min.js"></script>
		<script src="assets/draw/src/Leaflet.draw.js"></script>
    	<script src="assets/draw/src/Leaflet.Draw.Event.js"></script>
  

		<script src="assets/draw/src/Toolbar.js"></script>
		<script src="assets/draw/src/Tooltip.js"></script>

		<script src="assets/draw/src/ext/GeometryUtil.js"></script>
		<script src="assets/draw/src/ext/LatLngUtil.js"></script>
		<script src="assets/draw/src/ext/LineUtil.Intersect.js"></script>
		<script src="assets/draw/src/ext/Polygon.Intersect.js"></script>
		<script src="assets/draw/src/ext/Polyline.Intersect.js"></script>
		
		<script src="assets/draw/src/ext/TouchEvents.js"></script>	
		<script src="assets/draw/src/draw/DrawToolbar.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Feature.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.SimpleShape.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Polyline.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Marker.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Circle.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.CircleMarker.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Polygon.js"></script>
		<script src="assets/draw/src/draw/handler/Draw.Rectangle.js"></script>


    <script src="assets/draw/src/edit/EditToolbar.js"></script>
    <script src="assets/draw/src/edit/handler/EditToolbar.Edit.js"></script>
    <script src="assets/draw/src/edit/handler/EditToolbar.Delete.js"></script>

    <script src="assets/draw/src/Control.Draw.js"></script>

    <script src="assets/draw/src/edit/handler/Edit.Poly.js"></script>
    <script src="assets/draw/src/edit/handler/Edit.SimpleShape.js"></script>
    <script src="assets/draw/src/edit/handler/Edit.Rectangle.js"></script>
    <script src="assets/draw/src/edit/handler/Edit.Marker.js"></script>
    <script src="assets/draw/src/edit/handler/Edit.CircleMarker.js"></script>
    <script src="assets/draw/src/edit/handler/Edit.Circle.js"></script>
	<script type="text/javascript" src="assets/searchbox/leaflet.customsearchbox.min.js"></script>
		<script type="text/javascript" src="assets/iconlayers/iconLayers.js"></script>
		<script type="text/javascript" src="assets/extramarkers/js/leaflet.extra-markers.min.js"></script>
		<script type="text/javascript" src="assets/markercluster/js/leaflet.markercluster-src.js"></script>		
		<script type="text/javascript" src="assets/attribution/leaflet-control-condended-attribution.js"></script>
		<script type="text/javascript" src="assets/select/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="assets/select/defaults-es_ES.min.js"></script>
		<script type="text/javascript" src="assets/datepicker/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="assets/datepicker/bootstrap-datepicker.es.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.tablesorter/2.31.2/js/jquery.tablesorter.min.js" integrity="sha256-WX1A5tGpPfZZ48PgoZX2vpOojjCXsytpCvgPcRPnFKU=" crossorigin="anonymous"></script>
		<script src="https://code.highcharts.com/highcharts.js"></script>
		<script src="https://code.highcharts.com/highcharts-more.js"></script>
    	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
    	<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/wordcloud.js"></script>
		<script src="https://code.highcharts.com/modules/series-label.js"></script>
		<script src="https://code.highcharts.com/modules/exporting.js"></script>
		<script src="https://code.highcharts.com/modules/export-data.js"></script>
		<script src="https://code.highcharts.com/modules/data.js"></script>

		<script type="text/javascript" src="assets/datatables/datatables.min.js"></script>
		<script type="text/javascript" src="assets/base/js/sorttable.js"></script>		
		<script type="text/javascript" src="assets/app/js/observatorio.js?v=27"></script>
		<script type="text/javascript" src="assets/app/js/vars.js"></script>
		<script>
			$( document ).ready(function() {
				App.init();	
			});
		</script>
	</body>
</html>