<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>

<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Filtro de proyectos
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="row">
															<div class="col-lg-1">
																<div class="form-group">
																	<label style="padding-left: 10px;">Estado Ejecución</label>
																	<select style="padding-left: 10px;" class="form-control input-sm estado " id="estado" name="estado">
																		<option value="[Seleccione]">[Seleccione]</option>
																		<option value="1">En Ejecución</option>
																		<option value="0">Viables sin Ejecución</option>
																	</select>
																</div>
															</div>

															<div class="col-lg-1">
																<div class="form-group">
																	<label style="padding-left: 10px;">Tipo de formato</label>
																	<select style="padding-left: 10px;" class="form-control input-sm tipo " id="tipo" name="tipo" onchange="App.events(this); return false;">
																		<option value="[Seleccione]">[Seleccione]</option>
																		<option value="0">IOARR</option>
																		<option value="1">PROYECTO</option>
																		<option value="2">PROGRAMA DE INVERSION</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-2">
																<div class="form-group">
																	<label>Funcion</label>
																	<?php $dfuncion = dropDownList((object)['method' => 'funcion']); ?>
																	<select class="form-control input-sm selectpicker dfuncion" id="dfuncion" name="funcion" data-val="0,1" data-size="10" data-key="funcion" data-live-search="true" data-actions-box="true" multiple onchange="App.events(this); return false;">
																		<?php foreach ($dfuncion as $key) { ?>
																			<option value="<?php echo $key->cantidad; ?>"><?php echo $key->etiqueta; ?></option>
																		<?php } ?>

																	</select>
																</div>
															</div>
															<div class="col-lg-2">
																<div class="form-group">
																	<label>Programas</label>
																	<?php $dprograma = dropDownList((object)['method' => 'programa']);
																	?>

																	<select class="form-control input-sm  selectpicker programas" id="programas" name="programas" data-val="0,1" data-size="10" data-key="programas" data-live-search="true" multiple title="Seleccione..." data-actions-box="true" data-size="10" onchange="App.events(this); return false;">
																		<?php
																		foreach ($dprograma as $itemfuncion) {
																		?>
																			<optgroup label="<?php echo $itemfuncion->funcion; ?>">
																				<?php
																				$items  = explode(",", $itemfuncion->programa);
																				foreach ($items as $item) {
																				?>
																					<option value="<?php echo $item; ?>"><?php echo $item; ?></option>
																				<?php } ?>

																			</optgroup>
																		<?php
																		}
																		?>

																	</select>
																</div>
															</div>
															<div class="col-lg-2">
																<div class="form-group">
																	<label style="padding-left: 10px;">Sub Programas</label>
																	<select style="padding-left: 10px;" class="form-control input-sm sprograma selectpicker" data-live-search="true" data-actions-box="false" multiple id="sprograma" name="sprograma" onchange="App.events(this); return false;">
																		<option value="[Seleccione]">[Seleccione]</option>

																	</select>
																</div>
															</div>
															<div class="col-lg-2">
																<div class="form-group">
																	<label>Entidades</label>
																	<select id="select-statew" name="ddlOrigen[]" class=" form-control input-sm selectpicker" data-live-search="true" data-actions-box="false" multiple onchange="App.events(this); return false;">
																		<option value="[Seleccione]">[Seleccione]</option>
																		<option value="SI">Si</option>
																		<option value="NO">no</option>
																	</select>
																</div>
															</div>

															<div class="col-lg-2">
																<div class="form-group">
																	<label style="padding-left: 10px;">Accion</label>
																	<button type="button" class="form-control input-sm btn btn-primary filtroproyecto_general" id="filtroproyecto_general" name="filtroproyecto_general" onclick="App.events(this); return false;">Filtrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-8">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Total financiamiento de Proyectos de Inversión
					</div>
					<div id='chartgeneral_10'></div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Tabla total  de ejecución y financiamiento de Proyectos de Inversión
					</div>
					<table class="table table-hover table-sm" data-target="datos" width="100%">
						<thead>
							<tr>
								<th scope="col" class="text-center bold" width="4%">#</th>
								<th scope="col" class="text-center bold">Gobierno</th>
								<th scope="col" class="text-center bold">Cantidad</th>
								<th scope="col" class="text-center bold">Costo Actualizado</th>
								<th scope="col" class="text-center bold">Devengado Acumulado</th>
								<th scope="col" class="text-center bold">Saldo por ejecutar</th>
								<th scope="col" class="text-center bold">Saldo por financiar</th>
							</tr>
						</thead>
						<tbody id="cuerpoTabla2">
						
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Brecha de ejecución por función Salud, Educación, Seguridad y Transporte, financiamiento 2017 - 2021*
			</div>
			<br>
			<div class="row">
				<div class="col-lg-3">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Función salud 2017 - 2021*
						</div>
						<div id='chartgeneral_11'></div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Función Educación 2017 - 2021*
						</div>
						<div id='chartgeneral_22'></div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Función Seguridad 2017 - 2021*
						</div>
						<div id='chartgeneral_33'></div>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Función Transporte 2017 - 2021*
						</div>
						<div id='chartgeneral_44'></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="text-center col-lg-12">
					<a class="nav-link active vertabla" data-event="datos1" style="color: #1976D2; !important;" id="datos1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha de ejecución por función Salud, Educación, Seguridad y Transporte, financiamiento 2017 - 2021*</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-3">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla Función salud 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody id="fsalud" style="font-size: 12px;">
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Función Educación 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody  id="feducacion" style="font-size: 12px;">
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Función Seguridad 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody>
									</tbody id="fseguridad" style="font-size: 12px;">
								</table>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Función Transporte 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody  id="ftransporte" style="font-size: 12px;">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<br>

<div id="accordion">
	<div class="card">
		<div class="card-header" id="headingOne" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:#000;font-weight: bold; ">
					Listado de proyectos
				</button>
			</h5>
		</div>
		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;display: none;">
								Brecha IOARR por nivel Gobierno 2017-2021*

							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Nacional 2017 - 2021*


										</div>
										<div id='chartbrecha_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Regional 2017 - 2021*

										</div>
										<div id='chartbrecha_2'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Local 2017 - 2021*

										</div>
										<div id='chartbrecha_3'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos" style="color: #1976D2; !important;" id="datos" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha IOARR por nivel Gobierno 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Nacional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GN']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Regional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GR']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Local </div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GL']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>