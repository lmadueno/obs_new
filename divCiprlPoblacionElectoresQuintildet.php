<?php
require 'php/app.php';   
$data = json_decode($_GET['data']);	

?>
<div class="card">

    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="card-header card-special">
                    Detalle de Consolidado
                </div>

                <table class="table table-sm table-detail">
                    <tr>
                        <th class="text-center bold">#</th>
                        <th class="text-center bold">Provincia</th>
                        <th class="text-center bold">Distrito</th>
                        <th class="text-center bold">CIPRL</th>
                        <th class="text-center bold">Poblacion</th>
                        <th class="text-center bold">Electores</th>
                    </tr>
                    <?php 
                        $obj3= dropDownList((object) ['method' => 'quintilCiprlPoblaElect1','quintil'=>$data->quintil,'region'=>$data->region,'tipo'=>$data->tipo]);
                        $ir=0;
                        $tmpCiprl=0;
                        $tmpElectores=0;
                        $tmpPoblacion=0;
                        foreach ($obj3 as $key2){ 
                             $ir++;
                             $tmpCiprl +=$key2->tope_ciprl;
                             $tmpElectores +=$key2->valor;
                             $tmpPoblacion +=$key2->num_electores;
                        ?>
                    <tr>
                        <th class="text-center"><?php echo ($ir)?></th>
                        <th class="text-left"><?php echo ($key2->nom_prov)?></th>
                        <th class="text-left"><?php echo ($key2->nom_dist)?></th>
                        <th class="text-right"><?php echo number_format ($key2->tope_ciprl)?></th>
                        <th class="text-right"><?php echo number_format ($key2->valor)?></th>
                        <th class="text-right"><?php echo number_format($key2->num_electores)?></th>
                    </tr>
                    <?php  } ?>
                    <tr>
                        <th class="text-center"></th>
                        <th class="text-left"><b>Total</b></th>
                        <th class="text-left"></th>
                        <th class="text-right"><?php echo number_format($tmpCiprl)?></th>
                        <th class="text-right"><?php echo number_format($tmpElectores)?></th>
                        <th class="text-right"><?php echo number_format($tmpPoblacion)?></th>
                    </tr>
                </table>
            </div>          
        </div>
    </div>
</div>