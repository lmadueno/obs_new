<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<?php
	require 'php/app.php';
	

		
			$obj    = dropDownList((object) ['method' => 'misDraws']);
?>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-10">
                    <img src="assets/app/img/logo_lateral.png" alt="" width="185">
                </div>
                <div class="col-lg-2" style="text-align:right">
                    <a href="#" class="closeSidebar" onclick="App.events(this);" >
                        <img src="assets/app/img/left.jpg" alt="" width="30" height="30" style="margin-top:1em" class="float-right">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-outline-info">
                <div class="card-header">
                    <h6 class="m-b-0 text-white">Dibujar sobre el mapa</h6>		
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Mis Dibujos</label>
							</div>
						</div>
                        <div class="col-lg-12">
                            <table class="table table-sm table-detail">	
                            <?php 
                                $i=0;
                                foreach ($obj as $item){ 
                                    $i++;?> 
                                <tr>
                                    <td class="text-center" ><?php echo $i?></td>
                                    <td class="text-center" ><?php echo $item->descripcion?></td>
                                    <td class="text-center" ><?php echo $item->tipo?></td>
                                    <td class="text-center" >
                                        <a class="drawObjetoList"  href="#" onclick="App.events(this); return false;" id="<?php echo $item->id.'&'.$item->tipo?>">
                                            <i class="fas <?php echo $item->icono?>"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php }?>  
                            </table>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="divTableDraws">
	</div>
</div>