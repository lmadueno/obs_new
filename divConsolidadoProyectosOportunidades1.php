<?php
require 'php/app.php';

   $data = json_decode($_GET['data']);	
   
?>
<div class="row">
    <div class="col-lg-12">
        <table class="table table-sm table-detail">
            <tr>
                <th class="text-left bold" width="5%">#</th>
                <th class="text-left bold" width="45%">ENTIDAD</th>
                <th class="text-right bold" width="25%">CANTIDAD</th>
                <th class="text-right bold" width="25%">MONTO</th>
            </tr>
            <?php $FunOrg = dropDownList((object) ['method' => 'oportunidadesXFuncionXOrg','tipo'=>$data->tipo1,'codfuncion'=>$data->nivel,'codnivel'=>$data->cod]);?>
            <?php $i=0;
                foreach($FunOrg as  $n2){ $i++;
            ?>
            <tr>
                <td class="text-center"><?php echo($i)?></td>
                <td class="text-lef"> 
                    <a class="lnkAmpliar" data-event="lnkProvXrutas___<?php echo $n2->organizacion?>" href="#" onclick="App.events(this); return false;">
                        <?php echo ($n2->organizacion)?>
                    </a>
                </td>
                <td class="text-right"><?php echo number_format($n2->cantidad) ?></td>
                <td class="text-right"><?php echo number_format($n2->monto) ?></td>
            </tr>
            <tr data-target="lnkProvXrutas___<?php echo $n2->organizacion?>" style="display: none;">
                <td colspan="4">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-left bold" width="5%">#</th>
                                    <th class="text-left bold" width="45%">CODIGO</th>
                                    <th class="text-right bold" width="25%">FECHA DE REGISTRO</th>
                                    <th class="text-right bold" width="25%">MONTO</th>
                                </tr>
                                <?php $FunPro = dropDownList((object) ['method' => 'oportunidadesXFuncionXProyecto','tipo'=>$data->tipo1,'codfuncion'=>$data->nivel,'codnivel'=>$data->cod,'organizacion'=>$n2->organizacion]);?>
                                <?php $j=0;
                                    foreach($FunPro as $n3) { $j++;
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo($j)?> </td>
                                    <td class="text-center"><a href="" class="link_" onclick="App.events(this); return false;" data-event='<?php echo $n3->codigounico ?>'><?php echo $n3->codigounico?></a></td>
                                    <td class="text-right"><?php echo($n3->fecregistro) ?></td>
                                    <td class="text-right"><?php echo number_format($n3->monto) ?></td>
                                </tr>
                                <?php }  ?>   
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
            <?php }  ?>   
        </table>
     </div>
</div>



