<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);
$method1 = '';
$method2 = '';
if ($data->tipo == 'covid') {
	$method1 = 'quintiles_covid';
	$method2 = 'quintiles_covid_det';
} else {
	$method1 = 'quintiles_sinadef';
	$method2 = 'quintiles_sinadef_det';
}
?>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Consolidado anual COVID-19 por Regiones
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Poblacion
					</div>

				</div>
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Fallecidos/Poblacion
					</div>

				</div>
			</div>
			<div class="row">

				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 1]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintl</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<th class="text-center bold">Población</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										$suma5 = $suma5 + $key->poblacion;

									?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil1" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil1Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>

											<td class="text-center">
												<a class="lnkAmpliar" data-event="lnkProvXrutas1_<?php echo $key->quantil ?>" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<td class="text-center"><?php echo number_format($key->poblacion) ?></td>
										</tr>
										<tr data-target="lnkProvXrutas1_<?php echo $key->quantil ?>" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Poblacion</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 1, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion;


																	?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<td class="text-center"><?php echo number_format($key1->poblacion) ?></td>
																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<td class="text-center"><?php echo number_format($suma51) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo number_format($suma5) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map3" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 1]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintil</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<!-- <th class="text-center bold">Pob</th> -->
										<th class="text-center bold">Fall/Pob</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									$sumap = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										// $suma5 = $suma5 + round((($key->fallecidos/$key->poblacion)*1000),2);
										$sumap = $sumap + $key->poblacion;
									?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil1" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil1Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>

											<td class="text-center">
												<a class="lnkAmpliars" data-event="lnkProvXrutas1_<?php echo $key->quantil ?>2" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<!-- <td class="text-center"><?php echo number_format($key->poblacion) ?></td> -->
											<td class="text-center"><?php echo $key->fabpob ?></td>
										</tr>
										<tr data-target="lnkProvXrutas1_<?php echo $key->quantil ?>2" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Fall/Pob</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 1, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	$sumapd = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion;

																	?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<!-- <td class="text-center"><?php echo number_format($key1->poblacion) ?></td> -->
																			<td class="text-center"><?php echo number_format($key1->fabpobdet) ?></td>

																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<!-- <td class="text-center"><?php echo number_format($suma51) ?></td> -->
																		<td class="text-center"><?php echo round((($suma41 / $suma51) * 1000), 2) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo round((($suma4 / $sumap) * 1000), 2) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map6" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Consolidado anual COVID-19 - Provincias
			</div>
			<!-- <div class="card-body">
								<div class="row">
									<div class="col-lg-12">
										<select class="form-control ddlMapas" id="ddlMapas" onchange="App.events(this); return false;">
											<option value="1"><b>SINADEF</b></option>
											<option value="2"><b>COVID-12</b></option>
										</select>
									</div>
									<div class="col-lg-12">
										<div id="divmapa"></div>
									</div>
								</div> -->
			<div class="row">
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Población
					</div>

				</div>
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Fallecidos/Poblacion
					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 2]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintl</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<th class="text-center bold">Población</th>
									</tr>
								</thead>
								<tbody>
									<?php $suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										$suma5 = $suma5 + $key->poblacion;	 ?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil2" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil2Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>
											<td class="text-center">
												<a class="lnkAmpliar" data-event="lnkProvXrutas3_<?php echo $key->quantil ?>" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<td class="text-center"><?php echo number_format($key->poblacion) ?></td>
										</tr>
										<tr data-target="lnkProvXrutas3_<?php echo $key->quantil ?>" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Poblacion</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 2, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion; ?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<td class="text-center"><?php echo number_format($key1->poblacion) ?></td>
																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<td class="text-center"><?php echo number_format($suma51) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo number_format($suma5) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map4" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>


				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 2]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintl</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<!-- <th class="text-center bold">Pob</th> -->
										<th class="text-center bold">Fall/Pob</th>
									</tr>
								</thead>
								<tbody>
									<?php $suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										$suma5 = $suma5 + $key->poblacion;
									?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil2" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil2Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>
											<td class="text-center">
												<a class="lnkAmpliars" data-event="lnkProvXrutas3_<?php echo $key->quantil ?>2" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<!-- <td class="text-center"><?php echo number_format($key->poblacion) ?></td> -->
											<td class="text-center"><?php echo $key->fabpob ?></td>
										</tr>
										<tr data-target="lnkProvXrutas3_<?php echo $key->quantil ?>2" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Fall/Pob</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 2, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion; ?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<td class="text-center"><?php echo number_format($key1->fabpobdet) ?></td>
																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<td class="text-center"><?php echo round((($suma41 / $suma51) * 1000), 2) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo round((($suma4 / $suma5) * 1000), 2) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map7" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>





			</div>

		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Consolidado anual COVID-19 - Distritos
			</div>
			<!-- <div class="card-body">
								<div class="row">
									<div class="col-lg-12">
										<select class="form-control ddlMapas" id="ddlMapas" onchange="App.events(this); return false;">
											<option value="1"><b>SINADEF</b></option>
											<option value="2"><b>COVID-12</b></option>
										</select>
									</div>
									<div class="col-lg-12">
										<div id="divmapa"></div>
									</div>
								</div> -->
			<div class="row">
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Población
					</div>

				</div>
				<div class="col-lg-6">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Consolidado anual COVID-19 - Fallecidos/Poblacion
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 3]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintl</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<th class="text-center bold">Población</th>
									</tr>
								</thead>
								<tbody>
									<?php $suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										$suma5 = $suma5 + $key->poblacion;	?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil3" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil3Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>

											<td class="text-center">
												<a class="lnkAmpliar" data-event="lnkProvXrutas2_<?php echo $key->quantil ?>" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<td class="text-center"><?php echo number_format($key->poblacion) ?></td>
										</tr>
										<tr data-target="lnkProvXrutas2_<?php echo $key->quantil ?>" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Poblacion</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 3, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion; ?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<td class="text-center"><?php echo number_format($key1->poblacion) ?></td>
																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<td class="text-center"><?php echo number_format($suma51) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo number_format($suma5) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map5" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>

				<div class="col-lg-6">
					<div class="row">
						<div class="col-lg-12">
							<?php $quintilesSinadef = dropDownList((object) ['method' => $method1, 'idNivel' => 3]); ?>
							<table class="table table-sm table-detail datatable">
								<thead>
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Quintil</th>
										<th class="text-center bold">Entidades</th>
										<th class="text-center bold">Fallecidos</th>
										<!-- <th class="text-center bold">Pob</th> -->
										<th class="text-center bold">Fall/Pob</th>
									</tr>
								</thead>
								<tbody>
									<?php $suma1 = 0;
									$suma4 = 0;
									$suma5 = 0;
									foreach ($quintilesSinadef as $key) {
										$suma1 = $suma1 + $key->entidades;
										$suma4 = $suma4 + $key->fallecidos;
										$suma5 = $suma5 + $key->poblacion;	?>
										<tr>
											<?php if ($data->tipo == 'sinadef') { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil3" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } else { ?>
												<td class="text-center"><input type="checkbox" name="chk_quantil3Covid" id="chk_<?php echo $key->quantil ?>" checked></td>
											<?php } ?>

											<td class="text-center">
												<a class="lnkAmpliars" data-event="lnkProvXrutas2_<?php echo $key->quantil ?>2" href="#" onclick="App.events(this); return false;">
													<?php echo ($key->texto) ?>
												</a>
											</td>
											<td class="text-center"><?php echo number_format($key->entidades) ?></td>
											<td class="text-center"><?php echo number_format($key->fallecidos) ?></td>
											<!-- <td class="text-center"><?php echo number_format($key->poblacion) ?></td> -->
											<td class="text-center"><?php echo $key->fabpob ?></td>
										</tr>
										<tr data-target="lnkProvXrutas2_<?php echo $key->quantil ?>2" style="display: none;">
											<td colspan="8">
												<div class="card">
													<div class="card-header card-special">
														Entidades
													</div>
													<div class="card-body">
														<div class="row">
															<div class="col-lg-12">
																<table class="table table-sm table-detail datatable">
																	<tr>
																		<th class="text-center bold">#</th>
																		<th class="text-center bold">Entidades</th>
																		<th class="text-center bold">Fallecidos</th>
																		<th class="text-center bold">Fall/Pob</th>
																	</tr>
																	<?php
																	$quintilesSinadef1 = dropDownList((object) ['method' => $method2, 'idNivel' => 3, 'quantil' => $key->quantil]);

																	$suma41 = 0;
																	$suma51 = 0;
																	foreach ($quintilesSinadef1 as $key1) {
																		$suma41 = $suma41 + $key1->diferencia;
																		$suma51 = $suma51 + $key1->poblacion; ?>
																		<tr>
																			<td class="text-center"><?php echo ($key1->id) ?></td>
																			<td class="text-left"><?php echo ($key1->nomubigeo) ?></td>
																			<td class="text-center"><?php echo number_format($key1->diferencia) ?></td>
																			<td class="text-center"><?php echo number_format($key1->poblacion) ?></td>
																		</tr>
																	<?php } ?>
																	<tr>
																		<td class="text-center"></td>
																		<td class="text-center">Total</td>
																		<td class="text-center"><?php echo number_format($suma41) ?></td>
																		<td class="text-center"><?php echo round((($suma41 / $suma51) * 1000), 2) ?></td>
																	</tr>
																</table>
															</div>
														</div>
													</div>
												</div>
											</td>
										</tr>
									<?php } ?>
									<tr>
										<td class="text-center"></td>
										<td class="text-center">Total</td>
										<td class="text-center"><?php echo number_format($suma1) ?></td>
										<td class="text-center"><?php echo number_format($suma4) ?></td>
										<td class="text-center"><?php echo round((($suma4 / $suma5) * 1000), 2) ?></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-lg-12">
							<div id="map8" style="height:450px; width:100%;"></div>
						</div>
					</div>
				</div>
			</div>


		</div>
	</div>
</div>
</div>
<!-- por poblacion -->