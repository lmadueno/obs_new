<?php
require 'php/app.php';
$data = json_decode($_GET['data']);


if ($data->mostrar == 'empresas-tab') {
    $resumen    = dropDownList((object) ['method' => 'analisisEmpresaOxi']);
    ?>
    <div class="card card-outline-info">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Ranking de empresas por monto de inversión en obras por Impuestos 2009 - 2020
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header card-white text-center font-weight-bold">
                            Inversión OxI de Empresas
                        </div>
                        <span style="text-align:center;">(Millones de soles)</span>
                        <div class="card-body">
                            <div id="chartContainerEmpresas" style="height: 290px; margin: 0 auto;width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-6" style="padding-top:11em;">
                    <div class="row">
                        <div class="col-12">
                            <p align="justify">Un total de <?php echo  $resumen{
                                                            1}->celda2; ?> empresas han participado en el mecanismo OxI y han realizado una inversión de <?php echo  number_format($resumen{
                                                                                                                                                                                2}->celda2); ?> Millones de soles en el periodo 2009 al 2020. El Top 10 de estas empresas concentran una inversion de <?php echo  number_format($resumen{
                                                                                                                                                                                                                                                                                                                                        2}->celda3); ?> Millones de soles equivalente al
                                <?php echo  number_format($resumen{
                                3}->celda3); ?>%.</p>
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold">Descripción</th>
                                    <th class="text-center bold" width="40px">
                                        <a class="changeTablaOxi" id="tableTotal" href="#" onclick="App.events(this); return false;">
                                            Total
                                        </a>
                                    </th>
                                    <th class="text-center bold" width="60px">
                                        <a class="changeTablaOxi" id="tabletop10" href="#" onclick="App.events(this); return false;">
                                            Top 10
                                        </a>
                                    </th>
                                    <th class="text-center bold">
                                        <a class="changeTablaOxi" id="tableFiferencia" href="#" onclick="App.events(this); return false;">
                                            Diferencia
                                        </a>
                                    </th>
                                </tr>
                                <?php foreach ($resumen as $item) { ?>
                                    <tr>
                                        <th class="text-center bold"><?php echo  $item->celda1; ?></th>
                                        <th class="text-center"><?php echo  number_format($item->celda2); ?></th>
                                        <th class="text-center "><?php echo  number_format($item->celda3); ?></th>
                                        <th class="text-center "><?php echo  number_format($item->celda4); ?></th>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=188&sec=0" target="blank">Proinversión</a></h6>
            <br>
            <div id="idGeoempresas"></div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="scrollable" style="overflow-y: auto;max-height: 750px;">
                        <div id="stkd_charempresa"></div>
                    </div>

                </div>
                <div class="col-lg-12">
                    <div id="stkd_charempresa1"></div>
                </div>
            </div>
        </div>
    </div>
<?php
} else if ($data->mostrar == 'regiones-tab') {
    ?>
    <div class="card card-outline-info">
        <div class="card-body">
        <div id="idTablaciprlGraficoRegion"></div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Evolución CIPRL y monto utilizado en Gobiernos Regionales en Millones de Soles
                    </div>
                </div>
                <div class="col-lg-4">
                    <table class="table table-sm table-detail">
                        <thead>
                            <tr>
                                <th class="text-center bold" width="10%"><b>Año</b></th>
                                <th class="text-center bold" width="35%"><b>Tope CIPRL</b></th>
                                <th class="text-center bold" width="30%"><b>Monto Utilizado</b></th>
                                <th class="text-center bold" width="30%"><b>%</b></th>
                            </tr>
                        </thead>
                        <?php
                        $i = 0;
                        $sum = 0;
                        $sTot = 0;
                        $pipxUbigeoDet = dropDownList((object) ['method' => 'cirplxMontoOxi']);
                        foreach ($pipxUbigeoDet as $item) {
                            $i++;
                            $sum = $sum + $item->ciprl;
                            $sTot = $sTot + $item->oxi;
                        ?>
                            <tr>
                                <th class="text-center"><?php echo $item->ano ?></th>
                                <th class="text-right"><?php echo number_format($item->ciprl, 2) ?></th>
                                <th class="text-right"><?php echo number_format($item->oxi, 2) ?></th>
                                <th class="text-right"><?php echo number_format($item->porcentaje, 2) ?></th>

                            </tr>
                        <?php
                        }
                        ?>
                        <tr>
                            <th class="text-right bold" width="10%">Promedio</th>
                            <th class="text-right bold" width="35%"><?php echo number_format($sum / 10, 2) ?></th>
                            <th class="text-right bold" width="30%"><?php echo number_format($sTot / 10, 2) ?></th>
                            <th class="text-right bold" width="30%"><?php echo number_format($sTot/$sum, 2) ?></th>
                        </tr>
                    </table>
                </div>
                <div class="col-lg-8">
                    <div id="regionOxiCiprl" style="height: 320px; width: 100%"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Ranking de Gobiernos Regionales (GR) por monto de inversión en obras por impuestos 2009 - 2020
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header card-white text-center font-weight-bold">
                            Inversión OxI de Gobiernos Regionales (Concluidos)
                        </div>
                        <span style="text-align:center;">(Millones de soles)</span>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div id="chartContainerGobiernosRegionales1" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                    <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                </div>
                                <div class="col-12" style="padding-top:1em;">
                                    <p align="justify">Para el año 2020 se ha otorgado un monto de 4,175.57 Millones de Soles de Tope CIPRL para los Gobiernos Regionales.</p>
                                </div>
                                <div class="col-12" style="padding-top:1em;">
                                    <p align="justify">Actualmente 12 Gobiernos Regionales tienen 43 proyectos en estado 'Concluido' por un monto de 1080.19 Millones de soles, de un total de 1,540 millones de soles</p>
                                </div>
                                <div class="col-12" style="padding-top:1em;padding-button:3em">
                                    <table class="table table-sm table-detail">
                                        <tr>
                                            <th class="text-center bold"></th>
                                            <th class="text-center bold" width="40px">Total</th>
                                            <th class="text-center bold" width="60px">OxI</th>
                                            <th class="text-center bold">Sin OxI</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Gobiernos Regionales(GR)</th>
                                            <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">25</a></th>
                                            <th class="text-center ">12</th>
                                            <th class="text-center ">13</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Cantidad de Proyectos</th>
                                            <th class="text-center">43</th>
                                            <th class="text-center ">43</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Inversión (Millones de Soles)</th>
                                            <th class="text-center">1080.19</th>
                                            <th class="text-center ">1080.19</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Gobiernos Regionaales %</th>
                                            <th class="text-center">100%</th>
                                            <th class="text-center ">70.71%</th>
                                            <th class="text-center ">29.29%</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header card-white text-center font-weight-bold">
                            Inversión OxI de Gobiernos Regionales (Adjudicados)
                        </div>
                        <span style="text-align:center;">(Millones de soles)</span>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div id="chartContainerGobiernosRegionales2" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                    <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                </div>
                                <div class="col-12" style="padding-top:1em;">
                                    <p align="justify">Para el año 2020 se ha otorgado un monto de 4,175.57 Millones de Soles de Tope CIPRL para los Gobiernos Regionales.</p>
                                </div>
                                <div class="col-12" style="padding-top:1em;">
                                    <p align="justify">Actualmente 10 Gobiernos Regionales tienen 29 proyectos en estado 'Concluido' por un monto de 499.39 Millones de soles, de un total de 1,540 millones de soles</p>
                                </div>
                                <div class="col-12" style="padding-top:1em;padding-button:3em">
                                    <table class="table table-sm table-detail">
                                        <tr>
                                            <th class="text-center bold"></th>
                                            <th class="text-center bold" width="40px">Total</th>
                                            <th class="text-center bold" width="60px">OxI</th>
                                            <th class="text-center bold">Sin OxI</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Gobiernos Regionales(GR)</th>
                                            <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">25</a></th>
                                            <th class="text-center ">10</th>
                                            <th class="text-center ">15</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Cantidad de Proyectos</th>
                                            <th class="text-center">29</th>
                                            <th class="text-center ">29</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Inversión (Millones de Soles)</th>
                                            <th class="text-center">499.39</th>
                                            <th class="text-center ">499.39</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Gobiernos Regionales %</th>
                                            <th class="text-center">100%</th>
                                            <th class="text-center ">32.43%</th>
                                            <th class="text-center ">67.57%</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Gobiernos Regionales con OxI Concluido 2009 - 2019 (Millones de Soles)
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="idtablaRegionesConcluidas"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div id="chartContainerGobiernosRegionales3" style="height: 520px; width: 100%"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Gobiernos Regionales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="idtablaRegionesAdjudicadas"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div id="chartContainerGobiernosRegionales4" style="height: 520px; width: 100%"></div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else if ($data->mostrar == 'provincias-tab') {
    ?>
    <div class="card card-outline-info">
        <div class="card-body">
            <div id="idTablaciprlGraficoProvincias"></div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Evolución CIPRL y monto utilizado en Municipalidades Provinciales en Millones de Soles
                    </div>
                </div>
                <div class="col-4">
                    <table class="table table-sm table-detail">
                        <thead>
                            <tr>
                                <th class="text-center" width="10%"><b>Año</b></th>
                                <th class="text-center" width="35%"><b>Tope CIPRL</b></th>
                                <th class="text-center" width="30%"><b>Monto Utilizado</b></th>
                                <th class="text-center" width="30%"><b>%</b></th>
                            </tr>
                        </thead>
                        <tr>
                            <th class="text-center">2011</th>
                            <th class="text-right"> 4,836.87</th>
                            <th class="text-right"> 5.62</th>
                            <th class="text-right">0.12</th>

                        </tr>
                        <tr>
                            <th class="text-center">2012</th>
                            <th class="text-right">4,543.83</th>
                            <th class="text-right">50.90</th>
                            <th class="text-right">1.12</th>
                        </tr>
                        <tr>
                            <th class="text-center">2013</th>
                            <th class="text-right">4,298.36</th>
                            <th class="text-right">151.00</th>
                            <th class="text-right">3.51</th>
                        </tr>
                        <tr>
                            <th class="text-center">2014</th>
                            <th class="text-right">4,633.20</th>
                            <th class="text-right">80.02</th>
                            <th class="text-right">1.73</th>
                        </tr>
                        <tr>
                            <th class="text-center">2015</th>
                            <th class="text-right">4,008.13</th>
                            <th class="text-right">139.44</th>
                            <th class="text-right">3.48</th>
                        </tr>
                        <tr>
                            <th class="text-center">2016</th>
                            <th class="text-right">2,918.37</th>
                            <th class="text-right">259.78</th>
                            <th class="text-right">8.90</th>
                        </tr>
                        <tr>
                            <th class="text-center">2017</th>
                            <th class="text-right">2,691.04</th>
                            <th class="text-right">298.10</th>
                            <th class="text-right">11.08</th>
                        </tr>
                        <tr>
                            <th class="text-center">2018</th>
                            <th class="text-right">1,623.31</th>
                            <th class="text-right">128.61</th>
                            <th class="text-right">7.92</th>
                        </tr>
                        <tr>
                            <th class="text-center">2019*</th>
                            <th class="text-right">1,585.51</th>
                            <th class="text-right">0.00</th>
                            <th class="text-right">0</th>
                        </tr>
                        <tr>
                            <th class="text-center">2020</th>
                            <th class="text-right">0</th>
                            <th class="text-right">0.00</th>
                            <th class="text-right">0</th>
                        </tr>

                        <tr>
                            <th class="text-center bold vert-middle">Promedio</th>
                            <th class="text-right bold vert-middle">3,459.84</th>
                            <th class="text-right bold vert-middle">94.8</th>
                            <th class="text-right bold vert-middle">0.03</th>
                        </tr>
                    </table>
                </div>
                <div class="col-8">
                    <div id="container1" style="height: 320px; width: 100%"></div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Ranking de Municipalidades Provinciales (MP) por monto de inversión en obras por impuestos 2009 - 2019
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header card-white text-center font-weight-bold">
                            Inversión OxI de Municipalidades Provinciales (Adjudicados)
                        </div>
                        <span style="text-align:center;">(Millones de soles)</span>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div id="chartContainerProvinciasAdjudicadas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                    <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                </div>
                                <div class="col-12" style="padding-top:1em;">
                                    Actualmente 22 (11.2%) Municipalidades Provinciales (MP) de un total de 196 tienen adjudicado 29 proyectos por un monto de 266 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                </div>
                                <div class="col-12" style="padding-top:1em">
                                    <table class="table table-sm table-detail">
                                        <tr>
                                            <th class="text-center bold"></th>
                                            <th class="text-center bold" width="40px">Total</th>
                                            <th class="text-center bold" width="60px">OxI</th>
                                            <th class="text-center bold">Sin OxI</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Municipalidades Provinciales(MP)</th>
                                            <th class="text-center"><a href="https://www.gob.pe/estado" target="blank">196</a></th>
                                            <th class="text-center ">22</th>
                                            <th class="text-center ">174</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Cantidad de Proyectos</th>
                                            <th class="text-center">29</th>
                                            <th class="text-center ">29</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Inversión (Millones de Soles)</th>
                                            <th class="text-center">266</th>
                                            <th class="text-center ">266</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Municipalidades Provinciales %</th>
                                            <th class="text-center">100%</th>
                                            <th class="text-center ">11.22%</th>
                                            <th class="text-center ">88.78%</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-header card-white text-center font-weight-bold">
                            Inversión OxI de Municipalidades Provinciales (Concluidos)
                        </div>
                        <span style="text-align:center;">(Millones de soles)</span>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    <div id="chartContainerProvinciasConcluidas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                                    <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                                </div>
                                <div class="col-12" style="padding-top:1em">
                                    Actualmente 28 (14.28%) Municipalidades Provinciales (MP) de un total de 196, han concluido 63 proyectos por un monto de 587.20 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                                </div>
                                <div class="col-12" style="padding-top:1em">
                                    <table class="table table-sm table-detail">
                                        <tr>
                                            <th class="text-center bold"></th>
                                            <th class="text-center bold" width="40px">Total</th>
                                            <th class="text-center bold" width="60px">OxI</th>
                                            <th class="text-center bold">Sin OxI</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Municipalidades Provinciales(MP)</th>
                                            <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">196</a></th>
                                            <th class="text-center ">28</th>
                                            <th class="text-center ">168</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Cantidad de Proyectos</th>
                                            <th class="text-center">63</th>
                                            <th class="text-center ">63</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Inversión (Millones de Soles)</th>
                                            <th class="text-center">587.20</th>
                                            <th class="text-center ">587.20</th>
                                            <th class="text-center ">0</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center bold">Municipalidades Provinciales %</th>
                                            <th class="text-center">100%</th>
                                            <th class="text-center ">14.28%</th>
                                            <th class="text-center ">85.72%</th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Municipalidades Provinciales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="idtablaProvinciasAdjudicadas"></div>

                    <div class="offset-md-2 col-lg-8">
                        <h3><b>Leyenda</b></h3>
                        <table class="table table-sm table-detail">
                            <tr>
                                <td class="text-center bold" style="width:20%"><b> * </b></th>
                                <td class="text-center ">Al Primer Trimestre (2019)</td>
                            </tr>
                            <tr>
                                <td class="text-center bold" style="width:20%">Nota</th>
                                <td class="text-center ">No existen registros de OxI desde el año 2009 hasta el 2010</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12">
                    <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                        <div id="chartContainerProvincialesStakedAdjudicado" style="height: 720px; width: 100%"></div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-header card-special text-center font-weight-bold">
                        Municipalidades Provinciales con OxI Concluido 2009 - 2019 (Millones de Soles)
                    </div>
                </div>
                <div class="col-lg-12">
                    <div id="idtablaProvinciasConcluidas"></div>
                    <div class="offset-md-2 col-lg-8">
                        <h3><b>Leyenda</b></h3>
                        <table class="table table-sm table-detail">
                            <tr>
                                <td class="text-center bold" style="width:20%"><b> * </b></th>
                                <td class="text-center ">Al Primer Trimestre (2019)</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-12">
                    <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                        <div id="chartContainerProvincialesStakedConcluido" style="height: 720px; width: 100%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
} else if ($data->mostrar == 'distritos-tab') {
    ?>

    <div id="idTablaciprlGraficoDistritos"></div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Evolución CIPRL y monto utilizado en Municipalidades Distritales en Millones de Soles
            </div>
        </div>
        <div class="col-4">
            <table class="table table-sm table-detail">
                <thead>
                    <tr>
                        <th class="text-center" width="10%"><b>Año</b></th>
                        <th class="text-center" width="35%"><b>Tope CIPRL</b></th>
                        <th class="text-center" width="30%"><b>Monto Utilizado</b></th>
                        <th class="text-center" width="30%"><b>%</b></th>
                    </tr>
                </thead>
                <tr>
                    <th class="text-center">2011</th>
                    <th class="text-right"> 18,956.35</th>
                    <th class="text-right"> 5.34</th>
                    <th class="text-right">0.03</th>

                </tr>
                <tr>
                    <th class="text-center">2012</th>
                    <th class="text-right">18,593.46</th>
                    <th class="text-right">142.66</th>
                    <th class="text-right">0.77</th>
                </tr>
                <tr>
                    <th class="text-center">2013</th>
                    <th class="text-right">17,548.33</th>
                    <th class="text-right">268.66</th>
                    <th class="text-right">1.53</th>
                </tr>
                <tr>
                    <th class="text-center">2014</th>
                    <th class="text-right">18,195.35</th>
                    <th class="text-right">243.06</th>
                    <th class="text-right">1.34</th>
                </tr>
                <tr>
                    <th class="text-center">2015</th>
                    <th class="text-right">15,480.23</th>
                    <th class="text-right">114.47</th>
                    <th class="text-right">0.74</th>
                </tr>
                <tr>
                    <th class="text-center">2016</th>
                    <th class="text-right">11,953.96</th>
                    <th class="text-right">259.78</th>
                    <th class="text-right">2.17</th>
                </tr>
                <tr>
                    <th class="text-center">2017</th>
                    <th class="text-right">10,639.81</th>
                    <th class="text-right">298.10</th>
                    <th class="text-right">2.80</th>
                </tr>
                <tr>
                    <th class="text-center">2018</th>
                    <th class="text-right">7,476.87</th>
                    <th class="text-right">236.13</th>
                    <th class="text-right">3.16</th>
                </tr>
                <tr>
                    <th class="text-center">2019</th>
                    <th class="text-right">9,036.28</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0</th>
                </tr>
                <tr>
                    <th class="text-center">2020</th>
                    <th class="text-right">0</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0</th>
                </tr>
                <tr>
                    <th class="text-center bold vert-middle">Promedio</th>
                    <th class="text-right bold vert-middle">14,208.96</th>
                    <th class="text-right bold vert-middle">159.91</th>
                    <th class="text-right bold vert-middle">0.01</th>
                </tr>
            </table>
        </div>
        <div class="col-8">
            <div id="container2" style="height: 320px; width: 100%"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Ranking de Municipalidades Distritales (MD) por monto de inversión en obras por impuestos 2009 - 2019
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Municipalidades Distritales (Adjudicados)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="chartContainerDistritalesAdjudicadas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                        </div>
                        <div class="col-12" style="padding-top:1em;">
                            Actualmente 33 (1.76%) Municipalidades Distritales (MD) de un total de 1874 tienen adjudicado 51 proyectos por un monto de 444.22 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold"></th>
                                    <th class="text-center bold" width="40px">Total</th>
                                    <th class="text-center bold" width="60px">OxI</th>
                                    <th class="text-center bold">Sin OxI</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Municipalidades Distritales(MD)</th>
                                    <th class="text-center"><a href="https://www.gob.pe/estado" target="blank">1874</a></th>
                                    <th class="text-center ">33</th>
                                    <th class="text-center ">1841</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Cantidad de Proyectos</th>
                                    <th class="text-center">51</th>
                                    <th class="text-center ">51</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
                                    <th class="text-center">444.22</th>
                                    <th class="text-center ">444.22</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Municipalidades Distritales %</th>
                                    <th class="text-center">100%</th>
                                    <th class="text-center ">1.76%</th>
                                    <th class="text-center ">98.24%</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Municipalidades Distritales (Concluidos)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="chartContainerDistritosConcluidas" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            Actualmente 96 (14.28%) Municipalidades Distritale (MD) de un total de 1874 han concluido 123 proyectos por un monto de 994.99 Millones de soles mediante el mecanismo OxI en el periodo 2009 al 2019
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold"></th>
                                    <th class="text-center bold" width="40px">Total</th>
                                    <th class="text-center bold" width="60px">OxI</th>
                                    <th class="text-center bold">Sin OxI</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Municipalidades Distritales (MD)</th>
                                    <th class="text-center"><a href="https://www.gob.pe/estado/gobiernos-regionales" target="blank">1874</a></th>
                                    <th class="text-center ">96</th>
                                    <th class="text-center ">1778</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Cantidad de Proyectos</th>
                                    <th class="text-center">123</th>
                                    <th class="text-center ">123</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
                                    <th class="text-center">994.99 </th>
                                    <th class="text-center ">0</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Municipalidades Distritales %</th>
                                    <th class="text-center">100%</th>
                                    <th class="text-center ">5.12%</th>
                                    <th class="text-center ">94.88%</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Municipalidades Distritales con OxI Adjudicado 2009 - 2019 (Millones de Soles)
            </div>
        </div>
        <div class="col-lg-12">
            <div id="idtablaDistritosAdjudicadas"></div>

            <div class="offset-md-2 col-lg-8">
                <h3><b>Leyenda</b></h3>
                <table class="table table-sm table-detail">
                    <tr>
                        <td class="text-center bold" style="width:20%"><b> * </b></th>
                        <td class="text-center ">Al Primer Trimestre (2019)</td>
                    </tr>
                    <tr>
                        <td class="text-center bold" style="width:20%">Nota</th>
                        <td class="text-center ">No existen registros de OxI del año 2009</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                <div id="chartContainerDistritalesStakedAdjudicado" style="height: 720px; width: 100%"></div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Municipalidades Distritales con OxI Concluido 2015 - 2019 (Millones de Soles)
            </div>
        </div>
        <div class="col-lg-12">
            <div id="idtablaDistritosConcluidas"></div>
            <div class="offset-md-2 col-lg-8">
                <h3><b>Leyenda</b></h3>
                <table class="table table-sm table-detail">
                    <tr>
                        <td class="text-center bold" style="width:20%"><b> * </b></th>
                        <td class="text-center ">Al Primer Trimestre (2019)</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div class="scrollable" style="overflow-y: auto;max-height: 750px;">
                <div id="chartContainerDistritalesStakedConcluido" style="height: 1820px; width: 100%"></div>
            </div>
        </div>
    </div>
    <?php
} else if ($data->mostrar == 'ministerios-tab') {
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Ranking de Ministerios Nacionales (MIN) por monto de inversión en obras por impuestos(OxI) 2015 - 2018
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Ministerios (Adjudicados)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="chartContainerMinisterios" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                        </div>
                        <div class="col-12" style="padding-top:1em;">
                            Solo 7 (36.84%) Ministerios (MIN) de un total de 19, han adjudicado 30 proyectos por un monto de 640 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2019
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <?php $excelEmpresas = dropDownList((object) ['method' => 'ministeriosOXIExcelAdjudicados']); ?>
                            <?php
                            $temp = 0;
                            $temp1 = 0;
                            $temp2 = 0;
                            $temp3 = 0;
                            $temp4 = 0;
                            $temp5 = 0;
                            ?>
                            <?php foreach ($excelEmpresas as $key) {
                                $temp += $key->suma2015;
                                $temp1 += $key->suma2016;
                                $temp2 += $key->suma2017;
                                $temp3 += $key->suma2018;

                                $temp4 += $key->total;
                            } ?>
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold"></th>
                                    <th class="text-center bold" width="40px">Total</th>
                                    <th class="text-center bold" width="60px">OxI</th>
                                    <th class="text-center bold">Sin OxI</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Ministerios</th>
                                    <th class="text-center"><a href="http://www.pcm.gob.pe/entidades-pcm/" target="blank">19</a></th>
                                    <th class="text-center ">7</th>
                                    <th class="text-center ">12</th>
                                </tr>

                                <tr>
                                    <th class="text-center bold">Cantidad de Proyectos</th>
                                    <th class="text-center">30</th>
                                    <th class="text-center ">30</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
                                    <th class="text-center">640</th>
                                    <th class="text-center ">640</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Total Inversión</th>
                                    <th class="text-center">100%</th>
                                    <th class="text-center ">36.84%</th>
                                    <th class="text-center ">63.16%</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Ministerios (Concluidos)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="chartContainerMinisterios1" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                        </div>
                        <br>
                        <div class="col-12" style="padding-top:1em">
                            Solo 2 (10.52%) Ministerios (MIN) de un total de 19, han concluido 3 proyectos por un monto de 9.4 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2018
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <?php $excelEmpresas = dropDownList((object) ['method' => 'ministeriosOXIExcelAdjudicados']); ?>
                            <?php
                            $temp = 0;
                            $temp1 = 0;
                            $temp2 = 0;
                            $temp3 = 0;
                            $temp4 = 0;
                            $temp5 = 0;
                            ?>
                            <?php foreach ($excelEmpresas as $key) {
                                $temp += $key->suma2015;
                                $temp1 += $key->suma2016;
                                $temp2 += $key->suma2017;
                                $temp3 += $key->suma2018;
                                $temp4 += $key->total;
                                $temp5 += $key->suma2019;
                            } ?>
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold"></th>
                                    <th class="text-center bold" width="40px">Total</th>
                                    <th class="text-center bold" width="60px">OxI</th>
                                    <th class="text-center bold">Sin OxI</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Ministerios</th>
                                    <th class="text-center"><a href="http://www.pcm.gob.pe/entidades-pcm/" target="blank">19</a></th>
                                    <th class="text-center ">2</th>
                                    <th class="text-center ">17</th>
                                </tr>

                                <tr>
                                    <th class="text-center bold">Cantidad de Proyectos</th>
                                    <th class="text-center">3</th>
                                    <th class="text-center ">3</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
                                    <th class="text-center">9.4</th>
                                    <th class="text-center ">9.4</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Total Inversión</th>
                                    <th class="text-center">100%</th>
                                    <th class="text-center ">10.52%</th>
                                    <th class="text-center ">89.48%</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-lg-12" style="padding-top:1em;">
                                                <div id="charcontainerMin" style="height: 320px; width: 100%"></div>
                                        </div> -->
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Ministerios con OxI Adjudicado 2015 - 2018 (Millones de Soles)
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-sm table-detail">
                <thead>
                    <tr>
                        <th class="text-center bold vert-middle">#</th>
                        <th class="text-center bold vert-middle">Abreviatura</th>
                        <th class="text-center bold vert-middle">Ministerio</th>

                        <th class="text-center bold vert-middle">2016</th>
                        <th class="text-center bold vert-middle">2017</th>
                        <th class="text-center bold vert-middle">2018</th>
                        <th class="text-center bold vert-middle">2019*</th>
                        <th class="text-center bold vert-middle">Total</th>
                    </tr>
                </thead>
                <?php foreach ($excelEmpresas as $key) { ?>
                    <tr>
                        <td class="text-center"><?php $temp5++;
                                                echo ($temp5) ?></td>
                        <td class="text-left"><?php echo $key->nombre ?></td>
                        <td class="text-left"><?php echo $key->abreviatura ?></td>
                        <td class="text-right"><?php echo ($key->suma2016) ?></td>
                        <td class="text-right"><?php echo ($key->suma2017) ?></td>
                        <td class="text-right"><?php echo ($key->suma2018) ?></td>
                        <td class="text-right"><?php echo ($key->suma2019) ?></td>
                        <td class="text-right">
                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre ?>" href="#" onclick="App.events(this); return false;">
                                <?php echo ($key->total) ?>
                            </a>
                        </td>
                    </tr>
                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre ?>" style="display: none;">
                        <td colspan="9">
                            <div class="card">
                                <div class="card-header card-special">
                                    Lista de Proyectos
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-sm table-detail">

                                                <tr>
                                                    <th class="text-center bold" width="5%">#</th>
                                                    <th class="text-center bold" width="15%">Codigo Unico</th>
                                                    <th class="text-center bold" width="40%">Nombre Proyecto</th>
                                                    <th class="text-center bold" width="20%">Empresa</th>
                                                    <th class="text-center bold" width="5%">Monto</th>
                                                    <th class="text-center bold" width="15%">Fecha</th>

                                                </tr>
                                                <?php
                                                $estadoMinisterio = 'Adjudicado';
                                                $tempMinisDet = 0;
                                                $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                ?>
                                                <?php foreach ($tabladet as $key) { ?>
                                                    <tr>
                                                        <td class="text-center"><?php $tempMinisDet++;
                                                                                echo ($tempMinisDet) ?></td>
                                                        <td class="text-center"><a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip; ?>' target="blank"><?php echo $key->snip; ?></td>
                                                        <td class="text-left"><?php echo ucfirst($key->nombre_proyecto) ?></td>
                                                        <td class="text-left"><?php echo ($key->empresa) ?></td>
                                                        <td class="text-right"><?php echo ($key->monto_inversion) ?></td>
                                                        <td class="text-center"><?php echo ($key->fecha_buena_pro) ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php } ?>
                <tr>
                    <td class="text-center"></td>

                    <td class="text-center"></td>
                    <td class="text-left"><b>TOTAL</b></td>

                    <td class="text-right"><?php echo ($temp1) ?></td>
                    <td class="text-right"><?php echo ($temp2) ?></td>
                    <td class="text-right"><?php echo ($temp3) ?></td>
                    <td class="text-right">0</td>
                    <td class="text-right"><?php echo ($temp4) ?></td>
                </tr>
                <tr>
                    <td class="text-center"></td>


                    <td class="text-right"></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                    <td class="text-right"></td>
                </tr>
            </table>
        </div>
        <div class="offset-md-2 col-lg-8">
            <h3><b>Leyenda</b></h3>
            <table class="table table-sm table-detail">
                <tr>
                    <td class="text-center bold" style="width:20%"><b> * </b></th>
                    <td class="text-center ">Al Primer Trimestre (2019)</td>
                </tr>
                <tr>
                    <td class="text-center bold" style="width:20%">Nota</th>
                    <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2015</td>
                </tr>
            </table>
        </div>

    </div>
    <br>
    <div class="row">
        <div class="col-12">
            <div id="chartContainerMinisterios2" style="height: 320px; width: 100%"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Ministerios con OxI Concluidos 2009 - 2019 (Millones de Soles)
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-sm table-detail">
                <thead>
                    <tr>
                        <th class="text-center bold vert-middle">#</th>
                        <th class="text-center bold vert-middle">Ministerio</th>
                        <th class="text-center bold vert-middle">Abreviatura</th>

                        <th class="text-center bold vert-middle">2016</th>
                        <th class="text-center bold vert-middle">2017</th>
                        <th class="text-center bold vert-middle">2018</th>
                        <th class="text-center bold vert-middle">2019</th>
                        <th class="text-center bold vert-middle">Total</th>
                    </tr>
                </thead>
                <?php $excelEmpresas1 = dropDownList((object) ['method' => 'ministeriosOXIExcelConcluidos']);
                $temp = 0;
                $temp1 = 0;
                $temp2 = 0;
                $temp3 = 0;
                $temp4 = 0;
                $temp5 = 0;
                $tempMinisDet = 0;
                ?>
                <?php foreach ($excelEmpresas1 as $key) {

                    $temp1 += $key->suma2016;
                    $temp2 += $key->suma2017;
                    $temp3 += $key->suma2018;
                    $temp4 += $key->total;
                    $temp5 += $key->suma2019;
                ?>
                    <tr>
                        <td class="text-center"><?php $temp5++;
                                                echo ($temp5) ?></td>
                        <td class="text-left"><?php echo $key->nombre ?></td>
                        <td class="text-left"><?php echo $key->abreviatura ?></td>

                        <td class="text-right"><?php echo ($key->suma2016) ?></td>
                        <td class="text-right"><?php echo ($key->suma2017) ?></td>
                        <td class="text-right"><?php echo ($key->suma2018) ?></td>
                        <td class="text-right"><?php echo ($key->suma2019) ?></td>
                        <td class="text-right"><a href=""></a>
                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre ?>" href="#" onclick="App.events(this); return false;">
                                <?php echo ($key->total) ?>
                            </a>
                        </td>
                    </tr>
                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre ?>" style="display: none;">
                        <td colspan="9">
                            <div class="card">
                                <div class="card-header card-special">
                                    Lista de Proyectos
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center bold" width="5%">#</th>
                                                        <th class="text-center bold" width="15%">Codigo Unico</th>
                                                        <th class="text-center bold" width="40%">Nombre Proyecto</th>
                                                        <th class="text-center bold" width="20%">Empresa</th>
                                                        <th class="text-center bold" width="5%">Monto</th>
                                                        <th class="text-center bold" width="15%">Fecha</th>

                                                    </tr>
                                                </thead>
                                                <?php
                                                $estadoMinisterio = 'Concluido';
                                                $tempMinisDet = 0;
                                                $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                ?>
                                                <?php foreach ($tabladet as $key) { ?>
                                                    <tr>
                                                        <td class="text-center"><?php $tempMinisDet++;
                                                                                echo ($tempMinisDet) ?></td>
                                                        <td class="text-center"><?php echo ($key->snip) ?></td>
                                                        <td class="text-left"><?php echo ucfirst($key->nombre_proyecto) ?></td>
                                                        <td class="text-left"><?php echo ($key->empresa) ?></td>
                                                        <td class="text-right"><?php echo ($key->monto_inversion) ?></td>
                                                        <td class="text-center"><?php echo ($key->fecha_buena_pro) ?></td>

                        </td>

                    <?php } ?>

            </table>
        </div>
    </div>
    </div>
    </div>
    </tr>

<?php } ?>
<tr>
    <td class="text-center"></td>

    <td class="text-center"></td>
    <td class="text-left"><b>TOTAL</b></td>

    <td class="text-right"><?php echo ($temp1) ?></td>
    <td class="text-right"><?php echo ($temp2) ?></td>
    <td class="text-right"><?php echo ($temp3) ?></td>
    <td class="text-right">0</td>
    <td class="text-right"><?php echo ($temp4) ?></td>
</tr>
<tr>
    <td class="text-center"></td>

    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
</tr>
</table>
</div>
<div class="offset-md-2 col-lg-8">
    <h3><b>Leyenda</b></h3>
    <table class="table table-sm table-detail">
        <tr>
            <td class="text-center bold" style="width:20%"><b> * </b></th>
            <td class="text-center ">Al Primer Trimestre (2019)</td>
        </tr>
        <tr>
            <td class="text-center bold" style="width:20%">Nota</th>
            <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2015</td>
        </tr>
    </table>
</div>
</div>
<br>
<div class="row">
    <div class="col-12">
        <div id="chartContainerMinisterios3" style="height: 320px; width: 100%"></div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-12">
        <div class="card-header card-special text-center font-weight-bold">
            PMI por Ministerios 2015 - 2018 (Millones de Soles)
        </div>
    </div>
    <div class="col-lg-12">
        <?php
        $tempMinisDet = 0;
        $tabladet = dropDownList((object) ['method' => 'ministeriosPMI']);
        ?>
        <table class="table table-sm table-detail">
            <thead>
                <tr>
                    <th class="text-center bold vert-middle" rowspan="2">#</th>
                    <th class="text-center bold vert-middle" rowspan="2">Ministerio</th>
                    <th class="text-center bold vert-middle" colspan="2">2015</th>
                    <th class="text-center bold vert-middle" colspan="2">2016</th>
                    <th class="text-center bold vert-middle" colspan="2">2017</th>
                    <th class="text-center bold vert-middle" colspan="2">2018</th>
                    <th class="text-center bold vert-middle" colspan="2">Promedio</th>
                </tr>
                <tr>

                    <th class="text-center bold vert-middle">M S/.</th>
                    <th class="text-center bold vert-middle">%</th>
                    <th class="text-center bold vert-middle">M S/.</th>
                    <th class="text-center bold vert-middle">%</th>
                    <th class="text-center bold vert-middle">M S/.</th>
                    <th class="text-center bold vert-middle">%</th>
                    <th class="text-center bold vert-middle">M S/.</th>
                    <th class="text-center bold vert-middle">%</th>
                    <th class="text-center bold vert-middle">M S/.</th>
                    <th class="text-center bold vert-middle">%</th>

                </tr>
            </thead>
            <?php foreach ($tabladet as $key) { ?>
                <tr>
                    <td class="text-center"><?php $tempMinisDet++;
                                            echo ($tempMinisDet) ?></td>

                    <td class="text-left"><?php echo ucfirst(strtolower($key->ministerio)) ?></td>
                    <td class="text-right"><?php echo number_format($key->s2015) ?></td>
                    <td class="text-right"><?php echo number_format($key->p2015) ?></td>
                    <td class="text-right"><?php echo number_format($key->s2016) ?></td>
                    <td class="text-right"><?php echo number_format($key->p2016) ?></td>
                    <td class="text-right"><?php echo number_format($key->s2017) ?></td>
                    <td class="text-right"><?php echo number_format($key->p2017) ?></td>
                    <td class="text-right"><?php echo number_format($key->s2018) ?></td>
                    <td class="text-right"><?php echo number_format($key->p2018) ?></td>
                    <td class="text-right"><?php echo number_format($key->promedio) ?></td>
                    <td class="text-right"><?php echo number_format($key->promediop) ?></td>

                    </td>

                <?php } ?>
        </table>
    </div>
</div>

<?php
} else if ($data->mostrar == 'universidades-tab') {
    ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Evolución CIPRL y monto utilizado en Universidades Nacionales en Millones de Soles
            </div>
        </div>
        <div class="col-4">
            <table class="table table-sm table-detail">
                <thead>
                    <tr>
                        <th class="text-center" width="10%"><b>Año</b></th>
                        <th class="text-center" width="35%"><b>Tope CIPRL</b></th>
                        <th class="text-center" width="30%"><b>Monto Utilizado</b></th>
                        <th class="text-center" width="30%"><b>%</b></th>
                    </tr>
                </thead>
                <tr>
                    <th class="text-center">2014</th>
                    <th class="text-right">1,066.53</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0.00</th>
                </tr>
                <tr>
                    <th class="text-center">2015</th>
                    <th class="text-right">1,107.33</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0.00</th>
                </tr>
                <tr>
                    <th class="text-center">2016</th>
                    <th class="text-right">975.88</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0.00</th>
                </tr>
                <tr>
                    <th class="text-center">2017</th>
                    <th class="text-right">686.47</th>
                    <th class="text-right">0.00</th>
                    <th class="text-right">0.00</th>
                </tr>
                <tr>
                    <th class="text-center">2018</th>
                    <th class="text-right">736.90</th>
                    <th class="text-right">12.38</th>
                    <th class="text-right">1.68</th>
                </tr>
                <tr>
                    <th class="text-center">2019*</th>
                    <th class="text-right">956.40</th>
                    <th class="text-right">18.43</th>
                    <th class="text-right">1.93</th>
                </tr>
                <tr>
                    <th class="text-center bold vert-middle">Promedio</th>
                    <th class="text-right bold vert-middle">552.95</th>
                    <th class="text-right bold vert-middle">3.081</th>
                    <th class="text-right bold vert-middle">0.56</th>
                </tr>
            </table>
        </div>
        <div class="col-8">
            <div id="container3" style="height: 320px; width: 100%"></div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Ranking de Universidades Nacionales (UN) por monto de inversión en obras por impuestos(OxI) 2018 - 2019
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Universidades (Adjudicados)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="chartContainerUniversidades" style="height: 290px !important; margin: 0 auto !important;width: 100% !important;"></div>
                            <h6>Fuente: <a href="https://www.obrasporimpuestos.pe/0/0/modulos/JER/PlantillaStandard.aspx?are=0&prf=0&jer=189&sec=0" target="blank">Proinversión</a></h6>
                        </div>
                        <div class="col-12" style="padding-top:1em;">
                            Solo 3 (5.89%) Universidades Nacionales (UN) de un total de 51 han adjudicado 4 proyectos por un monto de 30.81 Millones de soles mediante el mecanismo OxI en el periodo 2015 al 2019
                        </div>
                        <div class="col-12" style="padding-top:1em">
                            <?php
                            $excelEmpresas = dropDownList((object) ['method' => 'UniversidadesOXIExcelAjudicaods']);

                            $temp2 = 0;
                            $temp3 = 0;
                            $temp4 = 0;
                            ?>
                            <?php foreach ($excelEmpresas as $key) {
                                $temp2 += $key->suma4;
                                $temp3 += $key->suma5;
                                $temp4 += $key->total;
                            } ?>
                            <table class="table table-sm table-detail">
                                <tr>
                                    <th class="text-center bold"></th>
                                    <th class="text-center bold" width="40px">Total</th>
                                    <th class="text-center bold" width="60px">OxI</th>
                                    <th class="text-center bold">Sin OxI</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Universidades</th>
                                    <th class="text-center"><a href="https://www.sunedu.gob.pe/universidades-publicas/" target="blank">51</a></th>
                                    <th class="text-center ">3</th>
                                    <th class="text-center ">49</th>
                                </tr>

                                <tr>
                                    <th class="text-center bold">Cantidad de Proyectos</th>
                                    <th class="text-center">4</th>
                                    <th class="text-center ">4</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Inversión (Millones de Soles)</th>
                                    <th class="text-center">30.81</th>
                                    <th class="text-center ">30.81</th>
                                    <th class="text-center ">0</th>
                                </tr>
                                <tr>
                                    <th class="text-center bold">Total Inversión</th>
                                    <th class="text-center">100%</th>
                                    <th class="text-center ">5.89%</th>
                                    <th class="text-center ">94.11%</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header card-white text-center font-weight-bold">
                    Inversión OxI de Universidades (Concluidos)
                </div>
                <span style="text-align:center;">(Millones de soles)</span>
                <div class="card-body">
                    <div class="col-12" style="padding-top:26.5em;">
                        0 Universidades Nacionales (UN) de un total de 51 no han concluido proyectos mediante el mecanismo OxI en el periodo 2015 al 2019
                    </div>
                    <div class="col-12" style="padding-top:1em">

                        <table class="table table-sm table-detail">
                            <tr>
                                <th class="text-center bold"></th>
                                <th class="text-center bold" width="40px">Total</th>
                                <th class="text-center bold" width="60px">OxI</th>
                                <th class="text-center bold">Sin OxI</th>
                            </tr>
                            <tr>
                                <th class="text-center bold">Universidades</th>
                                <th class="text-center"><a href="https://www.sunedu.gob.pe/universidades-publicas/" target="blank">51</a></th>
                                <th class="text-center ">0</th>
                                <th class="text-center ">0</th>
                            </tr>

                            <tr>
                                <th class="text-center bold">Cantidad de Proyectos</th>
                                <th class="text-center">0</th>
                                <th class="text-center ">0</th>
                                <th class="text-center ">0</th>
                            </tr>
                            <tr>
                                <th class="text-center bold">Inversión (Millones de Soles)</th>
                                <th class="text-center">0</th>
                                <th class="text-center ">0</th>
                                <th class="text-center ">0</th>
                            </tr>
                            <tr>
                                <th class="text-center bold">Total Inversión</th>
                                <th class="text-center">100%</th>
                                <th class="text-center ">0%</th>
                                <th class="text-center ">0%</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Universidades Nacionales con OxI Adjudicado 2018 - 2019 (Millones de Soles)
            </div>
        </div>
        <div class="col-lg-12">
            <table class="table table-sm table-detail">
                <thead>
                    <tr>
                        <th class="text-center bold vert-middle">#</th>
                        <th class="text-center bold vert-middle">Region</th>
                        <th class="text-center bold vert-middle">Universidad</th>
                        <th class="text-center bold vert-middle">2018</th>
                        <th class="text-center bold vert-middle">2019</th>
                        <th class="text-center bold vert-middle">Total</th>
                    </tr>
                </thead>

                <?php foreach ($excelEmpresas as $key) { ?>
                    <tr>
                        <td class="text-center"><?php echo $key->id ?></td>
                        <td class="text-left"><?php echo $key->region ?></td>
                        <td class="text-left"><?php echo $key->nombre ?></td>
                        <td class="text-right"><?php echo ($key->suma4) ?></td>
                        <td class="text-right"><?php echo ($key->suma5) ?></td>
                        <td class="text-right"><a href=""></a>
                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre ?>" href="#" onclick="App.events(this); return false;">
                                <?php echo ($key->total) ?>
                            </a>
                        </td>
                    </tr>
                    <tr data-target="lnkProvXrutas_<?php echo $key->nombre ?>" style="display: none;">
                        <td colspan="8">
                            <div class="card">
                                <div class="card-header card-special">
                                    Lista de Proyectos
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center bold" width="5%">#</th>
                                                        <th class="text-center bold" width="15%">Codigo Unico</th>
                                                        <th class="text-center bold" width="40%">Nombre Proyecto</th>
                                                        <th class="text-center bold" width="20%">Empresa</th>
                                                        <th class="text-center bold" width="5%">Monto</th>
                                                        <th class="text-center bold" width="15%">Fecha</th>

                                                    </tr>
                                                    <?php
                                                    $estadoMinisterio = 'Adjudicado';
                                                    $tempMinisDet = 0;
                                                    $tabladet = dropDownList((object) ['method' => 'ministerioOxiTablaDet', 'entidad' => $key->nombre, 'estado' => $estadoMinisterio]);
                                                    ?>
                                                    <?php foreach ($tabladet as $key) { ?>
                                                        <tr>
                                                            <td class="text-center"><?php $tempMinisDet++;
                                                                                    echo ($tempMinisDet) ?></td>
                                                            <td class="text-center"><a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip; ?>' target="blank"><?php echo $key->snip; ?></td>
                                                            <td class="text-left"><?php echo ucfirst($key->nombre_proyecto) ?></td>
                                                            <td class="text-left"><?php echo ($key->empresa) ?></td>
                                                            <td class="text-right"><?php echo ($key->monto_inversion) ?></td>
                                                            <td class="text-center"><?php echo ($key->fecha_buena_pro) ?></td>

                        </td>

                    <?php } ?>
                    </thead>
            </table>
        </div>
    </div>
    </div>
    </div>
    </tr>
<?php } ?>
<tr>
    <td class="text-center"></td>

    <td class="text-center"></td>
    <td class="text-left"><b>TOTAL</b></td>
    <td class="text-right"><?php echo ($temp2) ?></td>
    <td class="text-right"><?php echo ($temp3) ?></td>
    <td class="text-right"><?php echo ($temp4) ?></td>
</tr>
<tr>
    <td class="text-center"></td>

    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
    <td class="text-right"></td>
</tr>
</table>
</div>
<div class="offset-md-2 col-lg-8">
    <h3><b>Leyenda</b></h3>
    <table class="table table-sm table-detail">
        <tr>
            <td class="text-center bold" style="width:20%"><b> * </b></th>
            <td class="text-center ">Al Primer Trimestre (2019)</td>
        </tr>
        <tr>
            <td class="text-center bold" style="width:20%">Nota</th>
            <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2017</td>
        </tr>
    </table>
</div>
</div>
<br>
<div class="row">
    <div class="col-12">
        <div id="chartContainerUniversidades1" style="height: 320px; width: 100%"></div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            Universidades Nacionales con OxI Concluidos 2015 - 2019 (Millones de Soles)
        </div>
    </div>
    <div class="col-lg-12">
        No posee Ninguna Obra por Impuesto Concluida
        <div class="offset-md-2 col-lg-8">
            <h3><b>Leyenda</b></h3>
            <table class="table table-sm table-detail">
                <tr>
                    <td class="text-center bold" style="width:20%"><b> * </b></th>
                    <td class="text-center ">Al Primer Trimestre (2019)</td>
                </tr>
                <tr>
                    <td class="text-center bold" style="width:20%">Nota</th>
                    <td class="text-center ">No existen registros de OxI desde el año 2009 al año 2019</td>
                </tr>
            </table>
        </div>
    </div>
</div>
<br>

<?php
} else if ($data->mostrar == 'politica-tab') {
?>


    <div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                Resultado de Elecciones de Congresistas por Partido Político
                <br>

            </div>
            <div class="col-lg-12" style="text-align:center">
                (Elecciones Presidenciales 2016)
            </div>
        </div>
        <div class="col-lg-6">
            <?php $map3 = dropDownList((object) ['method' => 'partidosPoliticos']); ?>
            <table class="table table-sm table-detail">
                <tr>
                    <th class="text-center bold" width="5%"><input type="checkbox" id="cbox_1" name="checkbox_PartidoTotal" checked></th>
                    <th class="text-center bold" width="5%">#</th>
                    <th class="text-center bold" width="80%">Partido Politico</th>
                    <th class="text-center bold" width="5%">Congresistas</th>
                    <th class="text-center bold" width="30%">Votantes</th>
                </tr>
                <?php $j = 0;
                foreach ($map3 as $key) {
                    $j++; ?>
                    <tr>
                        <td class="text-center"><input type="checkbox" id="cbox_1<?php echo $key->nombre ?>" name="checkbox_Partido" checked></td>
                        <td class="text-center"><?php echo ($j) ?></td>
                        <td class="text-left"><?php echo ucfirst(strtolower($key->nombre)) ?></td>
                        <td class="text-center">
                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo ($key->id) ?>" href="#" onclick="App.events(this); return false;">
                                <?php echo $key->cantidad; ?>
                            </a>
                        </td>
                        <td class="text-right"><?php echo number_format($key->poblacion) ?></td>
                    </tr>
                    <tr data-target="lnkProvXrutas_<?php echo ($key->id) ?>" style="display: none;">
                        <td colspan="5">
                            <div class="card">
                                <div class="card-header card-special">
                                    Relación de Congresistas por Partido Político
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table table-sm table-detail">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center bold" width="5%">#</th>
                                                        <th class="text-center bold" width="35%">Apellidos</th>
                                                        <th class="text-center bold" width="45%">Población</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $distXoleoducto = dropDownList((object) ['method' => 'congresistasXPartido', 'descripcion' => $key->id]);
                                                    $i = 0;
                                                    foreach ($distXoleoducto as $item) {
                                                        $i++;
                                                    ?>
                                                        <tr>
                                                            <td class="text-left"><?php echo ($i) ?></td>
                                                            <td class="text-left"><?php echo ($item->apellidos) ?></td>
                                                            <td class="text-right"><?php echo number_format($item->votantes) ?></td>
                                                        </tr>
                                                    <?php } ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                <?php }  ?>
                <tr>
                    <th class="text-center bold" width="5%">Total</th>
                    <th class="text-center bold" width="5%"></th>
                    <th class="text-center bold" width="5%"></th>
                    <th class="text-right bold" width="50%">130</th>
                    <th class="text-center bold" width="50%">3,953,178</th>
                </tr>
            </table>
            <h6>Fuente: <a href="http://www.congreso.gob.pe/pleno/congresistas/" target="blank">http://www.congreso.gob.pe/pleno/congresistas/</a></h6>
        </div>
        <div class="col-lg-6">
            <div id="map3" style="height:450px; width:100%;"></div>
        </div>

    </div>
    <br>
    <div id="divCongresistas">
        <img src="assets/app/img/loading.gif" alt="Smiley face" height="42" width="42">
    </div>



<?php
} else if ($data->mostrar == 'proyecto-tab') {
?>
    <div id="proyectosrpd"> </div>

<?php
} else if ($data->mostrar == 'oportunidad-tab') {
?>
    <div id="proyectosopo"> </div>

<?php
}
?>