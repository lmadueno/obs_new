
<?php
	require 'php/app.php';
	$minMaxProjects = dropDownList((object) ['method' => 'minMaxProjects']);
	$PartidoPolitico = dropDownList((object) ['method' => 'PartidoPolitico']);
?>

<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-lg-10">
                    <img src="assets/app/img/logo_lateral.png" alt="" width="185">
                </div>
                <div class="col-lg-2" style="text-align:right">
                    <a href="#" class="closeSidebar" onclick="App.events(this);" >
                        <img src="assets/app/img/left.jpg" alt="" width="30" height="30" style="margin-top:1em" class="float-right">
                    </a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card card-outline-info">
                <div class="card-header">

                    <div class="row">
                        <div class="col-10"><h6 class="m-b-0 text-white">FILTRO DE PROYECTOS</h6></div> 
                        <div class="col-2 text-right">
						    <a href="#" class="lnkAmpliar"  data-event="lnk_proyectFiltro"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down  text-white"></i></a>
					    </div>
                    </div>
                </div>
                <div class="card-body" data-target="lnk_proyectFiltro" style="display:none">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <span><small>Proyectos de inversión registrados desde Enero 2015</small> </span>
								<br>
                                <label>Región</label>
                                <?php $ddlRegion = dropDownList((object) ['method' => 'coddpto']); 
								?>
								<select id="ddlRegion" title="Región" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlRegion); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlRegion" multiple>					
									<?php foreach ($ddlRegion as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>
								</select>
                            </div>
						</div>
                        <div class="col-lg-12">
                            <div class="form-group">
								<label>Provincia</label>
								<select id="ddlProvincia" title="Provincia" class="form-control form-control-sm selectpicker" data-val="" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlProvincia" data-max-options="20" multiple>
									<?php
										$ddlProvincias = dropDownList((object) ['method' => 'provincias']);	
										foreach($ddlProvincias as $ddlProvincia){
									?> 
							
									<optgroup label="<?php echo $ddlProvincia->nomdpto;?>">
									<?php
										$items  = explode(",", $ddlProvincia->codubigeo);
										$values = explode(",", $ddlProvincia->nomubigeo);
											
										foreach($items as $index => $item){
									?>
											<option value="<?php echo $items[$index]; ?>"><?php echo $values[$index]; ?></option>
									<?php } ?>
										</optgroup>	
										
									<?php } ?>
									
								</select>
							</div>           
                        </div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Estado Ejecucion</label>
								<?php $ddlEstado = dropDownList((object) ['method' => 'codEstado']);?>
								<select id="ddlEstado" title="Estado" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlEstado); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlEstado" multiple >									
									<?php foreach ($ddlEstado as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>									
								</select>
							</div>			
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Nivel de Gobierno</label>
								<?php $ddlGobierno = dropDownList((object) ['method' => 'codGobierno']);?>
								<select id="ddlGobierno" title="Gobierno" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlGobierno); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlGobierno" multiple >									
									<?php foreach ($ddlGobierno as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>									
								</select>
							</div>			
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Tipo de Formato</label>
								<?php $ddlTipoformato = dropDownList((object) ['method' => 'codTipoFormato']);?>
								<select id="ddlTipoformato" title="Formato" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlTipoformato); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlTipoformato" multiple >									
									<?php foreach ($ddlTipoformato as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>									
								</select>
							</div>			
						</div>
						<div class="col-lg-12">
							<div class="form-group">
								<label>Situacion del Proyecto</label>
								<?php $ddlSituacion = dropDownList((object) ['method' => 'codSituacion']);?>
								<select id="ddlSituacion" title="Situacion" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlSituacion); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlSituacion" multiple >									
									<?php foreach ($ddlSituacion as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>									
								</select>
							</div>			
						</div>
                        <!-- <div class="col-lg-12">
							<div class="form-group">
								<label>Fases</label>
								<?php $ddlFases = dropDownList((object) ['method' => 'codfase']);?>
								<select id="ddlFases" title="Fases" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlFases); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlFases" multiple disabled>									
									<?php foreach ($ddlFases as $key){ ?>
										<option value="<?php echo $key->codigo; ?>" data-content="<i class='fa fa-circle <?php echo $key->glosafase; ?>'></i> <?php echo $key->valor; ?>"></option>
									<?php } ?>									
								</select>
							</div>			
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Nivel</label>
								<?php $ddlNivel = dropDownList((object) ['method' => 'codnivelgobierno']);?>
								<select id="ddlNivel" title="Nivel" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlNivel); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlNivel" multiple>
									
									<?php foreach ($ddlNivel as $key){ ?>
										<option value="<?php echo $key->codigo; ?>" data-content="<i class='fa <?php echo $key->glosanivelgobierno; ?>'></i> <?php echo $key->valor; ?>"></option>
									<?php } ?>
								</select>
							</div>
						</div> -->
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Función</label>
								<?php $ddlFuncion = dropDownList((object) ['method' => 'codfuncionproyecto']);?>
								<select id="ddlFuncion" title="Función" class="form-control form-control-sm selectpicker" data-val="" data-size="10" data-live-search="true" data-actions-box="true" data-key="ddlFuncion" multiple>					
									<?php foreach ($ddlFuncion as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>División Funcional</label>
								<select id="ddlPrograma" title="División Funcional" class="form-control form-control-sm selectpicker" data-val="" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlPrograma" data-max-options="20" multiple disabled>
									<?php										
										$ddlProgramas = dropDownList((object) ['method' => 'ProgramaXfuncion']);
										foreach($ddlProgramas as $ddlPrograma){
											var_dump($ddlPrograma);
									?>
										<optgroup label="<?php echo $ddlPrograma->nomfuncion; ?>">
									<?php
										$items  = explode(",", $ddlPrograma->codprograma);
										$values = explode(",", $ddlPrograma->nomprograma);
											
										foreach($items as $index => $item){
									?>
											<option value="<?php echo $items[$index]; ?>"><?php echo $values[$index]; ?></option>
									<?php } ?>
										</optgroup>	
									<?php } ?>
								</select>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Grupo Funcional</label>
					
								<select id="ddlSubPrograma" title="Grupo Funcional" class="form-control form-control-sm selectpicker" data-val="" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlSubPrograma" data-max-options="20" multiple disabled>
								<?php										
										$ddlSubPrograma = dropDownList((object) ['method' => 'SubProgramaXfuncion']);
										foreach($ddlSubPrograma as $ddlSubPrograma){
											var_dump($ddlSubPrograma);
									?>
										<optgroup label="<?php echo $ddlSubPrograma->nomprogram; ?>">
									<?php
										$items  = explode(",", $ddlSubPrograma->codsubprograma);
										$values = explode(",", $ddlSubPrograma->nomsubprograma);
											
										foreach($items as $index => $item){
									?>
											<option value="<?php echo $items[$index]; ?>"><?php echo $values[$index]; ?></option>
									<?php } ?>
										</optgroup>	
									<?php } ?>		
								</select>
							</div>
						</div>
                        <div class="col-lg-12" style="display: none;">
							<div class="form-group">
								<label>Estado OXI</label>
								<select id="ddlEstadoOxi" title="Origen" class="form-control form-control-sm selectpicker" data-val="" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlEstadoOxi" multiple style="display: none;">
									<option value="1">Priorizados</option>		
									<option value="2">Adjudicado</option>	
									<option value="3">Concluido</option>								
								</select>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Monto PIP (Máx. <?php echo number_format($minMaxProjects{0}->max_monto);?> millones)</label>
								<div class="input-group">
									<input type="text" id="txtMontoIni" data-key="txtMontoAsignado" class="form-control form-control-sm form-control-money text-right" name="start" placeholder="Desde (S/.)" data-value="0" value="0">	
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtMontoFin" data-key="txtMontoAsignado" class="form-control form-control-sm form-control-money text-right" name="end" placeholder="Hasta (S/.) " data-value="<?php echo $minMaxProjects{0}->max_monto;?>" value="<?php echo $minMaxProjects{0}->max_monto;?>">					
								</div>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Monto tope CIPRL (Máx. <?php echo number_format($minMaxProjects{0}->max_tope_cipril);?> millones)</label>
								<div class="input-group">
									<input type="text" id="txtMontoCIPRLIni" data-key="txtMontoCIPRL" class="form-control form-control-sm form-control-money text-right" placeholder="Desde (S/.)" data-value="<?php echo $minMaxProjects{0}->min_tope_cipril;?>" value="0">	
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtMontoCIPRLFin" data-key="txtMontoCIPRL" class="form-control form-control-sm form-control-money text-right" placeholder="Hasta (S/.)" data-value="<?php echo $minMaxProjects{0}->max_tope_cipril;?>" value="<?php echo $minMaxProjects{0}->max_tope_cipril;?>">
								</div>
							</div>
						</div>
                        <div class="col-lg-12">
							<div class="form-group">
								<label>Población (Máx. <?php echo number_format($minMaxProjects{0}->max_poblacion);?> miles)</label>								
								<div class="input-group">
									<input type="text" id="txtPoblacionIni" data-key="txtPoblacion" class="form-control form-control-sm form-control-money text-right" placeholder="Desde" data-value="0" value="0">	
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtPoblacionFin" data-key="txtPoblacion" class="form-control form-control-sm form-control-money text-right" placeholder="Hasta" data-value="<?php echo $minMaxProjects{0}->max_poblacion;?>" value="<?php echo $minMaxProjects{0}->max_poblacion;?>">									
								</div>
							</div>
						</div>
                        <div class="col-lg-12 text-center">
							<button type="button" id="btnFiltrar" class="btn btn-sm btn-info waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-check"></i>
								Filtrar
							</button>
							<button type="button" id="btnLimpiar" class="btn btn-sm btn-danger waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-close"></i>
								Limpiar
							</button>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>

    <div id="divTableexcelfiltroProycts">
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card card-outline-info">
				<div class="card-header">
					<div class="row">
                        <div class="col-10"><h6 class="m-b-0 text-white">FILTRO DE EMPRESAS</h6></div> 
                        <div class="col-2 text-right">
						    <a href="#" class="lnkAmpliar"  data-event="lnk_EmpresaFiltro"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down  text-white"></i></a>
					    </div>
                    </div>
				</div>
				<div class="card-body" data-target="lnk_EmpresaFiltro" style="display:none">
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Región</label>	
								<select id="ddlRegion2" title="Región" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlRegion); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlRegion2" multiple>					
									<?php foreach ($ddlRegion as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Tamaño</label>
								<?php $ddlTamano2 = dropDownList((object) ['method' => 'codtamano']);?>
								<select id="ddlTamano2" title="Tamaño Empresa" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlTamano2);?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlTamano2" multiple>					
									<?php foreach ($ddlTamano2 as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo strtoupper ($key->valor); ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Sector</label>
								<?php $ddlSector2 = dropDownList((object) ['method' => 'codsector']);?>
								<select id="ddlSector2" title="Sector" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlSector2); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlSector2" multiple>					
									<?php foreach ($ddlSector2 as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Grupo</label>
								<?php $ddlGrupo = dropDownList((object) ['method' => 'codgrupo']);?>
								<select id="ddlGrupo2" title="Grupo Económico" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlGrupo); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlGrupo2" multiple>					
									<?php foreach ($ddlGrupo as $key){ ?>
										<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-9">	
									<label>Listado en Bolsa</label>
								</div>
								<div class="col-lg-3">	
									<input type="checkbox" id="txtradioBuena1" value="REGULADA" data-key="txtradioBuena1"  class="form-control form-control-sm" name="txtradioBuena1">
								</div>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-9">	
									<label>Participacion OxI</label>
								</div>
								<div class="col-lg-3">	
									<input type="checkbox" id="txtradioOxI" value="1" data-key="txtradioOxI"  class="form-control form-control-sm" name="txtradioOxI">
								</div>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-9">	
									<label>Buen Contribuyente</label>
								</div>
								<div class="col-lg-3">	
									<input type="checkbox" id="txtrasContribuyente" value="REGULADA" data-key="txtrasContribuyente" class="form-control form-control-sm" name="txtrasContribuyente">
								</div>	
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>RUC (11 digitos)</label>
								<input type="text" id="txtRuc" data-key="txtRuc" class="form-control form-control-sm" placeholder="Ingrese numero de R.U.C.">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Razon Social</label>
								<input type="text" id="txtRazonSocial" data-key="txtRazonSocial" class="form-control form-control-sm"  placeholder="Ingrese razón Social">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Ingresos (Máx. 16,321 millones)</label>
								<div class="input-group">
									<input type="text" id="txtIngresosIni" data-key="txtIngresos" class="form-control form-control-sm" value="0" data-value="0">
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtIngresosFin" data-key="txtIngresos" class="form-control form-control-sm" value="16320" data-value="16320">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Utilidad neta (Máx. 3,780 millones)</label>
								<div class="input-group">
									<input type="text" id="txtUtilIni" data-key="txtUtil" class="form-control form-control-sm" value="0" data-value="0">
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtUtilFin" data-key="txtUtil" class="form-control form-control-sm" value="3779" data-value="3779">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Rango OxI (Máx. 567 millones)</label>
								<div class="input-group">
									<input type="text" id="txtOxiIni" data-key="txtOxi" class="form-control form-control-sm" data-value="0" value="0">
									<span class="input-group-addon b-0">&nbsp;</span>							
									<input type="text" id="txtOxiFin" data-key="txtOxi" class="form-control form-control-sm" data-value="566" value="566">
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Radio de Influencia</label>
								<input type="text" id="txtradioInfl" class="form-control form-control-sm" name="start" placeholder="kilometros (Km)">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<button type="button" id="btnFiltrar2" class="btn btn-sm btn-info waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-check"></i>
								Filtrar
							</button>
							<button type="button" id="btnLimpiar2" class="btn btn-sm btn-danger waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-close"></i>
								Limpiar
							</button>
						</div>
					</div>					
				</div>						
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="card card-outline-info">
				<div class="card-header">
					<div class="row">
                        <div class="col-10"><h6 class="m-b-0 text-white">FILTRO DE E. PRESIDENCIALES 2016</h6></div> 
                        <div class="col-2 text-right">
						    <a href="#" class="lnkAmpliar"  data-event="lnk_PRESIDENCIALFiltro"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down  text-white"></i></a>
					    </div>
                    </div>
				</div>
				<div class="card-body" data-target="lnk_PRESIDENCIALFiltro" style="display:none">
					
					
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">	
									<label>Región</label>
								</div>		
								<div class="col-lg-9">	
									<label>Lima - Metropolitana</label>
								</div>
								<div class="col-lg-3">	
									<input type="checkbox" id="txtLimaMML" value="si" data-key="txtLimaMML"  class="form-control form-control-sm" name="txtLimaMML">
								</div>	
								<div class="col-lg-9">	
									<label>Lima - Provincias</label>
								</div>
								<div class="col-lg-3">	
									<input type="checkbox" id="txtLimaProvincia" value="si" data-key="txtLimaProvincia"  class="form-control form-control-sm" name="txtLimaProvincia">
								</div>	
								<div class="col-lg-12">	
									<select id="ddlRegion3" title="Región" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlRegion); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlRegion3" multiple>
                                    	<?php foreach ($ddlRegion as $key){ ?>
                                        	<option value="<?php echo $key->codigo; ?>"><?php echo $key->valor; ?></option>
                                    	<?php } ?>
                               		</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Provincia</label>
								<select id="ddlProvincia2" title="Provincia" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($ddlProvincia); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlProvincia2" data-max-options="20" multiple>
									<?php
										$ddlProvincias = dropDownList((object) ['method' => 'provincias']);	
										foreach($ddlProvincias as $ddlProvincia){
									?>
										<optgroup label="<?php echo $ddlProvincia->nomdpto; ?>">
									<?php
										$items  = explode(",", $ddlProvincia->codubigeo);
										$values = explode(",", $ddlProvincia->nomubigeo);
											
										foreach($items as $index => $item){
									?>
											<option value="<?php echo $items[$index]; ?>"><?php echo $values[$index]; ?></option>
									<?php } ?>
										</optgroup>	
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label>Partidos Políticos</label>
								<select id="ddlPartidosPoliticos" title="Partidos Políticos" class="form-control form-control-sm selectpicker" data-val="<?php echo listJoin($PartidoPolitico); ?>" data-size="10"  data-live-search="true" data-actions-box="true" data-key="ddlPartidosPoliticos" multiple>
									<?php 
									   
										foreach ($PartidoPolitico as $key){ ?>
										<option value="<?php echo $key->nombre_key; ?>"><?php echo $key->nombre_referencia; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					
                    <div class="row">
                        <div class="col-lg-12">
                        	<label>Filtros</label>
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Primer Lugar</label>
                                </div>
                                <div class="col-lg-3">
                                    <input type="radio" id="txtradioPrimerLugar" value="REGULADA" data-key="txtradioPrimerLugar"  class="form-control form-control-sm" name="txtradioPuesto" checked>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Segundo Lugar</label>
                                </div>
                                <div class="col-lg-3">
                                    <input type="radio" id="txtradioSegundoLugar" value="REGULADA" data-key="txtradioSegundoLugar"  class="form-control form-control-sm" name="txtradioPuesto">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label>Tercer Lugar</label>
                                </div>
                                <div class="col-lg-3">
                                    <input type="radio" id="txtradioTercerLugar" value="REGULADA" data-key="txtradioTercerLugar"  class="form-control form-control-sm" name="txtradioPuesto">
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<button type="button" id="btnFiltrar3" class="btn btn-sm btn-info waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-check"></i>
								Filtrar
							</button>
							<button type="button" id="btnLimpiar3" class="btn btn-sm btn-danger waves-effect waves-light" onclick="App.events(this)">
								<i class="fa fa-close"></i>
								Limpiar
							</button>
						</div>
					</div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				</div>
			</div>
		</div>
	</div>
</div>