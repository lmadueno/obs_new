<?php
	require 'php/app.php';
	

			$data   = json_decode($_GET['data']);
			$obj    = dropDownList((object) ['method' => 'DatosXubigeo', 'codnivel' => $data->idnivel, 'codUbigeo' => $data->codubigeo]);
			$obj = $obj{0};
?>


<style>
		.bold{
			font-weight: bold;
			background: #1976d226;
			color: #000;
		}
		.vert-middle{
			vertical-align: middle !important;
		}
</style>
	<div class="card card-outline-info">
		<div class="card-header">
		    <div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white"><?php echo $data->nomubigeo?></h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarEntidad" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarEntidad" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
        </div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-12">
					<table class="table table-sm">
						<tr>
							<td class="bold">Región</td>
							<td>
								<?php
									echo ucwords(strtolower($obj->region));
									$route = 'assets/app/pdf/' . $data->idnivel . '_';
									if($data->idnivel == 1){
								?>
									<a href="pdf.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank" title="Ficha Socioeconómica"><img src="assets/app/img/pdf.png"/></a>
									<?php if (file_exists($route . $data->codubigeo .'.pdf')) { ?>
										<a href="<?php echo $route . $data->codubigeo . '.pdf';?>" target="_blank" title="Brechas"><img src="assets/app/img/pdf.png"/></a>
									<?php } ?>															
								<?php } ?>
							</td>
							<td class="align-middle bold">Ubigeo</td>
							<td class="text-center">
								<?php 
									if($data->idnivel == 1){
										echo $obj->ubigeo_dept;
									} elseif ($data->idnivel == 2) {
										echo $obj->ubigeo_prov;
									} elseif ($data->idnivel == 3) {
										echo $obj->ubigeo_dist;
									}
								?>
							</td>
						</tr>
						<tr>
							<td class="bold">Provincia</td>
							<td>
								<?php echo ucwords(strtolower($obj->provincia)); ?>								
								<?php if($data->idnivel == 2){ ?>
									<a href="pdf.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank" title="Ficha Socioeconómica"><img src="assets/app/img/pdf.png"/></a>
									<?php if (file_exists($route . $data->codubigeo . '00.pdf')) { ?>
										<a href="<?php echo $route . $data->codubigeo . '00.pdf';?>" target="_blank" title="Brechas"><img src="assets/app/img/pdf.png"/></a>
									<?php } ?>									
								<?php } ?>
							</td>
							<td class="bold">Distrito</td>
							<td><?php echo ucwords(strtolower($obj->distrito)); ?>
							
								<?php if($data->idnivel == 3){ ?>
									<a href="pdf.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank" title="Ficha Socioeconómica"><img src="assets/app/img/pdf.png"/></a>
									<?php if (file_exists($route . $data->codubigeo . '.pdf')) { ?>
										<a href="<?php echo $route . $data->codubigeo . '.pdf';?>" target="_blank" title="Brechas"><img src="assets/app/img/pdf.png"/></a>
									<?php } ?>									
								<?php } ?>
							</td>
						</tr>
						<tr>
							<td class="bold">Autoridad</td>
							<td><?php echo ucwords(strtolower($obj->autoridad)); ?></td>
							<td class="bold">Cargo</td>
							<td><?php echo ucfirst(strtolower($obj->cargo)); ?></td>
						</tr>
						<tr>
							<td class="bold">Población</td>
							<td class="text-right"><?php echo number_format($obj->poblacion); ?> (<?php echo number_format((float)$obj->porc_poblacion, 2, '.', '')?>%)</td>
							<td class="bold">Población Nac.</td>
							<td class="text-right"><?php echo number_format($obj->poblacion_n); ?></td>
						</tr>
						<tr>
							<td class="bold">Electores</td>
							<td class="text-right"><?php echo number_format($obj->electores); ?> (<?php echo number_format((float)$obj->porc_electores, 2, '.', '')?>%)</td>
							<td class="bold">Electores Nac.</td>
							<td class="text-right"><?php echo number_format($obj->electores_n); ?></td>
						</tr>
						<tr>
							<td class="bold">Votantes</td>
							<td class="text-right"><?php echo  number_format($obj->votos); ?> (<?php echo number_format((float)$obj->porc_votos, 2, '.', '')?>%)</td>
							<td class="bold">Partido</td>
							<td class="text-right"><?php echo ucwords(strtolower($obj->partido)); ?></td>
							
						</tr>				
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Evolución de Canon y Tope CIPRL
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<div id="chart_<?php echo  $data->codubigeo;?>" style="min-width: 310px; height: 220px; margin: 0 auto"></div>	
								</div>
							</div>														
						</div>
						<div class="card-footer text-center">
							<a class="lnkAmpliar" data-event="lnkCanonCiprl" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar tabla resumen
							</a>
						</div>
						<div data-target="lnkCanonCiprl" class="card-body" style="display: none;">
							<div class="row">
								<div class="offset-md-2 col-lg-8">
									<table class="table table-sm table-detail">										
										<?php $ciprlXregion = dropDownList((object) ['method' => 'ciprlXregionDet', 'idnivel' => $data->idnivel, 'codubigeo' =>  $data->codubigeo]);?>
										<thead>										
											<tr>
												<th class="text-center bold">Año</th>
												<th class="text-center bold">PIM MEF</th>
												<th class="text-center bold">Canon MEF</th>
												<th class="text-center bold">Ciprl MEF</th>												
												<th class="text-center bold" title="R1: Regla Fiscal del Saldo de Deuda Total">Regla Fiscal R1</th>
												<th class="text-center bold" title="R2: Regla Fiscal de Ahorro en Cuenta Corriente">Regla Fiscal R2</th>												
											</tr>	
										</thead>
										<tbody>
										<?php foreach ($ciprlXregion as $key){ ?>
											
											<tr>
												<td class="text-right"><?php echo $key->ano?></td>
												<?php if($key->ano == 2020){?>
													<td class="text-right"><?php echo number_format($key->pim)?></td>
													<td class="text-right"><?php echo number_format($key->canon)?></td>
													<td class="text-right"><?php echo number_format($key->ciprl)?></td>												
													<td class="text-center"><?php echo strlen($key->cumple_r1) > 0 ? $key->cumple_r1 : '-'?></td>
													<td class="text-center"><?php echo strlen($key->cumple_r2) > 0 ? $key->cumple_r2 : '-'?></td>
												<?php }else if ($key->ano == 2021){?>
													<td class="text-right"><?php echo number_format($key->pim)?></td>
													<td class="text-right"><?php echo number_format($key->canon)?></td>
													<td class="text-right">*<?php echo number_format($key->ciprl)?></td>												
													<td class="text-center"><?php echo strlen($key->cumple_r1) > 0 ? $key->cumple_r1 : '-'?>*</td>
													<td class="text-center"><?php echo strlen($key->cumple_r2) > 0 ? $key->cumple_r2 : '-'?>*</td>
												<?php } else {?>	
													<td class="text-right"><?php echo number_format($key->pim)?></td>
													<td class="text-right"><?php echo number_format($key->canon)?></td>
													<td class="text-right"><?php echo number_format($key->ciprl)?></td>												
													<td class="text-center"><?php echo strlen($key->cumple_r1) > 0 ? $key->cumple_r1 : '-'?></td>
													<td class="text-center"><?php echo strlen($key->cumple_r2) > 0 ? $key->cumple_r2 : '-'?></td>
												<?php }?>										
											</tr>
										<?php } ?>
										</tbody>
									</table>			
								</div>	
							</div>
							<div class="row">
								<div class="col-lg-12">
									<table class="table table-sm table-detail">										
										<?php $reglasFiscales = dropDownList((object) ['method' => 'reglasFiscales', 'codnivel' => $data->idnivel, 'codubigeo' => $obj->codubigeo]);?>
										<thead>
											<tr>
												<th class="text-center bold vert-middle" colspan="6" style=" background: #2f7ed8; color: white; ">R1: Regla Fiscal del Saldo de Deuda Total</th>
												<th></th>
												<th class="text-center bold vert-middle" colspan="3" style=" background: #2f7ed8; color: white; ">R2: Regla Fiscal de Ahorro en Cuenta Corriente</th>
											</tr>											
											<tr>
												<th class="text-center bold vert-middle">Año</th>
												<th class="text-center bold vert-middle">Saldo de Deuda Total <sup>1/</sup></th>
												<th class="text-center bold vert-middle">Promo. ICT 2015-2018</th>
												<th class="text-center bold vert-middle">Límite Ley N° 29230 <sup>2/</sup></th>
												<th class="text-center bold vert-middle">Espacio Total</th>
												<th class="text-center bold vert-middle">SDT/Prom. ICT 2015-2018 %</th>
												<th style="border-bottom: 2px solid #dee2e600;"></th>
												<th class="text-center bold vert-middle" title="Ingreso Corriente Total">Ingreso Corriente Total</th>
												<th class="text-center bold vert-middle">Gasto Corriente Total</th>
												<th class="text-center bold vert-middle">Ahorro en Cta. Corriente</th>
											</tr>	
										</thead>
										<tbody>
										<?php foreach ($reglasFiscales as $key){ ?>
											<tr>
												<?php if($key->anio==2021){ ?>
													<td class="text-center"><?php echo $key->anio?>*</td>
												<?php }	else {?>
													<td class="text-center"><?php echo $key->anio?></td>
												<?php } ?>
												
												<td class="text-right"><?php echo number_format($key->deuda)?></td>
												<td class="text-right"><?php echo number_format($key->ict)?></td>
												<td class="text-right"><?php echo number_format($key->limite)?></td>
												<td class="text-right"><?php echo number_format($key->espacio)> 0 ? $key->cumple_r1 : '-'?></td>
												<td class="text-right"><?php echo number_format($key->sdt)?>%</td>											
												<td></td>
												<td class="text-right"><?php echo number_format($key->ingreso_corriente)?></td>
												<td class="text-right"><?php echo number_format($key->gasto_corriente)?></td>
												<td class="text-right"><?php echo number_format($key->ahorro)?></td>
											</tr>
										<?php } ?>
										</tbody>
									</table>
									<div class="offset-md-2 col-lg-6">
										<h3>Leyenda</h3>
										<table class="table table-sm table-detail">																										
											<tr>
												<td class="text-center bold"><b> - </b></td>
												<td class="text-center ">Sin Información Disponible</td>																								
											</tr>	
											<tr>
												<td class="text-center bold"><b> *  </b></th>
												<td class="text-center ">Junio del 2021
												</td>																								
											</tr>
																							
									</table>	
											
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<br>
			<?php $proyPriorizados = dropDownList((object) ['method' => 'proyPriorizados', 'codubigeo' => $obj->codubigeo]);?>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Lista de Proyectos Priorizados 2009 - 2021 (Millones de Soles)
						</div>
						<?php if(count($proyPriorizados) > 0){ ?>
						<div class="card-body">
							<div class="scrollable" style="overflow-y: auto;max-height: 240px;">								
								<?php foreach ($proyPriorizados as $key){ ?>
									<table class="table table-sm">
										<tr>
											<td class="bold" width="25%">Nombre del Proyecto</td>
											<td colspan="3" align="justify"><?php echo ucfirst($key->nombre_proyecto); ?></td>																			
										</tr>
										<tr>
										<td class="bold"><a href='https://ofi5.mef.gob.pe/invierte/formato/verProyectoCU/<?php echo $key->coigounico;?>' target="blank">Codigo Unico</a> <a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->coigounico;?>' target="blank">(SNIP)</a></td>
											<td width="20%"><?php echo $key->coigounico; ?></td>
											<td class="bold" width="20%">Sector</td>
											<td><?php echo $key->sector; ?></td>
										</tr>
										
										<tr>
											<td class="bold">Monto de Inversión (S/.)</td>
											<td class="text-right"><?php echo $key->monto; ?>MM</td>
											<td class="bold">Acuerdo</td>
											<td><?php echo $key->acuerdo; ?></td>
										</tr>
										
									</table>
								<?php } ?>
							</div>
						</div>
						
							<?php } else{ ?>

							<div class="card-body">
								La entidad no tiene priorizado ningún proyecto OxI a la actualidad.
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>	 
			<br>										
			<?php $proyAdjudicados = dropDownList((object) ['method' => 'proyAdjudicados', 'codubigeo' => $obj->codubigeo]);?>
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Lista de Proyectos Adjudicados y Concluidos 2009 - 2021 (Millones de Soles)
						</div>
						<?php if(count($proyAdjudicados) > 0){ ?>
						<div class="card-body">
							<div class="scrollable" style="overflow-y: auto;max-height: 240px;">								
								<?php foreach ($proyAdjudicados as $key){ ?>
									<table class="table table-sm">
										<tr>
											<td class="bold" width="25%">Nombre del Proyecto</td>
											<td colspan="3" align="justify"><?php echo strtoupper ($key->nombre_proyecto); ?></td>																			
										</tr>
										<tr>
											<td class="bold"><a href='https://ofi5.mef.gob.pe/invierte/ejecucion/traeListaEjecucionSimplePublica/<?php echo $key->snip;?>' target="blank">Codigo Unico</a> <a href='http://ofi4.mef.gob.pe/bp/ConsultarPIP/frmConsultarPIP.asp?accion=consultar&txtCodigo=<?php echo $key->snip;?>' target="blank">(SNIP)</a></td>
											<td width="20%"><?php echo $key->snip; ?></td>
											<td class="bold" width="20%">Empresa</td>
											<td><?php echo $key->empresa; ?></td>
										</tr>
										<tr>
											<td class="bold">Sector</td>
											<td><?php echo $key->sector; ?></td>
											<td class="bold">F. Buena Pro</td>
											<td><?php echo $key->fecha_buena_pro; ?></td>
										</tr>
										<tr>
											<td class="bold">Monto de Inversión</td>
											<td class="text-right"><?php echo $key->monto_inversion; ?>MM</td>
											<td class="bold">F. Firma de Convenio</td>
											<td><?php echo $key->fecha_firma; ?></td>
										</tr>
										<tr>
											<td class="bold">Población Beneficiada</td>
											<td class="text-right"><?php echo number_format($key->poblacion_beneficiada)	; ?></td>
											<td class="bold">Estado</td>
											<td><?php echo $key->estado; ?></td>
										</tr>
									</table>
								<?php } ?>
							</div>
						</div>
						
							<?php } else{ ?>

							<div class="card-body">
								La entidad no ha aplicado el mecanismo OxI.
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row" style="display: none;">
				<div class="col-lg-12">	
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Diferencia de fallecidos SINADEF por Quintiles <?php  if($data->idnivel == 1)  echo 'Región'. ' '.(($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.(($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.(($obj->distrito))?>
							(2020-2021)
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										La <?php  if($data->idnivel == 1)  echo 'Región'. ' '.(($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.(($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.(($obj->distrito))?> se encuentra en el Quintil
											<?php $rptaQuintlSinadef = dropDownList((object) ['method' => 'quintiles_sinadef_msj', 'ubigeo' => $data->codubigeo, 'idNivel' => $data->idnivel]);
												echo $rptaQuintlSinadef{0}->quantil;
											?>
										<a class="lnkAmpliarSalud" id="sinadef" href="#" onclick="App.events(this); return false;">
											ver mas detalle
										</a>
									</div>
								</div>
								<div class="col-lg-6" style="padding-top:2em;">
									<?php $quintilesSinadef = dropDownList((object) ['method' => 'quintiles_sinadef', 'idNivel' => $data->idnivel]);?>					
									<table class="table table-sm table-detail datatable">
										<thead>
											<tr>
												<th class="text-center bold">Quintl</th>
												<th class="text-center bold">Entidades</th>
												<th class="text-center bold">Fallecidos 2020</th>
												<th class="text-center bold">Fallecidos 2021</th>
												<th class="text-center bold">Diferencia Fallecidos</th>
												<th class="text-center bold">Población</th>
											</tr>	
										</thead>
										<tbody>									
											<?php 
												$suma1=0;
												$suma2=0;
												$suma3=0;
												$suma4=0;
												$suma5=0;
												foreach ($quintilesSinadef as $key){ 
													$suma1=$suma1+$key->entidades;
													$suma2=$suma2+$key->fallecidos2019;
													$suma3=$suma3+$key->fallecidos2020;
													$suma4=$suma4+$key->fallecidos;
													$suma5=$suma5+$key->poblacion;
												?>
												<tr>
													<td class="text-center">
														<a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->quantil?>" href="#" onclick="App.events(this); return false;">
															<?php echo ($key->texto)?>
														</a>
													</td>
													<td class="text-center"><?php echo number_format($key->entidades)?></td>
													<td class="text-center"><?php echo number_format($key->fallecidos2019)?></td>
													<td class="text-center"><?php echo number_format($key->fallecidos2020)?></td>
													<td class="text-center"><?php echo number_format($key->fallecidos)?></td>							
													<td class="text-center"><?php echo number_format($key->poblacion)?></td>									
												</tr>
												<tr data-target="lnkProvXrutas_<?php echo $key->quantil?>" style="display: none;">
													<td colspan="6">
														<div class="card">
															<div class="card-header card-special">
                                                                Entidades
                                                            </div>
															<div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
																		<table class="table table-sm table-detail datatable">
																			<tr>
																				<th class="text-center bold">#</th>
																				<th class="text-center bold">Entidades</th>
																				<th class="text-center bold">Fallecidos 2019</th>
																				<th class="text-center bold">Fallecidos 2020</th>
																				<th class="text-center bold">Diferencia Fallecidos</th>
																				<th class="text-center bold">Poblacion</th>
																			</tr>
																			<?php 
																			
																			$suma21=0;
																			$suma31=0;
																			$suma41=0;
																			$suma51=0;
																			$quintilesSinadef1 = dropDownList((object) ['method' => 'quintiles_sinadef_det', 'idNivel' => $data->idnivel,'quantil' => $key->quantil]);
																			foreach ($quintilesSinadef1 as $key1){ 
																		
																				$suma21=$suma21+$key1->fallecidos_2019;
																				$suma31=$suma31+$key1->fallecidos_2020;
																				$suma41=$suma41+$key1->diferencia;
																				$suma51=$suma51+$key1->poblacion;
																				
																			?>	
																			<tr>
																				<td class="text-center"><?php echo ($key1->id)?></td>
																				<td class="text-left"><?php echo ($key1->nomubigeo)?></td>
																				<td class="text-center"><?php echo number_format($key1->fallecidos_2019)?></td>
																				<td class="text-center"><?php echo number_format($key1->fallecidos_2020)?></td>
																				<td class="text-center"><?php echo number_format($key1->diferencia)?></td>
																				<td class="text-center"><?php echo number_format($key1->poblacion)?></td>							
																			</tr>
																			<?php } ?>
																			<tr>
																				<td class="text-center"></td>
																				<td class="text-center">Total</td>
																				<td class="text-center"><?php echo number_format($suma21)?></td>
																				<td class="text-center"><?php echo number_format($suma31)?></td>
																				<td class="text-center"><?php echo number_format($suma41)?></td>							
																				<td class="text-center"><?php echo number_format($suma51)?></td>									
																			</tr>
																		</table>	
																	</div>
																</div>
															</div>
														</div>
													</td>
												</tr>
											<?php } ?>
												<tr>
													
													<td class="text-center">Total</td>
													<td class="text-center"><?php echo number_format($suma1)?></td>
													<td class="text-center"><?php echo number_format($suma2)?></td>
													<td class="text-center"><?php echo number_format($suma3)?></td>
													<td class="text-center"><?php echo number_format($suma4)?></td>							
													<td class="text-center"><?php echo number_format($suma5)?></td>									
												</tr>
										</tbody>
									</table>	
								</div>
								<div class="col-lg-6">
									<div id="chartSinadefEntidad" style="min-width: 310px; height: 280px; margin: 0 auto"></div>			
								</div>
							</div>
							
						</div>		
					</div>			
				</div>		
			</div>
			<br>
			<div class="row" style="display: none;">
				<div class="col-lg-12">	
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Fallecidos COVID-19 por Quintiles <?php  if($data->idnivel == 1)  echo 'Región'. ' '.(($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.(($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.(($obj->distrito))?>
							(2020)
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-12">
									<div class="text-center">
										La <?php  if($data->idnivel == 1)  echo 'Región'. ' '.(($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.(($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.(($obj->distrito))?> se encuentra en el Quintil
											<?php $rptaQuintlSinadef = dropDownList((object) ['method' => 'quintiles_sinadef_msj', 'ubigeo' => $data->codubigeo, 'idNivel' => $data->idnivel]);
												echo $rptaQuintlSinadef{0}->quantil;
											?>
										<a class="lnkAmpliarSalud" id="covid" href="#" onclick="App.events(this); return false;">
											ver mas detalle
										</a>
									</div>
								</div>
								<div class="col-lg-6" style="padding-top:2em;">
									<?php $quintilesSinadef = dropDownList((object) ['method' => 'quintiles_covid', 'idNivel' => $data->idnivel]);?>					
									<table class="table table-sm table-detail datatable">
										<thead>
											<tr>
												<th class="text-center bold">Quintl</th>
												<th class="text-center bold">Entidades</th>
											
												<th class="text-center bold">Fallecidos 2020</th>
									
												<th class="text-center bold">Población</th>
											</tr>	
										</thead>
										<tbody>									
											<?php 
												$suma1=0;
												$suma4=0;
												$suma5=0;
												foreach ($quintilesSinadef as $key){ 
													$suma1=$suma1+$key->entidades;
												
													$suma4=$suma4+$key->fallecidos;
													$suma5=$suma5+$key->poblacion;
												?>
												<tr>
													<td class="text-center">
														<a class="lnkAmpliar" data-event="lnkProvXrutas1_<?php echo $key->quantil?>" href="#" onclick="App.events(this); return false;">
															<?php echo ($key->texto)?>
														</a>
													</td>
													<td class="text-center"><?php echo number_format($key->entidades)?></td>
													
													<td class="text-center"><?php echo number_format($key->fallecidos)?></td>							
													<td class="text-center"><?php echo number_format($key->poblacion)?></td>									
												</tr>
												<tr data-target="lnkProvXrutas1_<?php echo $key->quantil?>" style="display: none;">
													<td colspan="6">
														<div class="card">
															<div class="card-header card-special">
                                                                Entidades
                                                            </div>
															<div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-lg-12">
																		<table class="table table-sm table-detail datatable">
																			<tr>
																				<th class="text-center bold">#</th>
																				<th class="text-center bold">Entidades</th>
																			
																				<th class="text-center bold">Diferencia Fallecidos</th>
																				<th class="text-center bold">Poblacion</th>
																			</tr>
																			<?php 
																			
																			$suma21=0;
																			$suma31=0;
																			$suma41=0;
																			$suma51=0;
																			$quintilesSinadef1 = dropDownList((object) ['method' => 'quintiles_covid_det', 'idNivel' => $data->idnivel,'quantil' => $key->quantil]);
																			foreach ($quintilesSinadef1 as $key1){ 
																				$suma41=$suma41+$key1->diferencia;
																				$suma51=$suma51+$key1->poblacion;
																				
																			?>	
																			<tr>
																				<td class="text-center"><?php echo ($key1->id)?></td>
																				<td class="text-left"><?php echo ($key1->nomubigeo)?></td>
														
																				<td class="text-center"><?php echo number_format($key1->diferencia)?></td>
																				<td class="text-center"><?php echo number_format($key1->poblacion)?></td>							
																			</tr>
																			<?php } ?>
																			<tr>
																				<td class="text-center"></td>
																				<td class="text-center">Total</td>
																			
																				<td class="text-center"><?php echo number_format($suma41)?></td>							
																				<td class="text-center"><?php echo number_format($suma51)?></td>									
																			</tr>
																		</table>	
																	</div>
																</div>
															</div>
														</div>
													</td>
												</tr>
											<?php } ?>
												<tr>
													
													<td class="text-center">Total</td>
													<td class="text-center"><?php echo number_format($suma1)?></td>
													
													<td class="text-center"><?php echo number_format($suma4)?></td>							
													<td class="text-center"><?php echo number_format($suma5)?></td>									
												</tr>
										</tbody>
									</table>	
								</div>
								<div class="col-lg-6">
									<div id="chartCovidEntidad" style="min-width: 310px; height: 280px; margin: 0 auto"></div>			
								</div>
							</div>
							
						</div>		
					</div>			
				</div>		
			</div>					
			<br>
			<div class="row">
				<div class="col-lg-12">
					<?php $indCab = dropDownList((object) ['method' => 'indCabXubigeo', 'idNivel' => $data->idnivel, 'codUbigeo' => $data->codubigeo]);?>					
					<?php foreach ($indCab as $key){ ?>
						<?php $codInd = $key->idind; ?>
						<div class="card">
							<div class="card-body">
								<table class="table table-sm table-detail" style="margin-bottom: 0px;">
									<?php $indDet = dropDownList((object) ['method' => 'indDetXubigeo', 'idNivel' => $data->idnivel, 'codUbigeo' => $data->codubigeo, 'idInd' => $key->idind]);?>
									<thead>
										<tr>
											<td class="bold" width="90%"><?php echo str_replace('tic','TIC',ucfirst(strtolower($key->nomind)))?></td>
											<td class="text-right <?php echo setBackgroundglobal($key->ind_global);?>" ><?php echo $key->ind_global;?></td>									
										</tr>	
									</thead>
								</table>
								<table class="table table-sm table-detail datatable">
									<thead>
										<tr>
											<th class="text-center bold">Indicador</th>
											<th class="text-center bold">Descripción</th>
											<th class="text-center bold">Valor</th>
										</tr>	
									</thead>
									<tbody>									
										<?php foreach ($indDet as $key){ ?>
											<tr title="<?php echo ucfirst(strtolower($key->desind))?>">
												<td><?php echo ucfirst(strtolower($key->nomind))?></td>
												<td><?php echo ucfirst(strtolower($key->desind))?></td>							
												<td class="text-right"><?php echo number_format($key->valorind)?></td>									
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
						<br>
					<?php } ?>
				</div>
			</div>
			<input type="hidden" id="codnivelPIP" value="<?php echo $data->idnivel;?>" />
			<input type="hidden" id="coddptoPF" value="<?php echo $data->codubigeo;?>" />
			<input type="hidden" id="codUbigeoPIP" value="<?php echo $data->codubigeo;?>" />
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Consolidado de Proyectos de Inversión según los niveles de Gobierno <?php if($data->idnivel == 1)  echo 'de la Región'. ' '.ucwords(strtolower($obj->region)) ;elseif($data->idnivel == 2)  echo 'de la Provincia'.' '.ucwords(strtolower($obj->provincia)).', Región '.ucwords(strtolower($obj->region));elseif($data->idnivel == 3)  echo 'del Distrito'.' '.ucwords(strtolower($obj->distrito)).', Provincia '.ucwords(strtolower($obj->provincia)).', Región '.ucwords(strtolower($obj->region)); ?>			
						</div>
						<div class="card-body">	
							<div class="row justify-content-md-center">
								<div class="offset-md-4 col-lg-10">
									<label class="radio-inline"><input type="radio" name="radioDivPIPSTotal" id="rbtnPips" value="rbtnPipsTotal" checked>Función</label>
									<label class="radio-inline" style="padding-left:2em"><input type="radio" name="radioDivPIPSTotal" value="rbtnPipsTotal1" value="rbtnPips1">División Funcional</label>
									<label class="radio-inline" style="padding-left:2em"><input type="radio" name="radioDivPIPSTotal" value="rbtnPipsTotal2" value="rbtnPips2">Grupo Funcional</label>
                                </div>			
							</div>
						</div>	
						<div data-target="lnkWordCloud" id="divword2" style="min-width:400px;text-align:center ! important;"></div>
						<div class="card-footer text-center">
   							<a class="lnkAmpliar" data-event="lnkWordCloud" href="#" onclick="App.events(this); return false;">
								Mostrar/Ocultar Wordcloud
							</a>
						</div>	
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
					        	<a class="nav-link tabEntidadProyects" id="universidades-tab-funcion" style="color: black; !important;" href="#" onclick="App.events(this)" title="Municipalidades Distritales"><i class="fas fa-university"></i><b style="padding-left:0.5em;">Universidades</b></a>
                        	</li>
							<li class="nav-item">
					        	<a class="nav-link active tabEntidadProyects" id="ministerio-tab-funcion" style="color: #1976D2; !important;" href="#" onclick="App.events(this)" title="Gobiernos Nacionales"><i class="fa fa-hospital"></i><b style="padding-left:0.5em;">G. Nacional</b></a>
							</li>
				         	<li class="nav-item">
					        	<a class="nav-link tabEntidadProyects" id="regiones-tab-funcion" style="color: black; !important;" href="#" onclick="App.events(this)" title="Gobiernos Regionales"><i class="fa fa-building"></i><b style="padding-left:0.5em;">G. Regional</b></a>
                        	</li>
							<li class="nav-item">
					        	<a class="nav-link tabEntidadProyects" id="provincias-tab-funcion" style="color: black; !important;" href="#" onclick="App.events(this)" title="Municipalidades Provinciales"><i class="fa fa-place-of-worship"></i><b style="padding-left:0.5em;">M. Provincial</b></a>
                        	</li>
							<li class="nav-item">
					        	<a class="nav-link tabEntidadProyects" id="distritos-tab-funcion" style="color: black; !important;" href="#" onclick="App.events(this)" title="Municipalidades Distritales"><i class="fas fa-home"></i><b style="padding-left:0.5em;">M. Distrital</b></a>
                        	</li>
						</ul>	
						<div class="tab-content">
                       			 				<div class="tab-pane fade show active" id="universidades-funcion">									
														<div id="divpipUniversidades"></div>
							    					 </div>
												</div>
												<div class="tab-content">
                       			 					<div class="tab-pane fade show active" id="ministerio-funcion">
														<div id="divpipMinisterios"></div>
							     					</div>
												</div>
												<div class="tab-content">
                       			 					<div class="tab-pane fade show active" id="regiones-funcion">
														<div id="divpipRegiones"></div>
							     					</div>
												</div>
												<div class="tab-content">
                       			 					<div class="tab-pane fade show active" id="provincias-funcion">
														<div id="divpipProvincias"></div>
							     					</div>
												</div>
												<div class="tab-content">
                       			 					<div class="tab-pane fade show active" id="distritos-funcion">
														<div id="divpipDistritas"></div>
							     				</div>
						</div>					
					</div>						
				</div>						
			</div>
			<div class="row justify-content-md-center">
								<div class="col-md-4">
									<div class="card">
										<div class="card-header card-white text-center font-weight-bold">
											Formulación
										</div>
										<div class="card-body">
											<div id="pieRegion" style="height: 110px; margin: 0 auto"></div>
										</div>
										<div class="card-footer text-center font-weight-bold">
											-
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="card">
										<div class="card-header card-white text-center font-weight-bold">
											Perfil Viable
										</div>
										<div class="card-body">
											<div id="pieDistrito" style="height: 110px; margin: 0 auto"></div>
										</div>
										<div class="card-footer text-center font-weight-bold">
											-
										</div>
									</div>
								</div> 
			</div>
			<div class="row">
				<?php  if($data->idnivel == 1) {?>
				<div class="col-lg-12">
						<div class="card">
							<div class="card-header card-special text-center font-weight-bold">
								Empresas con capacidad de OxI en <?php  if($data->idnivel == 1)  echo 'Región'. ' '.ucwords(strtolower($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.ucwords(strtolower($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.ucwords(strtolower($obj->distrito))?>
							</div>
							<div class="card-body">
							<div class="scrollable" style="overflow-y: auto;max-height: 240px;">
							<table class="table table-sm table-detail">
										<?php $ciprlXunivDet = dropDownList((object) ['method' => 'funcionEmpresasXUbigeo', 'codnivel' => $data->idnivel, 'codUbigeo' =>  $data->codubigeo]);?>
										
										<thead>										
											<tr>
												<th class="text-center bold">Ranking</th>
												<th class="text-center bold">RUC</th>
												<th class="text-center bold">Nombre</th>
												<th class="text-center bold">Monto OxI</th>
												
											</tr>	
										</thead>
										<tbody>
										
											<?php foreach ($ciprlXunivDet as $key){ ?>
												<tr>
												<?php  if ($key->disponibleoxi != 0) {?>
													<td class="text-center"><?php echo $key->ranking ?></td>
													<td class="text-center"><?php echo ucfirst(strtolower($key->rucempresa))?></td>
													<td class="text-left"><?php echo $key->nomempresa?></td>
													<td class="text-center"><?php echo number_format($key->disponibleoxi)?></td>
												<?php } ?>	
												</tr>
											<?php } ?>
											
										</tbody>
							</table>
							</div>	
							</div>
						</div>
					<br>
				</div>
			<?php  }?>
			</div>
			<br>								
			<div class="row">
				<?php  if($data->idnivel == 1) {?>										
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Congresistas en <?php echo 'Región'.' '.ucwords(strtolower($obj->region))?> (Actualizado al 2016)
						</div>
						<div class="card-body">
							<table class="table table-sm table-detail">
								<?php $congresistas = dropDownList((object) ['method' => 'congresistasXRegion', 'codUbigeo' => $data->codubigeo]);
									  $i=0;
								?>
								<thead>										
									<tr>
										<th class="text-center bold">#</th>
										<th class="text-center bold">Nombre</th>
										<th class="text-center bold">Partido</th>
										<th class="text-center bold">Bancada</th>		
										<th class="text-center bold">Votantes</th>
									</tr>	
								</thead>
								<tbody>
									<?php foreach ($congresistas as $key){ ?>
										<tr>
											<td class="text-center"><?php  $i++;echo ($i) ?></td>
											<td class="text-left"><?php echo (($key->apellidos)) ?></td>
											<td class="text-left"><?php echo ucfirst(strtolower($key->parido))?></td>	
											<td class="text-left"><?php echo ucfirst($key->bancada)?></td>
											<td class="text-left"><?php echo number_format($key->votantes)?></td>

										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>				
					</div>
				</div>
				<?php  }?>
			</div>
			<br>	
				
			
			<div class="row" style="display: none;">
				<div class="col-lg-12">
						<div class="card">
							<div class="card-header card-special text-center font-weight-bold">
								Consolidado de Regiones por CIPRL, Población y Electores - <?php  if($data->idnivel == 1)  echo 'Región'. ' '.ucwords(strtolower($obj->region)) ;elseif($data->idnivel == 2) echo 'Provincia'. ' '.ucwords(strtolower($obj->provincia)) ;elseif($data->idnivel == 3) echo 'Distrito'. ' '.ucwords(strtolower($obj->distrito))?>
							</div>
							<div class="card-body">
								
									<div id="ciprlpoelec"></div>

							</div>
						</div>
						
				</div>							
			</div>	
				
			<div class="row">
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header card-special text-center font-weight-bold">
							Consolidado de comisarías, hospitales y II.EE
						</div>
						<div class="card-body">
							<fichaentidad-obs codUbigeo="<?php echo $data->codubigeo; ?>"></fichaentidad-obs>
						</div>
					</div>
				</div>
			</div>
    	</div>	     
	</div>
