<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);
?>
<table class="table table-sm table-detail" width="100%">
	<thead>
		<tr>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Partidos Politicos</th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center"><?php echo ucwords($data->texto); ?></th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">CIPRL</th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables sin ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables sin ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables con ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables con ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables </th>
		</tr>
	</thead>
	<tbody>
		<?php
		$i = 0;
		$s1 = 0;
		$ciprl = 0;
		$s2 = 0;
		$s3 = 0;
		$s4 = 0;
		$s5 = 0;
		$s6 = 0;
		$s7 = 0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'pi_partidospoliticos_sub', 'nivel' => $data->envio]);
		foreach ($pipxUbigeoDet as $item) {
			$i++;
			$s1 =   $s1 + $item->cantidad;
			$s2 =   $s2 + $item->csn;
			$ciprl =   $ciprl + $item->ciprl;
			$s3 =   $s3 + $item->msn;
			$s4 =   $s4 + $item->ccn;
			$s5 =   $s5 + $item->mcn;
			$s6 =   $s6 + $item->ccn + $item->csn;
			$s7 =   $s7 + $item->mcn + $item->msn;

		?>
			<tr>
				<td class="text-center"><?php echo $i; ?></td>
				<td class="text-center"><?php echo ($item->partido) ?></td>
				<td class="text-center"><button class="btn btn-link lnlEntidadPartido_sub" id="<?php echo ($item->partido . '-' . $data->envio) ?>" data-event="lnkProvXrutas_<?php echo ($item->partido . '-' . $data->envio) ?>" onclick="App.events(this);"> <?php echo number_format($item->cantidad) ?></button></td>
				<td class="text-center"><?php echo number_format($item->ciprl) ?></td>
				<td class="text-center"><?php echo number_format($item->csn) ?></td>
				<td class="text-center"><?php echo number_format($item->msn) ?></td>
				<td class="text-center"><?php echo number_format($item->ccn) ?></td>
				<td class="text-center"><?php echo number_format($item->mcn) ?></td>
				<td class="text-center"><?php echo number_format($item->ccn + $item->csn) ?></td>
				<td class="text-center"><?php echo number_format($item->mcn + $item->msn) ?></td>
			</tr>
			<tr data-target="lnkProvXrutas_<?php echo ($item->partido . '-' . $data->envio) ?>" style="display: none;">
				<td colspan="10">
					<div class="card">
						<div class="card-header">
							Entidades
						</div>
						<div class="card-body">
							<div id="div_<?php echo ($item->partido . '-' . $data->envio) ?>"></div>
						</div>
					</div>

				</td>
			</tr>
		<?php } ?>
		<tr>

			<td></td>
			<td>Total</td>
			<td class="text-center"><?php echo number_format($s1) ?></td>
			<td class="text-center"><?php echo number_format($ciprl) ?></td>
			<td class="text-center"><?php echo number_format($s2) ?></td>
			<td class="text-center"><?php echo number_format($s3) ?></td>
			<td class="text-center"><?php echo number_format($s4) ?></td>
			<td class="text-center"><?php echo number_format($s5) ?></td>
			<td class="text-center"><?php echo number_format($s6) ?></td>
			<td class="text-center"><?php echo number_format($s7) ?></td>

		</tr>
	</tbody>
</table>