<?php
    require 'php/app.php';
    $data = json_decode($_GET['obj']);		
?>
<style>
	.form-group,.form-control {
  				
		font-size: 10px !important;
				
			 }	
             .highcharts-title {
    	fill: #434348;
    	font-weight: bold;
    	font-size: 12px !important;
	}
	.bold{
		font-weight: bold;
		background: #1976d226;
		color: #000;
	}
	.table thead th {
		vertical-align: bottom;
		border-bottom: 2px solid #fefeff;
	}	 
</style>
<div class="container"  width= "100%"!important  height="100%"!important>
    <div class="card card-outline-info" >
        <div class="card-header">
			<div class="row">
                <div class="col-lg-11">
                    <h6 class="m-b-0 text-white">Resumen de Proyectos Filtrados</h6>				
                </div>
                <div class="col-lg-1" style="text-align:right !important">
                    <div class="row">
                        <div class="col-12">
                            <a class="lnkAmpliarDraw" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                <i class="fas fa-compress text-white"></i>
                            </a>   
                        </div>
                        <div class="col-12">
                            <a class="lnkAmpliarDraw" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;"  title="Minimizar Ventana">
                                <i class="fas fa-minus-square text-white"></i>
                            </a>  
                        </div>
                    </div>         
                </div>
            </div>
			</div>
        </div> 
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-4">
                            <div id="map10" style="height:450px; width:100%;"></div>
                        </div>
                        <div class="col-lg-8">
                            <div id="chartDrawUbigeos" style="height: 520px; width: 100%"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
					<button type="button" id="saveDrawObjetos" onclick="App.events(this)" class="btn btn-sm btn-info waves-effect waves-light" >
						<i class="fa fa-check"></i>
						Grabar
					</button>
				</div>
            </div>
			 <br>
			<div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group not-bottom">
                                        <label>Texto</label>
                                        <input type="text" class="form-control form-control-sm text-right" id="txtTextoTwit" >
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group not-bottom">
                                        <label>Desde</label>
                                        <input type="text" class="form-control form-control-sm text-right" id="txtDesdeTwit" >
                                    </div>                       
                                </div>   
                                <div class="col-lg-2">
                                    <button type="button" id="btnGenerateTwit" class="btn btn-success btn-sm" onclick="App.events(this);">Buscar Twit</button>                   
                                </div>                  
          </div>
		   <br>
		    <br>
		  <div id="divTwit" ></div>
        </div>   
    </div>     
</div>
