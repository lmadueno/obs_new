<?php
require 'php/app.php';

            $data = json_decode($_GET['data']);	
           
            $obj1    = dropDownList((object) ['method' => 'quintilCiprlPoblaElect','tipo'=>$data->tipo,'region'=>$data->region]);
            $i=0;
            
            
?>

<div class="row">
    <div class="col-lg-12" style="text-align:center !important">
        <div class="card-body">

            <label class="radio-inline" style="padding-left:1em"><input type="radio" name="rbtncpe" id="rbtnCIPRL" value="rbtnCIPRL" <?php if($data->checked==1){?>checked<?php } ?>>CIPRL</label>
            <label class="radio-inline" style="padding-left:2em"><input type="radio" name="rbtncpe" id="rbtnPoblacion" value="rbtnPoblacion" <?php if($data->checked==2){?>checked<?php } ?>>Población</label>
			<label class="radio-inline" style="padding-left:2em"><input type="radio" name="rbtncpe" id="rbtnEectores" value="rbtnEectores" <?php if($data->checked==3){?>checked<?php } ?>>Electores</label>
        </div>
    </div>
</div>
<input type="hidden" id="hiddenfieldtipoCPRL" value="<?php echo ($data->tipo)?>" />
<table class="table table-sm table-detail">
            <tr>
                <th class="text-center bold">#</th>
                <th class="text-center bold">Quintil</th>
                <th class="text-center bold">Cantidad</th>
                <th class="text-center bold">CIPRL</th>
                <th class="text-center bold">Población</th>
				<th class="text-center bold">Electores</th>
            </tr>
            <?php
                $tmpQuintilCantidad=0;  
                $tmpQuintilCiprl=0;
                $tmpQuintilPoblacion=0;
                $tmpQuintilElectores=0;
                foreach ($obj1 as $key1){  
                    $i++;
                    $tmpQuintilCantidad+=intval($key1->cantidad) ;
                    $tmpQuintilCiprl+=intval($key1->ciprl) ;
                    $tmpQuintilPoblacion+=intval($key1->poblacion) ;
                    $tmpQuintilElectores+=intval($key1->electores) ;
            ?>
            <tr>
                <td class="text-center "><?php echo ($i)?></td>
                <td class="text-center "><a class="lnkAmpliar2" id="<?php echo $key1->quintil?>"  data-event="lnkProvXrutas_<?php echo $key1->quintil?>" href="#" onclick="App.events(this); return false;"> <?php echo ($key1->quintil)?> </a> </td>
                <td class="text-center "><?php echo ($key1->cantidad)?></td>
                <td class="text-center "><?php echo number_format($key1->ciprl)?></td>
                <td class="text-center "><?php echo number_format($key1->poblacion)?></td>
                <td class="text-center "><?php echo number_format($key1->electores)?></td>
            </tr>
            <tr data-target="lnkProvXrutas_<?php echo $key1->quintil?>" style="display: none;">
                <td colspan="6">
                    <div id="tablapoblacioelectoresquintiles_<?php echo $key1->quintil?>">
                </td>
            </tr>
           
            <?php  } ?>
            <tr>
                <td class="text-center "></td>
                <td class="text-right">
                  Total :
                </td>
                <td class="text-center "><?php echo number_format($tmpQuintilCantidad)?></td>
                <td class="text-center "><?php echo number_format($tmpQuintilCiprl)?></td>
                <td class="text-center "><?php echo number_format($tmpQuintilPoblacion)?></td>
				<td class="text-center "><?php echo number_format($tmpQuintilElectores)?></td>
            </tr>
</table>
