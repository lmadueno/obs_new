<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>

<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Total brecha de Proyectos de Inversión
			</div>
			<br>

			<div class="row">
				<div class="col-lg-12">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8;">
										<div class="row">
											<div class="col-lg-12">
												<div class="panel panel-default">
													<div class="panel-body">
														<div class="row">
															<div class="col-lg-3">
																<div class="form-group">
																	<label style="padding-left: 10px;">Brecha de financiamiento de proyectos</label>
																	<select style="padding-left: 10px;" class="form-control input-sm brecha" id="brecha1" name="brecha1">
																		<option value="[Seleccione]">[Seleccione]</option>
																		<option value="1">En Ejecución</option>
																		<option value="0">Viables sin Ejecución</option>
																	</select>
																</div>
															</div>
															<div class="col-lg-3">
																<div class="form-group">
																	<label style="padding-left: 10px;">Incluido ejecución PMI</label>
																	<select style="padding-left: 10px;" class="form-control input-sm brecha2" id="brecha2" name="brecha2">
																		<option value="[Seleccione]">[Seleccione]</option>
																		<option value="SI">Si</option>
																		<option value="NO">no</option>
																	</select>
																</div>
															</div>
															
															<div class="col-lg-3">
																<div class="form-group">
																	<label style="padding-left: 10px;">Accion</label>
																	<button type="button" class="form-control input-sm btn btn-primary filtroproyecto" id="filtroproyecto" name="filtroproyecto" onclick="App.events(this); return false;">Filtrar</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-8">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Total brecha de ejecución y financiamiento de Proyectos de Inversión
					</div>
					<div id='chartbrecha_10'></div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card card-outline-info">
					<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
						Tabla total brecha de ejecución y financiamiento de Proyectos de Inversión
					</div>
					<table class="table table-hover table-sm" data-target="datos" width="100%">
						<thead>
							<tr>
								<th scope="col" class="text-center bold" width="4%">#</th>
								<th scope="col" class="text-center bold">Gobierno</th>
								<th scope="col" class="text-center bold">Cantidad</th>
								<th scope="col" class="text-center bold">Costo Actualizado</th>
								<th scope="col" class="text-center bold">Devengado Acumulado</th>
								<th scope="col" class="text-center bold">Saldo por ejecutar</th>
								<th scope="col" class="text-center bold">Saldo por financiar</th>
							</tr>
						</thead>
						<tbody id="cuerpoTabla2">
							
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Brecha de ejecución y financiamiento 2017 - 2021*
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Brecha de ejecución y financiamiento de IOARR 2017 - 2021*
						</div>
						<div id='chartbrecha_11'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Brecha de ejecución y financiamiento de Programas de Inversión 2017 - 2021*
						</div>
						<div id='chartbrecha_22'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Brecha de ejecución y financiamiento de Proyectos de Inversión 2017 - 2021*
						</div>
						<div id='chartbrecha_33'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos1" style="color: #1976D2; !important;" id="datos1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha de ejecución y financiamiento.</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla Brecha de ejecución y financiamiento de IOARR
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$catotal = 0;
										$cototal = 0;
										$detotal = 0;
										$ejtotal = 0;
										$s_total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha', 'tipo' => 'IOARR']);
										// echo json_encode($pipxUbigeoDet);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$catotal = $catotal + intval($item->cantidad);
											$cototal = $cototal + intval($item->costo);
											$detotal = $detotal + intval($item->devengado);
											$ejtotal = $ejtotal + intval($item->ejecutar);
											$s_total = $s_total + intval($item->s_financiar);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo $item->nivel_gobierno ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Brecha de ejecución y financiamiento de Proyectos

								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$catotal = 0;
										$cototal = 0;
										$detotal = 0;
										$ejtotal = 0;
										$s_total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha', 'tipo' => 'PROYECTO']);
										// echo json_encode($pipxUbigeoDet);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$catotal = $catotal + intval($item->cantidad);
											$cototal = $cototal + intval($item->costo);
											$detotal = $detotal + intval($item->devengado);
											$ejtotal = $ejtotal + intval($item->ejecutar);
											$s_total = $s_total + intval($item->s_financiar);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo $item->nivel_gobierno ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos1" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Brecha de ejecución y financiamiento de Programas de inversión

								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos1" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Gobierno</th>
											<th scope="col" class="text-center bold">Cantidad</th>
											<th scope="col" class="text-center bold">Costo Actualizado</th>
											<th scope="col" class="text-center bold">Devengado Acumulado</th>
											<th scope="col" class="text-center bold">Saldo por ejecutar</th>
											<th scope="col" class="text-center bold">Saldo por financiar</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$catotal = 0;
										$cototal = 0;
										$detotal = 0;
										$ejtotal = 0;
										$s_total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha', 'tipo' => 'PROGRAMA DE INVERSION']);
										// echo json_encode($pipxUbigeoDet);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$catotal = $catotal + intval($item->cantidad);
											$cototal = $cototal + intval($item->costo);
											$detotal = $detotal + intval($item->devengado);
											$ejtotal = $ejtotal + intval($item->ejecutar);
											$s_total = $s_total + intval($item->s_financiar);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo $item->nivel_gobierno ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<br>

<div id="accordion">
	<div class="card">
		<div class="card-header" id="headingOne" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="color:#000;font-weight: bold; ">
					Estadísticas Brecha IOARR
				</button>
			</h5>
		</div>
		<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Brecha IOARR por nivel Gobierno 2017-2021*

							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Nacional 2017 - 2021*


										</div>
										<div id='chartbrecha_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Regional 2017 - 2021*

										</div>
										<div id='chartbrecha_2'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de IOARR de Gobierno Local 2017 - 2021*

										</div>
										<div id='chartbrecha_3'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos" style="color: #1976D2; !important;" id="datos" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha IOARR por nivel Gobierno 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Nacional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GN']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Regional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GR']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de IOARR de Gobierno Local </div>
												<br>
												<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'IOARR', 'nivel' => 'GL']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>

	<div class="card">
		<div class="card-header" id="headingTwo" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="color:#000;font-weight: bold;">
					Estadisticas Brecha Proyectos de Inversión
				</button>
			</h5>
		</div>
		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
			<div class="card-body">
				<!-- brecha PROYECTOS  -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Brecha Proyectos de inversión por nivel Gobierno 2017-2021*

							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de Proyectos de inversión de Gobierno Nacional 2017 - 2021*
										</div>
										<div id='chart_brecha_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de Proyecto de inversión de Gobierno Regional 2017 - 2021*
										</div>
										<div id='chart_brecha_2'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Brecha de ejecución y financiamiento de Proyectos de inversión de Gobierno Local 2017 - 2021*
										</div>
										<div id='chart_brecha_3'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_1" style="color: #1976D2; !important;" id="datos_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha Programas de inversión por nivel Gobierno 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de Programas de inversión de Gobierno Nacional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROYECTO', 'nivel' => 'GN']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de Proyectos de inversión de Gobierno Regional
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROYECTO', 'nivel' => 'GR']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla Brecha de ejecución y financiamiento de Proyectos de inversión de Gobierno Local
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROYECTO', 'nivel' => 'GL']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
	<div class="card">
		<div class="card-header" id="headingThree" style="background:#ddebf8">
			<h5 class="mb-0">
				<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree" style="color:#000;font-weight: bold;">
					Estadisticas Brecha Programas de Inversión
				</button>
			</h5>
		</div>
		<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Brecha Programas de inversión por nivel Gobierno 2017-2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Tabla Brecha de ejecución y financiamiento de Programas de inversión de Gobierno Nacional
										</div>
										<div id='chart_brecha_1_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Tabla Brecha de ejecución y financiamiento de Programas de inversión de Gobierno Regional
										</div>
										<div id='chart_brecha_2_1'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Tabla Brecha de ejecución y financiamiento de Programas de inversión de Gobierno Local
										</div>
										<div id='chart_brecha_3_1'></div>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="text-center col-lg-12">

									<a class="nav-link active vertabla" data-event="datos_1" style="color: #1976D2; !important;" id="datos_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Brecha Programas de inversión por nivel Gobierno 2017-2021*</b></a>
									<!-- </div> -->
									<div class="row">
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de Brecha Programas de inversión Gobierno Nacional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROGRAMA DE INVERSION', 'nivel' => 'GN']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de Brecha Programas de inversión Gobierno Regional 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">funcion</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROGRAMA DE INVERSION', 'nivel' => 'GR']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="card card-outline-info" data-target="datos_1" style="display: none;">
												<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
													Tabla de Brecha Programas de inversión Gobierno Local 2017-2021*
												</div>
												<br>
												<table class="table table-hover table-sm" data-target="datos_1" style="display: none;" width="100%">
													<thead>
														<tr>
															<th scope="col" class="text-center bold" width="4%">#</th>
															<th scope="col" class="text-center bold">Función</th>
															<th scope="col" class="text-center bold">Cantidad</th>
															<th scope="col" class="text-center bold">Costo Actualizado</th>
															<th scope="col" class="text-center bold">Devengado Acumulado</th>
															<th scope="col" class="text-center bold">Saldo por ejecutar</th>
															<th scope="col" class="text-center bold">Saldo por financiar</th>
														</tr>
													</thead>
													<tbody>
														<?php
														$i = 0;

														$catotal = 0;
														$cototal = 0;
														$detotal = 0;
														$ejtotal = 0;
														$s_total = 0;
														$pipxUbigeoDet = dropDownList((object) ['method' => 'rango_brecha_colapsada', 'tipo' => 'PROGRAMA DE INVERSION', 'nivel' => 'GL']);
														// echo json_encode($pipxUbigeoDet);
														foreach ($pipxUbigeoDet as $item) {
															$i++;

															$catotal = $catotal + intval($item->cantidad);
															$cototal = $cototal + intval($item->costo);
															$detotal = $detotal + intval($item->devengado);
															$ejtotal = $ejtotal + intval($item->ejecutar);
															$s_total = $s_total + intval($item->s_financiar);

														?>
															<tr>
																<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
																<td scope="row" class="text-left"><?php echo $item->funcion ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->cantidad) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->costo) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->devengado) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->ejecutar) ?></td>
																<td scope="row" class="text-center"><?php echo number_format($item->s_financiar) ?></td>
															</tr>
														<?php
														} ?>
														<tr>
															<td scope="row" class="text-center" width="4%"></td>
															<td scope="row" class="text-center">Total Acumulado</td>
															<td scope="row" class="text-center"><?php echo number_format($catotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($cototal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($detotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($ejtotal) ?></td>
															<td scope="row" class="text-center"><?php echo number_format($s_total) ?></td>


														</tr>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<br>
			</div>
		</div>
	</div>
</div>