<?php
 require 'php/app.php';

 $data = json_decode($_GET['data']);	
 $excelEmpresas = dropDownList((object) ['method' => 'empresasOXIExcel','limitFin' =>  $data->limitFin,'limitInicio' => $data->limitInicio,'desdeLimite' => $data->desdeLimite]);
 ?>

<div class="row">
    <div class="col-lg-12">
        <div class="card-header card-special text-center font-weight-bold">
            <?php echo ($data->titulo)?>
        </div>
    </div>
    <div class="offset-md-4 col-lg-10">
            <div class="card-body">
            <?php if($data->rbtnTabla==1){?>
                <label class="radio-inline"><input type="radio" name="radioEmpresas" id="rbtn1" value="rbtn1" <?php if($data->desdeLimite==2){?>checked<?php } ?>>Desde 2015 - 2020</label>
                <label class="radio-inline" style="padding-left:2em"><input type="radio" name="radioEmpresas" value="rbtn2" value="rbtn2" <?php if($data->desdeLimite==1){?>checked<?php } ?>>Desde 2009 - 2020</label>
            <?php } else if($data->rbtnTabla==2){?>
                <label class="radio-inline"><input type="radio" name="radioEmpresas" id="rbtn1Resto" value="rbtn1Resto" <?php if($data->desdeLimite==2){?>checked<?php } ?>>Desde 2015 - 2020</label>
                <label class="radio-inline" style="padding-left:2em"><input type="radio" name="radioEmpresas" value="rbtn2Resto" id="rbtn2Resto" <?php if($data->desdeLimite==1){?>checked<?php } ?>>Desde 2009 - 2020</label>
            <?php } else if($data->rbtnTabla==3){?>
                <label class="radio-inline"><input type="radio" name="radioEmpresas" id="rbtn1Total" value="rbtn1Total" <?php if($data->desdeLimite==2){?>checked<?php } ?>>Desde 2015 - 2020</label>
                <label class="radio-inline" style="padding-left:2em"><input type="radio" name="radioEmpresas" value="rbtn2Total" id="rbtn2Total" <?php if($data->desdeLimite==1){?>checked<?php } ?>>Desde 2009 - 2020</label>
            <?php } ?>
            </div>
    </div>
    <div class="col-lg-12">
        <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
            <table class="table table-sm table-detail">
                <thead>																				
                    <tr>
                        <th class="text-center bold vert-middle">#</th>
                        <?php if($data->desdeLimite==1 or $data->desdeLimite==2){?><th class="text-center bold vert-middle">Nombre</th><?php } else { ?><th class="text-center bold vert-middle" width="600px"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2009</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2010</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2011</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2012</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2013</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><th class="text-center bold vert-middle">2014</th><?php } else { ?><th class="text-center bold vert-middle"></th><?php } ?>
                        <th class="text-center bold vert-middle">2015</th>
                        <th class="text-center bold vert-middle">2016</th>
                        <th class="text-center bold vert-middle">2017</th>
                        <th class="text-center bold vert-middle">2018</th>
                        <th class="text-center bold vert-middle">2019</th>
                        <th class="text-center bold vert-middle">2020</th>
                        <th class="text-center bold vert-middle">Total</th>
                    </tr>	
                </thead>
                    <?php 
                        $i=0;
                        $temp=0;
                        $temp1=0;
                        $temp2=0;
                        $temp3=0;
                        $temp4=0;
                        $temp5=0;
                        $temp6=0;
                        $temp7=0;
                        $temp8=0;
                        $temp9=0;
                        $temp10=0;
                        $temp11=0;
                        $temptotal=0;
                        foreach ($excelEmpresas as $key){ 
                        $i++; 
                        if($data->desdeLimite==1){
                            $temp+=$key->ano2009;
                            $temp1+=$key->ano2010;
                            $temp2+=$key->ano2011;
                            $temp3+=$key->ano2012;										
                            $temp4+=$key->ano2013;
                            $temp5+=$key->ano2014;
                        }
                        
                        $temp6+=$key->ano2015;
                        $temp7+=$key->ano2016;
                        $temp8+=$key->ano2017;
                        $temp9+=$key->ano2018;
                        $temp10+=$key->ano2019;
                        $temp11+=$key->ano2020;
                        $temptotal+=$key->total;
                            
                    ?>

                    <tr>
                        <td class="text-right"><?php    echo  $i?></td>
                        <td class="text-left"><?php     echo $key->nombre?></td>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2009, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2010, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2011, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2012, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>										
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2013, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php    echo number_format($key->ano2014, 2, '.', ',')?></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <td class="text-right"><?php    echo number_format($key->ano2015, 2, '.', ',')?></td>
                        <td class="text-right"><?php    echo number_format($key->ano2016, 2, '.', ',')?></td>
                        <td class="text-right"><?php    echo number_format($key->ano2017, 2, '.', ',')?></td>											
                        <td class="text-right"><?php    echo number_format($key->ano2018, 2, '.', ',')?></td>
                        <td class="text-right"><?php    echo number_format($key->ano2019, 2, '.', ',')?></td>
                        <td class="text-right"><?php    echo number_format($key->ano2020, 2, '.', ',')?></td>
                        <td class="text-right">
                            <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $key->nombre?>" href="#" onclick="App.events(this); return false;">
                                <?php    echo number_format($key->total, 2, '.', ',')?>
                            </a>			                                                    
                        </td>
                    </tr>
                    
                    </tr>
                    <?php } ?>
                    <tr>
                        <td class="text-center"></td>
                        <td class="text-left"><b>TOTAL</b></td>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center "></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp1, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp2, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp3, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center"></th><?php } ?>											
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp4, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center"></th><?php } ?>
                        <?php if($data->desdeLimite==1){?><td class="text-right"><?php echo number_format($temp5, 2, '.', ',')?></td></td><?php } else { ?><th class="text-center"></th><?php } ?>
                        <td class="text-right"><?php echo number_format($temp6, 2, '.', ',')?></td>
                        <td class="text-right"><?php echo number_format($temp7, 2, '.', ',')?></td>											
                        <td class="text-right"><?php echo number_format($temp8, 2, '.', ',')?></td>
                        <td class="text-right"><?php echo number_format($temp9, 2, '.', ',')?></td>
                        <td class="text-right"><?php echo number_format($temp10, 2, '.', ',')?></td>
                        <td class="text-right"><?php echo number_format($temp11, 2, '.', ',')?></td>
                        <td class="text-right"><?php echo number_format($temptotal, 2, '.', ',')?></td>																						        																								
                    </tr> 
            </table>
        </div>
    </div>                     
</div>

