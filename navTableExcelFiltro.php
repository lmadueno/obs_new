

<?php
    require 'php/app.php';
            $data = json_decode($_GET['data']);	
            $respuesta= dropDownList((object) ['method' => 'cantidadFiltroUbigeoExcel','region'=> $data->consulta_region,'provincia'=> $data->consulta_provincia,'distrito'=> $data->consulta_distrito]);
?>

<div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                <div class="row">
                    <div class="col-10">Lista de Entidades Filtradas</div> 
                    <div class="col-2 text-right">
						<a href="#" class="lnkAmpliar"  data-event="lnk_entidadesFiltro"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down "></i></a>
					</div>
                </div>
            </div>
        </div>
        <div class="col-lg-12" data-target="lnk_entidadesFiltro" style="display: none;">
            <table class="table table-sm table-detail" data-target="ampliartablauser" >   
                <tr>
                    <td class="text-center bold"  width="25%" > Regiones : </td>
					<td class="text-center " width="35%">
                        <b><?php echo $respuesta{0}->dpto;?></b>
                    </td> 
                    <td class="text-center ">
                        <a href="downloadExcelDpto.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank">
				            <img src="https://cdn3.iconfinder.com/data/icons/document-icons-2/30/647708-excel-16.png"/>
				        </a>
                    </td>   																						
                </tr> 
                <tr>
                    <td class="text-center bold"  width="25%" > Provincias : </td>
					<td class="text-center " width="35%">
                       <b><?php echo $respuesta{0}->provincia;?></b> 
                    </td>
                    <td class="text-center ">
                        <a href="downloadExcelProv.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank">
				            <img src="https://cdn3.iconfinder.com/data/icons/document-icons-2/30/647708-excel-16.png"/>
				        </a>
                    </td>    																						
                </tr> 
                <tr>
                    <td class="text-center bold"  width="25%" > Distritos : </td>
					<td class="text-center " width="35%">
                        <b><?php echo number_format($respuesta{0}->distrito);?></b>
                    </td> 
                    <td class="text-center ">
                        <a href="downloadExcelDist.php?data=<?php echo urlencode($_GET['data']);?>" target="_blank">
				            <img src="https://cdn3.iconfinder.com/data/icons/document-icons-2/30/647708-excel-16.png"/>
				        </a>
                    </td>    																						
                </tr>
                <tr>
                    <td class="text-center"  width="25%" ></td>
                    <td class="text-center" width="35%"></td>   	
                    <td class="text-center"></td>  																					
                </tr>                                       
            </table>                 
        </div>
    </div>