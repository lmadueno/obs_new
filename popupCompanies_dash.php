<?php
require 'php/app.php';

// $token = 'apis-token-352.UnXaJEl-X74YBqZWO2GiWc5vjZ7NmsuN';
// $ruc = '10460278975';

// // Iniciar llamada a API
// $curl = curl_init();

// // Buscar ruc sunat
// curl_setopt_array($curl, array(
//     CURLOPT_URL => 'https://api.apis.net.pe/v1/ruc?numero=' . $ruc,
//     CURLOPT_RETURNTRANSFER => true,
//     CURLOPT_ENCODING => '',
//     CURLOPT_MAXREDIRS => 10,
//     CURLOPT_TIMEOUT => 0,
//     CURLOPT_FOLLOWLOCATION => true,
//     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//     CURLOPT_CUSTOMREQUEST => 'GET',
//     CURLOPT_HTTPHEADER => array(
//         'Referer: http://apis.net.pe/api-ruc',
//         'Authorization: Bearer ' . $token
//     ),
// ));

// $response = curl_exec($curl);

// curl_close($curl);
// // Datos de empresas según padron reducido
// $empresa = json_decode($response);
// var_dump($empresa);

if (count($_GET) > 0) {

    if ($_GET['data']) {
        $data = json_decode($_GET['data']);
        // var_dump($data);
?>
        <div class="row">
            <div class="col-lg-12">

                <div class="card card-outline-info">
                    <div class="card-header" style="background:#304e9c">
                        <div class="row">
                            <div class="col-lg-12">
                                <h6 class="m-b-0 text-white">Información de la empresa</h6>
                            </div>
                            <div class="col-lg-12 " style="text-align:right !important">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <a class="lnkAmpliarDIALOEmpresa" id="maxi" href="#" onclick="App.events(this); return false;" title="Maximizar Ventana">
                                            <i class="fas fa-compress text-white"></i>
                                        </a>
                                    </div>
                                    <div class="col-12">
                                        <a class="lnkAmpliarDIALOEmpresa" style="display:none !important" id="mini" href="#" onclick="App.events(this); return false;" title="Minimizar Ventana">
                                            <i class="fas fa-minus-square text-white"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header card-special text-center font-weight-bold">
                                        Datos Generales
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Ruc</label>
                                                    <input type="text" class="form-control form-control-sm" value="<?php echo $data->ruc; ?>" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Listado en Bolsa</label>
                                                    <input type="text" class="form-control form-control-sm" value="<?php echo $data->listado; ?>" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Condicion contribuyente</label>
                                                    <input type="text" class="form-control form-control-sm" id="contribuyente_condicion" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <div class="form-group not-bottom">
                                                    <label>Actividad Exterior</label>
                                                    <input type="text" class="form-control form-control-sm" id="actividad_exterior" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group not-bottom">
                                                    <label>Emision Electronica</label>
                                                    <input type="text" class="form-control form-control-sm" id="emision_electronica" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-3">
                                                <div class="form-group not-bottom">
                                                    <label>Estado</label>
                                                    <input type="text" class="form-control form-control-sm" id="estado" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="form-group not-bottom">
                                                    <label>Tamaño Empresa</label>
                                                    <input type="text" class="form-control form-control-sm" value="<?php echo $data->tamano; ?>" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Fecha de inscripcion</label>
                                                    <input type="text" class="form-control form-control-sm" id="fecha_inscripcion" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Inicio Actividades</label>
                                                    <input type="text" class="form-control form-control-sm" id="inicio_actividades" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Nombre Comercial</label>
                                                    <input type="text" class="form-control form-control-sm" id="nombre_comercial" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Razon Social</label>
                                                    <input type="text" class="form-control form-control-sm" id="razon_social" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Sistema Contabilidad</label>
                                                    <input type="text" class="form-control form-control-sm" id="sistema_contabilidad" readonly>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <div class="form-group not-bottom">
                                                    <label>Sistema Emision</label>
                                                    <input type="text" class="form-control form-control-sm" id="sistema_emision" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group not-bottom">
                                                    <label>Tipo</label>
                                                    <input type="text" class="form-control form-control-sm" id="tipo" readonly>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group not-bottom">
                                                    <label>Direccion</label>
                                                    <input type="text" class="form-control form-control-sm" id="direccion" readonly>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group not-bottom">
                                                            <label>Actividad Economica</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div id="txtactividadEconomica"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group not-bottom">
                                                            <label>Sistema de Emisión Electrónica</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div id="txtcomprobante_electronico"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group not-bottom">
                                                            <label>Padrones</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
                                                            <div id="txtTrabajadores"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group not-bottom">
                                                            <label>Comprobantes de pago</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
                                                            <div id="txtrepresentantes"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-header card-special text-center font-weight-bold">
                                                        Locales
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                                                                    <table class="table table-sm table-detail">
                                                                        <?php $anexos = dropDownList((object) ['method' => 'anexos_sunat', 'ruc' => $data->ruc]); ?>
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="text-center bold">Departamento</th>
                                                                                <th class="text-center bold">Provincia</th>
                                                                                <th class="text-center bold">Distrito</th>
                                                                                <th class="text-center bold">Direccion</th>
                                                                                <th class="text-center bold"><input type="checkbox" name="mapsCoordenadas1"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php foreach ($anexos as $key) { ?>
                                                                                <tr>
                                                                                    <td class="text-center"><?php echo $key->departamento ?></td>
                                                                                    <td class="text-center"><?php echo $key->provincia ?></td>
                                                                                    <td class="text-center"><?php echo $key->distrito ?></td>
                                                                                    <td class="text-center"><?php echo $key->dir ?></td>
                                                                                    <td class="text-center"><input class="mapsLatLong" type="checkbox" id="<?php echo $key->dir ?>" name="mapsCoordenadas"> </td>
                                                                                </tr>
                                                                            <?php } ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="div">
                                                                    <div id="map111" style="height:450px; width:100%;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header card-special text-center font-weight-bold">
                                        Datos Economicos
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div id="chart_<?php echo $data->ruc; ?>" style="min-width: 310px; height: 220px; margin: 0 auto"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table class="table table-sm table-detail">
                                                    <?php $anexos = dropDownList((object) ['method' => 'ingresos_egresos_empresa', 'ruc' => $data->ruc]); ?>
                                                    <thead>
                                                        <tr>
                                                            <th class="text-center bold">Año</th>
                                                            <th class="text-center bold">Ingresos</th>
                                                            <th class="text-center bold">Activo</th>
                                                            <th class="text-center bold">Pasivo</th>
                                                            <th class="text-center bold">Patrimonio</th>
                                                            <th class="text-center bold">Utilidad</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($anexos as $key) { ?>
                                                            <tr>
                                                                <td class="text-center"><?php echo ($key->anio) ?></td>
                                                                <td class="text-center"><?php echo number_format($key->ingresos) ?></td>
                                                                <td class="text-center"><?php echo number_format($key->activo) ?></td>
                                                                <td class="text-center"><?php echo number_format($key->pasivo) ?></td>
                                                                <td class="text-center"><?php echo number_format($key->patrimonio) ?></td>
                                                                <td class="text-center"><?php echo number_format($key->utilidad) ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <br>
                                        <b>*</b>
                                        Capacidad OxI estimada en función a facturación máxima entre 2015 al 2019, aplicando 10% de utilidad antes de impuestos, o utilidad neta declarada 2019.
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="form-group not-bottom">
                                                            <label>Importaciones y Exportaciones</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">

                                                        <div class="scrollable" style="overflow-y: auto;max-height: 450px;">
                                                            <div id="txtexportaciones"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="scrollable" style="overflow-y: auto;max-height: 350px;">
                                                            <div id="txtexportacionesTable"></div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div id="divTablaproyectosComnpanies"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
    }
}
?>