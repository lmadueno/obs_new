<?php
ini_set("memory_limit","5G");
ini_set('max_execution_time', 0); 
// CREATE PHPSPREADSHEET OBJECT
require 'php/spreadsheet/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

$data   = json_decode($_GET['data']);
$sql_filter = '';			
$sql  = "SELECT 
pr.codproyecto,
pr.organizacion,
pr.pia,
pr.descripcion AS oportunidad,
fg.codfuncionproyecto,
fg.nomfuncionproyecto AS funcion,
po.codprogramaproyecto,
po.nomprogramaproyecto as programaproyecto,
pp.codsubprogramaproyecto,
pp.nomsubprograma AS programa,
pr.monto as monto,
pr.montoactualizado,
pr.codigosnip,
pr.codigounico,
CASE pr.codestadoproyecto
	WHEN 1 THEN 'ACTIVO'
	ELSE 'INACTIVO'
END AS estado,
pr.coddpto::integer as coddpto,
pr.codprov,
pr.coddist,
pr.coddpto || pr.codprov as codubigeo,
op.codorigen,
op.nomorigen AS origen,
dp.nomdpto AS region,
fp.codfase,
fp.nomfase AS fasegobierno,
ng.codnivelgobierno,
ng.nomnivelgobierno AS nivelgobierno,
pr.fecregistro,
to_char(to_date(fecregistro,'YYYY-MM-DD'), 'DD/MM/YYYY') as fecregistro2,
pr.ubicacion AS geometry,
CASE pr.codnivelgobierno
	WHEN 1 THEN 'fa-home'
	WHEN 2 THEN 'fa-building'
	WHEN 3 THEN 'fa-hospital'
	WHEN 4 THEN 'fa-university'
	WHEN 5 THEN 'fa-place-of-worship'
	ELSE NULL
END AS nivel,
CASE pr.codfase
	WHEN 1 THEN 'yellow'
	WHEN 2 THEN 'orange'
	WHEN 3 THEN 'red'
	WHEN 4 THEN 'green'
	WHEN 5 THEN ''
	WHEN 6 THEN 'cyan'
	WHEN 7 THEN 'white'
	WHEN 8 THEN 'purple'
	WHEN 9 THEN 'green'
	WHEN 10 THEN 'green-dark'
	WHEN 11 THEN 'blue'
	ELSE NULL
END AS fase,
pr.flag_seguimiento as flag_seguimiento,
pr.tope_cipril as tope_ciprl,
pr.estadooxi,
pr.poblacion
FROM obs.gen_proyecto pr
left JOIN obs.gen_funcionproyecto fg ON fg.codfuncionproyecto = pr.codfuncionproyecto
left JOIN obs.gen_programaproyecto po ON po.codprogramaproyecto = pr.codprogramaproyecto
left JOIN obs.gen_origenproyecto op ON op.codorigen = pr.codorigen
left JOIN obs.gen_departamento dp ON dp.coddpto = pr.coddpto
left JOIN obs.gen_faseproyecto fp ON fp.codfase = pr.codfase
left JOIN obs.gen_nivelgobierno ng ON ng.codnivelgobierno = pr.codnivelgobierno
left JOIN obs.gen_subprogramaproyecto pp ON pp.codprogramaproyecto = pr.codprogramaproyecto AND pp.codsubprogramaproyecto = pr.codsubprogramaproyecto
left JOIN distrito ds on ds.iddist = pr.coddpto || pr.codprov || pr.coddist
where

 ";

// -- pr.codfase not in(10) and
// -- pr.monto >= 0 and pr.tope_cipril >= 0  limit 1000";	

$sql_filter = $_GET['data'];
$sql_filter = str_replace("coddpto::integer", "pr.coddpto::integer", $sql_filter);
$sql_filter = str_replace("codfase", "pr.codfase", $sql_filter);
$sql_filter = str_replace("codnivelgobierno", "pr.codnivelgobierno", $sql_filter);
$sql_filter = str_replace("codfuncionproyecto", "pr.codfuncionproyecto", $sql_filter);
$sql_filter = str_replace("codsubprogramaproyecto", "pr.codsubprogramaproyecto", $sql_filter);
$sql_filter = str_replace("coalesce(tope_cipril,0)", "coalesce(tope_cipril,0)", $sql_filter);
$sql_filter = str_replace("coddpto||codprov", "pr.coddpto || pr.codprov", $sql_filter);
$sql_filter = str_replace("oportunidad", "pr.descripcion", $sql_filter);
$sql_filter = str_replace("codprogramaproyecto", "pr.codprogramaproyecto", $sql_filter);
$sql_filter = str_replace("estadooxi", "pr.estadooxi", $sql_filter);

$connect = pg_connect("host=localhost port=5432 dbname=colaboraccion user=postgres password=geoserver");
$result = pg_query($connect, $sql . ' ' . $sql_filter );

pg_close($connect);

// CREATE A NEW SPREADSHEET + SET METADATA
$spreadsheet = new Spreadsheet();
$spreadsheet->getProperties()
->setCreator('Colaboraccion')
->setLastModifiedBy('Colaboraccion')
->setTitle('Proyectos PIP')
->setSubject('Proyectos PIP')
->setDescription('Proyectos PIP')
->setKeywords('Proyectos PIP')
->setCategory('Proyectos PIP');
 
// NEW WORKSHEET
$sheet = $spreadsheet->getActiveSheet();
$sheet->setTitle('Proyectos');

$rowCount = 2;

while ($item = pg_fetch_object($result)) {
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('A' . $rowCount, $item->codproyecto);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('B' . $rowCount, $item->region);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('C' . $rowCount, $item->organizacion);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('D' . $rowCount, $item->oportunidad);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('E' . $rowCount, $item->funcion);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('F' . $rowCount, $item->programa);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('G' . $rowCount, $item->fasegobierno);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('H' . $rowCount, $item->nivelgobierno);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('I' . $rowCount, $item->montoactualizado);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('J' . $rowCount, $item->codigosnip);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('K' . $rowCount, $item->codigounico);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('L' . $rowCount, $item->origen);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('M' . $rowCount, $item->fecregistro);
	$spreadsheet->setActiveSheetIndex(0)->setCellValue('N' . $rowCount, $item->estado);
	$rowCount++;
}

$cell_st =[
	'font' =>['bold' => true],
	'alignment' =>['horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER],
	'borders'=>['bottom' =>['style'=> \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM]]
];

$spreadsheet->getActiveSheet()->getStyle('A1:N1')->applyFromArray($cell_st);

foreach (range('A','N') as $col) {
	$spreadsheet->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);  
}

$sheet
->setCellValue('A1', 'Código')
->setCellValue('B1', 'Región')
->setCellValue('C1', 'Organización')
->setCellValue('D1', 'Oportunidad')
->setCellValue('E1', 'Función')
->setCellValue('F1', 'Programa')
->setCellValue('G1', 'Fase')
->setCellValue('H1', 'Nivel')
->setCellValue('I1', 'Monto Asignado')
->setCellValue('J1', 'SNIP')
->setCellValue('K1', 'Cod. Único')
->setCellValue('L1', 'Origen')
->setCellValue('M1', 'F. Registro')
->setCellValue('N1', 'Estado');

// OUTPUT
$writer = new Xlsx($spreadsheet);
// $writer->save('C:\/asd.csv');
// OR FORCE DOWNLOAD
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Proyectos.xlsx"');
header('Cache-Control: max-age=0');
header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');
$writer->save('php://output');
?>
