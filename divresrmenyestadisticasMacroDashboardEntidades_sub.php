<?php
require 'php/app.php';
$data = json_decode($_GET['data']);
?>
<table class="table table-sm table-detail" width="100%">
	<thead>
		<tr>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">#</th>
			<?php if ($data->nivel == 2) { ?>
				<th class="text-center bold" style="background:#ddebf8;text-align:center">Region</th>
			<?php } else if ($data->nivel == 3) {  ?>
				<th class="text-center bold" style="background:#ddebf8;text-align:center">Region</th>
				<th class="text-center bold" style="background:#ddebf8;text-align:center">Provincia</th>
			<?php } ?>

			<th class="text-center bold" style="background:#ddebf8;text-align:center">Entidad</th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">CIPRL</th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables sin ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables sin ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables con ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables con ejecución </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Cantidad PI viables </th>
			<th class="text-center bold" style="background:#ddebf8;text-align:center">Monto PI viables </th>
		</tr>
	</thead>
	<tbody>
		<?php
		$i = 0;
		$s1 = 0;
		$s2 = 0;
		$s3 = 0;
		$s4 = 0;
		$s5 = 0;
		$s6 = 0;
		$pipxUbigeoDet = dropDownList((object) ['method' => 'pi_partidospoliticos_sub_entidades', 'nivel' => $data->nivel, 'partido' => $data->partido]);
		foreach ($pipxUbigeoDet as $item) {
			$i++;

			$s2 =   $s2 + $item->csn;
			$s3 =   $s3 + $item->msn;
			$s4 =   $s4 + $item->ccn;
			$s5 =   $s5 + $item->mcn;
			$s6 =   $s6 + $item->ciprl;
		?>
			<tr>
				<td class="text-center"><?php echo $i; ?></td>
				<?php if ($data->nivel == 2) { ?>
					<td class="text-center"><?php echo $item->nomubigeo; ?></td>
				<?php } else if ($data->nivel == 3) {  ?>
					<td class="text-center"><?php echo $item->depart; ?></td>
					<td class="text-center"><?php echo $item->provincia; ?></td>
				<?php }  ?>
				<td class="text-center"> <button class="btn btn-link lnkModalEntidades" id="<?php echo ($item->ubigeo) ?>" onclick="App.events(this);" data-toggle="modal" data-target="#exampleModal"> <?php echo ($item->nombre) ?></button></td>
				<td class="text-center"><?php echo number_format($item->ciprl) ?></td>
				<td class="text-center"> <button class="btn btn-link lnkAmpluarProyectosFase" id="<?php echo ($item->ubigeo . '-' . $data->nivel) ?>" data-toggle="modal" data-target="#exampleModal" onclick="App.events(this);"> <?php echo ($item->csn) ?></button></td>

				<td class="text-center"><?php echo number_format($item->msn) ?></td>

				<td class="text-center"> <button class="btn btn-link lnkAmpluarProyectosperfil" id="<?php echo ($item->ubigeo) ?>" onclick="App.events(this);"> <?php echo ($item->ccn) ?></button></td>
				<td class="text-center"><?php echo number_format($item->mcn) ?></td>
				<td class="text-center"><?php echo number_format($item->csn + $item->ccn) ?></td>
				<td class="text-center"><?php echo number_format($item->mcn + $item->msn) ?></td>
			</tr>
		<?php } ?>
		<tr>

			<td></td>
			<td>Total</td>
			<td class="text-center"><?php echo number_format($s6) ?></td>
			<td class="text-center"><?php echo number_format($s2) ?></td>
			<td class="text-center"><?php echo number_format($s3) ?></td>
			<td class="text-center"><?php echo number_format($s4) ?></td>
			<td class="text-center"><?php echo number_format($s5) ?></td>
			<td class="text-center"><?php echo number_format($s2 + $s4) ?></td>
			<td class="text-center"><?php echo number_format($s3 + $s5) ?></td>
		</tr>
	</tbody>
</table>