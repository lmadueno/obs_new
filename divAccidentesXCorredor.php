

<?php
    require 'php/app.php';

            $data = json_decode($_GET['data']);	
            $respuesta= dropDownList((object) ['method' => 'accidentexXcorredor','corredor'=> $data->corredor]);
?>

<div class="row">
        <div class="col-lg-12">
            <div class="card-header card-special text-center font-weight-bold">
                <div class="row">
                    <div class="col-10">Lista de Accidentes</div> 
                    <div class="col-2 text-right">
						<a href="#" class="lnkAmpliar"  data-event="lnk_entidadesFiltro1"  onclick="App.events(this); return false;"><i class="fa fa-chevron-down "></i></a>
					</div>
                </div>
            </div>
        </div>
        <div class="col-lg-12" data-target="lnk_entidadesFiltro1">
            <table class="table table-sm table-detail" >   
                <tr>
                    <td>#</td>
                    <td>Nombre</td>
                    <td>Accidentes</td>
                    <td>Muertos</td>
                    <td>Heridos</td>
                </tr>
                <?php    
                    $i=0;             
                    foreach ($respuesta as $item){
                        $i++;                     											
                ?>	
                <tr>
                    <td><?php echo number_format($i)?></td>
                    <td class="text-left">
                        <a class="lnkAmpliar" data-event="lnkProvXrutas_<?php echo $item->tca?>" href="#" onclick="App.events(this); return false;">
                            <?php echo $item->tca;?>
                        </a>													
                    </td>
                    <td class="text-right"><?php echo number_format($item->accidentes)?></td>
                    <td class="text-right"><?php echo number_format($item->muertos)?></td>
                    <td class="text-right"><?php echo number_format($item->heridos)?></td>
                </tr> 
                <tr data-target="lnkProvXrutas_<?php echo $item->tca?>" style="display: none;">
                    <td colspan="5"> 
                    <div class="card">
                        <div class="card-header card-special">
                                                            Distritos
                        </div>
                        <div class="car-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table class="table table-sm table-detail">
                                        <tr>
                                            <td>#</td>
                                            <td>Distrito</td>
                                            <td>Ciprl</td>
                                            <td>Canon</td>
                                            <td>PIM</td>
                                            <td>Poblacion</td>
                                        </tr>
                                        <?php    
                                            $i=0;  
                                            $respuesta= dropDownList((object) ['method' => 'accidentexXcorredorDistritos','tca'=> $item->tca]);          
                                            foreach ($respuesta as $item){
                                                $i++;                     											
                                            ?>	
                                                <tr>
                                                    <td><?php echo number_format($i)?></td>
                                                    <td><?php echo ($item->distrito)?></td>
                                                    <td><?php echo number_format($item->ciprl)?></td>
                                                    <td><?php echo number_format($item->canon)?></td>
                                                    <td><?php echo number_format($item->pim)?></td>
                                                    <td><?php echo number_format($item->valor)?></td>
                                                </tr>
                                                <?php                 
                                                     }               											
                                                 ?>
                                        </table> 
                                    </div>
                                 </div>
                             </div>
                        </div>
                    </td>   
                </tr>       
                <?php                 
                    }               											
                ?>	
            </table>                 
        </div>
    </div>
