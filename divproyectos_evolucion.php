<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);

?>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Cantidad total de Proyectos de Inversión registrados en INVIERTE
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Cantidad total por tipo de documento 2017 - 2021*
						</div>
						<div id='chart2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Cantidad total de Proyectos de inversión registrados en INVIERTE, por nivel de gobierno, 2017 - 2021*
						</div>
						<div id='chart1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Cantidad total de proyectos de inversión por estado de ejecución
							2017 - 2021*
						</div>
						<div id='chart3'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos" style="color: #1976D2; !important;" id="datos" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla estadísticas</b></a>
					<!-- </div> -->
					<div class="card card-outline-info" data-target="datos" style="display: none;">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Tabla de datos de Cantidad total de Proyectos de Inversión registrados en INVIERTE
						</div>
						<br>


						<table class="table table-hover table-sm" data-target="tabla1" width="100%">
							<thead>
								<tr>
									<th scope="col" class="text-center bold" >Gobierno</th>
									<th scope="col" class="text-center bold" >IOARR Viables</th>
									<th scope="col" class="text-center bold" >IOARR ejecución</th>
									<th scope="col" class="text-center bold" >IOARR total</th>
									<th scope="col" class="text-center bold" >PI viables</th>
									<th scope="col" class="text-center bold" >PI ejecución</th>
									<th scope="col" class="text-center bold" >PI total</th>
									<th scope="col" class="text-center bold">Viables</th>
									<th scope="col" class="text-center bold">Ejecución</th>
									<th scope="col" class="text-center bold">total</th>

								</tr>
							</thead>
							<tbody>
								<?php
								$i = 0;

								$sTotal = 0;
								$stotal = 0;
								$proyecto_monto_soles = dropDownList((object) ['method' => 'proyecto_monto_soles', 'fn' => 'count']);
								// echo print_r($proyecto_monto_soles, true);
								$t1 = intVal($proyecto_monto_soles[12]->{"sum"});
								$t2 = intVal($proyecto_monto_soles[13]->{"sum"});
								$t3 = $t1 + $t2;
								$t4 = intVal($proyecto_monto_soles[14]->{"sum"});
								$t5 = intVal($proyecto_monto_soles[15]->{"sum"});
								$t6 = $t4 + $t5;

								$t7 = $t1 + $t4;
								$t8 = $t2 + $t5;
								$t9 = $t7 + $t8;

								$x = 0;
								for ($i = 0; $i < 3; $i++) {
									$o = ['GN', 'GR', 'GL', 'Total'];

								?>
									<tr>
										<td scope="row" class="text-center" ><?php echo $o[$i] ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x + 1]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x]->{"sum"} + $proyecto_monto_soles[$x + 1]->{"sum"}) ?></td>

										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x + 2]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x + 3]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x + 2]->{"sum"} + $proyecto_monto_soles[$x + 3]->{"sum"}) ?></td>

										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x]->{"sum"} + $proyecto_monto_soles[$x + 2]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x + 1]->{"sum"} + $proyecto_monto_soles[$x + 3]->{"sum"}) ?></td>
										<td scope="row" class="text-center"><?php echo number_format($proyecto_monto_soles[$x]->{"sum"} + $proyecto_monto_soles[$x + 1]->{"sum"} + $proyecto_monto_soles[$x + 2]->{"sum"} + $proyecto_monto_soles[$x + 3]->{"sum"}) ?></td>

									</tr>
								<?php
									$x = $x + 4;
								}

								?>
								<tr>
									<td class="text-center" >Total</td>
									<td class="text-center"><?php echo number_format($t1) ?></td>
									<td class="text-center"><?php echo number_format($t2) ?></td>
									<td class="text-center"><?php echo number_format($t3) ?></td>
									<td class="text-center"><?php echo number_format($t4) ?></td>
									<td class="text-center"><?php echo number_format($t5) ?></td>
									<td class="text-center"><?php echo number_format($t6) ?></td>
									<td class="text-center"><?php echo number_format($t7) ?></td>
									<td class="text-center"><?php echo number_format($t8) ?></td>
									<td class="text-center"><?php echo number_format($t9) ?></td>
								</tr>
							</tbody>
						</table>

					</div>
				</div>

			</div>
			<br>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vernivel" data-event="nivel" style="color: #1976D2; !important;" id="nivel" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Gráfico por Niveles(GN,GR,GL)</b></a>
					<!-- </div> -->
				</div>
			</div>
			<div class="row" data-target="nivel" style="display: none; ">

				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Cantidad total por tipo de documento 2017 - 2021*
						</div>
						<br>
						<div class="row">
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Gobierno Nacional
									</div>
									<div id='chart11'></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Gobierno Regional
									</div>
									<div id='chart22'></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Gobierno Local
									</div>
									<div id='chart33'></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Cantidad total de proyectos por estado de ejecución 2017 - 2021*

						</div>
						<br>
						<div class="row">
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										 Gobierno Nacional
									</div>
									<div id='chart111'></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										 Gobierno Regional
									</div>
									<div id='chart222'></div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Gobierno Local
									</div>
									<div id='chart333'></div>
								</div>
							</div>
						</div>

					</div>

				</div>

			</div>
		</div>
		<br>
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Cantidad totales por tipo de formato 2017 - 2021*
			</div>
			<br>
			<div class="row">
				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							IOARR
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Cantidad IOARR total por nivel de gobierno 2017 - 2021*
									</div>
									<div id='chart_1'></div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Cantidad IOARR por estado de ejecución 2017 - 2021*
									</div>
									<div id='chart_2'></div>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							PROYECTOS DE INVERSIÓN
						</div>
						<br>
						<div class="row">
							<div class="col-lg-6">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Cantidad Proyectos de inversión por nivel de gobierno 2017 - 2021*
									</div>
									<div id='chart_3'></div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="card card-outline-info">
									<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
										Cantidad Proyectos de inversión x estado de ejecución 2017 - 2021*
									</div>
									<div id='chart_4'></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active ver_nivel" data-event="nivel_1" style="color: #1976D2; !important;" id="nivel_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Gráfico por Niveles(GN,GR,GL)</b></a>
					<!-- </div> -->
				</div>
			</div>
			<div class="card">
				<div class="row" data-target="nivel_1" style="display: none;">
					<div class="col-lg-6">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Cantidad IOARR, estado de ejecución por niveles 2017 - 2021*
							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											 Gobierno Nacional
										</div>
										<div id='chart_11'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											 Gobierno Regional
										</div>
										<div id='chart_22'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											 Gobierno Local
										</div>
										<div id='chart_33'></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="card card-outline-info">
							<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
								Cantidad Proyectos de inversión, estado de ejecución por niveles 2017 - 2021*

							</div>
							<br>
							<div class="row">
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											 Gobierno Nacional
										</div>
										<div id='chart_111'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											 Gobierno Regional
										</div>
										<div id='chart_222'></div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Gobierno Local
										</div>
										<div id='chart_333'></div>
									</div>
								</div>
							</div>

						</div>

					</div>

				</div>
			</div>

		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Evolución de proyectos de inversión 2017 - 2021*
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de proyectos de inversión por estado viable y ejecución 2017 - 2021*
						</div>
						<div id='chart__1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de IOARR en estado viable y ejecución 2017 - 2021*
						</div>
						<div id='chart__2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de Perfiles por estado viable y ejecución 2017 - 2021*
						</div>
						<div id='chart__3'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos" style="color: #1976D2; !important;" id="datos" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Evolución acumulada de proyectos de inversión 2017 - 2021*</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de proyectos de inversión 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de IOARR 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato', 'fn' => 'count', 'formato' => "IOARR"]);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de perfiles 2017 - 2021*
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato', 'fn' => 'count', 'formato' => "PROYECTO','PROGRAMA DE INVERSION"]);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Evolución de proyectos de inversión 2017 - 2021* - Gobierno Nacional
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de proyectos de inversióon por estado viable y ejecución 2017 - 2021*- Gobierno Nacional
						</div>
						<div id='chartgn1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de IOARR en estado viable y ejecución 2017 - 2021* Gobierno Nacional
						</div>
						<div id='chartgn2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de Perfiles en estado viable y ejecución 2017 - 2021* Gobierno Nacional
						</div>
						<div id='chartgn3'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos2" style="color: #1976D2; !important;" id="datos2" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Evolución acumulada de proyectos de inversión 2017 - 2021*</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos2" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de proyectos de inversión 2017 - 2021* Gobierno Nacional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => "IOARR','PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GN', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos2" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de IOARR 2017 - 2021* Gobierno Nacional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => 'IOARR', 'nivel' => 'GN', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos2" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de perfiles 2017 - 2021* Gobierno Nacional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos2" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'fn' => 'count', 'formato' => "PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GN']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Evolución de proyectos de inversión 2017 - 2021* Gobierno Regional
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de proyectos de inversión por estado viable y ejecución 2017 - 2021* Gobierno Regional
						</div>
						<div id='chartgr1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de IOARR en estado viable y ejecución 2017 - 2021* Gobierno Regional
						</div>
						<div id='chartgr2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de Perfiles en estado viable y ejecución 2017 - 2021 Gobierno Regional
						</div>
						<div id='chartgr3'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos3" style="color: #1976D2; !important;" id="datos3" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Evolución acumulada de proyectos de inversión 2017 - 2021*</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos3" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de proyectos de inversión 2017 - 2021* Gobierno Regional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos3" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => "IOARR','PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GR', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos3" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de IOARR 2017 - 2021* Gobierno Regional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos3" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => 'IOARR', 'nivel' => 'GR', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos3" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de perfiles 2017 - 2021* Gobierno Regional
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos3" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'fn' => 'count', 'formato' => "PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GR']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<br>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Evolución de proyectos de inversión 2017 - 2021* Gobierno Local
			</div>
			<br>
			<div class="row">
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de proyectos de inversión por estado viable y ejecución 2017 - 2021* Gobierno Local
						</div>
						<div id='chartgl1'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de IOARR en estado viable y ejecución 2017 - 2021* Gobierno Local
						</div>
						<div id='chartgl2'></div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Evolución de Perfiles en estado viable y ejecución 2017 - 2021* Gobierno Local
						</div>
						<div id='chartgl3'></div>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="text-center col-lg-12">

					<a class="nav-link active vertabla" data-event="datos4" style="color: #1976D2; !important;" id="datos4" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Ver Tabla Evolución acumulada de proyectos de inversión 2017 - 2021*</b></a>
					<!-- </div> -->
					<div class="row">
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos4" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de proyectos de inversión 2017 - 2021* Gobierno Local
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos4" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => "IOARR','PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GL', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos4" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de IOARR 2017 - 2021* Gobierno Local
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos4" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'formato' => 'IOARR', 'nivel' => 'GL', 'fn' => 'count']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="card card-outline-info" data-target="datos4" style="display: none;">
								<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
									Tabla de datos de evolución acumulada de perfiles 2017 - 2021* Gobierno Local
								</div>
								<br>
								<table class="table table-hover table-sm" data-target="datos4" style="display: none;" width="100%">
									<thead>
										<tr>
											<th scope="col" class="text-center bold" width="4%">#</th>
											<th scope="col" class="text-center bold">Estado</th>
											<th scope="col" class="text-center bold">Viables</th>
											<th scope="col" class="text-center bold">Ejecutado</th>
											<th scope="col" class="text-center bold">Total</th>
										</tr>
									</thead>
									<tbody>
										<?php
										$i = 0;

										$sTotal = 0;
										$stotal = 0;
										$total = 0;
										$pipxUbigeoDet = dropDownList((object) ['method' => 'monto_evolucion_tipo_formato_nivel', 'fn' => 'count', 'formato' => "PROYECTO','PROGRAMA DE INVERSION", 'nivel' => 'GL']);
										foreach ($pipxUbigeoDet as $item) {
											$i++;

											$sTotal = $sTotal + intval($item->viable);
											$stotal = $stotal + intval($item->ejecutable);
											$total = $total + intval($item->ejecutable + $item->viable);

										?>
											<tr>
												<td scope="row" class="text-center" width="4%"><?php echo ($i) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ss) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->viable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable) ?></td>
												<td scope="row" class="text-center"><?php echo number_format($item->ejecutable + $item->viable) ?></td>

											</tr>
										<?php
										} ?>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total Acumulado</td>
											<td scope="row" class="text-center"><?php echo number_format($sTotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($stotal) ?></td>
											<td scope="row" class="text-center"><?php echo number_format($total) ?></td>

										</tr>
										<tr>
											<td scope="row" class="text-center" width="4%"></td>
											<td scope="row" class="text-center">Total % Acumulado</td>
											<td scope="row" class="text-center"><?php echo (round(($sTotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($stotal / $total), 2) * 100) . '%' ?></td>
											<td scope="row" class="text-center"><?php echo (round(($total / $total), 2) * 100) . '%' ?></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>