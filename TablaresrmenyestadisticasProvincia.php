<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
?>

<input type="hidden" id="txttablaReumenEstadisticaDepartamento" value="<?php echo $data->departamento ?>">
<div class="card">
    <div class="card-header card-special">
        Provincias
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-sm table-detail">
                    <thead>
                        <tr>
							<th class="text-center bold" width="4%">#</th>
                            <th class="text-center bold" width="1%"></th>
							<th class="text-center bold" width="15%">Provincia</th>
							<th class="text-center bold" width="15%"> 2018</th>
							<th class="text-center bold" width="15%"> 2019</th>
							<th class="text-center bold" width="15%"> 2020</th>      
							<th class="text-center bold" width="15%"> 2021</th>      
							<th class="text-center bold" width="15%">Total</th>  
                        </tr>
                    </thead>
                    <tbody>
						<?php 
									$i=0;   
									$s2018=0 ;	
									$s2019= 0;	
									$s2020= 0 ;	 
									$s2021= 0 ;	 
									$stotal= 0 ;     
									$pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacionProvincia','tipo'=> $data->tipo,'departamento'=> $data->departamento]);
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  
										$s2018=   $s2018+$item->s2018 ;	
										$s2019=   $s2019+$item->s2019 ;	
										$s2020=   $s2020+$item->s2020 ;	
										$s2021=   $s2021+$item->s2021 ;	
										$stotal=   $stotal+$item->total ;												
                        ?>                                                       
                        <tr>
							<td ><?php echo $i;?></td>
                            <td class="text-center" title="Ficha Entidad">
                                <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                                    <i class="fas fa-file-contract"></i>
                                </a>													
                            </td>
                            <td class="text-left">
                            	<a class="lnkAmpliarInfoResumenD" id="<?php echo $item->ubigeo;?>" data-event="lnkProvXrutas_<?php echo $item->ubigeo?>" href="#" onclick="App.events(this); return false;">
                                    <?php echo $item->nombprov;?>
                                </a>													
                            </td>
                            <td class="text-right" title="<?php echo number_format($item->s2018)?>"><?php echo number_format(round($item->s2018/1000000, 1),2)?></td>												
                            <td class="text-right" title="<?php echo number_format($item->s2019)?>"><?php echo number_format(round($item->s2019/1000000, 1),2)?></td>
                            <td class="text-right" title="<?php echo number_format($item->s2020)?>"><?php echo number_format(round($item->s2020/1000000, 1),2)?></td>
                            <td class="text-right" title="<?php echo number_format($item->s2021)?>"><?php echo number_format(round($item->s2021/1000000, 1),2)?></td>
							<td class="text-right" title="<?php echo number_format($item->total)?>"><?php echo number_format(round(($item->total)/1000000, 1),2)?></td>                                                                                                               					
                        </tr>
						<tr data-target="lnkProvXrutas_<?php echo $item->ubigeo?>" style="display: none;">
							<td colspan="8">
								<div id="divinformacionDistrito_<?php echo $item->ubigeo;?>">
								</div>
							</td>
						</tr>
                        <?php } ?>
                        <tr>
                            <td ></td>
                            <td ></td>
                            <td >Total</td>
                            <td class="text-right" ><?php echo number_format(round($s2018/1000000, 1),2)?></td>												
                            <td class="text-right"><?php echo number_format(round($s2019/1000000, 1),2)?></td>
                            <td class="text-right"><?php echo number_format(round($s2020/1000000, 1),2)?></td>
                            <td class="text-right"><?php echo number_format(round($s2021/1000000, 1),2)?></td>
							<td class="text-right"><?php echo number_format(round(($stotal)/1000000, 1),2)?></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>														
    </div>
</div>