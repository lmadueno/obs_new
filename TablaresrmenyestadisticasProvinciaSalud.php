<?php
    require 'php/app.php';		
    $data = json_decode($_GET['data']);	
    if($data->tipo=='MINSA'){
?>

<div class="card">
    <div class="card-header card-special">
        Provincias
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-sm table-detail">
                    <thead>
                        <tr>
							<th class="text-center bold" width="4%">#</th>
                            <th class="text-center bold" width="1%"></th>
                            <th class="text-center bold" width="1%"></th>
							<th class="text-center bold" width="15%">Provincia</th>
                            <th class="text-center bold" width="20%"> Poblacion</th>
                            <th class="text-center bold" width="20%"> Fallecidos</th>
                            <th class="text-center bold" width="20%"> Fallecidos/Pob.</th>    
							      
                        </tr>
                    </thead>
                    <tbody>
						<?php 
									$i=0;   
									$s2020= 0 ;	 
                                    $sPoblacion=0;   
                                    $sPoblacionfall=0;  
									$pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacionProvinciaMinsa','tipo'=> $data->tipo,'departamento'=> $data->departamento,'nombdepa'=>$data->nomdepar]);
                                  
                                    foreach ($pipxUbigeoDet as $item){
										$i++;  									
                                        $s2020=    $s2020+ (intval($item->anio2020)+intval($item->anio2021)) ;
                                        $sPoblacion=   $sPoblacion+$item->poblacion ;	
                                        // $sPoblacionfall=   $sPoblacionfall+$item->fatotalp20_21 ;	
																		
                        ?>                                                       
                        <tr>
							<td ><?php echo $i;?></td>
                            <td class="text-center" title="Ficha Entidad">
                                <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeop;?>" href="#" onclick="App.events(this); return false;">
                                    <i class="fas fa-file-contract"></i>
                                </a>													
                            </td>
                            <td class="text-center" title="Resumen de Fallecidos por Mes">
                                <a class="lnkAmpliarMeses" id="<?php echo $item->ubigeop;?>" href="#" onclick="App.events(this); return false;">
                                    <i class="fas fa-calendar-alt"></i>
                                </a>													
                            </td>
                            <td class="text-left">
                            	<a class="lnkAmpliarInfoResumenDSalud" id="<?php echo $item->ubigeop;?>" data-event="lnkProvXrutas_<?php echo $item->ubigeop?>" href="#" onclick="App.events(this); return false;">
                                    <?php echo $item->provincia;?>
                                </a>													
                            </td>
                            <td class="text-right" ><?php echo number_format($item->poblacion)?></td>
                            <td class="text-right"><?php echo intval($item->anio2020)+ intval($item->anio2021)?></td>
                            <td class="text-right" ><?php  echo number_format(((intval($item->anio2020)+ intval($item->anio2021))/$item->poblacion),3);?></td>
                        </tr>
						<tr data-target="lnkProvXrutas_<?php echo $item->ubigeop?>" style="display: none;">
							<td colspan="8">
								<div id="divinformacionDistritoSalud_<?php echo $item->ubigeop;?>">
								</div>
							</td>
						</tr>
                        <?php } ?>
                        <tr>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td >Total</td>
                           
                            <td class="text-right" ><?php echo number_format($sPoblacion)?></td>	
                            <td class="text-right" ><?php echo number_format($s2020)?></td>	
                            <td class="text-right" ><?php echo number_format($sPoblacionfall,2)?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>														
    </div>
</div>
<?php
    }else if($data->tipo=='SINADEF'){
?>
<table class="table table-sm table-detail" width="100%">
    <tr>
        <th class="text-center bold vert-middle" colspan="5" style=" background: #2f7ed8; color: white; ">General</th>											
		<th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2018</th>
		<th></th>
	    <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2019</th>
        <th></th>
	    <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2020</th>	
        <th></th>
	    <th class="text-center bold vert-middle" colspan="2" style=" background: #2f7ed8; color: white; ">2021</th>	
  													
	</tr> 
    <tr>
    
        <th class="text-center bold vert-middle">#</th>
        <th class="text-center bold vert-middle"></th>
        <th class="text-center bold vert-middle"></th>
        <th class="text-left bold vert-middle">Provincia</th>
        <th class="text-center bold vert-middle">Poblacion</th>
        <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
        <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>	
        <th style="border-bottom: 2px solid #dee2e600;"></th>
        <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
        <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
        <th style="border-bottom: 2px solid #dee2e600;"></th>
        <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
        <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
        <th style="border-bottom: 2px solid #dee2e600;"></th>
        <th class="text-center bold vert-middle" title="FALLECIDOS"><i class="fa fa-skull-crossbones"></i></th>
        <th class="text-center bold vert-middle"  title="FALLECIDOS/POBLACION"><i class="fa fa-user-alt-slash"></i></th>
       													
	</tr>   

    <?php 
		$i=0;  
        $Suma1=0 ; 
        $Suma2=0 ;                          
        $Suma3=0 ;
        $Suma5=0 ;
        $Suma7=0 ;
        $pipxUbigeoDet = dropDownList((object) ['method' => 'chartciprliformacionProvinciaMinsa','tipo'=> $data->tipo,'departamento'=> $data->departamento,'nomdepar'=>$data->nomdepar]);
       
        foreach ($pipxUbigeoDet as $item){
			$i++;  
            $Suma1+= $item->anio2018;   
            $Suma2+= $item->poblacion;                       
            $Suma3+= $item->anio2019;
            $Suma5+= $item->anio2020;
            $Suma7+= $item->anio2021;                           										
    ?>	
    <tr>
        <td  class="text-center"><?php echo $i;?></td>
            <td class="text-center" title="Ficha Entidad">
                <a class="lnkAmpliarEntidad" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                    <i class="fas fa-file-contract"></i>
                </a>													
            </td>
            <td class="text-center" title="Resumen de Fallecidos por Mes">
                <a class="lnkAmpliarMesesSinadef" id="<?php echo $item->ubigeo;?>" href="#" onclick="App.events(this); return false;">
                    <i class="fas fa-calendar-alt"></i>
                </a>													
            </td>
            <td class="text-left">
                <a class="lnkAmpliarInfoResumenDSalud" id="<?php echo $item->ubigeo;?>" data-event="lnkProvXrutas_<?php echo $item->ubigeo?>" href="#" onclick="App.events(this); return false;">
                    <?php echo $item->provincia_domicilio;?>
                </a>													
            </td>
            <td class="text-center"><?php echo number_format($item->poblacion)?></td>
            <td class="text-center"><?php echo number_format($item->anio2018)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2018_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2019)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2019_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2020)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2020_1000, 1),2)?></td>
            <td></td>
            <td class="text-center"><?php echo number_format($item->anio2021)?></td>
            <td class="text-center"><?php echo number_format(round($item->fallecidos_2021_1000, 1),2)?></td>
        
    </tr>
    <tr data-target="lnkProvXrutas_<?php echo $item->ubigeo?>" style="display: none;">
		<td colspan="19">
			<div id="divinformacionDistritoSalud_<?php echo $item->ubigeo;?>"></div>
		</td>
	</tr>
  
    <?php
        }
    ?>
    <tr>	
        <td  class="text-center"></td>
        <td  class="text-center"></td>
        <td  class="text-center"></td>
        <td  class="text-left">Total</td>
        <td  class="text-center"><?php echo number_format($Suma2);?></td>
        <td  class="text-center"><?php echo number_format($Suma1);?></td>
        <td  class="text-center"></td>
        <td></td>
        <td  class="text-center"><?php echo number_format($Suma3);?></td>
        <td  class="text-center"></td>
        <td></td>
        <td  class="text-center"><?php echo number_format($Suma5);?></td>
        <td  class="text-center"></td>
        <td></td>
        <td  class="text-center"><?php echo number_format($Suma7);?></td>
        <td  class="text-center"></td>                                   
    </tr>
</table>


<?php
    }
?>
