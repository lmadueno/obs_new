<?php
require 'php/appdash.php';
$data = json_decode($_GET['data']);


?>
<div class="row">
	<div class="col-lg-12">
		<div class="card card-outline-info">
			<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
				Analisis del proceso de vacunación 2021 COVID-19
			</div>
			<br>
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-primary alert-dismissible fade show" role="alert">
							<strong>Leyenda:</strong>
							<hr>
							<ul>
								<li>
									<?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'vacunas']);
									echo "(*)Fecha de actualizacion " . $pipxUbigeoDet[0]->fecha;
									?>
								</li>
							</ul>
							<button type="button" class="close" data-dismiss="alert" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-lg-12">
									<div class="card-body">
										<div class="row">
											<div class="col-lg-12">
												<div class="card card-outline-info">
													<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
														<div class="row">
															<div class="col-lg-12">
																<div class="panel panel-default">
																	<div class="panel-body">
																		<div class="row">
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Departamentos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDepartamentos_vacunas" id="ddlDepartamentos_vacunas" name="ddlDepartamentos_vacunas" onchange="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Provincias</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlProvincias_vacunas" id="ddlProvincias_vacunas" name="ddlProvincias_vacunas" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Distritos</label>
																					<select style="padding-left: 10px;" class="form-control input-sm ddlDistritos_minsa" id="ddlDistritos_vacunas" name="ddlDistritos_vacunas" onclick="App.events(this); return false;">
																						<option value="0">[Seleccione]</option>
																					</select>
																				</div>
																			</div>
																			<div class="col-lg-3">
																				<div class="form-group">
																					<label style="padding-left: 10px;">Accion</label>
																					<button type="button" class="form-control input-sm btn btn-primary filtrovacunas" id="filtrovacunas" name="filtrovacunas" onclick="App.events(this); return false;">Filtrar</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<br>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											% Avance de vacunación (2 dósis) a población mayor a 18 años por Departamento

										</div>
										<div id='chartvacuna_3b' style=" height: 800px; margin: 0 auto"></div>
										<div class="row">
											<div class="text-center col-lg-12">

												<a class="nav-link active vertabla" data-event="tb_vacuna_1" style="color: #1976D2; !important;" id="tb_vacuna_1" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Tabla % Avance de vacunación (2 dósis) a población mayor a 18 años por Ciudad</b></a>
												<!-- </div> -->
											</div>
										</div>
										<table class="table table-sm table-detail" data-target="tb_vacuna_1" width="100%" style="display: none;">
											<thead>
												<tr>
													<th class="text-center bold" width="4%">#</th>
													<th class="text-center bold">Ciudad</th>
													<th class="text-center bold">Población</th>
													<th class="text-center bold">Hombres</th>
													<th class="text-center bold">Mujeres</th>
													<th class="text-center bold">Total</th>
												</tr>
											</thead>
											<tbody id='tablavacuna1' style="text-align: center;">
											</tbody>

										</table>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Días para completar vacunación a población mayor a 18 años por Ciudad

										</div>
										<div id='chartvacuna_3' style=" height: 800px; margin: 0 auto"></div>
										<div class="row">
											<div class="text-center col-lg-12">

												<a class="nav-link active vertabla" data-event="tb_vacuna_2" style="color: #1976D2; !important;" id="tb_vacuna_2" href="#" onclick="App.events(this); return false;" title="Estadisticas"><b style="padding-left:0.5em;">Tabla Días para completar vacunación a población mayor a 18 años por ciudad</b></a>
												<!-- </div> -->
											</div>
										</div>
										<table class="table table-sm table-detail" data-target="tb_vacuna_2" width="100%" style="display: none;">
											<thead>
												<tr>
													<th class="text-center bold" width="4%">#</th>
													<th class="text-center bold">Ciudad</th>
													<th class="text-center bold">Poblacion</th>
													<th class="text-center bold">Pendientes</th>
													<th class="text-center bold">Velocidad</th>
													<th class="text-center bold">Años</th>
												</tr>
											</thead>
											<tbody id='tablavacuna2' style="text-align: center;">
											</tbody>

										</table>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<br>
								<div class="col-lg-12">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Avande población vacunada con 2da dosis al <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'vacunas']);
									echo " ( " . date_format(date_create($pipxUbigeoDet[0]->fecha),'d-M-Y'). ")";
									?>

										</div>
										<div id='tbl_genera'>
											<table class="table table-sm table-detail" data-target="tb_vacuna_23" width="100%">
												<thead>
													<tr>
														<th class="text-center bold" width="4%">#</th>
														<th class="text-center bold">Nacional rango edad</th>
														<th class="text-center bold">Población INEI</th>
														<th class="text-center bold">Poblacion vacunada</th>
														<th class="text-center bold">% Poblacion vacunada</th>
														<th class="text-center bold">Tiempo Estimado en completar vacunación</th>
													</tr>
												</thead>
												<tbody id='tablavacuna' style="text-align: center;">
													<tr>
														<td>1</td>
														<td>18-24 años</td>
														<td>3,709,970</td>
														<td>240,256</td>
														<td>6.48%</td>
														<td>2,932 Días</td>
													</tr>
													<tr>
														<td>2</td>
														<td>25-39 años</td>
														<td>7,743,422</td>
														<td>1,099,256</td>
														<td>14.20%</td>
														<td>1,227 Días</td>
													</tr>
													<tr>
														<td>3</td>
														<td>40-54 años</td>
														<td>5,943,055</td>
														<td>3,668,475</td>
														<td>61.73%</td>
														<td>126 Días</td>
													</tr>
													<tr>
														<td>4</td>
														<td>55-64 años</td>
														<td>2,726,579</td>
														<td>2,103,121</td>
														<td>77,13%</td>
														<td>60 Días</td>
													</tr>
													<tr>
														<td>5</td>
														<td>65-79 años</td>
														<td>2,283,730</td>
														<td>1,842,894</td>
														<td>80.70%</td>
														<td>49 Días</td>
													</tr>
													<tr>
														<td>6</td>
														<td>80+ años</td>
														<td>647,355</td>
														<td>514,248</td>
														<td>79.44%</td>
														<td>53 Días</td>
													</tr>
													<tr>
													<td></td>
														<td>Total</td>
														<td>23,054,111</td>
														<td>9,468,250</td>
														<td>41.07%</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<br>
							<div class="row">
								<br>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Estimación de días para vacunación de población por rango de edad al <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'vacunas']);
									echo "( " . date_format(date_create($pipxUbigeoDet[0]->fecha),'d-M-Y'). ")";
									?>

										</div>
										<div id='chartvacuna_1'></div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="card card-outline-info">
										<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
											Vacunación semanal (doble dósis) y acumulado al <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'vacunas']);
									echo "( " . date_format(date_create($pipxUbigeoDet[0]->fecha),'d-M-Y'). ")";
									?>
										</div>
										<div id='chartvacuna_2'></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<br>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Distribución de vacunados por rango de edad
						</div>
						<div id='chartvacuna_4'></div>
					</div>
					<br>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Distribución de vacunados por grupo riesgo
						</div>
						<div id='chartvacuna_5'></div>
					</div>
					<br>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							Avance de vacunación (2 dósis) según población x rango de edad
						</div>
						<div id='chartvacuna_6'></div>
					</div>
					<br>
					<div class="card card-outline-info">
						<div class="card-header card-special text-center font-weight-bold" style="background:#ddebf8">
							% Avance de vacunación (2 dósis) según población x rango de edad <?php $pipxUbigeoDet = dropDownList((object) ['method' => 'fechas_update', 'tipo' => 'vacunas']);
									echo "( " . date_format(date_create($pipxUbigeoDet[0]->fecha),'d-M-Y'). ")";
									?>
						</div>
						<div id='chartvacuna_7'></div>
					</div>
				</div>

			</div>

		</div>
	</div>
</div>
<br>

<br>